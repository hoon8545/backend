package com.bandi.excel;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.domain.Group;
import com.bandi.domain.User;
import com.bandi.domain.kaist.AccessElement;
import com.bandi.domain.kaist.Authority;
import com.bandi.domain.kaist.RoleAuthority;
import com.bandi.domain.kaist.RoleMaster;
import com.bandi.domain.kaist.RoleMember;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.AccessElementService;
import com.bandi.service.manage.kaist.AuthorityService;
import com.bandi.service.manage.kaist.GroupService_KAIST;
import com.bandi.service.manage.kaist.RoleAuthorityService;
import com.bandi.service.manage.kaist.RoleMasterService;
import com.bandi.service.manage.kaist.RoleMemberService;
import com.bandi.service.manage.kaist.UserService_KAIST;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/application-context.xml",
        "classpath:spring/security-context.xml" })
@ActiveProfiles("local")

public class ExcelParse {

    @Resource
    protected RoleMasterService roleMasterService;

    @Resource
    protected RoleMemberService roleMemberService;

    @Resource
    protected AuthorityService authorityService;

    @Resource
    protected RoleAuthorityService roleAuthorityService;

    @Resource
    protected AccessElementService accessElementService;

    @Resource
    protected GroupService_KAIST groupService;

    @Resource
    protected UserService_KAIST userService;

    // Excel 2007 이후 버전용 .xlsx
    public void parseDataFromXlsx(File file) throws Exception {

        FileInputStream fis = new FileInputStream(file);

        XSSFWorkbook xlsxWorkbook = new XSSFWorkbook(fis);

        fis.close();


        /* sheetNum = 시작할 sheet index
        * 2~6번째 index의 sheet만 읽음.
        */

        for (int sheetNum = 2; sheetNum <= 6; sheetNum++) {
            XSSFSheet sheet = xlsxWorkbook.getSheetAt(sheetNum);

            int rowIndex = sheet.getPhysicalNumberOfRows();

            // cell을 전체 읽을 때 사용.
            // int cellIndex = sheet.getRow(2).getPhysicalNumberOfCells();

            switch (sheetNum) {
            case 2:
                // 3~7번째 CELL만 읽는다.
                roleMasterInsert(sheet, rowIndex, 3, 7);
                break;
            case 3:
                // 3~4번째 CELL만 읽는다.
                roleMemberInsert(sheet, rowIndex, 3, 4);
                break;
            case 4:
                // 3~6번째 CELL만 읽는다.
                authorityInsert(sheet, rowIndex, 3, 6);
                break;
            case 5:
                // 3~4번째 CELL만 읽는다.
                roleAuthorityInsert(sheet, rowIndex, 3, 4);
                break;
            case 6:
                // 3~4번째 CELL만 읽는다.
                accessElementInsert(sheet, rowIndex, 3, 4);
                break;
            }

        }

        xlsxWorkbook.close();
    }

    public void roleMasterInsert(XSSFSheet sheet, int rowIndex, int cellStart, int cellEnd) {

        List<List<String>> rowList = setRowList(sheet, rowIndex, cellStart, cellEnd);

        for (List<String> rowData : rowList) {

            RoleMaster roleMaster = new RoleMaster();

            roleMaster.setOid(rowData.get(0));
            roleMaster.setName(rowData.get(1));
            roleMaster.setDescription(rowData.get(2));
            roleMaster.setGroupId(rowData.get(3));
            roleMaster.setRoleType(rowData.get(4));

            try {
                Group group = groupService.get(rowData.get(3));

                if (group == null) {
                    throw new BandiException(ErrorCode_KAIST.GROUP_IS_NOT_EXISTED);
                } else {
                    roleMasterService.insert(roleMaster);
                }

            } catch (Exception e) {
                // KAIST-TODO 이혜련: INSERT 하다가 에러나는 것들에 대해서 어떻게 할까 rollback????뭘까???
                e.printStackTrace();
            }
        }
    }

    public void roleMemberInsert(XSSFSheet sheet, int rowIndex, int cellStart, int cellEnd) {
        List<List<String>> rowList = setRowList(sheet, rowIndex, cellStart, cellEnd);

        for (List<String> rowData : rowList) {
            RoleMember rolemember = new RoleMember();
            rolemember.setRoleMasterOid(rowData.get(0));
            rolemember.setTargetObjectId(rowData.get(1));
            rolemember.setTargetObjectType(BandiConstants_KAIST.OBJECT_TYPE_USER);

            try {
                User user = userService.get(rowData.get(1));

                if (user == null) {
                    throw new BandiException(ErrorCode_KAIST.USER_NOT_FOUND);
                } else {
                    roleMemberService.insert(rolemember);
                }

            } catch (Exception e) {
                // KAIST-TODO 이혜련: INSERT 하다가 에러나는 것들에 대해서 어떻게 할까 rollback????뭘까???
                e.printStackTrace();
            }
        }

    }

    public void authorityInsert(XSSFSheet sheet, int rowIndex, int cellStart, int cellEnd) {
        List<List<String>> rowList = setRowList(sheet, rowIndex, cellStart, cellEnd);

        for (List<String> rowData : rowList) {
            Authority authority = new Authority();

            authority.setClientOid(rowData.get(0));
            authority.setOid(rowData.get(1));
            authority.setName(rowData.get(2));
            authority.setDescription(rowData.get(3));

            try {
                authorityService.insert(authority);
            } catch (Exception e) {
                // KAIST-TODO 이혜련: INSERT 하다가 에러나는 것들에 대해서 어떻게 할까 rollback????뭘까???
                e.printStackTrace();
            }
        }
    }

    public void roleAuthorityInsert(XSSFSheet sheet, int rowIndex, int cellStart, int cellEnd) {
        List<List<String>> rowList = setRowList(sheet, rowIndex, cellStart, cellEnd);

        for (List<String> rowData : rowList) {
            RoleAuthority rolaauthority = new RoleAuthority();

            rolaauthority.setRoleMasterOid(rowData.get(0));
            rolaauthority.setAuthorityOid(rowData.get(1));

            try {
                roleAuthorityService.insert(rolaauthority);
            } catch (Exception e) {
                // KAIST-TODO 이혜련: INSERT 하다가 에러나는 것들에 대해서 어떻게 할까 rollback????뭘까???
                e.printStackTrace();
            }
        }
    }

    public void accessElementInsert(XSSFSheet sheet, int rowIndex, int cellStart, int cellEnd) {
        List<List<String>> rowList = setRowList(sheet, rowIndex, cellStart, cellEnd);

        for (List<String> rowData : rowList) {

            AccessElement accesselement = new AccessElement();
            accesselement.setTargetObjectId(rowData.get(0));
            accesselement.setTargetObjectType(BandiConstants_KAIST.OBJECT_TYPE_AUTHORITY);
            accesselement.setResourceOid(rowData.get(1));

            try {
                accessElementService.insert(accesselement);
            } catch (Exception e) {
                // KAIST-TODO 이혜련: INSERT 하다가 에러나는 것들에 대해서 어떻게 할까 rollback????뭘까???
                e.printStackTrace();
            }
        }

    }

    public List<List<String>> setRowList(XSSFSheet sheet, int rowIndex, int cellStart, int cellEnd) {
        List<List<String>> rowList = new ArrayList<>();


        for (int rowNum = 5; rowNum <= rowIndex; rowNum++) {
            XSSFRow row = sheet.getRow(rowNum);

            if (row != null) {

                List<String> cellList = new ArrayList<>();

                // 기존에 데이터가 있다가 지워진 ROW일지라도 데이터가 있다고 Row를 읽어들인다. 따라서 첫번째 CELL에 데이터가 BLACK(공백) 값이면
                // 데이터가 없는 ROW로 판단하여 제외한다.
                if(row.getCell(cellStart).getCellType() != XSSFCell.CELL_TYPE_BLANK) {
                    for (int cellNum = cellStart; cellNum <= cellEnd; cellNum++) {
                        XSSFCell cell = row.getCell(cellNum);

                        String value = "";
                        if (cell != null) {
                            switch (cell.getCellType()) {
                            case XSSFCell.CELL_TYPE_BLANK:
                                value = null;
                                break;
                            case XSSFCell.CELL_TYPE_NUMERIC:
                                value = String.valueOf((int) cell.getNumericCellValue());
                                break;
                            default:
                                value = cell.getStringCellValue();
                                break;
                            }
                        } else {
                            value = null;
                        }

                        cellList.add(value);
                    }

                    rowList.add(cellList);
                }
            }
        }
        return rowList;
    }

    // Excel 2007 이전 버전용 .xls
    /*
     * public void parseDataFromXls(File file) throws Exception {
     *
     * FileInputStream fis = new FileInputStream(file);
     *
     * HSSFWorkbook xlsWorkbook = new HSSFWorkbook(fis);
     *
     * fis.close();
     *
     * int sheetIndex = xlsWorkbook.getNumberOfSheets();
     *
     * List<Object> sheetNames = new ArrayList<>();
     *
     * Map<String, List<Object>> excelDataList = new HashMap<String,
     * List<Object>>();
     *
     * for (int sheetNum = 0; sheetNum < sheetIndex; sheetNum++) { // sheet의 갯수 만큼
     * 반복 HSSFSheet sheet = xlsWorkbook.getSheetAt(sheetNum);
     *
     * String sheetName = sheet.getSheetName();
     *
     * sheetNames.add(sheetName);
     *
     * int rowIndex = sheet.getPhysicalNumberOfRows();
     *
     * int cellIndex = sheet.getRow(1).getPhysicalNumberOfCells();
     *
     * List<Object> sheetList = new ArrayList<>();
     *
     * for (int rowNum = 2; rowNum <= rowIndex; rowNum++) { // row의 갯수만큼 반복 HSSFRow
     * row = sheet.getRow(rowNum);
     *
     * if (row != null) {
     *
     * List<Object> dataList = new ArrayList<>();
     *
     * int cellArrayIndex = 0;
     *
     * for (int cellNum = 1; cellNum <= cellIndex; cellNum++) { HSSFCell cell =
     * row.getCell(cellNum);
     *
     * String value = ""; if (cell != null) { switch (cell.getCellType()) {
     *
     * case HSSFCell.CELL_TYPE_FORMULA: value = cell.getCellFormula(); break;
     *
     * case HSSFCell.CELL_TYPE_BLANK: value = cell.getBooleanCellValue() + "";
     * break;
     *
     * case HSSFCell.CELL_TYPE_NUMERIC: value = cell.getNumericCellValue() + "";
     * break;
     *
     * case HSSFCell.CELL_TYPE_STRING: value = cell.getStringCellValue() + "";
     * break;
     *
     * case HSSFCell.CELL_TYPE_ERROR: value = cell.getErrorCellValue() + ""; break;
     *
     * } } else { value = "null"; }
     *
     * dataList.add(cellArrayIndex, value);
     *
     * if (cellArrayIndex == cellIndex - 1) { sheetList.add(rowNum - 2, dataList); }
     * cellArrayIndex++; } // cell data parse part
     *
     * if (rowNum == rowIndex - 1) { excelDataList.put("sheetNames", sheetNames);
     * excelDataList.put(sheetName, sheetList); }
     *
     * } } // rows data parse part }
     *
     * xlsWorkbook.close(); }
     */

    @Test
    public void excelParse() throws Exception {
        // 엑셀파일 경로
        File file = new File("D:\\test\\parse_roledata_v02.xlsx");

        String fileExtension = file.getName().substring(file.getName().lastIndexOf(".") + 1);

        if (fileExtension == "xls" || fileExtension.equals("xls")) {
            // parseDataFromXls(file);
        } else if (fileExtension == "xlsx" || fileExtension.equals("xlsx")) {
            parseDataFromXlsx(file);
        } else {
            throw new Exception("file type miss match");
        }

    }
    /*
     * public void insertDtoTest(Map<String, List<Object>> data) throws
     * ClassNotFoundException, InstantiationException, IllegalAccessException {
     * Client client = new Client(); String name = "com.bandi.domain.Client";
     * Class<?> cls = Class.forName(name);
     *
     * Object obj = cls.newInstance(); Client originObj = (Client) obj;
     * System.out.println(obj);
     *
     * List<T> test = new ArrayList<>(); List<Object> clientDataList =
     * data.get("Sheet1"); for (int j = 0; j < clientDataList.size(); j++) {
     * List<Object> cellData = (List<Object>) clientDataList.get(j);
     *
     * client.setOid(cellData.get(0).toString());
     * client.setName(cellData.get(1).toString());
     * client.setCreatorId(cellData.get(2).toString());
     *
     * System.out.println(
     * "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm");
     * System.out.println("oid = " +client.getOid()); System.out.println("name = "
     * +client.getName()); System.out.println("creatorId = "
     * +client.getCreatorId()); System.out.println(
     * "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm");
     *
     * } }
     */
}
