package com.bandi;

import com.bandi.exception.BandiException;
import com.bandi.service.manage.kaist.UserService_KAIST;
import com.bandi.service.sso.kaist.ClientApiService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith( SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml"})
@ActiveProfiles("local")

public class OTPTest{
    @Resource(name="initechClientApiService")
    protected ClientApiService clientApiService;

    @Resource
    protected UserService_KAIST userService;

    //@Test
    public void registerSerailNumber(){
        String userId = "0010nohyun";
        String serialNumber = "979934";
        boolean result = clientApiService.createOtpSerialNumber(userId, serialNumber);

        System.out.println( result );
    }

    @Test
    public void checkOTP(){
        String userId = "0010nohyun";
        String serialNumber = "026422";
        for( int i = 0; i < 15; i++){
            try{
                boolean result = clientApiService.checkOtp( userId, serialNumber );
                System.out.println( "otpResult:::: " + result );
            }catch( BandiException e){

                System.out.println( "otp " + i + " times : " + e.getErrorCode());

            }
        }

        boolean result = clientApiService.checkOtp( userId, serialNumber );
        System.out.println( "otpResult:::: " + result );
    }

    @Test
    public void unlockOTP(){
        String userId = "0010nohyun";
        String serialNumber = "28080485";
        String result = clientApiService.otpUnlock( userId, serialNumber);

        System.out.println( result );
    }

    //@Test
    public void checkStatus(){
        String userId = "0010nohyun";
        String result = clientApiService.otpCheckStatus( userId);

        System.out.println( result );
    }

}
