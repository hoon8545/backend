package com.bandi;

import com.bandi.domain.User;
import com.bandi.exception.BandiException;
import com.bandi.service.manage.RoleService;
import com.bandi.service.manage.kaist.ResourceService;
import com.bandi.service.manage.kaist.RoleMasterService;
import com.bandi.service.manage.kaist.RoleMemberService;
import com.bandi.service.manage.kaist.UserService_KAIST;
import com.bandi.service.sso.kaist.ClientApiService;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith( SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml"})
@ActiveProfiles("local")

public class ResourceAPITest{
    @Resource
    protected ResourceService resourceService;

    @Resource
    protected RoleMasterService roleMasterService;

    @Resource
    protected RoleMemberService roleMemberService;

    @Resource
    protected UserService_KAIST userService;

    //@Test
    public void getResourceByUser() throws Exception{

        String userId = "0010nohyun";
        //String userType = "P";
        String userType = "P";

        String resourceId = "";
        String clientId = "aMlzg4hH00K";
        String level = "ALL";

        List< String > userTypes = null;
        if( userType != null ){
            String[] arrTemp = userType.split( "" );
            userTypes = new ArrayList< String >( Arrays.asList( arrTemp ) );
        }

        List< String > rosourceOids = new ArrayList<>();

        if( resourceId != null & level != null ){
            rosourceOids = resourceService.getResourceByUser( userId, clientId, resourceId, level, userTypes, true );
        }

        if( rosourceOids != null ){

            ObjectMapper mapper = new ObjectMapper();

            String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString( rosourceOids );

            System.out.println( jsonString );

        }
    }

    //@Test
    public void getDelegetorUserId() throws Exception{
        String clientId = "aMlzg4hH00K";
        String resourceId = "aMrSQr3p05Y";
        String userId = "0010nohyun";

        String delegatorId = resourceService.getDelegatorId( clientId, resourceId, userId );

        System.out.println( "delegatorId --> " + delegatorId );
    }


    @Test
    public void getRoleByUser() throws Exception{
        String userId = "0010nohyun";

        User user = userService.get( userId );

        List<String> userTypes = null;
        if( user.getUserType() != null){
            String[] arrTemp = user.getUserType().split( "" );
            userTypes = new ArrayList<String>(Arrays.asList(arrTemp));
        }

        List<String> roleIds = roleMasterService.getRoleByUser( userId, userTypes);

        if( roleIds != null) {
            for( String roleId : roleIds){
                System.out.println(roleId);
            }
        }
    }


    //@Test
    public void hasAlreadyAuthority() throws Exception{

        String userId = "0010nohyun";
        List<String> authirityOids = new ArrayList<>();

        authirityOids.add( "aMrt2jRx00o");
        authirityOids.add( "aMrSTrJ907j");
        authirityOids.add( "aMmdgQ3j06v");
        authirityOids.add( "aMnChSkM00Q");

        boolean hasValue = roleMemberService.hasAlreadyAuthority( userId, authirityOids );

    }
}
