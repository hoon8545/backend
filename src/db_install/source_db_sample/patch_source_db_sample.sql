-- 03.04
ALTER TABLE CDUSER ADD ( TITLE VARCHAR(255 CHAR) );
ALTER TABLE CDUSER ADD ( POSITION VARCHAR(255 CHAR) );
ALTER TABLE CDUSER ADD ( RANK VARCHAR(255 CHAR) );
ALTER TABLE CDUSER ADD ( USERTYPE CHAR(1) );
ALTER TABLE CDUSER ADD ( FLAGTEMP CHAR(1) DEFAULT 'Y' NOT NULL );
ALTER TABLE CDUSER ADD ( TEMPSTARTDAT DATE );
ALTER TABLE CDUSER ADD ( TEMPENDDAT DATE );

-- 03.12
-- source_db_sample의 CDCODE, CDCODEDETAIL CREATE

-- 07.12
-- 기존에 있던 CDGROUP 삭제  -> source_db_sample_group.sql 실행. (cduser는 아직 진행안됨.)
DROP TABLE CDGROUP;

-- 07.29
-- 기존에 있던 CDGROUP 삭제  -> source_db_sample_group.sql 실행.
DROP TABLE CDGROUP;

-- 07.30
-- 기존에 있던 CDCODE 삭제  -> source_db_sample_code.sql 실행.
DROP TABLE CDCODE;

-- 07.31
-- 기존에 있던 CDUSER 삭제  -> source_db_sample_user.sql 실행.
DROP TABLE CDUSER;

-- 08.06
-- ** source_db_sample_* 로 시작하는 파일들은 용량이 커서 이클립스에서 열리지 않음. 에디터로 실행.

-- 1.  KAIST_UID 값이 현재 공유 DB에 없으므로(null임) 임시로 PERSON_ID 값을 이용하여 처리.
UPDATE eaiperson_map
SET KAIST_UID = PERSON_ID;

-- 2.  source_db_sampe_user에 EAIMAIN_ASSIGN 테이블(사용자 부서정보) CREATE, INSERT 추가됨.
-- 3.  03.create_db_eai_view 실행

-- 08.06
CREATE OR REPLACE VIEW EAIPERSONSYNCVIEW AS
SELECT pmas.*, pmap.KAIST_UID, pmap.SSO_UID, pgroup.ORGANIZATION_UID
FROM EAIPERSON_MASTER pmas
        LEFT OUTER JOIN EAIPERSON_MAP pmap
                     ON pmas.PERSON_UID = pmap.PERSON_UID
        LEFT OUTER JOIN 
                        (
                            SELECT etp.PERSON_UID, mass.ORGANIZATION_UID
                            FROM EAISCHOOL_EMPLOYEE_TO_PERSON etp
                                    LEFT OUTER JOIN (
                                                        SELECT EMPLOYEE_UID, ORGANIZATION_UID, BEGIN_DATE
                                                        FROM 
                                                            (
                                                                SELECT EMPLOYEE_UID, ORGANIZATION_UID, BEGIN_DATE, ROW_NUMBER() OVER (PARTITION BY EMPLOYEE_UID ORDER BY BEGIN_DATE DESC) AS rdx
                                                                FROM EAIMAIN_ASSIGN
                                                            )
                                                        WHERE rdx = 1
                                                    ) mass
                                                ON etp.EMPLOYEE_UID = mass.EMPLOYEE_UID
                        ) pgroup
                     ON pmas.PERSON_UID = pgroup.PERSON_UID;

-- 08.07
CREATE OR REPLACE VIEW EAIPERSONSYNCVIEW AS
SELECT pmas.*, pmap.KAIST_UID, pmap.SSO_UID, pmap.PERSON_ID, pgroup.ORGANIZATION_UID
FROM EAIPERSON_MASTER pmas
        LEFT OUTER JOIN EAIPERSON_MAP pmap
                     ON pmas.PERSON_UID = pmap.PERSON_UID
        LEFT OUTER JOIN 
                        (
                            SELECT etp.PERSON_UID, mass.ORGANIZATION_UID
                            FROM EAISCHOOL_EMPLOYEE_TO_PERSON etp
                                    LEFT OUTER JOIN (
                                                        SELECT EMPLOYEE_UID, ORGANIZATION_UID, BEGIN_DATE
                                                        FROM 
                                                            (
                                                                SELECT EMPLOYEE_UID, ORGANIZATION_UID, BEGIN_DATE, ROW_NUMBER() OVER (PARTITION BY EMPLOYEE_UID ORDER BY BEGIN_DATE DESC) AS rdx
                                                                FROM EAIMAIN_ASSIGN
                                                            )
                                                        WHERE rdx = 1
                                                    ) mass
                                                ON etp.EMPLOYEE_UID = mass.EMPLOYEE_UID
                        ) pgroup
                     ON pmas.PERSON_UID = pgroup.PERSON_UID;

CREATE OR REPLACE VIEW EAIPERSONINFOVIEW AS
SELECT pmas.*, pmap.KAIST_UID, pmap.SSO_UID, pcon.MOBILE_TELEPHONE_NUMBER, puse.BIRTHDAY, padd.POST_NUMBER, padd.ADDRESS, padd.ADDRESS_DETAIL
FROM EAIPERSON_MASTER pmas
        LEFT OUTER JOIN EAIPERSON_MAP pmap
                     ON pmas.PERSON_UID = pmap.PERSON_UID
        LEFT OUTER JOIN EAIPERSON_CONTACT_DETAIL pcon
                     ON pmas.PERSON_UID = pcon.PERSON_UID
        LEFT OUTER JOIN EAIPERSON_PRACTICAL_USE_DETAIL puse
                     ON pmas.PERSON_UID = puse.PERSON_UID
        LEFT OUTER JOIN EAIPERSON_ADDRESS_DETAIL padd
                     ON pmas.PERSON_UID = padd.PERSON_UID; 
                     
INSERT INTO BDSUSER(ID, GENERICID, GROUPID, ORIGINALGROUPID, PASSWORD, NAME, USERTYPE, KAISTUID, HASHVALUE, CREATORID)
SELECT SSO_UID, SSO_UID, ORGANIZATION_UID, ORGANIZATION_UID,  SSO_UID, KOREAN_NAME, PERSON_TYPE_CODE_UID, PERSON_ID, 1 AS HASHVALUE, 'initialRow' AS CREATORID FROM EAIPERSONSYNCVIEW
WHERE SSO_UID IS NOT NULL
AND ORGANIZATION_UID IS NOT NULL;

-- 08.07
DROP VIEW EAIPERSONSYNCVIEW;
DROP VIEW EAIPERSONINFOVIEW;

CREATE OR REPLACE VIEW EAIPERSONVIEW AS
SELECT pmas.*, pmap.KAIST_UID, pmap.SSO_UID, pmap.PERSON_ID, pcon.MOBILE_TELEPHONE_NUMBER, puse.BIRTHDAY, padd.POST_NUMBER, padd.ADDRESS, padd.ADDRESS_DETAIL, pgroup.ORGANIZATION_UID
FROM EAIPERSON_MASTER pmas
        LEFT OUTER JOIN EAIPERSON_MAP pmap
                     ON pmas.PERSON_UID = pmap.PERSON_UID
        LEFT OUTER JOIN EAIPERSON_CONTACT_DETAIL pcon
                     ON pmas.PERSON_UID = pcon.PERSON_UID
        LEFT OUTER JOIN EAIPERSON_PRACTICAL_USE_DETAIL puse
                     ON pmas.PERSON_UID = puse.PERSON_UID
        LEFT OUTER JOIN EAIPERSON_ADDRESS_DETAIL padd
                     ON pmas.PERSON_UID = padd.PERSON_UID 
        LEFT OUTER JOIN 
                        (
                            SELECT etp.PERSON_UID, mass.ORGANIZATION_UID
                            FROM EAISCHOOL_EMPLOYEE_TO_PERSON etp
                                    LEFT OUTER JOIN (
                                                        SELECT EMPLOYEE_UID, ORGANIZATION_UID, BEGIN_DATE
                                                        FROM 
                                                            (
                                                                SELECT EMPLOYEE_UID, ORGANIZATION_UID, BEGIN_DATE, ROW_NUMBER() OVER (PARTITION BY EMPLOYEE_UID ORDER BY BEGIN_DATE DESC) AS rdx
                                                                FROM EAIMAIN_ASSIGN
                                                            )
                                                        WHERE rdx = 1
                                                    ) mass
                                                ON etp.EMPLOYEE_UID = mass.EMPLOYEE_UID
                        ) pgroup
                     ON pmas.PERSON_UID = pgroup.PERSON_UID;
					 
-- 08.16
-- *** KAIST_UID(공유DB) 데이터 값이  NULL 이어서 PERSON_ID 값으로 대체 했었던 것을 KAIST_UID(기준정보) 데이터로 변경 ***
-- 1. '03.source_db_sample_user.sql' 파일의 'update' 구문만 다시 실행. (EAIPERSON_MAP의 임의의 KAISTUID 를 실제 값으로 변경)
-- 2. 'patch_source_db_sample_update.sql' 실행. (BDSUSER KAISTUID 변경)


-- 08.22
-- 기존 EAIPERSONVIEW에  EMAIL_ADDRESS 컬럼 추가 (MAIN_YN 값이 'Y'를 기준으로 MAX(DATE)로 처리, 앞의 값이 NULL 인경우는 MAIN_YN 'N'일 때로 처리)
CREATE OR REPLACE VIEW EAIPERSONVIEW AS                 
SELECT pmas.*, pmap.KAIST_UID, pmap.SSO_UID, pmail.EMAIL_ADDRESS, pmap.PERSON_ID, pcon.MOBILE_TELEPHONE_NUMBER, pcon.OWE_HOME_TELEPHONE_NUMBER, puse.BIRTHDAY, puse.RESIDENT_NUMBER, padd.POST_NUMBER, padd.ADDRESS, padd.ADDRESS_DETAIL, pgroup.ORGANIZATION_UID, pgroup.EMPLOYEE_UID 
FROM EAIPERSON_MASTER pmas
        LEFT OUTER JOIN EAIPERSON_MAP pmap
                     ON pmas.PERSON_UID = pmap.PERSON_UID
        LEFT OUTER JOIN EAIPERSON_CONTACT_DETAIL pcon
                     ON pmas.PERSON_UID = pcon.PERSON_UID
        LEFT OUTER JOIN EAIPERSON_PRACTICAL_USE_DETAIL puse
                     ON pmas.PERSON_UID = puse.PERSON_UID
        LEFT OUTER JOIN EAIPERSON_ADDRESS_DETAIL padd
                     ON pmas.PERSON_UID = padd.PERSON_UID 
        LEFT OUTER JOIN 
                        (
                            SELECT etp.PERSON_UID, etp.EMPLOYEE_UID, mass.ORGANIZATION_UID
                            FROM EAISCHOOL_EMPLOYEE_TO_PERSON etp
                                    LEFT OUTER JOIN (
                                                        SELECT EMPLOYEE_UID, ORGANIZATION_UID, BEGIN_DATE
                                                        FROM 
                                                            (
                                                                SELECT EMPLOYEE_UID, ORGANIZATION_UID, BEGIN_DATE, ROW_NUMBER() OVER (PARTITION BY EMPLOYEE_UID ORDER BY BEGIN_DATE DESC) AS rdx
                                                                FROM EAIMAIN_ASSIGN
                                                            )
                                                        WHERE rdx = 1
                                                    ) mass
                                                ON etp.EMPLOYEE_UID = mass.EMPLOYEE_UID
                        ) pgroup
                     ON pmas.PERSON_UID = pgroup.PERSON_UID
        LEFT OUTER JOIN
                        (
                            SELECT pmail.PERSON_UID,
                                    CASE WHEN pmail_y.EMAIL_ADDRESS IS NULL THEN
                                    (
                                            SELECT EMAIL_ADDRESS
                                                    FROM
                                                        (
                                                            SELECT PERSON_UID, EMAIL_ADDRESS, CREATE_DATE_TIME, ROW_NUMBER() OVER (PARTITION BY PERSON_UID ORDER BY CREATE_DATE_TIME DESC) AS rdx
                                                            FROM EAIPERSON_EMAIL_DETAIL
                                                            WHERE PERSON_UID = pmail.PERSON_UID
                                                            AND MAIN_YN = 'N'
                                                        )
                                            WHERE rdx = 1
                                    )
                                    ELSE pmail_y.EMAIL_ADDRESS END AS EMAIL_ADDRESS
                            FROM (
                                    SELECT DISTINCT PERSON_UID
                                    FROM EAIPERSON_EMAIL_DETAIL
                                    ) pmail
                                    LEFT OUTER JOIN(
                                                    SELECT PERSON_UID, EMAIL_ADDRESS
                                                    FROM (
                                                        SELECT PERSON_UID, EMAIL_ADDRESS, CREATE_DATE_TIME, ROW_NUMBER() OVER (PARTITION BY PERSON_UID ORDER BY CREATE_DATE_TIME DESC) AS rdx
                                                        FROM EAIPERSON_EMAIL_DETAIL
                                                        WHERE MAIN_YN = 'Y'
                                                    )
                                                    WHERE rdx = 1
                                                    )pmail_y
                                                 ON pmail.PERSON_UID = pmail_y.PERSON_UID
                        ) pmail
                     ON pmas.PERSON_UID = pmail.PERSON_UID;
                     
-- !!! 기존에 연동된 사용자들(INSERT~SELECT 구문) BDSUSER 테이블에  EMAIL, HANDPHONE 컬럼 업데이트.                     
UPDATE BDSUSER
SET EMAIL = (SELECT EMAIL_ADDRESS FROM EAIPERSONVIEW WHERE KAIST_UID = KAISTUID),
HANDPHONE = (SELECT MOBILE_TELEPHONE_NUMBER FROM EAIPERSONVIEW WHERE KAIST_UID = KAISTUID);

-- 08.29
-- 본인인증 NICE API 기능을 사용하려면 테스트 대상인 PERSON_CI 테이블이 필요함.
-- 03.source_db_sample_person_ci.sql 실행

commit;
