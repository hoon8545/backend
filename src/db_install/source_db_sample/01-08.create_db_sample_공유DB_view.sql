-- 조직도 연동 대상 EAI VIEW
CREATE OR REPLACE VIEW EAIORGANIZATIONVIEW AS
SELECT osd.*, om.CORPORATION_UID, om.CAMPUS_UID, om.KOREAN_NAME, om.ENGLISH_NAME, om.ORGANIZATION_TYPE_CODE_UID FROM EAIORGANIZATION_STRUCT_DETAIL osd
LEFT OUTER JOIN EAIORGANIZATION_MASTER om
ON om.ORGANIZATION_UID = osd.ORGANIZATION_UID
WHERE ORGANIZATION_STRUCT_UID = (
    SELECT MAX(ORGANIZATION_STRUCT_UID) KEEP (DENSE_RANK FIRST ORDER BY BEGIN_DATE DESC) FROM EAIORGANIZATION_STRUCT_MASTER
);

-- 사용자 연동 대상 및 기본정보 VIEW
CREATE OR REPLACE VIEW EAIPERSONVIEW AS                 
SELECT pmas.*, pmap.KAIST_UID, pmap.SSO_UID, pmail.EMAIL_ADDRESS, pmap.PERSON_ID, pcon.MOBILE_TELEPHONE_NUMBER, pcon.OWE_HOME_TELEPHONE_NUMBER, puse.BIRTHDAY, puse.RESIDENT_NUMBER, padd.POST_NUMBER, padd.ADDRESS, padd.ADDRESS_DETAIL, pgroup.ORGANIZATION_UID, pgroup.EMPLOYEE_UID 
FROM EAIPERSON_MASTER pmas
        LEFT OUTER JOIN EAIPERSON_MAP pmap
                     ON pmas.PERSON_UID = pmap.PERSON_UID
        LEFT OUTER JOIN EAIPERSON_CONTACT_DETAIL pcon
                     ON pmas.PERSON_UID = pcon.PERSON_UID
        LEFT OUTER JOIN EAIPERSON_PRACTICAL_USE_DETAIL puse
                     ON pmas.PERSON_UID = puse.PERSON_UID
        LEFT OUTER JOIN EAIPERSON_ADDRESS_DETAIL padd
                     ON pmas.PERSON_UID = padd.PERSON_UID 
        LEFT OUTER JOIN 
                        (
                            SELECT etp.PERSON_UID, etp.EMPLOYEE_UID, mass.ORGANIZATION_UID
                            FROM EAISCHOOL_EMPLOYEE_TO_PERSON etp
                                    LEFT OUTER JOIN (
                                                        SELECT EMPLOYEE_UID, ORGANIZATION_UID, BEGIN_DATE
                                                        FROM 
                                                            (
                                                                SELECT EMPLOYEE_UID, ORGANIZATION_UID, BEGIN_DATE, ROW_NUMBER() OVER (PARTITION BY EMPLOYEE_UID ORDER BY BEGIN_DATE DESC) AS rdx
                                                                FROM EAIMAIN_ASSIGN
                                                            )
                                                        WHERE rdx = 1
                                                    ) mass
                                                ON etp.EMPLOYEE_UID = mass.EMPLOYEE_UID
                        ) pgroup
                     ON pmas.PERSON_UID = pgroup.PERSON_UID
        LEFT OUTER JOIN
                        (
                            SELECT pmail.PERSON_UID,
                                    CASE WHEN pmail_y.EMAIL_ADDRESS IS NULL THEN
                                    (
                                            SELECT EMAIL_ADDRESS
                                                    FROM
                                                        (
                                                            SELECT PERSON_UID, EMAIL_ADDRESS, CREATE_DATE_TIME, ROW_NUMBER() OVER (PARTITION BY PERSON_UID ORDER BY CREATE_DATE_TIME DESC) AS rdx
                                                            FROM EAIPERSON_EMAIL_DETAIL
                                                            WHERE PERSON_UID = pmail.PERSON_UID
                                                            AND MAIN_YN = 'N'
                                                        )
                                            WHERE rdx = 1
                                    )
                                    ELSE pmail_y.EMAIL_ADDRESS END AS EMAIL_ADDRESS
                            FROM (
                                    SELECT DISTINCT PERSON_UID
                                    FROM EAIPERSON_EMAIL_DETAIL
                                    ) pmail
                                    LEFT OUTER JOIN(
                                                    SELECT PERSON_UID, EMAIL_ADDRESS
                                                    FROM (
                                                        SELECT PERSON_UID, EMAIL_ADDRESS, CREATE_DATE_TIME, ROW_NUMBER() OVER (PARTITION BY PERSON_UID ORDER BY CREATE_DATE_TIME DESC) AS rdx
                                                        FROM EAIPERSON_EMAIL_DETAIL
                                                        WHERE MAIN_YN = 'Y'
                                                    )
                                                    WHERE rdx = 1
                                                    )pmail_y
                                                 ON pmail.PERSON_UID = pmail_y.PERSON_UID
                        ) pmail
                     ON pmas.PERSON_UID = pmail.PERSON_UID;
                     
                     
-- !!!!최초의 사용자 연동 시  기존 사용자(SSO_ID 존재) INSERT 구문으로 생성!!!!
INSERT INTO BDSUSER(ID, GENERICID, GROUPID, ORIGINALGROUPID, PASSWORD, NAME, EMAIL, HANDPHONE, USERTYPE, KAISTUID, HASHVALUE, CREATORID)
SELECT SSO_UID, SSO_UID, ORGANIZATION_UID, ORGANIZATION_UID,  SSO_UID, KOREAN_NAME, EMAIL_ADDRESS, MOBILE_TELEPHONE_NUMBER, PERSON_TYPE_CODE_UID, KAIST_UID, 1 AS HASHVALUE, 'initialRow' AS CREATORID FROM EAIPERSONVIEW
WHERE SSO_UID IS NOT NULL
AND ORGANIZATION_UID IS NOT NULL;

COMMIT;