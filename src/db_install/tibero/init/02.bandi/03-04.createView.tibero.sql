-- OTP 사용자 정보 연계용 뷰 생성 (더 필요한 정보가 있을 경우 검색 부분에 컬럼을 추가하면 됨)
-- 졸업생, 퇴직자등 로그인을 수행해야 하기 때문에 검색조건에서 FlagUse = 'Y'를 삭제함
CREATE OR REPLACE VIEW USERVIEW
AS
SELECT ID AS USER_ID, NAME AS USER_NM
FROM BDSUSER
ORDER BY NAME;

-- 개발서버에서만 실행
GRANT SELECT ON USERVIEW TO OTP_TBS;

-- 운영서버에서만 실행
GRANT SELECT ON USERVIEW TO OTP;
