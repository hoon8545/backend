-- Configuration
INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('ACCESS_IP_ALLOW_POLICY', 'N', 'IP 접근 제어 정책', 'c90940b1cb4c3f2a8d75323ca32413c9bda3b5da9b9d658783b47cf8cc738c14');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('ALLOW_MULTI_LOGIN', 'X', '중복 로그인 허용', '95d7bbfd70255f82b515adf7f7ab2b1e13efd7db63313bfd0d67657cfd9aba14');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('EXPIRE_MINUTE_AUTHORIZATION_CODE', '10', 'AuthorizationCode 만료시간(분)', '4dbcf4def71e067da72a506d81844863cfd5734da031742d0370c871d0e704bd');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('ID_MIN_LENGTH', '9', '계정 최소 길이', '1099ef196db2542642d8fe2f35a9b7210d4b4b16655ec5ce512fc88553f7dcff');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('ID_TYPE', ' ', '계정 입력 타입', 'd0d1a4f3eaeeee2469949b4bedbcc28ec3d515a0f5727d5f7f34be7ef7353c64');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('MONITORING_TRESHOLD_CPU', '90', 'CPU 사용율 임계치', '566acf3eadd1342a11c0adc10f9cc6afbf7e9466bd7a8b99f4706b003d1cca2d');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('MONITORING_TRESHOLD_DISK', '80', 'DISK 사용율 임계치', 'f0bdadfb01fbb3d61c15a171fa2e9ca023d1d0ea4bfa3a863268bf42f2254292');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('MONITORING_TRESHOLD_DISK_RISK', '91', 'DISK 사용율 임계치(위험)', 'a43daef50933381441cc603da27556d680d1e48be4a4dd11a27e2c3a4e8f85fb');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('MONITORING_TRESHOLD_MEMORY', '90', 'MEMORY 사용율 임계치', 'a53339ee74d942833e4803ac8e44d0063f93413fc705b94aeeb7bf83e9051dbe');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('PASSWORD_MIN_LENGTH', '9', '패스워드 최소 길이', '6f024f378c23d34a911e498e35e25bf378b199678ec6677bfca91f0d48c620e2');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE, FLAGVALID)
VALUES('PASSWORD_SPECIAL_CHARACTERS', '~`!@#$%^&*()-_=+\\|[{]}:,?', '패스워드 입력 가능 특수문자', '1db748566465ad6402cbe9b41b2e1e3c8c89361de860ff2eb6f8884ef8aee560', 'Y');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('PASSWORD_TYPE', 'ANS', '패스워드 입력 타입', '617da53d9b7124b7400c8b83e78b7b8e3996be3c663b0f9293a0517fdfa5b76b');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('POLICY_USER_AUTHORIZATION_PERIOD', '5', '인증 오류 정책 (분)', 'bdf31fb5ff78f19f665766e2f740fba4a8d678bc99c13eff672ff0c0d51d6c73');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('POLICY_USER_AUTHORIZATION_TIMES', '5', '인증 오류 정책 (횟수)', '0d1339caa632af483cf7ad68f0022cbb8165f4ef150a63f399a2e1b7ff8ff634');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('POLICY_USER_PASSWORD_CHANGE_PERIOD', '180', '비밀번호 변경 정책 (일)', '94ac9cd3b54528a8d796e7219d2a4f0daf7887341c1575fd84a01a35fa0bebbc');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('SESSION_EXPIRE_MINUTE_ADMIN', '10', 'Admin 세션 만료시간(분)', '1b58f3d3067d682bac1c3328bdde34ce284f02b02547e54d83885cb2028251b6');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('SESSION_EXPIRE_MINUTE_USER', '10', 'User 세션 만료시간(분)', '7197d1557ba919fd0b02c23633d03aa06aad639af8a70b0b28ac866d10fc06b5');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('SESSION_MAX_COUNT', '1', '중복 세션 최대 허용 수', 'c32455cfb4a11380e58779602bf719300fe6d7e3188a5ff0cbc0c921ec500b3b');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('FILE_PATH', 'c://', '모니터링 대상 경로', '600118c43cecb6b0a15890e194e01dd01b432dc6f8b6afff6b521c3929752832');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('EXPORT_MAX_PAGE', '100', '내보내기 최대 페이지 수', 'ed86d174b4255128da4ce860d9f517bcf1c8d7735f8f673ea4c6bb134ce0dc8f');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('PASSWORD_UPPER_CASE_NUM', '0', '패스워드 최소 포함 대문자 수', 'N/A' );

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('PASSWORD_LOWER_CASE_NUM', '1', '패스워드 최소 포함 소문자 수', 'N/A' );

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('PASSWORD_SPECIAL_CHARACTER_NUM', '0', '패스워드 최소 포함 특수문자 수', 'N/A' );

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('PASSWORD_NUMBER_NUM', '1', '패스워드 최소 포함 숫자 수', 'N/A' );

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('POLICY_OTP_AUTHORIZATION_TIMES', '5', 'OTP 인증 오류 정책(횟수) ', 'N/A' );

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES ('POLICY_USER_PASSWORD_CHANGE_PERIOD_USE', 'Y', '비밀번호 변경 정책(사용여부)', 'N/A');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES ('POLICY_USER_PASSWORD_NOTICE_SETTING', 'AUTO', '알림 설정', 'N/A');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('PASSWORD_NON_REPEATABLE_NUMBER', '3', '패스워드 연속입력 불가 횟수', '1' );

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('PASSWORD_NON_INPUTABLE_SPECIAL_CHARACTERS', '&<>\\()$', '패스워드 입력 불가능 특수문자', '1' );

INSERT INTO BDSCONFIGURATION(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('RECENT_PASSWORD_NON_INPUTABLE_NUMBER', '3', '최근에 사용했던 패스워드 입력 불가 수', '1' );

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('ID_SPECIAL_CHARACTERS', '@.-_', '아이디 입력 가능 특수문자', '1' );

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('OTP_LONG_TERM_NON_USER_DAY', '30', '장기 미사용자 적용기준 (일)', '1');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('OTP_FAIL_COUNT', '10', '연속인증실패 허용횟수', '1');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('ID_MIN_LENGTH_USER', '5', '사용자 계정 최소 길이', '1');

-- ADMIN
INSERT INTO BDSADMIN
(ID, NAME, PASSWORD, HANDPHONE, EMAIL,
FLAGUSE, ROLE, ADMINGRANTFLAG, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, PASSWORDCHANGEDAT, HASHVALUE, MODULE, USERID)
VALUES( 'admin', '관리자테스트', '0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c', NULL, 'island0413@banidsnc.com',
'Y' , 'S', 'M', 'admin', SYSDATE, 'admin', SYSDATE, TO_DATE('19700101000000', 'YYYYMMDDHH24MISS'), 'b60a0764843d621435f0517e010eee61282f30e9c56d946900a81967c98871d3', 62, 'admin');

-- SCHEDULER
INSERT INTO BDSSCHEDULER(FLAGUSE, SCHEDULERNAME, EXPRESS, BODY, FLAGMULTI, MODULE)
VALUES('Y', 'MonitorSystemResource', '0 0/1 * * * ?', '시스템 모니터링', 'N', '1');

-- BDSADMINIP
insert into BDSADMINIP ( IP, HASHVALUE ) values ( '127.0.0.1', '12ca17b49af2289436f303e0166030a21e525d266e209267433801a8fd4071a0');

-- BDSWHITEIP
INSERT INTO BDSWHITEIP
(OID, IP, DESCRIPTION, FLAGENABLEDELETE, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGALLOW, HASHVALUE, TARGETOBJECTTYPE, TARGETOBJECTID)
VALUES('1MXLYGHv01C', '127.0.0.1', 'ADMIN:127.0.0.1', 'N', 'admin', SYSDATE, NULL, SYSDATE, 'Y', '77731827bcc96c51d2c2ad915aaff7e72b389231ada1a04bb349038252436ba5', 'ADMIN', '127.0.0.1');

--------------------------------------------------------------------------------------
-- IM --------------------------------------------------------------------------------
--------------------------------------------------------------------------------------

-- BDSGROUP
-- root
INSERT INTO BDSGROUP
(ID, Name, Description, SortOrder, ParentID, ORIGINALPARENTID,FullPathIndex, SubLastIndex, Status, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGSYNC, TYPECODE)
VALUES('G000', '카이스트', '', '000', 'N/A','N/A', '#0', 0, 'A', 'admin', SYSDATE, 'admin', SYSDATE, 'N', 'org');

INSERT INTO BDSGROUP
(ID, Name, Description, SortOrder, ParentID, ORIGINALPARENTID,FullPathIndex, SubLastIndex, Status, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGSYNC, TYPECODE)
VALUES('U000', '미 정의 부서', '', '99999', 'G000','G000', '#000', -1, 'A', 'admin', SYSDATE, 'admin', SYSDATE, 'N', 'org');

INSERT INTO BDSSCHEDULER (FLAGUSE, SCHEDULERNAME, EXPRESS, BODY, FLAGMULTI, MODULE)
VALUES('Y', 'SyncOrganization', '0 0 1 * * ?', '조직도 연동', 'Y', '4');

INSERT INTO BDSSCHEDULER (FLAGUSE, SCHEDULERNAME, EXPRESS, BODY, FLAGMULTI, MODULE)
VALUES('N', 'CheckTemporaryUser', '0 0 1 * * ?', '임시사용자 Check', 'Y', '4');

INSERT INTO BDSSCHEDULER
(FLAGUSE, SCHEDULERNAME, EXPRESS, BODY, FLAGMULTI, MODULE)
VALUES('N', 'SyncCommonCode', '0 0 2 * * ?', '공통코드 연동', 'Y', '4');

INSERT INTO BDSSCHEDULER (FLAGUSE, SCHEDULERNAME, EXPRESS, BODY, FLAGMULTI, MODULE)
VALUES('N', 'CheckDelegateRole', '0 0 1 * * ?', '위임 롤 생성 및 삭제', 'Y', '4');

INSERT INTO BDSSCHEDULER (FLAGUSE, SCHEDULERNAME, EXPRESS, BODY, FLAGMULTI, MODULE)
VALUES('N', 'SetInitClient', '0 0 1 * * ?', '클라이언트 초기 세팅', 'Y', '4');

INSERT INTO BDSSCHEDULER (FLAGUSE, SCHEDULERNAME, EXPRESS, BODY, FLAGMULTI, MODULE)
VALUES('N', 'SyncClientResource', '0 0 3 * * ?', '리소스 연동', 'Y', '4');

-- MSSQL, TIBERO 에서만 사용.
INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('CONNECTED_CLIENTS', ';', '접속중인 Client DB 계정', 'N/A');

-- BDSCODE
INSERT INTO BDSCODE (CODEID, NAME, DESCRIPTION) VALUES('title', '직위', NULL);
INSERT INTO BDSCODE (CODEID, NAME, DESCRIPTION) VALUES('position', '직책', NULL);
INSERT INTO BDSCODE (CODEID, NAME, DESCRIPTION) VALUES('rank', '직급', NULL);

-- BDSCODEDETAIL
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegZmlm086', '1', '사원', 'title', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegaDLY08J', '2', '주임', 'title', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegb4V509n', '3', '대리', 'title', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegb5aV09q', '4', '과장', 'title', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegb6SV09t', '5', '차장', 'title', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegb7EP09w', '6', '부장', 'title', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegb8Hn09_', '7', '이사', 'title', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegb9z10A0', '8', '상무', 'title', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegbB6f0A3', '9', '전무', 'title', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegbC6c0A7', '10', '부사장', 'title', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegbD230AA', '11', '사장', 'title', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegbE5d0AH', '12', '부회장', 'title', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegbEqc0AM', '13', '회장', 'title', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegbQkL0Ac', '1', '파트장', 'position', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegbRi90Af', '2', '팀장', 'position', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegbSXt0Am', '3', '실장', 'position', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegbTLN0Ar', '4', '본부장', 'position', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegbUUA0Au', '5', '사업부장', 'position', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegbqDP0B6', '6', 'CMO', 'position', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegbrAo0B9', '7', 'CCO', 'position', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegbsH40BC', '8', 'CFO', 'position', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegbth30BF', '9', 'COO', 'position', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegbur60BJ', '10', 'CTO', 'position', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegbvjY0BL', '11', 'CEO', 'position', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegc0tt0Bm', '1', '1년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegc1iH0Bq', '2', '2년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegc2Pg0Bs', '3', '3년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegc35U0Bv', '4', '4년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegc3vS0Bz', '5', '5년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegc53E0C0', '6', '6년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegc5xV0C3', '7', '7년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegc6e70C5', '8', '8년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegc7MP0C8', '9', '9년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegc8070CB', '10', '10년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegc9840CK', '11', '11년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegc9te0CO', '12', '12년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegcAh30CQ', '13', '13년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegcBSe0CT', '14', '14년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegcCU90CW', '15', '15년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegcDJe0Ca', '16', '16년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegcEEN0Cd', '17', '17년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegcFHn0Cf', '18', '18년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegcFy90Ci', '19', '19년', 'rank', 'N');
INSERT INTO BDSCODEDETAIL (OID, CODE, NAME, CODEID, FLAGSYNC) VALUES('aMegcGlE0Cl', '20', '20년', 'rank', 'N');


INSERT INTO BDSRESOURCE
(OID, CLIENTOID, NAME, PARENTOID, DESCRIPTION, SORTORDER, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, SUBLASTINDEX, FULLPATHINDEX, FLAGUSE)
VALUES('R_ROOT', 'N/A', 'RESOURCE ROOT', 'N/A', 'RESOURCE ROOT', 0, 'admin', SYSDATE, 'admin', SYSDATE, -1, '#0', 'Y');


-- 전사 공통 롤.(카이스트 모든 사용자를 대상)
INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('ALL', 'G000', '카이스트 전체공통 롤', 'A',  NULL, 'Y', '카이스트 전체 공통롤', 'admin', SYSDATE, NULL, NULL, 'N');

-- 최상위 부서의 공통 롤.(단위부서)
INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('G000', 'G000', '카이스트'' 부서공통 롤', 'D',  NULL, 'Y', '카이스트 부서 공통롤', 'admin', SYSDATE, NULL, NULL, 'N');

-- 최상위 부서장 롤
INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('G000_M', 'G000', '카이스트'' 부서장 롤', 'M',  NULL, 'Y', '카이스트 부서장롤', 'admin', SYSDATE, NULL,NULL, 'N');

-- 마종의 부서의 공통 롤.(단위부서)
INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('U000', 'U000', '미정의'' 부서공통 롤', 'D',  NULL, 'Y', '미정의 부서 공통롤', 'admin', SYSDATE, NULL, NULL, 'N');

-- 미정의 부서장 롤
INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('U000_M', 'U000', '미정의'' 부서장 롤', 'M',  NULL, 'Y', '미정의 부서장 롤', 'admin', SYSDATE, NULL,NULL, 'N');

-- 사용자유형 롤
INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('P', 'G000', '교수 롤', 'U',  'P', 'Y', '교수 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('E', 'G000', '직원 롤', 'U',  'E', 'Y', '직원 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('U', 'G000', '기간제직원 롤', 'U',  'U', 'Y', '기간제직원 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('H', 'G000', '학부모 롤', 'U',  'H', 'Y', '학부모 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('R', 'G000', '퇴직자 롤', 'U',  'R', 'Y', '퇴직자 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('S', 'G000', '재학생 롤', 'U',  'S', 'Y', '재학생 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('A', 'G000', '졸업생 롤', 'U',  'A', 'Y', '졸업생 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('G', 'G000', '학생합격자 롤', 'U',  'G', 'Y', '학생합격자 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('C', 'G000', '잠재입학생 롤', 'U',  'C', 'Y', '잠재입학생 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('L', 'G000', '입학지원자 롤', 'U',  'L', 'Y', '입학지원자 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('T', 'G000', '외부임시접속 롤', 'U',  'T', 'Y', '외부임시접속 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('V', 'G000', '협력업체직원 롤', 'U',  'V', 'Y', '협력업체직원 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('O', 'G000', '외부인 롤', 'U',  'O', 'Y', '외부인 롤', 'admin', SYSDATE, NULL, NULL, 'N');

-- 본인인증 정책관리
INSERT INTO BDSIDENTITYAUTHENTICATION
(SERVICETYPE, PERSONAL_INFORMATION_AUTHENTICATION_SERVICE, MOBILE_PHONE_AUTHENTICATION_SERVICE, MOBILE_PHONE, MAIL, FINDUID)
VALUES ('signup', 'enable', 'enable', 'enable', 'enable', 'enable');

INSERT INTO BDSIDENTITYAUTHENTICATION
(SERVICETYPE, PERSONAL_INFORMATION_AUTHENTICATION_SERVICE, MOBILE_PHONE_AUTHENTICATION_SERVICE, MOBILE_PHONE, MAIL, FINDUID, RESULT_SCREEN, RESULT_MAIL)
VALUES ('findid', 'enable', 'enable', 'enable', 'enable', 'enable', 'enable', 'disable');

INSERT INTO BDSIDENTITYAUTHENTICATION
(SERVICETYPE, PERSONAL_INFORMATION_AUTHENTICATION_SERVICE, MOBILE_PHONE_AUTHENTICATION_SERVICE, MOBILE_PHONE, MAIL, FINDUID)
VALUES ('findpw', 'enable', 'enable', 'enable', 'enable', 'enable');

-- SSOTYPE에 따른 initialRow
--APP 단일인증
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('A0000592','행정교육시스템모바일','a111111', '127.0.0.1', '1', 'N/A','N/A', 'Y','N','N','A','행정교육시스템모바일','kaist_uid/ku_ebs_pid/',NULL,'Y',NULL, TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'), NULL,'capdata');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('A0000634', '학습관리시스템', 'a222222', '127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N', 'A', 'KLMS Mobile APP', 'kaist_uid/kaist_suid/ku_kname/displayname/sn/givenname/c/mail/ku_ch_mail/telephoneNumber/postalCode/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/employeeType/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_outer_start_date/ku_outer_end_date/acad_ebs_org_id/uid/', NULL, 'Y', NULL, TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS')  , NULL, 'roro');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('A0000472', 'TESTAPPlication', 'a333333', '127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N' , 'A', 'test', 'kaist_uid',NULL , 'N',NULL, TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'), TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),'dmsong2007');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('A0000499', 'KREP(APP)', 'a444444','127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N', 'A'   , '전자식 복무관리', 'kaist_uid/kaist_suid/ku_kname/displayname/givenname/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/employeeType/uid/',NULL, 'Y',NULL, TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),NULL,'cwkim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('A0000710', '전자도서관', 'a555555' , '127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N', 'A', 'KAIST 전자도서관 서비스'  , 'kaist_uid/ku_employee_number/ku_std_no/',NULL , 'Y',NULL, TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),NULL, 'kamamovie');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('A0000442', 'IRT', 'a666666', '127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N', 'A'    , '국제협력DB', 'kaist_uid/displayname/sn/givenname/mail/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/employeeType/',NULL, 'Y',NULL, TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),NULL, 'topcheol');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('A0000450', 'APP테스트', 'a777777'    ,'127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N', 'A', 'test', 'kaist_uid/displayname/sn/givenname/mail/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/employeeType/',NULL, 'N',NULL, TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'), TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'), 'dmsong2007');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID)  VALUES ('A0000484', 'APP 단일 등록 테스트', 'a888888'    , '127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N', 'A', 'XSS 필터 수정 완료 테스트', 'kaist_uid',NULL, 'N'  ,NULL, TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'), TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),'dmsong2007');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('A0000763', 'KAIREN', 'a999999'    , '127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N', 'A', '학내 안전사고 제보 및 상황 공유', 'kaist_uid/ku_kname/displayname/mobile/ku_kaist_org_id/',NULL, 'Y',NULL   , TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),NULL,'syryu');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('A0000080', 'TestApplication', 'a1111110', '127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N'    , 'A', 'test', 'kaist_uid/kaist_suid/ku_kname/displayname/sn/givenname/ku_born_date/c/ku_sex/mail/ku_ch_mail/mobile/telephoneNumber/facsimiletelephonenumber/postalCode/ku_postaladdress1/ku_postaladdress2/ku_departmentcode/title/ku_acad_prog_code/employeeType/ku_stdnt_type_class/' ,NULL, 'Y',NULL , TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),NULL, 'dmsong');


-- WEB 단일인증
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('146', 'PRIMO',     'b11111', '127.0.0.1', '1',     'https://library.kaist.ac.kr/sso/prismo.do',    'https://apac-pds.hosted.exlibrisgroup.com',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/',  NULL,   'Y',    NULL,   NULL,   NULL,   'kamamovie');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('166', '기술경영학과_개발',     'b22222', '127.0.0.1', '1',     '/sso/login_process.php',   'https://btm.fantaon.com',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/mail/ku_employee_number/ku_std_no/ku_departmentcode/ku_departmentname_eng/ou/ku_user_status_kor/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   'choijh84');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('155', 'KREP(WEB)',     'b33333', '127.0.0.1', '1',     'https://krep.kaist.ac.kr/krep/krep_login.do',  'https://krep.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/kaist_suid/ku_kname/displayname/sn/givenname/ku_born_date/ku_sex/mail/ku_ch_mail/mobile/telephoneNumber/facsimiletelephonenumber/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_departmentcode/ku_departmentname_eng/ou/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_kaist_uid/ku_advr_ebs_person_id/ku_advr_name_ac/ku_prog_start_date/',   NULL,   'Y',    NULL,   NULL,   NULL,   'cwkim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('170', 'LIBM_DEV',      'b44444', '127.0.0.1', '1',     '/sso/mlibrary.do',     'https://m.emarket.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   'kamamovie');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('180', 'IDFM_DEV',      'b55555', '127.0.0.1', '1',     '/idfms/idfms_login.do',    'http://itinfra16.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/employeeType/uid/',  NULL,   'Y',    NULL,   NULL,   NULL,   'cwkim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('181', 'IDFM',      'b66666', '127.0.0.1', '1',     '/idfms/idfms_login.do',    'https://idfms.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/employeeType/uid/',  NULL,   'Y',    NULL,   NULL,   NULL,   'cwkim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('193', '단일인증 3.0 테스트', 'b77777', '127.0.0.1', '1',  '/requestsoap.jsp',     'http://simple3.kaist.ac.kr:8888',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/kaist_suid/ku_kname/displayname/sn/givenname/ku_born_date/ku_ssn/c/ku_sex/mail/ku_ch_mail/mobile/telephoneNumber/facsimiletelephonenumber/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_kaist_uid/ku_advr_ebs_person_id/ku_advr_name/ku_advr_name_ac/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/ku_register_date/ku_home_phone/acad_ebs_org_id/uid/',    NULL,   'Y',    NULL,   NULL,   NULL,   'cwkim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('171', 'LIBM',      'b88888', '127.0.0.1', '1',     'https://library.kaist.ac.kr/sso/mlibrary.do',  'https://m.library.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   'kamamovie');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('194', 'HRS',   'b99999', '127.0.0.1', '1',     '/auth/auth.php',   'https://humanrights.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_ch_mail/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentname_eng/ou/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/employeeType/',    NULL,   'Y',    NULL,   NULL,   NULL,   '00077365');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('195', 'CSRMS',     'b111110', '127.0.0.1', '1',    '/auth/auth.php',   'https://csrms.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_sex/ku_ch_mail/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/ku_acad_prog_code/ku_acad_prog/employeeType/',   NULL,   'Y',    NULL,   NULL,   NULL,   'stim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('210', 'NOJO',      'b122221', '127.0.0.1', '1',    '/auth/auth.php',   'https://nojo.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_born_date/mail/ku_ch_mail/mobile/postalCode/ku_postaladdress1/ku_postaladdress2/ku_employee_number/ku_kaist_org_id/ku_departmentname_eng/ou/ku_user_status_kor/employeeType/',   NULL,   'Y',    NULL,   NULL,   NULL,   'ejlee');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('211', 'FRES',      'b133332', '127.0.0.1', '1',    '/auth/auth.php',   'https://freshman.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentname_eng/ou/ku_user_status_kor/ku_psft_user_status_kor/employeeType/',  NULL,   'Y',    NULL,   NULL,   NULL,   'souvenir');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('212', 'KADM41',    'b144443', '127.0.0.1', '1',    '/Auth/Login',  'https://kadm.kaist.ac.kr:8041',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/telephoneNumber/ku_employee_number/ku_acad_org/ku_acad_name/ku_campus/ku_departmentcode/ou/ku_title_kor/employeeType/uid/',     NULL,   'Y',    NULL,   NULL,   NULL,   'londo1');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('213', 'KADM51',    'b155554', '127.0.0.1', '1',    '/Auth/Login',  'https://kadm.kaist.ac.kr:8051',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/telephoneNumber/ku_employee_number/ku_acad_org/ku_acad_name/ku_campus/ku_departmentcode/ou/ku_title_kor/employeeType/uid/',     NULL,   'Y',    NULL,   NULL,   NULL,   'londo1');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('214', 'KADM42',    'b166665', '127.0.0.1', '1',    '/Auth/Login',  'https://kadm.kaist.ac.kr:8042',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/telephoneNumber/ku_employee_number/ku_acad_org/ku_acad_name/ku_campus/ku_departmentcode/ou/ku_title_kor/employeeType/uid/',     NULL,   'Y',    NULL,   NULL,   NULL,   'londo1');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('3060', '관리부',      'b177776', '127.0.0.1', '1',    '/index.jsp',   'https://www.test.com',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/displayname/sn/ku_born_date/ku_ssn/',    NULL,   'Y',    NULL,   NULL,   NULL,   'cwkim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('3061', '경영관리팀',    'b188887', '127.0.0.1', '1',    '/test.jsp',    'https://www.k_test.co.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/displayname/givenname/ku_ssn/c/mobile/postalCode/ku_ebs_pid/ku_acad_org/acad_ebs_org_name_eng/acad_ebs_org_name_kor/',   NULL,   'Y',    NULL,   NULL,   NULL,   'cwkim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('200', '도서관(WEB)',      'b199998', '127.0.0.1', '1',    '/index_1.jsp',     'http://target.kaist.ac.kr:8080',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/displayname/ku_born_date/ku_sex/',   NULL,   'Y',    NULL,   NULL,   NULL,   'aisideru34');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('34', '찬우테스트',      'b211109', '127.0.0.1', '1',    '/index_1.jsp',     'http://cwkim.kaist.ac.kr:8080',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/kaist_suid/ku_kname/displayname/sn/givenname/ku_born_date/ku_ssn/c/ku_sex/mail/ku_ch_mail/mobile/telephoneNumber/facsimiletelephonenumber/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_kaist_uid/ku_advr_ebs_person_id/ku_advr_name/ku_advr_name_ac/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/ku_register_date/ku_home_phone/acad_ebs_org_id/uid/',    NULL,   'Y',    NULL,   NULL,   NULL,   'cwkim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('32', '안전팀홈페이지',    'b222220', '127.0.0.1', '1',    '/default2.aspx',   'https://safety.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/displayname/sn/givenname/ku_born_date/c/ku_sex/mail/ku_ch_mail/mobile/telephoneNumber/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_ebs_person_id/ku_advr_name/ku_advr_name_ac/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/ku_register_date/ku_home_phone/',  NULL,   'Y',    NULL,   NULL,   NULL,   'hwang_won');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('90', '전기및전자공학과 홈페이지', 'b233331',   '127.0.0.1', '1',   '/login/responseLogin.do',  'https://www.ee.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/mail/mobile/telephoneNumber/ku_employee_number/ku_std_no/ku_acad_org/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/ku_user_status_kor/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/employeeType/ku_stdnt_type_id/ku_stdnt_type_class/uid/',     NULL,   'Y',    NULL,   NULL,   NULL,   'gyyun');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('91', '카이펀',    'b244442', '127.0.0.1', '1',    '/kaist_login.php',     'https://kaifun.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_sex/mail/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_departmentcode/ku_departmentname_eng/ou/',    NULL,   'Y',    NULL,   NULL,   NULL,   'baramdonge');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('100', 'Startup',   'b255553', '127.0.0.1', '1',    '/_prog/_member/login_process.php',     'https://startup.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_name/displayname/',   NULL,   'Y',    NULL,   NULL,   NULL,   'suky');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('80', '사이버 카이스트',   'b266664', '127.0.0.1', '1',    '/login/index.php',     'http://cyber.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/displayname/sn/givenname/ku_born_date/mail/ku_ch_mail/mobile/ku_employee_number/ku_std_no/employeeType/ku_home_phone/',  NULL,   'Y',    NULL,   NULL,   NULL,   'ctj8210');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('81', 'KLMS교수학습혁신센터', 'b277775', '127.0.0.1', '1',  '/login/index.php',     'https://klms.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/displayname/sn/givenname/ku_born_date/mail/ku_ch_mail/mobile/ku_employee_number/ku_std_no/employeeType/ku_home_phone/',  NULL,   'Y',    NULL,   NULL,   NULL,   'ctj8210');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('130', '전산학과_개발',   'b288886', '127.0.0.1', '1',    '/sso/rslogin',     'https://csd1.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/mail/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_departmentcode/ku_departmentname_eng/ou/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_stdnt_type_class/ku_category_id/ku_advr_name/ku_advr_name_ac/ku_ebs_end_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/acad_ebs_org_id/uid/',     NULL,   'Y',    NULL,   NULL,   NULL,   'stim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('131', '전산학과_운영',   'b299997', '127.0.0.1', '1',    '/sso/rslogin',     'https://cs.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/mail/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_departmentcode/ku_departmentname_eng/ou/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_name/ku_advr_name_ac/ku_ebs_end_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/acad_ebs_org_id/uid/',    NULL,   'Y',    NULL,   NULL,   NULL,   'stim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('144', 'LIB',   'b311108', '127.0.0.1', '1',    '/sso/library.do',  'https://library.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   'kamamovie');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('72', '교수학습혁신센터 홈페이지', 'b322219', '127.0.0.1', '1',     '/login/index.php',     'http://celt.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/displayname/sn/givenname/ku_born_date/mail/ku_ch_mail/mobile/ku_employee_number/ku_std_no/employeeType/ku_home_phone/',  NULL,   'Y',    NULL,   NULL,   NULL,   'ctj8210');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('145', 'ILIB',      'b333330', '127.0.0.1', '1',    'https://library.kaist.ac.kr/sso/ilibrary.do',  'https://ilibrary.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   'kamamovie');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('150', 'LIIB_DEV',      'b344441', '127.0.0.1', '1',    'https://emarket.kaist.ac.kr/sso/library',  'https://emarket.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   'kamamovie');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('151', 'ILIB_DEV',      'b355552', '127.0.0.1', '1',    'https://emarket.kaist.ac.kr/sso/ilibrary.do',  'http://emarket.kaist.ac.kr:8080',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   'kamamovie');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('152', 'PRIMO_DEV',     'b366663', '127.0.0.1', '1',    'https://emarket.kaist.ac.kr/sso/primo',    'http://stg-pds.kesli.or.kr/pds',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   'kamamovie');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('161', 'CLI',   'b377774', '127.0.0.1', '1',    'https://clinic.kaist.ac.kr/process.php',   'https://clinic.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_ssn/ku_sex/mail/ku_ch_mail/mobile/telephoneNumber/postalCode/ku_postaladdress1/ku_postaladdress2/ku_employee_number/ku_std_no/ku_user_status_kor/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/employeeType/ku_ebs_end_date/ku_prog_end_date/',     NULL,   'Y',    NULL,   NULL,   NULL,   'mustbeno1');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('611', '전기및전자공학과홈페이지', 'b388885', '127.0.0.1', '1',     '/loginproc',   'https://ee.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/givenname/mail/mobile/telephoneNumber/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ou/ku_user_status_kor/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/employeeType/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/uid/',  NULL,   'Y',    NULL,   NULL,   NULL,   'wingleta');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('140', '문화기술대학원 성과관리시스템', 'b399996', '127.0.0.1', '1',  '/iam_sso/sso.php',     'https://ctpms.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/employeeType/uid/',    NULL,   'Y',    NULL,   NULL,   NULL,   'tjchoi');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('142', '문화기술대학원 졸업사정시스템', 'b411107', '127.0.0.1', '1',  '/ctreq/member/signin/',    'https://ctreq.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ou/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/acad_ebs_org_id/uid/',    NULL,   'Y',    NULL,   NULL,   NULL,   'kaist21091');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('132', 'OTL', 'b422218', '127.0.0.1', '1',  '/accounts/auth',   'https://otl.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/sn/givenname/mail/ku_std_no/ku_kaist_org_id/ou/uid/',    NULL,   'Y',    NULL,   NULL,   NULL,   'alperatz');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('148', 'SCS', 'b433329', '127.0.0.1', '1',  '/login/process',   'https://scspace.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/ku_employee_number/ku_std_no/',     NULL,   'Y',    NULL,   NULL,   NULL,   'yujingaya');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('221', 'ACDM', 'b444440', '127.0.0.1', '1',     '/auth/auth.php',   'https://academy.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/ku_employee_number/ku_acad_org/ku_acad_name/ku_campus/ku_kaist_org_id/ku_departmentname_eng/ou/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   'tjchoi');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('215', 'KADM52', 'b455551', '127.0.0.1', '1',   '/Auth/Login',  'https://kadm.kaist.ac.kr:8052',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/telephoneNumber/ku_employee_number/ku_acad_org/ku_acad_name/ku_campus/ku_departmentcode/ou/ku_title_kor/employeeType/uid/',     NULL,   'Y',    NULL,   NULL,   NULL,   'londo1');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('461', 'CSYEAR', 'b466662', '127.0.0.1', '1',   '/auth/auth.php',   'https://csyear.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/givenname/mail/mobile/telephoneNumber/ku_employee_number/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/ku_psft_user_status/ku_ebs_start_date/',   NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'stim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('491', 'INFO', 'b477773', '127.0.0.1', '1',     '/_prog/_member/sso_login.php',     'https://info.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/mail/telephoneNumber/ku_departmentname_eng/ou/ku_grade_level/ku_grade_level_kor/ku_position/ku_position_kor/employeeType/',    NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'ilips');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('500', 'IO-국제협력처', 'b488884', '127.0.0.1', '1',     '/login/loginAuth.do',  'https://io.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/ku_employee_number/ku_std_no/uid/',    NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'clampee');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('741', '원규시스템개발', 'b499995', '127.0.0.1', '1',  '/lmxsrv/member/ssoLogin.do',   'https://ruledev.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/givenname/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/uid/',  NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'magix');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('580', 'KDISK', 'b511106', '127.0.0.1', '1',    '/sso.php',     'https://kdisk.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/',    NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'capdata');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('560', 'dev_SCS', 'b522217', '127.0.0.1', '1',  '/login/process',   'https://scspacedev.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/ku_employee_number/ku_std_no/',     NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'yujingaya');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('681', '교수협의회', 'b533328', '127.0.0.1', '1',    '/auth/auth.php',   'https://profasso.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/ku_user_status_kor/employeeType/',     NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'geehyuklee');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('720', '윤리및안전개발서버', 'b544439',  '127.0.0.1', '1',   '/auth/auth.php',   'https://eethics-dev.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ou/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/uid/',    NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'ninjatori');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('740', '원규시스템운영', 'b555550',    '127.0.0.1', '1',   '/lmxsrv/member/ssoLogin.do',   'https://rule.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/givenname/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/uid/',  NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'magix');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('770', 'KISA', 'b566661',   '127.0.0.1', '1',   '/ksso.php',    'https://kisa.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_born_date/c/ku_sex/mail/ku_ch_mail/mobile/ku_std_no/ku_campus/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_prog_start_date/ku_prog_end_date/uid/',     NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'seeeelup');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('147', 'GSA', 'b577772',    '127.0.0.1', '1',   '/portal',  'https://gsa.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_born_date/c/ku_sex/mail/mobile/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_kaist_org_id/ou/title/ku_title_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_prog_start_date/ku_prog_end_date/uid/',  NULL,   'Y',    NULL,   NULL,   NULL,   'combacsa');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('141', 'VOTE', 'b588883',   '127.0.0.1', '1',   '/member/portal',   'https://vote.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/c/ku_sex/mail/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_kaist_org_id/title/ku_title_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_prog_start_date/ku_prog_end_date/',    NULL,   'Y',    NULL,   NULL,   NULL,   'combacsa');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('594', '과학영재입문사이트',     'b599994',  '127.0.0.1', '1',   '/auth/auth.php',   'https://preurp.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/employeeType/',   NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'bumjinee');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('462', 'StarLib운영', 'b611105',  '127.0.0.1', '1',   'https://library.kaist.ac.kr/sso/bsl.do',   'https://starlibrary.org',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/',  NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'kamamovie');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('620', 'K-school 홈페이지',     'b622216',  '127.0.0.1', '1',   '/Login/SSOlogin',  'https://kschool.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_born_date/ku_sex/mail/ku_ch_mail/mobile/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_departmentcode/ku_departmentname_eng/ou/ku_grade_level_kor/ku_person_type_kor/ku_user_status_kor/ku_position_kor/ku_psft_user_status_kor/ku_acad_prog/ku_acad_prog_eng/ku_ebs_start_date/ku_ebs_end_date/acad_ebs_org_name_eng/acad_ebs_org_name_kor/',  NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'rmr0322');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('725', '기록포탈',      'b633327', '127.0.0.1', '1',    '/ssoLogin.do',     'https://archives.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/telephoneNumber/ku_acad_org/ku_kaist_org_id/acad_ebs_org_id/uid/',  NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'kcleeb');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('726', '포토갤러리',     'b644438', '127.0.0.1', '1',    '/ssoLogin.do',     'https://photo.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/telephoneNumber/ku_acad_org/ku_kaist_org_id/acad_ebs_org_id/uid/',  NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'kcleeb');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('444', 'KMIS',      'b655549', '127.0.0.1', '1',    '/auth/index.jsp',  'https://kmis.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_ebs_pid/ku_departmentcode/ku_departmentname_eng/ou/employeeType/',   NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'yoonggang');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('143', 'DKMIS',     'b666660', '127.0.0.1', '1',    '/auth/index.jsp',  'https://dkmis.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_ebs_pid/ku_departmentcode/ku_departmentname_eng/ou/employeeType/uid/',   NULL,   'Y',    NULL,   NULL,   NULL,   'yoonggang');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('443', 'SPARCS',    'b677771', '127.0.0.1', '1',    '/account/callback',    'https://sparcssso.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_born_date/ku_sex/mail/ku_std_no/ku_kaist_org_id/ku_person_type/ku_person_type_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/employeeType/',     NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'hscho122');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('724', '사료관리시스템',   'b688882', '127.0.0.1', '1',    '/ssoLogin.do',     'https://ams.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/telephoneNumber/ku_acad_org/ku_kaist_org_id/acad_ebs_org_id/uid/',  NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'kcleeb');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('727', 'FIMS_발전기금관리',   'b699993',  '127.0.0.1', '1',   '/login_proc.php',  'https://foundation.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/ku_born_date/ku_ebs_pid/ku_employee_number/ku_kaist_org_id/',   NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'koreaks');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('750', 'ICT 개발',    'b711104', '127.0.0.1', '1',    '/webroot/sso/process.php',     'https://testict.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/ou/uid/',   NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'choichoijae');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('761', 'RD 운영',     'b722215', '127.0.0.1', '1',    '/webroot/sso/process.php',     'https://rd.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/ou/uid/',   NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'choichoijae');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('773', '클라우드포털',    'b733326', '127.0.0.1', '1',    '/auth/signup/internal1',   'https://kcloud.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_born_date/ku_sex/mail/ku_ch_mail/mobile/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_kaist_uid/ku_advr_ebs_person_id/ku_advr_name/ku_advr_name_ac/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/ku_home_phone/acad_ebs_org_id/uid/acad_ebs_org_name_eng/acad_ebs_org_name_kor///',  NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'stim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('112', 'KDS',   'b744437', '127.0.0.1', '1',    '/login/loginAuth.do',  'https://kds.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/c/mail/ku_ch_mail/mobile/telephoneNumber/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog_eng/employeeType/ku_stdnt_type_id/ku_stdnt_type_class/ku_prog_start_date/acad_ebs_org_id/',     NULL,   'Y',    NULL,   NULL,   NULL,   'kychung');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('680', '오너코드시스템',   'b755548', '127.0.0.1', '1',    '/session/login_callback',  'https://soc.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/c/mail/ku_std_no/ku_acad_name/ku_title_kor/ku_user_status_kor/ku_psft_user_status_kor/',   NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'stim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('460', 'CT',    'b766659',  '127.0.0.1', '1',   '/auth/auth.php',   'https://ct.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_employee_number/ku_std_no/ku_acad_org/ku_departmentcode/ku_departmentname_eng/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/employeeType/uid/',     NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'tjchoi');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('490', 'EETHICS',   'b777770',  '127.0.0.1', '1',   '/auth/auth.php',   'https://eethics.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/employeeType/ku_stdnt_type_id/ku_prog_end_date/acad_ebs_org_id/uid/',   NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'kim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('721', '출입권한자동화시스템_개발',     'b788881',  '127.0.0.1', '1',   '/AccessAuth/login.do',     'http://accessdemo.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/ku_employee_number/ku_std_no/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog/ku_acad_prog_eng/uid/',    NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'behappy1');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('722', '출입권한자동화시스템_운영',     'b799992',  '127.0.0.1', '1',   '/AccessAuth/login.do',     'https://access.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/ku_employee_number/ku_std_no/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog/ku_acad_prog_eng/uid/',     NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'behappy1');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('771', '장비예약관리시스템',     'b811103',  '127.0.0.1', '1',   '/auth/auth.php',   'https://cbeuser.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ou/ku_psft_user_status/ku_psft_user_status_kor/employeeType/ku_advr_kaist_uid/ku_advr_ebs_person_id/ku_advr_name_ac/uid/acad_ebs_org_name_eng/acad_ebs_org_name_kor/',    NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'moonyh');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('512', 'CUop프로그램',      'b822214', '127.0.0.1', '1',    '/liveproc/member/login.php﻿',  'https://cuop.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_sex/mail/mobile/ku_std_no/ku_acad_org/ku_acad_name/',    NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'kaiser0416');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('552', '테스트',   'b833325', '127.0.0.1', '1',    '/abcd/auth.jsp',   'http://143.248.201.16:8088',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/givenname/ku_born_date/mail/mobile/ku_ebs_pid/ku_employee_number/ku_psft_user_status/ku_psft_user_status_kor/',     NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'ejcho');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('660', '이륜차시스템',    'b844436', '127.0.0.1', '1',    '/member/SSOlogin.do',  'https://kbms.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/ku_born_date/ku_sex/mail/mobile/postalCode/ku_postaladdress1/ku_postaladdress2/ku_employee_number/ku_std_no/ku_acad_name/ou/ku_grade_level_kor/employeeType/',  NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'startac3961');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('765', '글쓰기센터',     'b855547', '127.0.0.1', '1',    '/auth/auth.php',   'https://writing.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/ku_ch_mail/ku_employee_number/ku_std_no/ou/employeeType/uid/',     NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'ing3537');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('766', '독서마일리지클럽',      'b866658', '127.0.0.1', '1',    '/auth/auth.php',   'https://bookclub.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/ku_ch_mail/ku_std_no/ou/employeeType/uid/acad_ebs_org_name_kor/',  NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'ing3537');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('784', '인사서비스개발',   'b877769', '127.0.0.1', '1',    '/kaist_human_resource/#!/Mainkaist_human_resourcePage',    'https://khrdev.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/givenname/ku_ebs_pid/ku_employee_number/ku_departmentname_eng/ou/title/ku_title_kor/',  NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'jhj75');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('769', '정보보호대학원홈페이지',   'b888880',  '127.0.0.1', '1',   '/sso/rslogin',     'https://gsis.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/mail/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_departmentcode/ku_departmentname_eng/ou/ku_person_type/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/acad_ebs_org_id/uid/acad_ebs_org_name_eng/acad_ebs_org_name_kor/',     NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'jisun813');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('498', '학업진로상담실',   'b899991', '127.0.0.1', '1',    '/auth/index.php',  'https://acc.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/ku_std_no/ku_departmentcode/ku_departmentname_eng/ou/ku_psft_user_status/ku_psft_user_status_kor/employeeType/uid/',    NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'ahsa89');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('515', 'BTM',   'b911102', '127.0.0.1', '1',    '/wp-content/themes/enfold-child/sso/login_process.php',    'https://btm.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/givenname/mail/',   NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'bsro');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('517', 'ITM',   'b922213', '127.0.0.1', '1',    '/wp-content/themes/TheFox_child_theme/sso/login_process.php',  'https://itm.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/givenname/mail/',   NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'bsro');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('521', '학부총학생회홈페이지',    'b933324',  '127.0.0.1', '1',   '/wiki/Special:KSSO',   'https://student.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/mail/',  NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'lego3410');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('522', '전기전자공학부인트라넷',   'b944435',  '127.0.0.1', '1',   '/login/responseLogin.do',  'https://eeintra.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/telephoneNumber/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/employeeType/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/acad_ebs_org_id/',     NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'wingleta');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('670', '학부생과제용',    'b955546', '127.0.0.1', '1',    '/checkmate',   'https://sa.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_born_date/ku_sex/mobile/postalCode/ku_std_no/employeeType/ku_stdnt_type_class/acad_ebs_org_id/',     NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'capdata');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('762', '상담센터 홈페이지',     'b966657', '127.0.0.1', '1',    '/iamps/loginprocess.asp',  'https://kcc.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/ku_sex/mail/mobile/ku_std_no/ku_kaist_org_id/uid/',     NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'chonbg');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('700', 'SOC DB Portal',     'b977768',  '127.0.0.1', '1',   '/auth/auth.php',   'https://socdb.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_born_date/mail/mobile/ku_employee_number/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/employeeType/ku_ebs_start_date/uid/',     NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'stim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('497', '채용정보홈페이지',      'b988879', '127.0.0.1', '1',    '/auth/index.php',  'https://career.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/ku_std_no/ku_departmentcode/ku_departmentname_eng/ou/ku_psft_user_status/ku_psft_user_status_kor/employeeType/uid/',    NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'ahsa89');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('730', '공과대학홈페이지',      'b999990', '127.0.0.1', '1',    '/sso/rslogin',     'https://engineering.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/mail/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_departmentcode/ku_departmentname_eng/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/acad_ebs_org_id/uid/',     NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'cej909');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('731', '아이디어팩토리',   'b1011101', '127.0.0.1', '1',   '/auth/auth.php',   'https://ideafactory.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/ku_ch_mail/mobile/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ou/ku_psft_user_status/ku_psft_user_status_kor/employeeType/uid/',     NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'hami1102');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('742', '인터넷증명발급',   'b1022212', '127.0.0.1', '1',   '/ssl/ssl_kaist_loginChk.asp',  'https://mkiosk.certpia.com',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/ku_born_date/',     NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'ejcho');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('752', 'TEST RD',   'b1033323', '127.0.0.1', '1',   '/webroot/sso/process.php',     'https://testrd.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/ou/uid/',   NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'choichoijae');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('760', 'ICT 운영',    'b1044434', '127.0.0.1', '1',   '/webroot/sso/process.php',     'https://ict.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/ou/uid/',   NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'choichoijae');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('781', '클라우드 포털 2',     'b1055545', '127.0.0.1', '1',   '/auth/signup/internal1',   'https://kcloud2.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_born_date/ku_sex/mail/ku_ch_mail/mobile/telephoneNumber/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_kaist_uid/ku_advr_ebs_person_id/ku_advr_name/ku_advr_name_ac/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/ku_home_phone/acad_ebs_org_id/uid/acad_ebs_org_name_eng/acad_ebs_org_name_kor/',    NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'stim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('780', '클라우드 포탈1',      'b1066656', '127.0.0.1', '1',   '/auth/signup/internal1',   'https://kcloud1.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_born_date/ku_sex/mail/ku_ch_mail/mobile/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_kaist_uid/ku_advr_ebs_person_id/ku_advr_name/ku_advr_name_ac/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/ku_home_phone/acad_ebs_org_id/uid/acad_ebs_org_name_eng/acad_ebs_org_name_kor/',    NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'stim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('768', '클라우드 포털 테스트용',      'b1077767',     '127.0.0.1', '1',   '/auth/signup/internal1',   'https://soccloud.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/kaist_suid/ku_kname/displayname/sn/givenname/ku_born_date/ku_sex/mail/ku_ch_mail/mobile/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_kaist_uid/ku_advr_ebs_person_id/ku_advr_name/ku_advr_name_ac/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/ku_home_phone/acad_ebs_org_id/uid/acad_ebs_org_name_eng/acad_ebs_org_name_kor/',     NULL,   'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   NULL,   'stim');

-- 통합 SSO
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('CA2',     '학사 시스템',   'c11111',   '127.0.0.1', '1',   'https://ssogw6.kaist.ac.kr/sso/CA2_autologin/',    'N/A',  'Y',    'N',    'N',    'S',    'Academic System &#40;학사 시스템&#41;',     NULL,  106,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'capdata');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('SOA',     'ERP 전자결재',     'c11112',   '127.0.0.1', '1',   'http://webs.kaist.ac.kr/erp2/sso/EBW_autologin.jsp?lang=',     'N/A',  'Y',    'N',    'N',    'S',    '&amp',     NULL,  103,     'N',    'N',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'haewon95');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('PO',  '포탈',   'c11113',   '127.0.0.1', '1',   'https://portalsso.kaist.ac.kr/ssoProcess.ps?lang=',    'N/A',  'Y',    'N',    'N',    'S',    'KAIST Portal Service &#40;포탈 서비스&#41;',    NULL,  909,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'ejcho');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('LIB',     '도서관(SSO)',     'c11114',   '127.0.0.1', '1',   'https://ssogw3.kaist.ac.kr/sso/LIB_autologin.do?lang=',    'N/A',  'Y',    'N',    'N',    'S',    'Library &#40;도서관&#41;',    NULL,  108,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'kamamovie');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('SAF',     '안전팀',  'c11115',   '127.0.0.1', '1',   'https://ssogw10.kaist.ac.kr/sso/SAF_autologin_uid.aspx',   'N/A',  'Y',    'N',    'N',    'S',    'Laboratory Safety (안전팀 홈페이지)',     NULL,  111,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'hwang_won');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('EBS',     'Std. ERP',     'c11116',   '127.0.0.1', '1',   'https://kebs.kaist.ac.kr:4443/oa_servlets/AppsLogin?langCode=',    'N/A',  'Y',    'N',    'N',    'S',    'Std. ERP',     NULL,  109,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'imaltair');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('SAT',     '온라인안전인증평가',    'c11117',   '127.0.0.1', '1',   'https://ssogw8.kaist.ac.kr/sso/SAT_autologin.asp',     'N/A',  'Y',    'N',    'N',    'S',    'Online Safety Certificate Accessment (온라인안전인증평가)',     NULL,  112,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'hwang_won');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('EBW',     'Web ERP',  'c11118',   '127.0.0.1', '1',   'http://webs.kaist.ac.kr/erp2/sso/EBW_autologin.jsp?lang=',     'N/A',  'Y',    'N',    'N',    'S',    'Research and General Administrative ERP Service &#40;연구 및 일반행정 ERP 서비스&#41;',  NULL,  102,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'imaltair');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('EIS',     'BI',   'c11119',   '127.0.0.1', '1',   'https://bi.kaist.ac.kr/sso/bi_autologin.aspx',     'N/A',  'Y',    'N',    'N',    'S',    'Business Intelligence Service &#40;경영정보 서비스&#41;',     NULL,  301,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'jiky1234');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('FP',  'Find People(서울)',  'c11120',   '127.0.0.1', '1',   'https://ssogw2.kaist.ac.kr/FP_autologin.asp',  'N/A',  'Y',    'N',    'N',    'S',    'Find People in Seoul Campus',  NULL,  401,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'khlee');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('EMA',     '이메일',  'c11121',   '127.0.0.1', '1',   'https://ssogw17.kaist.ac.kr/sso/EMA_autologin.jsp',    'N/A',  'Y',    'N',    'N',    'S',    '&amp;#40',     NULL,  101,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'jaekwonkim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('WF',  '전자문서',     'c11122',   '127.0.0.1', '1',   'http://workflow.kaist.ac.kr/WF_autologin.aspx',    'N/A',  'Y',    'N',    'N',    'S',    'EDMS Service (전자문서 서비스)',  NULL,  105,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'kcleeb');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('KLMS',    'KLMS',     'c11123',   '127.0.0.1', '1',   'https://ssogw15.kaist.ac.kr/sso/ICAM_autologin.php',   'N/A',  'Y',    'N',    'N',    'S',    'KLMS &#40;학습관리&#41;',  NULL,  107,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'sexylim');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('RIMS',    'RIMS',     'c11124',   '127.0.0.1', '1',   'https://rims.kaist.ac.kr/rims/sso/RIMS_autologin.jsp',     'N/A',  'Y',    'N',    'N',    'S',    'Research Information Management System (연구업적관리시스템)',   NULL,  110,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'hylee');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('IRT',     '국제협력 DB',  'c11125',   '127.0.0.1', '1',   'https://ssogw18.kaist.ac.kr/sso/IRT_autologin.jsp',    'N/A',  'Y',    'N',    'N',    'S',    'Intl Cooperation DB (국제협력 DB)',    NULL,  114,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'rudy96');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('PPMS',    '특허관리',     'c11126',   '127.0.0.1', '1',   'https://ssogw7.kaist.ac.kr/sso/PPMS_autologin_uid.php',    'N/A',  'Y',    'N',    'N',    'S',    'PPMS (특허관리)',  NULL,  115,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'whitejy02');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('ELN',     '전자연구노트',   'c11127',   '127.0.0.1', '1',   'https://ssogw16.kaist.ac.kr/sso/ELN_autologin.jsp',    'N/A',  'Y',    'N',    'N',    'S',    'Electonic Laboratory Note (전자연구노트)',   NULL,  116,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'kcleeb');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('URS',     '예약',   'c11128',   '127.0.0.1', '1',   'https://urs.kaist.ac.kr/urs/sso/urs_autologin.do',     'N/A',  'Y',    'N',    'N',    'S',    '&amp;#40',     NULL,  117,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'haewon95');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID) VALUES ('ICT',     '정보통신팀 홈페이지',   'c11129',   '127.0.0.1', '1',   'https://ict.kaist.ac.kr/sso/ict_autologin.jsp',    'N/A',  'Y',    'N',    'N',    'S',    'ICT homepage &#40;정보통신팀 홈페이지&#41;',    NULL,  118,     'Y',    'Y',    NULL,   TO_DATE(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'),   'choichoijae');

UPDATE BDSCLIENT
SET SECRET = 'N/A';


-- 권한을 사용하는 서비스의 그룹핑을 위한 코드
INSERT INTO BDSCODE
(CODEID, NAME, DESCRIPTION, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('EAM_GROUP', '권한관리 서비스 그룹', '권한관리 서비스 그룹핑', 'N', 'admin', SYSDATE, 'admin', SYSDATE);

-- 권한을 사용하는 서비스의 그룹핑을 위한 세부 코드
INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqtqPMi00h', 'HR', '인사', 'EAM_GROUP', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqtqZMz00j', 'SA', '학사', 'EAM_GROUP', 'N', 'admin', SYSDATE, NULL, SYSDATE);

-- 사용자 유형
INSERT INTO BDSCODE
(CODEID, NAME, DESCRIPTION, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('USER_TYPE', '사용자 유형', NULL, 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvBU-z03i', 'P', '교수', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvBXFV03k', 'E', '직원', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvBenJ03o', 'R', '퇴직자', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvBjgK03q', 'S', '재학생', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvBmpY03s', 'A', '졸업생', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvBr-o03u', 'G', '학생합격자', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvC53S042', 'V', '협력업체직원', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvC7If044', 'O', '외부인', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

UPDATE BDSCLIENT SET INFO_MARK_OPTN = 'kaist_uid' WHERE INFO_MARK_OPTN IS NULL;

COMMIT;
