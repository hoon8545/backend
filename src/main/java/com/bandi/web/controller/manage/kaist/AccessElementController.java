package com.bandi.web.controller.manage.kaist;

import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.AccessElement;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.AccessElementService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;


@Controller
@RequestMapping(value = {"/manage"})
public class AccessElementController {

    @Resource
    protected AccessElementService service;

    @RequestMapping(value="/accesselement", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<AccessElement> save(@RequestBody AccessElement accesselement) {
        if( accesselement == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        List<String> resourceOids = Arrays.asList( accesselement.getResourceOid().split("\\s*,\\s*"));

        service.saveAceList( accesselement.getClientOid(), accesselement.getTargetObjectId(), resourceOids, true);

        ResponseEntity<AccessElement> responseEntity = new ResponseEntity( accesselement, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/accesselement/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<AccessElement> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        AccessElement accesselement = service.get(oid);

        ResponseEntity<AccessElement> responseEntity = new ResponseEntity( accesselement, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/accesselement/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<AccessElement> update(@PathVariable String oid, @RequestBody AccessElement accesselement) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( accesselement == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(accesselement);

        ResponseEntity<AccessElement> responseEntity = new ResponseEntity( accesselement, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/accesselement/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/accesselement/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody AccessElement accesselement) {

        if( accesselement.getPageSize() == 0){
            accesselement.setPageSize( 10);
        }

        if( accesselement.getPageNo() == 0){
            accesselement.setPageNo(1);
        }

        String sortDirection = accesselement.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( accesselement.getPageNo(), accesselement.getPageSize(), accesselement.getSortBy(), sortDirection);
        paginator= service.search(paginator, accesselement);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/accesselement/mappedResrouceOids", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<List<String>> getMappedResourceOidsByTargetObjectId(@RequestBody AccessElement accesselement) {

        accesselement.setTargetObjectType( BandiConstants_KAIST.OBJECT_TYPE_AUTHORITY );

        List<String> mappedResourceOids = service.getMappedResourceOidsByTargetObjectId(  accesselement.getClientOid(), accesselement.getTargetObjectId(), accesselement.getTargetObjectType());

        ResponseEntity<List<String>> responseEntity = new ResponseEntity( mappedResourceOids, HttpStatus.OK);
        return responseEntity;
    }
}
