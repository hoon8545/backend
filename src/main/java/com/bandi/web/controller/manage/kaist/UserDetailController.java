package com.bandi.web.controller.manage.kaist;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.User;
import com.bandi.domain.kaist.UserDetail;
import com.bandi.domain.kaist.UserDetailCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.UserDetailService;


@Controller
@RequestMapping(value = {"/user"})
public class UserDetailController {

    @Resource
    protected UserDetailService service;

    @RequestMapping(value="/userdetail", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<UserDetail> save(@RequestBody UserDetail userdetail) {
        if( userdetail == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(userdetail);

        ResponseEntity<UserDetail> responseEntity = new ResponseEntity( userdetail, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/userdetail/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<UserDetail> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        UserDetail userdetail = service.get(oid);

        ResponseEntity<UserDetail> responseEntity = new ResponseEntity( userdetail, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/userdetail/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<UserDetail> update(@PathVariable String oid, @RequestBody UserDetail userdetail) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( userdetail == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(userdetail);

        ResponseEntity<UserDetail> responseEntity = new ResponseEntity( userdetail, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/userdetail/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/userdetail/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody UserDetail userdetail) {

        if( userdetail.getPageSize() == 0){
            userdetail.setPageSize( 10);
        }

        if( userdetail.getPageNo() == 0){
            userdetail.setPageNo(1);
        }

        String sortDirection = userdetail.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( userdetail.getPageNo(), userdetail.getPageSize(), userdetail.getSortBy(), sortDirection);
        paginator= service.search(paginator, userdetail);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }
    
    @RequestMapping(value="/userdetail/search/{userId}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity getByUserId( @PathVariable String userId) {
        if( userId == null || userId.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        UserDetailCondition condition = new UserDetailCondition();
        condition.createCriteria().andUserIdEqualTo(userId);
        List<UserDetail> userDetails = service.selectByCondition(condition);

        ResponseEntity<User> responseEntity = new ResponseEntity( userDetails, HttpStatus.OK);
        return responseEntity;
    }

}
