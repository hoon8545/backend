package com.bandi.web.controller.manage.kaist;

import com.bandi.common.util.JasperReportUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.domain.JasperReportParams;
import com.bandi.domain.kaist.UserLoginHistory;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.UserLoginHistoryService;

import net.sf.jasperreports.engine.JasperPrint;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Controller
@RequestMapping(value = {"/manage"})
public class UserLoginHistoryController {

    @Resource
    protected UserLoginHistoryService service;

    @RequestMapping(value="/userloginhistory", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<UserLoginHistory> save(@RequestBody UserLoginHistory userloginhistory) {
        if( userloginhistory == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(userloginhistory);

        ResponseEntity<UserLoginHistory> responseEntity = new ResponseEntity( userloginhistory, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/userloginhistory/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<UserLoginHistory> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        UserLoginHistory userloginhistory = service.get(oid);

        ResponseEntity<UserLoginHistory> responseEntity = new ResponseEntity( userloginhistory, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/userloginhistory/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<UserLoginHistory> update(@PathVariable String oid, @RequestBody UserLoginHistory userloginhistory) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( userloginhistory == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(userloginhistory);

        ResponseEntity<UserLoginHistory> responseEntity = new ResponseEntity( userloginhistory, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/userloginhistory/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/userloginhistory/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody UserLoginHistory userloginhistory) {

        if( userloginhistory.getPageSize() == 0){
            userloginhistory.setPageSize( 10);
        }

        if( userloginhistory.getPageNo() == 0){
            userloginhistory.setPageNo(1);
        }

        String sortDirection = userloginhistory.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( userloginhistory.getPageNo(), userloginhistory.getPageSize(), userloginhistory.getSortBy(), sortDirection);
        paginator= service.search(paginator, userloginhistory);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }
    
    @RequestMapping(value = "/userloginhistory/export", method = RequestMethod.POST, produces="application/json")
    public void export(HttpServletRequest request, HttpServletResponse response, @RequestBody UserLoginHistory userloginhistory) throws Exception {
        if( userloginhistory.getPageSize() == 0){
            userloginhistory.setPageSize(10);
        }

        if( userloginhistory.getPageNo() == 0){
            userloginhistory.setPageNo(1);
        }
        
        JasperReportParams jasperReportParams = new JasperReportParams();
        
        jasperReportParams.setPageFlag(request.getParameter("pageFlag"));
        jasperReportParams.setTotalPageStr(request.getParameter("totalPage"));
        jasperReportParams.setRangeStartStr(request.getParameter("rangeStart"));
        jasperReportParams.setRangeEndStr(request.getParameter("rangeEnd"));
        jasperReportParams.setJrxmlPath(request.getSession().getServletContext().getRealPath("/WEB-INF/classes/jasper/UserLoginHistory.jrxml"));
        
        JasperPrint jasperPrint = service.exportDataFileJasper(jasperReportParams, userloginhistory);
        
        response.setHeader("Set-Cookie", "fileDownload=true; path=/");
        response.setHeader("Content-Disposition", String.format("attachment;"));
        
        final OutputStream outStream = response.getOutputStream();
        
        switch (request.getParameter("fileFlag")) {
        case "E":
            JasperReportUtil.exportXlsx(jasperPrint, outStream);
            break;
        case "P":
            JasperReportUtil.exportPdf(jasperPrint, outStream);
            break;
        case "H":
            JasperReportUtil.exportHtml(jasperPrint, outStream);
            break;
        case "C":
            JasperReportUtil.exportCsv(jasperPrint, outStream);
            break;
        }                                   
    }
    
}
