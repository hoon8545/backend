package com.bandi.web.controller.manage.kaist;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.IdentityAuthentication;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.IdentityAuthenticationService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;


@Controller
@RequestMapping(value = {"/manage"})
public class IdentityAuthenticationController {

    @Resource
    protected IdentityAuthenticationService service;

    @RequestMapping(value="/identityauthentication", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<IdentityAuthentication> save(@RequestBody IdentityAuthentication identityauthentication) {
        if( identityauthentication == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(identityauthentication);

        ResponseEntity<IdentityAuthentication> responseEntity = new ResponseEntity( identityauthentication, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/getIdentityAuthentication/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<IdentityAuthentication> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        IdentityAuthentication identityauthentication = service.get(oid);

        ResponseEntity<IdentityAuthentication> responseEntity = new ResponseEntity( identityauthentication, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/identityauthentication/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<IdentityAuthentication> update(@PathVariable String oid, @RequestBody IdentityAuthentication identityauthentication) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( identityauthentication == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(identityauthentication);

        ResponseEntity<IdentityAuthentication> responseEntity = new ResponseEntity( identityauthentication, HttpStatus.OK);
        return responseEntity;
    }
    
    @RequestMapping(value ="/updateIdentityAuthentication", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity updateAuthentication(@RequestBody IdentityAuthentication identityAuthentication) {
    	if ( identityAuthentication == null ) {
    		throw new BandiException ( ErrorCode.PARAMETER_IS_NULL );
    	}
    	
    	service.update(identityAuthentication);
    	
    	ResponseEntity responseEntity = new ResponseEntity(HttpStatus.OK);
    	return responseEntity;
    }

}
