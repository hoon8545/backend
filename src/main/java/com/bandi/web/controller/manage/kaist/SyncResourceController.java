package com.bandi.web.controller.manage.kaist;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.SyncResource;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.SyncResourceService;


@Controller
@RequestMapping(value = {"/manage"})
public class SyncResourceController {

    @Resource
    protected SyncResourceService service;

    @RequestMapping(value="/syncresource", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<SyncResource> save(@RequestBody SyncResource syncresource) {
        if( syncresource == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(syncresource);

        ResponseEntity<SyncResource> responseEntity = new ResponseEntity( syncresource, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/syncresource/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<SyncResource> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        SyncResource syncresource = service.get(oid);

        ResponseEntity<SyncResource> responseEntity = new ResponseEntity( syncresource, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/syncresource/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<SyncResource> update(@PathVariable String oid, @RequestBody SyncResource syncresource) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( syncresource == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(syncresource);

        ResponseEntity<SyncResource> responseEntity = new ResponseEntity( syncresource, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/syncresource/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/syncresource/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody SyncResource syncresource) {

        if( syncresource.getPageSize() == 0){
            syncresource.setPageSize( 10);
        }

        if( syncresource.getPageNo() == 0){
            syncresource.setPageNo(1);
        }

        String sortDirection = syncresource.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( syncresource.getPageNo(), syncresource.getPageSize(), syncresource.getSortBy(), sortDirection);
        paginator= service.search(paginator, syncresource);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }

}
