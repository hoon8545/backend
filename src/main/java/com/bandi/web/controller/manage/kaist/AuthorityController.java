package com.bandi.web.controller.manage.kaist;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.Authority;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.AuthorityService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;


@Controller
@RequestMapping(value = {"/manage"})
public class AuthorityController {

    @Resource
    protected AuthorityService service;

    @RequestMapping(value="/authority", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<Authority> save(@RequestBody Authority authority) {
        if( authority == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(authority);

        ResponseEntity<Authority> responseEntity = new ResponseEntity( authority, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/authority/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<Authority> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        Authority authority = service.get(oid);

        ResponseEntity<Authority> responseEntity = new ResponseEntity( authority, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/authority/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<Authority> update(@PathVariable String oid, @RequestBody Authority authority) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( authority == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(authority);

        ResponseEntity<Authority> responseEntity = new ResponseEntity( authority, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/authority/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/authority/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody Authority authority) {

        if( authority.getPageSize() == 0){
            authority.setPageSize( 10);
        }

        if( authority.getPageNo() == 0){
            authority.setPageNo(1);
        }

        String sortDirection = authority.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( authority.getPageNo(), authority.getPageSize(), authority.getSortBy(), sortDirection);
        paginator= service.search(paginator, authority);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }

}
