package com.bandi.web.controller.manage.kaist;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.RoleMaster;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.RoleMasterService;


@Controller
@RequestMapping(value = {"/manage"})
public class RoleMasterController{

    @Resource
    protected RoleMasterService service;

    @RequestMapping(value="/rolemaster", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<RoleMaster> save(@RequestBody RoleMaster rolemaster) {
        if( rolemaster == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(rolemaster);

        ResponseEntity<RoleMaster> responseEntity = new ResponseEntity( rolemaster, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/rolemaster/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<RoleMaster> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        RoleMaster rolemaster = service.get(oid);

        ResponseEntity<RoleMaster> responseEntity = new ResponseEntity( rolemaster, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/rolemaster/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<RoleMaster> update(@PathVariable String oid, @RequestBody RoleMaster rolemaster) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( rolemaster == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(rolemaster);

        ResponseEntity<RoleMaster> responseEntity = new ResponseEntity( rolemaster, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/rolemaster/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/rolemaster/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody RoleMaster rolemaster) {

        if( rolemaster.getPageSize() == 0){
            rolemaster.setPageSize( 10);
        }

        if( rolemaster.getPageNo() == 0){
            rolemaster.setPageNo(1);
        }

        String sortDirection = rolemaster.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( rolemaster.getPageNo(), rolemaster.getPageSize(), rolemaster.getSortBy(), sortDirection);
        paginator= service.search(paginator, rolemaster);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity<>( paginator, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/rolemaster/selfRoleSearch", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> selfRoleList(@RequestBody RoleMaster rolemaster) {

        if( rolemaster.getPageSize() == 0){
            rolemaster.setPageSize( 10);
        }

        if( rolemaster.getPageNo() == 0){
            rolemaster.setPageNo(1);
        }

        String sortDirection = rolemaster.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( rolemaster.getPageNo(), rolemaster.getPageSize(), rolemaster.getSortBy(), sortDirection);
        paginator= service.selfRoleSearch(paginator, rolemaster);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity<>( paginator, HttpStatus.OK);
        return responseEntity;
    }
}
