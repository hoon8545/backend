package com.bandi.web.controller.manage.kaist;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.RoleAuthority;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.RoleAuthorityService;


@Controller
@RequestMapping(value = {"/manage"})
public class RoleAuthorityController {

    @Resource
    protected RoleAuthorityService service;

    @RequestMapping(value="/roleauthority", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<RoleAuthority> save(@RequestBody RoleAuthority roleauthority) {
        if( roleauthority == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(roleauthority);

        ResponseEntity<RoleAuthority> responseEntity = new ResponseEntity( roleauthority, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/roleauthoritys", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity <List<RoleAuthority>> saveList(@RequestBody List<RoleAuthority> roleauthoritys) {
        if( roleauthoritys == null || roleauthoritys.size() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insertList(roleauthoritys);

        ResponseEntity<List<RoleAuthority>> responseEntity = new ResponseEntity( roleauthoritys, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/roleauthority/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<RoleAuthority> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        RoleAuthority roleauthority = service.get(oid);

        ResponseEntity<RoleAuthority> responseEntity = new ResponseEntity( roleauthority, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/roleauthority/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<RoleAuthority> update(@PathVariable String oid, @RequestBody RoleAuthority roleauthority) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( roleauthority == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(roleauthority);

        ResponseEntity<RoleAuthority> responseEntity = new ResponseEntity( roleauthority, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/roleauthority/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/roleauthority/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody RoleAuthority roleauthority) {

        if( roleauthority.getPageSize() == 0){
            roleauthority.setPageSize( 10);
        }

        if( roleauthority.getPageNo() == 0){
            roleauthority.setPageNo(1);
        }

        String sortDirection = roleauthority.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( roleauthority.getPageNo(), roleauthority.getPageSize(), roleauthority.getSortBy(), sortDirection);
        paginator= service.search(paginator, roleauthority);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }

}
