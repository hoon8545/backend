package com.bandi.web.controller.manage.kaist;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.MessageInfo;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.MessageInfoService;


@Controller
@RequestMapping(value = {"/manage"})
public class MessageInfoController {

    @Resource
    protected MessageInfoService service;

    @RequestMapping(value="/messageinfo", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<MessageInfo> save(@RequestBody MessageInfo messageinfo) {
        if( messageinfo == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(messageinfo);

        ResponseEntity<MessageInfo> responseEntity = new ResponseEntity( messageinfo, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/messageinfo/getManageCode", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<MessageInfo> get() {

        String dbManageCode = service.getManageCode();

        if(dbManageCode == null) {
            dbManageCode = "10000";
        }

        ResponseEntity<MessageInfo> responseEntity = new ResponseEntity( dbManageCode, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/messageinfo/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<MessageInfo> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        MessageInfo messageinfo = service.get(oid);

        ResponseEntity<MessageInfo> responseEntity = new ResponseEntity( messageinfo, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/messageinfo/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<MessageInfo> update(@PathVariable String oid, @RequestBody MessageInfo messageinfo) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( messageinfo == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(messageinfo);

        ResponseEntity<MessageInfo> responseEntity = new ResponseEntity( messageinfo, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/messageinfo/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/messageinfo/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody MessageInfo messageinfo) {

        if( messageinfo.getPageSize() == 0){
            messageinfo.setPageSize( 10);
        }

        if( messageinfo.getPageNo() == 0){
            messageinfo.setPageNo(1);
        }

        String sortDirection = messageinfo.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( messageinfo.getPageNo(), messageinfo.getPageSize(), messageinfo.getSortBy(), sortDirection);
        paginator= service.search(paginator, messageinfo);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }

}
