package com.bandi.web.controller.manage.kaist;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.KaistOIMWebServiceUtil;
import com.bandi.common.util.MessageUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.domain.Admin;
import com.bandi.domain.User;
import com.bandi.domain.kaist.ServiceRequest;
import com.bandi.domain.kaist.ServiceRequestCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.cryption.CryptService;
import com.bandi.service.manage.AdminService;
import com.bandi.service.manage.ConfigurationService;
import com.bandi.service.manage.kaist.MessageInfoService;
import com.bandi.service.manage.kaist.ServiceRequestService;
import com.bandi.service.manage.kaist.UserService_KAIST;

import net.sf.json.JSONObject;


@Controller
public class ServiceRequestController {

    @Resource
    protected ServiceRequestService service;

    @Autowired
    protected CryptService cryptService;

    @Resource
    protected ConfigurationService configurationService;

    @Resource
    protected MessageInfoService messageInfoService;

    @Resource
    protected UserService_KAIST userService;

    @Resource
    protected AdminService adminService;
    
    private static final Logger logger = LoggerFactory.getLogger( ServiceRequestController.class );

    @RequestMapping(value="/user/before/servicerequest", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<ServiceRequest> save(@RequestBody ServiceRequest servicerequest) {
        if( servicerequest == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(servicerequest);

        ResponseEntity<ServiceRequest> responseEntity = new ResponseEntity( servicerequest, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/manage/servicerequest/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<ServiceRequest> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        ServiceRequest servicerequest = service.get(oid);

        ResponseEntity<ServiceRequest> responseEntity = new ResponseEntity( servicerequest, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/manage/servicerequest/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<ServiceRequest> update(@PathVariable String oid, @RequestBody ServiceRequest servicerequest) throws Exception {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( servicerequest == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        String kaistUid = servicerequest.getKaistUid();
        String status = servicerequest.getStatus();
        String reqType = servicerequest.getReqType();
        String email = servicerequest.getEmail();
        String approverUid = ContextUtil.getCurrentUserId();
        String approverComment = servicerequest.getApproverComment();
        String initPassword = "";
        String userId = userService.getByKaistUid(kaistUid).getId();

        Admin admin = adminService.get(approverUid);
        String approver = admin.getName();

        JSONObject messageJson = new JSONObject();
        JSONObject resultJson = new JSONObject();

        //승인일 경우
        if("A".equals(status)) {
            if("id".equals(reqType)) {

                messageJson.put(BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_FIND_ID_RESULT);
                messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE1, userId);
                messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE2, userId);
                messageJson.put(BandiConstants_KAIST.MESSAGE_EMAIL, email);
                messageJson.put(BandiConstants_KAIST.MESSAGE_KAIST_UID, kaistUid);

                resultJson = messageInfoService.messageSend(messageJson);

            } else if("password".equals(reqType)) {

                initPassword = service.getInitPssword(userId);

                messageJson.put(BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_INIT_PASSWORD);
                messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE, initPassword);
                messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE1, initPassword);
                messageJson.put(BandiConstants_KAIST.MESSAGE_EMAIL, email);
                messageJson.put(BandiConstants_KAIST.MESSAGE_KAIST_UID, kaistUid);
                
                resultJson = messageInfoService.messageSend(messageJson);

            } else if("idpassword".equals(reqType)) {

                initPassword = service.getInitPssword(userId);

                messageJson.put(BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_FIND_ID_PWD);
                messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE, userId);
                messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE1, initPassword);
                messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE2, userId);
                messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE3, initPassword);
                messageJson.put(BandiConstants_KAIST.MESSAGE_EMAIL, email);
                messageJson.put(BandiConstants_KAIST.MESSAGE_KAIST_UID, kaistUid);
                
                resultJson = messageInfoService.messageSend(messageJson);


            } else if("deleteuser".equals(reqType)) {
                /* KAIST-TODO 김보미 : IAM회원 탈퇴 일 경우 (퇴직자구분)
                 * [해결방안] : LDAP처리 후 회원탈퇴 기능 구현   */

            	KaistOIMWebServiceUtil imUtil = new KaistOIMWebServiceUtil();
            	
                //계정이 살아있는지 여부 확인
                String isDesabledUserValue = "false";
                if(!"".equals(kaistUid)&&null!=kaistUid){
                    try {
                        isDesabledUserValue = imUtil.isDisabledInternalUser(kaistUid);
                    }catch(NullPointerException ne){
                    	logger.error("ERROR IM UTRIL NullPointerException ERROR isDisabledInternalUser : "+kaistUid+"  [isDesabledUser : ]"+isDesabledUserValue);
                    } catch (Exception e){
                    	logger.error("ERROR IM UTRIL ERROR isDisabledInternalUser : "+kaistUid+"  [isDesabledUser : ]"+isDesabledUserValue);
                    }
                }

                //퇴직처리
                String disabledValue = "";
                if("false".equals(isDesabledUserValue)){
                    disabledValue = imUtil.setDisableInternalUser(kaistUid);
                }

                //퇴직처리 확인
                boolean sendMessageFlg = false;
                if("true".equals(disabledValue)){
                    sendMessageFlg = true;
                }else if("false".equals(disabledValue)){
                	throw new BandiException(userId + " is disabled.");
                }else if("systemerror".equals(disabledValue)){
                    logger.debug("System Error : [webservice fail]");
                }else{
                    logger.debug("System Error : [SYSTEM ERROR]");
                }

                if(sendMessageFlg){
                	//messageJson.put(BandiConstants_KAIST.MANAGECODE, "10026");
                	//messageJson.put("email", email);
                    //resultJson = messageInfoService.messageSend(messageJson);

                }
            } else {
                String approverCommentReturn = "";
                if(approverComment == null || approverComment.length() == 0) {
                    approverCommentReturn = MessageUtil.get( "serviceRequest.approverComment.reject.sendValue");
                    approverCommentReturn = approverCommentReturn.replaceAll("\n", "<br/>");
                } else {
                    approverCommentReturn = approverComment.replaceAll("\n", "<br/>");
                }

                messageJson.put(BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_DELETE);
                messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE, approverCommentReturn);
                messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE1, approverCommentReturn);
                messageJson.put(BandiConstants_KAIST.MESSAGE_EMAIL, email);
                messageJson.put(BandiConstants_KAIST.MESSAGE_KAIST_UID, kaistUid);

                resultJson = messageInfoService.messageSend(messageJson);
                
            }
        }
        // 기각일 경우
        else if("D".equals(status)) {
            String approverCommentReturn = "";
            if(approverComment == null || approverComment.length() == 0) {
                approverCommentReturn = MessageUtil.get( "serviceRequest.approverComment.reject.sendValue");
            } else {
                approverCommentReturn = approverComment.replaceAll("\n", "<br/>");
            }

            messageJson.put(BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_DELETE);
            messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE, approverCommentReturn);
            messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE1, approverCommentReturn);
            messageJson.put(BandiConstants_KAIST.MESSAGE_EMAIL, email);
            messageJson.put(BandiConstants_KAIST.MESSAGE_KAIST_UID, kaistUid);

            resultJson = messageInfoService.messageSend(messageJson);
            
        }

        // ====================================================================================
        
        //resultJson = messageInfoService.messageSend(messageJson);
        
        ServiceRequest sr = new ServiceRequest();
        boolean sendMailResult = true;
        
        if(resultJson.containsKey("EmailResult")){
			if(resultJson.getBoolean("EmailResult")){
				sendMailResult = true;
				if ("A".equals(status)) {
					if ("password".equals(reqType) || "idpassword".equals(reqType)) {
						User userFromDB = userService.get(userId);
						userFromDB.setPassword(initPassword);
						userService.updatePassword(userFromDB);
					}
				}
			}else{
				sendMailResult = false;
				throw new BandiException(MessageUtil.get("MAIL_SEND_FAIL"));
			}
		}
        
        sr = service.get(oid);
    	
        sr.setApproverUid(approverUid);
        sr.setApprover(approver);
        sr.setStatus(status);
        sr.setApproverComment(approverComment);
        
        service.update(sr);
        
        ResponseEntity<ServiceRequest> responseEntity = new ResponseEntity( sr, HttpStatus.OK);
        return responseEntity;
    }


    @RequestMapping(value="/manage/servicerequest/updateMulti/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<ServiceRequest> updateMulti(@PathVariable String oid, @RequestBody ServiceRequest servicerequest) throws Exception {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        JSONObject messageJson = new JSONObject();
        JSONObject resultJson = new JSONObject();
        ServiceRequest sr = new ServiceRequest();

        boolean sendMailResult = true;
        String status = servicerequest.getStatus();
        String approverUid = ContextUtil.getCurrentUserId();
        String reqType = "";
        String kaistUid = "";
        String email = "";
        String userId = "";
        String initPassword = "";

        Admin admin = adminService.get(approverUid);
        String approver = admin.getName();

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            for(String result : oids) {
                sr = service.get(result);
                
                kaistUid = sr.getKaistUid();
                email = sr.getEmail();
                reqType = sr.getReqType();
                
                User userFromDB = userService.getByKaistUid(kaistUid);

                if(userFromDB == null) {
                    throw new BandiException( MessageUtil.get("SERVICEREQUEST_USERID_NOT_FOUND"));
                }

                userId = userFromDB.getId();

                //승인일 경우
                if("A".equals(status)) {
                    sr.setApproverComment(MessageUtil.get("serviceRequest.approverComment.approve"));

                    if("id".equals(reqType)) {
                        messageJson.put(BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_FIND_ID_RESULT);
                        messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE1, userId);
                        messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE2, userId);
                        messageJson.put(BandiConstants_KAIST.MESSAGE_EMAIL, email);
                        messageJson.put(BandiConstants_KAIST.MESSAGE_KAIST_UID, kaistUid);

                        resultJson = messageInfoService.messageSend(messageJson);

                    } else if("password".equals(reqType)) {
                    	
                        initPassword = service.getInitPssword(userId);

                        messageJson.put(BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_INIT_PASSWORD);
                        messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE, initPassword);
                        messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE1, initPassword);
                        messageJson.put(BandiConstants_KAIST.MESSAGE_EMAIL, email);
                        messageJson.put(BandiConstants_KAIST.MESSAGE_KAIST_UID, kaistUid);

                        resultJson = messageInfoService.messageSend(messageJson);
                        if(resultJson.containsKey("EmailResult")){
        					if(resultJson.getBoolean("EmailResult")){
        						sendMailResult = true;
        						
        						userFromDB.setPassword(initPassword);
        						userService.updatePassword(userFromDB);
        					}else{
        						sendMailResult = false;
        						throw new BandiException(MessageUtil.get("MAIL_SEND_FAIL"));
        					}
        				}

                    } else if("idpassword".equals(reqType)) {
                    	
                    	initPassword = service.getInitPssword(userId);

                        messageJson.put(BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_FIND_ID_PWD);
                        messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE, userId);
                        messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE1, initPassword);
                        messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE2, userId);
                        messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE3, initPassword);
                        messageJson.put(BandiConstants_KAIST.MESSAGE_EMAIL, email);
                        messageJson.put(BandiConstants_KAIST.MESSAGE_KAIST_UID, kaistUid);

                        resultJson = messageInfoService.messageSend(messageJson);
                        
                    }
                }
                //기각일 경우
                else if("D".equals(status)) {
                    sr.setApproverComment(MessageUtil.get("serviceRequest.approverComment.reject"));

                    String sendValue = MessageUtil.get( "serviceRequest.approverComment.reject.sendValue");

                    messageJson.put(BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_DELETE);
                    messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE, sendValue);
                    messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE1, sendValue);
                    messageJson.put(BandiConstants_KAIST.MESSAGE_EMAIL, email);
                    messageJson.put(BandiConstants_KAIST.MESSAGE_KAIST_UID, kaistUid);

                    resultJson = messageInfoService.messageSend(messageJson);

                }
                //보류일 경우
                else if("W".equals(status)) {
                    sr.setApproverComment(MessageUtil.get("serviceRequest.approverComment.wait"));
                }
                
                if(resultJson.containsKey("EmailResult")){
					if(resultJson.getBoolean("EmailResult")){
						sendMailResult = true;
						
						if ("A".equals(status)) {
							if ("password".equals(reqType) || "idpassword".equals(reqType)) {
								userFromDB.setPassword(initPassword);
								userService.updatePassword(userFromDB);
							}
						}
					}else{
						sendMailResult = false;
						throw new BandiException(MessageUtil.get("MAIL_SEND_FAIL"));
					}
				}
                
                sr.setStatus(status);
                sr.setApproverUid(approverUid);
                sr.setApprover(approver);
                service.update(sr);
            }
        } else {
            sr = service.get(oid);
            sr.setStatus(status);
            update(oid, sr);
        }

        ResponseEntity<ServiceRequest> responseEntity = new ResponseEntity( sr, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/manage/servicerequest/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody ServiceRequest servicerequest) {

        if( servicerequest.getPageSize() == 0){
            servicerequest.setPageNo( 10);
        }

        if( servicerequest.getPageNo() == 0){
            servicerequest.setPageNo(1);
        }

        String sortDirection = servicerequest.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( servicerequest.getPageNo(), servicerequest.getPageSize(), servicerequest.getSortBy(), sortDirection);
        paginator= service.search(paginator, servicerequest);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }
    
    @RequestMapping(value="/manage/servicerequest/adminMainServiceRequest", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<List<ServiceRequest>> getAdminMainServiceRequest() {
    	
    	List<ServiceRequest> serviceRequestList = service.getAdminMainServiceRequest();
    	
    	ResponseEntity<List<ServiceRequest>> responseEntity = new ResponseEntity<List<ServiceRequest>>(serviceRequestList, HttpStatus.OK);
    	return responseEntity;
    }
}
