package com.bandi.web.controller.manage.kaist;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.DelegateHistory;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.DelegateHistoryService;


@Controller
@RequestMapping(value = {"/manage"})
public class DelegateHistoryController {

    @Resource
    protected DelegateHistoryService service;

    @RequestMapping(value="/delegatehistory", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<DelegateHistory> save(@RequestBody DelegateHistory delegatehistory) {
        if( delegatehistory == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if (delegatehistory.getAuthorities() == null || delegatehistory.getAuthorities().trim().length() == 0) {
            throw new BandiException( ErrorCode_KAIST.DELEGATE_AUTHORITY_IS_NULL);
        }

        service.insert(delegatehistory);

        ResponseEntity<DelegateHistory> responseEntity = new ResponseEntity<>( delegatehistory, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/delegatehistory/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<DelegateHistory> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        DelegateHistory delegatehistory = service.get(oid);

        ResponseEntity<DelegateHistory> responseEntity = new ResponseEntity( delegatehistory, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/delegatehistory/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<DelegateHistory> update(@PathVariable String oid, @RequestBody DelegateHistory delegatehistory) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( delegatehistory == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(delegatehistory);

        ResponseEntity<DelegateHistory> responseEntity = new ResponseEntity( delegatehistory, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/delegatehistory/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/delegatehistory/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody DelegateHistory delegatehistory) {

        if( delegatehistory.getPageSize() == 0){
            delegatehistory.setPageSize( 10);
        }

        if( delegatehistory.getPageNo() == 0){
            delegatehistory.setPageNo(1);
        }

        String sortDirection = delegatehistory.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( delegatehistory.getPageNo(), delegatehistory.getPageSize(), delegatehistory.getSortBy(), sortDirection);
        paginator= service.search(paginator, delegatehistory);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/delegatehistory/cancle/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity cancleDelegateRole(@PathVariable String oid) {
        if( oid == null || oid.trim().length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.cancleDelegateRole(oid);

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

}
