package com.bandi.web.controller.manage.kaist;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.DelegateAuthority;
import com.bandi.domain.kaist.DelegateAuthorityCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.DelegateAuthorityService;


@Controller
@RequestMapping(value = {"/manage"})
public class DelegateAuthorityController {

    @Resource
    protected DelegateAuthorityService service;

    @RequestMapping(value="/delegateauthority", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<DelegateAuthority> save(@RequestBody DelegateAuthority delegateauthority) {
        if( delegateauthority == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(delegateauthority);

        ResponseEntity<DelegateAuthority> responseEntity = new ResponseEntity( delegateauthority, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/delegateauthority/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<DelegateAuthority> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        DelegateAuthority delegateauthority = service.get(oid);

        ResponseEntity<DelegateAuthority> responseEntity = new ResponseEntity( delegateauthority, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/delegateauthority/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<DelegateAuthority> update(@PathVariable String oid, @RequestBody DelegateAuthority delegateauthority) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( delegateauthority == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(delegateauthority);

        ResponseEntity<DelegateAuthority> responseEntity = new ResponseEntity( delegateauthority, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/delegateauthority/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/delegateauthority/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody DelegateAuthority delegateauthority) {

        if( delegateauthority.getPageSize() == 0){
            delegateauthority.setPageSize( 10);
        }

        if( delegateauthority.getPageNo() == 0){
            delegateauthority.setPageNo(1);
        }

        String sortDirection = delegateauthority.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( delegateauthority.getPageNo(), delegateauthority.getPageSize(), delegateauthority.getSortBy(), sortDirection);
        paginator= service.search(paginator, delegateauthority);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/delegateauthority/search/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity getByDelegateOid( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        DelegateAuthorityCondition condition = new DelegateAuthorityCondition();
        condition.createCriteria().andDelegateOidEqualTo(oid);
        List<DelegateAuthority> delegateauthoritys = service.selectByCondition(condition);

        ResponseEntity<DelegateAuthority> responseEntity = new ResponseEntity( delegateauthoritys, HttpStatus.OK);
        return responseEntity;
    }

}
