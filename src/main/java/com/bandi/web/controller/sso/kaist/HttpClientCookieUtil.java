package com.bandi.web.controller.sso.kaist;

import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;
 

public class HttpClientCookieUtil {
  
	/**
	 * http component cookie를 servlet cookie로 변경
	 * 
	 * @param httpCookie
	 * @return
	 */
	public static Cookie getCookieFromHttpCookie(org.apache.http.cookie.Cookie httpCookie) {
		if(httpCookie == null) {
			return null;
		}
   
		String name = httpCookie.getName();
		String value = httpCookie.getValue();
   
		Cookie cookie = new Cookie(name, value);
 
		// set the domain
		String domain = httpCookie.getDomain();
		if(domain != null) {
			cookie.setDomain(domain);
		}
	   
		  // path
		String path = httpCookie.getPath();
		if(path != null) {
		    cookie.setPath(path);
		}
	   
		// secure
		cookie.setSecure(httpCookie.isSecure());
		
		// comment
		String commment = httpCookie.getComment();
		if(commment != null) {
			cookie.setComment(commment);
		}
	   
		// version
		cookie.setVersion(httpCookie.getVersion());
	   
		// From the Apache source code, maxAge is converted to expiry date using the following formula
		// if (maxAge >= 0) {
	        //     setExpiryDate(new Date(System.currentTimeMillis() + maxAge * 1000L));
	        // }
		// Reverse this to get the actual max age
	   
		Date expiryDate = httpCookie.getExpiryDate();
		
		if(expiryDate != null) {
			long maxAge = (expiryDate.getTime() - System.currentTimeMillis()) / 1000;
			// we have to lower down, no other option
			cookie.setMaxAge((int) maxAge);
		}
	   
		// return the servlet cookie
		return cookie;
	}
  
	/**
	 * http component cookie를 servlet cookie로 변경
	 * @param cookieList
	 * @return
	 */
	public static Cookie[] getCookiesFromHttpCookie(List<org.apache.http.cookie.Cookie> cookieList) {
		if( cookieList == null || cookieList.size() <1) {
			return null;
		}
		
		Cookie[] cookies = new Cookie[cookieList.size()];
		
		int count = cookieList.size();
		for(int i=0 ;i < count ;i++) { 
			cookies[i] = getCookieFromHttpCookie(cookieList.get(i));
		}
		
		return cookies;
	}

	/**
	 * java servlet cookie를 httpcomponent cookie로 변경
	 * @param cookie
	 * @return
	 */
	public static org.apache.http.cookie.Cookie getHttpCookieFromCookie(Cookie cookie) {
		if(cookie == null) {
			return null;
		}
   
		org.apache.http.impl.cookie.BasicClientCookie httpCookie = null;
   
		// get all the relevant parameters
		
		String name = cookie.getName();
		String value = cookie.getValue();
		
		// create the apache cookie
		httpCookie = new org.apache.http.impl.cookie.BasicClientCookie(name, value);
      
		// set additional parameters
		httpCookie.setSecure(cookie.getSecure());
		httpCookie.setComment(cookie.getComment());
		httpCookie.setVersion(cookie.getVersion());
		
		
		String domain = cookie.getDomain();
		if( domain != null) {
			httpCookie.setDomain(domain);
		}
		
		String path = cookie.getPath();
		if( path != null) {
			httpCookie.setPath(path);
		}
		
		// return the apache cookie
		return httpCookie;
	}
}

