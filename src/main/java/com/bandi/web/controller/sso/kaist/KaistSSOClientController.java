package com.bandi.web.controller.sso.kaist;


import java.io.File;
import java.io.FileNotFoundException;
import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bandi.service.manage.RoleService;
import com.bandi.service.manage.kaist.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bandi.common.BandiConstants;
import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.common.util.KaistOIMLdapUtil;
import com.bandi.common.util.WebUtils;
import com.bandi.domain.Client;
import com.bandi.domain.User;
import com.bandi.domain.kaist.OtpHistory;
import com.bandi.domain.kaist.UserLoginHistory;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.cryption.CryptService;
import com.bandi.service.manage.LoginHistoryService;
import com.bandi.service.sso.kaist.ClientApiException;
import com.bandi.service.sso.kaist.ClientApiService;
import com.bandi.service.sso.kaist.HttpConnService;
import com.bandi.service.sso.kaist.ResponseDTO;
import com.bandi.service.sso.kaist.ResponseErrorDTO;
import com.initech.eam.nls.CookieManager;
import com.initech.eam.saml.util.Util;
import com.initech.eam.smartenforcer.SECode;

@RestController
@RequestMapping("/api/sso")
public class KaistSSOClientController {

	protected Logger logger = LoggerFactory.getLogger(KaistSSOClientController.class);

	@Resource(name="initechClientApiService")
	protected ClientApiService clientApiService;

	@Resource(name="ssoLoginManager")
	protected SsoLoginManager ssoLoginManager;

	// protected static final String SESSION_TOKEN = "S_AC_TOKEN";

	// parameter name
	public static final String PARAM_OTP = "otp"; // otp
	public static final String PARAM_ALREADY_OPT_CHECK = "alrdotp"; // otp 검사 완료
	public static final String PARAM_OTP_USER_ID = "userId"; // otp 사용자 아이디

	public static final String PARAM_USER_ID = "user_id"; // userId
	public static final String PARAM_STATE = "state"; // userId
	public static final String PARAM_USER_PASSWORD = "pw"; // password
	public static final String PARAM_REMOTE_IP = "remote_ip"; // remoteIp
	public static final String PARAM_ALREADY_LOGIN = "alrdln"; // 이미 로그인 여부 검사, t or f
	public static final String PARAM_ONLY_LOGIN = "onlylg"; // 로그인만 하고 sso 쿠키를 생서하지 않음

	public static final String PARAM_OAUTH_CLIENT_ID = "client_id"; // client id
	public static final String PARAM_OAUTH_CLIENT_SECRET = "client_secret"; // client secret

	public static final String PARAM_AUTH_RESOURCE_ID = "resource_id"; // 권한 정보
	public static final String PARAM_AUTH_RESOURCE_LEVEL = "resource_level"; // 권한 정보 래벨
	public static final String PARAM_ACCESS_TOKEN = "access_token"; // oauth access token

	public static final String PARAM_SAML_APP_SERVER_URL= "saml_asu"; // app.server.url
	public static final String PARAM_SAML_APP_PROVIDER_NAME= "saml_apn"; // app.provider.name
	public static final String PARAM_SAML_RESPONSE = "saml_response"; // saml response


    // IAMPS 로그인 에서 사용하는 파라미터들.
    public static final String PARAM_LOGIN_PAGE = "login_page"; // 로그인 페이지 구분, IAMPS 로그인 용인지, 공통 로그인 페이지인지 구분
    public static final String PARAM_COMMON_REDIRECT_URL = "redirect_url"; // 공통 로그인 페이지를 사용하여 로그인 성공 시 리다이렉션 되어야 할 업무시스템 URL

    public static final String REDIRECT_URL = "REDIRECT_URL";

    public static final String ATTR_LOGIN_PARAMS = "common_login_params";
    public static final String ATTR_OTP_PARAMS = "common_otp_params";

	// saml의 AUTHN_ID
	public static final String SAML_AUTHN_ID ="AUTHN_ID";


	// 사용자 정보 결과 이름
	public static final String RESULT_USER_INFO ="USER_INFO";
	// 사용자 권한 결과 이름
	public static final String RESULT_AUTH_INFO ="AUTH_INFO";
    // 사용자 롤 결과 이름
    public static final String RESULT_ROLE_INFO ="ROLE_INFO";
    // 위임자 결과 이름
    public static final String RESULT_DELEGATOR_INFO ="DELEGATOR_INFO";

	// oauth token info 결과 이름
	public static final String RESULT_OAUTH_ACCESS_TOKEN ="OAUTH_ACCESS_TOKEN";

	// saml response 결과 이름
	public static final String RESULT_SAML_RESPONSE ="SAML_RESPONSE";

	// 에러시 추가 정보
	public static final String RESULT_ERROR_EXTRA_STRING ="ERROR_EXTRA_STRING";
	public static final String RESULT_ERROR_EXTRA_INT ="ERROR_EXTRA_INT";
	public static final String RESULT_PASSWORD_CHANGE_REQUIRED ="PASSWORD_CHANGE_REQUIRED"; // 비빌번호 변경 필요
	public static final String RESULT_LOGIN_FAIL_COUNT ="LOGIN_FAIL_COUNT"; // 로그인 실패 횟수

    public static final String RESULT_USER_NAME ="USER_NAME"; // 사용자 이름

    public static final String RESULT_IS_LOGIN ="IS_LOGIN"; // 로그인 여부

    public static final String OTP_CHECK = "otp_check";

	@Value("#{applicationProperties['sso.initech.oauth.client.id']}")
	private String oauthClientId;

	@Value("#{applicationProperties['sso.initech.oauth.client.secret']}")
	private String oauthClientSecret;


	@Value("#{applicationProperties['sso.initech.saml.app.server.url']}")
	private String samlAppServerUrl; // saml app url(이 컨트롤러가 있는 서버  url  agent 등록 필요)


	@Value("#{applicationProperties['sso.initech.saml.app.provider.name']}")
	private String samlProviderName = null;

	// saml request를 호출하기 위한 템플릿 path
	@Value("#{applicationProperties['sso.initech.saml.request.template.path']}")
	private String samlReqTempPath = null;

	private String cacheSamlReqTempPath = null;

	// 이니택에서 발행한 공개키 정보 위치
	@Value("#{applicationProperties['sso.initech.saml.pubilc.key.path']}")
	private String samlPubKeyPath = null;

	private String cacheSamlPubKeyPath = null;

    @Autowired
    protected UserService_KAIST userService;

    @Autowired
    protected ClientService_KAIST clientService;

    @Autowired
    protected ResourceService resourceService;

    @Autowired
    protected RoleMasterService roleMasterService ;

    @Resource
    protected CryptService cryptService;

    @Autowired
    protected LoginHistoryService loginHistoryService;

    @Autowired
    protected UserLoginHistoryService userLoginHistoryService;

    @Resource
    protected AuthenticationProvider authenticationProvider;

    @Resource(name="sas")
    protected SessionAuthenticationStrategy sessionStarategy;

    @Autowired
    protected OtpHistoryService otpHistoryService;

    public KaistSSOClientController() {

	}

	/**
	 * @제목 : 로그인 완료 후 아래 메소드 호출 - Response에 Nexess Token 쿠키를 생성한다.
	 * @param id
	 * @param ip
	 * @param response
	 */
	protected void writeNexessToken(String id, String ip, HttpServletRequest request, HttpServletResponse response) {
		ssoLoginManager.writeNexessToken(id, ip, request, response);
	}


	/**
	 * Http Send 시  사용할 cookie 정보를 request 객체에서 가져온다.
	 * @param request
	 * @return
	 */
	protected Map<String, String> getCookieMap(HttpServletRequest request) {
		return ssoLoginManager.getCookieMap(request);
	}

	/**
	 * 쿠키를 추가 한다. JSESSIONID 아이디 제외
	 * @param cookies  (apache http client cookies)
	 * @param isSecure
	 * @param response
	 */
	protected void addCookies(List<org.apache.http.cookie.Cookie> cookies, boolean isSecure, HttpServletResponse response) {
		ssoLoginManager.addCookies(HttpClientCookieUtil.getCookiesFromHttpCookie(cookies), isSecure, response);
	}


	/**
	 * ClientApiException 공통 에러 처리 메소드
	 * @param e
	 * @return
	 */
	protected ResponseEntity<ResponseDTO> processException( ClientApiException  e) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
        return processException( e, resultMap);

	}
	
	/**
	 * ClientApiException 공통 에러 처리 메소드
	 * @param e
	 * @param resultMap
	 * @return
	 */
	protected ResponseEntity<ResponseDTO> processException( ClientApiException  e, Map<String, Object> resultMap) {
		// TODO (NOWONE) 삭제 여부 결졍
        e.printStackTrace();

		if( resultMap == null ) {
			resultMap = new HashMap<String, Object>();
		}
		
		
        if(e.getLoginFailCount() !=null ) {
    		resultMap.put( RESULT_LOGIN_FAIL_COUNT, e.getLoginFailCount());
    	}

        if(e.getPasswordChangeRequired() !=null ) {
        	resultMap.put(RESULT_PASSWORD_CHANGE_REQUIRED, e.getPasswordChangeRequired());
    	}

        ResponseDTO dto = new ResponseErrorDTO(e.getErrorCode(), e.getMessage(), resultMap);
        ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.INTERNAL_SERVER_ERROR);

        return responseEntity;

	}

	/**
	 * BandiException 공통 에러 처리 메소드
	 * @param e
	 * @return
	 */
	protected ResponseEntity<ResponseDTO> processException( BandiException  e) {
        return processException( e, null);

	}

	/**
	 * BandiException 공통 에러 처리 메소드
	 * @param e
	 * @return
	 */
	protected ResponseEntity<ResponseDTO> processException( BandiException  e, Map<String, Object> resultMap) {
		// TODO (NOWONE) 삭제 여부 결졍
        e.printStackTrace();

        if( resultMap == null) {
        	resultMap = new HashMap<String, Object>();
        }
        ResponseDTO dto = new ResponseErrorDTO(e.getErrorCode(), e.getMessage(), resultMap);
        ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.INTERNAL_SERVER_ERROR);

        return responseEntity;

	}

	/**
	 * Exception 공통 에러 처리 메소드
	 * @param e
	 * @return
	 */
	protected ResponseEntity<ResponseDTO> processException( Exception  e) {
		// TODO (NOWONE) 삭제 여부 결졍
        e.printStackTrace();

        ResponseDTO dto = new ResponseErrorDTO(e.getClass().getSimpleName(), e.getMessage());
        ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.INTERNAL_SERVER_ERROR);

        return responseEntity;
	}

	/**
	 * 로그인한 사용자의 아디를 돌려준다.
	 * @param name
	 * @param request
	 * @return
	 */
	protected String getLoginIdWithCookie(HttpServletRequest request) {

		return CookieManager.getCookieValue(SECode.USER_ID, request);
	}

	/**
	 * 로그인한 사용자의 아이디를 돌려준다.
	 *
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="/login/id", method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO> getLoginId(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try{

        	if( !this.isLogin(request, response) ) {
        		// 로그인이 필요합니다.
        	    throw new ClientApiException(ErrorCode_KAIST.SSO_NEED_LOGIN);
        	}

        	Map<String, Object> resultMap = new HashMap<String, Object>();

    		String userId = this.getLoginIdWithCookie(request);

        	resultMap.put(PARAM_USER_ID, userId);

        	ResponseDTO dto = new ResponseDTO(resultMap);
        	ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.OK );

        	return responseEntity;

        } catch ( ClientApiException e) {

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);

        	return responseEntity;
        } catch ( Exception e){

            ResponseEntity<ResponseDTO> responseEntity = processException(e);

            return responseEntity;
        }
    }

	/**
	 * 실제 로그인하지 않고 사용자 아이디와 비밀 번호를 이용해서 로그인 만 한다.
	 *
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="/login/user", method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO> loginUser(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try{
        	String userId = request.getParameter(PARAM_USER_ID);
        	String password = request.getParameter(PARAM_USER_PASSWORD);
        	String remoteIp = request.getParameter(PARAM_REMOTE_IP);

        	// sso는 생성하지 않과 login만
        	String onlyLogin = request.getParameter(PARAM_ONLY_LOGIN);

        	Map<String, Object> resultMap = new HashMap<String, Object>();

        	try{
                clientApiService.login( userId, password, remoteIp );

            } catch( ClientApiException e ){
            	if( ErrorCode_KAIST.SSO_LOGIN_PASSWORD_CHANGE_REQUIRED.equals(e.getErrorCode())) {
            		// 비밀번호 기간 말료 변경 요청
            		resultMap.put(RESULT_PASSWORD_CHANGE_REQUIRED, "T");
            	} else {
            		throw e;
            	}
            }

    		// sso cookie가 필요 없는 경우
    		if( !"T".equals(onlyLogin) ) {
    			// initech sso login cookie 생성
    			writeNexessToken( userId, remoteIp, request, response );
    		}

        	resultMap.put("userId", userId);

        	ResponseDTO dto = new ResponseDTO(resultMap);
        	ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.OK );

        	return responseEntity;

        } catch ( ClientApiException e) {

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);

        	return responseEntity;
        } catch ( Exception e){

            ResponseEntity<ResponseDTO> responseEntity = processException(e);

            return responseEntity;
        }
    }

	/**
	 * 이니켁 SSO 서버에 로그인 한후 이니텍 쿠키를 브라우저에 설정한다.
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="/login", method=RequestMethod.POST )
    public ResponseEntity<ResponseDTO> login(HttpServletRequest request, HttpServletResponse response) throws Exception {

		// initech sso 없이 login  처리 하는 부분
		if( BandiProperties_KAIST.IS_TEST_WITHOUT_INITECH ) {
			return loginWithoutInitech(request, response);
		}

        String loginPage = request.getParameter(PARAM_LOGIN_PAGE);

        // 공통
    	// oauth
        String clientId = request.getParameter(PARAM_OAUTH_CLIENT_ID);
        String clientSecret = request.getParameter(PARAM_OAUTH_CLIENT_SECRET);
        String remoteIp = request.getParameter(PARAM_REMOTE_IP);

        // 권한 관리에서 필요한 파라미터
        String resourceId = request.getParameter(PARAM_AUTH_RESOURCE_ID);
        String level = request.getParameter(PARAM_AUTH_RESOURCE_LEVEL); // 숫자, all

        // 로그인
		String userId = request.getParameter(PARAM_USER_ID);
    	String password = request.getParameter(PARAM_USER_PASSWORD);

    	//  이미 로그인 검사는 완료하고 otp 검사를 하지 않은 경우 (T or F)
    	String alreadyLogin = request.getParameter(PARAM_ALREADY_LOGIN);

        // OTP
        String otpNum = request.getParameter(PARAM_OTP);
        String alreadyOtpCheck = request.getParameter(PARAM_ALREADY_OPT_CHECK);

        // 로그인 이력
        String errorCode = null; // 에러구분용
        String flagSuccess = null;
        String errorMessage = null;

        // OTP 이력
        String certificationResult = null;

        Map< String, Object > resultMap = new HashMap< String, Object >();
        try{

            // 이미 로그인 검사 완료 여부
            boolean isAlreadyLogin = "T".equals(alreadyLogin);

            // 이미 OTP 검사 완료 여부
            boolean isAlreadyOtpCheck = "T".equals(alreadyOtpCheck);

            boolean isInternalLoginPage = BandiConstants_KAIST.LOGIN_PAGE_IAMPS.equals( loginPage ) ||
                                           BandiConstants_KAIST.LOGIN_PAGE_COMMON.equals( loginPage );

            if ( BandiConstants_KAIST.LOGIN_PAGE_COMMON.equals( loginPage )){
                Map<String, String> params = (Map<String, String>)request.getSession().getAttribute(ATTR_LOGIN_PARAMS);

                clientId = params.get( PARAM_OAUTH_CLIENT_ID );
                clientSecret = params.get( PARAM_OAUTH_CLIENT_SECRET );

                resourceId = params.get( PARAM_AUTH_RESOURCE_ID);
                level = params.get( PARAM_AUTH_RESOURCE_LEVEL);

                String redirectUrl = params.get( PARAM_COMMON_REDIRECT_URL );
                resultMap.put( REDIRECT_URL, redirectUrl );
            }

            // ip setting
            if( isInternalLoginPage || remoteIp == null ){
                remoteIp = WebUtils.getRemoteIp( request );
            }

            // 공인인증서 로그인? KAIST-TODO : 하현승 부장이 처리하기로 함.

            // CLIENT 정보 검증 : clientSecret는 plain text로 전달
            Client client = null;
            if( clientId != null && clientSecret != null && BandiConstants_KAIST.LOGIN_PAGE_IAMPS.equals( loginPage ) == false){
                client = authorizeClient( clientId, clientSecret);
            }

            User user = getUser( userId );

            if ( user == null) {
            	//  사용자가 없음
            	if( userId.length() > 30 ) {
            		userId = userId.substring(0, 30);
            	}
            	throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_NOT_REGISTER);
            }

            // IAMPS 로그인 페이지에서 로그인 했을 때만 필요함.
            if ( BandiConstants_KAIST.LOGIN_PAGE_IAMPS.equals( loginPage )){
                resultMap.put( RESULT_USER_NAME, user.getName() );
            }

            if( !isAlreadyLogin ) {
            	// 최초 로그인할 경우(패스워드는 사용자 ID로 업데이트 되어 있는 상태)
	            if( user.getId().equals(user.getPassword())){

	            	KaistOIMLdapUtil ldaputil = new KaistOIMLdapUtil();

                    // LDAP에 사용자가 입력한 ID/PW를 전달하여 인증 여부를 체크.
	            	if(ldaputil.isAuthenticated(userId, password)){
	            		userService.resetInitechPassword(userId, password);

	            		user.setPasswordInitializeFlag(BandiConstants.FLAG_Y);
	                    user.setPassword(cryptService.passwordEncrypt(userId, password));
	            		userService.update(user);
	            	} else {
	            		throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_PASSWORD_INCORRECT);
	            	}
	            }

                try{
                    clientApiService.login( userId, password, remoteIp );
                } catch( ClientApiException e ){

                	if( ErrorCode_KAIST.SSO_LOGIN_PASSWORD_CHANGE_REQUIRED.equals(e.getErrorCode())) {
                		// 비밀번호 기간 만료 변경 요청
                		resultMap.put(RESULT_PASSWORD_CHANGE_REQUIRED, "T");

                		// 로그인은 성공시킨다. 따라서 Exception 던지면 안됨.
                	} else {
                	    throw e;
                    }
                }
            }


            if( !isAlreadyOtpCheck ) {
	            if( otpNum == null &&
                    ( BandiConstants_KAIST.LOGIN_PAGE_IAMPS.equals( loginPage ) || userService.isNeedOTPCheck( userId, clientId ))
                ){

                    // otp 사용 여부 검사하여 사용하면 otp 검사하도록 돌려보내기 위해 Exception 처리
	            	ClientApiException e = new ClientApiException( ErrorCode_KAIST.SSO_OTP_NEED_OTP_CHECK );
	                throw e;
	            }

	            //  otp 번호 검사
	            if( otpNum != null && otpNum.length() > 0 ){
                    clientApiService.checkOtp( userId, otpNum );
                    certificationResult = BandiConstants_KAIST.OTP_HISTORY_RESULT_SUCCESS;
                    logOtpHistory(userId, certificationResult, remoteIp);
	            }
            }

            String infoMarkOptn = null;
            if ( BandiConstants_KAIST.LOGIN_PAGE_IAMPS.equals( loginPage ) == false ){
                if( client != null && client.getInfoMarkOptn() != null ){
                    infoMarkOptn = client.getInfoMarkOptn();
                }

                setUserParameters( user.getId(), infoMarkOptn, resultMap );

                if( BandiConstants.FLAG_Y.equals( client.getFlagUseEam() ) ){
                    setResorucesByUser( user.getId(), clientId, resourceId, level, user.getUserType(), resultMap );

                    setRolesByUser( user.getId(), user.getUserType(), resultMap);
                }
            }

            boolean isSSOLogin = BandiConstants_KAIST.LOGIN_PAGE_IAMPS.equals( loginPage ) ||
                    ( client != null && BandiConstants_KAIST.SSO_TYPE_INTEGRATED_SSO.equals( client.getSsoType()));

        	if( isSSOLogin) {
        		// initech sso login cookie 생성 (마지막에 호출)
        		writeNexessToken( userId, remoteIp, request, response );
        	}

            ResponseDTO dto = new ResponseDTO(resultMap);
            ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.OK );

            if (clientId == null && BandiConstants_KAIST.LOGIN_PAGE_IAMPS.equals( loginPage )) {
				clientId = BandiConstants_KAIST.USER_CLIENT_ID_IAM;
			}

			flagSuccess = BandiConstants_KAIST.USER_LOGIN_SUCCESS;

            return responseEntity;

        } catch ( ClientApiException e) {

        	errorCode = e.getErrorCode();
        	errorMessage = e.getMessage();

        	if (clientId == null && BandiConstants_KAIST.LOGIN_PAGE_IAMPS.equals( loginPage )) {
        		clientId = BandiConstants_KAIST.USER_CLIENT_ID_IAM;
        	}

        	flagSuccess = BandiConstants_KAIST.USER_LOGIN_FAIL;

        	ResponseEntity<ResponseDTO> responseEntity = processException(e, resultMap);
        	return responseEntity;

        } catch ( BandiException e){
        	// TODO client id 없을 경우 처리 해야함
        	errorCode = e.getErrorCode();

        	errorMessage = e.getClass().getSimpleName();

        	if (clientId == null && BandiConstants_KAIST.LOGIN_PAGE_IAMPS.equals( loginPage )) {
        		clientId = BandiConstants_KAIST.USER_CLIENT_ID_IAM;
        	}

        	flagSuccess = BandiConstants_KAIST.USER_LOGIN_FAIL;

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);
            return responseEntity;

        } catch ( Exception e){
        	// TODO client id 없을 경우 처리 해야함
        	//String errorCode = e.getErrorCode();

        	errorMessage = e.getClass().getSimpleName();

        	if (clientId == null && BandiConstants_KAIST.LOGIN_PAGE_IAMPS.equals( loginPage )) {
        		clientId = BandiConstants_KAIST.USER_CLIENT_ID_IAM;
        	}

        	flagSuccess = BandiConstants_KAIST.USER_LOGIN_FAIL;

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);
            return responseEntity;

        } finally {

        	if (!ErrorCode_KAIST.SSO_OTP_NEED_OTP_CHECK.equals(errorCode)) {
        		logUserLoginHistory(userId, flagSuccess, errorMessage, clientId, remoteIp);
        	}

        	if (ErrorCode_KAIST.SSO_OTP_LOCKED.equals(errorCode)) {
        		certificationResult = BandiConstants_KAIST.OTP_HISTORY_RESULT_LOCK;
        		logOtpHistory(userId, certificationResult, remoteIp);
        	} else if (ErrorCode_KAIST.SSO_OTP_FAIL.equals(errorCode)) {
        		certificationResult = BandiConstants_KAIST.OTP_HISTORY_RESULT_FAIL;
        		logOtpHistory(userId, certificationResult, remoteIp);
        	}
        }

	}

	/**
	 * 이니텍 sso 없이 로그인 처리하는 부분
	 * @param request
	 * @param response
	 * @return
	 */
	protected ResponseEntity<ResponseDTO> loginWithoutInitech(HttpServletRequest request, HttpServletResponse response) {

        String loginPage = request.getParameter(PARAM_LOGIN_PAGE);

		// 공통
    	// oauth
        String clientId = request.getParameter(PARAM_OAUTH_CLIENT_ID);
        String clientSecret = request.getParameter(PARAM_OAUTH_CLIENT_SECRET);
        String remoteIp = request.getParameter(PARAM_REMOTE_IP);


        // 권한 관리에서 필요한 파라미터
        String resourceId = request.getParameter(PARAM_AUTH_RESOURCE_ID);
        String level = request.getParameter(PARAM_AUTH_RESOURCE_LEVEL); // 숫자, all

        // 로그인
		String userId = request.getParameter(PARAM_USER_ID);
    	String password = request.getParameter(PARAM_USER_PASSWORD);

        String otpNum = request.getParameter(PARAM_OTP);

        // 로그인 이력
        String errorCode = null;
        String flagSuccess = null;
        String errorMessage = null;

        // OTP 이력
        String certificationResult = null;


        try{
            Map< String, Object > resultMap = new HashMap< String, Object >();

            Map< String, String > userInfoMap = new HashMap<>();
            Map< String, String > authInfoMap = new HashMap<>();

            resultMap.put( RESULT_USER_INFO, userInfoMap );  // 사용자 정보 결과
            resultMap.put( RESULT_AUTH_INFO, authInfoMap );  // 권한 정보 결과

            boolean isInternalLoginPage = BandiConstants_KAIST.LOGIN_PAGE_IAMPS.equals( loginPage ) ||
                                           BandiConstants_KAIST.LOGIN_PAGE_COMMON.equals( loginPage );

            if ( BandiConstants_KAIST.LOGIN_PAGE_COMMON.equals( loginPage )){
                Map<String, String> params = (Map<String, String>)request.getSession().getAttribute(ATTR_LOGIN_PARAMS);

                clientId = params.get( PARAM_OAUTH_CLIENT_ID );
                clientSecret = params.get( PARAM_OAUTH_CLIENT_SECRET );

                resourceId = params.get( PARAM_AUTH_RESOURCE_ID);
                level = params.get( PARAM_AUTH_RESOURCE_LEVEL);

                String redirectUrl = params.get( PARAM_COMMON_REDIRECT_URL );
                resultMap.put( REDIRECT_URL, redirectUrl );
            }

            // ip setting
            if( isInternalLoginPage || remoteIp == null ){
                remoteIp = WebUtils.getRemoteIp( request );
            }

            // CLIENT 정보 검증 : clientSecret는 plain text로 전달
            Client client = null;

            if( clientId != null && clientSecret != null &&
                    BandiConstants_KAIST.LOGIN_PAGE_IAMPS.equals( loginPage ) == false ){
                client = authorizeClient( clientId, clientSecret );
            }

            User user = getUser( userId );
            if (user == null) { // 존재하지 않는 아이디
            	if( userId.length() > 30 ) {
            		userId = userId.substring(0, 30);
            	}
            	throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_NOT_REGISTER);
            }

            resultMap.put( RESULT_USER_NAME, user.getName());

        	// 이미 로그인 완료후 otp check 하지 않은 경우는 제외
            if( user.getId().equals(user.getPassword())){
            	KaistOIMLdapUtil ldaputil = new KaistOIMLdapUtil();

            	if(ldaputil.isAuthenticated(userId, password)){
            		user.setPasswordInitializeFlag(BandiConstants.FLAG_Y);
                    user.setPassword(cryptService.passwordEncrypt(userId, password));

            		userService.update(user);
            	} else {
            		throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_PASSWORD_INCORRECT);
            	}
            }


            // 이니텍 인증에 대한 실패 또는 예외처리 테스트 코드임. 지우지 마세요.
            if (true) {

                // 패스워드 변경 필요
                // resultMap.put(RESULT_PASSWORD_CHANGE_REQUIRED, "T");

                /*
                // 실패횟수 테스트
                ClientApiException e =  new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_PASSWORD_INCORRECT);
                e.setLoginFailCount( "5" );
                resultMap.put(RESULT_LOGIN_FAIL_COUNT, e.getLoginFailCount());
                throw e;
                */
            }

            loginToIEAM( userId, password, resultMap, request, response );

            //  otp 번호 검사
            if( otpNum != null && otpNum.length() > 0 ){
                System.out.println( "otp checked..........................................");
              //  clientApiService.checkOtp( userId, otpNum );
              //  certificationResult = BandiConstants_KAIST.OTP_HISTORY_RESULT_SUCCESS;
              //  logOtpHistory(userId, certificationResult, remoteIp);
            }

            // 테스트 코드이므로 원래 로그인 로직과 상관 없음.
            if( otpNum == null &&
                    ( BandiConstants_KAIST.LOGIN_PAGE_IAMPS.equals( loginPage ) ||
                            BandiConstants_KAIST.LOGIN_PAGE_COMMON.equals( loginPage))) {
                ClientApiException e = new ClientApiException( ErrorCode_KAIST.SSO_OTP_NEED_OTP_CHECK );

                // otp 사용 여부 검사하여 사용하면 otp 검사하도록 돌려보낸다.
                throw e;
            }


            String infoMarkOptn = null;
            if( client != null && client.getInfoMarkOptn() != null ){
                infoMarkOptn = client.getInfoMarkOptn();
            }

            setUserParameters( user.getId(), infoMarkOptn, resultMap );
            setResorucesByUser( user.getId(), clientId, resourceId, level, user.getUserType(), resultMap );
            setRolesByUser( user.getId(), user.getUserType(), resultMap );


            boolean isSSOLogin = BandiConstants_KAIST.LOGIN_PAGE_IAMPS.equals( loginPage ) ||
                   ( client != null && BandiConstants_KAIST.SSO_TYPE_INTEGRATED_SSO.equals( client.getSsoType()));

            // sso  대상일때만 쿠키 생성되도록 수정
            if( isSSOLogin){
                Cookie sampleCookie = new Cookie( "bandi_cookie", userId );
                sampleCookie.setHttpOnly( true );
                sampleCookie.setSecure( false );
                sampleCookie.setPath( "/" );

                response.addCookie( sampleCookie );
            }

            ResponseDTO dto = new ResponseDTO(resultMap);
            ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.OK );

            if (clientId == null && BandiConstants_KAIST.LOGIN_PAGE_IAMPS.equals( loginPage )) {
				clientId = BandiConstants_KAIST.USER_CLIENT_ID_IAM;
			}

			flagSuccess = BandiConstants_KAIST.USER_LOGIN_SUCCESS;

            return responseEntity;

        } catch ( ClientApiException e) {

        	errorCode = e.getErrorCode();
        	errorMessage = e.getMessage();

        	if (clientId == null && BandiConstants_KAIST.LOGIN_PAGE_IAMPS.equals( loginPage )) {
        		clientId = BandiConstants_KAIST.USER_CLIENT_ID_IAM;
        	}

        	flagSuccess = BandiConstants_KAIST.USER_LOGIN_FAIL;

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);
        	return responseEntity;

        } catch ( Exception e){

        	errorMessage = e.getClass().getSimpleName();

        	if (clientId == null && BandiConstants_KAIST.LOGIN_PAGE_IAMPS.equals( loginPage )) {
        		clientId = BandiConstants_KAIST.USER_CLIENT_ID_IAM;
        	}

        	flagSuccess = BandiConstants_KAIST.USER_LOGIN_FAIL;

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);
            return responseEntity;

        } finally {

        	if (!ErrorCode_KAIST.SSO_OTP_NEED_OTP_CHECK.equals(errorCode)) {
        		logUserLoginHistory(userId, flagSuccess, errorMessage, clientId, remoteIp);
        	}

        	if (ErrorCode_KAIST.SSO_OTP_LOCKED.equals(errorCode)) {
        		certificationResult = BandiConstants_KAIST.OTP_HISTORY_RESULT_LOCK;
        		logOtpHistory(userId, certificationResult, remoteIp);
        	} else if (ErrorCode_KAIST.SSO_OTP_FAIL.equals(errorCode)) {
        		certificationResult = BandiConstants_KAIST.OTP_HISTORY_RESULT_FAIL;
        		logOtpHistory(userId, certificationResult, remoteIp);
        	}
        }
	}

	/**
     *
	 * 이니텍 SSO 서버에 이미 로그인 했는지 검사( 쿠기 개수 조사 및 validate 검사)
	 * @param request
	 * @return
	 */
	protected boolean isLogin(HttpServletRequest request, HttpServletResponse response) {
		return ssoLoginManager.isLogin(request, response);
	}

	/**
	 * 로그인 여부 검사
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/login/check")
    public ResponseEntity<ResponseDTO> checkLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try{

    		boolean bolLogin = this.isLogin(request, response);

			if( !bolLogin ) {
				// 로그인이 필요합니다.
        	    throw new ClientApiException(ErrorCode_KAIST.SSO_NEED_LOGIN);
			}

    		Map<String, Object> resultMap = new HashMap<>();
    		resultMap.put(RESULT_IS_LOGIN, bolLogin);

        	ResponseDTO dto = new ResponseDTO(resultMap);
        	ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.OK );

        	return responseEntity;

        } catch ( ClientApiException e) {

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);
        	return responseEntity;

        } catch ( Exception e){

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);
            return responseEntity;

        }
    }

	/**
	 * Oauth 토큰과 사용자 정보를 가져온다.
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/oauth/token", method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO> getOauthToken(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try{

        	Map<String, Object> resultMap = new HashMap<>();

    		Map<String, String> cookieMap = null;

    		if(this.isLogin(request, response)) {
    			cookieMap = this.getCookieMap(request);
    		} else {
    			// 로그인 안되어 있으면 로그인
    			ResponseEntity<ResponseDTO> loginResEntity = this.loginUser(request, response);

    			//로그인 에러
    			if( loginResEntity.getBody() instanceof ResponseErrorDTO ) {
    				return loginResEntity;
    			}

    			// 로그인 정보  (예:로그인 중  비밀번호 만료  변경 필요 등등 )
    			Map<String, Object> loginResultMap = loginResEntity.getBody().getDataMap();
    			if( loginResultMap != null ) {
	    			for( String key : loginResultMap.keySet() ){
	    				resultMap.put(key, loginResultMap.get(key));
	    	        }
    			}

    			cookieMap = new HashMap<>();
        		Collection<String> setCookies = response.getHeaders("Set-Cookie");
        		if( setCookies != null ) {
        			for(String strCookie: setCookies) {
        				List<HttpCookie> httpCookies =  HttpCookie.parse(strCookie);
        				for (HttpCookie httpCookie : httpCookies) {
        					String name = httpCookie.getName().toLowerCase();
        					if( name.startsWith("initech") ) { // initech 쿠키만 전송
        						cookieMap.put(httpCookie.getName(), httpCookie.getValue());
        					}

    				    }
        			}
        		}
    		}


    		// app의 clientid EAM의 권한 가져올때 사용 실제 oauth에는 사용하지 않는다.
    		String clientId = request.getParameter(PARAM_OAUTH_CLIENT_ID);
    		String clientSecret = request.getParameter(PARAM_OAUTH_CLIENT_SECRET);

    		// String ssoOauthClientId = this.oauthClientId;
    		//String ssoOauthClientSecret = this.oauthClientSecret;

        	HttpConnService.Result authResult = clientApiService.getAuthCode(clientId, cookieMap);
            String authCode =  (String)authResult.getData();

            HttpConnService.Result tokenResult = clientApiService.getAccessToken(clientId, clientSecret, authCode);

        	this.addCookies(authResult.getCookies(), request.isSecure(), response);
        	this.addCookies(tokenResult.getCookies(), request.isSecure(), response);



    		Object objTokenMap = tokenResult.getData();

    		resultMap.put(RESULT_OAUTH_ACCESS_TOKEN, objTokenMap);

        	ResponseDTO dto = new ResponseDTO(resultMap);
        	ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.OK );

        	return responseEntity;

        } catch ( ClientApiException e) {

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);
        	return responseEntity;

        } catch ( Exception e){

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);
            return responseEntity;

        }
    }

	/**
	 * get post 받은 access token 문자열 중에 + 자동으로 공백으로 변경된다.
	 * 다시 공백을 + 문자로 변경 해야함
	 * @param accessToken
	 * @return
	 */
	protected String processBase64Token(String token) {
		if( token == null) {
			return null;
		}
    		// access token의  + 문자가 자동으로 공백을 치완된다.
		return token.replaceAll(" ", "+");
	}

	/**
	 * Oauth access token이 살아 있는지 검사한다.
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/check/oauth/token")
    public ResponseEntity<ResponseDTO> checkOauthToken(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try{

        	String accessToken = request.getParameter(PARAM_ACCESS_TOKEN);

        	if( accessToken == null) {
        		// token이 없을 경우
        		throw new ClientApiException( ErrorCode_KAIST.SSO_OAUTH_ACCESS_TOKEN_NOT_FOUND );
        	}


    		// access token의  + 문자가 자동으로 공백을 치완된다.
    		accessToken = processBase64Token( accessToken);

        	// access_token 검사
        	clientApiService.checkAccessToken(accessToken);

        	ResponseDTO dto = new ResponseDTO();
        	ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.OK );

        	return responseEntity;

        } catch ( ClientApiException e) {

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);
        	return responseEntity;

        } catch ( Exception e){

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);
            return responseEntity;

        }
    }

	/**
	 * 로그 아웃 처리한다
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/logout")
    public ResponseEntity<ResponseDTO> logout(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try{
            if( BandiProperties_KAIST.IS_TEST_WITHOUT_INITECH ) {
                Cookie[] cookies = request.getCookies();
                if(cookies != null){
                    for(Cookie cookie: cookies){
                        String name = cookie.getName(); // 쿠키 이름 가져오기

                        if( "bandi_cookie".equals( name)){
                            cookie.setMaxAge( 0 );
                            cookie.setPath("/");
                            response.addCookie( cookie );
                        }
                    }
                }
            } else{
            	ssoLoginManager.logout(request, response);
            }

        	ResponseDTO dto = new ResponseDTO();
        	ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.OK );

			return responseEntity;

        } catch ( ClientApiException e) {

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);
        	return responseEntity;

        } catch ( Exception e){

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);
            return responseEntity;

        }
    }

	// SAML
	/**
	 * request template 절대 경로
	 * @return
	 * @throws FileNotFoundException
	 */
	protected String getSamlRequestTemplatePath() throws FileNotFoundException {

		if( this.cacheSamlReqTempPath == null ) {
			File file = ResourceUtils.getFile(this.samlReqTempPath);

			this.cacheSamlReqTempPath = file.getAbsolutePath();
		}

		return this.cacheSamlReqTempPath;
	}

	/**
	 * saml public key path
	 * @return
	 * @throws FileNotFoundException
	 */
	protected String getSamlPublicKeyPath() throws FileNotFoundException {

		if( this.cacheSamlPubKeyPath == null ) {
			File file = ResourceUtils.getFile(this.samlPubKeyPath);

			this.cacheSamlPubKeyPath = file.getAbsolutePath();
		}

		return this.cacheSamlPubKeyPath;
	}

	/**
	 * Saml 토큰과 사용자 정보를 가져온다.
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/saml/token", method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO> getSmalToken(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try{
        	Map<String, Object> resultMap = new HashMap<>();

    		Map<String, String> cookieMap = null;

    		if(this.isLogin(request, response)) {
    			// 이미 로그인 했으면
    			cookieMap = this.getCookieMap(request);
    		} else {

    			// 로그인 안되어 있으면 로그인
    			ResponseEntity<ResponseDTO> loginResEntity = this.loginUser(request, response);

    			//로그인 에러
    			if( loginResEntity.getBody() instanceof ResponseErrorDTO ) {
    				return loginResEntity;
    			}

    			// 로그인 정보  (예:로그인 중  비밀번호 만료  변경 필요 등등 )
    			Map<String, Object> loginResultMap = loginResEntity.getBody().getDataMap();
    			if( loginResultMap != null ) {
	    			for( String key : loginResultMap.keySet() ){
	    				resultMap.put(key, loginResultMap.get(key));
	    	        }
    			}

    			cookieMap = new HashMap<>();
        		Collection<String> setCookies = response.getHeaders("Set-Cookie");
        		if( setCookies != null ) {
        			for(String strCookie: setCookies) {
        				List<HttpCookie> httpCookies =  HttpCookie.parse(strCookie);
        				for (HttpCookie httpCookie : httpCookies) {
        					cookieMap.put(httpCookie.getName(), httpCookie.getValue());
    				    }
        			}
        		}
    		}

    		String authnId = cookieMap.get(SAML_AUTHN_ID);

    		if( authnId == null ) {
    			authnId = Util.createID();

    			cookieMap.put(SAML_AUTHN_ID, authnId);

    			ssoLoginManager.addCookie(SAML_AUTHN_ID, authnId, response);
    		}


    		String acsUrl = this.samlAppServerUrl; // 상수값으로 필요 없음
    		String relayStateUrl = request.getParameter(PARAM_SAML_APP_SERVER_URL); // this.samlAppServerUrl
    		String providerName = request.getParameter(PARAM_SAML_APP_PROVIDER_NAME); //this.samlProviderName;

    		String samlReqTempPath = this.getSamlRequestTemplatePath();

    		String url = clientApiService.getSamlIdentityProveUrl(authnId, samlReqTempPath, providerName, acsUrl, relayStateUrl);

    		HttpConnService.Result tokenResult  = clientApiService.getSamlResponse(url, cookieMap);

    		this.addCookies(tokenResult.getCookies(), request.isSecure(), response);

    		String samlResponse = (String)tokenResult.getData();

    		resultMap.put(RESULT_SAML_RESPONSE, samlResponse);
    		ResponseDTO dto = new ResponseDTO(resultMap);
    		ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.OK );

			return responseEntity;

        } catch ( ClientApiException e) {

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);
        	return responseEntity;

        } catch ( Exception e){

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);
            return responseEntity;

        }
    }

	/**
	 * Saml Response이 살아 있는지 검사한다.
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/check/saml/token")
    public ResponseEntity<ResponseDTO> checkSamlToken(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try{

        	String appServerUrl = this.samlAppServerUrl;
        	String samlResponse = request.getParameter(PARAM_SAML_RESPONSE);
        	String authnId = request.getParameter(SAML_AUTHN_ID);

        	Map<String, String> cookieMap = this.getCookieMap(request);

        	if( samlResponse == null) {
        		samlResponse = cookieMap.get(PARAM_SAML_RESPONSE);
        	}

        	if( authnId == null) {
        		authnId = cookieMap.get(SAML_AUTHN_ID);
        	}

        	if( samlResponse == null) {
        		// TODO (NOWONE) 에러 처리
        	}

        	String publicKeyPath = this.getSamlPublicKeyPath();
        	clientApiService.checkSAMLToken(appServerUrl, authnId, samlResponse, publicKeyPath);

        	ResponseDTO dto = new ResponseDTO();
        	ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.OK );
        	return responseEntity;

        } catch ( ClientApiException e) {

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);
        	return responseEntity;

        } catch ( Exception e){

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);
            return responseEntity;

        }
    }

	/**
	 * access_token을 이용해서 사용자 정보를 가져온다.
	 *
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="/user/info", method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO> getUserInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try{

        	if( !this.isLogin(request, response) ) {
        		// 로그인이 필요합니다.
        	    throw new ClientApiException(ErrorCode_KAIST.SSO_NEED_LOGIN);
        	}

        	String userId = this.getLoginIdWithCookie(request);

        	Map<String, Object> resultMap = new HashMap<String, Object>();

            String clientId = request.getParameter(PARAM_OAUTH_CLIENT_ID);
            String clientSecret = request.getParameter(PARAM_OAUTH_CLIENT_SECRET);

            Client client = null;
            if( clientId != null && clientSecret != null){
                client = authorizeClient( clientId, clientSecret);
            }

            String accessToken = request.getParameter(PARAM_ACCESS_TOKEN);
            if( accessToken != null ) {
            	// access token을 보내주면 검사 한다.
            	// access token의  + 문자가 자동으로 공백을 치완된다.
            	accessToken = processBase64Token( accessToken);

            	// access token 검사와 사용자 정보 조회를 동시에 한다.
            	Map<String, String> ssoUserInfoMap = clientApiService.checkAccessTokenAndUser(accessToken);
            	userId = ssoUserInfoMap.get("id");
            }

            User user = getUser( userId );

            if ( user == null) {
            	//  사용자가 없음
            	throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_NOT_REGISTER);
            }

            String infoMarkOptn = null;
            if( client != null && client.getInfoMarkOptn() != null){
                infoMarkOptn = client.getInfoMarkOptn();
            }
            setUserParameters( user.getId(), client.getInfoMarkOptn(), resultMap );

        	ResponseDTO dto = new ResponseDTO(resultMap);
        	ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.OK );

        	return responseEntity;

        } catch ( ClientApiException e) {

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);

        	return responseEntity;
        } catch ( Exception e){

            ResponseEntity<ResponseDTO> responseEntity = processException(e);

            return responseEntity;
        }
    }

	/**
	 * access_token을 이용해서 사용자 권한를 가져온다.
	 *
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="/auth/info", method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO> getAuthInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try{

        	if( !this.isLogin(request, response) ) {
        		// 로그인이 필요합니다.
        	    throw new ClientApiException(ErrorCode_KAIST.SSO_NEED_LOGIN);
        	}

        	String userId = this.getLoginIdWithCookie(request);

        	Map<String, Object> resultMap = new HashMap<String, Object>();

        	// 권한 정보 요청
            String resourceId = request.getParameter(PARAM_AUTH_RESOURCE_ID);
            String level = request.getParameter(PARAM_AUTH_RESOURCE_LEVEL); // 숫자, all
            String clientId = request.getParameter(PARAM_OAUTH_CLIENT_ID);
            String clientSecret = request.getParameter(PARAM_OAUTH_CLIENT_SECRET);

            Client client = null;
            if( clientId != null && clientSecret != null){
                client = authorizeClient( clientId, clientSecret);
            }

            String accessToken = request.getParameter(PARAM_ACCESS_TOKEN);
            if( accessToken != null ) {
            	// access token을 보내주면 검사 한다.
            	// access token의  + 문자가 자동으로 공백을 치완된다.
            	accessToken = processBase64Token( accessToken);

            	// access token 검사와 사용자 정보 조회를 동시에 한다.
        		Map<String, String> ssoUserInfoMap = clientApiService.checkAccessTokenAndUser(accessToken);
        		userId = ssoUserInfoMap.get("id");

            }

            User user = getUser( userId );

            if ( user == null) {
            	//  사용자가 없음
            	throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_NOT_REGISTER);
            }

            setResorucesByUser( user.getId(), clientId, resourceId, level, user.getUserType(), resultMap );

        	ResponseDTO dto = new ResponseDTO(resultMap);
        	ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.OK );

        	return responseEntity;

        } catch ( ClientApiException e) {

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);

        	return responseEntity;
        } catch ( Exception e){

            ResponseEntity<ResponseDTO> responseEntity = processException(e);

            return responseEntity;
        }
    }

    /**
     * access_token을 이용해서 사용자 롤을 가져온다.
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value="/role/info", method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO> getRoleInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try{

            if( !this.isLogin(request, response) ) {
                // 로그인이 필요합니다.
                throw new ClientApiException(ErrorCode_KAIST.SSO_NEED_LOGIN);
            }

            String userId = this.getLoginIdWithCookie(request);

            Map<String, Object> resultMap = new HashMap<String, Object>();

            String clientId = request.getParameter(PARAM_OAUTH_CLIENT_ID);
            String clientSecret = request.getParameter(PARAM_OAUTH_CLIENT_SECRET);

            Client client = null;
            if( clientId != null && clientSecret != null){
                client = authorizeClient( clientId, clientSecret);
            }

            String accessToken = request.getParameter(PARAM_ACCESS_TOKEN);
            if( accessToken != null ) {
                // access token을 보내주면 검사 한다.
                // access token의  + 문자가 자동으로 공백을 치완된다.
                accessToken = processBase64Token( accessToken);

                // access token 검사와 사용자 정보 조회를 동시에 한다.
                Map<String, String> ssoUserInfoMap = clientApiService.checkAccessTokenAndUser(accessToken);
                userId = ssoUserInfoMap.get("id");

            }

            User user = getUser( userId );

            if ( user == null) {
                //  사용자가 없음
                throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_NOT_REGISTER);
            }

            if( BandiConstants.FLAG_Y.equals( client.getFlagUseEam())){
                setRolesByUser( user.getId(), user.getUserType(), resultMap );
            }

            ResponseDTO dto = new ResponseDTO(resultMap);
            ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.OK );

            return responseEntity;

        } catch ( ClientApiException e) {

            ResponseEntity<ResponseDTO> responseEntity = processException(e);

            return responseEntity;
        } catch ( Exception e){

            ResponseEntity<ResponseDTO> responseEntity = processException(e);

            return responseEntity;
        }
    }




    /**
	 * access_token을 이용해서 사용자 권한를 가져온다.
	 *
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="/user/auth/info", method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO> getUserAndAuthInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try{

        	if( !this.isLogin(request, response) ) {
        		// 로그인이 필요합니다.
        	    throw new ClientApiException(ErrorCode_KAIST.SSO_NEED_LOGIN);
        	}

        	String userId = this.getLoginIdWithCookie(request);

        	Map<String, Object> resultMap = new HashMap<String, Object>();

        	// 권한 정보 요청
            String resourceId = request.getParameter(PARAM_AUTH_RESOURCE_ID);
            String level = request.getParameter(PARAM_AUTH_RESOURCE_LEVEL); // 숫자, all
            String clientId = request.getParameter(PARAM_OAUTH_CLIENT_ID);
            String clientSecret = request.getParameter(PARAM_OAUTH_CLIENT_SECRET);

            Client client = null;
            if( clientId != null && clientSecret != null){
                client = authorizeClient( clientId, clientSecret);
            }

            String accessToken = request.getParameter(PARAM_ACCESS_TOKEN);
            if( accessToken != null ) {
            	// access token을 보내주면 검사 한다.
            	// access token의  + 문자가 자동으로 공백을 치완된다.
            	accessToken = processBase64Token( accessToken);

            	// access token 검사와 사용자 정보 조회를 동시에 한다.

            	Map<String, String> ssoUserInfoMap = clientApiService.checkAccessTokenAndUser(accessToken);
            	userId = ssoUserInfoMap.get("id");

            }

            User user = getUser( userId );

            if ( user == null) {
            	//  사용자가 없음
            	throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_NOT_REGISTER);
            }

            String infoMarkOptn = null;
            if( client != null && client.getInfoMarkOptn() != null){
                infoMarkOptn = client.getInfoMarkOptn();
            }
            setUserParameters( user.getId(), client.getInfoMarkOptn(), resultMap );

            if( BandiConstants.FLAG_Y.equals( client.getFlagUseEam())){
                setResorucesByUser( user.getId(), clientId, resourceId, level, user.getUserType(), resultMap );

                setRolesByUser( user.getId(), user.getUserType(), resultMap);
            }

        	ResponseDTO dto = new ResponseDTO(resultMap);
        	ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.OK );

        	return responseEntity;

        } catch ( ClientApiException e) {

        	ResponseEntity<ResponseDTO> responseEntity = processException(e);

        	return responseEntity;
        } catch ( Exception e){

            ResponseEntity<ResponseDTO> responseEntity = processException(e);

            return responseEntity;
        }
    }

    protected Client authorizeClient( String clientId, String clientSecret) {

        Client client = clientService.authorizeClient( clientId, clientSecret );

        if (client == null){
            throw new ClientApiException(ErrorCode_KAIST.CLIENT_NOT_EXISTED);
        }

        return client;
    }

    protected User getUser( String userId) {
        User user = userService.get( userId );

        return user;
    }

    protected void loginToIEAM( String userId, String password, Map<String, Object> resultMap, HttpServletRequest request, HttpServletResponse response) {

        UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(userId, password);

        // 사용자 정보 검증, 비밀번호 실패횟수, 잠김시간 처리 (패스워드 실패 횟수는 UserServiceImpl_KAIST.authenticateUser()에서 처리됨)
        Authentication authResult = authenticationProvider.authenticate(authReq);

        // 아래 메쏘드 수행 시 아래 두개의 클래스의 해당 메쏘드가 순차적으로 호출됨. 세션 처리는 내부적으로 수행됨.
        // BandiConcurrentSessionControlAuthenticationStrategy.onAuthentication() --> 중복로그인 처리
        // BandiRegisterSessionAuthenticationStrategy.onAuthentication() --> 세션만료시간 처리
        // 마지막으로, BandiSessionRegistryImpl.registerNewSession() 가 호출되어 세션이 생성됨.
        sessionStarategy.onAuthentication(authResult, request, response);


        if ( ContextUtil.get( BandiConstants.ATTR_IS_NEED_PASSSWORD_CHANGE ) != null){
            resultMap.put( "isNeedPasswordChange", ContextUtil.get( BandiConstants.ATTR_IS_NEED_PASSSWORD_CHANGE ) );
        }
    }

    protected void setUserParameters( String userId, String parameterKeys,  Map<String, Object> resultMap) {
        if( BandiProperties_KAIST.IS_TEST_WITHOUT_INITECH ) {

            if(parameterKeys == null) {
                parameterKeys = "ku_name";
            }

            Map< String, String > parameters = userService.getUserParameters( userId, parameterKeys);

            resultMap.put( RESULT_USER_INFO, parameters );
        } else {
            Map< String, String > parameters = userService.getUserParameters( userId, parameterKeys);

            if( parameters != null && parameters.isEmpty() == false){
                resultMap.put( RESULT_USER_INFO, parameters );
            } else {
                resultMap.put( RESULT_USER_INFO, "N/A" );
            }
        }
    }

    protected void setResorucesByUser( String userId, String clientId, String resourceId, String level, String userType, Map<String, Object> resultMap) {

        List<String> userTypes = null;
	    if( userType != null){
            String[] arrTemp = userType.split( "" );
            userTypes = new ArrayList<String>(Arrays.asList(arrTemp));
        }

        List<String> resourceOids = new ArrayList<>();

        if ( resourceId != null & level != null){
            resourceOids = resourceService.getResourceByUser( userId, clientId, resourceId, level, userTypes, true );
        }

        resultMap.put( RESULT_AUTH_INFO, resourceOids );
    }


    protected void setRolesByUser( String userId, String userType, Map<String, Object> resultMap) {

        List<String> userTypes = null;
        if( userType != null){
            String[] arrTemp = userType.split( "" );
            userTypes = new ArrayList<String>(Arrays.asList(arrTemp));
        }

        List<String> roleMasterOids = new ArrayList<>();

        if ( userId != null){
            roleMasterOids = roleMasterService.getRoleByUser( userId, userTypes);
        }

        resultMap.put( RESULT_ROLE_INFO, roleMasterOids );
    }

    @RequestMapping(value="/commonLogin", method = RequestMethod.GET, produces="application/json")
    public void commonLoginGet( HttpServletRequest request, HttpServletResponse response ){
        commonLogin( request, response);
    }

    @RequestMapping(value="/commonLogin", method = RequestMethod.POST, produces="application/json")
    public void commonLogin( HttpServletRequest request, HttpServletResponse response ){

        String clientId = request.getParameter( PARAM_OAUTH_CLIENT_ID );
        String clientSecret = request.getParameter( PARAM_OAUTH_CLIENT_SECRET );
        String recirectUrl = request.getParameter( PARAM_COMMON_REDIRECT_URL);

        String resourceId = request.getParameter(PARAM_AUTH_RESOURCE_ID);
        String level = request.getParameter(PARAM_AUTH_RESOURCE_LEVEL); // 숫자, all

        if( clientId == null || clientId.trim().length() == 0 ||
            clientSecret == null || clientSecret.trim().length() == 0 ||
            recirectUrl == null || recirectUrl.trim().length() == 0
        ) {
            throw new BandiException( ErrorCode_KAIST.PARAMETER_IS_NULL );
        }


        Map<String, String> commonLoginParams = new HashMap<>();

        commonLoginParams.put( PARAM_OAUTH_CLIENT_ID, clientId );
        commonLoginParams.put( PARAM_OAUTH_CLIENT_SECRET, clientSecret );
        commonLoginParams.put( PARAM_COMMON_REDIRECT_URL, recirectUrl );
        commonLoginParams.put( PARAM_AUTH_RESOURCE_ID, resourceId );
        commonLoginParams.put( PARAM_AUTH_RESOURCE_LEVEL, level );

        try{
            request.getSession().setAttribute( ATTR_LOGIN_PARAMS, commonLoginParams );

            // 공통 로그인 페이지로 이동
            response.sendRedirect( "/#/commonLogin");
        }catch( Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value="/isAlreadyLogined", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Map<String, String>> isAlreadyLogined( HttpServletRequest request, HttpServletResponse response ){
        Cookie[] cookies = request.getCookies();

        String userId = null;
        boolean isAlreadyLogined = false;

        Map<String, String> resultMap = new HashMap<>();

        if ( BandiProperties_KAIST.IS_TEST_WITHOUT_INITECH == false ) {
            isAlreadyLogined = this.isLogin( request, response);

            userId = getLoginIdWithCookie( request);
        } else {
            // 테스트 모드일 경우

            if( cookies != null){
                for( Cookie c : cookies ){
                    String name = c.getName(); // 쿠키 이름 가져오기

                    if( "bandi_cookie".equals( name ) ){
                        isAlreadyLogined = true;
                        userId = c.getValue();
                        break;
                    }
                }
            }
        }

        if (isAlreadyLogined){
            User user = userService.get( userId );
            resultMap.put( "userId", userId );
            resultMap.put( "userName", user.getName() );
        }

        resultMap.put( "isAlreadyLogined", String.valueOf(isAlreadyLogined));

        return new ResponseEntity<Map<String, String>>( resultMap, HttpStatus.OK);
    }

    @RequestMapping(value="/moveOTPCommon", method = RequestMethod.POST, produces="application/json")
    public void moveOTPCommonPost( HttpServletRequest request, HttpServletResponse response ){
	    moveOTPCommon( request, response );
    }

    /**
     * Method : GET
     * GET만 이용하는 이유는 업무시스템에서 API로 /moveOTPCommon 의 페이지 전환을 할 경우, Redirect 처리가 GET밖에 안되기 때문임.
     * @param request
     * @param response
     */
    @RequestMapping(value="/moveOTPCommon", method = RequestMethod.GET, produces="application/json")
    public void moveOTPCommon( HttpServletRequest request, HttpServletResponse response ){
        String recirectUrl = request.getParameter( PARAM_COMMON_REDIRECT_URL);
        String userId = request.getParameter(PARAM_USER_ID);
        String clientId = request.getParameter(PARAM_OAUTH_CLIENT_ID);
        String resourceId = request.getParameter(PARAM_AUTH_RESOURCE_ID);
        String level = request.getParameter(PARAM_AUTH_RESOURCE_LEVEL); // 숫자, all

        if( recirectUrl == null || recirectUrl.trim().length() == 0 ||
            userId == null || userId.trim().length() == 0
            ) {

            throw new BandiException( ErrorCode_KAIST.PARAMETER_IS_NULL );
        }

        try{
            Map<String, String> optParams = new HashMap<>();

            optParams.put( PARAM_COMMON_REDIRECT_URL, recirectUrl );
            optParams.put( PARAM_USER_ID, userId );
            optParams.put( PARAM_OAUTH_CLIENT_ID, clientId );
            optParams.put( PARAM_AUTH_RESOURCE_ID, resourceId );
            optParams.put( PARAM_AUTH_RESOURCE_LEVEL, level );

            request.getSession().setAttribute( ATTR_OTP_PARAMS, optParams );

            // 공통 OTP CHECK 페이지로 이동
            response.sendRedirect( "/#/commonCheckOTP");

        }catch( Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * OTP 체크만 수행. 수행의 결과를 담아서 리턴
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value="/commonCheckOtp", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<ResponseDTO> commonOtpCheck( HttpServletRequest request, HttpServletResponse response ){

        String userId = null;
        String recirectUrl = null;
        String clientId = null;
        String resourceId = null;
        String level = null;

        String remoteIp = null;

        // 로그인 이력
        String flagSuccess = null;
        String errorMessage = null;

        // OTP 이력
        String certificationResult = null;
        String errorCode = null;

        try {
            Map< String, Object > resultMap = new HashMap< String, Object >();

            String otpNum = request.getParameter( PARAM_OTP );

            // ip setting
            if( remoteIp == null ){
                remoteIp = WebUtils.getRemoteIp( request );
            }

            Map< String, String > params = ( Map< String, String > ) request.getSession().getAttribute( ATTR_OTP_PARAMS );

            if( params != null && otpNum != null ){
                userId = params.get( PARAM_USER_ID );
                recirectUrl = params.get( PARAM_COMMON_REDIRECT_URL );
                clientId = params.get( PARAM_OAUTH_CLIENT_ID);
                resourceId = params.get( PARAM_AUTH_RESOURCE_ID);
                level = params.get( PARAM_AUTH_RESOURCE_LEVEL);

                Client client = clientService.get( clientId );

                if (client == null){
                    throw new ClientApiException(ErrorCode_KAIST.CLIENT_NOT_EXISTED);
                }

                boolean isSuccess = false;
                if( BandiProperties_KAIST.IS_TEST_WITHOUT_INITECH ){
                    isSuccess = true;
                } else {
                    isSuccess = clientApiService.checkOtp( userId, otpNum );
                    certificationResult = BandiConstants_KAIST.OTP_HISTORY_RESULT_SUCCESS;
                    logOtpHistory(userId, certificationResult, remoteIp);
                }

                resultMap.put( OTP_CHECK, isSuccess ? "T" : "F");

                if( isSuccess){
                    resultMap.put( PARAM_USER_ID, userId );
                    resultMap.put( REDIRECT_URL, recirectUrl );

                    if( client != null){
                        if( BandiConstants_KAIST.SSO_TYPE_INTEGRATED_SSO.equals( client.getSsoType() ) ){

                            // initech sso login cookie 생성 (마지막에 호출)
                            writeNexessToken( userId, remoteIp, request, response );
                        }
                    }

                    // 사용자 정보 리턴
                    String infoMarkOptn = null;
                    if( client != null && client.getInfoMarkOptn() != null){
                        infoMarkOptn = client.getInfoMarkOptn();
                    }

                    User user = getUser( userId );
                    setUserParameters( userId, infoMarkOptn, resultMap );

                    // 권한 정보 리턴
                    if( BandiConstants.FLAG_Y.equals( client.getFlagUseEam())){

                        setResorucesByUser( user.getId(), clientId, resourceId, level, user.getUserType(), resultMap );

                        setRolesByUser( user.getId(), user.getUserType(), resultMap );
                    }
                }

                flagSuccess = BandiConstants_KAIST.USER_LOGIN_SUCCESS;

                ResponseDTO dto = new ResponseDTO(resultMap);
                ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.OK );

                return responseEntity;

            } else{
                // KAIST_TODO : 김종국 : 파라미터 이름이라도 보이게, exception 처리
                throw new ClientApiException( ErrorCode_KAIST.PARAMETER_IS_NULL );
            }

        } catch (ClientApiException e) {
        	errorCode = e.getErrorCode();
        	errorMessage = e.getMessage();
        	flagSuccess = BandiConstants_KAIST.USER_LOGIN_FAIL;

            ResponseEntity<ResponseDTO> responseEntity = processException(e);
            return responseEntity;
        } catch (Exception e) {
        	errorMessage = e.getClass().getSimpleName();
        	flagSuccess = BandiConstants_KAIST.USER_LOGIN_FAIL;

            ResponseEntity<ResponseDTO> responseEntity = processException(e);
            return responseEntity;
        }

        finally {

        	logUserLoginHistory(userId, flagSuccess, errorMessage, clientId, remoteIp);

        	if (ErrorCode_KAIST.SSO_OTP_LOCKED.equals(errorCode)) {
        		certificationResult = BandiConstants_KAIST.OTP_HISTORY_RESULT_LOCK;
        		logOtpHistory(userId, certificationResult, remoteIp);
        	} else if (ErrorCode_KAIST.SSO_OTP_FAIL.equals(errorCode)) {
        		certificationResult = BandiConstants_KAIST.OTP_HISTORY_RESULT_FAIL;
        		logOtpHistory(userId, certificationResult, remoteIp);
        	}
        }
    }

    protected void logUserLoginHistory(String userId, String flagSuccess, String errorMessage, String clientId, String remoteIp) {
    	try {
	    	UserLoginHistory userLoginHistory = new UserLoginHistory();

	    	userLoginHistory.setUserId(userId);
	    	userLoginHistory.setFlagSuccess(flagSuccess);
	    	userLoginHistory.setErrorMessage(errorMessage);
	    	userLoginHistory.setClientOid(clientId);
	    	userLoginHistory.setLoginAt(DateUtil.getNow());
	    	userLoginHistory.setLoginIp(remoteIp);

	    	userLoginHistoryService.insert(userLoginHistory);
	    	} catch( Exception e){
	            logger.error( "USER LOGIN WRITE HISTORY", e );
	    	}
    }

    /**
     * 리소스에 대한 위임자 정보를 조회
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value="/delegator/info", method=RequestMethod.POST)
    public ResponseEntity<ResponseDTO> getDelegatorInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {

        try{

            if( !this.isLogin(request, response) ) {
                // 로그인이 필요합니다.
                throw new ClientApiException(ErrorCode_KAIST.SSO_NEED_LOGIN);
            }

            String userId = this.getLoginIdWithCookie(request);

            Map<String, Object> resultMap = new HashMap<String, Object>();

            // 권한 정보 요청
            String resourceId = request.getParameter(PARAM_AUTH_RESOURCE_ID);
            String clientId = request.getParameter(PARAM_OAUTH_CLIENT_ID);
            String clientSecret = request.getParameter(PARAM_OAUTH_CLIENT_SECRET);

            Client client = null;
            if( clientId != null && clientSecret != null){
                client = authorizeClient( clientId, clientSecret);
            }

            String accessToken = request.getParameter(PARAM_ACCESS_TOKEN);
            if( accessToken != null ) {
                // access token을 보내주면 검사 한다.
                // access token의  + 문자가 자동으로 공백을 치완된다.
                accessToken = processBase64Token( accessToken);

                // access token 검사와 사용자 정보 조회를 동시에 한다.
                Map<String, String> ssoUserInfoMap = clientApiService.checkAccessTokenAndUser(accessToken);
                userId = ssoUserInfoMap.get("id");

            }

            User user = getUser( userId );

            if ( user == null) {
                //  사용자가 없음
                throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_NOT_REGISTER);
            }

            if( BandiConstants.FLAG_Y.equals( client.getFlagUseEam())){
                setDelegatorInfo( clientId, resourceId, user.getId(), resultMap );
            }

            ResponseDTO dto = new ResponseDTO(resultMap);
            ResponseEntity<ResponseDTO> responseEntity = new ResponseEntity<>( dto , HttpStatus.OK );

            return responseEntity;

        } catch ( ClientApiException e) {

            ResponseEntity<ResponseDTO> responseEntity = processException(e);

            return responseEntity;
        } catch ( Exception e){

            ResponseEntity<ResponseDTO> responseEntity = processException(e);

            return responseEntity;
        }
    }

    public void setDelegatorInfo( String clientOid, String resourceOid, String userId, Map< String, Object> resultMap ){

        String delegatorUserId = resourceService.getDelegatorId( clientOid, resourceOid, userId );

        resultMap.put( RESULT_DELEGATOR_INFO, delegatorUserId );
    }

    protected void logOtpHistory(String userId, String certificationResult, String remoteIp) {
		OtpHistory otphistory = new OtpHistory();
		try {
			otphistory.setUserId(userId);
			otphistory.setCertificationResult(certificationResult);
			otphistory.setLoginIp(remoteIp);
			otphistory.setCertifiedAt(DateUtil.getNow());

			otpHistoryService.insert(otphistory);

		} catch(Exception e) {
			logger.error("OTP HISTORY WRITE ERROR");
		}
	}



}
