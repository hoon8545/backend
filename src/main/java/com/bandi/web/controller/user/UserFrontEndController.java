package com.bandi.web.controller.user;

import java.io.File;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import com.bandi.domain.*;
import com.bandi.domain.kaist.*;
import com.bandi.service.manage.CodeDetailService;
import com.bandi.service.manage.kaist.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bandi.common.BandiDbConfigs;
import com.bandi.common.BandiProperties;
import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.common.util.FileUtils;
import com.bandi.common.util.bpel.KaistBpelUtils;
import com.bandi.dao.base.Paginator;
import com.bandi.domain.Client;
import com.bandi.domain.ClientCondition;
import com.bandi.domain.Configuration;
import com.bandi.domain.ConfigurationCondition;
import com.bandi.domain.User;
import com.bandi.domain.kaist.AccessElement;
import com.bandi.domain.kaist.Authority;
import com.bandi.domain.kaist.DelegateAuthority;
import com.bandi.domain.kaist.DelegateAuthorityCondition;
import com.bandi.domain.kaist.DelegateHistory;
import com.bandi.domain.kaist.ExtUser;
import com.bandi.domain.kaist.FileManager;
import com.bandi.domain.kaist.IdentityAuthentication;
import com.bandi.domain.kaist.Notice;
import com.bandi.domain.kaist.OtpSecurityArea;
import com.bandi.domain.kaist.PasswordChangeHistory;
import com.bandi.domain.kaist.RoleAuthority;
import com.bandi.domain.kaist.RoleMaster;
import com.bandi.domain.kaist.Terms;
import com.bandi.domain.kaist.TermsCondition;
import com.bandi.domain.kaist.UserLoginHistory;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.cryption.CryptService;
import com.bandi.service.manage.AdminService;
import com.bandi.service.manage.ConfigurationService;
import com.bandi.service.manage.kaist.AccessElementService;
import com.bandi.service.manage.kaist.AuthorityService;
import com.bandi.service.manage.kaist.ClientService_KAIST;
import com.bandi.service.manage.kaist.DelegateAuthorityService;
import com.bandi.service.manage.kaist.DelegateHistoryService;
import com.bandi.service.manage.kaist.ExtUserService;
import com.bandi.service.manage.kaist.FileManagerService;
import com.bandi.service.manage.kaist.IdentityAuthenticationService;
import com.bandi.service.manage.kaist.MessageInfoService;
import com.bandi.service.manage.kaist.NoticeService;
import com.bandi.service.manage.kaist.OtpSecurityAreaService;
import com.bandi.service.manage.kaist.PasswordChangeHistoryService;
import com.bandi.service.manage.kaist.ResourceService;
import com.bandi.service.manage.kaist.RoleAuthorityService;
import com.bandi.service.manage.kaist.RoleMasterService;
import com.bandi.service.manage.kaist.ServiceRequestService;
import com.bandi.service.manage.kaist.TermsService;
import com.bandi.service.manage.kaist.UserLoginHistoryService;
import com.bandi.service.manage.kaist.UserService_KAIST;
import com.bandi.service.sso.kaist.ClientApiService;
import com.bandi.web.controller.vo.TreeNode;

@Controller
public class UserFrontEndController{

    @Resource
    protected ClientService_KAIST clientService;

    @Resource
    protected UserService_KAIST userService;

    @Resource
    protected NoticeService noticeService;

    @Resource
    protected FileManagerService fileService;

    @Resource
    protected IdentityAuthenticationService identityService;

    @Resource
    protected CryptService cryptService;

    @Resource
    protected OtpSecurityAreaService otpSecurityService;

    @Resource
    protected ConfigurationService configurationService;

    @Resource
    protected ServiceRequestService serviceRequestService;

    @Resource
    protected AdminService adminService;

    @Resource
    protected MessageInfoService messageInfoService;

    @Resource
    protected AccessElementService accessElementService;

    @Autowired
    protected ResourceService resourceService;

    @Resource
    protected DelegateHistoryService delegateHistoryService;

    @Resource
    protected DelegateAuthorityService delegateAuthorityService;

    @Resource
    protected RoleMasterService roleMasterService;

    @Resource
    protected RoleMemberService roleMemberService;

    @Resource
    protected RoleAuthorityService roleAuthorityService;

    @Resource
    protected ExtUserService extUserService;

    @Resource
    protected UserLoginHistoryService userLoginHistoryService;

    @Resource
    protected PasswordChangeHistoryService passwordChangeHistoryService;

    @Resource
    protected AuthorityService authorityService;

    @Resource
    protected CodeDetailService codeDetailService;

    @Resource
    protected TermsService termsService;

    @Resource( name = "initechClientApiService" )
    protected ClientApiService clientApiService;

    @Resource( name = "fileUtils" )
    private FileUtils fileUtils;

    @Resource
    protected KaistBpelUtils bpelUtils;

    @RequestMapping( value = "/user/client/getAll", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< List< Client > > getAll(){

        ClientCondition condition = new ClientCondition();

        List< Client > clientList = clientService.selectByCondition( condition );

        ResponseEntity< List< Client > > responseEntity = new ResponseEntity< List< Client > >( clientList, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/getKaistUid/{userId}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< String > getKaistUid( @PathVariable String userId ){
        if( userId == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        String kaistUid = userService.getKaistUidByUserId( userId );

        ResponseEntity< String > responseEntity = new ResponseEntity< String >( kaistUid, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/notice/read/{oid}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< Notice > read( @PathVariable String oid ){
        if( oid == null || oid.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }
        Notice notice = noticeService.read( oid );

        ResponseEntity< Notice > responseEntity = new ResponseEntity( notice, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/notice/userLoginNotice", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< Notice > userLoginNotice(){
        List< Notice > notice = noticeService.getUserLoginNotice();

        ResponseEntity< Notice > responseEntity = new ResponseEntity( notice, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/filemanager/download", method = RequestMethod.POST, produces = "application/json" )
    public void downloadFile( @RequestBody FileManager fileOid, HttpServletResponse response ) throws Exception{
        if( fileOid == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }
        FileManager file = fileService.get( fileOid.getOid() );
        String storedName = file.getStoredName();
        String originalName = file.getOriginalName();

        byte fileByte[] = org.apache.commons.io.FileUtils.readFileToByteArray( new File( BandiProperties_KAIST.WINDOW_FILE_PATH_NOTICE + storedName ) );

        response.setContentType( "application/octet-stream" );
        response.setContentLength( fileByte.length );
        response.setHeader( "Content-Disposition", "attachment; filename=" + URLEncoder.encode( originalName, "UTF-8" ) + ";" );
        response.setHeader( "Content-Transfer-Encoding", "binary" );
        response.setHeader( "filename", URLEncoder.encode( originalName, "UTF-8" ) );
        response.getOutputStream().write( fileByte );
        response.getOutputStream().flush();
        response.getOutputStream().close();
    }

    @RequestMapping( value = "/user/getIdentityAuthentication/{serviceType}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< IdentityAuthentication > get( @PathVariable String serviceType ){
        if( serviceType == null || serviceType.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        IdentityAuthentication identityauthentication = identityService.get( serviceType );

        ResponseEntity< IdentityAuthentication > responseEntity = new ResponseEntity( identityauthentication, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/getUser/{oid}", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< User > getUser( @PathVariable String oid ){
        if( oid == null || "".equals( oid ) ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        User user = userService.getOriginalUserByKaistUid( oid );

        ResponseEntity< User > responseEntity = new ResponseEntity< User >( user, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/changePwd/{type}", method = RequestMethod.POST, produces = "json/application" )
    public ResponseEntity< User > updatePwd( @PathVariable String type, @RequestBody User user ){
        PasswordChangeHistory passwordchangehistory = new PasswordChangeHistory();

        int nonInputAbleNum = BandiDbConfigs.getInt( BandiConstants_KAIST.CONFIGURATION_COLUMN_RECENT_PASSWORD_NON_INPUTABLE_NUMBER );
        String nonInputAbleNumber = Integer.toString( nonInputAbleNum );
        String encryptPassword = null;
        List< String > oldPasswordList = new ArrayList< String >();
        if( ! "L".equals( BandiProperties_KAIST.PROPERTIES_MODE ) ){
            bpelUtils.authFailLogReset( user.getId() );
        }
        try{
            if( BandiConstants_KAIST.PASSWORD_CHANGE_FIND.equals( type ) ){ // 비밀번호 찾기로 인한 변경

                if( user.getPassword() == null || user.getId() == null || user.getGenericId() == null ){
                    throw new BandiException( ErrorCode.GENERAL_REQUEST_PARAMETER_ISNULL );
                }

                encryptPassword = cryptService.passwordEncrypt( user.getId(), user.getPassword() );

                // 최근에 변경했던 비밀번호들과 비교
                oldPasswordList = passwordChangeHistoryService.getOldPasswordList( nonInputAbleNumber, user.getId() );
                if( oldPasswordList.size() != 0 ){
                    for( int i = 0; i < oldPasswordList.size(); i++ ){
                        if( encryptPassword.equals( oldPasswordList.get( i ) ) == true ){
                            throw new BandiException( ErrorCode_KAIST.EXIST_OLD_HISTORY_PASSWORD );
                        }
                    }
                }

                user.setPasswordChangedAt( DateUtil.getNow() );
                userService.updatePassword( user );

                passwordchangehistory.setUserId( user.getId() );

            } else if( BandiConstants_KAIST.PASSWORD_CHANGE_EXPIRATION.equals( type ) ){ // 비밀번호 만료로 인한 변경

                if( user.getPassword() == null || user.getOldPassword() == null || user.getKaistUid() == null ){
                    throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
                }

                encryptPassword = cryptService.passwordEncrypt( user.getId(), user.getPassword() );
                String encryptOldPassword = cryptService.passwordEncrypt( user.getId(), user.getOldPassword() );

                User userFromDB = userService.getByKaistUid( user.getKaistUid() );

                if( userFromDB == null ){
                    throw new BandiException( ErrorCode_KAIST.USER_KAISTUID_NOT_FOUND );
                } else{
                    if( ! userFromDB.getPassword().equals( encryptOldPassword ) ){
                        throw new BandiException( ErrorCode_KAIST.OLD_PASSWORD_NOT_MATCH );
                    }
                }

                oldPasswordList = passwordChangeHistoryService.getOldPasswordList( nonInputAbleNumber, userFromDB.getId() );
                if( oldPasswordList.size() != 0 ){
                    for( int i = 0; i < oldPasswordList.size(); i++ ){
                        if( encryptPassword.equals( oldPasswordList.get( i ) ) == true ){
                            throw new BandiException( ErrorCode_KAIST.EXIST_OLD_HISTORY_PASSWORD );
                        }
                    }
                    user.setPasswordChangedAt( DateUtil.getNow() );
                    user.setId( userFromDB.getId() );
                    user.setGenericId( userFromDB.getGenericId() );
                    user.setName( userFromDB.getName() );
                    userService.updatePassword( user );
                } else{
                    user.setPasswordChangedAt( DateUtil.getNow() );
                    user.setId( userFromDB.getId() );
                    user.setGenericId( userFromDB.getGenericId() );
                    user.setName( userFromDB.getName() );
                    userService.updatePassword( user );
                }

            } else{ // 마이페이지에서의 변경

                if( user.getPassword() == null || user.getOldPassword() == null ){
                    throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
                }

                encryptPassword = cryptService.passwordEncrypt( user.getId(), user.getPassword() );
                String encryptOldPassword = cryptService.passwordEncrypt( user.getId(), user.getOldPassword() );

                String userId = ContextUtil.getCurrentUserId();
                User userFromDB = userService.get( userId );

                if( userFromDB == null ){
                    throw new BandiException( ErrorCode_KAIST.USER_KAISTUID_NOT_FOUND );
                } else{
                    if( ! userFromDB.getPassword().equals( encryptOldPassword ) ){
                        throw new BandiException( ErrorCode_KAIST.OLD_PASSWORD_NOT_MATCH );
                    }
                }

                oldPasswordList = passwordChangeHistoryService.getOldPasswordList( nonInputAbleNumber, userFromDB.getId() );
                if( oldPasswordList.size() != 0 ){
                    for( int i = 0; i < oldPasswordList.size(); i++ ){
                        if( encryptPassword.equals( oldPasswordList.get( i ) ) == true ){
                            throw new BandiException( ErrorCode_KAIST.EXIST_OLD_HISTORY_PASSWORD );
                        }
                    }
                    user.setPasswordChangedAt( DateUtil.getNow() );
                    user.setId( userFromDB.getId() );
                    user.setGenericId( userFromDB.getGenericId() );
                    user.setKaistUid( userFromDB.getKaistUid() );
                    userService.updatePassword( user );
                } else{
                    user.setPasswordChangedAt( DateUtil.getNow() );
                    user.setId( userFromDB.getId() );
                    user.setGenericId( userFromDB.getGenericId() );
                    user.setKaistUid( userFromDB.getKaistUid() );
                    userService.updatePassword( user );
                }
            }

            passwordchangehistory.setFlagSuccess( BandiConstants_KAIST.PASSWORD_CHANGE_SUCCESS );
            passwordchangehistory.setChangedPassword( encryptPassword ); // success -> password save


            ResponseEntity< User > responseEntity = new ResponseEntity< User >( HttpStatus.OK );
            return responseEntity;

        } catch( BandiException e ){

            if( BandiConstants_KAIST.PASSWORD_CHANGE_FIND.equals( type ) ){
                passwordchangehistory.setUserId( user.getId() );
            }

            passwordchangehistory.setFlagSuccess( BandiConstants_KAIST.PASSWORD_CHANGE_FAIL );

            if( ErrorCode_KAIST.EXIST_OLD_HISTORY_PASSWORD.equals( e.getErrorCode() ) ){
                throw new BandiException( e.getErrorCode(), new String[]{ nonInputAbleNumber } );
            }

            throw new BandiException( e.getErrorCode() );
        } finally{
            passwordChangeHistoryService.insert( passwordchangehistory );
        }

    }

    @RequestMapping( value = "/user/getUserDetailByUserId/{type}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< User > getUserDetailbyUserId( @PathVariable String type ){
        if( type == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        String userId = ContextUtil.getCurrentUserId();
        User user = userService.getDetail( userId );
        if( BandiConstants_KAIST.USER_SEX_CODE_MAN.equals( user.getSexCodeUid() ) ){
            user.setSex( BandiConstants_KAIST.USER_SEX_MAN );
        } else{
            user.setSex( BandiConstants_KAIST.USER_SEX_WOMEN );
        }

        ResponseEntity< User > responseEntity = new ResponseEntity< User >( user, HttpStatus.OK );

        return responseEntity;
    }

    @RequestMapping( value = "/user/otpsecurityarea/{userId}/{clientOid}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< OtpSecurityArea > dataFromDb( @PathVariable String userId, @PathVariable String clientOid ){
        if( userId == null || userId.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        if( clientOid == null || clientOid.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        String otpSecurityAreaOid = otpSecurityService.dataFromDb( userId, clientOid );

        OtpSecurityArea otpsequrityarea = otpSecurityService.get( otpSecurityAreaOid );

        ResponseEntity< OtpSecurityArea > responseEntity = new ResponseEntity( otpsequrityarea, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/client/search", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< Paginator > list( @RequestBody Client client ){

        if( client.getPageSize() == 0 ){
            client.setPageSize( 10 );
        }

        if( client.getPageNo() == 0 ){
            client.setPageNo( 1 );
        }

        String sortDirection = client.isSortDesc() ? "DESC" : "ASC";

        Paginator paginator = new Paginator( client.getPageNo(), client.getPageSize(), client.getSortBy(), sortDirection );
        paginator = clientService.search( paginator, client );

        ResponseEntity< Paginator > responseEntity = new ResponseEntity( paginator, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/otpsecurityarea", method = RequestMethod.POST, consumes = "application/json" )
    public ResponseEntity< OtpSecurityArea > save( @RequestBody OtpSecurityArea otpsecurityarea ){
        if( otpsecurityarea == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        otpSecurityService.insert( otpsecurityarea );

        ResponseEntity< OtpSecurityArea > responseEntity = new ResponseEntity( otpsecurityarea, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/otpsecurityarea/{oid}", method = RequestMethod.PUT, consumes = "application/json" )
    public ResponseEntity< OtpSecurityArea > update( @PathVariable String oid, @RequestBody OtpSecurityArea otpsecurityarea ){
        if( oid == null || oid.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        if( otpsecurityarea == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        otpSecurityService.update( otpsecurityarea );

        ResponseEntity< OtpSecurityArea > responseEntity = new ResponseEntity( otpsecurityarea, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/configuration_uncheckSession_kaist", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< Map< String, String > > list(){
        List< String > configKeys = new ArrayList< String >();

        List< String > columnList = Configuration.getColumnList();

        for( int i = 0; i < columnList.size(); i++ ){
            configKeys.add( columnList.get( i ) );
        }

        ConfigurationCondition condition = new ConfigurationCondition();
        condition.createCriteria().andConfigKeyIn( configKeys );

        List< Configuration > configurations = configurationService.selectByCondition( condition );

        Map< String, String > configMap = new HashMap();
        for( Configuration config : configurations ){
            configMap.put( config.getConfigKey(), config.getConfigValue() );
        }
        configMap.put( "productVersion", BandiProperties.PRODUCT_VERSION );

        ResponseEntity< Map< String, String > > responseEntity = new ResponseEntity( configMap, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/notice/search", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< Paginator > list( @RequestBody Notice notice ){

        if( notice.getPageSize() == 0 ){
            notice.setPageSize( 10 );
        }

        if( notice.getPageNo() == 0 ){
            notice.setPageNo( 1 );
        }

        String sortDirection = notice.isSortDesc() ? "DESC" : "ASC";

        Paginator paginator = new Paginator( notice.getPageNo(), notice.getPageSize(), notice.getSortBy(), sortDirection );
        paginator = noticeService.search( paginator, notice );

        ResponseEntity< Paginator > responseEntity = new ResponseEntity( paginator, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/filemanager", method = RequestMethod.POST, consumes = "multipart/form-data" )
    public ResponseEntity< FileManager > save( MultipartHttpServletRequest request ) throws Exception{
        if( request == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        Map< String, Object > map = new HashMap< String, Object >();

        map = fileUtils.insertFileInfo( request );
        if( request.getParameter( BandiConstants_KAIST.PARAM_FILE_OID ) != null ){
            map.put( BandiConstants_KAIST.PARAM_FILE_OID, request.getParameter( BandiConstants_KAIST.PARAM_FILE_OID ) );
        }

        List< String > fileOids = fileService.insertFile( map );

        ResponseEntity< FileManager > responseEntity = new ResponseEntity( fileOids, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/notice/{fileOid}", method = RequestMethod.POST, consumes = "application/json" )
    public ResponseEntity< Notice > save( @PathVariable List< String > fileOid, @RequestBody Notice notice ){
        if( notice == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }
        noticeService.insert( notice, fileOid );

        ResponseEntity< Notice > responseEntity = new ResponseEntity( notice, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/notice/{oid}/{fileOid}", method = RequestMethod.PUT, consumes = "application/json" )
    public ResponseEntity< Notice > update( @PathVariable String oid, @PathVariable List< String > fileOid, @RequestBody Notice notice ){
        if( oid == null || oid.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        if( notice == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        noticeService.update( notice, oid, fileOid );

        ResponseEntity< Notice > responseEntity = new ResponseEntity( notice, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/filemanager/{fileOid}", method = RequestMethod.DELETE, produces = "application/json" )
    public ResponseEntity delete( @PathVariable String[] fileOid ){
        if( fileOid == null || fileOid.length == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        fileService.delete( fileOid );

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/notice/{oid}", method = RequestMethod.DELETE, produces = "application/json" )
    public ResponseEntity delete( @PathVariable String oid ){
        if( oid == null || oid.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        if( oid.indexOf( "," ) > 0 ){
            List< String > oids = Arrays.asList( oid.split( "\\s*,\\s*" ) );
            noticeService.deleteBatch( oids );
        } else{
            noticeService.delete( oid );
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/notice/{oid}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< Notice > getNotice( @PathVariable String oid ){
        if( oid == null || oid.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        Notice notice = noticeService.get( oid );
        ResponseEntity< Notice > responseEntity = new ResponseEntity( notice, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/client/{oid}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< Client > getClient( @PathVariable String oid ){
        if( oid == null || oid.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        Client client = clientService.getForForm( oid );

        ResponseEntity< Client > responseEntity = new ResponseEntity( client, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/accesselement/mappedResrouceOids", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< List< String > > getMappedResourceOidsByTargetObjectId( @RequestBody AccessElement accesselement ){

        accesselement.setTargetObjectType( BandiConstants_KAIST.OBJECT_TYPE_AUTHORITY );

        List< String > mappedResourceOids = accessElementService.getMappedResourceOidsByTargetObjectId( accesselement.getClientOid(), accesselement.getTargetObjectId(), accesselement.getTargetObjectType() );

        ResponseEntity< List< String > > responseEntity = new ResponseEntity( mappedResourceOids, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/resource/tree", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< TreeNode > tree( @RequestBody com.bandi.domain.kaist.Resource resoruce ){

        TreeNode treeNode = resourceService.getTreeData( resoruce.getParentOid() );

        ResponseEntity< TreeNode > responseEntity = new ResponseEntity( treeNode, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/delegatehistory", method = RequestMethod.POST, consumes = "application/json" )
    public ResponseEntity< DelegateHistory > save( @RequestBody DelegateHistory delegatehistory ){
        if( delegatehistory == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        if( delegatehistory.getAuthorities() == null || delegatehistory.getAuthorities().trim().length() == 0 ){
            throw new BandiException( ErrorCode_KAIST.DELEGATE_AUTHORITY_IS_NULL );
        }

        delegateHistoryService.insert( delegatehistory );

        ResponseEntity< DelegateHistory > responseEntity = new ResponseEntity<>( delegatehistory, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/delegatehistory/cancle/{oid}", method = RequestMethod.DELETE, produces = "application/json" )
    public ResponseEntity cancleDelegateRole( @PathVariable String oid ){
        if( oid == null || oid.trim().length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        delegateHistoryService.cancleDelegateRole( oid );

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/delegateauthority/search/{oid}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity getByDelegateOid( @PathVariable String oid ){
        if( oid == null || oid.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        DelegateAuthorityCondition condition = new DelegateAuthorityCondition();
        condition.createCriteria().andDelegateOidEqualTo( oid );
        List< DelegateAuthority > delegateauthoritys = delegateAuthorityService.selectByCondition( condition );

        ResponseEntity< DelegateAuthority > responseEntity = new ResponseEntity( delegateauthoritys, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/delegatehistory/search", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< Paginator > list( @RequestBody DelegateHistory delegatehistory ){

        if( delegatehistory.getPageSize() == 0 ){
            delegatehistory.setPageSize( 10 );
        }

        if( delegatehistory.getPageNo() == 0 ){
            delegatehistory.setPageNo( 1 );
        }

        String sortDirection = delegatehistory.isSortDesc() ? "DESC" : "ASC";

        Paginator paginator = new Paginator( delegatehistory.getPageNo(), delegatehistory.getPageSize(), delegatehistory.getSortBy(), sortDirection );
        paginator = delegateHistoryService.search( paginator, delegatehistory );

        ResponseEntity< Paginator > responseEntity = new ResponseEntity( paginator, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/rolemaster/selfRoleSearch", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< Paginator > selfRoleList( @RequestBody RoleMaster rolemaster ){

        if( rolemaster.getPageSize() == 0 ){
            rolemaster.setPageSize( 10 );
        }

        if( rolemaster.getPageNo() == 0 ){
            rolemaster.setPageNo( 1 );
        }

        String sortDirection = rolemaster.isSortDesc() ? "DESC" : "ASC";

        Paginator paginator = new Paginator( rolemaster.getPageNo(), rolemaster.getPageSize(), rolemaster.getSortBy(), sortDirection );
        paginator = roleMasterService.selfRoleSearch( paginator, rolemaster );

        ResponseEntity< Paginator > responseEntity = new ResponseEntity<>( paginator, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/roleauthority/search", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< Paginator > list( @RequestBody RoleAuthority roleauthority ){

        if( roleauthority.getPageSize() == 0 ){
            roleauthority.setPageSize( 10 );
        }

        if( roleauthority.getPageNo() == 0 ){
            roleauthority.setPageNo( 1 );
        }

        String sortDirection = roleauthority.isSortDesc() ? "DESC" : "ASC";

        Paginator paginator = new Paginator( roleauthority.getPageNo(), roleauthority.getPageSize(), roleauthority.getSortBy(), sortDirection );
        paginator = roleAuthorityService.search( paginator, roleauthority );

        ResponseEntity< Paginator > responseEntity = new ResponseEntity( paginator, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/getGenericId", method = RequestMethod.POST, consumes = "application/json" )
    public ResponseEntity< List< User > > getGenericId( @RequestBody User user ){

        if( user == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        List< User > userList = userService.getGenericID( user.getGenericId() );

        ResponseEntity< List< User > > responseEntity = new ResponseEntity< List< User > >( userList, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/extuser/{oid}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< ExtUser > getExtUser( @PathVariable String oid ){
        if( oid == null || oid.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        ExtUser extuser = extUserService.get( oid );

        ResponseEntity< ExtUser > responseEntity = new ResponseEntity( extuser, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/userloginhistory/search", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< Paginator > list( @RequestBody UserLoginHistory userloginhistory ){

        if( userloginhistory.getPageSize() == 0 ){
            userloginhistory.setPageSize( 10 );
        }

        if( userloginhistory.getPageNo() == 0 ){
            userloginhistory.setPageNo( 1 );
        }

        String sortDirection = userloginhistory.isSortDesc() ? "DESC" : "ASC";

        Paginator paginator = new Paginator( userloginhistory.getPageNo(), userloginhistory.getPageSize(), userloginhistory.getSortBy(), sortDirection );
        paginator = userLoginHistoryService.search( paginator, userloginhistory );

        ResponseEntity< Paginator > responseEntity = new ResponseEntity( paginator, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/getSerialNumber/{userId:.+}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< String > getSerialNumber( @PathVariable String userId ){
        if( userId == null ){
            throw new BandiException( ErrorCode_KAIST.PARAMETER_IS_NULL );
        }

        String serialNumber = clientApiService.getSerialNumber( userId );

        ResponseEntity< String > responseEntity = new ResponseEntity< String >( serialNumber, HttpStatus.OK );
        return responseEntity;

    }

    @RequestMapping( value = "/user/getCheckOtpResult/{otpNumber}", method = RequestMethod.PUT, produces = "application/json" )
    public ResponseEntity< Integer > getResultCode( @PathVariable String otpNumber, @RequestBody User user ){
        if( otpNumber == null ){
            throw new BandiException( ErrorCode_KAIST.PARAMETER_IS_NULL );
        }

        if( user == null ){
            throw new BandiException( ErrorCode_KAIST.PARAMETER_IS_NULL );
        }

        String userId = user.getId();
        if( userId == null ){
            throw new BandiException( ErrorCode_KAIST.PARAMETER_IS_NULL );
        }

        int resultCode = clientApiService.getCheckOtpResultCode( userId, otpNumber );

        ResponseEntity< Integer > responseEntity = new ResponseEntity< Integer >( resultCode, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/getServicePortfolio/{type}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< List< Client > > getServicePortfolio( @PathVariable String type ){

        if( type == null ){
            throw new BandiException( ErrorCode_KAIST.PARAMETER_IS_NULL );
        }

        List< Client > clientList = null;
        Client client = null;

        if( BandiConstants_KAIST.SERVICE_PORTFOLIO_TYPE_USERMAIN.equals( type ) ){
            clientList = clientService.getServicePortfolioForUserMain();
        } else{
            ClientCondition condition = new ClientCondition();
            condition.or().andSsoTypeLike( BandiConstants_KAIST.SSO_TYPE_INTEGRATED_SSO );
            clientList = clientService.selectByCondition( condition );
        }

        for( int i = 0; i < clientList.size(); i++ ){
            client = clientList.get( i );

            if( client.getManagerId() != null ){
                User user = userService.getDetail( client.getManagerId() ); // 연락처정보 구해옴

                if( user != null ){
                    client.setEmailAddress( user.getEmail() );
                    client.setKoreanName( user.getKoreanName() );
                    client.setOfficeTelephoneNumber( user.getOfficeTelephoneNumber() );
                }
            }
        }

        ResponseEntity< List< Client > > responseEntity = new ResponseEntity< List< Client > >( clientList, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/getUserMenuType/{userId}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< Long > getUserMenuType( @PathVariable String userId ){
        if( userId == null ){
            throw new BandiException( ErrorCode_KAIST.PARAMETER_IS_NULL );
        }

        long result = userService.getUserMenuType( userId );

        ResponseEntity< Long > responseEntity = new ResponseEntity< Long >( result, HttpStatus.OK );
        return responseEntity;

    }

    @RequestMapping( value = "/user/client/getParameter_kaist/{oid}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< Client > getParameterKaist( @PathVariable String oid ){
        if( oid == null || oid.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        String parameter = clientService.getParameter( oid );

        ResponseEntity< Client > responseEntity = new ResponseEntity( parameter, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/resource/{oid}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< com.bandi.domain.kaist.Resource > getResource( @PathVariable String oid ){
        if( oid == null || oid.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        com.bandi.domain.kaist.Resource resource = resourceService.get( oid );

        ResponseEntity< com.bandi.domain.kaist.Resource > responseEntity = new ResponseEntity( resource, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/authority/search", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< Paginator > list( @RequestBody Authority authority ){

        if( authority.getPageSize() == 0 ){
            authority.setPageSize( 10 );
        }

        if( authority.getPageNo() == 0 ){
            authority.setPageNo( 1 );
        }

        String sortDirection = authority.isSortDesc() ? "DESC" : "ASC";

        Paginator paginator = new Paginator( authority.getPageNo(), authority.getPageSize(), authority.getSortBy(), sortDirection );
        paginator = authorityService.search( paginator, authority );

        ResponseEntity< Paginator > responseEntity = new ResponseEntity( paginator, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/authority", method = RequestMethod.POST, consumes = "application/json" )
    public ResponseEntity< Authority > saveAuthority( @RequestBody Authority authority ){
        if( authority == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        authorityService.insert( authority );

        ResponseEntity< Authority > responseEntity = new ResponseEntity( authority, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/authority/{oid}", method = RequestMethod.PUT, consumes = "application/json" )
    public ResponseEntity< Authority > updateAuthority( @PathVariable String oid, @RequestBody Authority authority ){
        if( oid == null || oid.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        if( authority == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        authorityService.update( authority );

        ResponseEntity< Authority > responseEntity = new ResponseEntity( authority, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/authority/{oid}", method = RequestMethod.DELETE, produces = "application/json" )
    public ResponseEntity deleteAuthority( @PathVariable String oid ){
        if( oid == null || oid.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        if( oid.indexOf( "," ) > 0 ){
            List< String > oids = Arrays.asList( oid.split( "\\s*,\\s*" ) );
            authorityService.deleteBatch( oids );
        } else{
            authorityService.delete( oid );
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/authority/{oid}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< Authority > getAuthority( @PathVariable String oid ){
        if( oid == null || oid.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        Authority authority = authorityService.get( oid );

        ResponseEntity< Authority > responseEntity = new ResponseEntity( authority, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/accesselement", method = RequestMethod.POST, consumes = "application/json" )
    public ResponseEntity< AccessElement > saveAccessElement( @RequestBody AccessElement accesselement ){
        if( accesselement == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        List< String > resourceOids = Arrays.asList( accesselement.getResourceOid().split( "\\s*,\\s*" ) );

        accessElementService.saveAceList( accesselement.getClientOid(), accesselement.getTargetObjectId(), resourceOids, true );

        ResponseEntity< AccessElement > responseEntity = new ResponseEntity( accesselement, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/client/kaist/{oid}", method = RequestMethod.PUT, consumes = "application/json" )
    public ResponseEntity< Client > update( @PathVariable String oid, @RequestBody Client client ){
        if( oid == null || oid.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        if( client == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        // 카이스트에서 사용하지 않음
        // checkValidation( client);

        clientService.update( client );

        ResponseEntity< Client > responseEntity = new ResponseEntity( client, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/resource/search", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< Paginator > list( @RequestBody com.bandi.domain.kaist.Resource resource ){

        if( resource.getPageSize() == 0 ){
            resource.setPageSize( 10 );
        }

        if( resource.getPageNo() == 0 ){
            resource.setPageNo( 1 );
        }

        String sortDirection = resource.isSortDesc() ? "DESC" : "ASC";

        Paginator paginator = new Paginator( resource.getPageNo(), resource.getPageSize(), resource.getSortBy(), sortDirection );
        paginator = resourceService.search( paginator, resource );

        ResponseEntity< Paginator > responseEntity = new ResponseEntity( paginator, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/extuser/search", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< Paginator > list( @RequestBody ExtUser extuser ){

        if( extuser.getPageSize() == 0 ){
            extuser.setPageSize( 10 );
        }

        if( extuser.getPageNo() == 0 ){
            extuser.setPageNo( 1 );
        }
        String sortDirection = extuser.isSortDesc() ? "DESC" : "ASC";

        Paginator paginator = new Paginator( extuser.getPageNo(), extuser.getPageSize(), extuser.getSortBy(),
                sortDirection );
        paginator = extUserService.search( paginator, extuser );

        ResponseEntity< Paginator > responseEntity = new ResponseEntity( paginator, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/before/getAll/{type}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< Terms > getAllTerms( @PathVariable String type ){
        TermsCondition condition = new TermsCondition();
        condition.createCriteria().andTermsTypeEqualTo( type );
        condition.setOrderByClause( "TERMSINDEX ASC" );
        List< Terms > terms = termsService.selectByCondition( condition );

        ResponseEntity< Terms > responseEntity = new ResponseEntity( terms, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/codedetail/list/{codeId}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< List< CodeDetail > > getCodeDetailList( @PathVariable String codeId ){
        if( codeId == null || codeId.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        List< String > codeIds = null;
        codeIds = Arrays.asList( codeId.split( "\\s*,\\s*" ) );

        CodeDetailCondition condition = new CodeDetailCondition();

        condition.createCriteria().andCodeIdIn( codeIds );
        condition.setOrderByClause( "OID ASC" );

        List< CodeDetail > codeDetailList = codeDetailService.selectByCondition( condition );

        ResponseEntity< List< CodeDetail > > responseEntity = new ResponseEntity( codeDetailList, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/rolemaster/search", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< Paginator > listRoleMaster( @RequestBody RoleMaster rolemaster ){

        if( rolemaster.getPageSize() == 0 ){
            rolemaster.setPageSize( 10 );
        }

        if( rolemaster.getPageNo() == 0 ){
            rolemaster.setPageNo( 1 );
        }

        String sortDirection = rolemaster.isSortDesc() ? "DESC" : "ASC";

        Paginator paginator = new Paginator( rolemaster.getPageNo(), rolemaster.getPageSize(), rolemaster.getSortBy(), sortDirection );
        paginator = roleMasterService.search( paginator, rolemaster );

        ResponseEntity< Paginator > responseEntity = new ResponseEntity<>( paginator, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/rolemember/search", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< Paginator > list( @RequestBody RoleMember rolemember ){

        if( rolemember.getPageSize() == 0 ){
            rolemember.setPageSize( 10 );
        }

        if( rolemember.getPageNo() == 0 ){
            rolemember.setPageNo( 1 );
        }

        String sortDirection = rolemember.isSortDesc() ? "DESC" : "ASC";

        Paginator paginator = new Paginator( rolemember.getPageNo(), rolemember.getPageSize(), rolemember.getSortBy(), sortDirection );
        paginator = roleMemberService.search( paginator, rolemember );

        ResponseEntity< Paginator > responseEntity = new ResponseEntity( paginator, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/extuser", method = RequestMethod.POST, consumes = "application/json" )
    public ResponseEntity< ExtUser > saveExtUser( @RequestBody ExtUser extuser ){
        if( extuser == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        extUserService.insert( extuser );

        ResponseEntity< ExtUser > responseEntity = new ResponseEntity( extuser, HttpStatus.OK );
        return responseEntity;
    }
}

