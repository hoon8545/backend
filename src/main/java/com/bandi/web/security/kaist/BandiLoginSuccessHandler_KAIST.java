package com.bandi.web.security.kaist;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.MDC;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import com.bandi.common.BandiConstants;
import com.bandi.common.util.ContextUtil;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.http.ResponseBody;
import com.bandi.service.manage.impl.LicenseServiceImpl;
import com.bandi.service.sso.exception.SsoException;
import com.bandi.web.controller.vo.LoginParam;
import com.bandi.web.security.BandiLoginSuccessHandler;

public class BandiLoginSuccessHandler_KAIST extends BandiLoginSuccessHandler {

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws ServletException, IOException {

		MDC.put( BandiConstants.MDC_SERVICE_NAME, this.getClass().getSimpleName());
		logger.debug(this.getClass().getSimpleName() + ".onAuthenticationSuccess(..) is called.");

		ContextUtil.getCurrentHttpSession().setAttribute( BandiConstants.MDC_USER_ID, authentication.getPrincipal());

		String oauthUrl = ContextUtil.get(LoginParam.PARAM_NAME_OAUTH_URL);
		String userType = ContextUtil.get(LoginParam.PARAM_NAME_USER_TYPE);

		addLoginHistory( userType);

		// 사용자가 웹 페이지를 통해서 로그인이 성공했을 때.
		if (oauthUrl != null && LoginParam.USER_TYPE_USER.equals(userType)) {

			boolean isAPIRequest = "true".equalsIgnoreCase( ContextUtil.get( LoginParam.PARAM_NAME_AIP_REQUEST ) );

			// authorize 요청 시 로그인 페이지를 경유했을 때, 발급된 authorize코드는 로그인 페이지를 통해서 클라이언트로 리다이렉트 되어야 하기 때문에
			// BandiConstants.IS_NEED_TO_REDIRECT_AT_LOGIN_VUE 파라미터의 값을 true로 전송.
			if( isAPIRequest){
                response.sendRedirect("/api/authorize?" + BandiConstants.IS_NEED_TO_REDIRECT_AT_LOGIN_VUE+ "=true&"+ oauthUrl);
            } else {
                if( LicenseServiceImpl.supportOnlySso() == false) {
                    Map<String, String> map = new HashMap<>();
                    ObjectMapper mapper = new ObjectMapper();
                    String json = null;

                    String authorities = "";
                    if( authentication.getAuthorities() != null && authentication.getAuthorities().size() > 0){
                        for( GrantedAuthority grantedAuthority : authentication.getAuthorities() ){
                            authorities += grantedAuthority.getAuthority();
                        }
                    }

                    boolean isNeedPassswordChange = BandiConstants.FLAG_Y
                            .equals(ContextUtil.get(BandiConstants.ATTR_IS_NEED_PASSSWORD_CHANGE));
                    map.put(BandiConstants.ATTR_IS_NEED_PASSSWORD_CHANGE, String.valueOf(isNeedPassswordChange));

                    map.put( BandiConstants.ATTR_LOGIN_USER_TYPE, BandiConstants.USER_TYPE_USER );
                    map.put( BandiConstants.ATTR_LOGIN_USER_ID, ContextUtil.get( BandiConstants.SSO_PARAM_USER_ID));
                    map.put( BandiConstants.ATTR_LOGIN_USER_NAME, ContextUtil.get( BandiConstants.SSO_PARAM_USER_NM));

                    ResponseBody body = new ResponseBody(map);

                    json = mapper.writeValueAsString(body);

                    response.setStatus(HttpServletResponse.SC_OK);

                    PrintWriter out = response.getWriter();
                    out.print(json);
                    out.flush();
                    out.close();
                } else {
                    response.sendRedirect("/authorize?" + BandiConstants.IS_NEED_TO_REDIRECT_AT_LOGIN_VUE + "=true&" + oauthUrl);
                }
            }

		// 관리자가 로그인 했을 때 또는 , 사용자가 api, oauth를 통해 로그인했을 때.
		} else {
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");

			Map<String, String> map = new HashMap<>();
			ObjectMapper mapper = new ObjectMapper();
			String json = null;

			// 사용자가 웹 페이지가 아닌 API, OAUTH를 통해서 로그인했을 경우 처리. 세션 정보를 리턴
			if( BandiConstants.API_URL_LOGIN.equals( request.getRequestURI() )
				|| BandiConstants.OAUTH_URL_LOGIN.equals( request.getRequestURI() ) ){

				String client_id = ContextUtil.get( BandiConstants.SSO_PARAM_CLLIENT_ID);
				String state = ContextUtil.get( BandiConstants.SSO_PARAM_STATE);

				String signedValue = null;
				String serverSignedValue = null;

				String signCheckFlag = ContextUtil.get( BandiConstants.SSO_PARAM_SIGN_CHECK_FLAG );

				if( signCheckFlag == null || !BandiConstants.UNCHECK_SIGN_VALUE.equals(signCheckFlag)) {
					signedValue = ContextUtil.get( BandiConstants.SSO_PARAM_SIGNED_VALUE);

					clientService.checkSignedValue( client_id, state, signedValue, null);

					serverSignedValue = cryptService.signByServerPrivateKey( state, serverKeyService.getServerPrivateKey() );
					if( signedValue == null){
						throw new SsoException( ErrorCode.SERVER_SIGN_FAIL, SsoException.SSO_SERVER_ERROR, state, null);
					}
				}

				map.put( "sessionId", ContextUtil.getCurrentSessionId());
				map.put( "state", state);

				map.put( "signedValue", serverSignedValue);

				json = mapper.writeValueAsString(map);

				if(BandiConstants.API_URL_LOGIN.equals( request.getRequestURI() )) {
					try{
						String key = request.getParameter( "encryptedKey" );

						String decryptedKey = cryptService.decryptByServerPrivateKey( key, serverKeyService.getServerPrivateKey() );

						json = cryptService.encrypt( json, decryptedKey );

					}catch( Exception e){
						throw new BandiException( ErrorCode.LOGIN_PARAM_ERROR);
					}
				}

			}
			// 관리자가 웹페이지를 통해 로그인했을 때
			else {
				boolean isNeedPassswordChange = BandiConstants.FLAG_Y
						.equals(ContextUtil.get(BandiConstants.ATTR_IS_NEED_PASSSWORD_CHANGE));

				map.put(BandiConstants.ATTR_IS_NEED_PASSSWORD_CHANGE, String.valueOf(isNeedPassswordChange));

				String authorities = "";
				if( authentication.getAuthorities() != null && authentication.getAuthorities().size() > 0){
					for( GrantedAuthority grantedAuthority : authentication.getAuthorities() ){
						authorities += grantedAuthority.getAuthority();
					}
				}

				map.put( BandiConstants.ATTR_AUTHORITIES, authorities );
				map.put( BandiConstants.ATTR_LOGIN_USER_NAME, ContextUtil.get( BandiConstants.ADMIN_REAL_NAME));
				map.put( BandiConstants.ATTR_LOGIN_USER_ID, ContextUtil.get( BandiConstants.ADMIN_REAL_ID));
				map.put( BandiConstants.ATTR_LOGIN_USER_TYPE, BandiConstants.USER_TYPE_MANAGER );
				map.put( BandiConstants.ADMIN_MODULE, ContextUtil.get( BandiConstants.ADMIN_MODULE));

				ResponseBody body = new ResponseBody(map);

				json = mapper.writeValueAsString(body);
			}

			response.setStatus(HttpServletResponse.SC_OK);

			PrintWriter out = response.getWriter();
			out.print(json);
			out.flush();
			out.close();
		}
	}

}
