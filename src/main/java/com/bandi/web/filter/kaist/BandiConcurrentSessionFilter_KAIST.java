package com.bandi.web.filter.kaist;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.MDC;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.web.session.SessionInformationExpiredEvent;

import com.bandi.common.BandiConstants;
import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.WebUtils;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.security.session.BandiSessionInformation;
import com.bandi.web.controller.sso.kaist.SsoLoginManager;
import com.bandi.web.filter.BandiConcurrentSessionFilter;

public class BandiConcurrentSessionFilter_KAIST extends BandiConcurrentSessionFilter {

    @Resource(name="ssoLoginManager")
	protected SsoLoginManager ssoLoginManager;

	public BandiConcurrentSessionFilter_KAIST(SessionRegistry sessionRegistry) {
		super(sessionRegistry);
	}

    // 예외처리 할 관리자 url 등록
    List< String > excludeAdminUrl = new ArrayList<>();
    {
        excludeAdminUrl.add( "/manage/initPasswordChange" );
        excludeAdminUrl.add( "/manage/configuration_uncheckSession" );
        excludeAdminUrl.add( "/manage/configuration_uncheckSession_kaist" );
        excludeAdminUrl.add( "/manage/user/getGenericId" );
        excludeAdminUrl.add( "/manage/notice/read" );
        excludeAdminUrl.add( "/manage/user/getUser" );
    }

    // 예외처리 할 사용자 url 등록
    List< String > excludeUserUrl = new ArrayList<>();
    {
        excludeUserUrl.add( "/user/configuration_uncheckSession_kaist" );
        excludeUserUrl.add( "/user/getGenericId" );
        excludeUserUrl.add( "/user/before/getSession" );
        excludeUserUrl.add( "/user/before/user" );
        excludeUserUrl.add( "/user/before/authenticationNumber_find" );
        excludeUserUrl.add( "/user/before/authenticationNumber_join" );
        excludeUserUrl.add( "/user/before/nice" );
        excludeUserUrl.add( "/user/before/juso" );
        excludeUserUrl.add( "/user/before/juso/result" );
        excludeUserUrl.add( "/user/before/getUserByUid" );
        excludeUserUrl.add( "/user/before/otp" );
        excludeUserUrl.add( "/user/notice" );
        excludeUserUrl.add( "/user/filemanager" );
        excludeUserUrl.add( "/user/client" );
        excludeUserUrl.add( "/user/search" );
        excludeUserUrl.add( "/user/getUser" );
        excludeUserUrl.add( "/user/getIdentityAuthentication" );
        excludeUserUrl.add( "/user/changePwd" );
        excludeUserUrl.add( "/user/getKaistUid" );
        excludeUserUrl.add( "/user/before/servicerequest" );
        excludeUserUrl.add( "/user/userdetail" );
        excludeUserUrl.add( "/user/before/getuserinfo");
        excludeUserUrl.add( "/user/before/extuser");
        excludeUserUrl.add( "/user/getServicePortfolio");
        excludeUserUrl.add( "/user/before/sendSuccessEmailOrSMS");
        excludeUserUrl.add( "/user/before/getAll");
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

		MDC.put(BandiConstants.MDC_SERVICE_NAME, this.getClass().getSimpleName());
		logger.debug(this.getClass().getSimpleName() + " is called.");

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

		String uri = request.getRequestURI();

		System.out.println( "uri --> " + uri);

		// !!!! 사용자/관리자 공통 URL 추가 시, 해당 URL을 타는 곳에서 ContextUtil.getCurrentUserId()를
        // !!!! 호출하는 곳이 존재하면 ContextUtil -> getCurrentUserId api 조건절에 해당 URL 반드시 추가해야함.
		if ( "/".equals( uri ) ||
             "/login".equals( uri ) ||
             "/manage/user/search".equals( uri ) ||   // 사용자,관리자 공통 사용
             "/manage/group/tree".equals( uri ) ||   // 사용자,관리자 공통 사용
             "/manage/servicerequest/search".equals( uri ) ||   // 사용자,관리자 공통 사용
             "/CommonLoginTest.jsp".equals( uri ) ||
             uri.startsWith( "/session"))
		{
		    // 세션, 쿠키 존재유무를 확인하지 않아도 됨
        } else if ( uri.startsWith( "/user" ) || uri.startsWith( "/api/sso" )){

		    // 사용자 로그인 유무 확인 및 사용시간 연장 처리

            Cookie[] cookies = request.getCookies();
            boolean isLoginUser = false;

            if ( BandiProperties_KAIST.IS_TEST_WITHOUT_INITECH == false ) {

                isLoginUser = ssoLoginManager.isLogin(request, response);

                if( isLoginUser){
                    extendLoginAge(request, response);
                } else {

                    // sso 관련 API 가 아닐 경우에만 Exception 처리
                    if( uri.startsWith( "/api/sso") == false){

                        if( excludeUserUrl.stream().anyMatch(extUrl -> uri.startsWith(extUrl)) == false ){
                            throw new BandiException( ErrorCode_KAIST.SSO_NEED_LOGIN );
                        }
                    }
                }

            } else {
                // 테스트 모드일 경우
                if( cookies != null){
                    for( Cookie c : cookies ){
                        String name = c.getName(); // 쿠키 이름 가져오기

                        if( "bandi_cookie".equals( name )) {
                            isLoginUser = true;
                            break;
                        }
                    }

                    if( isLoginUser == false) {

                        // sso 관련 API 가 아닐 경우에만 Exception 처리
                        if( uri.startsWith( "/api/sso") == false){
                            if( excludeUserUrl.stream().anyMatch( extUrl -> uri.startsWith( extUrl ) ) == false ){
                                throw new BandiException( ErrorCode_KAIST.SSO_NEED_LOGIN );
                            }
                        }
                    }
                }
            }
        } else {
            // 관리자 로그인 유무 확인 및 사용시간 연장 처리

            HttpSession session = request.getSession(false);

            if (session != null) {
                BandiSessionInformation info = (BandiSessionInformation )sessionRegistry.getSessionInformation(session.getId());

                if (info != null) {
                    if (info.isExpired()) {

                        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
                        this.handlers.logout(request, response, auth);

                        this.sessionInformationExpiredStrategy.onExpiredSessionDetected(new SessionInformationExpiredEvent(info, request, response));
                    }
                    else {

                        ContextUtil.put( BandiConstants.MDC_SESSION_ID, ContextUtil.getCurrentSessionId());

                        // 세션 탈취 공격 방어 ( 세션이 생성된 ip가 아닌 곳에서 접근할 경우, EXCEPTION 처리
                        String remoteIp = WebUtils.getRemoteIp( request);
                        if( remoteIp.equals( info.getRemoteIp() ) == false){
                            throw new BandiException( ErrorCode.SECURITY_SESSION_INVALID);
                        }

                        // 세션 정보 갱신
                        sessionRegistry.refreshLastRequest(info.getSessionId());
                    }
                } else {
                    if( uri.contains( "/manage" ) ){

                        if( excludeAdminUrl.contains( uri ) == false ){
                            session.invalidate();
                            throw new BandiException( ErrorCode.SECURITY_SESSION_INVALID );
                        }
                    }
                }
            }
        }

        chain.doFilter(request, response);
    }


    /**
     * login 시간을  늘려준다.
     * @param request
     * @param response
     */
    protected void extendLoginAge(HttpServletRequest request, HttpServletResponse response) {

    	ssoLoginManager.extendLoginAge(request, response);

    }



}
