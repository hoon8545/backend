package com.bandi.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bandi.domain.SyncGroup;
import com.bandi.domain.SyncGroupCondition;

public interface SyncGroupMapper {
    long countByCondition(SyncGroupCondition condition);

    int deleteByCondition(SyncGroupCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(SyncGroup record);

    int insertSelective(SyncGroup record);

    List<SyncGroup> selectByCondition(SyncGroupCondition condition);

    SyncGroup selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") SyncGroup record, @Param("condition") SyncGroupCondition condition);

    int updateByCondition(@Param("record") SyncGroup record, @Param("condition") SyncGroupCondition condition);

    int updateByPrimaryKeySelective(SyncGroup record);

    int updateByPrimaryKey(SyncGroup record);

	int getTotalCount();

	void deleteBatch(List list);

	int countForSearch(SyncGroupCondition condition);

	List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================

	// -- KAIST
	int updateStatusBatch(SyncGroup syncGroup);

}