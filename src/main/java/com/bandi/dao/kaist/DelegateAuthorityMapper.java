package com.bandi.dao.kaist;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bandi.domain.kaist.DelegateAuthority;
import com.bandi.domain.kaist.DelegateAuthorityCondition;

public interface DelegateAuthorityMapper {
    long countByCondition(DelegateAuthorityCondition condition);

    int deleteByCondition(DelegateAuthorityCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(DelegateAuthority record);

    int insertSelective(DelegateAuthority record);

    List<DelegateAuthority> selectByCondition(DelegateAuthorityCondition condition);

    DelegateAuthority selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") DelegateAuthority record, @Param("condition") DelegateAuthorityCondition condition);

    int updateByCondition(@Param("record") DelegateAuthority record, @Param("condition") DelegateAuthorityCondition condition);

    int updateByPrimaryKeySelective(DelegateAuthority record);

    int updateByPrimaryKey(DelegateAuthority record);

    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(DelegateAuthorityCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================

}