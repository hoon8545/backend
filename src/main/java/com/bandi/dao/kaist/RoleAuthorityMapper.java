package com.bandi.dao.kaist;

import com.bandi.domain.kaist.RoleAuthority;
import com.bandi.domain.kaist.RoleAuthorityCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RoleAuthorityMapper {
    long countByCondition(RoleAuthorityCondition condition);

    int deleteByCondition(RoleAuthorityCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(RoleAuthority record);

    int insertSelective(RoleAuthority record);

    List<RoleAuthority> selectByCondition(RoleAuthorityCondition condition);

    RoleAuthority selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") RoleAuthority record, @Param("condition") RoleAuthorityCondition condition);

    int updateByCondition(@Param("record") RoleAuthority record, @Param("condition") RoleAuthorityCondition condition);

    int updateByPrimaryKeySelective(RoleAuthority record);

    int updateByPrimaryKey(RoleAuthority record);


    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(RoleAuthorityCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================

    boolean isRoleAuthorityExisted( @Param("roleMasterOid") String roleMasterOid,  @Param("authorityOid") String authorityOid);

}
