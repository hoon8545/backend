package com.bandi.dao.kaist;

import com.bandi.domain.kaist.Terms;
import com.bandi.domain.kaist.TermsCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TermsMapper {
    long countByCondition(TermsCondition condition);

    int deleteByCondition(TermsCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(Terms record);

    int insertSelective(Terms record);

    List<Terms> selectByCondition(TermsCondition condition);

    Terms selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") Terms record, @Param("condition") TermsCondition condition);

    int updateByCondition(@Param("record") Terms record, @Param("condition") TermsCondition condition);

    int updateByPrimaryKeySelective(Terms record);

    int updateByPrimaryKey(Terms record);

    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(TermsCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================


}