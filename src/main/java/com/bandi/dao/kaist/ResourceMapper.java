package com.bandi.dao.kaist;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.bandi.domain.kaist.Resource;
import com.bandi.domain.kaist.ResourceCondition;

public interface ResourceMapper {
    long countByCondition(ResourceCondition condition);

    int deleteByCondition(ResourceCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(Resource record);

    int insertSelective(Resource record);

    List<Resource> selectByCondition(ResourceCondition condition);

    Resource selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") Resource record, @Param("condition") ResourceCondition condition);

    int updateByCondition(@Param("record") Resource record, @Param("condition") ResourceCondition condition);

    int updateByPrimaryKeySelective(Resource record);

    int updateByPrimaryKey(Resource record);

    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(ResourceCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

    // ===================== End of Code Gen =====================

    List getTreeData( String parentOid);

    void updateFullPathIndexInheritably( Map<String,String> map);

    List<String> getResourceByUser(Map<String, Object> map);

    int updateFlagUseHierarchy(@Param("record") Resource record, @Param("condition") ResourceCondition condition);

    String getDelegatorId( @Param("clientOid") String clientOid, @Param("resourceOid") String resourceOid, @Param("userId") String userId);
}
