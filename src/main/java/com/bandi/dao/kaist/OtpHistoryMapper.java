package com.bandi.dao.kaist;

import com.bandi.domain.kaist.OtpHistory;
import com.bandi.domain.kaist.OtpHistoryCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OtpHistoryMapper {
    long countByCondition(OtpHistoryCondition condition);

    int deleteByCondition(OtpHistoryCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(OtpHistory record);

    int insertSelective(OtpHistory record);

    List<OtpHistory> selectByCondition(OtpHistoryCondition condition);

    OtpHistory selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") OtpHistory record, @Param("condition") OtpHistoryCondition condition);

    int updateByCondition(@Param("record") OtpHistory record, @Param("condition") OtpHistoryCondition condition);

    int updateByPrimaryKeySelective(OtpHistory record);

    int updateByPrimaryKey(OtpHistory record);
    
    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(OtpHistoryCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================


}