package com.bandi.dao.kaist;

import com.bandi.domain.kaist.FileManager;
import com.bandi.domain.kaist.FileManagerCondition;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface FileManagerMapper {
    long countByCondition(FileManagerCondition condition);

    int deleteByCondition(FileManagerCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(FileManager record);

    int insertSelective(FileManager record);

    List<FileManager> selectByCondition(FileManagerCondition condition);

    FileManager selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") FileManager record, @Param("condition") FileManagerCondition condition);

    int updateByCondition(@Param("record") FileManager record, @Param("condition") FileManagerCondition condition);

    int updateByPrimaryKeySelective(FileManager record);

    int updateByPrimaryKey(FileManager record);
    
    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(FileManagerCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);
    
    // End of Code Gen -----------
    
    List<FileManager> selectByTargetObjectOid(String oid);
    
    void deleteByTargetObjectOid(String oid);
    
    void deleteByTargetObjectOids(List list);
    
    void updateTargetObjectOidByPrimaryKeys(Map<String, Object> map);
    
    int selectCountByTargetObjectOid(String targetObjectOid);
}