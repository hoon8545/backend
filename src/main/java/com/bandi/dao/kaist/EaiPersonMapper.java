package com.bandi.dao.kaist;

import org.apache.ibatis.annotations.Param;

import com.bandi.domain.User;

public interface EaiPersonMapper{
    boolean isExistKaistUidByEmail (@Param("kaistUid") String kaistUid, @Param("email") String email );

    boolean isExistKaistUidByPhone (@Param("kaistUid") String kaistUid, @Param("mobileTelephoneNumber") String mobileTelephoneNumber );

    User getUserByKaistUidFromEAI(String kaistUid);

    User getRegisterdUserByCI(@Param("ci") String ci);
}