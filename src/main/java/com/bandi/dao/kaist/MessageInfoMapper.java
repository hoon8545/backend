package com.bandi.dao.kaist;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bandi.domain.kaist.MessageInfo;
import com.bandi.domain.kaist.MessageInfoCondition;

public interface MessageInfoMapper {
    long countByCondition(MessageInfoCondition condition);

    int deleteByCondition(MessageInfoCondition condition);

    int deleteByPrimaryKey(String manageCode);

    int insert(MessageInfo record);

    int insertSelective(MessageInfo record);

    List<MessageInfo> selectByCondition(MessageInfoCondition condition);

    MessageInfo selectByPrimaryKey(String manageCode);

    int updateByConditionSelective(@Param("record") MessageInfo record, @Param("condition") MessageInfoCondition condition);

    int updateByCondition(@Param("record") MessageInfo record, @Param("condition") MessageInfoCondition condition);

    int updateByPrimaryKeySelective(MessageInfo record);

    int updateByPrimaryKey(MessageInfo record);

	int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(MessageInfoCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

    String getManageCode();

	// ===================== End of Code Gen =====================

}