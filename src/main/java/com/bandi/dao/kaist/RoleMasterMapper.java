package com.bandi.dao.kaist;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bandi.dao.base.PaginatorEx;
import com.bandi.domain.kaist.RoleMaster;
import com.bandi.domain.kaist.RoleMasterCondition;

public interface RoleMasterMapper {
    long countByCondition(RoleMasterCondition condition);

    int deleteByCondition(RoleMasterCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(RoleMaster record);

    int insertSelective(RoleMaster record);

    List<RoleMaster> selectByCondition(RoleMasterCondition condition);

    RoleMaster selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") RoleMaster record, @Param("condition") RoleMasterCondition condition);

    int updateByCondition(@Param("record") RoleMaster record, @Param("condition") RoleMasterCondition condition);

    int updateByPrimaryKeySelective(RoleMaster record);

    int updateByPrimaryKey(RoleMaster record);

    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(RoleMasterCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================
    int getSelfRoleTotalCount(@Param("roleMemberUserId")String roleMemberUserId, @Param("userTypes") List<String> userTypes);

    List selfRoleSearch( PaginatorEx paginatorex);

    List< String> getRoleByUser( @Param("userId") String userId, @Param("userTypes") List<String> userTypes );
}
