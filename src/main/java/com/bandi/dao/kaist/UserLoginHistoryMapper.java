package com.bandi.dao.kaist;

import com.bandi.domain.kaist.UserLoginHistory;
import com.bandi.domain.kaist.UserLoginHistoryCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserLoginHistoryMapper {
    long countByCondition(UserLoginHistoryCondition condition);

    int deleteByCondition(UserLoginHistoryCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(UserLoginHistory record);

    int insertSelective(UserLoginHistory record);

    List<UserLoginHistory> selectByCondition(UserLoginHistoryCondition condition);

    UserLoginHistory selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") UserLoginHistory record, @Param("condition") UserLoginHistoryCondition condition);

    int updateByCondition(@Param("record") UserLoginHistory record, @Param("condition") UserLoginHistoryCondition condition);

    int updateByPrimaryKeySelective(UserLoginHistory record);

    int updateByPrimaryKey(UserLoginHistory record);
    
    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(UserLoginHistoryCondition condition);
       
    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);
   
    // ===================== End of Code Gen =====================
    
    int countForSearchByEmployeeNumber(UserLoginHistory record);
    
    int countForSearchByStudentNumber(UserLoginHistory record);

    List pagingQueryForSearchByImployeeNumber( com.bandi.dao.base.PaginatorEx paginatorex);
    
    List pagingQueryForSearchByStudentNumber( com.bandi.dao.base.PaginatorEx paginatores);
}