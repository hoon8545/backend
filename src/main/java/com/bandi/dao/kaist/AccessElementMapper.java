package com.bandi.dao.kaist;

import com.bandi.domain.kaist.AccessElement;
import com.bandi.domain.kaist.AccessElementCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AccessElementMapper {
    long countByCondition(AccessElementCondition condition);

    int deleteByCondition(AccessElementCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(AccessElement record);

    int insertSelective(AccessElement record);

    List<AccessElement> selectByCondition(AccessElementCondition condition);

    AccessElement selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") AccessElement record, @Param("condition") AccessElementCondition condition);

    int updateByCondition(@Param("record") AccessElement record, @Param("condition") AccessElementCondition condition);

    int updateByPrimaryKeySelective(AccessElement record);

    int updateByPrimaryKey(AccessElement record);


    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(AccessElementCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================

    void deleteByRoleMasterOid( String roleMasterOid );

    List< String> getMappedResourceOidsByTargetObjectId( @Param("clientOid") String clientOid, @Param("targetObjectId") String targetObjectId, @Param("targetObjectType") String targetObjectType );
}
