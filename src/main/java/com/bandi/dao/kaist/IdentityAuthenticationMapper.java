package com.bandi.dao.kaist;

import com.bandi.domain.kaist.IdentityAuthentication;
import com.bandi.domain.kaist.IdentityAuthenticationCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface IdentityAuthenticationMapper {
    long countByCondition(IdentityAuthenticationCondition condition);

    int deleteByCondition(IdentityAuthenticationCondition condition);

    int deleteByPrimaryKey(String serviceType);

    int insert(IdentityAuthentication record);

    int insertSelective(IdentityAuthentication record);

    List<IdentityAuthentication> selectByCondition(IdentityAuthenticationCondition condition);

    IdentityAuthentication selectByPrimaryKey(String serviceType);

    int updateByConditionSelective(@Param("record") IdentityAuthentication record, @Param("condition") IdentityAuthenticationCondition condition);

    int updateByCondition(@Param("record") IdentityAuthentication record, @Param("condition") IdentityAuthenticationCondition condition);

    int updateByPrimaryKeySelective(IdentityAuthentication record);

    int updateByPrimaryKey(IdentityAuthentication record);
    
    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(IdentityAuthenticationCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================


}