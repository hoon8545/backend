package com.bandi.dao.kaist;

import com.bandi.domain.kaist.Authority;
import com.bandi.domain.kaist.AuthorityCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AuthorityMapper {
    long countByCondition(AuthorityCondition condition);

    int deleteByCondition(AuthorityCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(Authority record);

    int insertSelective(Authority record);

    List<Authority> selectByCondition(AuthorityCondition condition);

    Authority selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") Authority record, @Param("condition") AuthorityCondition condition);

    int updateByCondition(@Param("record") Authority record, @Param("condition") AuthorityCondition condition);

    int updateByPrimaryKeySelective(Authority record);

    int updateByPrimaryKey(Authority record);


    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(AuthorityCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

    // ===================== End of Code Gen =====================

    boolean isDuplicatedName( @Param("clientOid") String clientOid, @Param("name") String name );
}
