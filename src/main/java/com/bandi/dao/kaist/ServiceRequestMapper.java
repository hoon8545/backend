package com.bandi.dao.kaist;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bandi.domain.kaist.ServiceRequest;
import com.bandi.domain.kaist.ServiceRequestCondition;

public interface ServiceRequestMapper {
    long countByCondition(ServiceRequestCondition condition);

    int insert(ServiceRequest record);

    int insertSelective(ServiceRequest record);

    List<ServiceRequest> selectByCondition(ServiceRequestCondition condition);

    ServiceRequest selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") ServiceRequest record, @Param("condition") ServiceRequestCondition condition);

    int updateByCondition(@Param("record") ServiceRequest record, @Param("condition") ServiceRequestCondition condition);

    int updateByPrimaryKeySelective(ServiceRequest record);

    int updateByPrimaryKey(ServiceRequest record);

	int getTotalCount();

    int countForSearch(ServiceRequestCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================
    
    List<ServiceRequest> getAdminMainServiceRequest();

}