package com.bandi.dao.kaist;

import com.bandi.domain.kaist.ExtUser;
import com.bandi.domain.kaist.ExtUserCondition;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface ExtUserMapper {
    long countByCondition(ExtUserCondition condition);

    int deleteByCondition(ExtUserCondition condition);

    int deleteByPrimaryKey(String employeeUid);

    int insert(ExtUser record);

    int insertSelective(ExtUser record);

    List<ExtUser> selectByCondition(ExtUserCondition condition);

    ExtUser selectByPrimaryKey(String employeeUid);

    int updateByConditionSelective(@Param("record") ExtUser record, @Param("condition") ExtUserCondition condition);

    int updateByCondition(@Param("record") ExtUser record, @Param("condition") ExtUserCondition condition);

    int updateByPrimaryKeySelective(ExtUser record);

    int updateByPrimaryKey(ExtUser record);

    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(ExtUserCondition condition);

    int countForSearchMgmt(ExtUserCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================
    List pagingQueryForSearchMgmt( com.bandi.dao.base.PaginatorEx paginatorex);

    List<Map<String, String>> getCountryCodeList(String locale);

    ExtUser getByKaistUid(String kaistUid);

    List<ExtUser> getAdminMainExtuser();

    List<Map<String, String>> getCountryFullName(String countryCode);

}
