package com.bandi.dao.kaist;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bandi.domain.kaist.Sms;
import com.bandi.domain.kaist.SmsCondition;

public interface SmsMapper {
    long countByCondition(SmsCondition condition);

    int deleteByCondition(SmsCondition condition);

    int deleteByPrimaryKey(Short trNum);

    int insert(Sms record);

    int insertSelective(Sms record);

    List<Sms> selectByCondition(SmsCondition condition);

    Sms selectByPrimaryKey(Short trNum);

    int updateByConditionSelective(@Param("record") Sms record, @Param("example") SmsCondition condition);

    int updateByCondition(@Param("record") Sms record, @Param("example") SmsCondition condition);

    int updateByPrimaryKeySelective(Sms record);

    int updateByPrimaryKey(Sms record);
}