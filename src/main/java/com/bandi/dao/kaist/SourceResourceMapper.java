package com.bandi.dao.kaist;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bandi.domain.kaist.SourceResource;
import com.bandi.domain.kaist.SourceResourceCondition;

public interface SourceResourceMapper {
    long countByCondition(SourceResourceCondition condition);

    List<SourceResource> selectByCondition(SourceResourceCondition condition);

    List<SourceResource> selectByHierarchical( @Param("rootId") String rootId);

    List<String> getClients();
}