package com.bandi.dao.kaist;

import com.bandi.domain.kaist.DelegateHistory;
import com.bandi.domain.kaist.DelegateHistoryCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DelegateHistoryMapper {
    long countByCondition(DelegateHistoryCondition condition);

    int deleteByCondition(DelegateHistoryCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(DelegateHistory record);

    int insertSelective(DelegateHistory record);

    List<DelegateHistory> selectByCondition(DelegateHistoryCondition condition);

    DelegateHistory selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") DelegateHistory record, @Param("condition") DelegateHistoryCondition condition);

    int updateByCondition(@Param("record") DelegateHistory record, @Param("condition") DelegateHistoryCondition condition);

    int updateByPrimaryKeySelective(DelegateHistory record);

    int updateByPrimaryKey(DelegateHistory record);


    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(DelegateHistoryCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================


}
