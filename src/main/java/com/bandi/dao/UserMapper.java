package com.bandi.dao;

import java.sql.Timestamp;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bandi.domain.User;
import com.bandi.domain.UserCondition;

public interface UserMapper{
    int getTotalCount();

    long countByCondition(UserCondition condition);

    int deleteByCondition(UserCondition condition);

    int deleteByPrimaryKey(String id);

    int insert(User record);

    int insertSelective(User record);

    List<User> selectByCondition(UserCondition condition);

    User selectByPrimaryKey(String id);

    int updateByConditionSelective(@Param("record") User record, @Param("condition") UserCondition condition);

    int updateByCondition(@Param("record") User record, @Param("condition") UserCondition condition);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    void deleteBatch(List list);

    int countForSearch(UserCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

    // ===================== End of Code Gen =====================
    void updateRecordInvalid( String id);

    int modifyLoginFail( String userId);

    // -----------------------------------------------------------------------------------------------------------------
    // IM --------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------

    void updateBatchForDelete(@Param("list") List list, @Param("groupId") String groupId, @Param("updatorId") String updatorId, @Param( "updatedAt") Timestamp updatedAt);

    void updateWithFullPathIndex( @Param("fullPathIndex") String fullPathIndex, @Param("flagUse") String flagUse, @Param("updatorId") String updatorId, @Param( "updatedAt") Timestamp updatedAt);

    User getStatusActiveUser (String id);

    void updateTempUserState(@Param("list") List list, @Param("flagTemp") String flagTemp, @Param( "updatedAt") Timestamp updatedAt, @Param("updatorId") String updatorId);

    List<User> getGenericID(@Param("genericId") String genericId);

    List<User> getUsersByFullPathIndex( @Param("fullPathIndex") String fullPathIndex, @Param("clientOid") String clientOid);

    User selectByPrimaryKeyIncludeGroupName(String id);

    // -- KAIST
    User selectByKaistUid(String kaistUid);

    String getKaistUidByUserId(String userId);

    User selectByPrimaryKeyDetail(String id);

    User selectByKaistUidDetail(String kaistUid);

    List<String> getKoreanNameByCodeDetailUid(List<String> list);

    User getOriginalUserByKaistUid(String id);

    boolean isClientManager( String userId );

    boolean isGroupSupervisor( String userId );

    boolean isExtUser( String userId );
}
