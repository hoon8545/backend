package com.bandi.common.kaist;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;

import com.bandi.common.BandiProperties;

@DependsOn( value = {"applicationProperties"})
public class BandiProperties_KAIST extends BandiProperties{

    // -----------------------------------------------------------------------------------------------------------------
    // IAM.PS ----------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------

    public static String S_SITE_CODE;
    public static String S_SITE_PASSWORD;
    public static String S_AUTH_TYPE;
    public static String POPGUBUN;
    public static String CUSTOMIZE;
    public static String S_RETURN_URL;
    public static String S_ERROR_URL;
    public static String SMS_CALLBACK_CELLPHONE;
    public static String WINDOW_FILE_PATH_NOTICE;
    public static String WINDOW_FILE_PATH_USER;
    public static String JUSO_RETURN_URL;
    public static String JUSO_CONFMKEY;
    public static String JUSO_RESULT_TYPE;
    public static String PROPERTIES_MODE;
    public static String MAIL_SMTP_CCMODE;
    public static String MAIL_SMTP_CCADDRESS;
    public static String CALLBACK_PHONE_NUMBER;
    public static String OTP_UNLOCK_URL;
    public static String MAIL_SUPPORT_USERNAME;
    public static String MAIL_SUPPORT_PASSWORD;

    /* AES(나중에 삭제) Start */
    public static String AES_KEY;
    /* AES(나중에 삭제) End */

    /* OIM(나중에 삭제) Start */
    public static String OIMSERVER_HOSTNAME;
    public static String OIMSERVER_PORT;
    public static String OIMSERVER_PROTOCOL;
    public static String OIMSERVER_TYPE;
    public static String SOAPREQUEST_PATH;
    public static String SOAPRESPONSE_PATH;
    public static String OIMSERVER_XELADMIN;
    public static String OIMSERVER_XELPASSWORD;
    /* OIM(나중에 삭제) End */

    /* LDAP(나중에 삭제) Start */
    public static String LDAPSERVER_LDAPURL;
    public static String LDAPSERVER_LDAPUSER;
    public static String LDAPSERVER_LDAPPASS;
    /* LDAP(나중에 삭제) End */

    public static boolean IS_TEST_WITHOUT_INITECH;
    public static String OTP_REGISTRATION_URL;
    public static String OTP_CHECK_STATUS_URL;
    public static String OTP_CHECK_URL;
    public static String OTP_LONG_TERM_NONUSER_URL;
    public static String OTP_AUTHORIZATION_FAIL_COUNT_URL;
    public static String OTP_NEW_USER_SYNCHRONIZE_URL;

    public static boolean IS_TEST_ORG_SYNC;

    @Value("#{applicationProperties['mobileauth.sSiteCode']}")
    public void setSSiteCode( String sSiteCode ){
        S_SITE_CODE = sSiteCode;
    }

    @Value("#{applicationProperties['mobileauth.sSitePassword']}")
    public void setSSitePassword( String sSitePassword ){
        S_SITE_PASSWORD = sSitePassword;
    }

    @Value("#{applicationProperties['mobileauth.sAuthType']}")
    public void setSAuthType( String sAuthType ){
        S_AUTH_TYPE = sAuthType;
    }

    @Value("#{applicationProperties['mobileauth.popgubun']}")
    public void setPopgubun( String popgubun ){
        POPGUBUN = popgubun;
    }

    @Value("#{applicationProperties['mobileauth.customize']}")
    public void setCustomize( String customize ){
        CUSTOMIZE = customize;
    }


    @Value("#{applicationProperties['mobileauth.sReturnUrl']}")
    public void setSReturnUrl( String sReturnUrl ){
        S_RETURN_URL = sReturnUrl;
    }

    @Value("#{applicationProperties['mobileauth.sErrorUrl']}")
    public void setErrorUrl( String sErrorUrl ){
        S_ERROR_URL = sErrorUrl;
    }

    @Value("#{applicationProperties['juso.returnUrl']}")
    public void setJusoReturnUrl( String jusoReturnUrl ) {
        JUSO_RETURN_URL = jusoReturnUrl;
    }

    @Value("#{applicationProperties['juso.confmKey']}")
    public void setJusoConfmKey( String jusoConfmKey ) {
        JUSO_CONFMKEY = jusoConfmKey;
    }

    @Value("#{applicationProperties['juso.resultType']}")
    public void setResultType( String jusoResultType ) {
        JUSO_RESULT_TYPE = jusoResultType;
    }

    @Value("#{applicationProperties['mail.mode']}")
    public void setMode( String mode ){
        PROPERTIES_MODE = mode;
    }

    @Value("#{applicationProperties['mail.smtp.ccmode']}")
    public void setCcMode( String ccMode ){
        MAIL_SMTP_CCMODE = ccMode;
    }

    @Value("#{applicationProperties['mail.smtp.cc.address']}")
    public void setCcAddress( String ccAddress ){
        MAIL_SMTP_CCADDRESS = ccAddress;
    }

    @Value("#{applicationProperties['sms.callback.cellphone']}")
    public void setCallbackPhoneNumber( String callbackPhoneNumber ){
        CALLBACK_PHONE_NUMBER = callbackPhoneNumber;
    }

    /* AES(나중에 삭제) Start */
    @Value("#{applicationProperties['aes.key']}")
    public void setAesKey( String aesKey ) {
        AES_KEY = aesKey;
    }
    /* AES(나중에 삭제) End */

    /* OIM(나중에 삭제) Start */
    @Value("#{applicationProperties['oimserver.hostname']}")
    public void setOimserverHostname( String oimserverHostname ) {
    	OIMSERVER_HOSTNAME = oimserverHostname;
    }

    @Value("#{applicationProperties['oimserver.port']}")
    public void setOimserverPort( String oimserverPort ) {
    	OIMSERVER_PORT = oimserverPort;
    }

    @Value("#{applicationProperties['oimserver.protocol']}")
    public void setOimserverProtocol( String oimserverProtocol ) {
    	OIMSERVER_PROTOCOL = oimserverProtocol;
    }

    @Value("#{applicationProperties['oimserver.type']}")
    public void setOimserverType( String oimserverType ) {
    	OIMSERVER_TYPE = oimserverType;
    }

    @Value("#{applicationProperties['soaprequest.path']}")
    public void setSoaprequestPath( String soaprequestPath ) {
    	SOAPREQUEST_PATH = soaprequestPath;
    }

    @Value("#{applicationProperties['soapresponse.path']}")
    public void setSoapresponsePath( String soapresponsePath ) {
    	SOAPRESPONSE_PATH = soapresponsePath;
    }

    @Value("${oimserver.xeladmin}")
    public void setOimserverXeladmin( String oimserverXeladmin ){
    	OIMSERVER_XELADMIN = oimserverXeladmin;
    }

    @Value("${oimserver.xelpassword}")
    public void setOimserverXelpassword( String oimserverXelpassword ) {
    	OIMSERVER_XELPASSWORD = oimserverXelpassword;
    }
    /* OIM(나중에 삭제) End */

    /* LDAP(나중에 삭제) Start */
    @Value("#{applicationProperties['ldapserver.ldapurl']}")
    public void setLdapserverLdapurl( String ldapserverLdapurl ) {
    	LDAPSERVER_LDAPURL = ldapserverLdapurl;
    }

    @Value("${ldapserver.ldapuser}")
    public void setLdapserverLdapuser( String ldapserverLdapuser ) {
    	LDAPSERVER_LDAPUSER = ldapserverLdapuser;
    }

    @Value("${ldapserver.ldappass}")
    public void setLdapserverLdappass( String ldapserverLdappass ) {
    	LDAPSERVER_LDAPPASS = ldapserverLdappass;
    }
    /* LDAP(나중에 삭제) End */


    @Value("#{applicationProperties['otp.unlock.url']}")
    public void setOtpUnlockUrl( String otpUnlockUrl ) {
    	OTP_UNLOCK_URL = otpUnlockUrl;
    }

    @Value("#{applicationProperties['window.file.path.notice']}")
    public void setWindowFilePathNotice( String windowFilePathNotice ) {
    	WINDOW_FILE_PATH_NOTICE = windowFilePathNotice;
    }

    @Value("#{applicationProperties['window.file.path.user']}")
    public void setWindowFilePathUser( String windowFilePathUser ) {
    	WINDOW_FILE_PATH_USER = windowFilePathUser;
    }

    @Value("${mail.support.username}")
    public void setSupportUserName( String userName ){
        MAIL_SUPPORT_USERNAME = userName.trim();
    }

    @Value("${mail.support.password}")
    public void setSupportPwd( String password ){
        MAIL_SUPPORT_PASSWORD = password.trim();
    }

    @Value("#{applicationProperties['sso.without.initech'] ?: false}")
    public void setTestWithoutInitech( boolean isSsoWithoutInitech ){
        IS_TEST_WITHOUT_INITECH = isSsoWithoutInitech;
    }

    @Value("#{applicationProperties['otp.registraion.url']}")
    public void setOtpRegistration( String otpRegistration ) {
        OTP_REGISTRATION_URL = otpRegistration;
    }

    @Value("#{applicationProperties['otp.checkStatus.url']}")
    public void setOtpCheckStatus( String otpCheckStatus ) {
        OTP_CHECK_STATUS_URL = otpCheckStatus;
    }

    @Value("#{applicationProperties['otp.check.url']}")
    public void setOtpCheck( String otpCheck ) {
        OTP_CHECK_URL = otpCheck;
    }

    @Value("#{applicationProperties['otp.longTermNonUser.url']}")
    public void setLongTermNonUser( String otpLongTermUser ) {
    	OTP_LONG_TERM_NONUSER_URL = otpLongTermUser;
    }

    @Value("#{applicationProperties['otp.failCount.url']}")
    public void setAuthorizationFailCount( String otpFailCount ) {
    	OTP_AUTHORIZATION_FAIL_COUNT_URL = otpFailCount;
    }
    
    @Value("#{applicationProperties['otp.synchronize.url']}")
    public void setNewUserSynchronize( String newUserSynchronize ) {
    	OTP_NEW_USER_SYNCHRONIZE_URL = newUserSynchronize;
    }

    @Override
    @Value("#{applicationProperties['user.password.encrypt.type'] ?: 'SHA512'}")
    public void setUserPasswordEncryptType( String encryptType ){
        super.setUserPasswordEncryptType( encryptType );
    }

    @Value("#{applicationProperties['orgSync.test.local'] ?: false}")
    public void setTestOrgSync( boolean isTestOrgSync ){
        IS_TEST_ORG_SYNC = isTestOrgSync;
    }
}
