package com.bandi.common.util.bpel.domain;

import java.util.Date;

public class ImUserVO {
	 
	private String emplid;
	private String sso_id;
	private String sso_pw;
	private String name;
	private String name_ac;
	private String last_name;
	private String first_name;
	private String birthdate;
	private String country;
	private String national_id;
	private String sex;
	private String email_addr;
	private String ch_mail;
	private String cell_phone;
	private String busn_phone;
	private String home_phone;
	private String fax_phone;
	private String postal;
	private String address1;
	private String address2;
	private String semplid;
	private String ebs_personid;
	private String employee_number;
	private String std_no;
	private String acad_org;
	private String acad_name;
	private String campus;
	private String college_eng;
	private String college_kor;
	private String grad_eng;
	private String grad_kor;
	private String kaist_org_id;
	private String ebs_organization_id;
	private String ebs_org_name_eng;
	private String ebs_org_name_kor;
	private String ebs_grade_name_eng;
	private String ebs_grade_name_kor;
	private String ebs_grade_level_eng;
	private String ebs_grade_level_kor;
	private String ebs_person_type_eng;
	private String ebs_person_type;
	private String ebs_user_status_flag;
	private String ebs_user_status_kor;
	private String position_eng;
	private String position_kor;
	private String stu_status_flag;
	private String stu_status_kor;
	private String acad_prog_code;
	private String acad_prog_kor;
	private String acad_prog_eng;
	private String person_gubun;
	private String prog_effdt;
	private String stdnt_type_id;
	private String stdnt_type_class;
	private String stdnt_type_name_eng;
	private String stdnt_type_name_kor;
	private String stdnt_category_id;
	private String stdnt_category_name_eng;
	private String stdnt_category_name_kor;
	private String bank_sect;
	private String account_no;
	private String advr_emplid;
	private String advr_ebs_person_id;
	private String advr_first_name;
	private String advr_last_name;
	private String advr_name;
	private String advr_name_ac;
	private String ebs_start_date;
	private String ebs_end_date;
	private String prog_start_date;
	private String prog_end_date;
	private String outer_start_date;
	private String outer_end_date;
	private String register_date;
	private String register_ip;
	private String ext_org_id;
	private String ext_etc_org_name_eng;
	private String ext_etc_org_name_kor;
	private String ext_org_name_eng;
	private String ext_org_name_kor;
	private String combinded_name;
	private String combinded_status;
	private String granted_services;
	private String address_type;
	private String acad_kst_org_id;
	private String acad_ebs_org_id;
	private String acad_ebs_org_name_eng;
	private String acad_ebs_org_name_kor;
	private String fore_acad_prog_code;
	private String fore_acad_prog_kor;
	private String fore_acad_prog_eng;
	private String fore_std_no;
	private String fore_acad_org;
	private String fore_acad_name;
	private String fore_acad_kst_org_id;
	private String fore_acad_ebs_org_id;
	private String fore_acad_ebs_org_name_eng;
	private String fore_acad_ebs_org_name_kor;
	private String fore_campus;
	private String fore_stu_status_eng;
	private String fore_stu_status_kor;
	private String fore_prog_start_date;
	private String fore_prog_end_date;
	private String reg_type;
	private String ebs_person_type_kor;
	private String ebs_user_status_eng;
	private String stu_status_code;
	private String stu_status_eng;
	private String fore_stu_status_code;
	private Date lastupddttm;
	private String slastupddttm;
	private String i_pin;
	private String highschool_code;
	private String highschool_name;
	private String kaist_uid;
	private String kaist_suid;
	private String cl_campus;
	private String cl_building;
	private String cl_floor;
	private String cl_room_no;
	private String advr_kaist_uid;
	private String comments;
	private String result;

	public String getEmplid() {
		return emplid;
	}
	public void setEmplid(String emplid) {
		this.emplid = emplid;
	}
	public String getSso_id() {
		return sso_id;
	}
	public void setSso_id(String sso_id) {
		this.sso_id = sso_id;
	}
	public String getSso_pw() {
		return sso_pw;
	}
	public void setSso_pw(String sso_pw) {
		this.sso_pw = sso_pw;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName_ac() {
		return name_ac;
	}
	public void setName_ac(String name_ac) {
		this.name_ac = name_ac;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getNational_id() {
		return national_id;
	}
	public void setNational_id(String national_id) {
		this.national_id = national_id;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getEmail_addr() {
		return email_addr;
	}
	public void setEmail_addr(String email_addr) {
		this.email_addr = email_addr;
	}
	public String getCh_mail() {
		return ch_mail;
	}
	public void setCh_mail(String ch_mail) {
		this.ch_mail = ch_mail;
	}
	public String getCell_phone() {
		return cell_phone;
	}
	public void setCell_phone(String cell_phone) {
		this.cell_phone = cell_phone;
	}
	public String getBusn_phone() {
		return busn_phone;
	}
	public void setBusn_phone(String busn_phone) {
		this.busn_phone = busn_phone;
	}
	public String getHome_phone() {
		return home_phone;
	}
	public void setHome_phone(String home_phone) {
		this.home_phone = home_phone;
	}
	public String getFax_phone() {
		return fax_phone;
	}
	public void setFax_phone(String fax_phone) {
		this.fax_phone = fax_phone;
	}
	public String getPostal() {
		return postal;
	}
	public void setPostal(String postal) {
		this.postal = postal;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getSemplid() {
		return semplid;
	}
	public void setSemplid(String semplid) {
		this.semplid = semplid;
	}
	public String getEbs_personid() {
		return ebs_personid;
	}
	public void setEbs_personid(String ebs_personid) {
		this.ebs_personid = ebs_personid;
	}
	public String getEmployee_number() {
		return employee_number;
	}
	public void setEmployee_number(String employee_number) {
		this.employee_number = employee_number;
	}
	public String getStd_no() {
		return std_no;
	}
	public void setStd_no(String std_no) {
		this.std_no = std_no;
	}
	public String getAcad_org() {
		return acad_org;
	}
	public void setAcad_org(String acad_org) {
		this.acad_org = acad_org;
	}
	public String getAcad_name() {
		return acad_name;
	}
	public void setAcad_name(String acad_name) {
		this.acad_name = acad_name;
	}
	public String getCampus() {
		return campus;
	}
	public void setCampus(String campus) {
		this.campus = campus;
	}
	public String getCollege_eng() {
		return college_eng;
	}
	public void setCollege_eng(String college_eng) {
		this.college_eng = college_eng;
	}
	public String getCollege_kor() {
		return college_kor;
	}
	public void setCollege_kor(String college_kor) {
		this.college_kor = college_kor;
	}
	public String getGrad_eng() {
		return grad_eng;
	}
	public void setGrad_eng(String grad_eng) {
		this.grad_eng = grad_eng;
	}
	public String getGrad_kor() {
		return grad_kor;
	}
	public void setGrad_kor(String grad_kor) {
		this.grad_kor = grad_kor;
	}
	public String getKaist_org_id() {
		return kaist_org_id;
	}
	public void setKaist_org_id(String kaist_org_id) {
		this.kaist_org_id = kaist_org_id;
	}
	public String getEbs_organization_id() {
		return ebs_organization_id;
	}
	public void setEbs_organization_id(String ebs_organization_id) {
		this.ebs_organization_id = ebs_organization_id;
	}
	public String getEbs_org_name_eng() {
		return ebs_org_name_eng;
	}
	public void setEbs_org_name_eng(String ebs_org_name_eng) {
		this.ebs_org_name_eng = ebs_org_name_eng;
	}
	public String getEbs_org_name_kor() {
		return ebs_org_name_kor;
	}
	public void setEbs_org_name_kor(String ebs_org_name_kor) {
		this.ebs_org_name_kor = ebs_org_name_kor;
	}
	public String getEbs_grade_name_eng() {
		return ebs_grade_name_eng;
	}
	public void setEbs_grade_name_eng(String ebs_grade_name_eng) {
		this.ebs_grade_name_eng = ebs_grade_name_eng;
	}
	public String getEbs_grade_name_kor() {
		return ebs_grade_name_kor;
	}
	public void setEbs_grade_name_kor(String ebs_grade_name_kor) {
		this.ebs_grade_name_kor = ebs_grade_name_kor;
	}
	public String getEbs_grade_level_eng() {
		return ebs_grade_level_eng;
	}
	public void setEbs_grade_level_eng(String ebs_grade_level_eng) {
		this.ebs_grade_level_eng = ebs_grade_level_eng;
	}
	public String getEbs_grade_level_kor() {
		return ebs_grade_level_kor;
	}
	public void setEbs_grade_level_kor(String ebs_grade_level_kor) {
		this.ebs_grade_level_kor = ebs_grade_level_kor;
	}
	public String getEbs_person_type_eng() {
		return ebs_person_type_eng;
	}
	public void setEbs_person_type_eng(String ebs_person_type_eng) {
		this.ebs_person_type_eng = ebs_person_type_eng;
	}
	public String getEbs_person_type() {
		return ebs_person_type;
	}
	public void setEbs_person_type(String ebs_person_type) {
		this.ebs_person_type = ebs_person_type;
	}
	public String getEbs_user_status_flag() {
		return ebs_user_status_flag;
	}
	public void setEbs_user_status_flag(String ebs_user_status_flag) {
		this.ebs_user_status_flag = ebs_user_status_flag;
	}
	public String getEbs_user_status_kor() {
		return ebs_user_status_kor;
	}
	public void setEbs_user_status_kor(String ebs_user_status_kor) {
		this.ebs_user_status_kor = ebs_user_status_kor;
	}
	public String getPosition_eng() {
		return position_eng;
	}
	public void setPosition_eng(String position_eng) {
		this.position_eng = position_eng;
	}
	public String getPosition_kor() {
		return position_kor;
	}
	public void setPosition_kor(String position_kor) {
		this.position_kor = position_kor;
	}
	public String getStu_status_flag() {
		return stu_status_flag;
	}
	public void setStu_status_flag(String stu_status_flag) {
		this.stu_status_flag = stu_status_flag;
	}
	public String getStu_status_kor() {
		return stu_status_kor;
	}
	public void setStu_status_kor(String stu_status_kor) {
		this.stu_status_kor = stu_status_kor;
	}
	public String getAcad_prog_code() {
		return acad_prog_code;
	}
	public void setAcad_prog_code(String acad_prog_code) {
		this.acad_prog_code = acad_prog_code;
	}
	public String getAcad_prog_kor() {
		return acad_prog_kor;
	}
	public void setAcad_prog_kor(String acad_prog_kor) {
		this.acad_prog_kor = acad_prog_kor;
	}
	public String getAcad_prog_eng() {
		return acad_prog_eng;
	}
	public void setAcad_prog_eng(String acad_prog_eng) {
		this.acad_prog_eng = acad_prog_eng;
	}
	public String getPerson_gubun() {
		return person_gubun;
	}
	public void setPerson_gubun(String person_gubun) {
		this.person_gubun = person_gubun;
	}
	public String getProg_effdt() {
		return prog_effdt;
	}
	public void setProg_effdt(String prog_effdt) {
		this.prog_effdt = prog_effdt;
	}
	public String getStdnt_type_id() {
		return stdnt_type_id;
	}
	public void setStdnt_type_id(String stdnt_type_id) {
		this.stdnt_type_id = stdnt_type_id;
	}
	public String getStdnt_type_class() {
		return stdnt_type_class;
	}
	public void setStdnt_type_class(String stdnt_type_class) {
		this.stdnt_type_class = stdnt_type_class;
	}
	public String getStdnt_type_name_eng() {
		return stdnt_type_name_eng;
	}
	public void setStdnt_type_name_eng(String stdnt_type_name_eng) {
		this.stdnt_type_name_eng = stdnt_type_name_eng;
	}
	public String getStdnt_type_name_kor() {
		return stdnt_type_name_kor;
	}
	public void setStdnt_type_name_kor(String stdnt_type_name_kor) {
		this.stdnt_type_name_kor = stdnt_type_name_kor;
	}
	public String getStdnt_category_id() {
		return stdnt_category_id;
	}
	public void setStdnt_category_id(String stdnt_category_id) {
		this.stdnt_category_id = stdnt_category_id;
	}
	public String getStdnt_category_name_eng() {
		return stdnt_category_name_eng;
	}
	public void setStdnt_category_name_eng(String stdnt_category_name_eng) {
		this.stdnt_category_name_eng = stdnt_category_name_eng;
	}
	public String getStdnt_category_name_kor() {
		return stdnt_category_name_kor;
	}
	public void setStdnt_category_name_kor(String stdnt_category_name_kor) {
		this.stdnt_category_name_kor = stdnt_category_name_kor;
	}
	public String getBank_sect() {
		return bank_sect;
	}
	public void setBank_sect(String bank_sect) {
		this.bank_sect = bank_sect;
	}
	public String getAccount_no() {
		return account_no;
	}
	public void setAccount_no(String account_no) {
		this.account_no = account_no;
	}
	public String getAdvr_emplid() {
		return advr_emplid;
	}
	public void setAdvr_emplid(String advr_emplid) {
		this.advr_emplid = advr_emplid;
	}
	public String getAdvr_ebs_person_id() {
		return advr_ebs_person_id;
	}
	public void setAdvr_ebs_person_id(String advr_ebs_person_id) {
		this.advr_ebs_person_id = advr_ebs_person_id;
	}
	public String getAdvr_first_name() {
		return advr_first_name;
	}
	public void setAdvr_first_name(String advr_first_name) {
		this.advr_first_name = advr_first_name;
	}
	public String getAdvr_last_name() {
		return advr_last_name;
	}
	public void setAdvr_last_name(String advr_last_name) {
		this.advr_last_name = advr_last_name;
	}
	public String getAdvr_name() {
		return advr_name;
	}
	public void setAdvr_name(String advr_name) {
		this.advr_name = advr_name;
	}
	public String getAdvr_name_ac() {
		return advr_name_ac;
	}
	public void setAdvr_name_ac(String advr_name_ac) {
		this.advr_name_ac = advr_name_ac;
	}
	public String getEbs_start_date() {
		return ebs_start_date;
	}
	public void setEbs_start_date(String ebs_start_date) {
		this.ebs_start_date = ebs_start_date;
	}
	public String getEbs_end_date() {
		return ebs_end_date;
	}
	public void setEbs_end_date(String ebs_end_date) {
		this.ebs_end_date = ebs_end_date;
	}
	public String getProg_start_date() {
		return prog_start_date;
	}
	public void setProg_start_date(String prog_start_date) {
		this.prog_start_date = prog_start_date;
	}
	public String getProg_end_date() {
		return prog_end_date;
	}
	public void setProg_end_date(String prog_end_date) {
		this.prog_end_date = prog_end_date;
	}
	public String getOuter_start_date() {
		return outer_start_date;
	}
	public void setOuter_start_date(String outer_start_date) {
		this.outer_start_date = outer_start_date;
	}
	public String getOuter_end_date() {
		return outer_end_date;
	}
	public void setOuter_end_date(String outer_end_date) {
		this.outer_end_date = outer_end_date;
	}
	public String getRegister_date() {
		return register_date;
	}
	public void setRegister_date(String register_date) {
		this.register_date = register_date;
	}
	public String getRegister_ip() {
		return register_ip;
	}
	public void setRegister_ip(String register_ip) {
		this.register_ip = register_ip;
	}
	public String getExt_org_id() {
		return ext_org_id;
	}
	public void setExt_org_id(String ext_org_id) {
		this.ext_org_id = ext_org_id;
	}
	public String getExt_etc_org_name_eng() {
		return ext_etc_org_name_eng;
	}
	public void setExt_etc_org_name_eng(String ext_etc_org_name_eng) {
		this.ext_etc_org_name_eng = ext_etc_org_name_eng;
	}
	public String getExt_etc_org_name_kor() {
		return ext_etc_org_name_kor;
	}
	public void setExt_etc_org_name_kor(String ext_etc_org_name_kor) {
		this.ext_etc_org_name_kor = ext_etc_org_name_kor;
	}
	public String getExt_org_name_eng() {
		return ext_org_name_eng;
	}
	public void setExt_org_name_eng(String ext_org_name_eng) {
		this.ext_org_name_eng = ext_org_name_eng;
	}
	public String getExt_org_name_kor() {
		return ext_org_name_kor;
	}
	public void setExt_org_name_kor(String ext_org_name_kor) {
		this.ext_org_name_kor = ext_org_name_kor;
	}
	public String getCombinded_name() {
		return combinded_name;
	}
	public void setCombinded_name(String combinded_name) {
		this.combinded_name = combinded_name;
	}
	public String getCombinded_status() {
		return combinded_status;
	}
	public void setCombinded_status(String combinded_status) {
		this.combinded_status = combinded_status;
	}
	public String getGranted_services() {
		return granted_services;
	}
	public void setGranted_services(String granted_services) {
		this.granted_services = granted_services;
	}
	public String getAddress_type() {
		return address_type;
	}
	public void setAddress_type(String address_type) {
		this.address_type = address_type;
	}
	public String getAcad_kst_org_id() {
		return acad_kst_org_id;
	}
	public void setAcad_kst_org_id(String acad_kst_org_id) {
		this.acad_kst_org_id = acad_kst_org_id;
	}
	public String getAcad_ebs_org_id() {
		return acad_ebs_org_id;
	}
	public void setAcad_ebs_org_id(String acad_ebs_org_id) {
		this.acad_ebs_org_id = acad_ebs_org_id;
	}
	public String getAcad_ebs_org_name_eng() {
		return acad_ebs_org_name_eng;
	}
	public void setAcad_ebs_org_name_eng(String acad_ebs_org_name_eng) {
		this.acad_ebs_org_name_eng = acad_ebs_org_name_eng;
	}
	public String getAcad_ebs_org_name_kor() {
		return acad_ebs_org_name_kor;
	}
	public void setAcad_ebs_org_name_kor(String acad_ebs_org_name_kor) {
		this.acad_ebs_org_name_kor = acad_ebs_org_name_kor;
	}
	public String getFore_acad_prog_code() {
		return fore_acad_prog_code;
	}
	public void setFore_acad_prog_code(String fore_acad_prog_code) {
		this.fore_acad_prog_code = fore_acad_prog_code;
	}
	public String getFore_acad_prog_kor() {
		return fore_acad_prog_kor;
	}
	public void setFore_acad_prog_kor(String fore_acad_prog_kor) {
		this.fore_acad_prog_kor = fore_acad_prog_kor;
	}
	public String getFore_acad_prog_eng() {
		return fore_acad_prog_eng;
	}
	public void setFore_acad_prog_eng(String fore_acad_prog_eng) {
		this.fore_acad_prog_eng = fore_acad_prog_eng;
	}
	public String getFore_std_no() {
		return fore_std_no;
	}
	public void setFore_std_no(String fore_std_no) {
		this.fore_std_no = fore_std_no;
	}
	public String getFore_acad_org() {
		return fore_acad_org;
	}
	public void setFore_acad_org(String fore_acad_org) {
		this.fore_acad_org = fore_acad_org;
	}
	public String getFore_acad_name() {
		return fore_acad_name;
	}
	public void setFore_acad_name(String fore_acad_name) {
		this.fore_acad_name = fore_acad_name;
	}
	public String getFore_acad_kst_org_id() {
		return fore_acad_kst_org_id;
	}
	public void setFore_acad_kst_org_id(String fore_acad_kst_org_id) {
		this.fore_acad_kst_org_id = fore_acad_kst_org_id;
	}
	public String getFore_acad_ebs_org_id() {
		return fore_acad_ebs_org_id;
	}
	public void setFore_acad_ebs_org_id(String fore_acad_ebs_org_id) {
		this.fore_acad_ebs_org_id = fore_acad_ebs_org_id;
	}
	public String getFore_acad_ebs_org_name_eng() {
		return fore_acad_ebs_org_name_eng;
	}
	public void setFore_acad_ebs_org_name_eng(String fore_acad_ebs_org_name_eng) {
		this.fore_acad_ebs_org_name_eng = fore_acad_ebs_org_name_eng;
	}
	public String getFore_acad_ebs_org_name_kor() {
		return fore_acad_ebs_org_name_kor;
	}
	public void setFore_acad_ebs_org_name_kor(String fore_acad_ebs_org_name_kor) {
		this.fore_acad_ebs_org_name_kor = fore_acad_ebs_org_name_kor;
	}
	public String getFore_campus() {
		return fore_campus;
	}
	public void setFore_campus(String fore_campus) {
		this.fore_campus = fore_campus;
	}
	public String getFore_stu_status_eng() {
		return fore_stu_status_eng;
	}
	public void setFore_stu_status_eng(String fore_stu_status_eng) {
		this.fore_stu_status_eng = fore_stu_status_eng;
	}
	public String getFore_stu_status_kor() {
		return fore_stu_status_kor;
	}
	public void setFore_stu_status_kor(String fore_stu_status_kor) {
		this.fore_stu_status_kor = fore_stu_status_kor;
	}
	public String getFore_prog_start_date() {
		return fore_prog_start_date;
	}
	public void setFore_prog_start_date(String fore_prog_start_date) {
		this.fore_prog_start_date = fore_prog_start_date;
	}
	public String getFore_prog_end_date() {
		return fore_prog_end_date;
	}
	public void setFore_prog_end_date(String fore_prog_end_date) {
		this.fore_prog_end_date = fore_prog_end_date;
	}
	public String getEbs_person_type_kor() {
		return ebs_person_type_kor;
	}
	public void setEbs_person_type_kor(String ebs_person_type_kor) {
		this.ebs_person_type_kor = ebs_person_type_kor;
	}
	public String getEbs_user_status_eng() {
		return ebs_user_status_eng;
	}
	public void setEbs_user_status_eng(String ebs_user_status_eng) {
		this.ebs_user_status_eng = ebs_user_status_eng;
	}
	public String getStu_status_code() {
		return stu_status_code;
	}
	public void setStu_status_code(String stu_status_code) {
		this.stu_status_code = stu_status_code;
	}
	public String getStu_status_eng() {
		return stu_status_eng;
	}
	public void setStu_status_eng(String stu_status_eng) {
		this.stu_status_eng = stu_status_eng;
	}
	public String getFore_stu_status_code() {
		return fore_stu_status_code;
	}
	public void setFore_stu_status_code(String fore_stu_status_code) {
		this.fore_stu_status_code = fore_stu_status_code;
	}
	public Date getLastupddttm() {
		return lastupddttm;
	}
	public void setLastupddttm(Date lastupddttm) {
		this.lastupddttm = lastupddttm;
	}
	public String getSlastupddttm() {
		return slastupddttm;
	}
	public void setSlastupddttm(String slastupddttm) {
		this.slastupddttm = slastupddttm;
	}
	public String getI_pin() {
		return i_pin;
	}
	public void setI_pin(String i_pin) {
		this.i_pin = i_pin;
	}
	public String getHighschool_code() {
		return highschool_code;
	}
	public void setHighschool_code(String highschool_code) {
		this.highschool_code = highschool_code;
	}
	public String getHighschool_name() {
		return highschool_name;
	}
	public void setHighschool_name(String highschool_name) {
		this.highschool_name = highschool_name;
	}
	public String getKaist_uid() {
		return kaist_uid;
	}
	public void setKaist_uid(String kaist_uid) {
		this.kaist_uid = kaist_uid;
	}
	public String getKaist_suid() {
		return kaist_suid;
	}
	public void setKaist_suid(String kaist_suid) {
		this.kaist_suid = kaist_suid;
	}
	public String getCl_campus() {
		return cl_campus;
	}
	public void setCl_campus(String cl_campus) {
		this.cl_campus = cl_campus;
	}
	public String getCl_building() {
		return cl_building;
	}
	public void setCl_building(String cl_building) {
		this.cl_building = cl_building;
	}
	public String getCl_floor() {
		return cl_floor;
	}
	public void setCl_floor(String cl_floor) {
		this.cl_floor = cl_floor;
	}
	public String getCl_room_no() {
		return cl_room_no;
	}
	public void setCl_room_no(String cl_room_no) {
		this.cl_room_no = cl_room_no;
	}
	public String getAdvr_kaist_uid() {
		return advr_kaist_uid;
	}
	public void setAdvr_kaist_uid(String advr_kaist_uid) {
		this.advr_kaist_uid = advr_kaist_uid;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getResult() {
		return result;
	}
	public String getReg_type() {
		return reg_type;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public void setReg_type(String reg_type) {
		this.reg_type = reg_type;
	}
	
	/* only use for procedure */
	private String is_id_exist;
	private String birthcountry;
	private String mar_status;
	private Date mar_status_dt;
	private String highest_educ_lvl;
	private String ft_student;
	private String lang_cd;
	private String alter_emplid;
	private String lastupdoprid;
	private String national_id_type;
	private String ssn_key;
	private String primary_nid;
	private String tax_ref_id_sgp;
	private String eff_status;
	private String country_nm_format;
	private String name_type;
	private String name_initials;
	private String name_prefix;
	private String name_suffix;
	private String name_royal_prefix;
	private String name_royal_suffix; 
	private String name_title;
	private String last_name_srch;
	private String first_name_srch;
	private String middle_name;
	private String second_last_name;
	private String second_last_srch;
	private String pref_first_name;
	private String partner_last_name;
	private String partner_roy_prefix;
	private String last_name_pref_nid;
	private String name_display;
	private String name_formal;
	private String name_display_srch;
	private String phone_type;
	private String country_code;
	private String phone;
	private String extension;
	private String pref_phone_flag;
	private Date effdt;
	
	public Date getEffdt() {
		return effdt;
	}
	public void setEffdt(Date effdt) {
		this.effdt = effdt;
	}
	public String getIs_id_exist() {
		return is_id_exist;
	}
	public void setIs_id_exist(String is_id_exist) {
		this.is_id_exist = is_id_exist;
	}
	public String getBirthcountry() {
		return birthcountry;
	}
	public void setBirthcountry(String birthcountry) {
		this.birthcountry = birthcountry;
	}
	public String getMar_status() {
		return mar_status;
	}
	public void setMar_status(String mar_status) {
		this.mar_status = mar_status;
	}
	public Date getMar_status_dt() {
		return mar_status_dt;
	}
	public void setMar_status_dt(Date mar_status_dt) {
		this.mar_status_dt = mar_status_dt;
	}
	public String getHighest_educ_lvl() {
		return highest_educ_lvl;
	}
	public void setHighest_educ_lvl(String highest_educ_lvl) {
		this.highest_educ_lvl = highest_educ_lvl;
	}
	public String getFt_student() {
		return ft_student;
	}
	public void setFt_student(String ft_student) {
		this.ft_student = ft_student;
	}
	public String getLang_cd() {
		return lang_cd;
	}
	public void setLang_cd(String lang_cd) {
		this.lang_cd = lang_cd;
	}
	public String getAlter_emplid() {
		return alter_emplid;
	}
	public void setAlter_emplid(String alter_emplid) {
		this.alter_emplid = alter_emplid;
	}
	public String getLastupdoprid() {
		return lastupdoprid;
	}
	public void setLastupdoprid(String lastupdoprid) {
		this.lastupdoprid = lastupdoprid;
	}
	public String getNational_id_type() {
		return national_id_type;
	}
	public void setNational_id_type(String national_id_type) {
		this.national_id_type = national_id_type;
	}
	public String getSsn_key() {
		return ssn_key;
	}
	public void setSsn_key(String ssn_key) {
		this.ssn_key = ssn_key;
	}
	public String getPrimary_nid() {
		return primary_nid;
	}
	public void setPrimary_nid(String primary_nid) {
		this.primary_nid = primary_nid;
	}
	public String getTax_ref_id_sgp() {
		return tax_ref_id_sgp;
	}
	public void setTax_ref_id_sgp(String tax_ref_id_sgp) {
		this.tax_ref_id_sgp = tax_ref_id_sgp;
	}
	public String getEff_status() {
		return eff_status;
	}
	public void setEff_status(String eff_status) {
		this.eff_status = eff_status;
	}
	public String getCountry_nm_format() {
		return country_nm_format;
	}
	public void setCountry_nm_format(String country_nm_format) {
		this.country_nm_format = country_nm_format;
	}
	public String getName_type() {
		return name_type;
	}
	public void setName_type(String name_type) {
		this.name_type = name_type;
	}
	public String getName_initials() {
		return name_initials;
	}
	public void setName_initials(String name_initials) {
		this.name_initials = name_initials;
	}
	public String getName_prefix() {
		return name_prefix;
	}
	public void setName_prefix(String name_prefix) {
		this.name_prefix = name_prefix;
	}
	public String getName_suffix() {
		return name_suffix;
	}
	public void setName_suffix(String name_suffix) {
		this.name_suffix = name_suffix;
	}
	public String getName_royal_prefix() {
		return name_royal_prefix;
	}
	public void setName_royal_prefix(String name_royal_prefix) {
		this.name_royal_prefix = name_royal_prefix;
	}
	public String getName_royal_suffix() {
		return name_royal_suffix;
	}
	public void setName_royal_suffix(String name_royal_suffix) {
		this.name_royal_suffix = name_royal_suffix;
	}
	public String getName_title() {
		return name_title;
	}
	public void setName_title(String name_title) {
		this.name_title = name_title;
	}
	public String getLast_name_srch() {
		return last_name_srch;
	}
	public void setLast_name_srch(String last_name_srch) {
		this.last_name_srch = last_name_srch;
	}
	public String getFirst_name_srch() {
		return first_name_srch;
	}
	public void setFirst_name_srch(String first_name_srch) {
		this.first_name_srch = first_name_srch;
	}
	public String getMiddle_name() {
		return middle_name;
	}
	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}
	public String getSecond_last_name() {
		return second_last_name;
	}
	public void setSecond_last_name(String second_last_name) {
		this.second_last_name = second_last_name;
	}
	public String getSecond_last_srch() {
		return second_last_srch;
	}
	public void setSecond_last_srch(String second_last_srch) {
		this.second_last_srch = second_last_srch;
	}
	public String getPref_first_name() {
		return pref_first_name;
	}
	public void setPref_first_name(String pref_first_name) {
		this.pref_first_name = pref_first_name;
	}
	public String getPartner_last_name() {
		return partner_last_name;
	}
	public void setPartner_last_name(String partner_last_name) {
		this.partner_last_name = partner_last_name;
	}
	public String getPartner_roy_prefix() {
		return partner_roy_prefix;
	}
	public void setPartner_roy_prefix(String partner_roy_prefix) {
		this.partner_roy_prefix = partner_roy_prefix;
	}
	public String getLast_name_pref_nid() {
		return last_name_pref_nid;
	}
	public void setLast_name_pref_nid(String last_name_pref_nid) {
		this.last_name_pref_nid = last_name_pref_nid;
	}
	public String getName_display() {
		return name_display;
	}
	public void setName_display(String name_display) {
		this.name_display = name_display;
	}
	public String getName_formal() {
		return name_formal;
	}
	public void setName_formal(String name_formal) {
		this.name_formal = name_formal;
	}
	public String getName_display_srch() {
		return name_display_srch;
	}
	public void setName_display_srch(String name_display_srch) {
		this.name_display_srch = name_display_srch;
	}
	public String getPhone_type() {
		return phone_type;
	}
	public void setPhone_type(String phone_type) {
		this.phone_type = phone_type;
	}
	public String getCountry_code() {
		return country_code;
	}
	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getPref_phone_flag() {
		return pref_phone_flag;
	}
	public void setPref_phone_flag(String pref_phone_flag) {
		this.pref_phone_flag = pref_phone_flag;
	}
	
}
