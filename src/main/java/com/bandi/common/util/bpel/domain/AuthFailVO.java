package com.bandi.common.util.bpel.domain;

import java.sql.Timestamp;

public class AuthFailVO {

	private String serviceUno;
	private String serviceName;
	private String authUser;
	private String url;
	private int failCnt;
	private int authFailCnt;
	private Timestamp failDate;
	private String authTime;
	private String ssoId;
	
	public String getServiceUno() {
		return serviceUno;
	}
	public void setServiceUno(String serviceUno) {
		this.serviceUno = serviceUno;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getAuthUser() {
		return authUser;
	}
	public void setAuthUser(String authUser) {
		this.authUser = authUser;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getFailCnt() {
		return failCnt;
	}
	public void setFailCnt(int failCnt) {
		this.failCnt = failCnt;
	}
	public int getAuthFailCnt() {
		return authFailCnt;
	}
	public void setAuthFailCnt(int authFailCnt) {
		this.authFailCnt = authFailCnt;
	}
	public Timestamp getFailDate() {
		return failDate;
	}
	public void setFailDate(Timestamp failDate) {
		this.failDate = failDate;
	}
	public String getAuthTime() {
		return authTime;
	}
	public void setAuthTime(String authTime) {
		this.authTime = authTime;
	}
	public String getSsoId() {
		return ssoId;
	}
	public void setSsoId(String ssoId) {
		this.ssoId = ssoId;
	}
	
	
	
}
