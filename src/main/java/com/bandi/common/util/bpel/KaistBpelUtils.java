package com.bandi.common.util.bpel;

import java.util.Date;

import javax.annotation.Resource;

import org.jfree.ui.StandardDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.common.util.KaistOIMWebServiceUtil;
import com.bandi.common.util.bpel.domain.AuthFailVO;
import com.bandi.common.util.bpel.domain.ImUserVO;
import com.bandi.domain.User;
import com.bandi.domain.kaist.ExtUser;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.ExtUserService;
import com.bandi.service.standard.source.StandardMapper;
import com.bandi.service.standard2.source.SyncIntUserToStdMapper;
import com.bandi.service.standard2.source.SyncExtUserToStdMapper;


@Component
public class KaistBpelUtils {

	protected Logger logger = LoggerFactory.getLogger(KaistBpelUtils.class);

	@Autowired
	protected SyncIntUserToStdMapper syncIntUserToStdMapper;

	@Autowired
	protected SyncExtUserToStdMapper syncExtUserToStdMapper;

	@Autowired
	protected StandardMapper dao;
	
	@Resource
	protected ExtUserService extUserService;

	protected Date gActionDate = new Date();

	public boolean syncIntUserToStd(User user) {

		KaistOIMWebServiceUtil webUtils = new KaistOIMWebServiceUtil();

		boolean result = false;

		try {
			
			String id = user.getId();
			String pw = user.getPassword();
			String emplid = dao.getEmplid(user.getKaistUid());

			ImUserVO imUser = new ImUserVO();
			imUser.setEmplid(emplid);
			imUser.setSso_id(user.getId());
			imUser.setSso_pw(user.getPassword());
			imUser.setCh_mail(user.getEmail());
			imUser.setRegister_ip(ContextUtil.getCurrentRemoteIp());
			
			if("Y".equals(user.getFlagTemp())) {
				imUser.setOuter_start_date(user.getTempStartDat().toString());
				imUser.setOuter_end_date(user.getTempEndDat().toString());
			}

			syncIntUserToStdMapper.SP_K_SSO_ID_REG(imUser);

			if ("Failure".equals(imUser.getResult())) {
				logger.error("SSO_ID_REG result == failure");
				throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_INTENAL_ERROR);
			}
			
			syncIntUserToStdMapper.SP_K_REG_PERSON_INFO(imUser);

			if ("".equals(imUser.getResult())) {
				logger.error("REG_PERSON_INFO result == failure");
				throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_INTENAL_ERROR);
			}
			
			ImUserVO personInfo = dao.getPerson(imUser.getEmplid());
			if (personInfo == null) {
				logger.error("Cant get person info to person");
				throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_INTENAL_ERROR);
			}
			
			String wsuResult = "";
			personInfo.setSso_id(id);
			personInfo.setSso_pw(pw);
			
			if("Y".equals(user.getFlagTemp())) {
				personInfo.setOuter_start_date(user.getTempStartDat().toString());
				personInfo.setOuter_end_date(user.getTempEndDat().toString());
			}
			wsuResult = webUtils.CreateUser(personInfo);

			if ( "true".equals(wsuResult)) {
				result = true;
			}
		} catch (Exception e) {
			logger.error("KaistBpelUtils.SsoKaistUser()" + e.getMessage());
			throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_INTENAL_ERROR);
		}

		return result;
	}

	public boolean syncExtUserToStd(ExtUser extUser) {

		boolean result = false;

		ImUserVO imUser = new ImUserVO();

		try {

			if (extUser.getBirthday().contains("/")) {
				imUser.setNational_id(extUser.getBirthday().replace("/", "-"));
			} else {
				imUser.setNational_id(extUser.getBirthday());
			}

			syncExtUserToStdMapper.SP_PS_EMPLID_OTHERS(imUser);// 개인정보로 존재여부 확인

			if (!"SUCCESS".equals(imUser.getResult())) {
				logger.error("EMPLID_OHTERS result == fail");
				throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
			} else {

				if (extUser.getBirthday().contains("/")) {
					imUser.setBirthdate(extUser.getBirthday().replace("/", "-"));
				} else {
					imUser.setBirthdate(extUser.getBirthday());
				}

				imUser.setBirthcountry(extUser.getCountry());
				imUser.setSex(extUser.getSex());
				imUser.setEffdt(gActionDate);
				imUser.setCountry(extUser.getCountry());
				imUser.setLast_name(extUser.getLastName());
				imUser.setSecond_last_name(extUser.getLastName());
				imUser.setPartner_last_name(extUser.getLastName());
				imUser.setName(extUser.getLastName() + extUser.getFirstName());
				imUser.setFirst_name(extUser.getFirstName());
				imUser.setPref_first_name(extUser.getFirstName());
				imUser.setName_ac(extUser.getName());
				imUser.setPhone(extUser.getMobileTelephoneNumber());
				imUser.setCh_mail(extUser.getExternEmail());
				imUser.setReg_type(extUser.getExtReqType());
				imUser.setRegister_ip(ContextUtil.getCurrentRemoteIp());
				imUser.setPerson_gubun("O");

				syncExtUserToStdMapper.SP_PS_PERSON(imUser);
				if ("".equals(imUser.getResult()) || "Failure".equals(imUser.getResult())) {
					logger.error("PS_PERSON result == fail");
					throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
				}

				syncExtUserToStdMapper.SP_PS_PERS_DATA_EFFDT(imUser);
				if ("".equals(imUser.getResult()) || "Failure".equals(imUser.getResult())) {
					logger.error("PS_PERS_DATA_EFFDT result == fail");
					throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
				}

				syncExtUserToStdMapper.SP_PS_PERS_NID_OTHERS(imUser);
				if ("".equals(imUser.getResult()) || "Failure".equals(imUser.getResult())) {
					logger.error("PS_PERS_NID_OHTERS result == fail");
					throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
				}

				syncExtUserToStdMapper.SP_PS_NAMES(imUser);
				if ("".equals(imUser.getResult()) || "Failure".equals(imUser.getResult())) {
					logger.error("PS_NAMES result == fail");
					throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
				}

				if (imUser.getPhone() != null) {

					syncExtUserToStdMapper.SP_PS_PERSONAL_PHONE(imUser);
					if ("".equals(imUser.getResult()) || "Failure".equals(imUser.getResult())) {
						logger.error("PS_PERSONAL_PHONE result == fail");
						throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
					}

					syncExtUserToStdMapper.SP_K_SSO_ID_BEFORE_REG(imUser);
					if ("".equals(imUser.getResult()) || "Failure".equals(imUser.getResult())) {
						logger.error("SSO_ID_BEFORE_REG result == fail");
						throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
					}

					syncExtUserToStdMapper.SP_K_REG_PERSON_INFO(imUser);
					if ("".equals(imUser.getResult()) || "Failure".equals(imUser.getResult())) {
						logger.error("REG_PERSON_INFO result == fail");
						throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
					}

					syncExtUserToStdMapper.SP_K_GRANTED_SERVS(imUser);
					if ("".equals(imUser.getResult()) || "Failure".equals(imUser.getResult())) {
						logger.error("GRANTED_SERV result == fail");
						throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
					}

					syncExtUserToStdMapper.SP_K_SYNC_EFFDT(imUser);
					if ("".equals(imUser.getResult()) || "Failure".equals(imUser.getResult())) {
						logger.error("SYNC_EFFDT result == fail");
						throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
					}

					syncExtUserToStdMapper.SP_SYNC_PERSON_TO_BASIS(imUser);
					if (!"OK_UPDATE".equals(imUser.getResult()) || "OK_INSERT".equals(imUser.getResult())) {
						logger.error("SYNC_PERSON_TO_BASIS result == fail");
						throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
					}

					result = true;
				}
			}
		} catch (Exception e) {
			logger.error("KaistBpelUtils.SsoOtherUser()" + e.getMessage());
			throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
		}

		return result;
	}
	
	public void authFailLogReset(String ssoId) {
		
		AuthFailVO failVo = dao.getFailLog(ssoId);
		
		if(failVo != null) {
			int deleteResult = dao.deleteFailLog(ssoId);
			
			if(deleteResult != 1 ) {
				throw new BandiException(ErrorCode_KAIST.STANDARD_LOGIN_FAIL_LOG_RESET_FAIL_V1);
			}
		}
		
		AuthFailVO failV3Vo = dao.getV3FailLog(ssoId);
		
		if(failV3Vo != null) {
			int deleteV3Result = dao.deleteV3FailLog(ssoId);
			
			if(deleteV3Result != 1 ) {
				throw new BandiException(ErrorCode_KAIST.STANDARD_LOGIN_FAIL_LOG_RESET_FAIL_V3);
			}
		}
	}
}
