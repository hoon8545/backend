package com.bandi.common.util;

import java.io.ByteArrayInputStream;
import java.util.Iterator;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.stream.StreamSource;

// 나중에 삭제
public class KaistOIMXmlParsingUtil {	
	public static String getResponseXmlValue(String xml) throws Exception {		
		String value = null;
        String soapText = xml;
    
        MessageFactory msgFactory = MessageFactory.newInstance();   
        SOAPMessage message = msgFactory.createMessage();   
        SOAPPart soapPart = message.getSOAPPart();   

        byte[] buffer = soapText.getBytes("UTF-8");   
        ByteArrayInputStream stream = new ByteArrayInputStream(buffer);   
        StreamSource source = new StreamSource(stream);   
    
        soapPart.setContent(source);   

        SOAPBody soapBody = message.getSOAPBody();   
        Iterator i = soapBody.getChildElements();   
        Node node = (Node) i.next();
    
        value = node.getChildNodes().item(0).getChildNodes().item(0).getNodeValue();
        
		return value;
	}	
	
	public static String getPosId(String xml) throws Exception {
		String psoId = null;
        String soapText = xml;
    
        MessageFactory msgFactory = MessageFactory.newInstance();   
        SOAPMessage message = msgFactory.createMessage();   
        SOAPPart soapPart = message.getSOAPPart();   

        byte[] buffer = soapText.getBytes("UTF-8");   
        ByteArrayInputStream stream = new ByteArrayInputStream(buffer);   
        StreamSource source = new StreamSource(stream);   
   
        soapPart.setContent(source);   

        SOAPBody soapBody = message.getSOAPBody();   
        Iterator i = soapBody.getChildElements();   
        Node node = (Node) i.next();
    
        try {
        	psoId = node.getChildNodes().item(0).getChildNodes().item(0).getChildNodes().item(0).getAttributes().getNamedItem("ID").getNodeValue();
        } catch(NullPointerException e) {
        	psoId = null;
        }
        
        if(psoId != null) {
        	String [] psoIds = psoId.split(":");
        	
        	psoId = psoIds[1];       
        }
        
		return psoId;
	}
	
	public static boolean imPwParsing(String xml) throws Exception {
		boolean flag = false;
		String imFlag = null;
        String soapText = xml;
        
        MessageFactory msgFactory = MessageFactory.newInstance();   
        SOAPMessage message = msgFactory.createMessage();   
        SOAPPart soapPart = message.getSOAPPart();   

        byte[] buffer = soapText.getBytes("UTF-8");   
        ByteArrayInputStream stream = new ByteArrayInputStream(buffer);   
        StreamSource source = new StreamSource(stream);   
    
        soapPart.setContent(source);   

        SOAPBody soapBody = message.getSOAPBody();   
        Iterator i = soapBody.getChildElements();   
        Node node = (Node) i.next();
        
        imFlag = node.getChildNodes().item(0).getAttributes().getNamedItem("status").getNodeValue();
        
        if(imFlag != null && !"".equals(imFlag.trim())) {
        	if("success".equals(imFlag.toLowerCase())) {
        		flag = true;
        	} else {
        		flag = false;
        	}
        }
        
    	return flag;
	}
	
	public static String imPwParsingReturnCode(String xml) throws Exception {
		String flag = "false";
		String imFlag = "";
        String soapText = xml;
        
        MessageFactory msgFactory = MessageFactory.newInstance();   
        SOAPMessage message = msgFactory.createMessage();   
        SOAPPart soapPart = message.getSOAPPart();   

        byte[] buffer = soapText.getBytes("UTF-8");   
        ByteArrayInputStream stream = new ByteArrayInputStream(buffer);   
        StreamSource source = new StreamSource(stream);   
   
        soapPart.setContent(source);   

        SOAPBody soapBody = message.getSOAPBody();   
        Iterator i = soapBody.getChildElements();   
        Node node = (Node) i.next();
        
        imFlag = node.getChildNodes().item(0).getAttributes().getNamedItem("status").getNodeValue();

        if(imFlag != null && !"".equals(imFlag.trim())) {
        	if("success".equals(imFlag.toLowerCase())) {
        		flag = "true";
        	} else {
        		imFlag = node.getChildNodes().item(0).getAttributes().getNamedItem("error").getNodeValue();
        		if("customError".equals(imFlag)) {
        			flag = "policyerror";
        		} else {
        			flag = "false";
        		}
        	}
        }
        
    	return flag;
	}
	
	public static String imUserStateParsingReturnCode(String xml) throws Exception {
		String flag = "false";
		String imFlag = "";
        String soapText = xml;
        
        MessageFactory msgFactory = MessageFactory.newInstance();   
        SOAPMessage message = msgFactory.createMessage();   
        SOAPPart soapPart = message.getSOAPPart();   

        byte[] buffer = soapText.getBytes("UTF-8");   
        ByteArrayInputStream stream = new ByteArrayInputStream(buffer);   
        StreamSource source = new StreamSource(stream);   
    
        soapPart.setContent(source);   

        SOAPBody soapBody = message.getSOAPBody();   
        Iterator i = soapBody.getChildElements();   
        Node node = (Node) i.next();
        
        imFlag = node.getChildNodes().item(0).getAttributes().getNamedItem("active").getNodeValue();
        
        if(imFlag != null && !"".equals(imFlag.trim())) {
        	if("true".equals(imFlag.toLowerCase())) {
        		flag = "true";
        	} else {
       			flag = "false";
        	}
        }
        
    	return flag;
	}
	
	public static String getCIParsing(String xml) throws Exception {
		String CI = "";
        String soapText = xml;
        
        MessageFactory msgFactory = MessageFactory.newInstance();   
        SOAPMessage message = msgFactory.createMessage();  
        SOAPPart soapPart = message.getSOAPPart();   
        
        byte[] buffer = soapText.getBytes("UTF-8");   
        ByteArrayInputStream stream = new ByteArrayInputStream(buffer);   
        StreamSource source = new StreamSource(stream);   
  
        soapPart.setContent(source);
        
        SOAPBody soapBody = message.getSOAPBody();   
        Iterator i = soapBody.getChildElements();   
        Node node = (Node) i.next();
        
        CI = node.getChildNodes().item(0).getTextContent();
        
    	return CI;
	}	
	
	public static String getKaistUid(String responseXML) throws Exception {
		String kaistUid = null;
		String soapText = responseXML;
		boolean proceed = false;

		if(soapText != null && !"".equals(soapText.trim())) {
        	if(soapText.indexOf("Success") > -1) {
        		proceed = true;
        	} else {
        		proceed = false;
        	}	
        }
        
        MessageFactory msgFactory = MessageFactory.newInstance();
        
		if(proceed) {
	        SOAPMessage message = msgFactory.createMessage();   
	        SOAPPart soapPart = message.getSOAPPart();   

	        byte[] buffer = soapText.getBytes("UTF-8");   
	        ByteArrayInputStream stream = new ByteArrayInputStream(buffer);   
	        StreamSource source = new StreamSource(stream);   
   
	        soapPart.setContent(source);   

	        SOAPBody soapBody = message.getSOAPBody();   
	        Iterator i = soapBody.getChildElements();   
	        Node node = (Node) i.next();
	        kaistUid = node.getChildNodes().item(3).getTextContent().trim();
		} else {
			kaistUid = "false";
		}
        
		return kaistUid;
	}
	
	public static String isOtherUserSuccess(String responseXML) throws Exception {		
		String resultString = null;
		String soapText = responseXML;
		
        if(soapText != null && !"".equals(soapText.trim())) {
        	if(soapText.indexOf("Success") != -1) {
        		resultString = "true";
        	} else {
        		resultString = "false";
        	}	
        }
        
		return resultString;
	}
	
	public static String getBpelResult(String responseXML) throws Exception {
		String flag = "false";
		String soapText = responseXML;
  
        MessageFactory msgFactory     = MessageFactory.newInstance();   
        SOAPMessage message           = msgFactory.createMessage();   
        SOAPPart soapPart             = message.getSOAPPart();   

        byte[] buffer                 = soapText.getBytes("UTF-8");   
        ByteArrayInputStream stream   = new ByteArrayInputStream(buffer);   
        StreamSource source           = new StreamSource(stream);   
    
        soapPart.setContent(source);   

        SOAPBody soapBody = message.getSOAPBody();   
        Iterator i = soapBody.getChildElements();   
        Node node = (Node) i.next();
          
        flag = node.getChildNodes().item(1).getTextContent().trim();
        
        if(flag != null && !"".equals(flag.trim())) {
        	if(flag.toLowerCase().equals("Success".toLowerCase().trim())) {
        		flag="true";
        	} else {
        		flag="false";
        	}	
        }
  
		return flag;
	}
		
	public static String getImResult(String responseXML) throws Exception {
	    String flag = "false";
	    
	    System.out.println("ImApiService result ::: " + responseXML);
	    
	    if(responseXML.indexOf("success")!= -1) {
	    	flag = "true";
	    }
        
		return flag;
	}	
}