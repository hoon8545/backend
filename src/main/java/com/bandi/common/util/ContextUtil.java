package com.bandi.common.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.MDC;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.bandi.common.BandiConstants;
import com.bandi.common.kaist.BandiProperties_KAIST;
import com.initech.eam.nls.CookieManager;
import com.initech.eam.smartenforcer.SECode;

public class ContextUtil{

    public static void put(String key, String value) {
        MDC.put( key, value);
    }

    public static String get(String key) {
        return MDC.get( key);
    }

    public static void remove(String key) {
		MDC.remove( key);
    }

    public static String getCurrentRemoteIp() {
    	return get( BandiConstants.MDC_REMOTE_IP);
    }

    public static String getCurrentRequestId() {
		return get( BandiConstants.MDC_REQUEST_ID);
    }

    public static HttpServletRequest getHttpServletRequest(){

        HttpServletRequest request = null;

        if( RequestContextHolder.getRequestAttributes() != null) {
            request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        }

        return request;
    }

	public static HttpSession getCurrentHttpSession(){

		HttpSession session = null;
		HttpServletRequest request = getHttpServletRequest();

		if( request != null){
			session = request.getSession( false );
		}

		return session;
	}

	public static String getCurrentSessionId() {
		HttpSession session = getCurrentHttpSession();
		String sessionId = null;

		if( session != null){
			sessionId = getCurrentHttpSession().getId();
		}

		if( sessionId == null){
			sessionId = MDC.get( BandiConstants.MDC_SESSION_ID);
		}

		return sessionId;
	}

	public static String getCurrentUserId() {
		String userId = null;

        HttpServletRequest httpServletRequest = getHttpServletRequest();

        // !!!! 사용자 로그인 후 REQUEST URL이 사용자/관리자 공통 URL이며(/user로 시작 X), 내부 로직에서 getCurrentUserId() 호출한다면
        // !!!! 아래 쿠키를 가져오는 로직을 타지 않으므로 if 조건절에 URL 패턴이 반드시 추가되어야 함.
        if( httpServletRequest != null && httpServletRequest.getRequestURI().startsWith( "/user" )){

            if( BandiProperties_KAIST.IS_TEST_WITHOUT_INITECH ){
                Cookie[] cookies = httpServletRequest.getCookies();
                if( cookies != null ){
                    for( Cookie cookie : cookies ){
                        String name = cookie.getName(); // 쿠키 이름 가져오기

                        if( "bandi_cookie".equals( name ) ){

                            userId = cookie.getValue();
                            break;
                        }
                    }
                }
            } else{
                userId = CookieManager.getCookieValue( SECode.USER_ID, httpServletRequest );
            }
        }

		if( userId == null) {
            if( RequestContextHolder.getRequestAttributes() != null) {
                HttpSession session = getCurrentHttpSession();

                if( session != null){
                    userId = (String) getCurrentHttpSession().getAttribute( BandiConstants.MDC_USER_ID);
                }
            }
        }

		if( userId == null){
			userId = MDC.get( BandiConstants.MDC_USER_ID);
		}

		return userId;
	}
}
