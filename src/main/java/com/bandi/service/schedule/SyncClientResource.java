package com.bandi.service.schedule;

import org.quartz.DisallowConcurrentExecution;

import com.bandi.service.orgSync.kaist.SyncClientResourceService;

@DisallowConcurrentExecution
public class SyncClientResource extends ScheduleJob{

    protected  SyncClientResourceService syncClientResourceService;

    public void setSyncClientResourceService(SyncClientResourceService syncClientResourceService) {
        this.syncClientResourceService = syncClientResourceService;
    }

    @Override
	public void doJob(){
        syncClientResourceService.execute();
	}
}
