package com.bandi.service.schedule;

import org.quartz.DisallowConcurrentExecution;

import com.bandi.service.manage.kaist.CheckDelegateRoleService;

@DisallowConcurrentExecution
public class CheckDelegateRole extends ScheduleJob{

    protected  CheckDelegateRoleService checkDelegateRoleService;

	public void setCheckDelegateRoleService(CheckDelegateRoleService checkDelegateRoleService) {
        this.checkDelegateRoleService = checkDelegateRoleService;
    }

    @Override
	public void doJob(){
        checkDelegateRoleService.excute();
	}
}
