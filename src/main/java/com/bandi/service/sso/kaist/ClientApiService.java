package com.bandi.service.sso.kaist;

import java.util.Map;

import javax.servlet.http.Cookie;

public interface ClientApiService {
	// 파라미터 이름

	public static final String PARAM_OTP = "otp"; // otp
	public static final String PARAM_OTP_USER_ID = "userId"; // otp 사용자 아이디

	public static final String PARAM_USER_ID = "userId"; // 사용자 아이디
	public static final String PARAM_USER_PASSWORD = "password"; // 비밀번호
	public static final String PARAM_CLIENT_ID = "client_id"; // oauth client id
	public static final String PARAM_CLIENT_SECRET = "client_secret"; // oauth client secret
	public static final String PARAM_GRANT_TYPE = "grant_type"; // grant_type
	public static final String PARAM_CODE = "code"; // code
	public static final String PARAM_REDIRECT_URI = "redirect_uri"; // redirect_uri
	public static final String PARAM_ACCESS_TOKEN = "access_token"; // redirect_uri

	public static final String PARAM_SSO_ERROR = "error"; // error 메세지
	public static final String PARAM_SSO_ERROR_DESC = "error_description"; // error 메세지 상세


	//ConfigMap Const

	// INITECT 서버 설정
	public static final String CONFIG_INITECH_SSO_ND_URL = "INITECK_SSO_ND_URL"; // 이니텍 로그인 서버 xmlrpc
	public static final String CONFIG_INITECH_SSO_ND_URL2 = "INITECK_SSO_ND_URL2"; // 이니텍 로그인 서버 xmlrpc
	public static final String CONFIG_INITECH_PROVIDER = "INITECT_PROVIDER";
	public static final String CONFIG_INITECH_COOKIE_DOMAIN = "INITECT_COOKIE_DOMAIN";
	public static final String CONFIG_INITECH_COOKIE_PADDING = "INITECT_COOKIE_PADDING";
	public static final String CONFIG_INITECH_TOA = "INITECT_TOA"; // int 형

	// OAUTH
	public static final String CONFIG_CLIENT_ID = "CLIENT_ID";
	public static final String CONFIG_CLIENT_SECRET = "CLIENT_SECRET";
	public static final String CONFIG_SSO_OAUTH_URL = "SSO_OAUTH_URL";
	public static final String CONFIG_SSO_OAUTH_REDIRECT_URL = "SSO_OAUTH_REDIRECT_URL";

	// SAML
	public static final String CONFIG_SSO_SAML_IDENTITY_URL = "SSO_SAML_IDENTITY_URL";
	public static final String CONFIG_SSO_SAML_APP_SERVER_URL = "SSO_SAML_APP_SERVER_URL";
	public static final String CONFIG_SSO_SAML_APP_PROVIDER_NAME = "SSO_SAML_APP_PROVIDER_NAME";

	// public static final String CONFIG_SSO_SAML_ACLURL = "SSO_SAML_ACLURL";

	// OTP
	public static final String CONFIG_OTP_URL = "OTP_URL";

	// IM
	public static final String CONFIG_IM_URL = "IM_URL";

	// CLIENT CRUD
    public static final String CONFIG_INITECH_CLIENT_API_URL = "INITECH_CLIENT_API_URL";

	/**
	 * 사용자 아이디와 OTP 번호로 2차 인증을 한다.
	 *
	 * @param id
	 * @param otpNum
	 * @return
	 * @throws Exception
	 */
	boolean checkOtp(String userId, String otpNum);

	/**
	 * 사용자가 유효한지 확인 한다.
	 *
	 * @param userId
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public boolean isValidUser(String userId, String password);

	/**
	 * 사용자 정보를 가져온다.
	 *
	 * @param userId
	 * @return
	 */
	public Map<String, String> getUserInfo(String userId);

	/**
	 * 권한 정보를 가져온다.
	 *
	 * @param userId
	 * @param type
	 * @return
	 */
	public Map<String, String> getAuthInfo(String userId, String type);


	/**
    *
	 * 이니텍 SSO 서버에 이미 로그인 했는지 검사( 쿠기 개수 조사)
	 * @param request
	 * @return
	 */
	boolean isLogin(Cookie[] cookies);

	/**
	 * Initech sso 서버에 로그인 한다.
	 * @param userId
	 * @param password
	 * @param remoteIp
	 * @return
	 */
	boolean login(String userId, String password, String remoteIp);

	/**
	 * 로그아웃 시킨다.
	 *
	 * @param userId
	 * @param value
	 * @return
	 * @throws Exception
	 */
	public void logout(String userId, String value);


	/**
	 * 로그인 이후 authentication code를 가져온다. (clientId)
	 *
	 * @param cookieMap
	 * @return
	 */
	HttpConnService.Result getAuthCode(Map<String, String> cookieMap);

	/**
	 * 로그인 이후 authentication code를 가져온다.
	 *
	 * @param clientId
	 * @param cookieMap
	 * @return
	 */
	HttpConnService.Result getAuthCode(String clientId, Map<String, String> cookieMap);

	/**
	 * authorization_code로  AccessToken을 요청한다. (clientId, clientSecret default)
	 *
	 * @param authCode
	 * @return
	 * @throws Exception
	 */
	HttpConnService.Result getAccessToken( String authCode);

	/**
	 * authorization_code로  AccessToken을 요청한다.
	 *
	 * @param clientId
	 * @param clientSecret
	 * @param authCode
	 * @return
	 * @throws Exception
	 */
	HttpConnService.Result getAccessToken(String clientId, String clientSecret, String authCode);

	/**
	 * accessToken이 유효한지 요청한다.
	 *
	 * @param accessToken
	 * @return
	 * @throws Exception
	 */
	boolean checkAccessToken(String accessToken);


	/**
	 * accessToken이 유효한지 검사하고 유효하면 사용자 정보를 돌려준다.
	 *
	 * @param accessToken
	 * @return
	 * @throws Exception
	 */
	Map<String, String> checkAccessTokenAndUser(String accessToken);

	/**
	 *
	 * @param appServerUrl agent에 등록 되어 있는 업무시스템 url
	 * @param authnId
	 * @param samlResponse saml 응답
	 * @param publicKeyPath 인증서 실제 경로
	 * @return
	 */
	boolean checkSAMLToken(String appServerUrl, String authnId, String samlResponse, String publicKeyPath);

	/**
	 *
	 * @param samlResponse saml 응답
	 * @return
	 */
	String getSamlSsoId(String samlResponse);

	/**
	 * saml 요청을 검사하는 url ( SAML SSO 로그인페이지 이동)
	 * @param authnId
	 * @param samlReqTempPath
	 * @param providerName
	 * @param acsurl  Assertion Consumer Service, 인증되 사용자에 대한 정보가 담긴 Saml response 검정하고 서비스 제공 포워딩 하는 url
	 * @param relayStateUrl 완료후 redirect 하는 url
	 * @return
	 * @throws Exception
	 */
	String getSamlIdentityProveUrl(String authnId, String samlReqTempPath,
							String providerName, String acsUrl, String relayStateUrl);


	/**
	 * saml response를 가져온다.
	 * @param url
	 * @param cookieMap
	 * @return
	 */
	HttpConnService.Result getSamlResponse(String url, Map<String, String> cookieMap);


    void createClient(String clientId, String clientName,  Map<String, String> cookieMap);

    String getClientSecrect(String clientId, Map<String, String> cookieMap);

    void updateClient(String clientId, String clientName, Map<String, String> cookieMap);

    void deleteClient(String clientId, Map<String, String> cookieMap);

    boolean createOtpSerialNumber(String userId, String serialNumber);

    String otpUnlock(String userId, String serialNumber);

    String otpCheckStatus(String userId);
    
    String getSerialNumber(String userId);
    
    int getCheckOtpResultCode(String userId, String otpNumber);
    
    String changeLongTermNonUser(String day);
    
    String changeOtpFailCount(String count);
    
    String otpSynchronize();
}
