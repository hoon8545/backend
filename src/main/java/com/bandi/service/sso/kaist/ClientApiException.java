package com.bandi.service.sso.kaist;

import com.bandi.exception.BandiException;

/**
 * 에러 객체
 * @author nowone
 *
 */
public class ClientApiException extends BandiException {

	private static final long serialVersionUID = 1L;
	
	// 로그인 실패 회수
	protected String loginFailCount;
	
	// 이니텍 리턴코드
	protected int retCode = 0;
	
	// 비밀번호 변경 해야 하는 여부 ( T or F);
	protected String passwordChangeRequired;

    
    public ClientApiException(String errorCode){
    	super(errorCode);
    }
    
    public ClientApiException(String errorCode, int retCode){
    	super(errorCode);
    	this.retCode = retCode;
    }
    
    public ClientApiException(String errorCode, String[] arguments) {
    	super(errorCode, arguments);
    }

	public String getLoginFailCount() {
		return loginFailCount;
	}

	public void setLoginFailCount(String loginFailCount) {
		this.loginFailCount = loginFailCount;
	}

	public String getPasswordChangeRequired() {
		return passwordChangeRequired;
	}

	public void setPasswordChangeRequired(String passwordChangeRequired) {
		this.passwordChangeRequired = passwordChangeRequired;
	}

	public int getRetCode() {
		return retCode;
	}

	public void setRetCode(int retCode) {
		this.retCode = retCode;
	}
}
