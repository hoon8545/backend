package com.bandi.service.sso.kaist;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 일반 적인 DTO 
 * @author nowone
 *
 */
public class GeneralDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/** 모든 요청 데이타를 저장하는 맵 */
	protected Map<String, Object> dataMap;
    
    public GeneralDTO() {
    	dataMap = new HashMap<>();
    }
    
    public GeneralDTO(Map<String, Object> map) {
    	this.addDataMap(map);
    }
    
    public Object get(String key) {
    	return dataMap.get(key);
    }
    
    public Object set(String key, Object value) {
    	return dataMap.put(key, value);
    }
    
    public Object remove(String key) {
		if( this.dataMap == null) {
			return null;
		}
		
		return this.dataMap.remove(key);
	}
    
    /**
     * 원본에 값을 추가한다.
     * @param map
     */
    public void addDataMap(Map<String , Object> map) {
    	if( this.dataMap == null) {
    		this.dataMap = new HashMap<>();
    	}
    	
    	if( map ==  null || map.size() < 1) {
    		return;
    	}
    	
    	for( String key : map.keySet() ){
    		this.dataMap.put(key, map.get(key));
        }
    	
    }
    
    /**
     * 결과 Map을 복사하여 돌려준다.
     * @return
     */
    public Map<String, Object> getDataMap() {
    	Map<String, Object> returnMap = new HashMap<>();
    	
    	if( this.dataMap !=null && this.dataMap.size() > 0) {
	    	for( String key : this.dataMap.keySet() ){
	    		returnMap.put(key, this.dataMap.get(key));
	        }
    	}
    	
    	return returnMap;
    }
    
    public int getInt(String key) {
    	if( !dataMap.containsKey(key)) {
    		return 0;
    	}
    	
    	Object value = dataMap.get(key);
    	try {
	    	if( value instanceof Integer) {
	    		return (int)value;
	    	} else if( value instanceof String) {
	    		return Integer.parseInt((String)value);
	    	} else  {
	    		return Integer.parseInt(value.toString());
	    	}
    	} catch(NumberFormatException nfe) {
    		return 0;
    	}
    	
    }
    
    public long getLong(String key) {
    	if( !dataMap.containsKey(key)) {
    		return 0L;
    	}
    	
    	Object value = dataMap.get(key);
    	try {
	    	if( value instanceof Integer) {
	    		return (long)value;
	    	} else if( value instanceof Long) {
	    		return (long)value;
	    	} else if( value instanceof String) {
	    		return Integer.parseInt((String)value);
	    	} else  {
	    		return Integer.parseInt(value.toString());
	    	}
    	} catch(NumberFormatException nfe) {
    		return 0L;
    	}
    	
    }
    
    public double getDouble(String key) {
    	if( !dataMap.containsKey(key)) {
    		return 0.0;
    	}
    	
    	Object value = dataMap.get(key);
    	try {
	    	if( value instanceof Integer) {
	    		return (double)value;
	    	} else if( value instanceof Double) {
	    		return (double)value;
	    	} else if( value instanceof String) {
	    		return Double.parseDouble((String)value);
	    	} else  {
	    		return Double.parseDouble(value.toString());
	    	}
    	} catch(NumberFormatException nfe) {
    		return 0.0;
    	}
    	
    }
    
    public String getString(String key) {
    	if( !dataMap.containsKey(key)) {
    		return null;
    	}
    	
    	Object value = dataMap.get(key);
    	if( value instanceof String) {
    		return (String)value;
    	} else  {
    		return value.toString();
    	}
    	
    }
    
    public boolean isExist(String key) {
    	return dataMap.containsKey(key);
    }
    
    public boolean isTrue(String key) {
    	if( !dataMap.containsKey(key)) {
    		return false;
    	}
    	
    	Object value = dataMap.get(key);
    	if( value instanceof Boolean) {
    		return (boolean)value;
    	} else  {
    		return this.isBoolValue(getString( key));
    	}
    	
    }
    
    /**
     * 문자열 검사
     * @param source
     * @return
     */
    public boolean isValid(String source) {
    	if( source == null || source.trim().length() < 1) {
    		return false;
    	}
    	
    	return true;
    }
    
    public void setBoolValue(String name, String value) {
    	
    	if( "1".equals(value) )
    		this.set(name, "1");
    	else {
    		this.set(name, "0");
    	}
    }
    
    /**
	 * true false 문자열
	 * @param val
	 * @return
	 */
	public String getBoolValue(boolean val) {
	    if(val) {
		    return "1";
	    } else {
		    return "0";
	    }
	}
    
    /**
	 * 문자열을 boolean 값으로 변경
	 * @param value
	 * @return
	 */
	protected boolean isBoolValue( String value ) {
		if( value == null || !"1".equals(value.trim()) ) {
			return false;
		} else {
			return true;
		}
	}
	
    @Override
    public String toString(){
    	StringBuilder sb = new StringBuilder(this.getClass().getSimpleName() +"{");
    	if( this.dataMap !=null && this.dataMap.size() > 0) {
    		int index = 0;
	    	for( String key : this.dataMap.keySet() ){
	    		if( index > 0) {
	    			sb.append(", ");
	    		}
	    		sb.append(key + "='" +this.dataMap.get(key) + "'" );
	    		
	    		index++;
	        }
    	}
    	
    	sb.append("}");
    	
    	return sb.toString();
    }
}
