package com.bandi.service.sso.kaist;

import java.util.Map;

/**
 * 에러 발생시 보내주는 Body
 * @author nowone
 *
 */
public class ResponseErrorDTO extends ResponseDTO{

	private static final long serialVersionUID = 1L;

    public ResponseErrorDTO(Map<String, Object> dataMap) {
    	super(true, dataMap);
    }
    
    public ResponseErrorDTO(boolean error, Map<String, Object> dataMap) {
        super(error, dataMap);
    }

    public ResponseErrorDTO(String errorCode, String errorMessage) {
    	super(errorCode, errorMessage);
    }
    
    public ResponseErrorDTO(String errorCode, String errorMessage, Map<String, Object> dataMap) {
    	super(errorCode, errorMessage, dataMap);
    }
}
