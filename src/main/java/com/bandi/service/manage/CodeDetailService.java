package com.bandi.service.manage;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.CodeDetail;
import com.bandi.domain.CodeDetailCondition;

import java.util.List;


public interface CodeDetailService{

    int insert( CodeDetail codedetail );

    CodeDetail get( String oid );

    void update( CodeDetail codedetail );

    void deleteBatch( List list );

    int delete( String id );

    Paginator search( Paginator paginator, CodeDetail codedetail );

    long countByCondition( CodeDetailCondition condition );

    int deleteByCondition( CodeDetailCondition condition );

    List< CodeDetail > selectByCondition( CodeDetailCondition condition );

    int updateByConditionSelective( CodeDetail record, CodeDetailCondition condition );

    void move( CodeDetail codedetail, String beforeCodeId );

    void updateAfterCompareDB( CodeDetail codedetail );

    List<String> getDescendantCodeFromCodeId( String codeId );

    // ===================== End of Code Gen =====================

    CodeDetail selectByCodeAndCodeId( String code, String codeId );
}
