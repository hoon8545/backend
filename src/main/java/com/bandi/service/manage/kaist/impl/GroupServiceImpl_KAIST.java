package com.bandi.service.manage.kaist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.bandi.common.BandiConstants;
import com.bandi.common.util.CommonUtil;
import com.bandi.domain.Group;
import com.bandi.domain.GroupCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.impl.GroupServiceImpl;
import com.bandi.service.manage.impl.LicenseServiceImpl;
import com.bandi.service.manage.kaist.GroupService_KAIST;
import com.bandi.service.manage.kaist.RoleMasterService;
import com.bandi.service.manage.kaist.RoleMemberService;

public class GroupServiceImpl_KAIST extends GroupServiceImpl implements GroupService_KAIST{

    @Autowired
    RoleMasterService roleMasterService;

    @Autowired
    RoleMemberService roleMemberService;

    @Override
    public int insert( Group group) {

        if( group.getId() == null || group.getId().length() == 0){
            throw new BandiException( ErrorCode.GROUP_ID_IS_NULL);
        }

        if (group.getParentId() == null || group.getParentId().length() == 0) {
            throw new BandiException( ErrorCode.PARENT_GROUP_ID_IS_NULL);
        }

        Group groupFromDB = get(group.getId());

        if( groupFromDB != null){
            throw new BandiException( ErrorCode.GROUP_ID_DUPLICATED, new String[] {group.getId()});
        }

        checkGroupName(group);

        Group parentGroup = get(group.getParentId());

        if (parentGroup == null) {
            throw new BandiException( ErrorCode.PARENT_GROUP_NOT_FOUND);
        }

        parentGroup.setSubLastIndex(parentGroup.getSubLastIndex() + 1);

        update( parentGroup);

        if ( !Group.CONST_STATUS_DELETE.equals(group.getStatus())) {
            group.setStatus( Group.CONST_STATUS_ACTIVE);
        }

        group.setSubLastIndex( -1);
        group.setFullPathIndex( parentGroup.getFullPathIndex() + CommonUtil.makePathIndex( parentGroup.getSubLastIndex()));

        group.setOriginalParentId( group.getParentId());

        // parentId가 G000이고, FlagSync가 N이면, 모든 클라이언트에 해당 부서를 ViewElement에 추가
        // kaist에서는 사용하지 않음
        /*if( Group.GROUP_ROOT_OID.equals( group.getParentId() ) && BandiConstants.FLAG_N.equals( group.getFlagSync() )){
            viewElementService.addInitData( group.getId(), BandiConstants.OBJECT_TYPE_GROUP);
        }*/
        group = setCreatorInfo( group );

        int nCount = dao.insertSelective(group);

        handleAfterGroupCreated( group );

        return nCount;
    }

    @Override
    public int delete(String id) {

        Group group = get(id);

        if( group == null){
            throw new BandiException( ErrorCode.GROUP_IS_NOT_EXISTED );
        }

        //  KAIST-TODO : 하위에 사용자/하위부서/롤가 존재할 경우?
        // String fullPathIndex = group.getFullPathIndex();

        /*if( dao.hasDescendantsUser( fullPathIndex + "%")){
            throw new BandiException( ErrorCode.CANNOT_DLETE_GROUP_DESCENDAT_IS_EXISTED );
        }*/

        /* // kaist에서는 사용하지 않음
        // ViewElement 삭제
        viewElementService.deleteAfterGroupDelete( id, fullPathIndex);
        */

        group.setStatus( Group.CONST_STATUS_DELETE );

        // kaist에서는 삭제된 부서를 그대로 둔다.
        //Group currentRetiredGroup = getCurrentRetiredGroup();
        //move( group, currentRetiredGroup.getId());

        // kaist에서는 사용하지 않음.
        // aceService.removeAce( id, BandiConstants.OBJECT_TYPE_GROUP );

        update(group);

        handleAfterGroupDeleted( group);

        return 1;
    }

    @Override
    public void update(Group group) {
        group = setUpdatorInfo(group);

        dao.updateByPrimaryKeySelective(group);

        handleAfterGroupUpdated( group);
    }

    @Override
    public void move(Group group, String targetGroupID ) {

        if ( targetGroupID.equals( group.getParentId())) {
            throw new BandiException( ErrorCode.TARGET_GROUP_IS_CURRENT_PARETN_GROUP);
        }

        Group targetGroup = get( targetGroupID );

        if ( targetGroup == null) {
            throw new BandiException( ErrorCode.TARGET_GROUP_NOT_FOUND);
        }

        if ( targetGroup.getFullPathIndex().indexOf(group.getFullPathIndex()) >= 0) {
            throw new BandiException(ErrorCode.TARGET_GROUP_IS_SUB_GROUP);
        }

        group.setOriginalParentId( group.getParentId() );

        group.setParentId( targetGroupID );

        checkGroupName(group);

        targetGroup.setSubLastIndex(targetGroup.getSubLastIndex() + 1);

        update( targetGroup);

        String oldFullPathIndex = group.getFullPathIndex();
        String newFullPathIndex = targetGroup.getFullPathIndex() + CommonUtil.makePathIndex(targetGroup.getSubLastIndex());

        group.setFullPathIndex( newFullPathIndex );

        update( group);

        updateFullPathIndexInheritably(oldFullPathIndex, newFullPathIndex);

    }

    @Override
    public boolean duplicate( String parentID, String szName, String groupID ) {
        GroupCondition condition = new GroupCondition();
        condition.createCriteria().andParentIdEqualTo( parentID )
                .andStatusEqualTo( Group.CONST_STATUS_ACTIVE )
                .andNameEqualTo( szName);

        if( groupID != null) {
            condition.getOredCriteria().get(0).andIdNotEqualTo( groupID );
        }

        List group = selectByCondition( condition);

        return group != null && group.size() > 0;
    }


    protected void handleAfterGroupCreated( Group group ) {

        // 부서 공통롤, 부서장롤 생성
        if ( LicenseServiceImpl.supportEam()){
            roleMasterService.createDeptRole(  group.getId(), group.getName() );

            roleMasterService.createDeptManageRole(  group.getId(), group.getName(), group.getParentId() );
        }
    }

    protected void handleAfterGroupUpdated( Group group ) {

        // 부서 공통롤, 부선장 롤 이름변경
        if ( LicenseServiceImpl.supportEam()){
            if( group.getName() != null){
                roleMasterService.updateDeptRoleName( group.getId(), group.getName() );
            }
        }
    }

    protected void handleAfterGroupDeleted( Group group ) {

        // 부서 공통롤, 부선장 롤 삭제
        if ( LicenseServiceImpl.supportEam()){
            roleMasterService.deleteByGroupId( group.getId() );

            roleMemberService.deleteByGroupId( group.getId() );
        }
    }
}
