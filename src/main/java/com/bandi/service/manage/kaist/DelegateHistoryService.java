package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.DelegateHistory;
import com.bandi.domain.kaist.DelegateHistoryCondition;


public interface DelegateHistoryService {

    void insert(DelegateHistory delegatehistory);

    DelegateHistory get(String oid);

    void update(DelegateHistory delegatehistory);

    void deleteBatch(List list);

    int delete(String id);

    Paginator search(Paginator paginator,DelegateHistory delegatehistory);

    long countByCondition( DelegateHistoryCondition condition);

    int deleteByCondition( DelegateHistoryCondition condition);

    List<DelegateHistory> selectByCondition( DelegateHistoryCondition condition);

    int updateByConditionSelective( DelegateHistory record, DelegateHistoryCondition condition);

    // ===================== End of Code Gen =====================

    void deleteDelegateRole(String oid);

    void cancleDelegateRole(String oid);

}
