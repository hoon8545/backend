package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.AccessElement;
import com.bandi.domain.kaist.AccessElementCondition;


public interface AccessElementService {

    int insert(AccessElement accesselement);

    AccessElement get(String oid);

    void update(AccessElement accesselement);

    void deleteBatch(List list);

    int delete(String id);

    Paginator search(Paginator paginator,AccessElement accesselement);

    long countByCondition( AccessElementCondition condition);

    int deleteByCondition( AccessElementCondition condition);

    List<AccessElement> selectByCondition( AccessElementCondition condition);

    int updateByConditionSelective( AccessElement record, AccessElementCondition condition);

    // ===================== End of Code Gen =====================

    void deleteByClientOid( String clientOid);

    void deleteByResourceOid ( String resourceOid);

    void deleteByAuthorityOid( String authorityOid );

    void saveAceList( String clientOid, String authorityOid, List<String> resourceOids, boolean isInherite);

    List<String> getMappedResourceOidsByTargetObjectId( String clientOid, String targetObjectId, String targetObjectType);
}
