package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.RoleAuthority;
import com.bandi.domain.kaist.RoleAuthorityCondition;


public interface RoleAuthorityService {

    int insert(RoleAuthority roleauthority);

    RoleAuthority get(String oid);

    void update(RoleAuthority roleauthority);

    void deleteBatch(List list);

    int delete(String id);

    Paginator search(Paginator paginator,RoleAuthority roleauthority);

    long countByCondition( RoleAuthorityCondition condition);

    int deleteByCondition( RoleAuthorityCondition condition);

    List<RoleAuthority> selectByCondition( RoleAuthorityCondition condition);

    int updateByConditionSelective( RoleAuthority record, RoleAuthorityCondition condition);

    // ===================== End of Code Gen =====================

    void deleteByClientOid( String clientOid);

    void deleteByRoleMasterOid( String roleMasterOid );

    void deleteByAuthorityOid( String authorityOid );

    void insertList(List<RoleAuthority> roleauthoritys);
}
