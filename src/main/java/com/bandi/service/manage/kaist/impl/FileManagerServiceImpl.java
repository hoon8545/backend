package com.bandi.service.manage.kaist.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.common.util.FileUtils;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.FileManagerMapper;
import com.bandi.domain.kaist.FileManager;
import com.bandi.domain.kaist.FileManagerCondition;
import com.bandi.service.manage.kaist.FileManagerService;
import com.bandi.service.manage.kaist.NoticeService;

@Service
public class FileManagerServiceImpl implements FileManagerService{

    @Resource
    private FileManagerMapper dao;
    
    @Resource
    private NoticeService noticeService;

    @Resource(name = "fileUtils")
    private FileUtils fileUtils;


    @SuppressWarnings("unchecked")
    public List<String> insertFile(Map<String, Object> map) throws Exception {
    	List<FileManager> list = (List<FileManager>) map.get(BandiConstants_KAIST.PARAM_FILEMANAGER);
		List<String> fileOids = (List<String>) map.get(BandiConstants_KAIST.PARAM_FILE_OIDS);

    	if(map.get(BandiConstants_KAIST.PARAM_FILE_OID) != null) { // 맵에 fileOid가 담겨있으면
    		String [] fileOid = map.get(BandiConstants_KAIST.PARAM_FILE_OID).toString().split(",");
    		for(int i=0; i<fileOid.length; i++) {
    			dao.deleteByPrimaryKey(fileOid[i]);
    		}
    	}

    	for(int i=0; i<list.size(); i++) {
    		dao.insertSelective(list.get(i));
    	}

    	return fileOids;
    }

    public FileManager get(String oid) {
        FileManager filemanager = null;
        filemanager = dao.selectByPrimaryKey(oid);
        return filemanager;
    }

    public void update(FileManager filemanager) {
    	filemanager = setUpdatorInfo( filemanager );

        dao.updateByPrimaryKeySelective(filemanager);
    }

    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }

    public void delete(String [] fileOid) {
    	FileManager file = get(fileOid[0]);
    	String targetObjectOid = file.getTargetObjectOid();
    	int targetCount = dao.selectCountByTargetObjectOid(targetObjectOid);
    	
    	if (targetCount == fileOid.length) {
    		noticeService.updateFlagFile(targetObjectOid);
    	}
    	
    	for(int i=0; i<fileOid.length; i++) {
    		dao.deleteByPrimaryKey(fileOid[i]);
    	}
    }

    public Paginator search(Paginator paginator, FileManager filemanager) {

        FileManagerCondition condition = new FileManagerCondition();

        if( filemanager != null) {
            String originalName = filemanager.getOriginalName();
            if (originalName!=null && !"".equals(originalName)){
                condition.or().andOriginalNameLike( filemanager.getOriginalName()+"%");
            }
            String storedName = filemanager.getStoredName();
            if (storedName!=null && !"".equals(storedName)){
                condition.or().andStoredNameLike( filemanager.getStoredName()+"%");
            }
            String path = filemanager.getPath();
            if (path!=null && !"".equals(path)){
                condition.or().andPathLike( filemanager.getPath()+"%");
            }
            int size = filemanager.getFileSize();
            condition.or().andSizeEqualTo( size);
            java.sql.Timestamp createdAt = filemanager.getCreatedAt();
            if (createdAt!=null){
                condition.or().andCreatedAtEqualTo(filemanager.getCreatedAt());
            }
            String creatorId = filemanager.getCreatorId();
            if (creatorId!=null && !"".equals(creatorId)){
                condition.or().andCreatorIdEqualTo( filemanager.getCreatorId());
            }
            String targetObjectType = filemanager.getTargetObjectType();
            if (targetObjectType!=null && !"".equals(targetObjectType)){
                condition.or().andTargetObjectTypeLike( filemanager.getTargetObjectType()+"%");
            }
            String targetObjectOid = filemanager.getTargetObjectOid();
            if (targetObjectOid!=null){
                condition.or().andTargetObjectOidEqualTo(filemanager.getTargetObjectOid());
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    public long countByCondition( FileManagerCondition condition){
        return dao.countByCondition( condition);
    }

    public int deleteByCondition( FileManagerCondition condition){
        return dao.deleteByCondition( condition);
    }

    public List<FileManager> selectByCondition( FileManagerCondition condition){
        return dao.selectByCondition( condition);
    }

    public int updateByConditionSelective( FileManager record, FileManagerCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }

    protected FileManager setCreatorInfo(FileManager filemanager){
    	filemanager.setCreatorId( ContextUtil.getCurrentUserId() );
    	filemanager.setCreatedAt( DateUtil.getNow() );
    	return filemanager;
    }

    protected FileManager setUpdatorInfo(FileManager filemanager){
    	filemanager.setUpdatorId( ContextUtil.getCurrentUserId() );
    	filemanager.setUpdatedAt( DateUtil.getNow() );
    	return filemanager;
    }

    // ===================== End of Code Gen =====================

    public List<FileManager> list(String oid){
    	return dao.selectByTargetObjectOid(oid);
    }
}
