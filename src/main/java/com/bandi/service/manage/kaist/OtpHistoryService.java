package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.OtpHistory;
import com.bandi.domain.kaist.OtpHistoryCondition;
import com.bandi.domain.JasperReportParams;

import net.sf.jasperreports.engine.JasperPrint;


public interface OtpHistoryService {

    int insert(OtpHistory otphistory);

    OtpHistory get(String oid);
    
    void update(OtpHistory otphistory);
    
    void deleteBatch(List list);
    
    int delete(String id);
    
    Paginator search(Paginator paginator,OtpHistory otphistory);

    long countByCondition( OtpHistoryCondition condition);

    int deleteByCondition( OtpHistoryCondition condition);

    List<OtpHistory> selectByCondition( OtpHistoryCondition condition);
    
    int updateByConditionSelective( OtpHistory record, OtpHistoryCondition condition);

    JasperPrint exportDataFileJasper(JasperReportParams jasperReportParams, OtpHistory otphistory);
    // ===================== End of Code Gen =====================

}
