package com.bandi.service.manage.kaist.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.dao.UserMapper;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.ServiceRequestMapper;
import com.bandi.domain.Configuration;
import com.bandi.domain.User;
import com.bandi.domain.kaist.ServiceRequest;
import com.bandi.domain.kaist.ServiceRequestCondition;
import com.bandi.service.cryption.CryptService;
import com.bandi.service.manage.ConfigurationService;
import com.bandi.service.manage.kaist.ServiceRequestService;

@Service
public class ServiceRequestServiceImpl implements ServiceRequestService{

    @Resource
    protected ServiceRequestMapper dao;

    @Autowired
    protected CryptService cryptService;

    @Resource
    protected ConfigurationService configurationService;
    
    @Resource
    protected UserMapper userDao;

    @Override
    public int insert(ServiceRequest servicerequest) {

    	if( servicerequest.getOid() == null || "".equals( servicerequest.getOid().trim())){
    		servicerequest.setOid( com.bandi.common.IdGenerator.getUUID());
        }

    	if( servicerequest.getDocNo() == null || "".equals( servicerequest.getDocNo().trim())){
    		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS", new Locale("de", "DE"));
    		String current_dt = formatter.format(new Date());
    		String random = Integer.toString(new Random().nextInt(89) + 10);

    		servicerequest.setDocNo(current_dt+random);
        }
    	
    	User user = userDao.getOriginalUserByKaistUid(servicerequest.getKaistUid());

        servicerequest.setUserName(user.getName());
        servicerequest.setUserType(user.getUserType());
        servicerequest.setFlagReg(BandiConstants_KAIST.FLAG_Y);
        servicerequest.setStatus("R");
        servicerequest.setUpdatedAt(null);
    	servicerequest = setCreatorInfo( servicerequest );

        return dao.insertSelective(servicerequest);
    }

    @Override
    public ServiceRequest get(String oid) {
        ServiceRequest servicerequest = null;
        servicerequest = dao.selectByPrimaryKey(oid);
        return servicerequest;
    }

    @Override
    public void update(ServiceRequest servicerequest) {

    	servicerequest = setUpdatorInfo( servicerequest );

        dao.updateByPrimaryKeySelective(servicerequest);
    }

    @Override
    public Paginator search(Paginator paginator, ServiceRequest servicerequest) {

        ServiceRequestCondition condition = new ServiceRequestCondition();

        if( servicerequest != null) {
            String kaistUid = servicerequest.getKaistUid();
            if (kaistUid!=null && !"".equals(kaistUid)){
                condition.or().andKaistUidEqualTo( servicerequest.getKaistUid());
            }
            String userName = servicerequest.getUserName();
            if (userName!=null && !"".equals(userName)){
                condition.or().andUserNameLike( servicerequest.getUserName()+"%");
            }
            String email = servicerequest.getEmail();
            if (email!=null && !"".equals(email)){
                condition.or().andEmailLike( servicerequest.getEmail()+"%");
            }
            String handphone = servicerequest.getHandphone();
            if (handphone!=null && !"".equals(handphone)){
                condition.or().andHandphoneLike( servicerequest.getHandphone()+"%");
            }
            String reqType = servicerequest.getReqType();
            if (reqType!=null && !"".equals(reqType)){
                condition.or().andReqTypeLike( servicerequest.getReqType());
            }
            String status = servicerequest.getStatus();
            if (status!=null && !"".equals(status)){
                condition.or().andStatusLike( servicerequest.getStatus()+"%");
            }
            if (servicerequest.getCreatedAtBetween() != null && servicerequest.getCreatedAtBetween().length == 2) {
				condition.or().andCreatedAtBetween(servicerequest.getCreatedAtBetween()[0],
						servicerequest.getCreatedAtBetween()[1]);
			} else {
				java.sql.Timestamp createdAt = servicerequest.getCreatedAt();
				if (createdAt != null) {
					condition.or().andCreatedAtEqualTo(servicerequest.getCreatedAt());
				}
			}
            if (servicerequest.getUpdatedAtBetween() != null && servicerequest.getUpdatedAtBetween().length == 2) {
				condition.or().andUpdatedAtBetween(servicerequest.getUpdatedAtBetween()[0],
						servicerequest.getUpdatedAtBetween()[1]);
			} else {
				java.sql.Timestamp updatedAt = servicerequest.getUpdatedAt();
				if (updatedAt != null) {
					condition.or().andUpdatedAtEqualTo(servicerequest.getUpdatedAt());
				}
			}
            String approver = servicerequest.getApprover();
            if (approver!=null && !"".equals(approver)){
                condition.or().andApproverLike( servicerequest.getApprover()+"%");
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    @Override
    public long countByCondition( ServiceRequestCondition condition){
        return dao.countByCondition( condition);
    }

    @Override
    public List<ServiceRequest> selectByCondition( ServiceRequestCondition condition){
        return dao.selectByCondition( condition);
    }

    @Override
    public int updateByConditionSelective( ServiceRequest record, ServiceRequestCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }

    protected ServiceRequest setCreatorInfo(ServiceRequest servicerequest){
    	servicerequest.setCreatorId( ContextUtil.getCurrentUserId() );
    	servicerequest.setCreatedAt( DateUtil.getNow() );
    	return servicerequest;
    }

    protected ServiceRequest setUpdatorInfo(ServiceRequest servicerequest){
    	servicerequest.setUpdatorId( ContextUtil.getCurrentUserId() );
    	servicerequest.setUpdatedAt( DateUtil.getNow() );
    	return servicerequest;
    }

    // ===================== End of Code Gen =====================

    @Override
    public String getInitPssword(String userId) {

        Configuration configuration = configurationService.get("PASSWORD_SPECIAL_CHARACTERS");
        String specialChar = configuration.getConfigValue();
        int random = (int)(Math.random() * specialChar.length());
        String initPassword = userId + specialChar.charAt(random) + cryptService.getRandomString(4);

        return initPassword;
    }
    
    @Override
    public List<ServiceRequest> getAdminMainServiceRequest() {
    	return dao.getAdminMainServiceRequest();
    }

}
