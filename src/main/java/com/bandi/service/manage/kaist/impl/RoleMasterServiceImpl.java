package com.bandi.service.manage.kaist.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import com.bandi.common.BandiConstants;
import com.bandi.common.IdGenerator;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.common.util.MessageUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.RoleMasterMapper;
import com.bandi.domain.Group;
import com.bandi.domain.User;
import com.bandi.domain.kaist.RoleMaster;
import com.bandi.domain.kaist.RoleMasterCondition;
import com.bandi.domain.kaist.RoleMember;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.GroupService_KAIST;
import com.bandi.service.manage.kaist.RoleAuthorityService;
import com.bandi.service.manage.kaist.RoleMasterService;
import com.bandi.service.manage.kaist.RoleMemberService;
import com.bandi.service.manage.kaist.UserService_KAIST;

public class RoleMasterServiceImpl implements RoleMasterService{

    @Resource
    protected RoleMasterMapper dao;

    @Resource
    protected RoleMemberService roleMemberService;

    @Resource
    protected RoleAuthorityService roleAuthorityService;

    @Resource
    protected UserService_KAIST userService;

    @Resource
    protected GroupService_KAIST groupService;

    @Override
    public int insert(RoleMaster rolemaster) {

        if( rolemaster.getOid() == null || rolemaster.getOid().trim().length() == 0){
            rolemaster.setOid( IdGenerator.getUUID() );
        }

        checkDuplicateOid(rolemaster);

        checkExistGroup(rolemaster);

    	rolemaster = setCreatorInfo( rolemaster );

        return dao.insertSelective(rolemaster);
    }

    protected void checkExistGroup(RoleMaster rolemaster) {
        if (rolemaster.getGroupId() == null) {
            throw new BandiException(ErrorCode_KAIST.TARGET_GROUPID_IS_NULL);
        }

        Group group = groupService.get(rolemaster.getGroupId());

        if (group == null) {
            throw new BandiException(ErrorCode_KAIST.TARGET_ROLE_GROUP_NOT_FOUND);
        }

    }

    public void checkDuplicateOid(RoleMaster rolemaster) {
        RoleMaster roleMasterFromDB = get(rolemaster.getOid());

        if (roleMasterFromDB != null) {
            throw new BandiException( ErrorCode_KAIST.ROLEMASTER_ID_DUPLICATED, new String[] {rolemaster.getOid()});
        }
    }


    @Override
    public RoleMaster get(String oid) {
        RoleMaster rolemaster = null;
        rolemaster = dao.selectByPrimaryKey(oid);
        return rolemaster;
    }

    @Override
    public void update(RoleMaster rolemaster) {
    	rolemaster = setUpdatorInfo( rolemaster );

        dao.updateByPrimaryKeySelective(rolemaster);
    }

    @Override
    public void deleteBatch(List list) {
        for( int i = 0; i < list.size(); i++){
            delete(  (String)list.get( i ));
        }
    }

    @Override
    public int delete(String id) {

        roleAuthorityService.deleteByRoleMasterOid( id );
        roleMemberService.deleteByRoleMasterOid( id );

        return dao.deleteByPrimaryKey(id);
    }

    @Override
    public Paginator search(Paginator paginator, RoleMaster rolemaster) {

        RoleMasterCondition condition = new RoleMasterCondition();

        if( rolemaster != null) {
            String groupId = rolemaster.getGroupId();
            if (groupId!=null && !"".equals(groupId)){
                condition.or().andGroupIdEqualTo( rolemaster.getGroupId());
            }
            String name = rolemaster.getName();
            if (name!=null && !"".equals(name)){
                condition.or().andNameLike( rolemaster.getName()+"%");
            }
            String roleType = rolemaster.getRoleType();
            if (roleType!=null && !"".equals(roleType)){
                condition.or().andRoleTypeLike( rolemaster.getRoleType()+"%");
            }
            String flagAutoDelete = rolemaster.getFlagAutoDelete();
            if (flagAutoDelete!=null && !"".equals(flagAutoDelete)){
                condition.or().andFlagAutoDeleteEqualTo( rolemaster.getFlagAutoDelete());
            }
            String flagDelegated = rolemaster.getFlagDelegated();
            if (flagDelegated!=null && !"".equals(flagDelegated)){
                condition.or().andFlagDelegatedEqualTo( rolemaster.getFlagDelegated());
            }

            if( rolemaster.getAuthorityOid() != null && rolemaster.getAuthorityOid().length() > 0){
                condition.setAuthorityOid( rolemaster.getAuthorityOid() );
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    @Override
    public long countByCondition( RoleMasterCondition condition){
        return dao.countByCondition( condition);
    }

    @Override
    public int deleteByCondition( RoleMasterCondition condition){
        return dao.deleteByCondition( condition);
    }

    @Override
    public List<RoleMaster> selectByCondition( RoleMasterCondition condition){
        return dao.selectByCondition( condition);
    }

    @Override
    public int updateByConditionSelective( RoleMaster record, RoleMasterCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }

    protected RoleMaster setCreatorInfo(RoleMaster rolemaster){
    	rolemaster.setCreatorId( ContextUtil.getCurrentUserId() );
    	rolemaster.setCreatedAt( DateUtil.getNow() );
    	return rolemaster;
    }

    protected RoleMaster setUpdatorInfo(RoleMaster rolemaster){
    	rolemaster.setUpdatorId( ContextUtil.getCurrentUserId() );
    	rolemaster.setUpdatedAt( DateUtil.getNow() );
    	return rolemaster;
    }


    // ===================== End of Code Gen =====================

    @Override
    public void updateDeptRoleName( String roleMasterOid, String name) {
        RoleMaster roleMaster = new RoleMaster();
        roleMaster.setOid( roleMasterOid );
        roleMaster.setDescription( name );

        dao.updateByPrimaryKeySelective(roleMaster);
    }

    @Override
    public void deleteByGroupId( String roleMasterOid ){
        RoleMasterCondition condition = new RoleMasterCondition();
        condition.createCriteria().andGroupIdEqualTo( roleMasterOid );

        deleteByCondition( condition );
    }

    @Override
    public void createDeptRole( String groupId, String groupName ){
        RoleMaster roleMaster = new RoleMaster();

        roleMaster.setGroupId( groupId );
        roleMaster.setName( groupName );
        roleMaster.setRoleType( RoleMaster.ROLE_TYPE_DEPARTMENT);
        roleMaster.setFlagAutoDelete( BandiConstants.FLAG_Y );
        roleMaster.setDescription( groupName + " dept role" );

        insert(  roleMaster );
    }


    @Override
    public void createDeptManageRole( String groupId, String groupName, String parentGroupId ){

        RoleMaster roleMaster = new RoleMaster();

        roleMaster.setGroupId( groupId );
        roleMaster.setName( groupName + "' " + MessageUtil.get( "ROLE_NAME_DEPARTMENT_AUTHORITY_MANAGE" ) );
        roleMaster.setRoleType( RoleMaster.ROLE_TYPE_MANAGE);
        roleMaster.setFlagAutoDelete( BandiConstants.FLAG_Y );
        roleMaster.setDescription( groupName + "' " + MessageUtil.get( "ROLE_NAME_DEPARTMENT_AUTHORITY_MANAGE" ) );

        if( roleMaster.getOid() == null || roleMaster.getOid().trim().length() == 0){
            roleMaster.setOid( IdGenerator.getUUID() );
        }

        insert(  roleMaster );
    }

    @Override
    public Paginator selfRoleSearch(Paginator paginator, RoleMaster rolemaster) {
        RoleMasterCondition condition = new RoleMasterCondition();

        List<String> userTypes = null;
        if( rolemaster.getRoleMemberUserId() != null) {
            condition.setRoleMemberUserId( rolemaster.getRoleMemberUserId() );

            User user = userService.get( rolemaster.getRoleMemberUserId() );

            if( user.getUserType() != null){
                String[] arrTemp = user.getUserType().split( "" );
                userTypes = new ArrayList<String>( Arrays.asList(arrTemp));
            }

            condition.setUserTypes( userTypes );
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.getSelfRoleTotalCount(rolemaster.getRoleMemberUserId(), userTypes ));
        paginator.setList(dao.selfRoleSearch(paginatorex));

        return paginator;
    }

    public List<String> getRoleByUser( String userId, List<String> userTypes) {

        List< String > roleMasterOids = new ArrayList<>();

        if(userId != null){
            roleMasterOids = dao.getRoleByUser( userId, userTypes );
        }

        return roleMasterOids;
    }
}
