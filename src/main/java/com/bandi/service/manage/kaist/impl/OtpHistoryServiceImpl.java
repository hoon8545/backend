package com.bandi.service.manage.kaist.impl;

import java.util.*;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.OtpHistoryMapper;
import com.bandi.domain.kaist.OtpHistory;
import com.bandi.domain.kaist.OtpHistoryCondition;
import com.bandi.service.manage.kaist.OtpHistoryService;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;

import org.springframework.beans.factory.annotation.Autowired;

import java.sql.*;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;

import org.apache.ibatis.mapping.*;
import org.apache.ibatis.session.*;

import com.bandi.common.*;
import com.bandi.domain.JasperReportParams;
import com.bandi.exception.*;

import net.sf.jasperreports.engine.*;
@Service
public class OtpHistoryServiceImpl implements OtpHistoryService {

    @Resource
    protected OtpHistoryMapper dao;
    
    @Autowired
    protected DataSource dataSource;

    @Autowired
    protected SqlSessionFactory sqlSessionFactory;

    public int insert(OtpHistory otphistory) {
        if (otphistory.getOid() == null || "".equals(otphistory.getOid())) {
        	otphistory.setOid(com.bandi.common.IdGenerator.getUUID());
        }
		otphistory = setCreatorInfo( otphistory );
		
        return dao.insertSelective(otphistory);
    }

    public OtpHistory get(String oid) {
        OtpHistory otphistory = null;
        otphistory = dao.selectByPrimaryKey(oid);
        return otphistory;
    }

    public void update(OtpHistory otphistory) {
    	otphistory = setUpdatorInfo( otphistory );
    	
        dao.updateByPrimaryKeySelective(otphistory);
    }

    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }
    
    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }   

    public Paginator search(Paginator paginator, OtpHistory otphistory) {

        OtpHistoryCondition condition = new OtpHistoryCondition();

        if( otphistory != null) {
            String userId = otphistory.getUserId();
            if (userId!=null && !"".equals(userId)){
                condition.or().andUserIdEqualTo( otphistory.getUserId());
            }
            String loginIp = otphistory.getLoginIp();
            if (loginIp!=null && !"".equals(loginIp)){
                condition.or().andLoginIpLike( otphistory.getLoginIp()+"%");
            }
            if (otphistory.getCertifiedAtBetween() != null && otphistory.getCertifiedAtBetween().length == 2) {
				condition.or().andCertifiedAtBetween(otphistory.getCertifiedAtBetween()[0],
						otphistory.getCertifiedAtBetween()[1]);
			} else {
				java.sql.Timestamp certifedAt = otphistory.getCertifiedAt();
				if (certifedAt != null) {
					condition.or().andCertifiedAtEqualTo(otphistory.getCertifiedAt());
				}
			}
            String certificationResult = otphistory.getCertificationResult();
            if (certificationResult!=null && !"".equals(certificationResult)){
                condition.or().andCertificationResultLike( otphistory.getCertificationResult()+"%");
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);
        
        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;       
    }

    public long countByCondition( OtpHistoryCondition condition){
        return dao.countByCondition( condition);
    }

    public int deleteByCondition( OtpHistoryCondition condition){
        return dao.deleteByCondition( condition);
    }

    public List<OtpHistory> selectByCondition( OtpHistoryCondition condition){
        return dao.selectByCondition( condition);
    }

    public int updateByConditionSelective( OtpHistory record, OtpHistoryCondition condition){
    	record = setUpdatorInfo( record );
    	
        return dao.updateByConditionSelective( record, condition);
    }
    
    public JasperPrint exportDataFileJasper(JasperReportParams jasperReportParams, OtpHistory otphistory) {
        int endPage;
        
        switch (jasperReportParams.getPageFlag()) {
        case "T":
            String totalPageStr = jasperReportParams.getTotalPageStr();
            int totalPage = 0;
            
            if(totalPageStr == null || totalPageStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                totalPage = Integer.parseInt(totalPageStr);
            }
            
            otphistory.setPageNo(1);
            endPage = totalPage;
            break;
        case "R":
            String rangeStartStr = jasperReportParams.getRangeStartStr();
            String rangeEndStr = jasperReportParams.getRangeEndStr();
            
            int rangeStart = 0;
            int rangeEnd = 0;
            
            if(rangeStartStr == null || rangeStartStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                rangeStart = Integer.parseInt(rangeStartStr);
            }
            
            if(rangeEndStr == null || rangeEndStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                rangeEnd = Integer.parseInt(rangeEndStr);
            }
            
            otphistory.setPageNo(rangeStart);
            endPage = rangeEnd;
            break;
        default:
            endPage = otphistory.getPageNo();
            break;
        }
        
        OtpHistoryCondition condition = new OtpHistoryCondition();

        if( otphistory != null) {
            String userId = otphistory.getUserId();
            if (userId!=null && !"".equals(userId)){
                condition.or().andUserIdEqualTo( otphistory.getUserId());
            }
            String loginIp = otphistory.getLoginIp();
            if (loginIp!=null && !"".equals(loginIp)){
                condition.or().andLoginIpLike( otphistory.getLoginIp()+"%");
            }
            java.sql.Timestamp certifiedAt = otphistory.getCertifiedAt();
            if (certifiedAt!=null){
                condition.or().andCertifiedAtEqualTo(otphistory.getCertifiedAt());
            }
            String certificationResult = otphistory.getCertificationResult();
            if (certificationResult!=null && !"".equals(certificationResult)){
                condition.or().andCertificationResultLike( otphistory.getCertificationResult()+"%");
            }
        }
        
        String sortDirection = otphistory.isSortDesc() ? "DESC" : "ASC";
        Paginator paginator = new Paginator(otphistory.getPageNo(), endPage, otphistory.getPageSize(), otphistory.getSortBy(),
                sortDirection);

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(), paginator.getEndPage(), paginator.getRowsperpage());
        paginatorex.setCondition(condition);
        
        SqlSession sqlSession = sqlSessionFactory.openSession();
        MappedStatement ms = sqlSession.getConfiguration()
                .getMappedStatement("com.bandi.dao.OtpHistoryMapper.pagingQueryForSearch");
        BoundSql boundSql = ms.getBoundSql(paginatorex);

        List<ParameterMapping> boundParams = boundSql.getParameterMappings();
        String sql = boundSql.getSql();
        String paramString = "";
        boolean timestampFlag = true;
        for (ParameterMapping param : boundParams) {
            if (boundSql.getAdditionalParameter(param.getProperty()) instanceof Integer) {
                paramString = Integer.toString((int) boundSql.getAdditionalParameter(param.getProperty()));
            } else if (boundSql.getAdditionalParameter(param.getProperty()) instanceof Timestamp) {
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
                
                String databaseType = BandiProperties.BANDI_DATABASE_TYPE;
                String convertTimestamp = "";
                
                if("oracle".equals(databaseType)) {
                    convertTimestamp =  "TO_TIMESTAMP('"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            + "', 'mm/dd/yyyy hh24:mi:ss.ff3') "; 
                }else if("mariadb".equals(databaseType)) {
                    convertTimestamp = "STR_TO_DATE('"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            + "', '%m/%d/%Y %H:%i:%S.%x') ";
                }else if("mssql".equals(databaseType)) {
                    convertTimestamp = "CONVERT(CHAR(23),'"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            +"',21)";
                }
                
                if (timestampFlag) {
                    paramString = convertTimestamp;
                    timestampFlag = false;
                } else {
                    paramString = convertTimestamp;
                }

            } else {
                paramString = (String) boundSql.getAdditionalParameter(param.getProperty());
            }
            
            if (boundSql.getAdditionalParameter(param.getProperty()) instanceof String) {
                sql = sql.replaceFirst("\\?", "'" + paramString + "'");
            } else {
                sql = sql.replaceFirst("\\?", paramString);
            }
        }
        sql = sql.replaceAll("(\\s)", " ");
        
        Map<String, Object> params = new HashMap<>();
        params.put("pageName", "OtpHistory");
        params.put("sql", sql);
        params.put("userId", otphistory.getUserId());
        params.put("loginIp", otphistory.getLoginIp());
        params.put("certifiedAt", otphistory.getCertifiedAt());
        params.put("certificationResult", otphistory.getCertificationResult());
        
        try {
            JasperReport otphistoryReport = JasperCompileManager.compileReport(jasperReportParams.getJrxmlPath());

            Connection con = dataSource.getConnection();
        
            JasperPrint jasperPrint = JasperFillManager.fillReport(otphistoryReport, params, con);

            return jasperPrint;
        } catch (Exception e) {
            throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
        }
    }
    
    protected OtpHistory setCreatorInfo(OtpHistory otphistory){
    	otphistory.setCreatorId( ContextUtil.getCurrentUserId() );
    	otphistory.setCreatedAt( DateUtil.getNow() );
    	return otphistory;
    }
    
    protected OtpHistory setUpdatorInfo(OtpHistory otphistory){
    	otphistory.setUpdatorId( ContextUtil.getCurrentUserId() );
    	otphistory.setUpdatedAt( DateUtil.getNow() );
    	return otphistory;
    }
    
    // ===================== End of Code Gen =====================

}
