package com.bandi.service.manage.kaist.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.SyncResourceMapper;
import com.bandi.domain.kaist.SyncResource;
import com.bandi.domain.kaist.SyncResourceCondition;
import com.bandi.service.manage.kaist.SyncResourceService;

@Service
public class SyncResourceServiceImpl implements SyncResourceService {

    @Resource
    protected SyncResourceMapper dao;
    

    public int insert(SyncResource syncresource) {
        // PK에 해당하는 변수에 값을 채워주세요.
		syncresource = setCreatorInfo( syncresource );
		
        return dao.insertSelective(syncresource);
    }

    public SyncResource get(String oid) {
        SyncResource syncresource = null;
        syncresource = dao.selectByPrimaryKey(oid);
        return syncresource;
    }

    public void update(SyncResource syncresource) {
    	syncresource = setUpdatorInfo( syncresource );
    	
        dao.updateByPrimaryKeySelective(syncresource);
    }

    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }
    
    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }   

    public Paginator search(Paginator paginator, SyncResource syncresource) {

        SyncResourceCondition condition = new SyncResourceCondition();

        if( syncresource != null) {
            String clientOid = syncresource.getClientOid();
            if (clientOid!=null && !"".equals(clientOid)){
                condition.or().andClientOidEqualTo( syncresource.getClientOid());
            }
            String resourceOid = syncresource.getResourceOid();
            if (resourceOid!=null && !"".equals(resourceOid)){
                condition.or().andResourceOidEqualTo( syncresource.getResourceOid());
            }
            String name = syncresource.getName();
            if (name!=null && !"".equals(name)){
                condition.or().andNameLike( syncresource.getName()+"%");
            }
            String parentOid = syncresource.getParentOid();
            if (parentOid!=null && !"".equals(parentOid)){
                condition.or().andParentOidEqualTo( syncresource.getParentOid());
            }
            String actionType = syncresource.getActionType();
            if (actionType!=null && !"".equals(actionType)){
                condition.or().andActionTypeLike( syncresource.getActionType()+"%");
            }
            String actionStatus = syncresource.getActionStatus();
            if (actionStatus!=null && !"".equals(actionStatus)){
                condition.or().andActionStatusLike( syncresource.getActionStatus()+"%");
            }
            java .sql.Timestamp createdAt = syncresource.getCreatedAt();
            if (createdAt!=null){
                condition.or().andCreatedAtEqualTo(syncresource.getCreatedAt());
            }
            java .sql.Timestamp appliedAt = syncresource.getAppliedAt();
            if (appliedAt!=null){
                condition.or().andAppliedAtEqualTo(syncresource.getAppliedAt());
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);
        
        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;       
    }

    public long countByCondition( SyncResourceCondition condition){
        return dao.countByCondition( condition);
    }

    public int deleteByCondition( SyncResourceCondition condition){
        return dao.deleteByCondition( condition);
    }

    public List<SyncResource> selectByCondition( SyncResourceCondition condition){
        return dao.selectByCondition( condition);
    }

    public int updateByConditionSelective( SyncResource record, SyncResourceCondition condition){
    	record = setUpdatorInfo( record );
    	
        return dao.updateByConditionSelective( record, condition);
    }
    
    
    protected SyncResource setCreatorInfo(SyncResource syncresource){
    	syncresource.setCreatorId( ContextUtil.getCurrentUserId() );
    	syncresource.setCreatedAt( DateUtil.getNow() );
    	return syncresource;
    }
    
    protected SyncResource setUpdatorInfo(SyncResource syncresource){
    	syncresource.setUpdatorId( ContextUtil.getCurrentUserId() );
    	syncresource.setUpdatedAt( DateUtil.getNow() );
    	return syncresource;
    }
    
    // ===================== End of Code Gen =====================

}
