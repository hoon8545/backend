package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.User;
import com.bandi.domain.kaist.NiceResult;
import com.bandi.domain.kaist.UserDetail;
import com.bandi.domain.kaist.UserDetailCondition;


public interface UserDetailService {

    int insert(UserDetail userdetail);

    UserDetail get(String kaistuid);

    void update(UserDetail userdetail);

    void deleteBatch(List list);

    int delete(String id);

    Paginator search(Paginator paginator,UserDetail userdetail);

    long countByCondition( UserDetailCondition condition);

    int deleteByCondition( UserDetailCondition condition);

    List<UserDetail> selectByCondition( UserDetailCondition condition);

    int updateByConditionSelective( UserDetail record, UserDetailCondition condition);

    // ===================== End of Code Gen =====================

    String getKaistUidByEmployeeNumber(String employeeNumber, String birthday);

    String getKaistUidByStudentNumber(String studentNumber, String birthday);

    void checkKaistUidforMailandPhone(String type, UserDetail userdetail, String serviceType);

    void updateUserId(User user);

    NiceResult niceResult(NiceResult niceResult);

}
