package com.bandi.service.manage.kaist.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bandi.common.BandiConstants;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.OtpSecurityAreaMapper;
import com.bandi.domain.Client;
import com.bandi.domain.User;
import com.bandi.domain.kaist.OtpSecurityArea;
import com.bandi.domain.kaist.OtpSecurityAreaCondition;
import com.bandi.service.manage.ClientService;
import com.bandi.service.manage.UserService;
import com.bandi.service.manage.kaist.OtpSecurityAreaService;

@Service
public class OtpSecurityAreaServiceImpl implements OtpSecurityAreaService {

    @Resource
    protected OtpSecurityAreaMapper dao;

    @Autowired
    UserService userService;

    @Autowired
    ClientService clientService;

    @Override
    public int insert(OtpSecurityArea otpsecurityarea) {
        if( otpsecurityarea.getOid() == null || "".equals( otpsecurityarea.getOid().trim())){
            otpsecurityarea.setOid( com.bandi.common.IdGenerator.getUUID());
        }

        // KAIST - OTP사용여부를 확인한 후 UPDATE
        if (otpsecurityarea.getFlagUse() == null || "".equals(otpsecurityarea.getFlagUse().trim())) {
            otpsecurityarea.setFlagUse(BandiConstants.FLAG_Y);
        } else {
            otpsecurityarea.setFlagUse(BandiConstants.FLAG_N);
        }

        OtpSecurityArea otpSecurityAreaFromDb = get(otpsecurityarea.getUserId());
        User userFromDb = userService.get(otpsecurityarea.getUserId());
        Client clientFromDb = clientService.get(otpsecurityarea.getClientOid());

		otpsecurityarea = setCreatorInfo( otpsecurityarea );

        return dao.insertSelective(otpsecurityarea);
    }

    @Override
    public OtpSecurityArea get(String oid) {
        OtpSecurityArea otpsecurityarea = null;
        otpsecurityarea = dao.selectByPrimaryKey(oid);
        return otpsecurityarea;
    }

    @Override
    public void update(OtpSecurityArea otpsecurityarea) {

        if ((otpsecurityarea.getFlagUse().equals(BandiConstants.FLAG_N))) {
            otpsecurityarea.setFlagUse(BandiConstants.FLAG_Y);
        } else {
            otpsecurityarea.setFlagUse(BandiConstants.FLAG_N);
        }

        otpsecurityarea = setUpdatorInfo( otpsecurityarea );

        dao.updateByPrimaryKeySelective(otpsecurityarea);
    }

    @Override
    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }

    @Override
    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }

    @Override
    public Paginator search(Paginator paginator, OtpSecurityArea otpsecurityarea) {

        OtpSecurityAreaCondition condition = new OtpSecurityAreaCondition();

        if( otpsecurityarea != null) {
            String userId = otpsecurityarea.getUserId();
            if (userId!=null && !"".equals(userId)){
                condition.or().andUserIdEqualTo( otpsecurityarea.getUserId());
            }
            String clientOid = otpsecurityarea.getClientOid();
            if (clientOid!=null && !"".equals(clientOid)){
                condition.or().andClientOidEqualTo( otpsecurityarea.getClientOid());
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    @Override
    public long countByCondition( OtpSecurityAreaCondition condition){
        return dao.countByCondition( condition);
    }

    @Override
    public int deleteByCondition( OtpSecurityAreaCondition condition){
        return dao.deleteByCondition( condition);
    }

    @Override
    public List<OtpSecurityArea> selectByCondition( OtpSecurityAreaCondition condition){
        return dao.selectByCondition( condition);
    }

    @Override
    public int updateByConditionSelective( OtpSecurityArea record, OtpSecurityAreaCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }


    protected OtpSecurityArea setCreatorInfo(OtpSecurityArea otpsecurityarea){
    	otpsecurityarea.setCreatorId( ContextUtil.getCurrentUserId() );
    	otpsecurityarea.setCreatedAt( DateUtil.getNow() );
    	return otpsecurityarea;
    }

    protected OtpSecurityArea setUpdatorInfo(OtpSecurityArea otpsecurityarea){
    	otpsecurityarea.setUpdatorId( ContextUtil.getCurrentUserId() );
    	otpsecurityarea.setUpdatedAt( DateUtil.getNow() );
    	return otpsecurityarea;
    }

    // ===================== End of Code Gen =====================

    @Override
    public String dataFromDb(String userId, String clientOid) {
        String otpSecurityAreaOid = dao.dataFromDb(userId, clientOid);
        return otpSecurityAreaOid;
    }

}
