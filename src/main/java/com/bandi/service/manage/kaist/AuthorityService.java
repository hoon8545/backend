package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.Authority;
import com.bandi.domain.kaist.AuthorityCondition;


public interface AuthorityService {

    int insert(Authority authority);

    Authority get(String oid);

    void update(Authority authority);

    void deleteBatch(List list);

    int delete(String id);

    Paginator search(Paginator paginator,Authority authority);

    long countByCondition( AuthorityCondition condition);

    int deleteByCondition( AuthorityCondition condition);

    List<Authority> selectByCondition( AuthorityCondition condition);

    int updateByConditionSelective( Authority record, AuthorityCondition condition);

    // ===================== End of Code Gen =====================

    void deleteByClientOid( String clientOid );

}
