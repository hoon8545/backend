package com.bandi.service.manage.kaist;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.FileManager;
import com.bandi.domain.kaist.FileManagerCondition;


public interface FileManagerService {

    List<String> insertFile(Map<String, Object> map) throws Exception;

    FileManager get(String oid);
    
    void update(FileManager filemanager);
    
    void deleteBatch(List list);
    
    void delete(String [] fileOid);
    
    Paginator search(Paginator paginator,FileManager filemanager);

    long countByCondition( FileManagerCondition condition);

    int deleteByCondition( FileManagerCondition condition);

    List<FileManager> selectByCondition( FileManagerCondition condition);
    
    int updateByConditionSelective( FileManager record, FileManagerCondition condition);

    // ===================== End of Code Gen =====================
    
    List<FileManager> list(String oid);
}
