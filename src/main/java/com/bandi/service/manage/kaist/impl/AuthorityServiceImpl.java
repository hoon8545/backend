package com.bandi.service.manage.kaist.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;

import com.bandi.common.IdGenerator;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AuthorityMapper;
import com.bandi.domain.Client;
import com.bandi.domain.kaist.Authority;
import com.bandi.domain.kaist.AuthorityCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.AccessElementService;
import com.bandi.service.manage.kaist.AuthorityService;
import com.bandi.service.manage.kaist.ClientService_KAIST;
import com.bandi.service.manage.kaist.RoleAuthorityService;

public class AuthorityServiceImpl implements AuthorityService {

    @Autowired
    RoleAuthorityService roleAuthorityService;

    @Autowired
    AccessElementService accessElementService;

    @Resource
    ClientService_KAIST clientService;

    @Resource
    protected AuthorityMapper dao;

    @Override
    public int insert(Authority authority) {

        if( authority.getOid() == null || authority.getOid().trim().length() == 0){
            authority.setOid( IdGenerator.getUUID() );
        }

        checkDuplicateOid(authority);

        checkExsitClientOid(authority);

        checkDuplicatedName(authority);

		authority = setCreatorInfo( authority );

        return dao.insertSelective(authority);
    }

    protected void checkDuplicatedName( Authority authority ){
        if (dao.isDuplicatedName( authority.getClientOid(), authority.getName())) {
            throw new BandiException( ErrorCode_KAIST.AUTHORITY_NAME_DUPLICATED);
        }
    }

    public void checkDuplicateOid(Authority authority) {
        Authority authorityFromDB = get(authority.getOid());

        if (authorityFromDB != null) {
            throw new BandiException( ErrorCode_KAIST.AUTHORITY_ID_DUPLICATED, new String[] {authority.getOid()});
        }
    }

    public void checkExsitClientOid(Authority authority) {
        if (authority.getClientOid() == null) {
            throw new BandiException(ErrorCode_KAIST.TARGET_CLIENTOID_IS_NULL);
        }

        Client client = clientService.get(authority.getClientOid());

        if (client == null) {
            throw new BandiException(ErrorCode_KAIST.TARGET_CLIENT_NOT_FOUND);
        }

    }

    @Override
    public Authority get(String oid) {
        Authority authority = null;
        authority = dao.selectByPrimaryKey(oid);
        return authority;
    }

    @Override
    public void update(Authority authority) {
    	authority = setUpdatorInfo( authority );

        dao.updateByPrimaryKeySelective(authority);
    }

    @Override
    public void deleteBatch(List list) {

        for( int i = 0; i < list.size(); i++){
            delete(  (String)list.get( i ));
        }
    }

    @Override
    public int delete(String id) {

        roleAuthorityService.deleteByAuthorityOid( id );

        accessElementService.deleteByAuthorityOid( id );

        return dao.deleteByPrimaryKey(id);
    }

    @Override
    public Paginator search(Paginator paginator, Authority authority) {

        AuthorityCondition condition = new AuthorityCondition();

        if( authority != null) {
            String clientOid = authority.getClientOid();
            if (clientOid!=null && !"".equals(clientOid)){
                condition.or().andClientOidEqualTo( authority.getClientOid());
            }
            String name = authority.getName();
            if (name!=null && !"".equals(name)){
                condition.or().andNameLike( authority.getName()+"%");
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        if( authority.getResourceOid() != null && authority.getResourceOid().trim().length() > 0){
            condition.setResourceOid( authority.getResourceOid() );
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    @Override
    public long countByCondition( AuthorityCondition condition){
        return dao.countByCondition( condition);
    }

    @Override
    public int deleteByCondition( AuthorityCondition condition){
        return dao.deleteByCondition( condition);
    }

    @Override
    public List<Authority> selectByCondition( AuthorityCondition condition){
        return dao.selectByCondition( condition);
    }

    @Override
    public int updateByConditionSelective( Authority record, AuthorityCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }


    protected Authority setCreatorInfo(Authority authority){
    	authority.setCreatorId( ContextUtil.getCurrentUserId() );
    	authority.setCreatedAt( DateUtil.getNow() );
    	return authority;
    }

    protected Authority setUpdatorInfo(Authority authority){
    	authority.setUpdatorId( ContextUtil.getCurrentUserId() );
    	authority.setUpdatedAt( DateUtil.getNow() );
    	return authority;
    }

    // ===================== End of Code Gen =====================


    @Override
    public void deleteByClientOid( String clientOid ){

        AuthorityCondition condition = new AuthorityCondition();
        condition.createCriteria().andClientOidEqualTo( clientOid );

        deleteByCondition( condition );

    }
}
