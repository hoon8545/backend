package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.Notice;
import com.bandi.domain.kaist.NoticeCondition;


public interface NoticeService {

    int insert(Notice notice, List<String> fileOid);
    
    Notice get(String oid);
    
    void update(Notice notice, String oid, List<String> fileOid);
    
    void deleteBatch(List list);
    
    int delete(String oid);
    
    Paginator search(Paginator paginator,Notice notice);

    long countByCondition( NoticeCondition condition);

    int deleteByCondition( NoticeCondition condition);

    List<Notice> selectByCondition( NoticeCondition condition);
    
    int updateByConditionSelective( Notice record, NoticeCondition condition);

    // ===================== End of Code Gen =====================
    
    Notice read(String oid);
    
    List<Notice> getUserLoginNotice();
    
    void updateFlagFile(String oid);

}
