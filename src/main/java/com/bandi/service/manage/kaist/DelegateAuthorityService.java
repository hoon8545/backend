package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.DelegateAuthority;
import com.bandi.domain.kaist.DelegateAuthorityCondition;


public interface DelegateAuthorityService {

    int insert(DelegateAuthority delegateauthority);

    DelegateAuthority get(String oid);

    void update(DelegateAuthority delegateauthority);

    void deleteBatch(List list);

    int delete(String id);

    Paginator search(Paginator paginator,DelegateAuthority delegateauthority);

    long countByCondition( DelegateAuthorityCondition condition);

    int deleteByCondition( DelegateAuthorityCondition condition);

    List<DelegateAuthority> selectByCondition( DelegateAuthorityCondition condition);

    int updateByConditionSelective( DelegateAuthority record, DelegateAuthorityCondition condition);

    // ===================== End of Code Gen =====================

}
