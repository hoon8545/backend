package com.bandi.service.manage.kaist.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;

import com.bandi.common.BandiConstants;
import com.bandi.common.cache.BandiCacheManager;
import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.common.util.CommonUtil;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.MessageUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.domain.Client;
import com.bandi.domain.ClientCondition;
import com.bandi.domain.User;
import com.bandi.domain.ViewColumnCondition;
import com.bandi.domain.ViewElementCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.impl.ClientServiceImpl;
import com.bandi.service.manage.impl.LicenseServiceImpl;
import com.bandi.service.manage.kaist.AccessElementService;
import com.bandi.service.manage.kaist.AuthorityService;
import com.bandi.service.manage.kaist.ClientService_KAIST;
import com.bandi.service.manage.kaist.ResourceService;
import com.bandi.service.manage.kaist.RoleAuthorityService;
import com.bandi.service.manage.kaist.UserService_KAIST;
import com.bandi.service.sso.kaist.ClientApiService;

public class ClientServiceImpl_KAIST extends ClientServiceImpl implements ClientService_KAIST{

    @Autowired
    ResourceService resourceService;

    @Autowired
    AuthorityService authorityService;

    @Autowired
    RoleAuthorityService roleAuthorityService;

    @Autowired
    AccessElementService accessElementService;

    @Resource(name = "initechClientApiService")
    ClientApiService initechClientApiService;

    @Autowired
    UserService_KAIST userService;

    @Override
    public int insert(Client client) {

        // KAIST - SSO타입이 WEB인 경우 REDIRECT URI가 존재
        if(client.getRedirectUri() == null || "".equals(client.getRedirectUri().trim())) {
            // KAIST - REDICRED URI 를 사용하지 않을경우, "N/A"로 설정 - not null  처리
            client.setRedirectUri( BandiConstants.NOT_AVAILABLE);
        }

        // KAIST - SSO타입이 WEB, SSO인 경우에만 URL이 존재
        if(client.getUrl() == null || "".equals(client.getUrl().trim())) {
            client.setUrl( BandiConstants.NOT_AVAILABLE);
        }

        if (client.getOid() == null || "".equals(client.getOid().trim())) {
            client.setOid( CommonUtil.generateOidWithoutSpecialCharacters());
        }

        Client clientFromDb = get( client.getOid() );
        if( clientFromDb != null ) {
            throw new BandiException( ErrorCode.CLIENT_ID_DUPLICATED);
        }

        /* SECRET는 이니텍에 클라이언트 생성 후 이니텍 클라이언트를 조회하여 우리쪽에 저장한다. handleAfterClientCreated() 참조.
        String secretTemp = client.getSecret();
        String encryptedSecret = cryptService.passwordEncrypt(client.getOid(), client.getSecret());
        client.setSecret(encryptedSecret);
        */

        ClientCondition condition = new ClientCondition();
        condition.createCriteria().andNameEqualTo(client.getName());

        List<Client> list = selectByCondition(condition);

        if (list != null && list.size() > 0) {
            throw new BandiException(ErrorCode.CLIENT_NAME_DUPLICATED);
        }

        // KAIST - 통합 sso일 경우 서비스 정렬번호 중복체크
        if (BandiConstants_KAIST.SSO_TYPE_INTEGRATED_SSO.equals(client.getSsoType())) {
            ClientCondition conditionServiceOrderNo = new ClientCondition();
            conditionServiceOrderNo.createCriteria().andServiceOrderNoEqualTo(client.getServiceOrderNo());

            list = selectByCondition(conditionServiceOrderNo);

            if (list != null && list.size() > 0) {
                throw new BandiException(MessageUtil.get("CLIENT_SERVICEORDERNO_DUPLICATED"));
            }
        }

        // License Check
        /* 카이스트는 라이센스 무제한
        if (checkLicenseType()) {
            client.setFlagLicense( BandiConstants.FLAG_Y);
        } else {
            client.setFlagLicense(BandiConstants.FLAG_N);
        }
        */

        if (client.getIp() != null) {

            String[] ips = client.getIp().split("\\s");
            addWhiteIpAfterDelete(client.getOid(), client.getName(), ips);
        }

        // 카이스트 사용하지 않음.
        //setHashValue(client);
        client.setHashValue( BandiConstants.NOT_AVAILABLE );

        // kaist에서는 우리쪽 im 기능을 사용하지 않음.
        /*
        if (LicenseServiceImpl.supportIm()){
            viewColumnService.initData( client.getOid() );
            ddlService.createView( client.getOid(), secretTemp);
            viewElementService.addInitData( client.getOid() );
            viewElementService.addCodeInitData( client.getOid() );
        }
        */

        client = setCreatorInfo( client );
        client.setInfoMarkOptn(BandiConstants_KAIST.INFO_MARK_OPTN_DEFAULT_KAISTUID);

        if( BandiProperties_KAIST.IS_TEST_WITHOUT_INITECH == false){
            // 이니텍 클라이언트 생성 및 조회
            initechClientApiService.createClient( client.getOid(), client.getName(), null );

            String secretKey = initechClientApiService.getClientSecrect( client.getOid(), null );

            client.setSecret(  secretKey );
        }

        int result = dao.insertSelective(client);

        handleAfterClientCreated( client );

        BandiCacheManager.put(BandiCacheManager.CACHE_CLIENT, client.getOid(), client);

        return result;
    }

    // bandiModified
    @Override
    public void update(Client client) {

    	if(!BandiConstants_KAIST.SSO_TYPE_WEB_SINGLE_AUTH.equals(client.getSsoType())) {
    		client.setRedirectUri( BandiConstants.NOT_AVAILABLE);
    	}

        ClientCondition condition = new ClientCondition();
        condition.createCriteria().andNameEqualTo(client.getName()).andOidNotEqualTo(client.getOid());

        List<Client> list = selectByCondition(condition);

        if (list != null && list.size() > 0) {
            throw new BandiException(ErrorCode.CLIENT_NAME_DUPLICATED);
        }

        if (client.getIp() != null) {
            String[] ips = client.getIp().split("\\s");

            // // 카이스트 : 모든 인증은 이니텍 SSO를 통해서 받아야 하므로, Y로 설정하여 사용
            addWhiteIpAfterDelete(client.getOid(), client.getName(), ips);
        }

        // License Check
        /* 카이스트는 라이센스 무제한
        if (checkLicenseType()) {
            client.setFlagLicense(BandiConstants.FLAG_Y);
        } else {
            client.setFlagLicense(BandiConstants.FLAG_N);
        }
        */

        // secret 재발급 처리
        /*if( client.getSecret() != null && client.getSecret().trim().length() > 0){
        } else {
            Client clientFromDb = get(client.getOid());
            client.setSecret( clientFromDb.getSecret() );
        }*/

        if( BandiProperties_KAIST.IS_TEST_WITHOUT_INITECH == false){
            Client clientFromDb = get(client.getOid());

            if( clientFromDb.getName().equals(  client.getName() ) == false){
                initechClientApiService.updateClient( client.getOid(), client.getName(), null );
            }
        }

        // 카이스트 사용하지 않음
        //setHashValue(client);

        client = setUpdatorInfo( client );

        int result = dao.updateByPrimaryKeySelective(client);

        handleAfterClientUpdate( client );

        // kaist 수정한 값이 캐시에 들어가도록 변경
        if (result > 0) {
            Client updatedClient = getFromDb(client.getOid());
            BandiCacheManager.put(BandiCacheManager.CACHE_CLIENT, client.getOid(), updatedClient);
        }
    }

    @Override
    public int delete(String oid) {
        int result = 0;
        result = dao.deleteByPrimaryKey(oid);

        if (result > 0) {

            deleteWhiteIp( oid);

            // 카이스트에서는 우리쪽 IM을 사용하지 않음
            if( false && LicenseServiceImpl.supportIm()){

                // clientoid를 기준으로 BDSViewElement, BDSViewColumn 삭제
                ViewColumnCondition columnCondition = new ViewColumnCondition();
                columnCondition.createCriteria().andClientOidEqualTo( oid);

                ViewElementCondition elementCondition = new ViewElementCondition();
                elementCondition.createCriteria().andClientOidEqualTo( oid );

                viewColumnService.deleteByCondition( columnCondition);
                viewElementService.deleteByCondition( elementCondition);

                ddlService.dropView( oid) ;
            }

            handleAfterClientDeleted( oid );

            BandiCacheManager.remove(BandiCacheManager.CACHE_CLIENT, oid);
        }

        return result;
    }

    @Override
    public Paginator search(Paginator paginator, Client client) {

        ClientCondition condition = new ClientCondition();
        String currentUserId = ContextUtil.getCurrentUserId();

        if (client != null) {
            String oid = client.getOid();
            if (oid != null && !"".equals(oid)) {
                condition.or().andOidEqualTo(client.getOid());
            }

            String name = client.getName();

            if (name != null && !"".equals(name)) {
                condition.or().andNameLike(client.getName() + "%");
            }

            String url = client.getUrl();

            if (url != null && !"".equals(url)) {
                condition.or().andUrlLike(client.getUrl() + "%");
            }

            String redirectUri = client.getRedirectUri();
            if (redirectUri != null && !"".equals(redirectUri)) {
                condition.or().andRedirectUriLike(client.getRedirectUri() + "%");
            }

            String ip = client.getIp();
            if (ip != null && !"".equals(ip)) {
                condition.or().andIpLike(client.getIp() + "%");
            }

            String flagValid = client.getFlagValid();
            if (flagValid!=null && !"".equals(flagValid)){
                condition.or().andFlagValidEqualTo( client.getFlagValid());
            }

            String flagUseSso = client.getFlagUseSso();
            if (flagUseSso != null && !"".equals(flagUseSso)) {
                condition.or().andFlagUseSsoEqualTo(client.getFlagUseSso());
            }

            String flagUseEam = client.getFlagUseEam();
            if (flagUseEam!=null && !"".equals(flagUseEam)){
                condition.or().andFlagUseEamEqualTo( client.getFlagUseEam());
            }
            String flagUseOtp = client.getFlagUseOtp();
            if (flagUseOtp!=null && !"".equals(flagUseOtp)){
                condition.or().andFlagUseOtpEqualTo( client.getFlagUseOtp());
            }

            String ssoType = client.getSsoType();
            if (ssoType!=null && !"".equals(ssoType)){
                condition.or().andSsoTypeLike( client.getSsoType()+"%");
            }

            String managerId = client.getManagerId();
            if (managerId!=null && !"".equals(managerId)){
                condition.or().andManagerIdEqualTo( client.getManagerId());
            }

            if(client.getFlagUseOtp().equals(BandiConstants.FLAG_Y)) {
            	User currentUser = userService.get(currentUserId);
                if (currentUser!=null && !"".equals(currentUserId)) {
                    String userType = currentUser.getUserType();
                    if (userType!=null && !"".equals(userType)){
                        condition.or().andUserTypeLike( "%"+userType+"%");
                    } else {
                    	throw new BandiException(ErrorCode_KAIST.USER_TYPE_IS_NULL);
                    }
                }
            }

            String eamGroup = client.getEamGroup();
            if (eamGroup!=null && !"".equals(eamGroup)){
                condition.or().andEamGroupEqualTo( client.getEamGroup());
            }
        }

        if (paginator.getSortBy() != null) {
            condition.setOrderByClause(paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("OID DESC");
        }

        if (currentUserId != null || !"".equals(currentUserId)) {
        	condition.setUserId(currentUserId);
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(), paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }


    protected void handleAfterClientCreated( Client client ) {
        if ( LicenseServiceImpl.supportEam() ){

            if( BandiConstants.FLAG_Y.equals( client.getFlagUseEam())){
                // kaist 권한 관리는 신규로 개발 예정
                // menuService.createInitMenu( client);

                resourceService.createClientRootResource( client.getOid(), client.getName() );
            }
        }
    }

    protected void handleAfterClientUpdate( Client client ) {

        if ( LicenseServiceImpl.supportEam() ){

            if( BandiConstants.FLAG_Y.equals( client.getFlagUseEam())){

                resourceService.updateRootResourceNameOrCreateRootResource( client.getOid(), client.getName());
            }
        }
    }

    protected void handleAfterClientDeleted( String oid ) {

        if( BandiProperties_KAIST.IS_TEST_WITHOUT_INITECH == false){
            // 이니텍 클라이언트 삭제
            initechClientApiService.deleteClient( oid, null );
        }

        // kaist 권한 관리는 신규로 개발 예정
        //permissionService.deleteByClientOid( oid );
        //roleService.deleteByClientOid( oid );
        //menuService.deleteByClientOid( oid );
        //aclService.deleteByClientOid( oid);

        roleAuthorityService.deleteByClientOid( oid );
        accessElementService.deleteByClientOid( oid );

        authorityService.deleteByClientOid( oid);
        resourceService.deleteByClientOid( oid );

    }

    @Override
    public Client authorizeClient(String clientId, String clientSecret) {
        // clientId 검증
        Client client = get(clientId);

        if (client == null) {
            throw new BandiException(ErrorCode.CLIENT_NOT_EXISTED);
        }

        if (clientSecret != null) {
            // KAIST : 이니텍에서 발급한 SECRETKEY를 그대로 이용함.
            //String secret = cryptService.passwordEncrypt(clientId, clientSecret);

            if (client.getSecret().equals( clientSecret) == false) {

                throw new BandiException(ErrorCode.CLIENT_SECRET_INCORRECT);
            }
        }

        return client;
    }

    // bandiModified
    @Override
    public String getParameter(String oid) {

        Client client = get( oid);

        String parameter = client.getInfoMarkOptn();

        return parameter;
    }

    // scheduler
    @Override
    public void setInitClient() {
        ClientCondition condition = new ClientCondition();
        condition.createCriteria().andSecretEqualTo(BandiConstants_KAIST.CLIENT_INIT_SECRET);

        List<Client> clients = selectByCondition(condition);

        String secretKey = "";

        for ( Client client : clients ) {
        	try {
            initechClientApiService.createClient( client.getOid(), client.getName(), null );

            secretKey = initechClientApiService.getClientSecrect( client.getOid(), null );
            client.setSecret(secretKey);

            dao.updateByPrimaryKeySelective(client);
        	} catch( Exception e) {
        		e.printStackTrace();
        	}
        }
    }

    @Override
    public Client getForForm(String oid) {

		if ("empty".equals(oid)) {

			Client client = new Client();

			client.setOid( CommonUtil.generateOidWithoutSpecialCharacters());

			client.setFlagUseSso( BandiConstants.FLAG_Y);

			return client;
		} else {


			Client client = BandiCacheManager.get(BandiCacheManager.CACHE_CLIENT, oid);

			if (client != null) {

				User userFromDB = userService.get(client.getManagerId());

				if( userFromDB != null){
                    client.setManager( userFromDB.getName() );
                }
				return client;
			}

			client = dao.selectByPrimaryKey(oid);

			if (client != null) {
				BandiCacheManager.put(BandiCacheManager.CACHE_CLIENT, client.getOid(), client);
			}

			return client;
		}
	}

    @Override
    public Client getFromDb(String oid) {
        Client client = dao.selectByPrimaryKey(oid);
        return client;
    }

	@Override
	public List<Client> getServicePortfolioForUserMain() {
		return dao.getServicePortfolioForUserMain();
	}
}
