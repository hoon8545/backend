package com.bandi.service.manage.kaist.impl;


import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bandi.dao.UserMapper;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.ExtUserMapper;
import com.bandi.domain.kaist.ExtUser;
import com.bandi.domain.kaist.ExtUserCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.ExtUserService;
import com.bandi.service.manage.kaist.UserService_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;

@Service
public class ExtUserServiceImpl implements ExtUserService {

    @Resource
    protected ExtUserMapper dao;

    @Resource
    protected UserMapper userDao;

    @Resource
    protected UserService_KAIST userService;


    public int insert(ExtUser extuser) {
        // PK에 해당하는 변수에 값을 채워주세요.
    	if( extuser.getOid() == null || "".equals( extuser.getOid().trim())){
    		extuser.setOid( com.bandi.common.IdGenerator.getUUID());
        }
    	if( extuser.getAvailableFlag() == null || "".equals(extuser.getAvailableFlag().trim())) {
    		extuser.setAvailableFlag("R");
    	}

    	extuser = setCreatorInfo( extuser );

		ExtUserCondition condition = new ExtUserCondition();

    	if ("R".equals(extuser.getExtReqType())) {
        	condition.createCriteria().andBirthdayEqualTo(extuser.getBirthday()).andNameEqualTo(extuser.getName()).
        		andExternEmailEqualTo(extuser.getExternEmail()).andExtReqTypeEqualTo("R").andMobileTelephoneNumberEqualTo(extuser.getMobileTelephoneNumber());

        	long cnt = dao.countByCondition(condition);

        	if(cnt!=0) {
        		throw new BandiException(ErrorCode_KAIST.EXTUSER_ALREAY_REGISTED_USER);
        	}

    	} else if("E".equals(extuser.getExtReqType()) && "R".equals(extuser.getAvailableFlag())) {
    		condition.createCriteria().andKaistUidEqualTo(extuser.getKaistUid()).andExtReqTypeEqualTo("R").andAvailableFlagEqualTo("A");

    		long cnt = dao.countByCondition(condition);

    		if(cnt!=1) {
    			throw new BandiException(ErrorCode_KAIST.EXTUSER_NON_REGISTED_USER);
    		}

    		List<ExtUser> recordList = dao.selectByCondition(condition);
    		ExtUser record = recordList.get(0);

    		condition.clear();
    		condition.createCriteria().andNameEqualTo(extuser.getName()).andExternEmailEqualTo(extuser.getExternEmail()).andExtReqTypeEqualTo("E").andAvailableFlagNotEqualTo("A");

    		long cnt2 = dao.countByCondition(condition);
    		if(cnt2!=0) {
    			throw new BandiException(ErrorCode_KAIST.EXTUSER_PROGRESS_REQUEST_EXIST);
    		}
    		extuser.setFirstName(record.getFirstName());
    		extuser.setLastName(record.getLastName());
    		extuser.setBirthday(record.getBirthday());
    		extuser.setSex(record.getSex());
    		extuser.setCountry(record.getCountry());
    		extuser.setExtCompany(record.getExtCompany());

    	} else if ("E".equals(extuser.getExtReqType()) && "A".equals(extuser.getAvailableFlag())) {
    		extuser = setUpdatorInfo(extuser);
    	}

        return dao.insertSelective(extuser);
    }

    public ExtUser get(String oid) {
        ExtUser extuser = null;
        extuser = dao.selectByPrimaryKey(oid);
        return extuser;
    }

    public void update(ExtUser extuser) {
    	extuser = setUpdatorInfo( extuser );

        dao.updateByPrimaryKeySelective(extuser);
    }

    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }

    public int delete(String oid) {
        return dao.deleteByPrimaryKey(oid);
    }

    public Paginator search(Paginator paginator, ExtUser extuser) {

        ExtUserCondition condition = new ExtUserCondition();

        if( extuser != null) {
            String name = extuser.getName();
            if (name!=null && !"".equals(name)){
                condition.or().andNameLike( extuser.getName()+"%");
            }
            String mobileTelephoneNumber = extuser.getMobileTelephoneNumber();
            if (mobileTelephoneNumber!=null && !"".equals(mobileTelephoneNumber)){
                condition.or().andMobileTelephoneNumberLike( extuser.getMobileTelephoneNumber()+"%");
            }
            String externEmail = extuser.getExternEmail();
            if (externEmail!=null && !"".equals(externEmail)){
                condition.or().andExternEmailLike( extuser.getExternEmail()+"%");
            }
            String extReqType = extuser.getExtReqType();
            if (extReqType!=null && !"".equals(extReqType)){
                condition.or().andExtReqTypeLike( extuser.getExtReqType()+"%");
            }
            String extCompany = extuser.getExtCompany();
            if (extCompany!=null && !"".equals(extCompany)){
                condition.or().andExtCompanyLike( extuser.getExtCompany()+"%");
            }
            String availableFlag = extuser.getAvailableFlag();
            if (availableFlag!=null && !"".equals(availableFlag)){
                condition.or().andAvailableFlagEqualTo( extuser.getAvailableFlag());
            }
            String updatorId = extuser.getUpdatorId();
            if (updatorId!=null && !"".equals(updatorId)){
                condition.or().andUpdatorIdEqualTo( extuser.getUpdatorId());
            }
            String creatorId = extuser.getCreatorId();
            if (creatorId!=null && !"".equals(creatorId)){
                condition.or().andCreatorIdEqualTo( extuser.getCreatorId());
            }
            if (extuser.getCreatedAtBetween() != null && extuser.getCreatedAtBetween().length == 2) {
				condition.or().andCreatedAtBetween(extuser.getCreatedAtBetween()[0],
						extuser.getCreatedAtBetween()[1]);
			} else {
				java.sql.Timestamp createdAt = extuser.getCreatedAt();
				if (createdAt != null) {
					condition.or().andCreatedAtEqualTo(extuser.getCreatedAt());
				}
			}
            if (extuser.getUpdatedAtBetween() != null && extuser.getUpdatedAtBetween().length == 2) {
				condition.or().andUpdatedAtBetween(extuser.getUpdatedAtBetween()[0],
						extuser.getUpdatedAtBetween()[1]);
			} else {
				java.sql.Timestamp updatedAt = extuser.getUpdatedAt();
				if (updatedAt != null) {
					condition.or().andUpdatedAtEqualTo(extuser.getUpdatedAt());
				}
			}
            if (extuser.getEndAtBetween() != null && extuser.getEndAtBetween().length == 2) {
				condition.or().andTempEndDatBetween(extuser.getEndAtBetween()[0],
						extuser.getEndAtBetween()[1]);
			} else {
				java.sql.Timestamp endAt = extuser.getTempEndDat();
				if (endAt != null) {
					condition.or().andTempEndDatEqualTo(extuser.getUpdatedAt());
				}
			}
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("ID asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    public long countByCondition( ExtUserCondition condition){
        return dao.countByCondition( condition);
    }

    public int deleteByCondition( ExtUserCondition condition){
        return dao.deleteByCondition( condition);
    }

    public List<ExtUser> selectByCondition( ExtUserCondition condition){
        return dao.selectByCondition( condition);
    }

    public int updateByConditionSelective( ExtUser record, ExtUserCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }


    protected ExtUser setCreatorInfo(ExtUser extuser){
    	extuser.setCreatorId( ContextUtil.getCurrentUserId() );
    	extuser.setCreatedAt( DateUtil.getNow() );
    	return extuser;
    }

    protected ExtUser setUpdatorInfo(ExtUser extuser){
    	extuser.setUpdatorId( ContextUtil.getCurrentUserId() );
    	extuser.setUpdatedAt( DateUtil.getNow() );
    	return extuser;
    }

    // ===================== End of Code Gen =====================

    public Paginator searchMgmt(Paginator paginator, ExtUser extuser) {

        ExtUserCondition condition = new ExtUserCondition();

        if( extuser != null) {
            String name = extuser.getName();
            if (name!=null && !"".equals(name)){
                condition.or().andMgmtNameLike( extuser.getName()+"%");
            }

            String kaistUid = extuser.getKaistUid();
            if (kaistUid!=null && !"".equals(kaistUid)){
                condition.or().andMgmtKaistUidLike( extuser.getKaistUid()+"%");
            }
            String extCompany = extuser.getExtCompany();
            if (extCompany!=null && !"".equals(extCompany)){
                condition.or().andMgmtExtCompanyLike( extuser.getExtCompany()+"%");
            }
            if (extuser.getEndAtBetween() != null && extuser.getEndAtBetween().length == 2) {
				condition.or().andTempEndDatBetween(extuser.getEndAtBetween()[0],
						extuser.getEndAtBetween()[1]);
			} else {
				java.sql.Timestamp tempEndDat = extuser.getTempEndDat();
				if (tempEndDat != null) {
					condition.or().andTempEndDatEqualTo(extuser.getTempEndDat());
				}
			}
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( "BDSEXTUSER."+paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("ID asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearchMgmt(condition));
        paginator.setList(dao.pagingQueryForSearchMgmt(paginatorex));

        return paginator;
    }

    public List<Map<String, String>> getCountryCodeList(){

    	List<Map<String, String>> result = new ArrayList<Map<String, String>>();
    	String locale = Locale.getDefault().toString();
    	if("ko_KR".equals(locale)) {
    		result = dao.getCountryCodeList("kor");
    	} else {
    		result = dao.getCountryCodeList(null);
    	}
    	return result;
    }

    public ExtUser getByKaistUid(String kaistUid) {
    	return dao.getByKaistUid(kaistUid);
    }

    public Timestamp addEndDate(Timestamp startDate, int duration) {
    	Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(startDate.getTime());
		cal.add(Calendar.MONTH, duration);
		Timestamp endDate = new Timestamp(cal.getTime().getTime());
		return endDate;
    }

    public List<ExtUser> getAdminMainExtuser() {
    	return dao.getAdminMainExtuser();
    }

    public String getCountryName(String countryCode) {
    	String result = "";

    	String locale = Locale.getDefault().toString();
    	List<Map<String,String>> countryNames = dao.getCountryFullName(countryCode);
    	if("ko_KR".equals(locale)) {
    		result = countryNames.get(0).get("COUNTRY_NM_KOR");
    	} else {
    		result = countryNames.get(0).get("COUNTRY_NM_ENG");
    	}
    	return result;
    }

}
