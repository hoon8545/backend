package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.SyncResource;
import com.bandi.domain.kaist.SyncResourceCondition;


public interface SyncResourceService {

    int insert(SyncResource syncresource);

    SyncResource get(String oid);

    void update(SyncResource syncresource);

    void deleteBatch(List list);

    int delete(String id);

    Paginator search(Paginator paginator,SyncResource syncresource);

    long countByCondition( SyncResourceCondition condition);

    int deleteByCondition( SyncResourceCondition condition);

    List<SyncResource> selectByCondition( SyncResourceCondition condition);

    int updateByConditionSelective( SyncResource record, SyncResourceCondition condition);

    // ===================== End of Code Gen =====================

}
