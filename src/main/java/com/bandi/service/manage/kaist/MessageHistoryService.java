package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.JasperReportParams;
import com.bandi.domain.kaist.MessageHistory;
import com.bandi.domain.kaist.MessageHistoryCondition;

import net.sf.jasperreports.engine.JasperPrint;


public interface MessageHistoryService {

    int insert(MessageHistory messagehistory);

    MessageHistory get(String oid);

    void update(MessageHistory messagehistory);

    void deleteBatch(List list);

    int delete(String id);

    Paginator search(Paginator paginator,MessageHistory messagehistory);

    long countByCondition( MessageHistoryCondition condition);

    int deleteByCondition( MessageHistoryCondition condition);

    List<MessageHistory> selectByCondition( MessageHistoryCondition condition);

    int updateByConditionSelective( MessageHistory record, MessageHistoryCondition condition);

    JasperPrint exportDataFileJasper(JasperReportParams jasperReportParams, MessageHistory messagehistory);
    // ===================== End of Code Gen =====================

}
