package com.bandi.service.manage.kaist.impl;

import java.util.*;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.OtpServiceMenuMapper;
import com.bandi.domain.kaist.OtpServiceMenu;
import com.bandi.domain.kaist.OtpServiceMenuCondition;
import com.bandi.service.manage.kaist.OtpServiceMenuService;
import com.bandi.common.util.CommonUtil;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;

import org.springframework.beans.factory.annotation.Autowired;

import java.sql.*;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;

import org.apache.ibatis.mapping.*;
import org.apache.ibatis.session.*;

import com.bandi.common.*;
import com.bandi.domain.JasperReportParams;
import com.bandi.exception.*;

import net.sf.jasperreports.engine.*;
@Service
public class OtpServiceMenuServiceImpl implements OtpServiceMenuService {

    @Resource
    protected OtpServiceMenuMapper dao;
    
    @Autowired
    protected DataSource dataSource;

    @Autowired
    protected SqlSessionFactory sqlSessionFactory;

    public int insert(OtpServiceMenu otpservicemenu) {
    	
    	if (otpservicemenu.getOid() == null || "".equals(otpservicemenu.getOid())) {
    		otpservicemenu.setOid(com.bandi.common.IdGenerator.getUUID());
    	}
    	
		otpservicemenu = setCreatorInfo( otpservicemenu );
		
        return dao.insertSelective(otpservicemenu);
    }

    public OtpServiceMenu get(String oid) {
        OtpServiceMenu otpservicemenu = null;
        otpservicemenu = dao.selectByPrimaryKey(oid);
        return otpservicemenu;
    }

    public void update(OtpServiceMenu otpservicemenu) {
    	otpservicemenu = setUpdatorInfo( otpservicemenu );
    	
        dao.updateByPrimaryKeySelective(otpservicemenu);
    }

    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }
    
    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }   

    public Paginator search(Paginator paginator, OtpServiceMenu otpservicemenu) {

        OtpServiceMenuCondition condition = new OtpServiceMenuCondition();

        if( otpservicemenu != null) {
            String clientId = otpservicemenu.getClientId();
            if (clientId!=null && !"".equals(clientId)){
                condition.or().andClientIdEqualTo( otpservicemenu.getClientId());
            }
            String menuId = otpservicemenu.getMenuId();
            if (menuId!=null && !"".equals(menuId)){
                condition.or().andMenuIdEqualTo( otpservicemenu.getMenuId());
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);
        
        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;       
    }

    public long countByCondition( OtpServiceMenuCondition condition){
        return dao.countByCondition( condition);
    }

    public int deleteByCondition( OtpServiceMenuCondition condition){
        return dao.deleteByCondition( condition);
    }

    public List<OtpServiceMenu> selectByCondition( OtpServiceMenuCondition condition){
        return dao.selectByCondition( condition);
    }

    public int updateByConditionSelective( OtpServiceMenu record, OtpServiceMenuCondition condition){
    	record = setUpdatorInfo( record );
    	
        return dao.updateByConditionSelective( record, condition);
    }
    
    public JasperPrint exportDataFileJasper(JasperReportParams jasperReportParams, OtpServiceMenu otpservicemenu) {
        int endPage;
        
        switch (jasperReportParams.getPageFlag()) {
        case "T":
            String totalPageStr = jasperReportParams.getTotalPageStr();
            int totalPage = 0;
            
            if(totalPageStr == null || totalPageStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                totalPage = Integer.parseInt(totalPageStr);
            }
            
            otpservicemenu.setPageNo(1);
            endPage = totalPage;
            break;
        case "R":
            String rangeStartStr = jasperReportParams.getRangeStartStr();
            String rangeEndStr = jasperReportParams.getRangeEndStr();
            
            int rangeStart = 0;
            int rangeEnd = 0;
            
            if(rangeStartStr == null || rangeStartStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                rangeStart = Integer.parseInt(rangeStartStr);
            }
            
            if(rangeEndStr == null || rangeEndStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                rangeEnd = Integer.parseInt(rangeEndStr);
            }
            
            otpservicemenu.setPageNo(rangeStart);
            endPage = rangeEnd;
            break;
        default:
            endPage = otpservicemenu.getPageNo();
            break;
        }
        
        OtpServiceMenuCondition condition = new OtpServiceMenuCondition();

        if( otpservicemenu != null) {
            String clientId = otpservicemenu.getClientId();
            if (clientId!=null && !"".equals(clientId)){
                condition.or().andClientIdEqualTo( otpservicemenu.getClientId());
            }
            String menuId = otpservicemenu.getMenuId();
            if (menuId!=null && !"".equals(menuId)){
                condition.or().andMenuIdEqualTo( otpservicemenu.getMenuId());
            }
        }
        
        String sortDirection = otpservicemenu.isSortDesc() ? "DESC" : "ASC";
        Paginator paginator = new Paginator(otpservicemenu.getPageNo(), endPage, otpservicemenu.getPageSize(), otpservicemenu.getSortBy(),
                sortDirection);

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(), paginator.getEndPage(), paginator.getRowsperpage());
        paginatorex.setCondition(condition);
        
        SqlSession sqlSession = sqlSessionFactory.openSession();
        MappedStatement ms = sqlSession.getConfiguration()
                .getMappedStatement("com.bandi.dao.kaist.OtpServiceMenuMapper.pagingQueryForSearch");
        BoundSql boundSql = ms.getBoundSql(paginatorex);

        List<ParameterMapping> boundParams = boundSql.getParameterMappings();
        String sql = boundSql.getSql();
        String paramString = "";
        boolean timestampFlag = true;
        for (ParameterMapping param : boundParams) {
            if (boundSql.getAdditionalParameter(param.getProperty()) instanceof Integer) {
                paramString = Integer.toString((int) boundSql.getAdditionalParameter(param.getProperty()));
            } else if (boundSql.getAdditionalParameter(param.getProperty()) instanceof Timestamp) {
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
                
                String databaseType = BandiProperties.BANDI_DATABASE_TYPE;
                String convertTimestamp = "";
                
                if("oracle".equals(databaseType)) {
                    convertTimestamp =  "TO_TIMESTAMP('"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            + "', 'mm/dd/yyyy hh24:mi:ss.ff3') "; 
                }else if("mariadb".equals(databaseType)) {
                    convertTimestamp = "STR_TO_DATE('"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            + "', '%m/%d/%Y %H:%i:%S.%x') ";
                }else if("mssql".equals(databaseType)) {
                    convertTimestamp = "CONVERT(CHAR(23),'"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            +"',21)";
                }
                
                if (timestampFlag) {
                    paramString = convertTimestamp;
                    timestampFlag = false;
                } else {
                    paramString = convertTimestamp;
                }

            } else {
                paramString = (String) boundSql.getAdditionalParameter(param.getProperty());
            }
            
            if (boundSql.getAdditionalParameter(param.getProperty()) instanceof String) {
                sql = sql.replaceFirst("\\?", "'" + paramString + "'");
            } else {
                sql = sql.replaceFirst("\\?", paramString);
            }
        }
        sql = sql.replaceAll("(\\s)", " ");
        
        Map<String, Object> params = new HashMap<>();
        params.put("pageName", "OtpServiceMenu");
        params.put("sql", sql);
        params.put("clientId", otpservicemenu.getClientId());
        
        try {
            JasperReport otpservicemenuReport = JasperCompileManager.compileReport(jasperReportParams.getJrxmlPath());

            Connection con = dataSource.getConnection();
        
            JasperPrint jasperPrint = JasperFillManager.fillReport(otpservicemenuReport, params, con);

            return jasperPrint;
        } catch (Exception e) {
            throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
        }
    }
    
    protected OtpServiceMenu setCreatorInfo(OtpServiceMenu otpservicemenu){
    	otpservicemenu.setCreatorId( ContextUtil.getCurrentUserId() );
    	otpservicemenu.setCreatedAt( DateUtil.getNow() );
    	return otpservicemenu;
    }
    
    protected OtpServiceMenu setUpdatorInfo(OtpServiceMenu otpservicemenu){
    	otpservicemenu.setUpdatorId( ContextUtil.getCurrentUserId() );
    	otpservicemenu.setUpdatedAt( DateUtil.getNow() );
    	return otpservicemenu;
    }
    
    // ===================== End of Code Gen =====================

}
