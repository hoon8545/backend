package com.bandi.service.manage.kaist;

import java.util.List;
import java.util.Map;

import com.bandi.domain.User;
import com.bandi.service.manage.UserService;

public interface UserService_KAIST extends UserService{

    Map<String, String> getUserParameters( String userId, String parameterKeys);

    boolean isNeedOTPCheck( String userId, String clientId );

    String getKaistUidByUserId(String userId);

    User getByKaistUid(String kaistUid);

    User getDetail(String id);

    User getDetailByKaistUid(String kaistUid);

    List<String> getKoreanNameByCodeDetailUid(List<String> codeUid);

    void resetInitechPassword( String userId, String plainNewPassword);

    void setUserInfo(User user);

    void insertSync(User user);

    void insertConcurrent(User user);

    void deleteConcurrent(String id);

    User getOriginalUserByKaistUid(String kaistUid);

    long getUserMenuType( String userId);

    boolean isClientManager( String userId );

    boolean isGroupSupervisor( String userId );

    boolean isExtUser( String userId );

}
