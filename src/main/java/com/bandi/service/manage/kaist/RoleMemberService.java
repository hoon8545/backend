package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.RoleMember;
import com.bandi.domain.kaist.RoleMemberCondition;


public interface RoleMemberService {

    int insert(RoleMember rolemember);

    RoleMember get(String oid);

    void update(RoleMember rolemember);

    void deleteBatch(List list);

    int delete(String id);

    Paginator search(Paginator paginator,RoleMember rolemember);

    long countByCondition( RoleMemberCondition condition);

    int deleteByCondition( RoleMemberCondition condition);

    List<RoleMember> selectByCondition( RoleMemberCondition condition);

    int updateByConditionSelective( RoleMember record, RoleMemberCondition condition);

    // ===================== End of Code Gen =====================

    void deleteByGroupId( String groupId);

    void deleteByUserId( String userId);

    void deleteByRoleMasterOid( String roleMasterOid );

    void deleteWhenUserMoved( String userId);

    List<String> selectParentGroupManagerIds( String parentGroupId);

    void addRoleMemeber(RoleMember rolemember );

    boolean isRoleMemberExisted( String roleMasterOid, String userId);

    boolean hasAlreadyAuthority( String userId, List< String> authorityOids );
}
