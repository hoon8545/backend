package com.bandi.service.manage.kaist.impl;

import com.bandi.domain.Configuration;
import com.bandi.domain.User;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.impl.AdminServiceImpl;
import com.bandi.common.BandiDbConfigs;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.domain.Admin;
import com.bandi.domain.AdminCondition;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class AdminServiceImpl_KAIST extends AdminServiceImpl{

    @Override
    public int insert(Admin admin) {

        if( admin.getUserId() == null || admin.getUserId().trim().length() == 0){
            throw new BandiException( ErrorCode_KAIST.ADMIN_USER_ID_IS_NULL);
        }

        // 사용자 ID로 등록된 admin이 존재하는지 검사
        AdminCondition condition = new AdminCondition();
        condition.createCriteria().andUserIdEqualTo( admin.getUserId() );

        long adminCountWithUserId = countByCondition( condition );

        if( adminCountWithUserId > 0){
            throw new BandiException( ErrorCode_KAIST.ADMIN_USER_ID_IS_DUPLICATED);
        }

        Admin adminFromDB = getFromDb(admin.getId());
        User userFromDB = userService.getFromDb(admin.getId());

        if(adminFromDB != null || userFromDB != null) {
            throw new BandiException( ErrorCode.ADMIN_ID_DUPLICATED);
        }

        Configuration configuration = configurationService.get("PASSWORD_SPECIAL_CHARACTERS");
        String specialChar = configuration.getConfigValue();
        int random = (int)(Math.random() * specialChar.length());

        String password = admin.getId() + cryptService.getRandomString(4);
        admin.setPassword(password);

        if( admin.getId() != null && admin.getPassword() != null ){
            admin.setPassword( cryptService.passwordEncrypt( admin.getId(), admin.getPassword() ));
        }

        if( "".equals( admin.getHandphone() ) ) {
            admin.setHandphone(null);
        }

        admin.setLoginFailedAt(new Timestamp(0));
        admin.setPasswordChangedAt(new Timestamp(0));

        setHashValue( admin);

        executeSendMailPasswordInit(admin.getEmail(), "", password, "");

        admin = setCreatorInfo( admin );

        return dao.insertSelective(admin);
    }

	@Override
    public Paginator search(Paginator paginator, Admin admin) {

		AdminCondition condition = new AdminCondition();

		if( admin != null){
			String id = admin.getId();
			if( id != null && ! "".equals( id ) ){
				condition.or().andIdEqualTo(admin.getId());
			}

			String name = admin.getName();
			if( name != null && ! "".equals( name ) ){
				condition.or().andNameLike( admin.getName() + "%" );
			}

			String handphone = admin.getHandphone();
			if( handphone != null && ! "".equals( handphone ) ){
				condition.or().andHandphoneLike( admin.getHandphone() + "%" );
			}

			String email = admin.getEmail();
			if( email != null && ! "".equals( email ) ){
				condition.or().andEmailLike( admin.getEmail() + "%" );
			}

			String role = admin.getRole();
			if (role!=null && !"".equals(role)){
				condition.or().andRoleLike( admin.getRole()+"%");
	        }

			String flagUse = admin.getFlagUse();
			if( flagUse != null && ! "".equals( flagUse ) ){
				condition.or().andFlagUseEqualTo( admin.getFlagUse());
			}

			int loginFailCount = admin.getLoginFailCount();
			int policyAuthorizationTimes = BandiDbConfigs.getInt(BandiDbConfigs.KEY_POLICY_USER_AUTHORIZATION_TIMES);
			if( loginFailCount == policyAuthorizationTimes ) {
				condition.or().andLoginFailCountEqualTo(policyAuthorizationTimes);
			}else if( loginFailCount == 0) {
				condition.or().andLoginFailCountLessThan(policyAuthorizationTimes);
			}else {
				//Nothing do
			}

	        java.sql.Timestamp loginFailedAt = admin.getLoginFailedAt();
			if (loginFailedAt!=null){
	            condition.or().andLoginFailedAtEqualTo(admin.getLoginFailedAt());
	        }

			java.sql.Timestamp passwordChangedAt = admin.getPasswordChangedAt();
			if (passwordChangedAt!=null){
	            condition.or().andPasswordChangedAtEqualTo(admin.getPasswordChangedAt());
	        }
			String flagValid = admin.getFlagValid();
			if (flagValid!=null && !"".equals(flagValid)){
				condition.or().andFlagValidEqualTo( admin.getFlagValid());
	        }

			int module = admin.getModule();
            if ( module != 0 ) {
                condition.or().andModuleEqualTo( module);
            }

            String userId = admin.getUserId();
            if (userId!=null && !"".equals(userId)){
                condition.or().andUserIdEqualTo( admin.getUserId());
            }
		}

		if( paginator.getSortBy() != null) {
			condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
		} else {
			condition.setOrderByClause("ID asc");
		}

		PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
		paginatorex.setCondition(condition);

		paginator.setTotal(dao.countForSearch(condition));
		paginator.setList(dao.pagingQueryForSearch(paginatorex));

		return paginator;
	}
}
