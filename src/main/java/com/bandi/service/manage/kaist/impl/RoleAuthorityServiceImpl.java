package com.bandi.service.manage.kaist.impl;

import java.util.List;

import javax.annotation.Resource;

import com.bandi.common.IdGenerator;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.RoleAuthorityMapper;
import com.bandi.domain.kaist.Authority;
import com.bandi.domain.kaist.RoleAuthority;
import com.bandi.domain.kaist.RoleAuthorityCondition;
import com.bandi.domain.kaist.RoleMaster;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.AuthorityService;
import com.bandi.service.manage.kaist.RoleAuthorityService;
import com.bandi.service.manage.kaist.RoleMasterService;

public class RoleAuthorityServiceImpl implements RoleAuthorityService {

    @Resource
    protected RoleAuthorityMapper dao;

    @Resource
    protected AuthorityService authorityService;

    @Resource
    protected RoleMasterService roleMasterService;

    @Override
    public int insert(RoleAuthority roleauthority) {

        if( roleauthority.getOid() == null || roleauthority.getOid().trim().length() == 0){
            roleauthority.setOid( IdGenerator.getUUID() );
        }

        if (roleauthority.getClientOid() == null || roleauthority.getClientOid().trim().length() == 0) {
            setClientOid(roleauthority);
        }

        checkExistRoleMaster(roleauthority);

        checkExistAuthority(roleauthority);

        int nResult = 0;
        if ( isRoleAuthorityExisted( roleauthority) == false){
            nResult = dao.insertSelective(roleauthority);
        }

        return nResult;
    }

    protected void checkExistAuthority(RoleAuthority roleauthority) {
        if (roleauthority.getAuthorityOid() == null) {
            throw new BandiException(ErrorCode_KAIST.TARGET_AUTHORITYOID_IS_NULL);
        }

        Authority authority = authorityService.get(roleauthority.getAuthorityOid());

        if (authority == null) {
            throw new BandiException(ErrorCode_KAIST.TARGET_AUTHORITY_NOT_FOUND);
        }

    }

    protected boolean isRoleAuthorityExisted( RoleAuthority roleauthority ){
        boolean isExisted = false;

        if( roleauthority != null && roleauthority.getRoleMasterOid() != null && roleauthority.getAuthorityOid() != null){
            isExisted = dao.isRoleAuthorityExisted( roleauthority.getRoleMasterOid(), roleauthority.getAuthorityOid() );
        }

        return isExisted;
    }

    protected void checkExistRoleMaster(RoleAuthority roleauthority) {
        if (roleauthority.getRoleMasterOid() == null) {
            throw new BandiException(ErrorCode_KAIST.TARGET_ROLEMASTEROID_IS_NULL);
        }

        RoleMaster rolemaster = roleMasterService.get(roleauthority.getRoleMasterOid());

        if (rolemaster == null) {
            throw new BandiException(ErrorCode_KAIST.TARGET_ROLE_NOT_FOUND);
        }
    }

    protected void setClientOid(RoleAuthority roleauthority) {
        if (roleauthority.getAuthorityOid() == null) {
            throw new BandiException(ErrorCode_KAIST.TARGET_AUTHORITYOID_IS_NULL);
        }

        Authority authority = authorityService.get(roleauthority.getAuthorityOid());

        if ( authority != null ) {
            roleauthority.setClientOid(authority.getClientOid());
        } else {
            throw new BandiException(ErrorCode_KAIST.TARGET_AUTHORITY_NOT_FOUND);
        }
    }

    @Override
    public RoleAuthority get(String oid) {
        RoleAuthority roleauthority = null;
        roleauthority = dao.selectByPrimaryKey(oid);
        return roleauthority;
    }

    @Override
    public void update(RoleAuthority roleauthority) {
    	roleauthority = setUpdatorInfo( roleauthority );

        dao.updateByPrimaryKeySelective(roleauthority);
    }

    @Override
    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }

    @Override
    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }

    @Override
    public Paginator search(Paginator paginator, RoleAuthority roleauthority) {

        RoleAuthorityCondition condition = new RoleAuthorityCondition();

        if( roleauthority != null) {
            String roleMasterOid = roleauthority.getRoleMasterOid();
            if (roleMasterOid!=null && !"".equals(roleMasterOid)){
                condition.or().andRoleMasterOidEqualTo( roleauthority.getRoleMasterOid());
            }
            String clientOid = roleauthority.getClientOid();
            if (clientOid!=null && !"".equals(clientOid)){
                condition.or().andClientOidEqualTo( roleauthority.getClientOid());
            }
            String authorityOid = roleauthority.getAuthorityOid();
            if (authorityOid!=null && !"".equals(authorityOid)){
                condition.or().andAuthorityOidEqualTo( roleauthority.getAuthorityOid());
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    @Override
    public long countByCondition( RoleAuthorityCondition condition){
        return dao.countByCondition( condition);
    }

    @Override
    public int deleteByCondition( RoleAuthorityCondition condition){
        return dao.deleteByCondition( condition);
    }

    @Override
    public List<RoleAuthority> selectByCondition( RoleAuthorityCondition condition){
        return dao.selectByCondition( condition);
    }

    @Override
    public int updateByConditionSelective( RoleAuthority record, RoleAuthorityCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }


    protected RoleAuthority setCreatorInfo(RoleAuthority roleauthority){
    	roleauthority.setCreatorId( ContextUtil.getCurrentUserId() );
    	roleauthority.setCreatedAt( DateUtil.getNow() );
    	return roleauthority;
    }

    protected RoleAuthority setUpdatorInfo(RoleAuthority roleauthority){
    	roleauthority.setUpdatorId( ContextUtil.getCurrentUserId() );
    	roleauthority.setUpdatedAt( DateUtil.getNow() );
    	return roleauthority;
    }

    // ===================== End of Code Gen =====================

    @Override
    public void deleteByClientOid( String clientOid ){

        RoleAuthorityCondition condition = new RoleAuthorityCondition();
        condition.createCriteria().andClientOidEqualTo( clientOid );

        deleteByCondition( condition );
    }

    @Override
    public void deleteByRoleMasterOid( String roleMasterOid ){

        RoleAuthorityCondition condition = new RoleAuthorityCondition();;
        condition.createCriteria().andRoleMasterOidEqualTo( roleMasterOid );

        deleteByCondition( condition );
    }

    @Override
    public void deleteByAuthorityOid( String authorityOid ){

        RoleAuthorityCondition condition = new RoleAuthorityCondition();;
        condition.createCriteria().andAuthorityOidEqualTo( authorityOid);

        deleteByCondition( condition );
    }

    @Override
    public void insertList(List<RoleAuthority> roleauthoritys) {

        for (RoleAuthority roleAuthority : roleauthoritys) {
            insert(roleAuthority);
        }
    }

}
