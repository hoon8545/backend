package com.bandi.service.manage.kaist.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bandi.common.IdGenerator;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.DelegateAuthorityMapper;
import com.bandi.domain.kaist.DelegateAuthority;
import com.bandi.domain.kaist.DelegateAuthorityCondition;
import com.bandi.service.manage.kaist.DelegateAuthorityService;

@Service
public class DelegateAuthorityServiceImpl implements DelegateAuthorityService {

    @Resource
    protected DelegateAuthorityMapper dao;


    @Override
    public int insert(DelegateAuthority delegateauthority) {

        if( delegateauthority.getOid() == null || delegateauthority.getOid().trim().length() == 0){
            delegateauthority.setOid( IdGenerator.getUUID() );
        }

		delegateauthority = setCreatorInfo( delegateauthority );

        return dao.insertSelective(delegateauthority);
    }

    @Override
    public DelegateAuthority get(String oid) {
        DelegateAuthority delegateauthority = null;
        delegateauthority = dao.selectByPrimaryKey(oid);
        return delegateauthority;
    }

    @Override
    public void update(DelegateAuthority delegateauthority) {
    	delegateauthority = setUpdatorInfo( delegateauthority );

        dao.updateByPrimaryKeySelective(delegateauthority);
    }

    @Override
    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }

    @Override
    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }

    @Override
    public Paginator search(Paginator paginator, DelegateAuthority delegateauthority) {

        DelegateAuthorityCondition condition = new DelegateAuthorityCondition();

        if( delegateauthority != null) {
            String delegateOid = delegateauthority.getDelegateOid();
            if (delegateOid!=null && !"".equals(delegateOid)){
                condition.or().andDelegateOidEqualTo( delegateauthority.getDelegateOid());
            }
            String authorityOid = delegateauthority.getAuthorityOid();
            if (authorityOid!=null && !"".equals(authorityOid)){
                condition.or().andAuthorityOidEqualTo( delegateauthority.getAuthorityOid());
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    @Override
    public long countByCondition( DelegateAuthorityCondition condition){
        return dao.countByCondition( condition);
    }

    @Override
    public int deleteByCondition( DelegateAuthorityCondition condition){
        return dao.deleteByCondition( condition);
    }

    @Override
    public List<DelegateAuthority> selectByCondition( DelegateAuthorityCondition condition){
        return dao.selectByCondition( condition);
    }

    @Override
    public int updateByConditionSelective( DelegateAuthority record, DelegateAuthorityCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }


    protected DelegateAuthority setCreatorInfo(DelegateAuthority delegateauthority){
    	delegateauthority.setCreatorId( ContextUtil.getCurrentUserId() );
    	delegateauthority.setCreatedAt( DateUtil.getNow() );
    	return delegateauthority;
    }

    protected DelegateAuthority setUpdatorInfo(DelegateAuthority delegateauthority){
    	delegateauthority.setUpdatorId( ContextUtil.getCurrentUserId() );
    	delegateauthority.setUpdatedAt( DateUtil.getNow() );
    	return delegateauthority;
    }

    // ===================== End of Code Gen =====================

}
