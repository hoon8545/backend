package com.bandi.service.manage.kaist.impl;

import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bandi.common.BandiProperties;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.UserLoginHistoryMapper;
import com.bandi.domain.JasperReportParams;
import com.bandi.domain.kaist.UserLoginHistory;
import com.bandi.domain.kaist.UserLoginHistoryCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.UserLoginHistoryService;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

@Service
public class UserLoginHistoryServiceImpl implements UserLoginHistoryService {

    @Resource
    protected UserLoginHistoryMapper dao;
    
    @Autowired
    protected DataSource dataSource;

    @Autowired
    protected SqlSessionFactory sqlSessionFactory;
    

    public int insert(UserLoginHistory userloginhistory) {
    	if (userloginhistory.getOid() == null || "".equals(userloginhistory.getOid().trim())) {
    		userloginhistory.setOid(com.bandi.common.IdGenerator.getUUID());
    		userloginhistory.setIamServerId(String.valueOf(com.bandi.common.IdGenerator.getServerId()));
		}
		userloginhistory = setCreatorInfo( userloginhistory );
		
        return dao.insertSelective(userloginhistory);
    }

    public UserLoginHistory get(String oid) {
        UserLoginHistory userloginhistory = null;
        userloginhistory = dao.selectByPrimaryKey(oid);
        return userloginhistory;
    }

    public void update(UserLoginHistory userloginhistory) {
    	userloginhistory = setUpdatorInfo( userloginhistory );
    	
        dao.updateByPrimaryKeySelective(userloginhistory);
    }

    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }
    
    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }   

    public Paginator search(Paginator paginator, UserLoginHistory userloginhistory) {

        UserLoginHistoryCondition condition = new UserLoginHistoryCondition();

        if( userloginhistory != null) {
            String userId = userloginhistory.getUserId();
            if (userId!=null && !"".equals(userId)){
                condition.or().andUserIdEqualTo( userloginhistory.getUserId());
            }
            if (userloginhistory.getLoginAtBetween() != null && userloginhistory.getLoginAtBetween().length == 2) {
				condition.or().andLoginAtBetween(userloginhistory.getLoginAtBetween()[0],
						userloginhistory.getLoginAtBetween()[1]);
			} else {
				java.sql.Timestamp loginAt = userloginhistory.getLoginAt();
				if (loginAt != null) {
					condition.or().andLoginAtEqualTo(userloginhistory.getLoginAt());
				}
			}
            String clientOid = userloginhistory.getClientOid();
            if (clientOid!=null && !"".equals(clientOid)){
                condition.or().andClientOidEqualTo( userloginhistory.getClientOid());
            }
            String flagSuccess = userloginhistory.getFlagSuccess();
            if (flagSuccess!=null && !"".equals(flagSuccess)){
                condition.or().andFlagSuccessEqualTo( userloginhistory.getFlagSuccess());
            }
            String errorMessage = userloginhistory.getErrorMessage();
            if (errorMessage!=null && !"".equals(errorMessage)){
                condition.or().andErrorMessageLike( userloginhistory.getErrorMessage()+"%");
            }
            String loginIp = userloginhistory.getLoginIp();
            if (loginIp!=null && !"".equals(loginIp)){
                condition.or().andLoginIpLike( userloginhistory.getLoginIp()+"%");
            }
            String iamServerId = userloginhistory.getIamServerId();
            if (iamServerId!=null && !"".equals(iamServerId)){
                condition.or().andIamServerIdEqualTo( userloginhistory.getIamServerId());
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }
        
        if (!"".equals(userloginhistory.getEmployeeNumber())) {
        	condition.setEmployeeNumber(userloginhistory.getEmployeeNumber());
        } 
        if (!"".equals(userloginhistory.getStudentNumber())) {
        	condition.setStudentNumber(userloginhistory.getStudentNumber());
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);
        
        if("".equals(userloginhistory.getEmployeeNumber()) && "".equals(userloginhistory.getStudentNumber())) {
        	paginator.setTotal(dao.countForSearch(condition));
            paginator.setList(dao.pagingQueryForSearch(paginatorex));
        } else if( "".equals(userloginhistory.getStudentNumber()) ){ // 사번으로검색
        	paginator.setTotal(dao.countForSearchByEmployeeNumber(userloginhistory));
        	paginator.setList(dao.pagingQueryForSearchByImployeeNumber(paginatorex));
        } else { // 학번으로 검색
        	paginator.setTotal(dao.countForSearchByStudentNumber(userloginhistory));
        	paginator.setList(dao.pagingQueryForSearchByStudentNumber(paginatorex));
        }

        return paginator;       
    }
    
    public JasperPrint exportDataFileJasper(JasperReportParams jasperReportParams, UserLoginHistory userloginhistory) {
        int endPage;
        
        switch (jasperReportParams.getPageFlag()) {
        case "T":
            String totalPageStr = jasperReportParams.getTotalPageStr();
            int totalPage = 0;
            
            if(totalPageStr == null || totalPageStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                totalPage = Integer.parseInt(totalPageStr);
            }
            
            userloginhistory.setPageNo(1);
            endPage = totalPage;
            break;
        case "R":
            String rangeStartStr = jasperReportParams.getRangeStartStr();
            String rangeEndStr = jasperReportParams.getRangeEndStr();
            
            int rangeStart = 0;
            int rangeEnd = 0;
            
            if(rangeStartStr == null || rangeStartStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                rangeStart = Integer.parseInt(rangeStartStr);
            }
            
            if(rangeEndStr == null || rangeEndStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                rangeEnd = Integer.parseInt(rangeEndStr);
            }
            
            userloginhistory.setPageNo(rangeStart);
            endPage = rangeEnd;
            break;
        default:
            endPage = userloginhistory.getPageNo();
            break;
        }
        
        UserLoginHistoryCondition condition = new UserLoginHistoryCondition();

        if( userloginhistory != null) {
            String userId = userloginhistory.getUserId();
            if (userId!=null && !"".equals(userId)){
                condition.or().andUserIdEqualTo( userloginhistory.getUserId());
            }
            if (userloginhistory.getLoginAtBetween() != null && userloginhistory.getLoginAtBetween().length == 2) {
				condition.or().andLoginAtBetween(userloginhistory.getLoginAtBetween()[0],
						userloginhistory.getLoginAtBetween()[1]);
			} else {
				java.sql.Timestamp loginAt = userloginhistory.getLoginAt();
				if (loginAt != null) {
					condition.or().andLoginAtEqualTo(userloginhistory.getLoginAt());
				}
			}
            String loginIp = userloginhistory.getLoginIp();
            if (loginIp!=null && !"".equals(loginIp)){
                condition.or().andLoginIpLike( userloginhistory.getLoginIp()+"%");
            }
        }
        
        String sortDirection = userloginhistory.isSortDesc() ? "DESC" : "ASC";
        Paginator paginator = new Paginator(userloginhistory.getPageNo(), endPage, userloginhistory.getPageSize(), userloginhistory.getSortBy(),
                sortDirection);

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(), paginator.getEndPage(), paginator.getRowsperpage());
        paginatorex.setCondition(condition);
        
        SqlSession sqlSession = sqlSessionFactory.openSession();
        MappedStatement ms = sqlSession.getConfiguration()
                .getMappedStatement("com.bandi.dao.kaist.UserLoginHistoryMapper.pagingQueryForSearch");
        BoundSql boundSql = ms.getBoundSql(paginatorex);

        List<ParameterMapping> boundParams = boundSql.getParameterMappings();
        String sql = boundSql.getSql();
        String paramString = "";
        boolean timestampFlag = true;
        for (ParameterMapping param : boundParams) {
            if (boundSql.getAdditionalParameter(param.getProperty()) instanceof Integer) {
                paramString = Integer.toString((int) boundSql.getAdditionalParameter(param.getProperty()));
            } else if (boundSql.getAdditionalParameter(param.getProperty()) instanceof Timestamp) {
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
                
                String databaseType = BandiProperties.BANDI_DATABASE_TYPE;
                String convertTimestamp = "";
                
                if("oracle".equals(databaseType)) {
                    convertTimestamp =  "TO_TIMESTAMP('"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            + "', 'mm/dd/yyyy hh24:mi:ss.ff3') "; 
                }else if("mariadb".equals(databaseType)) {
                    convertTimestamp = "STR_TO_DATE('"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            + "', '%m/%d/%Y %H:%i:%S.%x') ";
                }else if("mssql".equals(databaseType)) {
                    convertTimestamp = "CONVERT(CHAR(23),'"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            +"',21)";
                }
                
                if (timestampFlag) {
                    paramString = convertTimestamp;
                    timestampFlag = false;
                } else {
                    paramString = convertTimestamp;
                }

            } else {
                paramString = (String) boundSql.getAdditionalParameter(param.getProperty());
            }
            
            if (boundSql.getAdditionalParameter(param.getProperty()) instanceof String) {
                sql = sql.replaceFirst("\\?", "'" + paramString + "'");
            } else {
                sql = sql.replaceFirst("\\?", paramString);
            }
        }
        sql = sql.replaceAll("(\\s)", " ");
        
        Map<String, Object> params = new HashMap<>();
        params.put("pageName", "UserLoginHistory");
        params.put("sql", sql);
        params.put("loginAt", userloginhistory.getLoginAtBetween());
        params.put("loginIp", userloginhistory.getLoginIp());
        params.put("userId", userloginhistory.getUserId());
        
        try {
            JasperReport userloginhistoryReport = JasperCompileManager.compileReport(jasperReportParams.getJrxmlPath());

            Connection con = dataSource.getConnection();
        
            JasperPrint jasperPrint = JasperFillManager.fillReport(userloginhistoryReport, params, con);

            return jasperPrint;
        } catch (Exception e) {
            throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
        }
    }

    public long countByCondition( UserLoginHistoryCondition condition){
        return dao.countByCondition( condition);
    }

    public int deleteByCondition( UserLoginHistoryCondition condition){
        return dao.deleteByCondition( condition);
    }

    public List<UserLoginHistory> selectByCondition( UserLoginHistoryCondition condition){
        return dao.selectByCondition( condition);
    }

    public int updateByConditionSelective( UserLoginHistory record, UserLoginHistoryCondition condition){
    	record = setUpdatorInfo( record );
    	
        return dao.updateByConditionSelective( record, condition);
    }
    
    
    protected UserLoginHistory setCreatorInfo(UserLoginHistory userloginhistory){
    	userloginhistory.setCreatorId( ContextUtil.getCurrentUserId() );
    	userloginhistory.setCreatedAt( DateUtil.getNow() );
    	return userloginhistory;
    }
    
    protected UserLoginHistory setUpdatorInfo(UserLoginHistory userloginhistory){
    	userloginhistory.setUpdatorId( ContextUtil.getCurrentUserId() );
    	userloginhistory.setUpdatedAt( DateUtil.getNow() );
    	return userloginhistory;
    }
    
    // ===================== End of Code Gen =====================

}
