package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.Terms;
import com.bandi.domain.kaist.TermsCondition;
import com.bandi.domain.JasperReportParams;

import net.sf.jasperreports.engine.JasperPrint;


public interface TermsService {

    int insert(Terms terms);

    Terms get(String oid);
    
    void update(Terms terms);
    
    void deleteBatch(List list);
    
    int delete(String id);
    
    Paginator search(Paginator paginator,Terms terms);

    long countByCondition( TermsCondition condition);

    int deleteByCondition( TermsCondition condition);

    List<Terms> selectByCondition( TermsCondition condition);
    
    int updateByConditionSelective( Terms record, TermsCondition condition);

    JasperPrint exportDataFileJasper(JasperReportParams jasperReportParams, Terms terms);
    // ===================== End of Code Gen =====================

}
