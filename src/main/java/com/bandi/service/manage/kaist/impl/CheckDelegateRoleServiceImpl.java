package com.bandi.service.manage.kaist.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.util.DateUtil_KAIST;
import com.bandi.domain.kaist.Authority;
import com.bandi.domain.kaist.DelegateAuthority;
import com.bandi.domain.kaist.DelegateAuthorityCondition;
import com.bandi.domain.kaist.DelegateHistory;
import com.bandi.domain.kaist.DelegateHistoryCondition;
import com.bandi.domain.kaist.DelegateRoleDatas;
import com.bandi.domain.kaist.RoleAuthority;
import com.bandi.domain.kaist.RoleMaster;
import com.bandi.domain.kaist.RoleMember;
import com.bandi.service.manage.kaist.AuthorityService;
import com.bandi.service.manage.kaist.CheckDelegateRoleService;
import com.bandi.service.manage.kaist.DelegateAuthorityService;
import com.bandi.service.manage.kaist.DelegateHistoryService;
import com.bandi.service.manage.kaist.RoleAuthorityService;
import com.bandi.service.manage.kaist.RoleMasterService;
import com.bandi.service.manage.kaist.RoleMemberService;

public class CheckDelegateRoleServiceImpl implements CheckDelegateRoleService {

    @Resource
    protected DelegateHistoryService delegateHistoryService;

    @Resource
    protected RoleMasterService roleMasterService;

    @Resource
    protected RoleMemberService roleMemberService;

    @Resource
    protected RoleAuthorityService roleAuthorityService;

    @Resource
    protected AuthorityService authorityService;

    @Resource
    protected DelegateAuthorityService delegateAuthorityService;

    protected static final Logger logger = LoggerFactory.getLogger("syncOrg");

    @Override
    public void excute() {
        try {
            logger.debug("[START]DelegateRole");

            // 생성될 임시롤 데이터 수집
            DelegateRoleDatas delegateRoleDatas = setCreateDelegateRoleData();

            // 임시롤 생성
            createDelegateRole(delegateRoleDatas);

            // 삭제될 임시롤 데이터 수집
            delegateRoleDatas = setDeleteDelegateRoleData(delegateRoleDatas);

            // 임시롤 삭제
            deleteDelegateRole(delegateRoleDatas);

        } catch (Throwable t) {
            t.printStackTrace();
            logger.error("[FAIL]DelegateRole", t.getMessage());
        } finally {
            logger.debug("[END]DelegateRole");
        }

    }

    protected DelegateRoleDatas setCreateDelegateRoleData() {

        DelegateRoleDatas delegateRoleDatas = new DelegateRoleDatas();

        DelegateHistoryCondition condition = new DelegateHistoryCondition();
        condition.createCriteria().andStartDelegateAtLessThanOrEqualTo(DateUtil_KAIST.getCurrectYMD())
        .andEndDelegateAtGreaterThanOrEqualTo(DateUtil_KAIST.getCurrectYMD())
        .andStatusEqualTo(BandiConstants_KAIST.DELEGATE_ROLE_FLAG_WAIT);

        delegateRoleDatas.setDelegateRoles(delegateHistoryService.selectByCondition(condition));

        for (DelegateHistory delegateHistory : delegateRoleDatas.getDelegateRoles()) {
            String delegateOid = delegateHistory.getOid();

            DelegateAuthorityCondition delegateAuthorityCondition = new DelegateAuthorityCondition();
            delegateAuthorityCondition.createCriteria().andDelegateOidEqualTo(delegateOid);

            List<DelegateAuthority> delegateauthoritys = delegateAuthorityService.selectByCondition(delegateAuthorityCondition);

            delegateRoleDatas.getDelegateAuthoritys().put(delegateOid, delegateauthoritys);
        }

        return delegateRoleDatas;

    }

    protected DelegateRoleDatas setDeleteDelegateRoleData(DelegateRoleDatas delegateRoleDatas) {
        DelegateHistoryCondition condition = new DelegateHistoryCondition();
        condition.createCriteria().andEndDelegateAtLessThan(DateUtil_KAIST.getCurrectYMD())
        .andStatusEqualTo(BandiConstants_KAIST.DELEGATE_ROLE_FLAG_USE);

        List<DelegateHistory> delegateHistorys = delegateHistoryService.selectByCondition(condition);

        for (DelegateHistory delegateHistory : delegateHistorys) {
            String deleteDelegateOid = delegateHistory.getOid();

            delegateRoleDatas.getDeleteDelegateRoles().add(deleteDelegateOid);
        }

        return delegateRoleDatas;
    }

    /*
     * 위임한 롤을 BDSROLEMASTER에 임시로 생성할 때
     * BDSDELEGATEHISTORY(위임 테이블) -> OID 컬럼 값이
     * 임시 롤로 생성 될 BDSROLEMASTER(롤 테이블) -> OID 값으로 들어간다.
     *
     * ex) BDSDELEGATEHISTORY 의  OID 값이 delegate001이었다면
     * BDSROLEMASTER가 생성될 때 OID 값이 delegate001 값으로 생성됩니다.
     */
    protected void createDelegateRole(DelegateRoleDatas delegateRoleDatas) {

        for (DelegateHistory delegateHistory :delegateRoleDatas.getDelegateRoles() ) {

            String delegateOid = delegateHistory.getOid();

            RoleMaster targetRoleMaster = roleMasterService.get(delegateHistory.getRoleMasterOid());

            RoleMaster roleMasterFromDB = roleMasterService.get(delegateHistory.getOid());

            if (roleMasterFromDB == null ) {
                // 임시롤 생성
                roleMasterService.insert(convertToDelegateRoleMaster(targetRoleMaster, delegateOid));

                // 임시롤 멤버 생성
                roleMemberService.insert(convertToDelegateRoleMember(delegateOid, delegateHistory.getToUserId()));

                // 사용유무 변경.
                delegateHistory.setStatus(BandiConstants_KAIST.DELEGATE_ROLE_FLAG_USE);
                delegateHistoryService.update(delegateHistory);
            }


            List<DelegateAuthority> delegateauthoritys= delegateRoleDatas.getDelegateAuthoritys().get(delegateOid);

            for (DelegateAuthority delegateauthority : delegateauthoritys) {
                // 임시롤-권한 맵핑
                Authority authority = authorityService.get(delegateauthority.getAuthorityOid());
                roleAuthorityService.insert(convertToDelegateRoleAuthority(delegateOid, authority));
            }

        }

    }

    protected void deleteDelegateRole(DelegateRoleDatas delegateRoleDatas) {
        for (String delegateOid : delegateRoleDatas.getDeleteDelegateRoles()) {
            // 임시롤, 임시롤 멤버, 임시롤-권한 맵핑 삭제
            delegateHistoryService.deleteDelegateRole(delegateOid);
        }
    }

    protected RoleMaster convertToDelegateRoleMaster (RoleMaster targetRoleMaster, String delegateOid) {
        RoleMaster rolemaster = new RoleMaster();

        rolemaster.setOid(delegateOid);
        rolemaster.setGroupId(targetRoleMaster.getGroupId());
        rolemaster.setName(targetRoleMaster.getName() + "_" + BandiConstants_KAIST.DELEGATE_ROLE_NAME);
        rolemaster.setRoleType(targetRoleMaster.getRoleType());
        rolemaster.setFlagDelegated(BandiConstants_KAIST.DELEGATE_ROLE_FLAG_USE);

        return rolemaster;
    }

    protected RoleMember convertToDelegateRoleMember (String delegateOid, String toUserId) {
        RoleMember rolemember = new RoleMember();

        rolemember.setRoleMasterOid(delegateOid);
        rolemember.setTargetObjectId(toUserId);
        rolemember.setTargetObjectType(BandiConstants_KAIST.OBJECT_TYPE_USER);

        return rolemember;
    }

    protected RoleAuthority convertToDelegateRoleAuthority (String delegateOid, Authority authority) {
        RoleAuthority roleAuthority = new RoleAuthority();

        roleAuthority.setRoleMasterOid(delegateOid);
        roleAuthority.setClientOid(authority.getClientOid());
        roleAuthority.setAuthorityOid(authority.getOid());

        return roleAuthority;
    }


}
