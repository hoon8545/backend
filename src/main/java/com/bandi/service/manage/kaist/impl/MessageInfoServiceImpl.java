package com.bandi.service.manage.kaist.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;

import com.bandi.common.BandiConstants;
import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.MessageInfoMapper;
import com.bandi.dao.kaist.SmsMapper;
import com.bandi.domain.kaist.MessageHistory;
import com.bandi.domain.kaist.MessageInfo;
import com.bandi.domain.kaist.MessageInfoCondition;
import com.bandi.domain.kaist.Sms;
import com.bandi.service.manage.kaist.MessageHistoryService;
import com.bandi.service.manage.kaist.MessageInfoService;

import net.sf.json.JSONObject;

@Service
public class MessageInfoServiceImpl implements MessageInfoService {

    @Resource
    protected MessageInfoMapper dao;

    @Resource
    protected SmsMapper smsDao;

    @Resource
    protected MessageHistoryService messageHistoryService;

    @Override
    public int insert(MessageInfo messageinfo) {

        messageinfo = setCreatorInfo(messageinfo);

        return dao.insertSelective(messageinfo);
    }

    @Override
    public MessageInfo get(String managecode) {
        MessageInfo messageinfo = null;
        messageinfo = dao.selectByPrimaryKey(managecode);
        return messageinfo;
    }

    @Override
    public void update(MessageInfo messageinfo) {
        messageinfo = setUpdatorInfo(messageinfo);

        dao.updateByPrimaryKeySelective(messageinfo);
    }

    @Override
    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }

    @Override
    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }

    @Override
    public Paginator search(Paginator paginator, MessageInfo messageinfo) {

        MessageInfoCondition condition = new MessageInfoCondition();

        if (messageinfo != null) {
        }

        if (paginator.getSortBy() != null) {
            condition.setOrderByClause(paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("manageCode asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(), paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    @Override
    public long countByCondition(MessageInfoCondition condition) {
        return dao.countByCondition(condition);
    }

    @Override
    public int deleteByCondition(MessageInfoCondition condition) {
        return dao.deleteByCondition(condition);
    }

    @Override
    public List<MessageInfo> selectByCondition(MessageInfoCondition condition) {
        return dao.selectByCondition(condition);
    }

    @Override
    public int updateByConditionSelective(MessageInfo record, MessageInfoCondition condition) {
        record = setUpdatorInfo(record);

        return dao.updateByConditionSelective(record, condition);
    }

    @Override
    public String getManageCode() {
        return dao.getManageCode();
    }

    protected MessageInfo setCreatorInfo(MessageInfo messageinfo) {
        messageinfo.setCreatorId(ContextUtil.getCurrentUserId());
        messageinfo.setCreatedAt(DateUtil.getNow());
        return messageinfo;
    }

    protected MessageInfo setUpdatorInfo(MessageInfo messageinfo) {
        messageinfo.setUpdatorId(ContextUtil.getCurrentUserId());
        messageinfo.setUpdatedAt(DateUtil.getNow());
        return messageinfo;
    }

    // ===================== End of Code Gen =====================

    @Override
    public JSONObject messageSend(JSONObject json) {

        boolean checkMailSend = false;
        boolean checkSmsSend = false;

        MessageHistory messageHistory = new MessageHistory();

        if (json.containsKey("admin")) {
            messageHistory.setSendUid(json.getString("admin"));
        } else if(!json.containsKey("extUser")){
            messageHistory.setSendUid(json.getString("kaistUid"));
        }
        
        messageHistory.setSendCode(json.getString("manageCode"));
        
        if((json.containsKey("extUser"))) {
        	messageHistory.setFlagExtUser(BandiConstants_KAIST.FLAG_Y);
        	messageHistory.setSendUid(BandiConstants_KAIST.MESSAGE_USERTYPE_EXTUSER);
        } else {
        	messageHistory.setFlagExtUser(BandiConstants.FLAG_N);
        }

        // messageCode를 통해 Message 정보를 가져옴
        MessageInfo messageInfo = new MessageInfo();
        messageInfo = dao.selectByPrimaryKey(json.getString("manageCode"));

        String eventTitle = messageInfo.getEventTitle();
        messageHistory.setEventTitle(eventTitle);

        if (BandiConstants_KAIST.MESSAGEINFO_TYPE_EMAIL.equals(json.get(BandiConstants_KAIST.MESSAGE_FLAG_STATIC))) {
            messageInfo.setFlagEmail(BandiConstants.FLAG_Y);
            messageInfo.setFlagsms(BandiConstants.FLAG_N);
        }
        if (BandiConstants_KAIST.MESSAGEINFO_TYPE_SMS.equals(json.get(BandiConstants_KAIST.MESSAGE_FLAG_STATIC))) {
            messageInfo.setFlagEmail(BandiConstants.FLAG_N);
            messageInfo.setFlagsms(BandiConstants.FLAG_Y);
        }

        // SMS 보낼때 사용
        if (BandiConstants.FLAG_Y.equals(messageInfo.getFlagsms())) {
            Sms sms = new Sms();
            sms.setTran_msg(messageInfo.getSmsBody());
            sms.setTran_callback(BandiProperties_KAIST.CALLBACK_PHONE_NUMBER);
            sms.setTran_phone(json.getString(BandiConstants_KAIST.MESSAGE_PHONE_NUMBER));
            sms.setTran_msg(sms.getTran_msg().replaceAll(BandiConstants_KAIST.MESSAGE_SEND_VALUE, json.get(BandiConstants_KAIST.MESSAGE_SEND_VALUE).toString()));
            messageHistory.setSendTo(json.getString(BandiConstants_KAIST.MESSAGE_PHONE_NUMBER));
            smsDao.insert(sms);
            json.put(BandiConstants_KAIST.MESSAGEINFO_TYPE_SMS, true);
            checkSmsSend = true;  
        }

        // Email 보낼때 사용
        if (BandiConstants.FLAG_Y.equals(messageInfo.getFlagEmail())) {
            Iterator<String> jsonkey = json.keySet().iterator();
            JSONObject sendJson = new JSONObject();
            while (jsonkey.hasNext()) {
                String key = jsonkey.next();
                if (key.length() >= 9) {
                    String checkkey = (String) key.subSequence(0, 9);
                    if (BandiConstants_KAIST.MESSAGE_SEND_VALUE.equals(checkkey)) {
                        sendJson.put(key, json.get(key));
                    }
                }
            }

            String sendTo = "";
            if (json.containsKey(BandiConstants_KAIST.MESSAGE_EMAIL)) {
                sendTo = json.getString(BandiConstants_KAIST.MESSAGE_EMAIL);
                messageHistory.setSendTo(sendTo);
            }

            if (!json.containsKey(BandiConstants_KAIST.MESSAGE_USERTYPE_EXTUSER)) {
                if (json.containsKey(BandiConstants_KAIST.MESSAGE_USER_NAME)) {
                    String sendName = json.getString(BandiConstants_KAIST.MESSAGE_USER_NAME);
                    if (!"".equals(sendName))
                        messageHistory.setSendName(sendName);
                }
            }

            // checkMailSend = mailSend(messageInfo, sendJson, sendTo);
            if (mailSend(messageInfo, sendJson, sendTo)) {
            	json.put("EmailResult", true);
            	checkMailSend = true;
            }else {
            	json.put("EmailResult", false);
            }
        }

        if (BandiConstants.FLAG_Y.equals(messageInfo.getFlagEmail()) && BandiConstants.FLAG_Y.equals(messageInfo.getFlagsms())) {
            messageHistory.setSendType(BandiConstants_KAIST.MESSAGEINFO_TYPE_EMAIL_SMS);
        } else if (BandiConstants.FLAG_Y.equals(messageInfo.getFlagEmail())) {
            
            if(json.containsKey("EmailResult")){
				if(json.getBoolean("EmailResult")){
					json.put("result", "SUCCESS");
				}else{
					json.put("result", "FAIL");
				}
			}
            
            messageHistory.setSendType(BandiConstants_KAIST.MESSAGEINFO_TYPE_EMAIL);
        } else if (BandiConstants.FLAG_Y.equals(messageInfo.getFlagsms())) {
            messageHistory.setSendType(BandiConstants_KAIST.MESSAGEINFO_TYPE_SMS);
        }

        if (checkMailSend == true || checkSmsSend == true) {
            messageHistoryService.insert(messageHistory);
            return json;
        }
        
        return json;
    }

    public boolean mailSend(MessageInfo messageInfo, JSONObject sendJson, String sendTo) {
        boolean flag = false;

        String mailSubject = messageInfo.getEmailTitle();
        String mailBody = messageInfo.getEmailBody();
        Document doc = null;

        if (sendJson != null) {
            try {
                doc = Jsoup.parse(mailBody);
                Element span = null;
                String key = null;

                Iterator<String> itr = sendJson.keySet().iterator();

                while (itr.hasNext()) {
                    key = itr.next();
                    span = doc.select("span#" + key).first(); // <span>One</span>
                    if (null == span) {
                        continue;
                    }
                    span.prepend((String) sendJson.get(key));
                }

                System.out.println("DOC HTML :" + doc.html());
                mailBody = doc.html();
            } catch (Exception e) {

            }

        }

        String mode = BandiProperties_KAIST.PROPERTIES_MODE;

        Session session = getSession(mode);

        try {
            // Instantiatee a message
            MimeMessage message = new MimeMessage(session);

            message.setHeader("Content-Transfer-Encoding", "quoted-printable");

            InternetAddress fadd = new InternetAddress();

            fadd.setAddress(BandiProperties_KAIST.MAIL_SENDER_EMAIL);
            fadd.setPersonal("IAMPS Admin", "EUC-KR");

            message.setFrom(fadd);

            InternetAddress toAddr = new InternetAddress();
            toAddr.setAddress(sendTo);

            message.addRecipient(Message.RecipientType.TO, toAddr);

            // ccmode Y 이면 ccuser에거 참조로 메일을 보낸다.
            if (BandiConstants.FLAG_Y.equals(BandiProperties_KAIST.MAIL_SMTP_CCMODE)) {
                InternetAddress ccAddr = new InternetAddress();
                ccAddr.setAddress(BandiProperties_KAIST.MAIL_SMTP_CCADDRESS);
                message.addRecipient(Message.RecipientType.CC, ccAddr);
            }
            message.setSubject(mailSubject);
            message.setContent(mailBody, "text/html;charset=EUC-KR");

            Transport.send(message);

            flag = true;

        } catch (MessagingException mex) {
            mex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return flag;
    }

    @Override
    public Session getSession(String mode) {
        Session session = null;

        Properties props = new Properties();

        if (BandiConstants_KAIST.MAIL_MODE_LOCAL.equals(mode)) {
            props.put("mail.smtp.auth", BandiConstants_KAIST.FLAG_TRUE);
            props.put("mail.smtp.starttls.enable", BandiConstants_KAIST.FLAG_TRUE);
            props.put("mail.smtp.host", BandiProperties_KAIST.MAIL_SMTP_HOST);
            props.put("mail.smtp.port", BandiProperties_KAIST.MAIL_SMTP_PORT);

            session = Session.getInstance(props, new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication( BandiProperties_KAIST.MAIL_SUPPORT_USERNAME,BandiProperties_KAIST.MAIL_SUPPORT_PASSWORD);
                }
            });
        } else {

            // need to specify which host to send it to
            props.put("mail.smtp.host", BandiProperties_KAIST.MAIL_SMTP_HOST);
            props.put("mail.mime.charset", "euc-kr");

            // To see what is going on behind the scene
            props.put("mail.debug", BandiConstants_KAIST.FLAG_TRUE);
            session = Session.getInstance(props);
        }

        return session;
    }

}
