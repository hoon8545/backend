package com.bandi.service.manage.kaist.impl;

import com.bandi.common.BandiConstants;
import com.bandi.common.BandiDbConfigs;
import com.bandi.domain.WhiteIp;
import com.bandi.service.manage.impl.WhiteIpServiceImpl;
import com.bandi.web.filter.ipfilter.Config;

import java.util.ArrayList;
import java.util.List;

public class WhiteIpServiceImpl_KAIST extends WhiteIpServiceImpl{

    private static Config config  = new Config();

    @Override
    public void refreshIpFilter(){

        boolean allowFirst = BandiConstants.FLAG_Y.equals( BandiDbConfigs.get( BandiDbConfigs.KEY_ACCESS_IP_ALLOW_POLICY));

        // KAIST : 기본 설정은 접근 허용
        config.setDefaultAllow(true);

        config.setAllowFirst( allowFirst);

        List< WhiteIp > whiteIpList = getAll();

        config.getAllowList().clear();
        config.getDenyList().clear();

        List<String> alwaysAllowIp = new ArrayList<>();

        for ( WhiteIp ip : whiteIpList ){
            if( BandiConstants.FLAG_N.equals(ip.getFlagEnableDelete())){
                alwaysAllowIp.add(ip.getIp().trim());
            }
        }

        for ( WhiteIp ip : whiteIpList ){

            if(BandiConstants.FLAG_Y.equals( ip.getFlagAllow())) {
                config.allow(ip.getIp().trim());
            } else {
                if( BandiConstants.FLAG_N.equals(ip.getFlagEnableDelete())){
                    config.allow(ip.getIp().trim());
                } else {

                    // 관리자 사용 IP 및 client 사용 IP 들은 항상 허용 처리.
                    if( alwaysAllowIp.contains( ip.getIp().trim() ) == false){
                        config.deny( ip.getIp().trim());
                    }
                }
            }
        }

        IP_FILTER.loadConfig( config);
    }
}
