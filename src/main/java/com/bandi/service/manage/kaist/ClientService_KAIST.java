package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.domain.Client;
import com.bandi.service.manage.ClientService;

public interface ClientService_KAIST extends ClientService{

    Client authorizeClient( String clientId, String clientSecret);

    String getParameter( String oid);

    void setInitClient();

    Client getFromDb(String oid);

    List<Client> getServicePortfolioForUserMain();

}
