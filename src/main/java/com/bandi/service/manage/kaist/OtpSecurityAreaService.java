package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.OtpSecurityArea;
import com.bandi.domain.kaist.OtpSecurityAreaCondition;


public interface OtpSecurityAreaService {

    int insert(OtpSecurityArea otpsecurityarea);

    OtpSecurityArea get(String oid);

    void update(OtpSecurityArea otpsecurityarea);

    void deleteBatch(List list);

    int delete(String id);

    Paginator search(Paginator paginator,OtpSecurityArea otpsecurityarea);

    long countByCondition( OtpSecurityAreaCondition condition);

    int deleteByCondition( OtpSecurityAreaCondition condition);

    List<OtpSecurityArea> selectByCondition( OtpSecurityAreaCondition condition);

    int updateByConditionSelective( OtpSecurityArea record, OtpSecurityAreaCondition condition);

    // ===================== End of Code Gen =====================

    String dataFromDb(String userId, String clientOid);

}
