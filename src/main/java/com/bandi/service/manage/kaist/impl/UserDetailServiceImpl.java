package com.bandi.service.manage.kaist.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.common.util.MessageUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.UserDetailMapper;
import com.bandi.domain.User;
import com.bandi.domain.kaist.NiceResult;
import com.bandi.domain.kaist.UserDetail;
import com.bandi.domain.kaist.UserDetailCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.UserDetailService;
import com.bandi.service.orgSync.source.SourceUserMapper;

import NiceID.Check.CPClient;

@Service
public class UserDetailServiceImpl implements UserDetailService {

    @Resource
    protected UserDetailMapper dao;

    @Resource
    protected SourceUserMapper sourceUserdao;

    @Override
    public int insert(UserDetail userdetail) {
		userdetail = setCreatorInfo( userdetail );

        return dao.insertSelective(userdetail);
    }

    @Override
    public UserDetail get(String kaistuid) {
        UserDetail userdetail = null;
        userdetail = dao.selectByPrimaryKey(kaistuid);
        return userdetail;
    }

    @Override
    public void update(UserDetail userdetail) {
    	userdetail = setUpdatorInfo( userdetail );

        dao.updateByPrimaryKeySelective(userdetail);
    }

    @Override
    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }

    @Override
    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }

    @Override
    public Paginator search(Paginator paginator, UserDetail userdetail) {

        UserDetailCondition condition = new UserDetailCondition();

        if( userdetail != null) {
            String kaistUid = userdetail.getKaistUid();
            if (kaistUid!=null && !"".equals(kaistUid)){
                condition.or().andKaistUidEqualTo( userdetail.getKaistUid());
            }
            String userId = userdetail.getUserId();
            if (userId!=null && !"".equals(userId)){
                condition.or().andUserIdEqualTo( userdetail.getUserId());
            }
            String koreanName = userdetail.getKoreanName();
            if (koreanName!=null && !"".equals(koreanName)){
                condition.or().andKoreanNameLike( userdetail.getKoreanName()+"%");
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("kaistUid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    @Override
    public long countByCondition( UserDetailCondition condition){
        return dao.countByCondition( condition);
    }

    @Override
    public int deleteByCondition( UserDetailCondition condition){
        return dao.deleteByCondition( condition);
    }

    @Override
    public List<UserDetail> selectByCondition( UserDetailCondition condition){
        return dao.selectByCondition( condition);
    }

    @Override
    public int updateByConditionSelective( UserDetail record, UserDetailCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }


    protected UserDetail setCreatorInfo(UserDetail userdetail){
    	userdetail.setCreatorId( ContextUtil.getCurrentUserId() );
    	userdetail.setCreatedAt( DateUtil.getNow() );
    	return userdetail;
    }

    protected UserDetail setUpdatorInfo(UserDetail userdetail){
    	userdetail.setUpdatorId( ContextUtil.getCurrentUserId() );
    	userdetail.setUpdatedAt( DateUtil.getNow() );
    	return userdetail;
    }

    // ===================== End of Code Gen =====================

    @Override
    public String getKaistUidByEmployeeNumber(String employeeNumber, String birthday) {
        return dao.getKaistUidByEmployeeNumber(employeeNumber, birthday);
    }

    @Override
    public String getKaistUidByStudentNumber(String studentNumber, String birthday) {
        return dao.getKaistUidByStudentNumber(studentNumber, birthday);
    }

    @Override
    public void checkKaistUidforMailandPhone(String type, UserDetail userdetail, String serviceType) {

        UserDetail userdetailFromDb = get(userdetail.getKaistUid());

        if(userdetailFromDb == null) {
            throw new BandiException( ErrorCode_KAIST.USER_KAISTUID_NOT_FOUND);
        }

        if (BandiConstants_KAIST.AUTHENTICATION_TYPE_EMAIL.equals(type)) {
            if (userdetail.getChMail().equals(userdetailFromDb.getChMail()) == false) {
                throw new BandiException( ErrorCode_KAIST.EMAIL_IS_NOT_MATCH);
            }
        } else {
            if (userdetail.getMobileTelephoneNumber().equals(userdetailFromDb.getMobileTelephoneNumber()) == false) {
                throw new BandiException( ErrorCode_KAIST.HANDPHONE_IS_NOT_MATCH);
            }
        }
        
        if(BandiConstants_KAIST.SERVICE_TYPE_JOIN.equals(serviceType)) {
	        if(userdetailFromDb.getUserId() != null) {
	            throw new BandiException( ErrorCode_KAIST.USER_KAISTUID_IS_EXIST);
	        }
	    }
    }

    @Override
    public void updateUserId(User user) {
        UserDetail userdetail = get(user.getKaistUid());

        userdetail.setUserId(user.getId());

        update(userdetail);
    }

    @Override
    public NiceResult niceResult(NiceResult niceResult) {

        if(!"".equals(niceResult.getsEncodeData()) && !"".equals(niceResult.getsMode())){
            niceResult.setProceed(true);
        }else{
            niceResult.setProceed(false);
            niceResult.setMessage(MessageUtil.get("NICEID_RESPONSE_MESSAGE_PARAMETER_IS_MISSING"));
            niceResult.setFlag(BandiConstants_KAIST.NICE_API_FLAG_FAIL);
        }

        CPClient niceCheck = new CPClient();

        if(niceResult.isProceed()){
            niceResult.setiReturn(niceCheck.fnDecode(BandiProperties_KAIST.S_SITE_CODE, BandiProperties_KAIST.S_SITE_PASSWORD, niceResult.getsEncodeData()));

            Map<String,String> mapresult = new HashMap<String,String>();

            if(niceResult.getiReturn() == 0){
                niceResult.setsPlainData(niceCheck.getPlainData());
                niceResult.setsCipherTime(niceCheck.getCipherDateTime());

                // NICE 데이터 추출
                mapresult = niceCheck.fnParse(niceResult.getsPlainData());
                niceResult.setsRequestNumber(mapresult.get(BandiConstants_KAIST.NICE_API_RESULTMAP_KEY_REQ_SEQ));
                niceResult.setsResponseNumber(mapresult.get(BandiConstants_KAIST.NICE_API_RESULTMAP_KEY_RES_SEQ));
                niceResult.setsAuthType(mapresult.get(BandiConstants_KAIST.NICE_API_RESULTMAP_KEY_AUTH_TYPE));
                niceResult.setsName(mapresult.get(BandiConstants_KAIST.NICE_API_RESULTMAP_KEY_NAME));
                niceResult.setsBirthDate(mapresult.get(BandiConstants_KAIST.NICE_API_RESULTMAP_KEY_BIRTHDATE));
                niceResult.setsGender(mapresult.get(BandiConstants_KAIST.NICE_API_RESULTMAP_KEY_GENDER));
                niceResult.setsNationalInfo(mapresult.get(BandiConstants_KAIST.NICE_API_RESULTMAP_KEY_NATIONALINFO));
                niceResult.setsDupInfo(mapresult.get(BandiConstants_KAIST.NICE_API_RESULTMAP_KEY_DI));
                niceResult.setsConnInfo(mapresult.get(BandiConstants_KAIST.NICE_API_RESULTMAP_KEY_CI));

                // 정상적인 CI값이 추출되었는지 확인
                if(niceResult.getsConnInfo() != null && !"".equals(niceResult.getsConnInfo()) ){
                    niceResult.setMessage(MessageUtil.get("NICEID_RESPONSE_MESSAGE_SUCCESS"));
                    niceResult.setProceed(true);
                    niceResult.setFlag(BandiConstants_KAIST.NICE_API_FLAG_SUCCESS);
                }else{
                    niceResult.setMessage(MessageUtil.get("NICEID_RESPONSE_MESSAGE_FAIL"));
                    niceResult.setProceed(false);
                    niceResult.setFlag(BandiConstants_KAIST.NICE_API_FLAG_FAIL);
                }

            }else if(niceResult.getiReturn() == -1){
                niceResult.setMessage(MessageUtil.get("NICEID_RESPONSE_MESSAGE_DECRYPTION_SYSTEM_ERROR"));
                niceResult.setProceed(false);
                niceResult.setFlag(BandiConstants_KAIST.NICE_API_FLAG_FAIL);
            }else if(niceResult.getiReturn() == -4) {
                niceResult.setMessage(MessageUtil.get("NICEID_RESPONSE_MESSAGE_DECRYPTION_PROCESSING_ERROR"));
                niceResult.setProceed(false);
                niceResult.setFlag(BandiConstants_KAIST.NICE_API_FLAG_FAIL);
            }else if(niceResult.getiReturn() == -5){
                niceResult.setMessage(MessageUtil.get("NICEID_RESPONSE_MESSAGE_DECRYPTION_HASH_ERROR"));
                niceResult.setProceed(false);
                niceResult.setFlag(BandiConstants_KAIST.NICE_API_FLAG_FAIL);
            }else if( niceResult.getiReturn() == -6){
                niceResult.setMessage(MessageUtil.get("NICEID_RESPONSE_MESSAGE_DECRYPTION_DATA_ERROR"));
                niceResult.setProceed(false);
                niceResult.setFlag(BandiConstants_KAIST.NICE_API_FLAG_FAIL);
            }else if( niceResult.getiReturn() == -9){
                niceResult.setMessage(MessageUtil.get("NICEID_RESPONSE_MESSAGE_INPUT_DATA_ERROR"));
                niceResult.setProceed(false);
                niceResult.setFlag(BandiConstants_KAIST.NICE_API_FLAG_FAIL);
            }else if( niceResult.getiReturn() == -12){
                niceResult.setMessage(MessageUtil.get("NICEID_RESPONSE_MESSAGE_SITE_PASSWORD_ERROR"));
                niceResult.setProceed(false);
                niceResult.setFlag(BandiConstants_KAIST.NICE_API_FLAG_FAIL);
            }else{
                niceResult.setMessage(MessageUtil.get("NICEID_RESPONSE_MESSAGE_UNKNOWN_ERROR") + niceResult.getiReturn());
                niceResult.setProceed(false);
                niceResult.setFlag(BandiConstants_KAIST.NICE_API_FLAG_FAIL);
            }

        }

        if (niceResult.getiReturn() == 0 && niceResult.isProceed()) {

            // NICE RETURN 값으로 추출받은 CI 값과 USERDETAIL 테이블을 비교 하여 사용자 찾기.
            String kaistUid = getKaistUidFromPersonCI(niceResult.getsConnInfo());
            UserDetail userdetailFromDb = get(kaistUid);

            if (userdetailFromDb == null) {
                niceResult.setMessage(MessageUtil.get("NICEID_RESPONSE_MESSAGE_USER_IS_NOT_EXIST"));
                niceResult.setFlag(BandiConstants_KAIST.NICE_API_FLAG_FAIL);
            } else {
                // KAIST-TODO 우상훈 or 김보미 : MODE에 따라 분기처리 ?  회원가입(완료), ID찾기, PWD 찾기, OTP 등...
                // [해결방안] 김보미 : OTP 등록시 NICE_API 기능 처리하면됨
                if (BandiConstants_KAIST.NICE_API_MODE_REGISTER.equals(niceResult.getsMode())) {

                    if(isAlreadyRegisterdUser(userdetailFromDb)) {
                        niceResult.setMessage(MessageUtil.get("NICEID_RESPONSE_MESSAGE_USER_IS_REGISTERED"));
                        niceResult.setFlag(BandiConstants_KAIST.NICE_API_FLAG_FAIL);
                    } else {
                        niceResult.setFlag(BandiConstants_KAIST.NICE_API_FLAG_SUCCESS);
                        niceResult.setMessage(BandiConstants_KAIST.NICE_API_REGISTER_MESSAGE_SUCCESS);
                        niceResult.setKaistUid(userdetailFromDb.getKaistUid());
                    }
                } else if (BandiConstants_KAIST.NICE_API_MODE_FINDID_OR_FINDPWD_OR_OTPUNLOCK.equals(niceResult.getsMode())) {
                    // 기준정보에도있고, 회원가입된 사람이면 SUCCESS
                    if(isAlreadyRegisterdUser(userdetailFromDb)) {
                        niceResult.setMessage(BandiConstants_KAIST.NICE_API_REGISTER_MESSAGE_SUCCESS);
                        niceResult.setFlag(BandiConstants_KAIST.NICE_API_FLAG_SUCCESS);
                        niceResult.setKaistUid(userdetailFromDb.getKaistUid());
                    } else {
                        niceResult.setMessage(MessageUtil.get("NICEID_RESPONSE_MESSAGE_USER_IS_NOT_REGISTERED"));
                        niceResult.setFlag(BandiConstants_KAIST.NICE_API_FLAG_FAIL);
                    }
                }
            }

        } else {
            niceResult.setMessage(MessageUtil.get("NICEID_RESPONSE_MESSAGE_AUTHENTICATION_FAIL"));
            niceResult.setFlag(BandiConstants_KAIST.NICE_API_FLAG_FAIL);
        }

        return niceResult;
    }

    protected String getKaistUidFromPersonCI(String ci) {
        return sourceUserdao.getKaistUidFromPersonCI(ci);
    }

    protected boolean isAlreadyRegisterdUser(UserDetail userdetail) {
        if (userdetail.getUserId() != null) {
            return true;
        }
        return false;
    }
}
