package com.bandi.service.manage.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;

import com.bandi.common.BandiConstants;
import com.bandi.common.BandiProperties;
import com.bandi.common.cache.BandiCacheManager;
import com.bandi.common.util.CommonUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.common.util.LogUtil;
import com.bandi.common.util.NetworkUtil;
import com.bandi.dao.LicenseMapper;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.domain.Client;
import com.bandi.domain.ClientCondition;
import com.bandi.domain.License;
import com.bandi.domain.LicenseCondition;
import com.bandi.domain.LicenseKey;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.cryption.CryptService;
import com.bandi.service.manage.ClientService;
import com.bandi.service.manage.LicenseService;

import net.sf.ehcache.Element;

@DependsOn( value = {"messageUtil"})
public class LicenseServiceImpl implements LicenseService{

    protected Logger logger = LoggerFactory.getLogger(LicenseServiceImpl.class);

    @Autowired
    protected CryptService cryptService;

    @Resource
    protected ClientService clientService;

    @Resource
    protected LicenseMapper dao;

    public int insert( License license ){
        return dao.insertSelective( license );
    }

    public License get( String servername ){
        License license = null;
        license = dao.selectByPrimaryKey( servername );
        return license;
    }

    public void update( License license ){
        dao.updateByPrimaryKeySelective( license );
    }

    public void deleteBatch( List list ){
        dao.deleteBatch( list );
    }

    public int delete( String id ){
        return dao.deleteByPrimaryKey( id );
    }

    public Paginator search( Paginator paginator, License license ){

        LicenseCondition condition = new LicenseCondition();

        if( license != null){
            String serverName = license.getServerName();
            if( serverName != null && ! "".equals( serverName ) ){
                condition.or().andServerNameLike( license.getServerName() + "%" );
            }

            String type = license.getType();
            if( type != null && ! "".equals( type ) ){
                condition.or().andTypeLike( license.getType() + "%" );
            }

            String clientIds = license.getClientIds();
            if( clientIds != null && ! "".equals( clientIds ) ){
                condition.or().andClientIdsLike( license.getClientIds() + "%" );
            }

            String createdAt = license.getCreatedAt();
            if( createdAt != null ){
                condition.or().andCreatedAtLike( license.getCreatedAt() );
            }

            String expiredAt = license.getExpiredAt();
            if( expiredAt != null ){
                condition.or().andCreatedAtLike( license.getExpiredAt() );
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause( "SERVERNAME DESC" );
        }

        PaginatorEx paginatorex = new PaginatorEx( paginator.getPage(), paginator.getRowsperpage() );
        paginatorex.setCondition( condition );

        paginator.setTotal( dao.countForSearch( condition ) );
        paginator.setList( dao.pagingQueryForSearch( paginatorex ) );

        return paginator;
    }

    public long countByCondition( LicenseCondition condition ){
        return dao.countByCondition( condition );
    }

    public int deleteByCondition( LicenseCondition condition ){
        return dao.deleteByCondition( condition );
    }

    public List< License > selectByCondition( LicenseCondition condition ){
        return dao.selectByCondition( condition );
    }

    public int updateByConditionSelective( License record, LicenseCondition condition ){
        return dao.updateByConditionSelective( record, condition );
    }

    // ===================== End of Code Gen =====================

    @Override
    public void initClientLicense(){

        License license = null;

        LicenseKey licenseKey = new LicenseKey();

        boolean isSuccess = true;

        try{

            // KAIST-라이센스 사용하지 않음
            if( true){
                return;
            }

            // --------------------------------
            // 라이선스 조회
            // --------------------------------
            license = dao.selectByPrimaryKey( BandiProperties.SSO_SERVER_NAME );

            if( license == null || CommonUtil.isEmpty( license.getLicenseKey() ) ){
                throw new BandiException(ErrorCode.LICENSE_FAIL);
            }

            // --------------------------------
            // 라이선스 파싱
            // --------------------------------
            licenseKey.setLicenseKey( license.getLicenseKey() );
            licenseKey = parseLicenseKey( licenseKey );

            // --------------------------------
            // 라이선스 유효성 체크
            // --------------------------------
            // hashKey 비교
            if( ! licenseKey.getHashKey().equals( licenseKey.getHashKeyDecrypt() ) ){
                throw new BandiException(ErrorCode.LICENSE_FAIL);
            }

            // 서버키 비교
            if( ! BandiProperties.SSO_SERVER_NAME.equals( licenseKey.getSsoServerName() ) ){
                throw new BandiException(ErrorCode.LICENSE_FAIL);
            }

            // 서버 IP
            if( BandiConstants.LICENSE_TYPE_UNLIMITED_SERVER_IP.equals( licenseKey.getLicenseType() ) ) {
                boolean isLicense = checkLicenseDate( licenseKey );
                setLicense( isLicense );
            } else {

                if ( ! checkServerIp(licenseKey) ) {
                    throw new BandiException(ErrorCode.LICENSE_FAIL);
                }

                if( BandiConstants.LICENSE_TYPE_WITHOUT_LIMIT.equals( licenseKey.getLicenseType() ) ){
                    // 무제한 라이선스
                    setLicense( true );

                } else if( BandiConstants.LICENSE_TYPE_CLIENT_LIMIT.equals( licenseKey.getLicenseType() ) ){
                    // 클라이언트 제한
                    setLicense( licenseKey );

                } else if( BandiConstants.LICENSE_TYPE_DAY_LIMIT.equals( licenseKey.getLicenseType() ) ){
                    // 기간 제한
                    boolean isLicense = checkLicenseDate( licenseKey );
                    setLicense( isLicense );

                } else if( BandiConstants.LICENSE_TYPE_DAY_AND_CLIENT_LIMIT.equals( licenseKey.getLicenseType() ) ){
                    // 기간 및 클라이언트 제한
                    boolean isLicense = checkLicenseDate( licenseKey );
                    if( isLicense ){
                        setLicense( licenseKey );
                    }

                } else{
                    // 라이선스 타입 미존재
                    throw new BandiException(ErrorCode.LICENSE_FAIL);
                }

            }

        } catch( BandiException e ){
            isSuccess = false;
            logger.error(e.getMessage(), e);
        } catch( Exception e) {
            isSuccess = false;
            logger.error( e.getMessage(), e );
        } finally {

            if( isSuccess == false){
                StringBuffer sb = new StringBuffer();
                sb.append( System.lineSeparator()).append(  "License Fail : 라이센스 키의 유효성 체크(HashKey,ServerIp,ServerName)를 실패하였습니다." );

                logger.error( LogUtil.makeErrorMsg( sb.toString()) );
                System.exit(-1);
            }
        }
    }

    protected void setLicense( LicenseKey licenseKey ) {

        String[] clientIds = licenseKey.getClientIDs().split( "\\" + BandiConstants.DELIMITER_TYPE_LICENSE_LIST );

        List list = BandiCacheManager.getAllCacheElements( BandiCacheManager.CACHE_CLIENT );

        if( list != null && list.size() > 0 ){

            for( int i = 0; i < list.size(); i++ ){
                Element element = (Element) list.get(i);
                Client client = SerializationUtils.deserialize( (byte[]) element.getValue());

                boolean isSuccess = false;

                for( String compClientId : clientIds ){
                    if( client.getOid().equals( compClientId ) ){

                        client.setFlagLicense( BandiConstants.FLAG_Y );
                        isSuccess = true;
                        break;
                    }
                }

                // 라이선스 미존재
                if( ! isSuccess ){
                    client.setFlagLicense( BandiConstants.FLAG_N );
                }

                BandiCacheManager.put( BandiCacheManager.CACHE_CLIENT, client.getOid(), client );
            }
        }
    }

    protected void setLicense( boolean licenseFlag ){

        List list = BandiCacheManager.getAllCacheElements( BandiCacheManager.CACHE_CLIENT );

        if( list != null && list.size() > 0 ){

            for( int i = 0; i < list.size(); i++ ){

                Element element = (Element) list.get(i);

                Client client = SerializationUtils.deserialize( (byte[]) element.getValue());

                Client clientForUpdate = new Client();

                if( licenseFlag ){
                    clientForUpdate.setFlagLicense( BandiConstants.FLAG_Y );
                    client.setFlagLicense(BandiConstants.FLAG_Y);
                } else{
                    clientForUpdate.setFlagLicense( BandiConstants.FLAG_N );
                    client.setFlagLicense(BandiConstants.FLAG_N);
                }

                ClientCondition condition = new ClientCondition();
                condition.createCriteria().andOidEqualTo( client.getOid());

                // flagLicense Setting
                clientService.updateByConditionSelective( clientForUpdate, condition);

                BandiCacheManager.put( BandiCacheManager.CACHE_CLIENT, client.getOid(), client );
            }
        }
    }

    protected LicenseKey parseLicenseKey( LicenseKey licenseKey ) throws Exception{
        // 파싱
        licenseKey = setLicenseKey( licenseKey );
        return licenseKey;
    }

    protected LicenseKey setLicenseKey( LicenseKey licenseKey ) throws Exception{
        String[] licenseValue = cryptService.decryptLicense( licenseKey.getLicenseKey() ).split( "\\" + BandiConstants.DELIMITER_TYPE_LICENSE );

        int index = 0;
        licenseKey.setRandomString( licenseValue[ index++ ] );
        licenseKey.setSsoServerName( licenseValue[ index++ ] );
        licenseKey.setLicenseType( licenseValue[ index++ ] );
        licenseKey.setClientIDs( licenseValue[ index++ ] );
        licenseKey.setCreateDate( licenseValue[ index++ ] );
        licenseKey.setExpireDate( licenseValue[ index++ ] );
        licenseKey.setEncryptIndex( licenseValue[ index++ ] );
        licenseKey.setHashKeyEncrypt( licenseValue[ index++ ] );
        licenseKey.setHashKeyDecrypt( cryptService.decryptLicenseHash( licenseKey.getHashKeyEncrypt() ) );
        if(licenseValue.length < 9) {
            licenseKey.setServerIp("");
        } else {
            licenseKey.setServerIp( licenseValue[ index++ ] );
        }

        String hashKey = getHashKey( licenseKey );
        licenseKey.setHashKey( hashKey );

        return licenseKey;
    }

    protected String getHashKey( LicenseKey licenseKey ) throws Exception{
        StringBuffer sb = new StringBuffer();
        sb.append( licenseKey.getRandomString() );
        sb.append( licenseKey.getSsoServerName() );
        sb.append( licenseKey.getLicenseType() );
        sb.append( licenseKey.getClientIDs() );
        sb.append( licenseKey.getCreateDate() );
        sb.append( licenseKey.getExpireDate() );
        sb.append( licenseKey.getEncryptIndex() );

        String hashKey = cryptService.getHashCode( sb.toString() );
        return hashKey;
    }

    protected boolean checkLicenseDate( LicenseKey licenseKey ) throws Exception{
        boolean result = true;
        if( CommonUtil.isEmpty( licenseKey.getExpireDate() ) ){
            result = false;

        } else{
            String currentDt = DateUtil.getCurrectYMD();
            if( currentDt.compareTo( licenseKey.getExpireDate() ) > 0 ){
                result = false;

                throw new Exception( "LICENSE ERROR : EXPIRES");

            } else{
                result = true;
            }

        }
        return result;
    }

    protected boolean checkServerIp( LicenseKey licenseKey ) {
        boolean result = false;

        String[] serverIpArray = licenseKey.getServerIp().split("\\" + BandiConstants.DELIMITER_TYPE_LICENSE_LIST);

        String serverLocalIp = "";

        serverLocalIp = NetworkUtil.getInstance().getLocalIp();

        for( String serverIp : serverIpArray) {
            if( serverLocalIp.equals(serverIp) ) {
                result = true;
            }
        }

        return result;
    }

    public static final long MODULE_CM      = 0x00000001;
    public static final long MODULE_SSO     = 0x00000002;
    public static final long MODULE_IM      = 0x00000004;
    public static final long MODULE_EAM     = 0x00000008;
    public static final long MODULE_OTP     = 0x00000010;

    private static long PRODUCT_TYPE = MODULE_CM | MODULE_IM | MODULE_EAM | MODULE_OTP;

    public static boolean supportModule( long moduleType){
        if ( ( PRODUCT_TYPE & moduleType ) == moduleType){
            return true;
        } else {
            return false;
        }
    }

    public static boolean supportCommon(){
        return supportModule( MODULE_CM );
    }

    public static boolean supportSso(){
        return supportModule( MODULE_SSO );
    }

    public static boolean supportIm(){
        return supportModule( MODULE_IM );
    }

    public static boolean supportEam(){
        return supportModule( MODULE_EAM );
    }

    public static boolean supportOtp(){
        return supportModule( MODULE_OTP );
    }

    public static boolean supportOnlySso() {
        if ( PRODUCT_TYPE == (MODULE_CM | MODULE_SSO) ) {
            return true;
        } else {
            return false;
        }
    }

    public static List<String> getSupportModules() {
        List<String> modules = new ArrayList();
        modules.add( String.valueOf( LicenseServiceImpl.MODULE_CM ));

        if (LicenseServiceImpl.supportSso()){
            modules.add( String.valueOf( LicenseServiceImpl.MODULE_SSO ));
        }

        if (LicenseServiceImpl.supportIm()){
            modules.add( String.valueOf( LicenseServiceImpl.MODULE_IM ));
        }

        if (LicenseServiceImpl.supportEam()){
            modules.add( String.valueOf( LicenseServiceImpl.MODULE_EAM));
        }

        if (LicenseServiceImpl.supportOtp()){
            modules.add( String.valueOf( LicenseServiceImpl.MODULE_OTP));
        }

        return modules;
    }

}
