package com.bandi.service.kaist;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;

import com.bandi.common.BandiAppContext;
import com.bandi.service.BandiInitializeContext;
import com.bandi.service.BandiInitializer;
import com.bandi.service.manage.impl.LicenseServiceImpl;
import com.bandi.service.schedule.BandiScheduler;

public class BandiInitializeContext_KAIST extends BandiInitializeContext implements BandiInitializer{
	@Override
    public void init() throws Exception{
		ApplicationContext context = BandiAppContext.getContext();
		if( context instanceof WebApplicationContext ){

			licenseService.initClientLicense();

			( ( BandiScheduler ) BandiAppContext.getContext().getBean( "bandiScheduler" ) ).initScheduler();

            whiteIpService.refreshIpFilter();

            cacheClient();

			initAdminPasswordChange();

			loginHistoryService.updateLoginHistoryStatusWhenRestart();

            if( LicenseServiceImpl.supportSso() ){

                setResourcesIntegrity();
                accessTokenService.expire();
                cacheAccessToken();

                inspectionTargetService.refreshInspectionTarget();
            }
		}
	}


}
