package com.bandi.service.standard2.source;

import com.bandi.common.util.bpel.domain.ImUserVO;

public interface SyncIntUserToStdMapper {
	
	public ImUserVO SP_K_SSO_ID_REG(ImUserVO user);
	
	public ImUserVO SP_K_REG_PERSON_INFO(ImUserVO user);
}
