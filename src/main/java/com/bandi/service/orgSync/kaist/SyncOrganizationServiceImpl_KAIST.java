package com.bandi.service.orgSync.kaist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.ibatis.session.ResultContext;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.bandi.common.BandiConstants;
import com.bandi.common.IdGenerator;
import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.common.util.DateUtil_KAIST;
import com.bandi.dao.UserMapper;
import com.bandi.dao.kaist.UserDetailMapper;
import com.bandi.domain.Group;
import com.bandi.domain.GroupCondition;
import com.bandi.domain.SyncGroup;
import com.bandi.domain.SyncGroupCondition;
import com.bandi.domain.SyncUser;
import com.bandi.domain.SyncUserCondition;
import com.bandi.domain.User;
import com.bandi.domain.UserCondition;
import com.bandi.domain.kaist.RoleMaster;
import com.bandi.domain.kaist.RoleMasterCondition;
import com.bandi.domain.kaist.RoleMember;
import com.bandi.domain.kaist.UserDetail;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.GroupService_KAIST;
import com.bandi.service.manage.kaist.RoleMasterService;
import com.bandi.service.manage.kaist.RoleMemberService;
import com.bandi.service.manage.kaist.SyncGroupService_KAIST;
import com.bandi.service.manage.kaist.SyncUserService_KAIST;
import com.bandi.service.manage.kaist.UserDetailService;
import com.bandi.service.manage.kaist.UserService_KAIST;
import com.bandi.service.orgSync.SyncOrganizationServiceImpl;
import com.bandi.service.orgSync.source.SourceConcurrentUser;
import com.bandi.service.orgSync.source.SourceConcurrentUserMapper;
import com.bandi.service.orgSync.source.SourceGroup;
import com.bandi.service.orgSync.source.SourceGroupHeadUser;
import com.bandi.service.orgSync.source.SourceGroupHeadUserMapper;
import com.bandi.service.orgSync.source.SourceUser;
import com.bandi.service.orgSync.source.SourceUserCondition;

public class SyncOrganizationServiceImpl_KAIST extends SyncOrganizationServiceImpl
        implements SyncOrganizationService_KAIST {

    // -- 연계 대상 Source쪽은 별다른 비즈니스 로직이 없이 쿼리만 사용하기 때문에, Service없이 Mapper만 사용
    // -----------------------
    // -----------------------------------------------------------------------------------------------------------------

    @Resource
    protected UserService_KAIST userService;

    @Resource
    protected GroupService_KAIST groupService;

    @Resource
    protected RoleMemberService roleMemberService;

    @Resource
    protected RoleMasterService roleMasterService;

    @Resource
    protected SyncGroupService_KAIST syncGroupService;

    @Resource
    protected SyncUserService_KAIST syncUserService;

    @Autowired
    protected SqlSessionFactory sqlSessionFactory;

    @Autowired
    protected SqlSessionFactory orgSyncSqlSessionFactory;

    @Autowired
    protected DataSource dataSource;

    @Resource
    protected UserDetailService userDetailService;

    @Resource
    protected UserMapper userDao;

    @Resource
    protected UserDetailMapper userDetailDao;

    @Resource
    protected SourceConcurrentUserMapper sourceConcurrentUserDao;

    @Resource
    protected SourceGroupHeadUserMapper sourceGroupHeadUserDao;

    @Value("#{applicationProperties['orgSync.target.database']}")
    private String syncOrgTargetDatabase; // ASIS(기준정보) or TOBE(공유DB)

    protected static final Logger logger = LoggerFactory.getLogger("syncOrg");

    @Override
    public void execute() {

        try {
            logger.debug("[START]SyncOrg");

            // 작업이 시작된 시간을 기준으로 처리됨.
            Timestamp now = DateUtil_KAIST.getNow();

            if (BandiConstants_KAIST.SYNC_ORGANIZATION_TYPE_ASIS.equals(syncOrgTargetDatabase)) {
                // 반영 데이터 필터링
                // SourceDB에서 변경된 부서를 SyncGroup 테이블에 입력
                compareGroupsAsIs(now);

                // SourceDB에서 변경된 사용자를 SyncUser 테이블에 입력
                compareUsersAsIs(now);

                // 실제 반영
                // BDSGroup 테이블에 적용
                syncGroupsAsIs(now);

                // BDSUser 테이블에 적용
                syncUsersAsIs(now);

                if (BandiProperties_KAIST.IS_TEST_ORG_SYNC == false) {
                    // 겸직 사용자 반영
                    syncConcurrentUsersAsIs();

                    // 부서장 반영
                    syncGroupHeadUserAsIs();
                }
            } else {
                // KAIST-TODO 이혜련 : 09.16 기준정보로 조직도연동을 변경함. 공유 DB로 추후에 변경되어야 함.
                // compareGroupsToBe(now);

                // KAIST-TODO 이혜련 : 09.16 기준정보로 조직도연동을 변경함. 공유 DB로 추후에 변경되어야 함.
                // compareUsersToBe(now);

                // KAIST-TODO 이혜련 : 09.16 기준정보로 조직도연동을 변경함. 공유 DB로 추후에 변경되어야 함.
                // syncGroupsToBe(now);

                // KAIST-TODO 이혜련 : 09.16 기준정보로 조직도연동을 변경함. 공유 DB로 추후에 변경되어야 함.
                // syncUsersToBe(now);
            }

        } catch (Throwable t) {

            t.printStackTrace();

            logger.error("[FAIL]SyncOrg", t.getMessage());

        } finally {
            logger.debug("[END]SyncOrg");
        }
    }

    protected void compareGroupsAsIs(Timestamp now) {
        List<SyncGroup> syncGroups = new ArrayList<>();

        try (SqlSession orgSqlSession = orgSyncSqlSessionFactory.openSession()) {
            orgSqlSession.select("com.bandi.service.orgSync.source.SourceGroupMapper.selectByHierarchical",
                    SourceGroup.SOURCE_ROOT_OID, new ResultHandler<SourceGroup>() {
                        @Override
                        public void handleResult(ResultContext<? extends SourceGroup> context) {
                            SourceGroup sourceGroup = context.getResultObject();

                            String sourceGroupId = sourceGroup.getOrganizationIdToString();
                            SyncGroup syncGroup = convertToSyncGroupAsIs(sourceGroup, now);

                            Group group = groupService.get(sourceGroupId);

                            boolean isDeleteGroup = checkDeleteGroup(sourceGroup.getDateTo());

                            if (isDeleteGroup == false) {
                                if (group == null) { // 등록
                                    syncGroup.setActionType(SyncOrgConstants_KAIST.ACTION_TYPE_INSERT);
                                    syncGroups.add(syncGroup);
                                } else { // 수정, 이동
                                    String modifiedFileds = findModifiedGroupFiledsAsIs(sourceGroup, group);

                                    if (modifiedFileds.length() > 0) {
                                        syncGroup.setErrorMessage(modifiedFileds);

                                        if (sourceGroup.getOrganizationIdParentToString().equals(group.getParentId())
                                                || isParentIdEqualsKaistRootId(
                                                        sourceGroup.getOrganizationIdParentToString(),
                                                        group.getParentId())) {
                                            syncGroup.setActionType(SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE);
                                        } else {
                                            syncGroup.setActionType(SyncOrgConstants_KAIST.ACTION_TYPE_MOVE);
                                        }
                                        syncGroups.add(syncGroup);
                                    }
                                }
                            } else {
                                if (group != null) { // 등록되었으나 삭제될 부서
                                    if (Group.CONST_STATUS_ACTIVE.equals(group.getStatus())) {
                                        syncGroup.setActionType(SyncOrgConstants_KAIST.ACTION_TYPE_DELETE);
                                        syncGroups.add(syncGroup);
                                    }
                                } else { // 등록되지 않았지만 등록 후 STATUS 값은 D로 해야할 부서
                                    syncGroup.setActionType(SyncOrgConstants_KAIST.ACTION_TYPE_NOT_AVAILABLE);
                                    syncGroups.add(syncGroup);
                                }
                            }
                        }
                    });
        }

        // ORGANIZATIONS_KAIST_V 에서 ROW 자체가 삭제된 경우
        GroupCondition groupCondition = new GroupCondition();
        groupCondition.or().andFlagSyncEqualTo(BandiConstants_KAIST.FLAG_Y);
        groupCondition.setOrderByClause("FULLPATHINDEX");

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            sqlSession.select("com.bandi.dao.GroupMapper.selectByCondition", groupCondition,
                    new ResultHandler<Group>() {
                        @Override
                        public void handleResult(ResultContext<? extends Group> context) {
                            Group group = context.getResultObject();

                            int result = sourceGroupMapper.isExistSourceGroup(group.getId());

                            if (result == 0) {
                                SyncGroup syncGroup = convertToSyncGroupAsIs(group, now);
                                syncGroup.setActionType(SyncOrgConstants_KAIST.ACTION_TYPE_DELETE);
                                syncGroups.add(syncGroup);
                            }
                        }
                    });
        }

        String sql = "";
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            sql = sqlSession.getConfiguration().getMappedStatement("com.bandi.dao.SyncGroupMapper.insert")
                    .getBoundSql(new Object()).getSql();
        }

        try (Connection con = dataSource.getConnection()) {
            try (PreparedStatement pstmt = con.prepareStatement(sql)) {
                int index = 0;

                for (SyncGroup syncGroup : syncGroups) {
                    index++;

                    pstmt.setString(1, syncGroup.getOid());
                    pstmt.setString(2, syncGroup.getId());
                    pstmt.setString(3, syncGroup.getName());
                    pstmt.setString(4, syncGroup.getDescription());
                    pstmt.setInt(5, syncGroup.getSortOrder());
                    pstmt.setString(6, syncGroup.getParentId());
                    pstmt.setString(7, syncGroup.getActionType());
                    pstmt.setString(8, syncGroup.getActionStatus());
                    pstmt.setTimestamp(9, syncGroup.getCreatedAt());
                    pstmt.setTimestamp(10, syncGroup.getAppliedAt());
                    pstmt.setString(11, syncGroup.getErrorMessage());
                    pstmt.setString(12, syncGroup.getGroupCode());
                    pstmt.setString(13, syncGroup.getCreatorId());
                    pstmt.setString(14, syncGroup.getUpdatorId());
                    pstmt.setTimestamp(15, syncGroup.getUpdatedAt());

                    pstmt.addBatch();
                    pstmt.clearParameters();

                    if ((index % 1000) == 0) {
                        pstmt.executeBatch();
                        pstmt.clearBatch();
                        con.commit();
                    }
                }
                pstmt.executeBatch();
                con.commit();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    protected boolean checkDeleteGroup(Date deleteDateTime) {
        if (deleteDateTime != null) {
            if (DateUtil_KAIST.isBeforeDateFromToday(deleteDateTime)) {
                return true;
            }
        }
        return false;
    }

    protected SyncGroup convertToSyncGroupAsIs(Object obj, Timestamp now) {

        SyncGroup syncGroup = null;

        if (obj != null) {
            syncGroup = new SyncGroup();
            syncGroup.setOid(IdGenerator.getUUID());
        } else {
            return null;
        }

        if (obj instanceof SourceGroup) {
            SourceGroup sourceGroup = (SourceGroup) obj;

            syncGroup.setId(sourceGroup.getOrganizationIdToString());
            syncGroup.setGroupCode(sourceGroup.getType());
            syncGroup.setName(sourceGroup.getName());

            if (SourceGroup.SOURCE_ROOT_OID.equals(sourceGroup.getOrganizationIdParentToString())) {
                syncGroup.setParentId(Group.GROUP_ROOT_OID);
            } else {
                syncGroup.setParentId(sourceGroup.getOrganizationIdParentToString());
            }

        } else if (obj instanceof Group) {

            Group group = (Group) obj;

            syncGroup.setId(group.getId());
            syncGroup.setGroupCode(group.getTypeCode());
            syncGroup.setName(group.getName());
            syncGroup.setParentId(group.getParentId());

        }

        syncGroup.setActionStatus(SyncOrgConstants_KAIST.ACTION_STATUS_WAIT);
        syncGroup.setCreatedAt(now);

        return syncGroup;
    }

    public String findModifiedGroupFiledsAsIs(SourceGroup sourceGroup, Group group) {

        StringBuffer sb = new StringBuffer();

        compareModifiedField(sourceGroup.getName(), group.getName(), "Name", sb);
        compareModifiedField(sourceGroup.getType(), group.getTypeCode(), "GroupType", sb);

        if (sourceGroup.getOrganizationIdParentToString().equals(group.getParentId()) == false) {
            if (isParentIdEqualsKaistRootId(sourceGroup.getOrganizationIdParentToString(),
                    group.getParentId()) == false) {
                sb.append("ParentId").append(";;").append(group.getParentId()).append(";;")
                        .append(sourceGroup.getOrganizationIdParent()).append("|");
            }
        }

        return sb.toString();
    }

    protected void syncGroupsAsIs(Timestamp now) {
        List<SyncGroup> syncGroups = new ArrayList<>();

        SyncGroupCondition condition = new SyncGroupCondition();
        condition.createCriteria().andActionStatusEqualTo(SyncOrgConstants_KAIST.ACTION_STATUS_WAIT);
        condition.setOrderByClause("SEQ");

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            sqlSession.select("com.bandi.dao.SyncGroupMapper.selectByCondition", condition,
                    new ResultHandler<SyncGroup>() {

                        @Override
                        public void handleResult(ResultContext<? extends SyncGroup> context) {
                            SyncGroup syncGroup = context.getResultObject();

                            try {
                                Group group = null;
                                if (SyncOrgConstants_KAIST.ACTION_TYPE_INSERT.equals(syncGroup.getActionType())) {
                                    group = convertToGroupAsIs(syncGroup, SyncOrgConstants_KAIST.ACTION_TYPE_INSERT);

                                    try {
                                        groupService.insert(group);
                                    } catch (Exception e) {
                                        throw e;
                                    }

                                } else if (SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE
                                        .equals(syncGroup.getActionType())) {
                                    group = convertToGroupAsIs(syncGroup, SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE);

                                    try {
                                        groupService.update(group);
                                    } catch (Exception e) {
                                        throw e;
                                    }

                                } else if (SyncOrgConstants_KAIST.ACTION_TYPE_MOVE.equals(syncGroup.getActionType())) {
                                    group = convertToGroupAsIs(syncGroup, SyncOrgConstants_KAIST.ACTION_TYPE_MOVE);
                                    Group targetGroup = groupService.get(syncGroup.getParentId());
                                    if (targetGroup == null) {
                                        throw new BandiException(ErrorCode.TARGET_GROUP_NOT_FOUND);
                                    }
                                    try {
                                        groupService.move(group, targetGroup.getId());
                                    } catch (Exception e) {
                                        throw e;
                                    }

                                } else if (SyncOrgConstants_KAIST.ACTION_TYPE_DELETE
                                        .equals(syncGroup.getActionType())) {
                                    // 등록된 상태에서 삭제 될 부서
                                    groupService.delete(syncGroup.getId());
                                } else if (SyncOrgConstants_KAIST.ACTION_TYPE_NOT_AVAILABLE
                                        .equals(syncGroup.getActionType())) {
                                    // 등록되지 않은 상태이기때문에 등록할 때 상태값 D로 INSERT
                                    group = convertToGroupAsIs(syncGroup,
                                            SyncOrgConstants_KAIST.ACTION_TYPE_NOT_AVAILABLE);

                                    try {
                                        groupService.insert(group);
                                    } catch (Exception e) {
                                        throw e;
                                    }
                                }

                                syncGroup.setActionStatus(SyncOrgConstants_KAIST.ACTION_STATUS_FINISH);

                            } catch (Exception e) {

                                syncGroup.setActionStatus(SyncOrgConstants_KAIST.ACTION_STATUS_ERROR);
                                syncGroup.setErrorMessage(syncGroup.getErrorMessage() + "|" + e.getMessage());

                                if (syncGroup.getErrorMessage() != null
                                        && syncGroup.getErrorMessage().length() > 6000) {
                                    syncGroup.setErrorMessage(syncGroup.getErrorMessage().substring(0, 6000));
                                }

                                e.printStackTrace();

                            } finally {
                                syncGroup.setAppliedAt(now);
                                syncGroups.add(syncGroup);
                            }
                        }
                    });
        }

        String sql = "";
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            sql = sqlSession.getConfiguration().getMappedStatement("com.bandi.dao.SyncGroupMapper.updateStatusBatch")
                    .getBoundSql(new Object()).getSql();
        }

        try (Connection con = dataSource.getConnection()) {
            try (PreparedStatement pstmt = con.prepareStatement(sql)) {
                int index = 0;

                for (SyncGroup syncGroup : syncGroups) {
                    index++;

                    pstmt.setString(1, syncGroup.getActionStatus());
                    pstmt.setTimestamp(2, syncGroup.getAppliedAt());
                    pstmt.setString(3, syncGroup.getErrorMessage());
                    pstmt.setString(4, syncGroup.getOid());

                    pstmt.addBatch();
                    pstmt.clearParameters();

                    if ((index % 1000) == 0) {
                        pstmt.executeBatch();
                        pstmt.clearBatch();
                        con.commit();
                    }
                }
                pstmt.executeBatch();
                con.commit();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    protected Group convertToGroupAsIs(SyncGroup syncGroup, String actionType) {
        Group group = null;

        if (syncGroup != null) {

            Group groupFromDB = groupService.get(syncGroup.getId());

            group = new Group();

            if (SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE.equals(actionType)
                    || SyncOrgConstants_KAIST.ACTION_TYPE_MOVE.equals(actionType)) {
                group = groupFromDB;
            }

            if (SyncOrgConstants_KAIST.ACTION_TYPE_INSERT.equals(actionType)) {
                group.setParentId(syncGroup.getParentId());
            }

            if (SyncOrgConstants_KAIST.ACTION_TYPE_NOT_AVAILABLE.equals(actionType)) {
                group.setParentId(syncGroup.getParentId());
                group.setStatus(Group.CONST_STATUS_DELETE);
            }

            group.setId(syncGroup.getId());
            group.setName(syncGroup.getName());
            group.setTypeCode(syncGroup.getGroupCode());
            group.setFlagSync(BandiConstants.FLAG_Y);
        }
        return group;
    }

    protected void compareUsersAsIs(Timestamp now) {

        List<SyncUser> syncUsers = new ArrayList<>();

        SourceUserCondition condition = new SourceUserCondition();

        try (SqlSession orgSqlSession = orgSyncSqlSessionFactory.openSession()) {
            orgSqlSession.select("com.bandi.service.orgSync.source.SourceUserMapper.selectByCondition", condition,
                    new ResultHandler<SourceUser>() {
                        @Override
                        public void handleResult(ResultContext<? extends SourceUser> context) {
                            SourceUser sourceUser = context.getResultObject();
                            String sourceUserId = sourceUser.getKaistUid();

                            SyncUser syncUser = convertToSyncUserAsIs(sourceUser, now);

                            UserDetail userdetail = userDetailService.get(sourceUserId);

                            if (userdetail == null) {
                                syncUser.setActionType(SyncOrgConstants_KAIST.ACTION_TYPE_INSERT);
                                syncUsers.add(syncUser);
                            } else {
                                String modifiedFileds = findModifiedUserFiledsAsIs(sourceUser, userdetail);

                                if (modifiedFileds.length() > 0) {
                                    syncUser.setErrorMessage(modifiedFileds);

                                    if (sourceUser.getEbsOrganizationId().equals(userdetail.getEbsOrganizationId())) {
                                        syncUser.setActionType(SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE);
                                    } else {
                                        syncUser.setActionType(SyncOrgConstants_KAIST.ACTION_TYPE_MOVE);
                                    }
                                    syncUsers.add(syncUser);
                                }
                            }
                        }
                    });

        }

        String sql = "";
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            sql = sqlSession.getConfiguration().getMappedStatement("com.bandi.dao.SyncUserMapper.insert")
                    .getBoundSql(new Object()).getSql();
        }

        try (Connection con = dataSource.getConnection()) {
            try (PreparedStatement pstmt = con.prepareStatement(sql)) {
                int index = 0;

                for (SyncUser syncUser : syncUsers) {
                    index++;

                    pstmt.setString(1, syncUser.getOid());
                    pstmt.setString(2, syncUser.getKaistUid());
                    pstmt.setString(3, syncUser.getId());
                    pstmt.setString(4, syncUser.getKoreanName());
                    pstmt.setString(5, syncUser.getEnglishName());
                    pstmt.setString(6, syncUser.getLastName());
                    pstmt.setString(7, syncUser.getFirstName());
                    pstmt.setDate(8, syncUser.getBirthday());
                    pstmt.setString(9, syncUser.getNationCodeUid());
                    pstmt.setString(10, syncUser.getSexCodeUid());
                    pstmt.setString(11, syncUser.getEmailAddress());
                    pstmt.setString(12, syncUser.getChMail());
                    pstmt.setString(13, syncUser.getMobileTelephoneNumber());
                    pstmt.setString(14, syncUser.getOfficeTelephoneNumber());
                    pstmt.setString(15, syncUser.getOweHomeTelephoneNumber());
                    pstmt.setString(16, syncUser.getFaxTelephoneNumber());
                    pstmt.setString(17, syncUser.getPostNumber());
                    pstmt.setString(18, syncUser.getAddress());
                    pstmt.setString(19, syncUser.getAddressDetail());
                    pstmt.setString(20, syncUser.getPersonId());
                    pstmt.setString(21, syncUser.getEmployeeNumber());
                    pstmt.setString(22, syncUser.getStdNo());
                    pstmt.setString(23, syncUser.getAcadOrg());
                    pstmt.setString(24, syncUser.getAcadName());
                    pstmt.setString(25, syncUser.getAcadKstOrgId());
                    pstmt.setString(26, syncUser.getAcadEbsOrgId());
                    pstmt.setString(27, syncUser.getAcadEbsOrgNameEng());
                    pstmt.setString(28, syncUser.getAcadEbsOrgNameKor());
                    pstmt.setString(29, syncUser.getCampusUid());
                    pstmt.setString(30, syncUser.getEbsOrganizationId());
                    pstmt.setString(31, syncUser.getEbsOrgNameEng());
                    pstmt.setString(32, syncUser.getEbsOrgNameKor());
                    pstmt.setString(33, syncUser.getEbsGradeNameEng());
                    pstmt.setString(34, syncUser.getEbsGradeNameKor());
                    pstmt.setString(35, syncUser.getEbsGradeLevelEng());
                    pstmt.setString(36, syncUser.getEbsGradeLevelKor());
                    pstmt.setString(37, syncUser.getEbsPersonTypeEng());
                    pstmt.setString(38, syncUser.getEbsPersonTypeKor());
                    pstmt.setString(39, syncUser.getEbsUserStatusEng());
                    pstmt.setString(40, syncUser.getEbsUserStatusKor());
                    pstmt.setString(41, syncUser.getPositionEng());
                    pstmt.setString(42, syncUser.getPositionKor());
                    pstmt.setString(43, syncUser.getStuStatusEng());
                    pstmt.setString(44, syncUser.getStuStatusKor());
                    pstmt.setString(45, syncUser.getAcadProgCode());
                    pstmt.setString(46, syncUser.getAcadProgKor());
                    pstmt.setString(47, syncUser.getAcadProgEng());
                    pstmt.setString(48, syncUser.getPersonTypeCodeUid());
                    pstmt.setDate(49, syncUser.getProgEffdt());
                    pstmt.setString(50, syncUser.getStdntTypeId());
                    pstmt.setString(51, syncUser.getStdntTypeClass());
                    pstmt.setString(52, syncUser.getStdntCategoryId());
                    pstmt.setString(53, syncUser.getAdvrEbsPersonId());
                    pstmt.setString(54, syncUser.getAdvrName());
                    pstmt.setString(55, syncUser.getAdvrNameAc());
                    pstmt.setDate(56, syncUser.getEntranceDate());
                    pstmt.setDate(57, syncUser.getResignDate());
                    pstmt.setDate(58, syncUser.getProgStartDate());
                    pstmt.setDate(59, syncUser.getProgEndDate());
                    pstmt.setString(60, syncUser.getKaistSuid());
                    pstmt.setString(61, syncUser.getAdvrKaistUid());
                    pstmt.setString(62, syncUser.getActionType());
                    pstmt.setString(63, syncUser.getActionStatus());
                    pstmt.setTimestamp(64, syncUser.getCreatedAt());
                    pstmt.setString(65, syncUser.getCreatorId());
                    pstmt.setString(66, syncUser.getUpdatorId());
                    pstmt.setTimestamp(67, syncUser.getUpdatedAt());
                    pstmt.setTimestamp(68, syncUser.getAppliedAt());
                    pstmt.setString(69, syncUser.getErrorMessage());

                    pstmt.addBatch();
                    pstmt.clearParameters();

                    if ((index % 1000) == 0) {
                        pstmt.executeBatch();
                        pstmt.clearBatch();
                        con.commit();
                    }
                }
                pstmt.executeBatch();
                con.commit();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    protected SyncUser convertToSyncUserAsIs(Object obj, Timestamp now) {

        SyncUser syncUser = null;

        if (obj != null) {
            syncUser = new SyncUser();
            syncUser.setOid(IdGenerator.getUUID());
        } else {
            return null;
        }

        if (obj instanceof SourceUser) {
            SourceUser sourceUser = (SourceUser) obj;

            syncUser.setKaistUid(sourceUser.getKaistUid());
            syncUser.setId(sourceUser.getSsoId());
            syncUser.setKoreanName(sourceUser.getNameAc());
            syncUser.setEnglishName(sourceUser.getName());
            syncUser.setLastName(sourceUser.getLastName());
            syncUser.setFirstName(sourceUser.getFirstName());
            syncUser.setBirthday(DateUtil_KAIST.getKaistStringToDate(sourceUser.getBirthdate()));
            syncUser.setNationCodeUid(sourceUser.getCountry());
            syncUser.setSexCodeUid(sourceUser.getSex());
            syncUser.setEmailAddress(sourceUser.getEmailAddr());
            syncUser.setChMail(sourceUser.getChMail());
            syncUser.setMobileTelephoneNumber(sourceUser.getCellPhone());
            syncUser.setOfficeTelephoneNumber(sourceUser.getBusnPhone());
            syncUser.setOweHomeTelephoneNumber(sourceUser.getHomePhone());
            syncUser.setFaxTelephoneNumber(sourceUser.getFaxPhone());
            syncUser.setPostNumber(sourceUser.getPostal());
            syncUser.setAddress(sourceUser.getAddress1());
            syncUser.setAddressDetail(sourceUser.getAddress2());
            syncUser.setPersonId(sourceUser.getEbsPersonid());
            syncUser.setEmployeeNumber(sourceUser.getEmployeeNumber());
            syncUser.setStdNo(sourceUser.getStdNo());
            syncUser.setAcadOrg(sourceUser.getAcadOrg());
            syncUser.setAcadName(sourceUser.getAcadName());
            syncUser.setAcadKstOrgId(sourceUser.getAcadKstOrgId());
            syncUser.setAcadEbsOrgId(sourceUser.getAcadEbsOrgId());
            syncUser.setAcadEbsOrgNameEng(sourceUser.getAcadEbsOrgNameEng());
            syncUser.setAcadEbsOrgNameKor(sourceUser.getAcadEbsOrgNameKor());
            syncUser.setCampusUid(sourceUser.getCampus());
            syncUser.setEbsOrganizationId(sourceUser.getEbsOrganizationId());
            syncUser.setEbsOrgNameEng(sourceUser.getEbsOrgNameEng());
            syncUser.setEbsOrgNameKor(sourceUser.getEbsOrgNameKor());
            syncUser.setEbsGradeNameEng(sourceUser.getEbsGradeNameEng());
            syncUser.setEbsGradeNameKor(sourceUser.getEbsGradeNameKor());
            syncUser.setEbsGradeLevelEng(sourceUser.getEbsGradeLevelEng());
            syncUser.setEbsGradeLevelKor(sourceUser.getEbsGradeLevelKor());
            syncUser.setEbsPersonTypeEng(sourceUser.getEbsPersonTypeEng());
            syncUser.setEbsPersonTypeKor(sourceUser.getEbsPersonTypeKor());
            syncUser.setEbsUserStatusEng(sourceUser.getEbsUserStatusEng());
            syncUser.setEbsUserStatusKor(sourceUser.getEbsUserStatusKor());
            syncUser.setPositionEng(sourceUser.getPositionEng());
            syncUser.setPositionKor(sourceUser.getPositionKor());
            syncUser.setStuStatusEng(sourceUser.getStuStatusEng());
            syncUser.setStuStatusKor(sourceUser.getStuStatusKor());
            syncUser.setAcadProgCode(sourceUser.getAcadProgCode());
            syncUser.setAcadProgEng(sourceUser.getAcadProgEng());
            syncUser.setAcadProgKor(sourceUser.getAcadProgKor());
            syncUser.setPersonTypeCodeUid(sourceUser.getPersonGubun());
            syncUser.setProgEffdt(DateUtil_KAIST.getKaistStringToDate(sourceUser.getProgEffdt()));
            syncUser.setStdntTypeId(sourceUser.getStdntTypeId());
            syncUser.setStdntTypeClass(sourceUser.getStdntTypeClass());
            syncUser.setStdntCategoryId(sourceUser.getStdntCategoryId());
            syncUser.setAdvrEbsPersonId(sourceUser.getAdvrEbsPersonId());
            syncUser.setAdvrName(sourceUser.getAdvrName());
            syncUser.setAdvrNameAc(sourceUser.getAdvrNameAc());
            syncUser.setEntranceDate(DateUtil_KAIST.getKaistStringToDate(sourceUser.getEbsStartDate()));
            syncUser.setResignDate(DateUtil_KAIST.getKaistStringToDate(sourceUser.getEbsEndDate()));
            syncUser.setProgStartDate(DateUtil_KAIST.getKaistStringToDate(sourceUser.getProgStartDate()));
            syncUser.setProgEndDate(DateUtil_KAIST.getKaistStringToDate(sourceUser.getProgEndDate()));
            syncUser.setKaistSuid(sourceUser.getKaistSuid());
            syncUser.setAdvrKaistUid(sourceUser.getAdvrKaistUid());

        } else if (obj instanceof UserDetail) {

            UserDetail userdetail = (UserDetail) obj;

            syncUser.setKaistUid(userdetail.getKaistUid());
            syncUser.setId(userdetail.getUserId());
            syncUser.setKoreanName(userdetail.getKoreanName());
            syncUser.setEnglishName(userdetail.getEnglishName());
            syncUser.setLastName(userdetail.getLastName());
            syncUser.setFirstName(userdetail.getFirstName());
            syncUser.setBirthday(userdetail.getBirthday());
            syncUser.setNationCodeUid(userdetail.getNationCodeUid());
            syncUser.setSexCodeUid(userdetail.getSexCodeUid());
            syncUser.setEmailAddress(userdetail.getEmailAddress());
            syncUser.setChMail(userdetail.getChMail());
            syncUser.setMobileTelephoneNumber(userdetail.getMobileTelephoneNumber());
            syncUser.setOfficeTelephoneNumber(userdetail.getOfficeTelephoneNumber());
            syncUser.setOweHomeTelephoneNumber(userdetail.getOweHomeTelephoneNumber());
            syncUser.setFaxTelephoneNumber(userdetail.getFaxTelephoneNumber());
            syncUser.setPostNumber(userdetail.getPostNumber());
            syncUser.setAddress(userdetail.getAddress());
            syncUser.setAddressDetail(userdetail.getAddressDetail());
            syncUser.setPersonId(userdetail.getPersonId());
            syncUser.setEmployeeNumber(userdetail.getEmployeeNumber());
            syncUser.setStdNo(userdetail.getStdNo());
            syncUser.setAcadOrg(userdetail.getAcadOrg());
            syncUser.setAcadName(userdetail.getAcadName());
            syncUser.setAcadKstOrgId(userdetail.getAcadKstOrgId());
            syncUser.setAcadEbsOrgId(userdetail.getAcadEbsOrgId());
            syncUser.setAcadEbsOrgNameEng(userdetail.getAcadEbsOrgNameEng());
            syncUser.setAcadEbsOrgNameKor(userdetail.getAcadEbsOrgNameKor());
            syncUser.setCampusUid(userdetail.getCampusUid());
            syncUser.setEbsOrganizationId(userdetail.getEbsOrganizationId());
            syncUser.setEbsOrgNameEng(userdetail.getEbsOrgNameEng());
            syncUser.setEbsOrgNameKor(userdetail.getEbsOrgNameKor());
            syncUser.setEbsGradeNameEng(userdetail.getEbsGradeNameEng());
            syncUser.setEbsGradeNameKor(userdetail.getEbsGradeNameKor());
            syncUser.setEbsGradeLevelEng(userdetail.getEbsGradeLevelEng());
            syncUser.setEbsGradeLevelKor(userdetail.getEbsGradeLevelKor());
            syncUser.setEbsPersonTypeEng(userdetail.getEbsPersonTypeEng());
            syncUser.setEbsPersonTypeKor(userdetail.getEbsPersonTypeKor());
            syncUser.setEbsUserStatusEng(userdetail.getEbsUserStatusEng());
            syncUser.setEbsUserStatusKor(userdetail.getEbsUserStatusKor());
            syncUser.setPositionEng(userdetail.getPositionEng());
            syncUser.setPositionKor(userdetail.getPositionKor());
            syncUser.setStuStatusEng(userdetail.getStuStatusEng());
            syncUser.setStuStatusKor(userdetail.getStuStatusKor());
            syncUser.setAcadProgCode(userdetail.getAcadProgCode());
            syncUser.setAcadProgEng(userdetail.getAcadProgEng());
            syncUser.setAcadProgKor(userdetail.getAcadProgKor());
            syncUser.setPersonTypeCodeUid(userdetail.getPersonTypeCodeUid());
            syncUser.setProgEffdt(userdetail.getProgEffdt());
            syncUser.setStdntTypeId(userdetail.getStdntTypeId());
            syncUser.setStdntTypeClass(userdetail.getStdntTypeClass());
            syncUser.setStdntCategoryId(userdetail.getStdntCategoryId());
            syncUser.setAdvrEbsPersonId(userdetail.getAdvrEbsPersonId());
            syncUser.setAdvrName(userdetail.getAdvrName());
            syncUser.setAdvrNameAc(userdetail.getAdvrNameAc());
            syncUser.setEntranceDate(userdetail.getEntranceDate());
            syncUser.setResignDate(userdetail.getResignDate());
            syncUser.setProgStartDate(userdetail.getProgStartDate());
            syncUser.setProgEndDate(userdetail.getProgEndDate());
            syncUser.setKaistSuid(userdetail.getKaistSuid());
            syncUser.setAdvrKaistUid(userdetail.getAdvrKaistUid());

        }

        syncUser.setActionStatus(SyncOrgConstants_KAIST.ACTION_STATUS_WAIT);
        syncUser.setCreatedAt(now);

        return syncUser;
    }

    protected void compareModifiedField(String sourceValue, String targetValue, String key, StringBuffer sb) {
        if (sourceValue != null && targetValue != null) {
            if (sourceValue.equals(targetValue) == false) {
                sb.append(key).append(";;").append(targetValue).append(";;").append(sourceValue).append("|");
            }
        } else if ((sourceValue == null || sourceValue.length() == 0)
                && (targetValue == null || targetValue.length() == 0)) {
            // Nothing do
        } else {
            sb.append(key).append(";;").append(targetValue).append(";;").append(sourceValue).append("|");
        }
    }

    protected void compareModifiedField(String sourceValue, Date targetValue, String key, StringBuffer sb) {
        if (sourceValue != null && targetValue != null) {
            if (sourceValue.equals(DateUtil_KAIST.getKaistDateToString(targetValue)) == false) {
                sb.append(key).append(";;").append(targetValue).append(";;").append(sourceValue).append("|");
            }
        } else if ((sourceValue == null || sourceValue.length() == 0) && targetValue == null) {
            // Nothing do
        } else {
            sb.append(key).append(";;").append(targetValue).append(";;").append(sourceValue).append("|");
        }
    }

    protected String findModifiedUserFiledsAsIs(SourceUser sourceUser, UserDetail userDetail) {

        StringBuffer sb = new StringBuffer();

        compareModifiedField(sourceUser.getNameAc(), userDetail.getKoreanName(), "Name", sb);
        compareModifiedField(sourceUser.getName(), userDetail.getEnglishName(), "EnglishName", sb);
        compareModifiedField(sourceUser.getFirstName(), userDetail.getFirstName(), "FirstName", sb);
        compareModifiedField(sourceUser.getFirstName(), userDetail.getFirstName(), "FirstName", sb);
        compareModifiedField(sourceUser.getLastName(), userDetail.getLastName(), "LastName", sb);
        compareModifiedField(sourceUser.getBirthdate(), userDetail.getBirthday(), "Birthday", sb);
        compareModifiedField(sourceUser.getCountry(), userDetail.getNationCodeUid(), "Country", sb);
        compareModifiedField(sourceUser.getSex(), userDetail.getSexCodeUid(), "Sex", sb);
        compareModifiedField(sourceUser.getEmailAddr(), userDetail.getEmailAddress(), "EmailAddress", sb);
        compareModifiedField(sourceUser.getChMail(), userDetail.getChMail(), "ChMail", sb);
        compareModifiedField(sourceUser.getCellPhone(), userDetail.getMobileTelephoneNumber(), "Handphone", sb);
        compareModifiedField(sourceUser.getHomePhone(), userDetail.getOweHomeTelephoneNumber(), "HomePhone", sb);
        compareModifiedField(sourceUser.getBusnPhone(), userDetail.getOfficeTelephoneNumber(), "BusinessPhone", sb);
        compareModifiedField(sourceUser.getFaxPhone(), userDetail.getFaxTelephoneNumber(), "FaxPhone", sb);
        compareModifiedField(sourceUser.getPostal(), userDetail.getPostNumber(), "PostNumber", sb);
        compareModifiedField(sourceUser.getAddress1(), userDetail.getAddress(), "Address", sb);
        compareModifiedField(sourceUser.getAddress2(), userDetail.getAddressDetail(), "AddressDetail", sb);
        compareModifiedField(sourceUser.getEbsPersonid(), userDetail.getPersonId(), "EbsPersonId", sb);
        compareModifiedField(sourceUser.getEmployeeNumber(), userDetail.getEmployeeNumber(), "EmployeeNumber", sb);
        compareModifiedField(sourceUser.getStdNo(), userDetail.getStdNo(), "StdNo", sb);
        compareModifiedField(sourceUser.getAcadOrg(), userDetail.getAcadOrg(), "AcadOrg", sb);
        compareModifiedField(sourceUser.getAcadName(), userDetail.getAcadName(), "AcadName", sb);
        compareModifiedField(sourceUser.getAcadKstOrgId(), userDetail.getAcadKstOrgId(), "AcadKstOrgId", sb);
        compareModifiedField(sourceUser.getAcadEbsOrgId(), userDetail.getAcadEbsOrgId(), "AcadEbsOrgId", sb);
        compareModifiedField(sourceUser.getAcadEbsOrgNameEng(), userDetail.getAcadEbsOrgNameEng(), "AcadEbsOrgNameEng",
                sb);
        compareModifiedField(sourceUser.getAcadEbsOrgNameKor(), userDetail.getAcadEbsOrgNameKor(), "AcadEbsOrgNameKor",
                sb);
        compareModifiedField(sourceUser.getCampus(), userDetail.getCampusUid(), "Campus", sb);
        compareModifiedField(sourceUser.getEbsOrganizationId(), userDetail.getEbsOrganizationId(), "EbsOrganizationId",
                sb);
        compareModifiedField(sourceUser.getEbsOrgNameEng(), userDetail.getEbsOrgNameEng(), "EbsOrgNameEng", sb);
        compareModifiedField(sourceUser.getEbsOrgNameKor(), userDetail.getEbsOrgNameKor(), "EbsOrgNameKor", sb);
        compareModifiedField(sourceUser.getEbsGradeNameEng(), userDetail.getEbsGradeNameEng(), "EbsGradeNameEng", sb);
        compareModifiedField(sourceUser.getEbsGradeNameKor(), userDetail.getEbsGradeNameKor(), "EbsGradeNameKor", sb);
        compareModifiedField(sourceUser.getEbsGradeLevelEng(), userDetail.getEbsGradeLevelEng(), "EbsGradeLevelEng",
                sb);
        compareModifiedField(sourceUser.getEbsGradeLevelKor(), userDetail.getEbsGradeLevelKor(), "EbsGradeLevelKor",
                sb);
        compareModifiedField(sourceUser.getEbsPersonTypeEng(), userDetail.getEbsPersonTypeEng(), "EbsPersonTypeEng",
                sb);
        compareModifiedField(sourceUser.getEbsPersonTypeKor(), userDetail.getEbsPersonTypeKor(), "EbsPersonTypeKor",
                sb);
        compareModifiedField(sourceUser.getEbsPersonTypeEng(), userDetail.getEbsPersonTypeEng(), "EbsPersonTypeEng",
                sb);
        compareModifiedField(sourceUser.getEbsUserStatusEng(), userDetail.getEbsUserStatusEng(), "EbsUserStatusEng",
                sb);
        compareModifiedField(sourceUser.getEbsUserStatusKor(), userDetail.getEbsUserStatusKor(), "EbsUserStatusKor",
                sb);
        compareModifiedField(sourceUser.getPositionEng(), userDetail.getPositionEng(), "PositionEng", sb);
        compareModifiedField(sourceUser.getPositionKor(), userDetail.getPositionKor(), "PositionKor", sb);
        compareModifiedField(sourceUser.getStuStatusEng(), userDetail.getStuStatusEng(), "StuStatusEng", sb);
        compareModifiedField(sourceUser.getStuStatusKor(), userDetail.getStuStatusKor(), "StuStatusKor", sb);
        compareModifiedField(sourceUser.getStuStatusEng(), userDetail.getStuStatusEng(), "StuStatusEng", sb);
        compareModifiedField(sourceUser.getAcadProgCode(), userDetail.getAcadProgCode(), "AcadProgCode", sb);
        compareModifiedField(sourceUser.getAcadProgKor(), userDetail.getAcadProgKor(), "AcadProgKor", sb);
        compareModifiedField(sourceUser.getAcadProgEng(), userDetail.getAcadProgEng(), "AcadProgEng", sb);
        compareModifiedField(sourceUser.getPersonGubun(), userDetail.getPersonTypeCodeUid(), "PersonGubun", sb);
        compareModifiedField(sourceUser.getStdntTypeId(), userDetail.getStdntTypeId(), "StdntTypeId", sb);
        compareModifiedField(sourceUser.getStdntTypeClass(), userDetail.getStdntTypeClass(), "StdntTypeClass", sb);
        compareModifiedField(sourceUser.getStdntCategoryId(), userDetail.getStdntCategoryId(), "StdntCategoryId", sb);
        compareModifiedField(sourceUser.getAdvrEbsPersonId(), userDetail.getAdvrEbsPersonId(), "AdvrEbsPersonId", sb);
        compareModifiedField(sourceUser.getAdvrName(), userDetail.getAdvrName(), "AdvrName", sb);
        compareModifiedField(sourceUser.getAdvrNameAc(), userDetail.getAdvrNameAc(), "AdvrNameAc", sb);
        compareModifiedField(sourceUser.getEbsEndDate(), userDetail.getResignDate(), "EbsEndDate", sb);
        compareModifiedField(sourceUser.getProgStartDate(), userDetail.getProgStartDate(), "ProgStartDate", sb);
        compareModifiedField(sourceUser.getProgEndDate(), userDetail.getProgEndDate(), "ProgEndDate", sb);
        compareModifiedField(sourceUser.getKaistSuid(), userDetail.getKaistSuid(), "KaistSUid", sb);

        if (sourceUser.getProgEffdt() != null && userDetail.getProgEffdt() != null) {
            if (sourceUser.getProgEffdt()
                    .equals(DateUtil_KAIST.getKaistTimestampToString(userDetail.getProgEffdt())) == false) {
                sb.append("ProgEffdt").append(";;").append(userDetail.getProgEffdt()).append(";;")
                        .append(sourceUser.getProgEffdt()).append("|");
            }
        } else if ((sourceUser.getProgEffdt() == null || sourceUser.getProgEffdt().length() == 0)
                && userDetail.getProgEffdt() == null) {
            // Nothing do
        } else {
            sb.append("ProgEffdt").append(";;").append(userDetail.getProgEffdt()).append(";;")
                    .append(sourceUser.getProgEffdt()).append("|");
        }

        return sb.toString();
    }

    protected void syncUsersAsIs(Timestamp now) {
        SyncUserCondition condition = new SyncUserCondition();
        condition.createCriteria().andActionStatusEqualTo(SyncOrgConstants_KAIST.ACTION_STATUS_WAIT);
        condition.setOrderByClause("SEQ");

        List<SyncUser> syncUsers = new ArrayList<>();

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            sqlSession.select("com.bandi.dao.SyncUserMapper.selectByCondition", condition,
                    new ResultHandler<SyncUser>() {

                        @Override
                        public void handleResult(ResultContext<? extends SyncUser> context) {
                            SyncUser syncUser = context.getResultObject();

                            try {
                                UserDetail userDetail = null;
                                User user = null;

                                if (SyncOrgConstants_KAIST.ACTION_TYPE_INSERT.equals(syncUser.getActionType())) {
                                    userDetail = convertToUserDetailAsIs(syncUser,
                                            SyncOrgConstants_KAIST.ACTION_TYPE_INSERT);

                                    if (isExistSsoId(syncUser)) {
                                        user = convertToUserAsIs(syncUser, SyncOrgConstants_KAIST.ACTION_TYPE_INSERT);
                                    }

                                    try {
                                        userDetailService.insert(userDetail);
                                        if (isExistSsoId(syncUser)) {
                                            userService.insertSync(user);
                                        }
                                    } catch (Exception e) {
                                        throw e;
                                    }

                                } else if (SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE.equals(syncUser.getActionType())) {
                                    userDetail = convertToUserDetailAsIs(syncUser,
                                            SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE);

                                    if (isExistSsoId(syncUser)) {
                                        user = convertToUserAsIs(syncUser, SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE);
                                    }

                                    try {
                                        userDetailService.update(userDetail);

                                        if (isExistSsoId(syncUser)) {
                                            userService.update(user);
                                        }
                                    } catch (Exception e) {
                                        throw e;
                                    }

                                } else if (SyncOrgConstants_KAIST.ACTION_TYPE_MOVE.equals(syncUser.getActionType())) {
                                    userDetail = convertToUserDetailAsIs(syncUser,
                                            SyncOrgConstants_KAIST.ACTION_TYPE_MOVE);

                                    if (isExistSsoId(syncUser)) {
                                        user = convertToUserAsIs(syncUser, SyncOrgConstants_KAIST.ACTION_TYPE_MOVE);
                                    }

                                    try {
                                        userDetailService.update(userDetail);

                                        if (isExistSsoId(syncUser)) {
                                            userService.move(user, syncUser.getEbsOrganizationId());
                                        }
                                    } catch (Exception e) {
                                        throw e;
                                    }
                                }
                                syncUser.setActionStatus(SyncOrgConstants_KAIST.ACTION_STATUS_FINISH);

                            } catch (Exception e) {

                                syncUser.setActionStatus(SyncOrgConstants_KAIST.ACTION_STATUS_ERROR);
                                syncUser.setErrorMessage(syncUser.getErrorMessage() + "|" + e.getMessage());

                                if (syncUser.getErrorMessage() != null && syncUser.getErrorMessage().length() > 6000) {
                                    syncUser.setErrorMessage(syncUser.getErrorMessage().substring(0, 6000));
                                }

                                e.printStackTrace();

                            } finally {
                                syncUser.setAppliedAt(now);
                                syncUsers.add(syncUser);
                            }
                        }
                    });
        }

        String sql = "";
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            sql = sqlSession.getConfiguration().getMappedStatement("com.bandi.dao.SyncUserMapper.updateStatusBatch")
                    .getBoundSql(new Object()).getSql();
        }

        try (Connection con = dataSource.getConnection()) {
            try (PreparedStatement pstmt = con.prepareStatement(sql)) {
                int index = 0;

                for (SyncUser syncUser : syncUsers) {
                    index++;

                    pstmt.setString(1, syncUser.getActionStatus());
                    pstmt.setTimestamp(2, syncUser.getAppliedAt());
                    pstmt.setString(3, syncUser.getErrorMessage());
                    pstmt.setString(4, syncUser.getOid());

                    pstmt.addBatch();
                    pstmt.clearParameters();

                    if ((index % 1000) == 0) {
                        pstmt.executeBatch();
                        pstmt.clearBatch();
                        con.commit();
                    }
                }
                pstmt.executeBatch();
                con.commit();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    protected User convertToUserAsIs(SyncUser syncUser, String actionType) {
        User user = null;
        User userFromDB = null;

        if (syncUser != null) {
            user = new User();

            userFromDB = userService.getFromDb(syncUser.getId());

            if (SyncOrgConstants_KAIST.ACTION_TYPE_INSERT.equals(actionType)) {
                user.setKaistUid(syncUser.getKaistUid());
                user.setId(syncUser.getId());
                user.setPassword(syncUser.getId());
                user.setPasswordInitializeFlag(BandiConstants.FLAG_N);
                user.setGroupId(syncUser.getEbsOrganizationId());
            }

            if (SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE.equals(actionType)
                    || SyncOrgConstants_KAIST.ACTION_TYPE_MOVE.equals(actionType)) {
                user = userFromDB;

            }

            user.setName(syncUser.getKoreanName());
            user.setEmail(syncUser.getEmailAddress());
            if (user.getEmail() == null) {
                if (syncUser.getChMail() != null) {
                    user.setEmail(syncUser.getChMail());
                } else {
                    user.setEmail("");
                }
            }
            user.setHandphone(syncUser.getMobileTelephoneNumber());
            if (user.getHandphone() == null) {
                user.setHandphone("");
            }

            user.setUserType(syncUser.getPersonTypeCodeUid());
            user.setFlagSync(BandiConstants.FLAG_Y);
        }

        return user;

    }

    protected UserDetail convertToUserDetailAsIs(SyncUser syncUser, String actionType) {
        UserDetail userDetail = null;

        if (syncUser != null) {

            if (SyncOrgConstants_KAIST.ACTION_TYPE_INSERT.equals(actionType)) {
                userDetail = new UserDetail();
                userDetail.setKaistUid(syncUser.getKaistUid());
                userDetail.setUserId(syncUser.getId());
            } else if (SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE.equals(actionType)
                    || SyncOrgConstants_KAIST.ACTION_TYPE_MOVE.equals(actionType)) {
                userDetail = userDetailService.get(syncUser.getKaistUid());
            }

            userDetail.setKoreanName(syncUser.getKoreanName());
            userDetail.setEnglishName(syncUser.getEnglishName());
            userDetail.setLastName(syncUser.getLastName());
            userDetail.setFirstName(syncUser.getFirstName());
            userDetail.setBirthday(syncUser.getBirthday());
            userDetail.setNationCodeUid(syncUser.getNationCodeUid());
            userDetail.setSexCodeUid(syncUser.getSexCodeUid());
            userDetail.setEmailAddress(syncUser.getEmailAddress());
            userDetail.setChMail(syncUser.getChMail());
            userDetail.setMobileTelephoneNumber(syncUser.getMobileTelephoneNumber());
            userDetail.setOfficeTelephoneNumber(syncUser.getOfficeTelephoneNumber());
            userDetail.setOweHomeTelephoneNumber(syncUser.getOweHomeTelephoneNumber());
            userDetail.setFaxTelephoneNumber(syncUser.getFaxTelephoneNumber());
            userDetail.setPostNumber(syncUser.getPostNumber());
            userDetail.setAddress(syncUser.getAddress());
            userDetail.setAddressDetail(syncUser.getAddressDetail());
            userDetail.setPersonId(syncUser.getPersonId());
            userDetail.setEmployeeNumber(syncUser.getEmployeeNumber());
            userDetail.setStdNo(syncUser.getStdNo());
            userDetail.setAcadOrg(syncUser.getAcadOrg());
            userDetail.setAcadName(syncUser.getAcadName());
            userDetail.setAcadKstOrgId(syncUser.getAcadKstOrgId());
            userDetail.setAcadEbsOrgId(syncUser.getAcadEbsOrgId());
            userDetail.setAcadEbsOrgNameEng(syncUser.getAcadEbsOrgNameEng());
            userDetail.setAcadEbsOrgNameKor(syncUser.getAcadEbsOrgNameKor());
            userDetail.setCampusUid(syncUser.getCampusUid());
            userDetail.setEbsOrganizationId(syncUser.getEbsOrganizationId());
            userDetail.setEbsOrgNameEng(syncUser.getEbsOrgNameEng());
            userDetail.setEbsOrgNameKor(syncUser.getEbsOrgNameKor());
            userDetail.setEbsGradeNameEng(syncUser.getEbsGradeNameEng());
            userDetail.setEbsGradeNameKor(syncUser.getEbsGradeNameKor());
            userDetail.setEbsGradeLevelEng(syncUser.getEbsGradeLevelEng());
            userDetail.setEbsGradeLevelKor(syncUser.getEbsGradeLevelKor());
            userDetail.setEbsPersonTypeEng(syncUser.getEbsPersonTypeEng());
            userDetail.setEbsPersonTypeKor(syncUser.getEbsPersonTypeKor());
            userDetail.setEbsUserStatusEng(syncUser.getEbsUserStatusEng());
            userDetail.setEbsUserStatusKor(syncUser.getEbsUserStatusKor());
            userDetail.setPositionEng(syncUser.getPositionEng());
            userDetail.setPositionKor(syncUser.getPositionKor());
            userDetail.setStuStatusEng(syncUser.getStuStatusEng());
            userDetail.setStuStatusKor(syncUser.getStuStatusKor());
            userDetail.setAcadProgCode(syncUser.getAcadProgCode());
            userDetail.setAcadProgKor(syncUser.getAcadProgKor());
            userDetail.setAcadProgEng(syncUser.getAcadProgEng());
            userDetail.setPersonTypeCodeUid(syncUser.getPersonTypeCodeUid());
            userDetail.setProgEffdt(syncUser.getProgEffdt());
            userDetail.setStdntTypeId(syncUser.getStdntTypeId());
            userDetail.setStdntTypeClass(syncUser.getStdntTypeClass());
            userDetail.setStdntCategoryId(syncUser.getStdntCategoryId());
            userDetail.setAdvrEbsPersonId(syncUser.getAdvrEbsPersonId());
            userDetail.setAdvrName(syncUser.getAdvrName());
            userDetail.setAdvrNameAc(syncUser.getAdvrNameAc());
            userDetail.setEntranceDate(syncUser.getEntranceDate());
            userDetail.setResignDate(syncUser.getResignDate());
            userDetail.setProgStartDate(syncUser.getProgStartDate());
            userDetail.setProgEndDate(syncUser.getProgEndDate());
            userDetail.setKaistSuid(syncUser.getKaistSuid());
            userDetail.setAdvrKaistUid(syncUser.getAdvrKaistUid());
        }

        return userDetail;
    }

    protected boolean isExistSsoId(SyncUser syncUser) {
        if ((syncUser.getId() == null || syncUser.getId().length() == 0)) {
            return false;
        }
        return true;
    }

    protected boolean isParentIdEqualsKaistRootId(String sourceGroupParentId, String groupParentId) {
        if (SourceGroup.SOURCE_ROOT_OID.equals(sourceGroupParentId) && Group.GROUP_ROOT_OID.equals(groupParentId)) {
            return true;
        }
        return false;
    }

    protected void syncConcurrentUsersAsIs() {
        /*
         * 겸직 테이블을 돌며 BDSUSER 테이블에 없는 겸직 사용자를 등록한다.
         * 1. 원직 및 겸직 그룹 정보가 BDSUSER에 있는 경우는 제외한다.
         * 2. PERSON 데이터에 PERSON_ID가 없는 겸직 정보는 제외한다.
         * 3. 겸직 부서가 BDSGROUP에 없는 경우는 제외한다.
         * 4. 한 사용자가 동일한 부서에 겸직된 정보 ROW가 2건 이상일 경우, 한번만 INSERT 한다.
         * 5. 겸직 테이블 뷰 생성
         * 오류로 컬럼의 데이터 주의
         * 실제 person_id 컬럼 > 뷰 organization_id 컬럼 / 실제 organization_id > 뷰 job_id 컬럼
         */

        try (SqlSession orgSqlSession = orgSyncSqlSessionFactory.openSession()) {
            orgSqlSession.select("com.bandi.service.orgSync.source.SourceConcurrentUserMapper.getAll",
                    new ResultHandler<SourceConcurrentUser>() {
                        @Override
                        public void handleResult(ResultContext<? extends SourceConcurrentUser> context) {
                            SourceConcurrentUser sourceConcurrentUser = context.getResultObject();

                            if (sourceConcurrentUser.getEmployeeNumber() != null) {
                                UserDetail userdetail = userDetailDao
                                        .selectByPersonId(String.valueOf(sourceConcurrentUser.getOrganizationId()));

                                if (userdetail != null) {

                                    UserCondition userCondition = new UserCondition();
                                    userCondition.createCriteria().andKaistUidEqualTo(userdetail.getKaistUid())
                                            .andGroupIdEqualTo(String.valueOf(sourceConcurrentUser.getJobId()));

                                    List<User> list = userService.selectByCondition(userCondition);

                                    if (list == null || list.size() == 0) {
                                        Group group = groupService.get(String.valueOf(sourceConcurrentUser.getJobId()));

                                        if (group != null) {
                                            // 겸직 사용자로 INSERT
                                            User concurrentUser = convertToConcurrentUser(sourceConcurrentUser,
                                                    userdetail.getKaistUid());

                                            if (concurrentUser != null) {
                                                userService.insertConcurrent(concurrentUser);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    });
        }

        /* 겸직 테이블에서 삭제 및 제외된 사용자는 BDSUSER ROW를 삭제한다. */
        UserCondition userCondition = new UserCondition();
        userCondition.createCriteria().andIdNotEqualGenericId();

        List<SourceConcurrentUser> sourceConcurrentUser = sourceConcurrentUserDao.getAll();

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            sqlSession.select("com.bandi.dao.UserMapper.selectByCondition", userCondition, new ResultHandler<User>() {
                @Override
                public void handleResult(ResultContext<? extends User> context) {
                    User user = context.getResultObject();
                    boolean excludeDelete = false;

                    UserDetail userDetail = userDetailService.get(user.getKaistUid());

                    for (SourceConcurrentUser sourceConcurrentUser : sourceConcurrentUser) {
                        if (userDetail.getPersonId().equals(String.valueOf(sourceConcurrentUser.getOrganizationId()))
                                && user.getGroupId().equals(String.valueOf(sourceConcurrentUser.getJobId()))) {
                            excludeDelete = true;
                        }
                    }

                    if (excludeDelete == false) {
                        userService.deleteConcurrent(user.getId());
                    }
                }
            });
        }
    }

    protected void syncGroupHeadUserAsIs() {
        try (SqlSession orgSqlSession = orgSyncSqlSessionFactory.openSession()) {
            orgSqlSession.select("com.bandi.service.orgSync.source.SourceGroupHeadUserMapper.getAll",
                    new ResultHandler<SourceGroupHeadUser>() {
                        @Override
                        public void handleResult(ResultContext<? extends SourceGroupHeadUser> context) {
                            SourceGroupHeadUser sourceGroupHeadUser = context.getResultObject();

                            Group group = groupService.get(String.valueOf(sourceGroupHeadUser.getOrganizationId()));

                            // 부서 와 사용자 정보가 모두 존재하면, 부서장 정보를 BDSROLEMEMBER에 등록한다.
                            // KAIST-TODO 이혜련 : 한 부서에 부서장 정보가 2건 존재하는 부분은 어떻게 처리할지 몰라서 우선은 그냥 다 넣음.
                            // KAIST-TODO 이혜련: 한 사용자가  현재 속한 부서는 A인데, 부서장 테이블에는 B, C 부서장으로 지정된 경우에는 우선은 INSERT 한다.
                            if (group != null) {
                                UserDetail userdetail = userDetailDao.selectByPersonId(String.valueOf(sourceGroupHeadUser.getPersonId()));

                                if (userdetail != null && userdetail.getUserId() != null) {
                                    RoleMember rolemember = convertToRoleMemberAsIs(sourceGroupHeadUser, userdetail.getUserId());

                                    if (rolemember != null) {
                                        roleMemberService.insert(rolemember);
                                    }

                                }
                            }
                        }
                    });
        }


        /* 부서장 테이블에서 삭제 및 제외된 사용자는 BDSROLEMEMBER ROW를 삭제한다.*/
        List<SourceGroupHeadUser> sourceGroupHeadUsers = sourceGroupHeadUserDao.getAll();

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            sqlSession.select("com.bandi.dao.kaist.RoleMemberMapper.getGroupHeadSyncUser",
                    new ResultHandler<RoleMember>() {
                        @Override
                        public void handleResult(ResultContext<? extends RoleMember> context) {
                            RoleMember rolemember = context.getResultObject();
                            boolean excludeDelete = false;

                            User user = userService.getDetail(rolemember.getTargetObjectId());

                            for (SourceGroupHeadUser sourceGroupHeadUser : sourceGroupHeadUsers) {
                                if (user.getPersonId().equals(String.valueOf(sourceGroupHeadUser.getPersonId()))
                                        && rolemember.getGroupId().equals(String.valueOf(sourceGroupHeadUser.getOrganizationId()))) {
                                    excludeDelete = true;
                                }
                            }

                            if (excludeDelete == false) {
                                roleMemberService.delete(rolemember.getOid());
                            }

                        }
                    });
        }
    }

    protected RoleMember convertToRoleMemberAsIs(SourceGroupHeadUser sourceGroupHeadUser, String userId) {
        RoleMember rolemember = null;

        RoleMasterCondition condition = new RoleMasterCondition();
        condition.createCriteria().andRoleTypeEqualTo(RoleMaster.ROLE_TYPE_MANAGE)
                .andGroupIdEqualTo(String.valueOf(sourceGroupHeadUser.getOrganizationId()));

        // 부서장이 속한 부서의 '부서장 ROLE'을 찾는다.
        List<RoleMaster> rolemasters = roleMasterService.selectByCondition(condition);

        if (rolemasters != null && rolemasters.size() > 0) {
            // 부서장 ROLE
            RoleMaster rolemaster = rolemasters.get(0);

            // 중복검사
            boolean existRoleMember = roleMemberService.isRoleMemberExisted(rolemaster.getOid(), userId);

            if (existRoleMember == false) {
                rolemember = new RoleMember();

                rolemember.setRoleMasterOid(rolemaster.getOid());
                rolemember.setTargetObjectId(userId);
                rolemember.setTargetObjectType(BandiConstants_KAIST.OBJECT_TYPE_USER);
                rolemember.setFlagSync(BandiConstants_KAIST.FLAG_Y);
            }
        }

        return rolemember;
    }

    protected User convertToConcurrentUser(SourceConcurrentUser sourceConcurrentUser, String kaistUid) {

        User originalUser = userDao.getOriginalUserByKaistUid(kaistUid);

        if (originalUser != null) {
            originalUser.setId(IdGenerator.getUUID());
            originalUser.setGroupId(String.valueOf(sourceConcurrentUser.getJobId()));
            originalUser.setOriginalGroupId(String.valueOf(sourceConcurrentUser.getJobId()));
        }

        return originalUser;
    }

    // KAIST-TODO 이혜련 : 09.16 기준정보로 조직도연동을 변경함. 공유 DB로 추후에 변경되어야 함.
    /*
     * protected void compareGroupsToBe(Timestamp now) { List<SyncGroup> syncGroups
     * = new ArrayList<>();
     *
     * try (SqlSession orgSqlSession = orgSyncSqlSessionFactory.openSession()) {
     *
     * orgSqlSession.select(
     * "com.bandi.service.orgSync.source.SourceGroupMapper.selectByHierarchical",
     * SourceGroup.SOURCE_ROOT_OID, new ResultHandler<SourceGroup>() {
     *
     * @Override public void handleResult(ResultContext<? extends SourceGroup>
     * context) { SourceGroup sourceGroup = context.getResultObject();
     *
     * String sourceGroupId = sourceGroup.getOrganizationUid(); SyncGroup syncGroup
     * = convertToSyncGroupToBe(sourceGroup, now);
     *
     * Group group = groupService.get(sourceGroupId);
     *
     * Date beginDate = sourceGroup.getBeginDate(); Date endDate =
     * sourceGroup.getEndDate();
     *
     * if (group == null) { if (DateUtil_KAIST.isCompareDateBetween(new
     * Date(now.getTime()), beginDate, endDate)) {
     * syncGroup.setActionType(SyncOrgConstants_KAIST.ACTION_TYPE_INSERT);
     * syncGroups.add(syncGroup); } } else { if
     * (DateUtil_KAIST.isBeforeDateFromToday(endDate)) { if
     * (Group.CONST_STATUS_ACTIVE.equals(group.getStatus())) {
     * syncGroup.setActionType(SyncOrgConstants_KAIST.ACTION_TYPE_DELETE);
     * syncGroups.add(syncGroup); } } else { String modifiedFileds =
     * findModifiedGroupFiledsToBe(sourceGroup, group);
     *
     * if (modifiedFileds.length() > 0) { syncGroup.setErrorMessage(modifiedFileds);
     *
     * if (sourceGroup.getUpperOrganizationUid().equals(group.getParentId()) ||
     * isParentIdEqualsKaistRootId(sourceGroup.getUpperOrganizationUid(),
     * group.getParentId())) {
     * syncGroup.setActionType(SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE); } else {
     * syncGroup.setActionType(SyncOrgConstants_KAIST.ACTION_TYPE_MOVE); }
     * syncGroups.add(syncGroup); } } }
     *
     * } }); }
     *
     * String sql = ""; try (SqlSession sqlSession =
     * sqlSessionFactory.openSession()) { sql =
     * sqlSession.getConfiguration().getMappedStatement(
     * "com.bandi.dao.SyncGroupMapper.insert") .getBoundSql(new Object()).getSql();
     * }
     *
     * try (Connection con = dataSource.getConnection()) { try (PreparedStatement
     * pstmt = con.prepareStatement(sql)) { int index = 0;
     *
     * for (SyncGroup syncGroup : syncGroups) { index++;
     *
     * pstmt.setString(1, syncGroup.getOid()); pstmt.setString(2,
     * syncGroup.getId()); pstmt.setString(3, syncGroup.getName());
     * pstmt.setString(4, syncGroup.getDescription()); pstmt.setInt(5,
     * syncGroup.getSortOrder()); pstmt.setString(6, syncGroup.getParentId());
     * pstmt.setString(7, syncGroup.getActionType()); pstmt.setString(8,
     * syncGroup.getActionStatus()); pstmt.setTimestamp(9,
     * syncGroup.getCreatedAt()); pstmt.setTimestamp(10, syncGroup.getAppliedAt());
     * pstmt.setString(11, syncGroup.getErrorMessage()); pstmt.setString(12,
     * syncGroup.getGroupCode()); pstmt.setString(13, syncGroup.getCreatorId());
     * pstmt.setString(14, syncGroup.getUpdatorId()); pstmt.setTimestamp(15,
     * syncGroup.getUpdatedAt());
     *
     * pstmt.addBatch(); pstmt.clearParameters();
     *
     * if ((index % 1000) == 0) { pstmt.executeBatch(); pstmt.clearBatch();
     * con.commit(); } } pstmt.executeBatch(); con.commit(); } } catch (SQLException
     * e) { e.printStackTrace(); }
     *
     * }
     */

    // KAIST-TODO 이혜련 : 09.16 기준정보로 조직도연동을 변경함. 공유 DB로 추후에 변경되어야 함.
    /*
     * protected void compareUsersToBe(Timestamp now) {
     *
     * List<SyncUser> syncUsers = new ArrayList<>();
     *
     * try (SqlSession orgSqlSession = orgSyncSqlSessionFactory.openSession()) {
     *
     * SourceUserCondition sourceUserCondition = new SourceUserCondition();
     * sourceUserCondition.createCriteria().andSsoUidIsNotNull().
     * andOrganizationUidIsNotNull();
     *
     * orgSqlSession.select(
     * "com.bandi.service.orgSync.source.SourceUserMapper.selectByCondition",
     * sourceUserCondition, new ResultHandler<SourceUser>() {
     *
     * @Override public void handleResult(ResultContext<? extends SourceUser>
     * context) { SourceUser sourceUser = context.getResultObject(); String
     * sourceUserId = sourceUser.getSsoUid();
     *
     * SyncUser syncUser = convertToSyncUserToBe(sourceUser, now);
     *
     * User user = userService.get(sourceUserId);
     *
     * // 08/05 기준 User가 INSERT 되는 경우 사라짐. (최초의 INSERT SELECT를 통해서 기존 회원들 INSERT
     * 시킴.) syncUser.setActionType(SyncOrgConstants_KAIST.ACTION_TYPE_INSERT);
     * syncUsers.add(syncUser);
     *
     * if (user != null) {
     *
     * String modifiedFileds = findModifiedUserFileds(sourceUser, user);
     *
     * if (modifiedFileds.length() > 0) { syncUser.setErrorMessage(modifiedFileds);
     *
     * if (sourceUser.getOrganizationUid().equals(user.getGroupId())) {
     * syncUser.setActionType(SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE); } else {
     * syncUser.setActionType(SyncOrgConstants_KAIST.ACTION_TYPE_MOVE); }
     * syncUsers.add(syncUser); } } } });
     *
     * }
     *
     * String sql = ""; try (SqlSession sqlSession =
     * sqlSessionFactory.openSession()) { sql =
     * sqlSession.getConfiguration().getMappedStatement(
     * "com.bandi.dao.SyncUserMapper.insert") .getBoundSql(new Object()).getSql(); }
     *
     * try (Connection con = dataSource.getConnection()) { try (PreparedStatement
     * pstmt = con.prepareStatement(sql)) { int index = 0;
     *
     * for (SyncUser syncUser : syncUsers) { index++;
     *
     * pstmt.setString(1, syncUser.getOid()); pstmt.setString(2, syncUser.getId());
     * pstmt.setString(3, syncUser.getName()); pstmt.setString(4,
     * syncUser.getKaistUid()); pstmt.setString(5, syncUser.getGroupId());
     * pstmt.setString(6, syncUser.getUserType()); pstmt.setString(7,
     * syncUser.getEmail()); pstmt.setString(8, syncUser.getHandphone());
     * pstmt.setString(9, syncUser.getActionType()); pstmt.setString(10,
     * syncUser.getActionStatus()); pstmt.setTimestamp(11, syncUser.getCreatedAt());
     * pstmt.setString(12, syncUser.getCreatorId()); pstmt.setString(13,
     * syncUser.getUpdatorId()); pstmt.setTimestamp(14, syncUser.getUpdatedAt());
     * pstmt.setTimestamp(15, syncUser.getAppliedAt()); pstmt.setString(16,
     * syncUser.getErrorMessage());
     *
     * pstmt.addBatch(); pstmt.clearParameters();
     *
     * if ((index % 1000) == 0) { pstmt.executeBatch(); pstmt.clearBatch();
     * con.commit(); } } pstmt.executeBatch(); con.commit(); } } catch (SQLException
     * e) { e.printStackTrace(); }
     *
     * }
     */

    // KAIST-TODO 이혜련 : 09.16 기준정보로 조직도연동을 변경함. 공유 DB로 추후에 변경되어야 함.
    /*
     * protected void syncGroupsToBe(Timestamp now) { List<SyncGroup> syncGroups =
     * new ArrayList<>();
     *
     * SyncGroupCondition condition = new SyncGroupCondition();
     * condition.createCriteria().andActionStatusEqualTo(SyncOrgConstants_KAIST.
     * ACTION_STATUS_WAIT); condition.setOrderByClause("SEQ");
     *
     * try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
     * sqlSession.select("com.bandi.dao.SyncGroupMapper.selectByCondition",
     * condition, new ResultHandler<SyncGroup>() {
     *
     * @Override public void handleResult(ResultContext<? extends SyncGroup>
     * context) { SyncGroup syncGroup = context.getResultObject();
     *
     * try { Group group = null; if
     * (SyncOrgConstants_KAIST.ACTION_TYPE_INSERT.equals(syncGroup.getActionType()))
     * { group = convertToGroupToBe(syncGroup,
     * SyncOrgConstants_KAIST.ACTION_TYPE_INSERT);
     *
     * try { groupService.insert(group); } catch (Exception e) { // 이름이 중복되었을 때의 처리
     * if (e instanceof BandiException && ErrorCode.GROUP_NAME_DUPLICATED
     * .equals(((BandiException) e).getErrorCode())) { Thread.sleep(1);
     * group.setName( group.getName() + "_" +
     * DateUtil_KAIST.getCurrectTimeMillis());
     *
     * groupService.insert(group); } else { throw e; } }
     *
     * } else if
     * (SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE.equals(syncGroup.getActionType()))
     * { group = convertToGroupToBe(syncGroup,
     * SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE);
     *
     * try { groupService.update(group); } catch (Exception e) { // 이름이 중복되었을 때의 처리
     * if (e instanceof BandiException && ErrorCode.GROUP_NAME_DUPLICATED
     * .equals(((BandiException) e).getErrorCode())) { Thread.sleep(1);
     * group.setName( group.getName() + "_" +
     * DateUtil_KAIST.getCurrectTimeMillis());
     *
     * groupService.update(group); } else { throw e; } }
     *
     * } else if
     * (SyncOrgConstants_KAIST.ACTION_TYPE_MOVE.equals(syncGroup.getActionType())) {
     * group = convertToGroupToBe(syncGroup,
     * SyncOrgConstants_KAIST.ACTION_TYPE_MOVE); Group targetGroup =
     * groupService.get(syncGroup.getParentId()); if (targetGroup == null) { throw
     * new BandiException(ErrorCode.TARGET_GROUP_NOT_FOUND); } try {
     * groupService.move(group, targetGroup.getId()); } catch (Exception e) { // 이름이
     * 중복되었을 때의 처리 if (e instanceof BandiException &&
     * ErrorCode.GROUP_NAME_DUPLICATED .equals(((BandiException) e).getErrorCode()))
     * { Thread.sleep(1); group.setName( group.getName() + "_" +
     * DateUtil_KAIST.getCurrectTimeMillis());
     *
     * groupService.move(group, syncGroup.getParentId()); } else { throw e; } }
     *
     * } else if
     * (SyncOrgConstants_KAIST.ACTION_TYPE_DELETE.equals(syncGroup.getActionType()))
     * { group = convertToGroupToBe(syncGroup,
     * SyncOrgConstants_KAIST.ACTION_TYPE_DELETE);
     * groupService.delete(group.getId()); }
     *
     * syncGroup.setActionStatus(SyncOrgConstants_KAIST.ACTION_STATUS_FINISH);
     *
     * } catch (Exception e) {
     *
     * syncGroup.setActionStatus(SyncOrgConstants_KAIST.ACTION_STATUS_ERROR);
     * syncGroup.setErrorMessage(syncGroup.getErrorMessage() + "|" +
     * e.getMessage());
     *
     * e.printStackTrace();
     *
     * } finally { syncGroup.setAppliedAt(now); syncGroups.add(syncGroup); } } }); }
     *
     * String sql = ""; try (SqlSession sqlSession =
     * sqlSessionFactory.openSession()) { sql =
     * sqlSession.getConfiguration().getMappedStatement(
     * "com.bandi.dao.SyncGroupMapper.updateStatusBatch") .getBoundSql(new
     * Object()).getSql(); }
     *
     * try (Connection con = dataSource.getConnection()) { try (PreparedStatement
     * pstmt = con.prepareStatement(sql)) { int index = 0;
     *
     * for (SyncGroup syncGroup : syncGroups) { index++;
     *
     * pstmt.setString(1, syncGroup.getActionStatus()); pstmt.setTimestamp(2,
     * syncGroup.getAppliedAt()); pstmt.setString(3, syncGroup.getErrorMessage());
     * pstmt.setString(4, syncGroup.getOid());
     *
     * pstmt.addBatch(); pstmt.clearParameters();
     *
     * if ((index % 1000) == 0) { pstmt.executeBatch(); pstmt.clearBatch();
     * con.commit(); } } pstmt.executeBatch(); con.commit(); } } catch (SQLException
     * e) { e.printStackTrace(); }
     *
     * }
     */

    // KAIST-TODO 이혜련 : 09.16 기준정보로 조직도연동을 변경함. 공유 DB로 추후에 변경되어야 함.
    /*
     * protected void syncUsersToBe(Timestamp now) { SyncUserCondition condition =
     * new SyncUserCondition();
     * condition.createCriteria().andActionStatusEqualTo(SyncOrgConstants_KAIST.
     * ACTION_STATUS_WAIT); condition.setOrderByClause("SEQ");
     *
     * List<SyncUser> syncUsers = new ArrayList<>();
     *
     * try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
     * sqlSession.select("com.bandi.dao.SyncUserMapper.selectByCondition",
     * condition, new ResultHandler<SyncUser>() {
     *
     * @Override public void handleResult(ResultContext<? extends SyncUser> context)
     * { SyncUser syncUser = context.getResultObject();
     *
     * try { User user = null;
     *
     * if
     * (SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE.equals(syncUser.getActionType()))
     * { user = convertToUser(syncUser, SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE);
     * try { userService.update(user); } catch (Exception e) { // 이름이 중복되었을 때의 처리 if
     * (e instanceof BandiException && ErrorCode.USER_NAME_DUPLICATED
     * .equals(((BandiException) e).getErrorCode())) { Thread.sleep(1);
     * user.setName(user.getName() + "_" + DateUtil_KAIST.getCurrectTimeMillis());
     *
     * userService.update(user); } else { throw e; } }
     *
     * } else if
     * (SyncOrgConstants_KAIST.ACTION_TYPE_MOVE.equals(syncUser.getActionType())) {
     * user = convertToUser(syncUser, SyncOrgConstants_KAIST.ACTION_TYPE_MOVE);
     * Group targetGroup = groupService.get(syncUser.getGroupId());
     *
     * if (targetGroup == null) { throw new
     * BandiException(ErrorCode.TARGET_GROUP_NOT_FOUND); } try {
     * userService.move(user, targetGroup.getId()); } catch (Exception e) { // 이름이
     * 중복되었을 때의 처리 if (e instanceof BandiException && ErrorCode.USER_NAME_DUPLICATED
     * .equals(((BandiException) e).getErrorCode())) { Thread.sleep(1);
     * user.setName(user.getName() + "_" + DateUtil_KAIST.getCurrectTimeMillis());
     *
     * userService.move(user, syncUser.getGroupId()); } else { throw e; } } }
     * syncUser.setActionStatus(SyncOrgConstants_KAIST.ACTION_STATUS_FINISH);
     *
     * } catch (Exception e) {
     *
     * syncUser.setActionStatus(SyncOrgConstants_KAIST.ACTION_STATUS_ERROR);
     * syncUser.setErrorMessage(syncUser.getErrorMessage() + "|" + e.getMessage());
     *
     * e.printStackTrace();
     *
     * } finally { syncUser.setAppliedAt(now); syncUsers.add(syncUser); } } }); }
     *
     * String sql = ""; try (SqlSession sqlSession =
     * sqlSessionFactory.openSession()) { sql =
     * sqlSession.getConfiguration().getMappedStatement(
     * "com.bandi.dao.SyncUserMapper.updateStatusBatch") .getBoundSql(new
     * Object()).getSql(); }
     *
     * try (Connection con = dataSource.getConnection()) { try (PreparedStatement
     * pstmt = con.prepareStatement(sql)) { int index = 0;
     *
     * for (SyncUser syncUser : syncUsers) { index++;
     *
     * pstmt.setString(1, syncUser.getActionStatus()); pstmt.setTimestamp(2,
     * syncUser.getAppliedAt()); pstmt.setString(3, syncUser.getErrorMessage());
     * pstmt.setString(4, syncUser.getOid());
     *
     * pstmt.addBatch(); pstmt.clearParameters();
     *
     * if ((index % 1000) == 0) { pstmt.executeBatch(); pstmt.clearBatch();
     * con.commit(); } } pstmt.executeBatch(); con.commit(); } } catch (SQLException
     * e) { e.printStackTrace(); }
     *
     * }
     */

    // KAIST-TODO 이혜련 : 09.16 기준정보로 조직도연동을 변경함. 공유 DB로 추후에 변경되어야 함.
    /*
     * public String findModifiedGroupFiledsToBe(SourceGroup sourceGroup, Group
     * group) {
     *
     * StringBuffer sb = new StringBuffer();
     *
     * if (sourceGroup.getKoreanName().equals(group.getName()) == false) {
     * sb.append("Name").append(";;").append(group.getName()).append(";;").append(
     * sourceGroup.getKoreanName()) .append("|"); }
     *
     * if (sourceGroup.getSortOrder() != group.getSortOrder()) {
     * sb.append("SortOrder").append(";;").append(group.getSortOrder()).append(";;")
     * .append(sourceGroup.getSortOrder()).append("|"); }
     *
     * if (sourceGroup.getUpperOrganizationUid().equals(group.getParentId()) ==
     * false) { if
     * (isParentIdEqualsKaistRootId(sourceGroup.getUpperOrganizationUid(),
     * group.getParentId()) == false) {
     * sb.append("ParentId").append(";;").append(group.getParentId()).append(";;")
     * .append(sourceGroup.getUpperOrganizationUid()).append("|"); } }
     *
     * return sb.toString(); }
     */

    // KAIST-TODO 이혜련 : 09.16 기준정보로 조직도연동을 변경함. 공유 DB로 추후에 변경되어야 함.
    /*
     * protected String findModifiedUserFiledsToBe(SourceUser sourceUser, User user)
     * {
     *
     * StringBuffer sb = new StringBuffer();
     *
     * if (sourceUser.getKoreanName().equals(user.getName()) == false) {
     * sb.append("Name").append(";;").append(user.getName()).append(";;")
     * .append(sourceUser.getKoreanName()).append("|"); }
     *
     * if (sourceUser.getOrganizationUid().equals(user.getGroupId()) == false) {
     * sb.append("Group").append(";;").append(user.getGroupId()).append(";;")
     * .append(sourceUser.getOrganizationUid()).append("|"); }
     *
     * if (sourceUser.getPersonTypeCodeUid().equals(user.getUserType()) == false) {
     * sb.append("UserType").append(";;").append(user.getUserType()).append(";;")
     * .append(sourceUser.getPersonTypeCodeUid()).append("|"); }
     *
     * if( sourceUser.getEmailAddress() != null && user.getEmail() != null) {
     * if(sourceUser.getEmailAddress().equals(user.getEmail()) == false) {
     * sb.append(
     * "Email").append(";;").append(user.getEmail()).append(";;").append(sourceUser.
     * getEmailAddress()).append("|"); } }else if((sourceUser.getEmailAddress() ==
     * null || sourceUser.getEmailAddress().length() == 0) && (user.getEmail() ==
     * null || user.getEmail().length() == 0)) { //Nothing do }else { sb.append(
     * "Email").append(";;").append(user.getEmail()).append(";;").append(sourceUser.
     * getEmailAddress()).append("|"); }
     *
     * if( sourceUser.getMobileTelephoneNumber() != null && user.getHandphone() !=
     * null) { if(sourceUser.getMobileTelephoneNumber().equals(user.getHandphone())
     * == false) { sb.append(
     * "Handphone").append(";;").append(user.getHandphone()).append(";;").append(
     * sourceUser.getMobileTelephoneNumber()).append("|"); } }else
     * if((sourceUser.getMobileTelephoneNumber() == null ||
     * sourceUser.getMobileTelephoneNumber().length() == 0) && (user.getHandphone()
     * == null || user.getHandphone().length() == 0)) { //Nothing do }else {
     * sb.append(
     * "Handphone").append(";;").append(user.getHandphone()).append(";;").append(
     * sourceUser.getMobileTelephoneNumber()).append("|"); }
     *
     * return sb.toString(); }
     */

    // KAIST-TODO 이혜련 : 09.16 기준정보로 조직도연동을 변경함. 공유 DB로 추후에 변경되어야 함.
    /*
     * protected SyncGroup convertToSyncGroupToBe(Object obj, Timestamp now) {
     *
     * SyncGroup syncGroup = null;
     *
     * if (obj != null) { syncGroup = new SyncGroup();
     * syncGroup.setOid(IdGenerator.getUUID()); } else { return null; }
     *
     * if (obj instanceof SourceGroup) { SourceGroup sourceGroup = (SourceGroup)
     * obj;
     *
     * syncGroup.setId(sourceGroup.getOrganizationUid());
     * syncGroup.setName(sourceGroup.getKoreanName());
     * syncGroup.setSortOrder(sourceGroup.getSortOrder());
     *
     * if
     * (SourceGroup.SOURCE_ROOT_OID.equals(sourceGroup.getUpperOrganizationUid())) {
     * syncGroup.setParentId(Group.GROUP_ROOT_OID); } else {
     * syncGroup.setParentId(sourceGroup.getUpperOrganizationUid()); }
     *
     * } else if (obj instanceof Group) {
     *
     * Group group = (Group) obj;
     *
     * syncGroup.setId(group.getId()); syncGroup.setName(group.getName());
     * syncGroup.setSortOrder(group.getSortOrder());
     * syncGroup.setParentId(group.getParentId());
     *
     * }
     *
     * syncGroup.setActionStatus(SyncOrgConstants_KAIST.ACTION_STATUS_WAIT);
     * syncGroup.setCreatedAt(now);
     *
     * return syncGroup; }
     */

    // KAIST-TODO 이혜련 : 09.16 기준정보로 조직도연동을 변경함. 공유 DB로 추후에 변경되어야 함.
    /*
     * protected Group convertToGroupToBe(SyncGroup syncGroup, String actionType) {
     *
     * Group group = null;
     *
     * if (syncGroup != null) {
     *
     * Group groupFromDB = groupService.get(syncGroup.getId());
     *
     * group = new Group();
     *
     * if (SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE.equals(actionType) ||
     * SyncOrgConstants_KAIST.ACTION_TYPE_MOVE.equals(actionType)) { group =
     * groupFromDB; }
     *
     * if (SyncOrgConstants_KAIST.ACTION_TYPE_MOVE.equals(actionType) == false) {
     * group.setParentId(syncGroup.getParentId()); }
     *
     * group.setId(syncGroup.getId()); group.setName(syncGroup.getName());
     * group.setSortOrder(syncGroup.getSortOrder());
     * group.setFlagSync(BandiConstants.FLAG_Y); } return group; }
     */

    // KAIST-TODO 이혜련 : 09.16 기준정보로 조직도연동을 변경함. 공유 DB로 추후에 변경되어야 함.
    /*
     * protected SyncUser convertToSyncUserToBe(Object obj, Timestamp now) {
     *
     * SyncUser syncUser = null;
     *
     * if (obj != null) { syncUser = new SyncUser();
     * syncUser.setOid(IdGenerator.getUUID()); } else { return null; }
     *
     * if (obj instanceof SourceUser) { SourceUser sourceUser = (SourceUser) obj;
     *
     * syncUser.setId(sourceUser.getSsoUid());
     * syncUser.setName(sourceUser.getKoreanName());
     * syncUser.setKaistUid(sourceUser.getKaistUid());
     * syncUser.setGroupId(sourceUser.getOrganizationUid());
     * syncUser.setUserType(sourceUser.getPersonTypeCodeUid());
     * syncUser.setEmail(sourceUser.getEmailAddress());
     * syncUser.setHandphone(sourceUser.getMobileTelephoneNumber());
     *
     * } else if (obj instanceof User) {
     *
     * User user = (User) obj;
     *
     * syncUser.setId(user.getId()); syncUser.setName(user.getName());
     * syncUser.setKaistUid(user.getKaistUid());
     * syncUser.setGroupId(user.getGroupId());
     * syncUser.setUserType(user.getUserType()); syncUser.setEmail(user.getEmail());
     * syncUser.setHandphone(user.getHandphone()); }
     *
     * syncUser.setActionStatus(SyncOrgConstants_KAIST.ACTION_STATUS_WAIT);
     * syncUser.setCreatedAt(now);
     *
     * return syncUser; }
     */

    // KAIST-TODO 이혜련 : 09.16 기준정보로 조직도연동을 변경함. 공유 DB로 추후에 변경되어야 함.
    /*
     * protected User convertToUserToBe(SyncUser syncUser, String actionType) { User
     * user = null; User userFromDB = null;
     *
     * if (syncUser != null) { user = new User();
     *
     * userFromDB = userService.getFromDb(syncUser.getId());
     *
     * if (SyncOrgConstants_KAIST.ACTION_TYPE_UPDATE.equals(actionType) ||
     * SyncOrgConstants_KAIST.ACTION_TYPE_MOVE.equals(actionType)) { user =
     * userFromDB; user.setName(syncUser.getName());
     * user.setUserType(syncUser.getUserType()); user.setEmail(syncUser.getEmail());
     * user.setHandphone(syncUser.getHandphone()); }
     *
     * user.setFlagSync(BandiConstants.FLAG_Y); }
     *
     * return user;
     *
     * }
     */

}