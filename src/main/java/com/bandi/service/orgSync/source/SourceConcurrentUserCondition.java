package com.bandi.service.orgSync.source;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SourceConcurrentUserCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SourceConcurrentUserCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andNameAcIsNull() {
            addCriterion("NAME_AC is null");
            return (Criteria) this;
        }

        public Criteria andNameAcIsNotNull() {
            addCriterion("NAME_AC is not null");
            return (Criteria) this;
        }

        public Criteria andNameAcEqualTo(String value) {
            addCriterion("NAME_AC =", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotEqualTo(String value) {
            addCriterion("NAME_AC <>", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcGreaterThan(String value) {
            addCriterion("NAME_AC >", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcGreaterThanOrEqualTo(String value) {
            addCriterion("NAME_AC >=", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcLessThan(String value) {
            addCriterion("NAME_AC <", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcLessThanOrEqualTo(String value) {
            addCriterion("NAME_AC <=", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcLike(String value) {
            addCriterion("NAME_AC like", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotLike(String value) {
            addCriterion("NAME_AC not like", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcIn(List<String> values) {
            addCriterion("NAME_AC in", values, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotIn(List<String> values) {
            addCriterion("NAME_AC not in", values, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcBetween(String value1, String value2) {
            addCriterion("NAME_AC between", value1, value2, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotBetween(String value1, String value2) {
            addCriterion("NAME_AC not between", value1, value2, "nameAc");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIsNull() {
            addCriterion("EMPLOYEE_NUMBER is null");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIsNotNull() {
            addCriterion("EMPLOYEE_NUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER =", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER <>", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberGreaterThan(String value) {
            addCriterion("EMPLOYEE_NUMBER >", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberGreaterThanOrEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER >=", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLessThan(String value) {
            addCriterion("EMPLOYEE_NUMBER <", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLessThanOrEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER <=", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLike(String value) {
            addCriterion("EMPLOYEE_NUMBER like", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotLike(String value) {
            addCriterion("EMPLOYEE_NUMBER not like", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIn(List<String> values) {
            addCriterion("EMPLOYEE_NUMBER in", values, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotIn(List<String> values) {
            addCriterion("EMPLOYEE_NUMBER not in", values, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberBetween(String value1, String value2) {
            addCriterion("EMPLOYEE_NUMBER between", value1, value2, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotBetween(String value1, String value2) {
            addCriterion("EMPLOYEE_NUMBER not between", value1, value2, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andGradeNameIsNull() {
            addCriterion("GRADE_NAME is null");
            return (Criteria) this;
        }

        public Criteria andGradeNameIsNotNull() {
            addCriterion("GRADE_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andGradeNameEqualTo(String value) {
            addCriterion("GRADE_NAME =", value, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameNotEqualTo(String value) {
            addCriterion("GRADE_NAME <>", value, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameGreaterThan(String value) {
            addCriterion("GRADE_NAME >", value, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameGreaterThanOrEqualTo(String value) {
            addCriterion("GRADE_NAME >=", value, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameLessThan(String value) {
            addCriterion("GRADE_NAME <", value, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameLessThanOrEqualTo(String value) {
            addCriterion("GRADE_NAME <=", value, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameLike(String value) {
            addCriterion("GRADE_NAME like", value, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameNotLike(String value) {
            addCriterion("GRADE_NAME not like", value, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameIn(List<String> values) {
            addCriterion("GRADE_NAME in", values, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameNotIn(List<String> values) {
            addCriterion("GRADE_NAME not in", values, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameBetween(String value1, String value2) {
            addCriterion("GRADE_NAME between", value1, value2, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameNotBetween(String value1, String value2) {
            addCriterion("GRADE_NAME not between", value1, value2, "gradeName");
            return (Criteria) this;
        }

        public Criteria andPositionStartDateIsNull() {
            addCriterion("POSITION_START_DATE is null");
            return (Criteria) this;
        }

        public Criteria andPositionStartDateIsNotNull() {
            addCriterion("POSITION_START_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andPositionStartDateEqualTo(Date value) {
            addCriterion("POSITION_START_DATE =", value, "positionStartDate");
            return (Criteria) this;
        }

        public Criteria andPositionStartDateNotEqualTo(Date value) {
            addCriterion("POSITION_START_DATE <>", value, "positionStartDate");
            return (Criteria) this;
        }

        public Criteria andPositionStartDateGreaterThan(Date value) {
            addCriterion("POSITION_START_DATE >", value, "positionStartDate");
            return (Criteria) this;
        }

        public Criteria andPositionStartDateGreaterThanOrEqualTo(Date value) {
            addCriterion("POSITION_START_DATE >=", value, "positionStartDate");
            return (Criteria) this;
        }

        public Criteria andPositionStartDateLessThan(Date value) {
            addCriterion("POSITION_START_DATE <", value, "positionStartDate");
            return (Criteria) this;
        }

        public Criteria andPositionStartDateLessThanOrEqualTo(Date value) {
            addCriterion("POSITION_START_DATE <=", value, "positionStartDate");
            return (Criteria) this;
        }

        public Criteria andPositionStartDateIn(List<Date> values) {
            addCriterion("POSITION_START_DATE in", values, "positionStartDate");
            return (Criteria) this;
        }

        public Criteria andPositionStartDateNotIn(List<Date> values) {
            addCriterion("POSITION_START_DATE not in", values, "positionStartDate");
            return (Criteria) this;
        }

        public Criteria andPositionStartDateBetween(Date value1, Date value2) {
            addCriterion("POSITION_START_DATE between", value1, value2, "positionStartDate");
            return (Criteria) this;
        }

        public Criteria andPositionStartDateNotBetween(Date value1, Date value2) {
            addCriterion("POSITION_START_DATE not between", value1, value2, "positionStartDate");
            return (Criteria) this;
        }

        public Criteria andPositionEndDateIsNull() {
            addCriterion("POSITION_END_DATE is null");
            return (Criteria) this;
        }

        public Criteria andPositionEndDateIsNotNull() {
            addCriterion("POSITION_END_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andPositionEndDateEqualTo(Date value) {
            addCriterion("POSITION_END_DATE =", value, "positionEndDate");
            return (Criteria) this;
        }

        public Criteria andPositionEndDateNotEqualTo(Date value) {
            addCriterion("POSITION_END_DATE <>", value, "positionEndDate");
            return (Criteria) this;
        }

        public Criteria andPositionEndDateGreaterThan(Date value) {
            addCriterion("POSITION_END_DATE >", value, "positionEndDate");
            return (Criteria) this;
        }

        public Criteria andPositionEndDateGreaterThanOrEqualTo(Date value) {
            addCriterion("POSITION_END_DATE >=", value, "positionEndDate");
            return (Criteria) this;
        }

        public Criteria andPositionEndDateLessThan(Date value) {
            addCriterion("POSITION_END_DATE <", value, "positionEndDate");
            return (Criteria) this;
        }

        public Criteria andPositionEndDateLessThanOrEqualTo(Date value) {
            addCriterion("POSITION_END_DATE <=", value, "positionEndDate");
            return (Criteria) this;
        }

        public Criteria andPositionEndDateIn(List<Date> values) {
            addCriterion("POSITION_END_DATE in", values, "positionEndDate");
            return (Criteria) this;
        }

        public Criteria andPositionEndDateNotIn(List<Date> values) {
            addCriterion("POSITION_END_DATE not in", values, "positionEndDate");
            return (Criteria) this;
        }

        public Criteria andPositionEndDateBetween(Date value1, Date value2) {
            addCriterion("POSITION_END_DATE between", value1, value2, "positionEndDate");
            return (Criteria) this;
        }

        public Criteria andPositionEndDateNotBetween(Date value1, Date value2) {
            addCriterion("POSITION_END_DATE not between", value1, value2, "positionEndDate");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameIsNull() {
            addCriterion("EBS_ORG_NAME is null");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameIsNotNull() {
            addCriterion("EBS_ORG_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEqualTo(String value) {
            addCriterion("EBS_ORG_NAME =", value, "ebsOrgName");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameNotEqualTo(String value) {
            addCriterion("EBS_ORG_NAME <>", value, "ebsOrgName");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameGreaterThan(String value) {
            addCriterion("EBS_ORG_NAME >", value, "ebsOrgName");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_ORG_NAME >=", value, "ebsOrgName");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameLessThan(String value) {
            addCriterion("EBS_ORG_NAME <", value, "ebsOrgName");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameLessThanOrEqualTo(String value) {
            addCriterion("EBS_ORG_NAME <=", value, "ebsOrgName");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameLike(String value) {
            addCriterion("EBS_ORG_NAME like", value, "ebsOrgName");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameNotLike(String value) {
            addCriterion("EBS_ORG_NAME not like", value, "ebsOrgName");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameIn(List<String> values) {
            addCriterion("EBS_ORG_NAME in", values, "ebsOrgName");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameNotIn(List<String> values) {
            addCriterion("EBS_ORG_NAME not in", values, "ebsOrgName");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameBetween(String value1, String value2) {
            addCriterion("EBS_ORG_NAME between", value1, value2, "ebsOrgName");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameNotBetween(String value1, String value2) {
            addCriterion("EBS_ORG_NAME not between", value1, value2, "ebsOrgName");
            return (Criteria) this;
        }

        public Criteria andEbsPositionIsNull() {
            addCriterion("EBS_POSITION is null");
            return (Criteria) this;
        }

        public Criteria andEbsPositionIsNotNull() {
            addCriterion("EBS_POSITION is not null");
            return (Criteria) this;
        }

        public Criteria andEbsPositionEqualTo(String value) {
            addCriterion("EBS_POSITION =", value, "ebsPosition");
            return (Criteria) this;
        }

        public Criteria andEbsPositionNotEqualTo(String value) {
            addCriterion("EBS_POSITION <>", value, "ebsPosition");
            return (Criteria) this;
        }

        public Criteria andEbsPositionGreaterThan(String value) {
            addCriterion("EBS_POSITION >", value, "ebsPosition");
            return (Criteria) this;
        }

        public Criteria andEbsPositionGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_POSITION >=", value, "ebsPosition");
            return (Criteria) this;
        }

        public Criteria andEbsPositionLessThan(String value) {
            addCriterion("EBS_POSITION <", value, "ebsPosition");
            return (Criteria) this;
        }

        public Criteria andEbsPositionLessThanOrEqualTo(String value) {
            addCriterion("EBS_POSITION <=", value, "ebsPosition");
            return (Criteria) this;
        }

        public Criteria andEbsPositionLike(String value) {
            addCriterion("EBS_POSITION like", value, "ebsPosition");
            return (Criteria) this;
        }

        public Criteria andEbsPositionNotLike(String value) {
            addCriterion("EBS_POSITION not like", value, "ebsPosition");
            return (Criteria) this;
        }

        public Criteria andEbsPositionIn(List<String> values) {
            addCriterion("EBS_POSITION in", values, "ebsPosition");
            return (Criteria) this;
        }

        public Criteria andEbsPositionNotIn(List<String> values) {
            addCriterion("EBS_POSITION not in", values, "ebsPosition");
            return (Criteria) this;
        }

        public Criteria andEbsPositionBetween(String value1, String value2) {
            addCriterion("EBS_POSITION between", value1, value2, "ebsPosition");
            return (Criteria) this;
        }

        public Criteria andEbsPositionNotBetween(String value1, String value2) {
            addCriterion("EBS_POSITION not between", value1, value2, "ebsPosition");
            return (Criteria) this;
        }

        public Criteria andReasonIsNull() {
            addCriterion("REASON is null");
            return (Criteria) this;
        }

        public Criteria andReasonIsNotNull() {
            addCriterion("REASON is not null");
            return (Criteria) this;
        }

        public Criteria andReasonEqualTo(int value) {
            addCriterion("REASON =", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonNotEqualTo(int value) {
            addCriterion("REASON <>", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonGreaterThan(int value) {
            addCriterion("REASON >", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonGreaterThanOrEqualTo(int value) {
            addCriterion("REASON >=", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonLessThan(int value) {
            addCriterion("REASON <", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonLessThanOrEqualTo(int value) {
            addCriterion("REASON <=", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonIn(List<Integer> values) {
            addCriterion("REASON in", values, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonNotIn(List<Integer> values) {
            addCriterion("REASON not in", values, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonBetween(int value1, int value2) {
            addCriterion("REASON between", value1, value2, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonNotBetween(int value1, int value2) {
            addCriterion("REASON not between", value1, value2, "reason");
            return (Criteria) this;
        }

        public Criteria andJobIdIsNull() {
            addCriterion("JOB_ID is null");
            return (Criteria) this;
        }

        public Criteria andJobIdIsNotNull() {
            addCriterion("JOB_ID is not null");
            return (Criteria) this;
        }

        public Criteria andJobIdEqualTo(int value) {
            addCriterion("JOB_ID =", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdNotEqualTo(int value) {
            addCriterion("JOB_ID <>", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdGreaterThan(int value) {
            addCriterion("JOB_ID >", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdGreaterThanOrEqualTo(int value) {
            addCriterion("JOB_ID >=", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdLessThan(int value) {
            addCriterion("JOB_ID <", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdLessThanOrEqualTo(int value) {
            addCriterion("JOB_ID <=", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdIn(List<Integer> values) {
            addCriterion("JOB_ID in", values, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdNotIn(List<Integer> values) {
            addCriterion("JOB_ID not in", values, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdBetween(int value1, int value2) {
            addCriterion("JOB_ID between", value1, value2, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdNotBetween(int value1, int value2) {
            addCriterion("JOB_ID not between", value1, value2, "jobId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIsNull() {
            addCriterion("ORGANIZATION_ID is null");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIsNotNull() {
            addCriterion("ORGANIZATION_ID is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdEqualTo(int value) {
            addCriterion("ORGANIZATION_ID =", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotEqualTo(int value) {
            addCriterion("ORGANIZATION_ID <>", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdGreaterThan(int value) {
            addCriterion("ORGANIZATION_ID >", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdGreaterThanOrEqualTo(int value) {
            addCriterion("ORGANIZATION_ID >=", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdLessThan(int value) {
            addCriterion("ORGANIZATION_ID <", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdLessThanOrEqualTo(int value) {
            addCriterion("ORGANIZATION_ID <=", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIn(List<Integer> values) {
            addCriterion("ORGANIZATION_ID in", values, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotIn(List<Integer> values) {
            addCriterion("ORGANIZATION_ID not in", values, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdBetween(int value1, int value2) {
            addCriterion("ORGANIZATION_ID between", value1, value2, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotBetween(int value1, int value2) {
            addCriterion("ORGANIZATION_ID not between", value1, value2, "organizationId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNull() {
            addCriterion("PERSON_ID is null");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNotNull() {
            addCriterion("PERSON_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPersonIdEqualTo(String value) {
            addCriterion("PERSON_ID =", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotEqualTo(String value) {
            addCriterion("PERSON_ID <>", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThan(String value) {
            addCriterion("PERSON_ID >", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThanOrEqualTo(String value) {
            addCriterion("PERSON_ID >=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThan(String value) {
            addCriterion("PERSON_ID <", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThanOrEqualTo(String value) {
            addCriterion("PERSON_ID <=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLike(String value) {
            addCriterion("PERSON_ID like", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotLike(String value) {
            addCriterion("PERSON_ID not like", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIn(List<String> values) {
            addCriterion("PERSON_ID in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotIn(List<String> values) {
            addCriterion("PERSON_ID not in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdBetween(String value1, String value2) {
            addCriterion("PERSON_ID between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotBetween(String value1, String value2) {
            addCriterion("PERSON_ID not between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeIsNull() {
            addCriterion("JOB_LEVEL_CODE is null");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeIsNotNull() {
            addCriterion("JOB_LEVEL_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeEqualTo(String value) {
            addCriterion("JOB_LEVEL_CODE =", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeNotEqualTo(String value) {
            addCriterion("JOB_LEVEL_CODE <>", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeGreaterThan(String value) {
            addCriterion("JOB_LEVEL_CODE >", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeGreaterThanOrEqualTo(String value) {
            addCriterion("JOB_LEVEL_CODE >=", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeLessThan(String value) {
            addCriterion("JOB_LEVEL_CODE <", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeLessThanOrEqualTo(String value) {
            addCriterion("JOB_LEVEL_CODE <=", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeLike(String value) {
            addCriterion("JOB_LEVEL_CODE like", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeNotLike(String value) {
            addCriterion("JOB_LEVEL_CODE not like", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeIn(List<String> values) {
            addCriterion("JOB_LEVEL_CODE in", values, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeNotIn(List<String> values) {
            addCriterion("JOB_LEVEL_CODE not in", values, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeBetween(String value1, String value2) {
            addCriterion("JOB_LEVEL_CODE between", value1, value2, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeNotBetween(String value1, String value2) {
            addCriterion("JOB_LEVEL_CODE not between", value1, value2, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andNameAcLikeInsensitive(String value) {
            addCriterion("upper(NAME_AC) like", value.toUpperCase(), "nameAc");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLikeInsensitive(String value) {
            addCriterion("upper(EMPLOYEE_NUMBER) like", value.toUpperCase(), "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andGradeNameLikeInsensitive(String value) {
            addCriterion("upper(GRADE_NAME) like", value.toUpperCase(), "gradeName");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameLikeInsensitive(String value) {
            addCriterion("upper(EBS_ORG_NAME) like", value.toUpperCase(), "ebsOrgName");
            return (Criteria) this;
        }

        public Criteria andEbsPositionLikeInsensitive(String value) {
            addCriterion("upper(EBS_POSITION) like", value.toUpperCase(), "ebsPosition");
            return (Criteria) this;
        }

        public Criteria andPersonIdLikeInsensitive(String value) {
            addCriterion("upper(PERSON_ID) like", value.toUpperCase(), "personId");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeLikeInsensitive(String value) {
            addCriterion("upper(JOB_LEVEL_CODE) like", value.toUpperCase(), "jobLevelCode");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}