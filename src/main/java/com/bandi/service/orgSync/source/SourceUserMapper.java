package com.bandi.service.orgSync.source;

import java.util.List;

public interface SourceUserMapper {
    long countByCondition(SourceUserCondition condition);

    List<SourceUser> selectByCondition(SourceUserCondition condition);

    String getKaistUidFromPersonCI(String ci);
}