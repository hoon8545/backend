package com.bandi.service.orgSync.source;

import java.io.Serializable;
import java.util.Date;

import com.bandi.domain.base.BaseObject;

public class SourceConcurrentUser extends BaseObject implements Serializable {
    private String nameAc;

    private String employeeNumber;

    private String gradeName;

    private Date positionStartDate;

    private Date positionEndDate;

    private String ebsOrgName;

    private String ebsPosition;

    private int reason;

    private int jobId;

    private int organizationId;

    private String personId;

    private String jobLevelCode;

    private static final long serialVersionUID = 1L;

    public String getNameAc() {
        return nameAc;
    }

    public void setNameAc(String nameAc) {
        this.nameAc = nameAc == null ? null : nameAc.trim();
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber == null ? null : employeeNumber.trim();
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName == null ? null : gradeName.trim();
    }

    public Date getPositionStartDate() {
        return positionStartDate;
    }

    public void setPositionStartDate(Date positionStartDate) {
        this.positionStartDate = positionStartDate;
    }

    public Date getPositionEndDate() {
        return positionEndDate;
    }

    public void setPositionEndDate(Date positionEndDate) {
        this.positionEndDate = positionEndDate;
    }

    public String getEbsOrgName() {
        return ebsOrgName;
    }

    public void setEbsOrgName(String ebsOrgName) {
        this.ebsOrgName = ebsOrgName == null ? null : ebsOrgName.trim();
    }

    public String getEbsPosition() {
        return ebsPosition;
    }

    public void setEbsPosition(String ebsPosition) {
        this.ebsPosition = ebsPosition == null ? null : ebsPosition.trim();
    }

    public int getReason() {
        return reason;
    }

    public void setReason(int reason) {
        this.reason = reason;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId == null ? null : personId.trim();
    }

    public String getJobLevelCode() {
        return jobLevelCode;
    }

    public void setJobLevelCode(String jobLevelCode) {
        this.jobLevelCode = jobLevelCode == null ? null : jobLevelCode.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", nameAc=").append(nameAc);
        sb.append(", employeeNumber=").append(employeeNumber);
        sb.append(", gradeName=").append(gradeName);
        sb.append(", positionStartDate=").append(positionStartDate);
        sb.append(", positionEndDate=").append(positionEndDate);
        sb.append(", ebsOrgName=").append(ebsOrgName);
        sb.append(", ebsPosition=").append(ebsPosition);
        sb.append(", reason=").append(reason);
        sb.append(", jobId=").append(jobId);
        sb.append(", organizationId=").append(organizationId);
        sb.append(", personId=").append(personId);
        sb.append(", jobLevelCode=").append(jobLevelCode);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}