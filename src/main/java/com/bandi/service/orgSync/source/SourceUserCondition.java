package com.bandi.service.orgSync.source;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SourceUserCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SourceUserCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andEmplidIsNull() {
            addCriterion("EMPLID is null");
            return (Criteria) this;
        }

        public Criteria andEmplidIsNotNull() {
            addCriterion("EMPLID is not null");
            return (Criteria) this;
        }

        public Criteria andEmplidEqualTo(String value) {
            addCriterion("EMPLID =", value, "emplid");
            return (Criteria) this;
        }

        public Criteria andEmplidNotEqualTo(String value) {
            addCriterion("EMPLID <>", value, "emplid");
            return (Criteria) this;
        }

        public Criteria andEmplidGreaterThan(String value) {
            addCriterion("EMPLID >", value, "emplid");
            return (Criteria) this;
        }

        public Criteria andEmplidGreaterThanOrEqualTo(String value) {
            addCriterion("EMPLID >=", value, "emplid");
            return (Criteria) this;
        }

        public Criteria andEmplidLessThan(String value) {
            addCriterion("EMPLID <", value, "emplid");
            return (Criteria) this;
        }

        public Criteria andEmplidLessThanOrEqualTo(String value) {
            addCriterion("EMPLID <=", value, "emplid");
            return (Criteria) this;
        }

        public Criteria andEmplidLike(String value) {
            addCriterion("EMPLID like", value, "emplid");
            return (Criteria) this;
        }

        public Criteria andEmplidNotLike(String value) {
            addCriterion("EMPLID not like", value, "emplid");
            return (Criteria) this;
        }

        public Criteria andEmplidIn(List<String> values) {
            addCriterion("EMPLID in", values, "emplid");
            return (Criteria) this;
        }

        public Criteria andEmplidNotIn(List<String> values) {
            addCriterion("EMPLID not in", values, "emplid");
            return (Criteria) this;
        }

        public Criteria andEmplidBetween(String value1, String value2) {
            addCriterion("EMPLID between", value1, value2, "emplid");
            return (Criteria) this;
        }

        public Criteria andEmplidNotBetween(String value1, String value2) {
            addCriterion("EMPLID not between", value1, value2, "emplid");
            return (Criteria) this;
        }

        public Criteria andSsoIdIsNull() {
            addCriterion("SSO_ID is null");
            return (Criteria) this;
        }

        public Criteria andSsoIdIsNotNull() {
            addCriterion("SSO_ID is not null");
            return (Criteria) this;
        }

        public Criteria andSsoIdEqualTo(String value) {
            addCriterion("SSO_ID =", value, "ssoId");
            return (Criteria) this;
        }

        public Criteria andSsoIdNotEqualTo(String value) {
            addCriterion("SSO_ID <>", value, "ssoId");
            return (Criteria) this;
        }

        public Criteria andSsoIdGreaterThan(String value) {
            addCriterion("SSO_ID >", value, "ssoId");
            return (Criteria) this;
        }

        public Criteria andSsoIdGreaterThanOrEqualTo(String value) {
            addCriterion("SSO_ID >=", value, "ssoId");
            return (Criteria) this;
        }

        public Criteria andSsoIdLessThan(String value) {
            addCriterion("SSO_ID <", value, "ssoId");
            return (Criteria) this;
        }

        public Criteria andSsoIdLessThanOrEqualTo(String value) {
            addCriterion("SSO_ID <=", value, "ssoId");
            return (Criteria) this;
        }

        public Criteria andSsoIdLike(String value) {
            addCriterion("SSO_ID like", value, "ssoId");
            return (Criteria) this;
        }

        public Criteria andSsoIdNotLike(String value) {
            addCriterion("SSO_ID not like", value, "ssoId");
            return (Criteria) this;
        }

        public Criteria andSsoIdIn(List<String> values) {
            addCriterion("SSO_ID in", values, "ssoId");
            return (Criteria) this;
        }

        public Criteria andSsoIdNotIn(List<String> values) {
            addCriterion("SSO_ID not in", values, "ssoId");
            return (Criteria) this;
        }

        public Criteria andSsoIdBetween(String value1, String value2) {
            addCriterion("SSO_ID between", value1, value2, "ssoId");
            return (Criteria) this;
        }

        public Criteria andSsoIdNotBetween(String value1, String value2) {
            addCriterion("SSO_ID not between", value1, value2, "ssoId");
            return (Criteria) this;
        }

        public Criteria andSsoPwIsNull() {
            addCriterion("SSO_PW is null");
            return (Criteria) this;
        }

        public Criteria andSsoPwIsNotNull() {
            addCriterion("SSO_PW is not null");
            return (Criteria) this;
        }

        public Criteria andSsoPwEqualTo(String value) {
            addCriterion("SSO_PW =", value, "ssoPw");
            return (Criteria) this;
        }

        public Criteria andSsoPwNotEqualTo(String value) {
            addCriterion("SSO_PW <>", value, "ssoPw");
            return (Criteria) this;
        }

        public Criteria andSsoPwGreaterThan(String value) {
            addCriterion("SSO_PW >", value, "ssoPw");
            return (Criteria) this;
        }

        public Criteria andSsoPwGreaterThanOrEqualTo(String value) {
            addCriterion("SSO_PW >=", value, "ssoPw");
            return (Criteria) this;
        }

        public Criteria andSsoPwLessThan(String value) {
            addCriterion("SSO_PW <", value, "ssoPw");
            return (Criteria) this;
        }

        public Criteria andSsoPwLessThanOrEqualTo(String value) {
            addCriterion("SSO_PW <=", value, "ssoPw");
            return (Criteria) this;
        }

        public Criteria andSsoPwLike(String value) {
            addCriterion("SSO_PW like", value, "ssoPw");
            return (Criteria) this;
        }

        public Criteria andSsoPwNotLike(String value) {
            addCriterion("SSO_PW not like", value, "ssoPw");
            return (Criteria) this;
        }

        public Criteria andSsoPwIn(List<String> values) {
            addCriterion("SSO_PW in", values, "ssoPw");
            return (Criteria) this;
        }

        public Criteria andSsoPwNotIn(List<String> values) {
            addCriterion("SSO_PW not in", values, "ssoPw");
            return (Criteria) this;
        }

        public Criteria andSsoPwBetween(String value1, String value2) {
            addCriterion("SSO_PW between", value1, value2, "ssoPw");
            return (Criteria) this;
        }

        public Criteria andSsoPwNotBetween(String value1, String value2) {
            addCriterion("SSO_PW not between", value1, value2, "ssoPw");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("NAME is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("NAME is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("NAME =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("NAME <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("NAME >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("NAME >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("NAME <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("NAME <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("NAME like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("NAME not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("NAME in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("NAME not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("NAME between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("NAME not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameAcIsNull() {
            addCriterion("NAME_AC is null");
            return (Criteria) this;
        }

        public Criteria andNameAcIsNotNull() {
            addCriterion("NAME_AC is not null");
            return (Criteria) this;
        }

        public Criteria andNameAcEqualTo(String value) {
            addCriterion("NAME_AC =", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotEqualTo(String value) {
            addCriterion("NAME_AC <>", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcGreaterThan(String value) {
            addCriterion("NAME_AC >", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcGreaterThanOrEqualTo(String value) {
            addCriterion("NAME_AC >=", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcLessThan(String value) {
            addCriterion("NAME_AC <", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcLessThanOrEqualTo(String value) {
            addCriterion("NAME_AC <=", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcLike(String value) {
            addCriterion("NAME_AC like", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotLike(String value) {
            addCriterion("NAME_AC not like", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcIn(List<String> values) {
            addCriterion("NAME_AC in", values, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotIn(List<String> values) {
            addCriterion("NAME_AC not in", values, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcBetween(String value1, String value2) {
            addCriterion("NAME_AC between", value1, value2, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotBetween(String value1, String value2) {
            addCriterion("NAME_AC not between", value1, value2, "nameAc");
            return (Criteria) this;
        }

        public Criteria andLastNameIsNull() {
            addCriterion("LAST_NAME is null");
            return (Criteria) this;
        }

        public Criteria andLastNameIsNotNull() {
            addCriterion("LAST_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andLastNameEqualTo(String value) {
            addCriterion("LAST_NAME =", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameNotEqualTo(String value) {
            addCriterion("LAST_NAME <>", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameGreaterThan(String value) {
            addCriterion("LAST_NAME >", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameGreaterThanOrEqualTo(String value) {
            addCriterion("LAST_NAME >=", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameLessThan(String value) {
            addCriterion("LAST_NAME <", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameLessThanOrEqualTo(String value) {
            addCriterion("LAST_NAME <=", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameLike(String value) {
            addCriterion("LAST_NAME like", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameNotLike(String value) {
            addCriterion("LAST_NAME not like", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameIn(List<String> values) {
            addCriterion("LAST_NAME in", values, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameNotIn(List<String> values) {
            addCriterion("LAST_NAME not in", values, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameBetween(String value1, String value2) {
            addCriterion("LAST_NAME between", value1, value2, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameNotBetween(String value1, String value2) {
            addCriterion("LAST_NAME not between", value1, value2, "lastName");
            return (Criteria) this;
        }

        public Criteria andFirstNameIsNull() {
            addCriterion("FIRST_NAME is null");
            return (Criteria) this;
        }

        public Criteria andFirstNameIsNotNull() {
            addCriterion("FIRST_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andFirstNameEqualTo(String value) {
            addCriterion("FIRST_NAME =", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameNotEqualTo(String value) {
            addCriterion("FIRST_NAME <>", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameGreaterThan(String value) {
            addCriterion("FIRST_NAME >", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameGreaterThanOrEqualTo(String value) {
            addCriterion("FIRST_NAME >=", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameLessThan(String value) {
            addCriterion("FIRST_NAME <", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameLessThanOrEqualTo(String value) {
            addCriterion("FIRST_NAME <=", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameLike(String value) {
            addCriterion("FIRST_NAME like", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameNotLike(String value) {
            addCriterion("FIRST_NAME not like", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameIn(List<String> values) {
            addCriterion("FIRST_NAME in", values, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameNotIn(List<String> values) {
            addCriterion("FIRST_NAME not in", values, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameBetween(String value1, String value2) {
            addCriterion("FIRST_NAME between", value1, value2, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameNotBetween(String value1, String value2) {
            addCriterion("FIRST_NAME not between", value1, value2, "firstName");
            return (Criteria) this;
        }

        public Criteria andBirthdateIsNull() {
            addCriterion("BIRTHDATE is null");
            return (Criteria) this;
        }

        public Criteria andBirthdateIsNotNull() {
            addCriterion("BIRTHDATE is not null");
            return (Criteria) this;
        }

        public Criteria andBirthdateEqualTo(String value) {
            addCriterion("BIRTHDATE =", value, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateNotEqualTo(String value) {
            addCriterion("BIRTHDATE <>", value, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateGreaterThan(String value) {
            addCriterion("BIRTHDATE >", value, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateGreaterThanOrEqualTo(String value) {
            addCriterion("BIRTHDATE >=", value, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateLessThan(String value) {
            addCriterion("BIRTHDATE <", value, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateLessThanOrEqualTo(String value) {
            addCriterion("BIRTHDATE <=", value, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateLike(String value) {
            addCriterion("BIRTHDATE like", value, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateNotLike(String value) {
            addCriterion("BIRTHDATE not like", value, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateIn(List<String> values) {
            addCriterion("BIRTHDATE in", values, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateNotIn(List<String> values) {
            addCriterion("BIRTHDATE not in", values, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateBetween(String value1, String value2) {
            addCriterion("BIRTHDATE between", value1, value2, "birthdate");
            return (Criteria) this;
        }

        public Criteria andBirthdateNotBetween(String value1, String value2) {
            addCriterion("BIRTHDATE not between", value1, value2, "birthdate");
            return (Criteria) this;
        }

        public Criteria andCountryIsNull() {
            addCriterion("COUNTRY is null");
            return (Criteria) this;
        }

        public Criteria andCountryIsNotNull() {
            addCriterion("COUNTRY is not null");
            return (Criteria) this;
        }

        public Criteria andCountryEqualTo(String value) {
            addCriterion("COUNTRY =", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryNotEqualTo(String value) {
            addCriterion("COUNTRY <>", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryGreaterThan(String value) {
            addCriterion("COUNTRY >", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryGreaterThanOrEqualTo(String value) {
            addCriterion("COUNTRY >=", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryLessThan(String value) {
            addCriterion("COUNTRY <", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryLessThanOrEqualTo(String value) {
            addCriterion("COUNTRY <=", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryLike(String value) {
            addCriterion("COUNTRY like", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryNotLike(String value) {
            addCriterion("COUNTRY not like", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryIn(List<String> values) {
            addCriterion("COUNTRY in", values, "country");
            return (Criteria) this;
        }

        public Criteria andCountryNotIn(List<String> values) {
            addCriterion("COUNTRY not in", values, "country");
            return (Criteria) this;
        }

        public Criteria andCountryBetween(String value1, String value2) {
            addCriterion("COUNTRY between", value1, value2, "country");
            return (Criteria) this;
        }

        public Criteria andCountryNotBetween(String value1, String value2) {
            addCriterion("COUNTRY not between", value1, value2, "country");
            return (Criteria) this;
        }

        public Criteria andNationalIdIsNull() {
            addCriterion("NATIONAL_ID is null");
            return (Criteria) this;
        }

        public Criteria andNationalIdIsNotNull() {
            addCriterion("NATIONAL_ID is not null");
            return (Criteria) this;
        }

        public Criteria andNationalIdEqualTo(String value) {
            addCriterion("NATIONAL_ID =", value, "nationalId");
            return (Criteria) this;
        }

        public Criteria andNationalIdNotEqualTo(String value) {
            addCriterion("NATIONAL_ID <>", value, "nationalId");
            return (Criteria) this;
        }

        public Criteria andNationalIdGreaterThan(String value) {
            addCriterion("NATIONAL_ID >", value, "nationalId");
            return (Criteria) this;
        }

        public Criteria andNationalIdGreaterThanOrEqualTo(String value) {
            addCriterion("NATIONAL_ID >=", value, "nationalId");
            return (Criteria) this;
        }

        public Criteria andNationalIdLessThan(String value) {
            addCriterion("NATIONAL_ID <", value, "nationalId");
            return (Criteria) this;
        }

        public Criteria andNationalIdLessThanOrEqualTo(String value) {
            addCriterion("NATIONAL_ID <=", value, "nationalId");
            return (Criteria) this;
        }

        public Criteria andNationalIdLike(String value) {
            addCriterion("NATIONAL_ID like", value, "nationalId");
            return (Criteria) this;
        }

        public Criteria andNationalIdNotLike(String value) {
            addCriterion("NATIONAL_ID not like", value, "nationalId");
            return (Criteria) this;
        }

        public Criteria andNationalIdIn(List<String> values) {
            addCriterion("NATIONAL_ID in", values, "nationalId");
            return (Criteria) this;
        }

        public Criteria andNationalIdNotIn(List<String> values) {
            addCriterion("NATIONAL_ID not in", values, "nationalId");
            return (Criteria) this;
        }

        public Criteria andNationalIdBetween(String value1, String value2) {
            addCriterion("NATIONAL_ID between", value1, value2, "nationalId");
            return (Criteria) this;
        }

        public Criteria andNationalIdNotBetween(String value1, String value2) {
            addCriterion("NATIONAL_ID not between", value1, value2, "nationalId");
            return (Criteria) this;
        }

        public Criteria andSexIsNull() {
            addCriterion("SEX is null");
            return (Criteria) this;
        }

        public Criteria andSexIsNotNull() {
            addCriterion("SEX is not null");
            return (Criteria) this;
        }

        public Criteria andSexEqualTo(String value) {
            addCriterion("SEX =", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotEqualTo(String value) {
            addCriterion("SEX <>", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThan(String value) {
            addCriterion("SEX >", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThanOrEqualTo(String value) {
            addCriterion("SEX >=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThan(String value) {
            addCriterion("SEX <", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThanOrEqualTo(String value) {
            addCriterion("SEX <=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLike(String value) {
            addCriterion("SEX like", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotLike(String value) {
            addCriterion("SEX not like", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexIn(List<String> values) {
            addCriterion("SEX in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotIn(List<String> values) {
            addCriterion("SEX not in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexBetween(String value1, String value2) {
            addCriterion("SEX between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotBetween(String value1, String value2) {
            addCriterion("SEX not between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andEmailAddrIsNull() {
            addCriterion("EMAIL_ADDR is null");
            return (Criteria) this;
        }

        public Criteria andEmailAddrIsNotNull() {
            addCriterion("EMAIL_ADDR is not null");
            return (Criteria) this;
        }

        public Criteria andEmailAddrEqualTo(String value) {
            addCriterion("EMAIL_ADDR =", value, "emailAddr");
            return (Criteria) this;
        }

        public Criteria andEmailAddrNotEqualTo(String value) {
            addCriterion("EMAIL_ADDR <>", value, "emailAddr");
            return (Criteria) this;
        }

        public Criteria andEmailAddrGreaterThan(String value) {
            addCriterion("EMAIL_ADDR >", value, "emailAddr");
            return (Criteria) this;
        }

        public Criteria andEmailAddrGreaterThanOrEqualTo(String value) {
            addCriterion("EMAIL_ADDR >=", value, "emailAddr");
            return (Criteria) this;
        }

        public Criteria andEmailAddrLessThan(String value) {
            addCriterion("EMAIL_ADDR <", value, "emailAddr");
            return (Criteria) this;
        }

        public Criteria andEmailAddrLessThanOrEqualTo(String value) {
            addCriterion("EMAIL_ADDR <=", value, "emailAddr");
            return (Criteria) this;
        }

        public Criteria andEmailAddrLike(String value) {
            addCriterion("EMAIL_ADDR like", value, "emailAddr");
            return (Criteria) this;
        }

        public Criteria andEmailAddrNotLike(String value) {
            addCriterion("EMAIL_ADDR not like", value, "emailAddr");
            return (Criteria) this;
        }

        public Criteria andEmailAddrIn(List<String> values) {
            addCriterion("EMAIL_ADDR in", values, "emailAddr");
            return (Criteria) this;
        }

        public Criteria andEmailAddrNotIn(List<String> values) {
            addCriterion("EMAIL_ADDR not in", values, "emailAddr");
            return (Criteria) this;
        }

        public Criteria andEmailAddrBetween(String value1, String value2) {
            addCriterion("EMAIL_ADDR between", value1, value2, "emailAddr");
            return (Criteria) this;
        }

        public Criteria andEmailAddrNotBetween(String value1, String value2) {
            addCriterion("EMAIL_ADDR not between", value1, value2, "emailAddr");
            return (Criteria) this;
        }

        public Criteria andChMailIsNull() {
            addCriterion("CH_MAIL is null");
            return (Criteria) this;
        }

        public Criteria andChMailIsNotNull() {
            addCriterion("CH_MAIL is not null");
            return (Criteria) this;
        }

        public Criteria andChMailEqualTo(String value) {
            addCriterion("CH_MAIL =", value, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailNotEqualTo(String value) {
            addCriterion("CH_MAIL <>", value, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailGreaterThan(String value) {
            addCriterion("CH_MAIL >", value, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailGreaterThanOrEqualTo(String value) {
            addCriterion("CH_MAIL >=", value, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailLessThan(String value) {
            addCriterion("CH_MAIL <", value, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailLessThanOrEqualTo(String value) {
            addCriterion("CH_MAIL <=", value, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailLike(String value) {
            addCriterion("CH_MAIL like", value, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailNotLike(String value) {
            addCriterion("CH_MAIL not like", value, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailIn(List<String> values) {
            addCriterion("CH_MAIL in", values, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailNotIn(List<String> values) {
            addCriterion("CH_MAIL not in", values, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailBetween(String value1, String value2) {
            addCriterion("CH_MAIL between", value1, value2, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailNotBetween(String value1, String value2) {
            addCriterion("CH_MAIL not between", value1, value2, "chMail");
            return (Criteria) this;
        }

        public Criteria andCellPhoneIsNull() {
            addCriterion("CELL_PHONE is null");
            return (Criteria) this;
        }

        public Criteria andCellPhoneIsNotNull() {
            addCriterion("CELL_PHONE is not null");
            return (Criteria) this;
        }

        public Criteria andCellPhoneEqualTo(String value) {
            addCriterion("CELL_PHONE =", value, "cellPhone");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNotEqualTo(String value) {
            addCriterion("CELL_PHONE <>", value, "cellPhone");
            return (Criteria) this;
        }

        public Criteria andCellPhoneGreaterThan(String value) {
            addCriterion("CELL_PHONE >", value, "cellPhone");
            return (Criteria) this;
        }

        public Criteria andCellPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("CELL_PHONE >=", value, "cellPhone");
            return (Criteria) this;
        }

        public Criteria andCellPhoneLessThan(String value) {
            addCriterion("CELL_PHONE <", value, "cellPhone");
            return (Criteria) this;
        }

        public Criteria andCellPhoneLessThanOrEqualTo(String value) {
            addCriterion("CELL_PHONE <=", value, "cellPhone");
            return (Criteria) this;
        }

        public Criteria andCellPhoneLike(String value) {
            addCriterion("CELL_PHONE like", value, "cellPhone");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNotLike(String value) {
            addCriterion("CELL_PHONE not like", value, "cellPhone");
            return (Criteria) this;
        }

        public Criteria andCellPhoneIn(List<String> values) {
            addCriterion("CELL_PHONE in", values, "cellPhone");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNotIn(List<String> values) {
            addCriterion("CELL_PHONE not in", values, "cellPhone");
            return (Criteria) this;
        }

        public Criteria andCellPhoneBetween(String value1, String value2) {
            addCriterion("CELL_PHONE between", value1, value2, "cellPhone");
            return (Criteria) this;
        }

        public Criteria andCellPhoneNotBetween(String value1, String value2) {
            addCriterion("CELL_PHONE not between", value1, value2, "cellPhone");
            return (Criteria) this;
        }

        public Criteria andBusnPhoneIsNull() {
            addCriterion("BUSN_PHONE is null");
            return (Criteria) this;
        }

        public Criteria andBusnPhoneIsNotNull() {
            addCriterion("BUSN_PHONE is not null");
            return (Criteria) this;
        }

        public Criteria andBusnPhoneEqualTo(String value) {
            addCriterion("BUSN_PHONE =", value, "busnPhone");
            return (Criteria) this;
        }

        public Criteria andBusnPhoneNotEqualTo(String value) {
            addCriterion("BUSN_PHONE <>", value, "busnPhone");
            return (Criteria) this;
        }

        public Criteria andBusnPhoneGreaterThan(String value) {
            addCriterion("BUSN_PHONE >", value, "busnPhone");
            return (Criteria) this;
        }

        public Criteria andBusnPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("BUSN_PHONE >=", value, "busnPhone");
            return (Criteria) this;
        }

        public Criteria andBusnPhoneLessThan(String value) {
            addCriterion("BUSN_PHONE <", value, "busnPhone");
            return (Criteria) this;
        }

        public Criteria andBusnPhoneLessThanOrEqualTo(String value) {
            addCriterion("BUSN_PHONE <=", value, "busnPhone");
            return (Criteria) this;
        }

        public Criteria andBusnPhoneLike(String value) {
            addCriterion("BUSN_PHONE like", value, "busnPhone");
            return (Criteria) this;
        }

        public Criteria andBusnPhoneNotLike(String value) {
            addCriterion("BUSN_PHONE not like", value, "busnPhone");
            return (Criteria) this;
        }

        public Criteria andBusnPhoneIn(List<String> values) {
            addCriterion("BUSN_PHONE in", values, "busnPhone");
            return (Criteria) this;
        }

        public Criteria andBusnPhoneNotIn(List<String> values) {
            addCriterion("BUSN_PHONE not in", values, "busnPhone");
            return (Criteria) this;
        }

        public Criteria andBusnPhoneBetween(String value1, String value2) {
            addCriterion("BUSN_PHONE between", value1, value2, "busnPhone");
            return (Criteria) this;
        }

        public Criteria andBusnPhoneNotBetween(String value1, String value2) {
            addCriterion("BUSN_PHONE not between", value1, value2, "busnPhone");
            return (Criteria) this;
        }

        public Criteria andHomePhoneIsNull() {
            addCriterion("HOME_PHONE is null");
            return (Criteria) this;
        }

        public Criteria andHomePhoneIsNotNull() {
            addCriterion("HOME_PHONE is not null");
            return (Criteria) this;
        }

        public Criteria andHomePhoneEqualTo(String value) {
            addCriterion("HOME_PHONE =", value, "homePhone");
            return (Criteria) this;
        }

        public Criteria andHomePhoneNotEqualTo(String value) {
            addCriterion("HOME_PHONE <>", value, "homePhone");
            return (Criteria) this;
        }

        public Criteria andHomePhoneGreaterThan(String value) {
            addCriterion("HOME_PHONE >", value, "homePhone");
            return (Criteria) this;
        }

        public Criteria andHomePhoneGreaterThanOrEqualTo(String value) {
            addCriterion("HOME_PHONE >=", value, "homePhone");
            return (Criteria) this;
        }

        public Criteria andHomePhoneLessThan(String value) {
            addCriterion("HOME_PHONE <", value, "homePhone");
            return (Criteria) this;
        }

        public Criteria andHomePhoneLessThanOrEqualTo(String value) {
            addCriterion("HOME_PHONE <=", value, "homePhone");
            return (Criteria) this;
        }

        public Criteria andHomePhoneLike(String value) {
            addCriterion("HOME_PHONE like", value, "homePhone");
            return (Criteria) this;
        }

        public Criteria andHomePhoneNotLike(String value) {
            addCriterion("HOME_PHONE not like", value, "homePhone");
            return (Criteria) this;
        }

        public Criteria andHomePhoneIn(List<String> values) {
            addCriterion("HOME_PHONE in", values, "homePhone");
            return (Criteria) this;
        }

        public Criteria andHomePhoneNotIn(List<String> values) {
            addCriterion("HOME_PHONE not in", values, "homePhone");
            return (Criteria) this;
        }

        public Criteria andHomePhoneBetween(String value1, String value2) {
            addCriterion("HOME_PHONE between", value1, value2, "homePhone");
            return (Criteria) this;
        }

        public Criteria andHomePhoneNotBetween(String value1, String value2) {
            addCriterion("HOME_PHONE not between", value1, value2, "homePhone");
            return (Criteria) this;
        }

        public Criteria andFaxPhoneIsNull() {
            addCriterion("FAX_PHONE is null");
            return (Criteria) this;
        }

        public Criteria andFaxPhoneIsNotNull() {
            addCriterion("FAX_PHONE is not null");
            return (Criteria) this;
        }

        public Criteria andFaxPhoneEqualTo(String value) {
            addCriterion("FAX_PHONE =", value, "faxPhone");
            return (Criteria) this;
        }

        public Criteria andFaxPhoneNotEqualTo(String value) {
            addCriterion("FAX_PHONE <>", value, "faxPhone");
            return (Criteria) this;
        }

        public Criteria andFaxPhoneGreaterThan(String value) {
            addCriterion("FAX_PHONE >", value, "faxPhone");
            return (Criteria) this;
        }

        public Criteria andFaxPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("FAX_PHONE >=", value, "faxPhone");
            return (Criteria) this;
        }

        public Criteria andFaxPhoneLessThan(String value) {
            addCriterion("FAX_PHONE <", value, "faxPhone");
            return (Criteria) this;
        }

        public Criteria andFaxPhoneLessThanOrEqualTo(String value) {
            addCriterion("FAX_PHONE <=", value, "faxPhone");
            return (Criteria) this;
        }

        public Criteria andFaxPhoneLike(String value) {
            addCriterion("FAX_PHONE like", value, "faxPhone");
            return (Criteria) this;
        }

        public Criteria andFaxPhoneNotLike(String value) {
            addCriterion("FAX_PHONE not like", value, "faxPhone");
            return (Criteria) this;
        }

        public Criteria andFaxPhoneIn(List<String> values) {
            addCriterion("FAX_PHONE in", values, "faxPhone");
            return (Criteria) this;
        }

        public Criteria andFaxPhoneNotIn(List<String> values) {
            addCriterion("FAX_PHONE not in", values, "faxPhone");
            return (Criteria) this;
        }

        public Criteria andFaxPhoneBetween(String value1, String value2) {
            addCriterion("FAX_PHONE between", value1, value2, "faxPhone");
            return (Criteria) this;
        }

        public Criteria andFaxPhoneNotBetween(String value1, String value2) {
            addCriterion("FAX_PHONE not between", value1, value2, "faxPhone");
            return (Criteria) this;
        }

        public Criteria andAddressTypeIsNull() {
            addCriterion("ADDRESS_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andAddressTypeIsNotNull() {
            addCriterion("ADDRESS_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andAddressTypeEqualTo(String value) {
            addCriterion("ADDRESS_TYPE =", value, "addressType");
            return (Criteria) this;
        }

        public Criteria andAddressTypeNotEqualTo(String value) {
            addCriterion("ADDRESS_TYPE <>", value, "addressType");
            return (Criteria) this;
        }

        public Criteria andAddressTypeGreaterThan(String value) {
            addCriterion("ADDRESS_TYPE >", value, "addressType");
            return (Criteria) this;
        }

        public Criteria andAddressTypeGreaterThanOrEqualTo(String value) {
            addCriterion("ADDRESS_TYPE >=", value, "addressType");
            return (Criteria) this;
        }

        public Criteria andAddressTypeLessThan(String value) {
            addCriterion("ADDRESS_TYPE <", value, "addressType");
            return (Criteria) this;
        }

        public Criteria andAddressTypeLessThanOrEqualTo(String value) {
            addCriterion("ADDRESS_TYPE <=", value, "addressType");
            return (Criteria) this;
        }

        public Criteria andAddressTypeLike(String value) {
            addCriterion("ADDRESS_TYPE like", value, "addressType");
            return (Criteria) this;
        }

        public Criteria andAddressTypeNotLike(String value) {
            addCriterion("ADDRESS_TYPE not like", value, "addressType");
            return (Criteria) this;
        }

        public Criteria andAddressTypeIn(List<String> values) {
            addCriterion("ADDRESS_TYPE in", values, "addressType");
            return (Criteria) this;
        }

        public Criteria andAddressTypeNotIn(List<String> values) {
            addCriterion("ADDRESS_TYPE not in", values, "addressType");
            return (Criteria) this;
        }

        public Criteria andAddressTypeBetween(String value1, String value2) {
            addCriterion("ADDRESS_TYPE between", value1, value2, "addressType");
            return (Criteria) this;
        }

        public Criteria andAddressTypeNotBetween(String value1, String value2) {
            addCriterion("ADDRESS_TYPE not between", value1, value2, "addressType");
            return (Criteria) this;
        }

        public Criteria andPostalIsNull() {
            addCriterion("POSTAL is null");
            return (Criteria) this;
        }

        public Criteria andPostalIsNotNull() {
            addCriterion("POSTAL is not null");
            return (Criteria) this;
        }

        public Criteria andPostalEqualTo(String value) {
            addCriterion("POSTAL =", value, "postal");
            return (Criteria) this;
        }

        public Criteria andPostalNotEqualTo(String value) {
            addCriterion("POSTAL <>", value, "postal");
            return (Criteria) this;
        }

        public Criteria andPostalGreaterThan(String value) {
            addCriterion("POSTAL >", value, "postal");
            return (Criteria) this;
        }

        public Criteria andPostalGreaterThanOrEqualTo(String value) {
            addCriterion("POSTAL >=", value, "postal");
            return (Criteria) this;
        }

        public Criteria andPostalLessThan(String value) {
            addCriterion("POSTAL <", value, "postal");
            return (Criteria) this;
        }

        public Criteria andPostalLessThanOrEqualTo(String value) {
            addCriterion("POSTAL <=", value, "postal");
            return (Criteria) this;
        }

        public Criteria andPostalLike(String value) {
            addCriterion("POSTAL like", value, "postal");
            return (Criteria) this;
        }

        public Criteria andPostalNotLike(String value) {
            addCriterion("POSTAL not like", value, "postal");
            return (Criteria) this;
        }

        public Criteria andPostalIn(List<String> values) {
            addCriterion("POSTAL in", values, "postal");
            return (Criteria) this;
        }

        public Criteria andPostalNotIn(List<String> values) {
            addCriterion("POSTAL not in", values, "postal");
            return (Criteria) this;
        }

        public Criteria andPostalBetween(String value1, String value2) {
            addCriterion("POSTAL between", value1, value2, "postal");
            return (Criteria) this;
        }

        public Criteria andPostalNotBetween(String value1, String value2) {
            addCriterion("POSTAL not between", value1, value2, "postal");
            return (Criteria) this;
        }

        public Criteria andAddress1IsNull() {
            addCriterion("ADDRESS1 is null");
            return (Criteria) this;
        }

        public Criteria andAddress1IsNotNull() {
            addCriterion("ADDRESS1 is not null");
            return (Criteria) this;
        }

        public Criteria andAddress1EqualTo(String value) {
            addCriterion("ADDRESS1 =", value, "address1");
            return (Criteria) this;
        }

        public Criteria andAddress1NotEqualTo(String value) {
            addCriterion("ADDRESS1 <>", value, "address1");
            return (Criteria) this;
        }

        public Criteria andAddress1GreaterThan(String value) {
            addCriterion("ADDRESS1 >", value, "address1");
            return (Criteria) this;
        }

        public Criteria andAddress1GreaterThanOrEqualTo(String value) {
            addCriterion("ADDRESS1 >=", value, "address1");
            return (Criteria) this;
        }

        public Criteria andAddress1LessThan(String value) {
            addCriterion("ADDRESS1 <", value, "address1");
            return (Criteria) this;
        }

        public Criteria andAddress1LessThanOrEqualTo(String value) {
            addCriterion("ADDRESS1 <=", value, "address1");
            return (Criteria) this;
        }

        public Criteria andAddress1Like(String value) {
            addCriterion("ADDRESS1 like", value, "address1");
            return (Criteria) this;
        }

        public Criteria andAddress1NotLike(String value) {
            addCriterion("ADDRESS1 not like", value, "address1");
            return (Criteria) this;
        }

        public Criteria andAddress1In(List<String> values) {
            addCriterion("ADDRESS1 in", values, "address1");
            return (Criteria) this;
        }

        public Criteria andAddress1NotIn(List<String> values) {
            addCriterion("ADDRESS1 not in", values, "address1");
            return (Criteria) this;
        }

        public Criteria andAddress1Between(String value1, String value2) {
            addCriterion("ADDRESS1 between", value1, value2, "address1");
            return (Criteria) this;
        }

        public Criteria andAddress1NotBetween(String value1, String value2) {
            addCriterion("ADDRESS1 not between", value1, value2, "address1");
            return (Criteria) this;
        }

        public Criteria andAddress2IsNull() {
            addCriterion("ADDRESS2 is null");
            return (Criteria) this;
        }

        public Criteria andAddress2IsNotNull() {
            addCriterion("ADDRESS2 is not null");
            return (Criteria) this;
        }

        public Criteria andAddress2EqualTo(String value) {
            addCriterion("ADDRESS2 =", value, "address2");
            return (Criteria) this;
        }

        public Criteria andAddress2NotEqualTo(String value) {
            addCriterion("ADDRESS2 <>", value, "address2");
            return (Criteria) this;
        }

        public Criteria andAddress2GreaterThan(String value) {
            addCriterion("ADDRESS2 >", value, "address2");
            return (Criteria) this;
        }

        public Criteria andAddress2GreaterThanOrEqualTo(String value) {
            addCriterion("ADDRESS2 >=", value, "address2");
            return (Criteria) this;
        }

        public Criteria andAddress2LessThan(String value) {
            addCriterion("ADDRESS2 <", value, "address2");
            return (Criteria) this;
        }

        public Criteria andAddress2LessThanOrEqualTo(String value) {
            addCriterion("ADDRESS2 <=", value, "address2");
            return (Criteria) this;
        }

        public Criteria andAddress2Like(String value) {
            addCriterion("ADDRESS2 like", value, "address2");
            return (Criteria) this;
        }

        public Criteria andAddress2NotLike(String value) {
            addCriterion("ADDRESS2 not like", value, "address2");
            return (Criteria) this;
        }

        public Criteria andAddress2In(List<String> values) {
            addCriterion("ADDRESS2 in", values, "address2");
            return (Criteria) this;
        }

        public Criteria andAddress2NotIn(List<String> values) {
            addCriterion("ADDRESS2 not in", values, "address2");
            return (Criteria) this;
        }

        public Criteria andAddress2Between(String value1, String value2) {
            addCriterion("ADDRESS2 between", value1, value2, "address2");
            return (Criteria) this;
        }

        public Criteria andAddress2NotBetween(String value1, String value2) {
            addCriterion("ADDRESS2 not between", value1, value2, "address2");
            return (Criteria) this;
        }

        public Criteria andSemplidIsNull() {
            addCriterion("SEMPLID is null");
            return (Criteria) this;
        }

        public Criteria andSemplidIsNotNull() {
            addCriterion("SEMPLID is not null");
            return (Criteria) this;
        }

        public Criteria andSemplidEqualTo(String value) {
            addCriterion("SEMPLID =", value, "semplid");
            return (Criteria) this;
        }

        public Criteria andSemplidNotEqualTo(String value) {
            addCriterion("SEMPLID <>", value, "semplid");
            return (Criteria) this;
        }

        public Criteria andSemplidGreaterThan(String value) {
            addCriterion("SEMPLID >", value, "semplid");
            return (Criteria) this;
        }

        public Criteria andSemplidGreaterThanOrEqualTo(String value) {
            addCriterion("SEMPLID >=", value, "semplid");
            return (Criteria) this;
        }

        public Criteria andSemplidLessThan(String value) {
            addCriterion("SEMPLID <", value, "semplid");
            return (Criteria) this;
        }

        public Criteria andSemplidLessThanOrEqualTo(String value) {
            addCriterion("SEMPLID <=", value, "semplid");
            return (Criteria) this;
        }

        public Criteria andSemplidLike(String value) {
            addCriterion("SEMPLID like", value, "semplid");
            return (Criteria) this;
        }

        public Criteria andSemplidNotLike(String value) {
            addCriterion("SEMPLID not like", value, "semplid");
            return (Criteria) this;
        }

        public Criteria andSemplidIn(List<String> values) {
            addCriterion("SEMPLID in", values, "semplid");
            return (Criteria) this;
        }

        public Criteria andSemplidNotIn(List<String> values) {
            addCriterion("SEMPLID not in", values, "semplid");
            return (Criteria) this;
        }

        public Criteria andSemplidBetween(String value1, String value2) {
            addCriterion("SEMPLID between", value1, value2, "semplid");
            return (Criteria) this;
        }

        public Criteria andSemplidNotBetween(String value1, String value2) {
            addCriterion("SEMPLID not between", value1, value2, "semplid");
            return (Criteria) this;
        }

        public Criteria andEbsPersonidIsNull() {
            addCriterion("EBS_PERSONID is null");
            return (Criteria) this;
        }

        public Criteria andEbsPersonidIsNotNull() {
            addCriterion("EBS_PERSONID is not null");
            return (Criteria) this;
        }

        public Criteria andEbsPersonidEqualTo(String value) {
            addCriterion("EBS_PERSONID =", value, "ebsPersonid");
            return (Criteria) this;
        }

        public Criteria andEbsPersonidNotEqualTo(String value) {
            addCriterion("EBS_PERSONID <>", value, "ebsPersonid");
            return (Criteria) this;
        }

        public Criteria andEbsPersonidGreaterThan(String value) {
            addCriterion("EBS_PERSONID >", value, "ebsPersonid");
            return (Criteria) this;
        }

        public Criteria andEbsPersonidGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_PERSONID >=", value, "ebsPersonid");
            return (Criteria) this;
        }

        public Criteria andEbsPersonidLessThan(String value) {
            addCriterion("EBS_PERSONID <", value, "ebsPersonid");
            return (Criteria) this;
        }

        public Criteria andEbsPersonidLessThanOrEqualTo(String value) {
            addCriterion("EBS_PERSONID <=", value, "ebsPersonid");
            return (Criteria) this;
        }

        public Criteria andEbsPersonidLike(String value) {
            addCriterion("EBS_PERSONID like", value, "ebsPersonid");
            return (Criteria) this;
        }

        public Criteria andEbsPersonidNotLike(String value) {
            addCriterion("EBS_PERSONID not like", value, "ebsPersonid");
            return (Criteria) this;
        }

        public Criteria andEbsPersonidIn(List<String> values) {
            addCriterion("EBS_PERSONID in", values, "ebsPersonid");
            return (Criteria) this;
        }

        public Criteria andEbsPersonidNotIn(List<String> values) {
            addCriterion("EBS_PERSONID not in", values, "ebsPersonid");
            return (Criteria) this;
        }

        public Criteria andEbsPersonidBetween(String value1, String value2) {
            addCriterion("EBS_PERSONID between", value1, value2, "ebsPersonid");
            return (Criteria) this;
        }

        public Criteria andEbsPersonidNotBetween(String value1, String value2) {
            addCriterion("EBS_PERSONID not between", value1, value2, "ebsPersonid");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIsNull() {
            addCriterion("EMPLOYEE_NUMBER is null");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIsNotNull() {
            addCriterion("EMPLOYEE_NUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER =", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER <>", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberGreaterThan(String value) {
            addCriterion("EMPLOYEE_NUMBER >", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberGreaterThanOrEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER >=", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLessThan(String value) {
            addCriterion("EMPLOYEE_NUMBER <", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLessThanOrEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER <=", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLike(String value) {
            addCriterion("EMPLOYEE_NUMBER like", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotLike(String value) {
            addCriterion("EMPLOYEE_NUMBER not like", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIn(List<String> values) {
            addCriterion("EMPLOYEE_NUMBER in", values, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotIn(List<String> values) {
            addCriterion("EMPLOYEE_NUMBER not in", values, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberBetween(String value1, String value2) {
            addCriterion("EMPLOYEE_NUMBER between", value1, value2, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotBetween(String value1, String value2) {
            addCriterion("EMPLOYEE_NUMBER not between", value1, value2, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andStdNoIsNull() {
            addCriterion("STD_NO is null");
            return (Criteria) this;
        }

        public Criteria andStdNoIsNotNull() {
            addCriterion("STD_NO is not null");
            return (Criteria) this;
        }

        public Criteria andStdNoEqualTo(String value) {
            addCriterion("STD_NO =", value, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoNotEqualTo(String value) {
            addCriterion("STD_NO <>", value, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoGreaterThan(String value) {
            addCriterion("STD_NO >", value, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoGreaterThanOrEqualTo(String value) {
            addCriterion("STD_NO >=", value, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoLessThan(String value) {
            addCriterion("STD_NO <", value, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoLessThanOrEqualTo(String value) {
            addCriterion("STD_NO <=", value, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoLike(String value) {
            addCriterion("STD_NO like", value, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoNotLike(String value) {
            addCriterion("STD_NO not like", value, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoIn(List<String> values) {
            addCriterion("STD_NO in", values, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoNotIn(List<String> values) {
            addCriterion("STD_NO not in", values, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoBetween(String value1, String value2) {
            addCriterion("STD_NO between", value1, value2, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoNotBetween(String value1, String value2) {
            addCriterion("STD_NO not between", value1, value2, "stdNo");
            return (Criteria) this;
        }

        public Criteria andAcadOrgIsNull() {
            addCriterion("ACAD_ORG is null");
            return (Criteria) this;
        }

        public Criteria andAcadOrgIsNotNull() {
            addCriterion("ACAD_ORG is not null");
            return (Criteria) this;
        }

        public Criteria andAcadOrgEqualTo(String value) {
            addCriterion("ACAD_ORG =", value, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgNotEqualTo(String value) {
            addCriterion("ACAD_ORG <>", value, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgGreaterThan(String value) {
            addCriterion("ACAD_ORG >", value, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_ORG >=", value, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgLessThan(String value) {
            addCriterion("ACAD_ORG <", value, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgLessThanOrEqualTo(String value) {
            addCriterion("ACAD_ORG <=", value, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgLike(String value) {
            addCriterion("ACAD_ORG like", value, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgNotLike(String value) {
            addCriterion("ACAD_ORG not like", value, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgIn(List<String> values) {
            addCriterion("ACAD_ORG in", values, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgNotIn(List<String> values) {
            addCriterion("ACAD_ORG not in", values, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgBetween(String value1, String value2) {
            addCriterion("ACAD_ORG between", value1, value2, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgNotBetween(String value1, String value2) {
            addCriterion("ACAD_ORG not between", value1, value2, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadNameIsNull() {
            addCriterion("ACAD_NAME is null");
            return (Criteria) this;
        }

        public Criteria andAcadNameIsNotNull() {
            addCriterion("ACAD_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andAcadNameEqualTo(String value) {
            addCriterion("ACAD_NAME =", value, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameNotEqualTo(String value) {
            addCriterion("ACAD_NAME <>", value, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameGreaterThan(String value) {
            addCriterion("ACAD_NAME >", value, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_NAME >=", value, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameLessThan(String value) {
            addCriterion("ACAD_NAME <", value, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameLessThanOrEqualTo(String value) {
            addCriterion("ACAD_NAME <=", value, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameLike(String value) {
            addCriterion("ACAD_NAME like", value, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameNotLike(String value) {
            addCriterion("ACAD_NAME not like", value, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameIn(List<String> values) {
            addCriterion("ACAD_NAME in", values, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameNotIn(List<String> values) {
            addCriterion("ACAD_NAME not in", values, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameBetween(String value1, String value2) {
            addCriterion("ACAD_NAME between", value1, value2, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameNotBetween(String value1, String value2) {
            addCriterion("ACAD_NAME not between", value1, value2, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdIsNull() {
            addCriterion("ACAD_KST_ORG_ID is null");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdIsNotNull() {
            addCriterion("ACAD_KST_ORG_ID is not null");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdEqualTo(String value) {
            addCriterion("ACAD_KST_ORG_ID =", value, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdNotEqualTo(String value) {
            addCriterion("ACAD_KST_ORG_ID <>", value, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdGreaterThan(String value) {
            addCriterion("ACAD_KST_ORG_ID >", value, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_KST_ORG_ID >=", value, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdLessThan(String value) {
            addCriterion("ACAD_KST_ORG_ID <", value, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdLessThanOrEqualTo(String value) {
            addCriterion("ACAD_KST_ORG_ID <=", value, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdLike(String value) {
            addCriterion("ACAD_KST_ORG_ID like", value, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdNotLike(String value) {
            addCriterion("ACAD_KST_ORG_ID not like", value, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdIn(List<String> values) {
            addCriterion("ACAD_KST_ORG_ID in", values, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdNotIn(List<String> values) {
            addCriterion("ACAD_KST_ORG_ID not in", values, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdBetween(String value1, String value2) {
            addCriterion("ACAD_KST_ORG_ID between", value1, value2, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdNotBetween(String value1, String value2) {
            addCriterion("ACAD_KST_ORG_ID not between", value1, value2, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdIsNull() {
            addCriterion("ACAD_EBS_ORG_ID is null");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdIsNotNull() {
            addCriterion("ACAD_EBS_ORG_ID is not null");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_ID =", value, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdNotEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_ID <>", value, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdGreaterThan(String value) {
            addCriterion("ACAD_EBS_ORG_ID >", value, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_ID >=", value, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdLessThan(String value) {
            addCriterion("ACAD_EBS_ORG_ID <", value, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdLessThanOrEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_ID <=", value, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdLike(String value) {
            addCriterion("ACAD_EBS_ORG_ID like", value, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdNotLike(String value) {
            addCriterion("ACAD_EBS_ORG_ID not like", value, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdIn(List<String> values) {
            addCriterion("ACAD_EBS_ORG_ID in", values, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdNotIn(List<String> values) {
            addCriterion("ACAD_EBS_ORG_ID not in", values, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdBetween(String value1, String value2) {
            addCriterion("ACAD_EBS_ORG_ID between", value1, value2, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdNotBetween(String value1, String value2) {
            addCriterion("ACAD_EBS_ORG_ID not between", value1, value2, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngIsNull() {
            addCriterion("ACAD_EBS_ORG_NAME_ENG is null");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngIsNotNull() {
            addCriterion("ACAD_EBS_ORG_NAME_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG =", value, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngNotEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG <>", value, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngGreaterThan(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG >", value, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG >=", value, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngLessThan(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG <", value, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngLessThanOrEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG <=", value, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngLike(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG like", value, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngNotLike(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG not like", value, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngIn(List<String> values) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG in", values, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngNotIn(List<String> values) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG not in", values, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngBetween(String value1, String value2) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG between", value1, value2, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngNotBetween(String value1, String value2) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG not between", value1, value2, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorIsNull() {
            addCriterion("ACAD_EBS_ORG_NAME_KOR is null");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorIsNotNull() {
            addCriterion("ACAD_EBS_ORG_NAME_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR =", value, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorNotEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR <>", value, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorGreaterThan(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR >", value, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR >=", value, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorLessThan(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR <", value, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorLessThanOrEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR <=", value, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorLike(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR like", value, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorNotLike(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR not like", value, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorIn(List<String> values) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR in", values, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorNotIn(List<String> values) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR not in", values, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorBetween(String value1, String value2) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR between", value1, value2, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorNotBetween(String value1, String value2) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR not between", value1, value2, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andCampusIsNull() {
            addCriterion("CAMPUS is null");
            return (Criteria) this;
        }

        public Criteria andCampusIsNotNull() {
            addCriterion("CAMPUS is not null");
            return (Criteria) this;
        }

        public Criteria andCampusEqualTo(String value) {
            addCriterion("CAMPUS =", value, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusNotEqualTo(String value) {
            addCriterion("CAMPUS <>", value, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusGreaterThan(String value) {
            addCriterion("CAMPUS >", value, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusGreaterThanOrEqualTo(String value) {
            addCriterion("CAMPUS >=", value, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusLessThan(String value) {
            addCriterion("CAMPUS <", value, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusLessThanOrEqualTo(String value) {
            addCriterion("CAMPUS <=", value, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusLike(String value) {
            addCriterion("CAMPUS like", value, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusNotLike(String value) {
            addCriterion("CAMPUS not like", value, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusIn(List<String> values) {
            addCriterion("CAMPUS in", values, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusNotIn(List<String> values) {
            addCriterion("CAMPUS not in", values, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusBetween(String value1, String value2) {
            addCriterion("CAMPUS between", value1, value2, "campus");
            return (Criteria) this;
        }

        public Criteria andCampusNotBetween(String value1, String value2) {
            addCriterion("CAMPUS not between", value1, value2, "campus");
            return (Criteria) this;
        }

        public Criteria andCollegeEngIsNull() {
            addCriterion("COLLEGE_ENG is null");
            return (Criteria) this;
        }

        public Criteria andCollegeEngIsNotNull() {
            addCriterion("COLLEGE_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andCollegeEngEqualTo(String value) {
            addCriterion("COLLEGE_ENG =", value, "collegeEng");
            return (Criteria) this;
        }

        public Criteria andCollegeEngNotEqualTo(String value) {
            addCriterion("COLLEGE_ENG <>", value, "collegeEng");
            return (Criteria) this;
        }

        public Criteria andCollegeEngGreaterThan(String value) {
            addCriterion("COLLEGE_ENG >", value, "collegeEng");
            return (Criteria) this;
        }

        public Criteria andCollegeEngGreaterThanOrEqualTo(String value) {
            addCriterion("COLLEGE_ENG >=", value, "collegeEng");
            return (Criteria) this;
        }

        public Criteria andCollegeEngLessThan(String value) {
            addCriterion("COLLEGE_ENG <", value, "collegeEng");
            return (Criteria) this;
        }

        public Criteria andCollegeEngLessThanOrEqualTo(String value) {
            addCriterion("COLLEGE_ENG <=", value, "collegeEng");
            return (Criteria) this;
        }

        public Criteria andCollegeEngLike(String value) {
            addCriterion("COLLEGE_ENG like", value, "collegeEng");
            return (Criteria) this;
        }

        public Criteria andCollegeEngNotLike(String value) {
            addCriterion("COLLEGE_ENG not like", value, "collegeEng");
            return (Criteria) this;
        }

        public Criteria andCollegeEngIn(List<String> values) {
            addCriterion("COLLEGE_ENG in", values, "collegeEng");
            return (Criteria) this;
        }

        public Criteria andCollegeEngNotIn(List<String> values) {
            addCriterion("COLLEGE_ENG not in", values, "collegeEng");
            return (Criteria) this;
        }

        public Criteria andCollegeEngBetween(String value1, String value2) {
            addCriterion("COLLEGE_ENG between", value1, value2, "collegeEng");
            return (Criteria) this;
        }

        public Criteria andCollegeEngNotBetween(String value1, String value2) {
            addCriterion("COLLEGE_ENG not between", value1, value2, "collegeEng");
            return (Criteria) this;
        }

        public Criteria andCollegeKorIsNull() {
            addCriterion("COLLEGE_KOR is null");
            return (Criteria) this;
        }

        public Criteria andCollegeKorIsNotNull() {
            addCriterion("COLLEGE_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andCollegeKorEqualTo(String value) {
            addCriterion("COLLEGE_KOR =", value, "collegeKor");
            return (Criteria) this;
        }

        public Criteria andCollegeKorNotEqualTo(String value) {
            addCriterion("COLLEGE_KOR <>", value, "collegeKor");
            return (Criteria) this;
        }

        public Criteria andCollegeKorGreaterThan(String value) {
            addCriterion("COLLEGE_KOR >", value, "collegeKor");
            return (Criteria) this;
        }

        public Criteria andCollegeKorGreaterThanOrEqualTo(String value) {
            addCriterion("COLLEGE_KOR >=", value, "collegeKor");
            return (Criteria) this;
        }

        public Criteria andCollegeKorLessThan(String value) {
            addCriterion("COLLEGE_KOR <", value, "collegeKor");
            return (Criteria) this;
        }

        public Criteria andCollegeKorLessThanOrEqualTo(String value) {
            addCriterion("COLLEGE_KOR <=", value, "collegeKor");
            return (Criteria) this;
        }

        public Criteria andCollegeKorLike(String value) {
            addCriterion("COLLEGE_KOR like", value, "collegeKor");
            return (Criteria) this;
        }

        public Criteria andCollegeKorNotLike(String value) {
            addCriterion("COLLEGE_KOR not like", value, "collegeKor");
            return (Criteria) this;
        }

        public Criteria andCollegeKorIn(List<String> values) {
            addCriterion("COLLEGE_KOR in", values, "collegeKor");
            return (Criteria) this;
        }

        public Criteria andCollegeKorNotIn(List<String> values) {
            addCriterion("COLLEGE_KOR not in", values, "collegeKor");
            return (Criteria) this;
        }

        public Criteria andCollegeKorBetween(String value1, String value2) {
            addCriterion("COLLEGE_KOR between", value1, value2, "collegeKor");
            return (Criteria) this;
        }

        public Criteria andCollegeKorNotBetween(String value1, String value2) {
            addCriterion("COLLEGE_KOR not between", value1, value2, "collegeKor");
            return (Criteria) this;
        }

        public Criteria andGradEngIsNull() {
            addCriterion("GRAD_ENG is null");
            return (Criteria) this;
        }

        public Criteria andGradEngIsNotNull() {
            addCriterion("GRAD_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andGradEngEqualTo(String value) {
            addCriterion("GRAD_ENG =", value, "gradEng");
            return (Criteria) this;
        }

        public Criteria andGradEngNotEqualTo(String value) {
            addCriterion("GRAD_ENG <>", value, "gradEng");
            return (Criteria) this;
        }

        public Criteria andGradEngGreaterThan(String value) {
            addCriterion("GRAD_ENG >", value, "gradEng");
            return (Criteria) this;
        }

        public Criteria andGradEngGreaterThanOrEqualTo(String value) {
            addCriterion("GRAD_ENG >=", value, "gradEng");
            return (Criteria) this;
        }

        public Criteria andGradEngLessThan(String value) {
            addCriterion("GRAD_ENG <", value, "gradEng");
            return (Criteria) this;
        }

        public Criteria andGradEngLessThanOrEqualTo(String value) {
            addCriterion("GRAD_ENG <=", value, "gradEng");
            return (Criteria) this;
        }

        public Criteria andGradEngLike(String value) {
            addCriterion("GRAD_ENG like", value, "gradEng");
            return (Criteria) this;
        }

        public Criteria andGradEngNotLike(String value) {
            addCriterion("GRAD_ENG not like", value, "gradEng");
            return (Criteria) this;
        }

        public Criteria andGradEngIn(List<String> values) {
            addCriterion("GRAD_ENG in", values, "gradEng");
            return (Criteria) this;
        }

        public Criteria andGradEngNotIn(List<String> values) {
            addCriterion("GRAD_ENG not in", values, "gradEng");
            return (Criteria) this;
        }

        public Criteria andGradEngBetween(String value1, String value2) {
            addCriterion("GRAD_ENG between", value1, value2, "gradEng");
            return (Criteria) this;
        }

        public Criteria andGradEngNotBetween(String value1, String value2) {
            addCriterion("GRAD_ENG not between", value1, value2, "gradEng");
            return (Criteria) this;
        }

        public Criteria andGradKorIsNull() {
            addCriterion("GRAD_KOR is null");
            return (Criteria) this;
        }

        public Criteria andGradKorIsNotNull() {
            addCriterion("GRAD_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andGradKorEqualTo(String value) {
            addCriterion("GRAD_KOR =", value, "gradKor");
            return (Criteria) this;
        }

        public Criteria andGradKorNotEqualTo(String value) {
            addCriterion("GRAD_KOR <>", value, "gradKor");
            return (Criteria) this;
        }

        public Criteria andGradKorGreaterThan(String value) {
            addCriterion("GRAD_KOR >", value, "gradKor");
            return (Criteria) this;
        }

        public Criteria andGradKorGreaterThanOrEqualTo(String value) {
            addCriterion("GRAD_KOR >=", value, "gradKor");
            return (Criteria) this;
        }

        public Criteria andGradKorLessThan(String value) {
            addCriterion("GRAD_KOR <", value, "gradKor");
            return (Criteria) this;
        }

        public Criteria andGradKorLessThanOrEqualTo(String value) {
            addCriterion("GRAD_KOR <=", value, "gradKor");
            return (Criteria) this;
        }

        public Criteria andGradKorLike(String value) {
            addCriterion("GRAD_KOR like", value, "gradKor");
            return (Criteria) this;
        }

        public Criteria andGradKorNotLike(String value) {
            addCriterion("GRAD_KOR not like", value, "gradKor");
            return (Criteria) this;
        }

        public Criteria andGradKorIn(List<String> values) {
            addCriterion("GRAD_KOR in", values, "gradKor");
            return (Criteria) this;
        }

        public Criteria andGradKorNotIn(List<String> values) {
            addCriterion("GRAD_KOR not in", values, "gradKor");
            return (Criteria) this;
        }

        public Criteria andGradKorBetween(String value1, String value2) {
            addCriterion("GRAD_KOR between", value1, value2, "gradKor");
            return (Criteria) this;
        }

        public Criteria andGradKorNotBetween(String value1, String value2) {
            addCriterion("GRAD_KOR not between", value1, value2, "gradKor");
            return (Criteria) this;
        }

        public Criteria andKaistOrgIdIsNull() {
            addCriterion("KAIST_ORG_ID is null");
            return (Criteria) this;
        }

        public Criteria andKaistOrgIdIsNotNull() {
            addCriterion("KAIST_ORG_ID is not null");
            return (Criteria) this;
        }

        public Criteria andKaistOrgIdEqualTo(String value) {
            addCriterion("KAIST_ORG_ID =", value, "kaistOrgId");
            return (Criteria) this;
        }

        public Criteria andKaistOrgIdNotEqualTo(String value) {
            addCriterion("KAIST_ORG_ID <>", value, "kaistOrgId");
            return (Criteria) this;
        }

        public Criteria andKaistOrgIdGreaterThan(String value) {
            addCriterion("KAIST_ORG_ID >", value, "kaistOrgId");
            return (Criteria) this;
        }

        public Criteria andKaistOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("KAIST_ORG_ID >=", value, "kaistOrgId");
            return (Criteria) this;
        }

        public Criteria andKaistOrgIdLessThan(String value) {
            addCriterion("KAIST_ORG_ID <", value, "kaistOrgId");
            return (Criteria) this;
        }

        public Criteria andKaistOrgIdLessThanOrEqualTo(String value) {
            addCriterion("KAIST_ORG_ID <=", value, "kaistOrgId");
            return (Criteria) this;
        }

        public Criteria andKaistOrgIdLike(String value) {
            addCriterion("KAIST_ORG_ID like", value, "kaistOrgId");
            return (Criteria) this;
        }

        public Criteria andKaistOrgIdNotLike(String value) {
            addCriterion("KAIST_ORG_ID not like", value, "kaistOrgId");
            return (Criteria) this;
        }

        public Criteria andKaistOrgIdIn(List<String> values) {
            addCriterion("KAIST_ORG_ID in", values, "kaistOrgId");
            return (Criteria) this;
        }

        public Criteria andKaistOrgIdNotIn(List<String> values) {
            addCriterion("KAIST_ORG_ID not in", values, "kaistOrgId");
            return (Criteria) this;
        }

        public Criteria andKaistOrgIdBetween(String value1, String value2) {
            addCriterion("KAIST_ORG_ID between", value1, value2, "kaistOrgId");
            return (Criteria) this;
        }

        public Criteria andKaistOrgIdNotBetween(String value1, String value2) {
            addCriterion("KAIST_ORG_ID not between", value1, value2, "kaistOrgId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdIsNull() {
            addCriterion("EBS_ORGANIZATION_ID is null");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdIsNotNull() {
            addCriterion("EBS_ORGANIZATION_ID is not null");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdEqualTo(String value) {
            addCriterion("EBS_ORGANIZATION_ID =", value, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdNotEqualTo(String value) {
            addCriterion("EBS_ORGANIZATION_ID <>", value, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdGreaterThan(String value) {
            addCriterion("EBS_ORGANIZATION_ID >", value, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_ORGANIZATION_ID >=", value, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdLessThan(String value) {
            addCriterion("EBS_ORGANIZATION_ID <", value, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdLessThanOrEqualTo(String value) {
            addCriterion("EBS_ORGANIZATION_ID <=", value, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdLike(String value) {
            addCriterion("EBS_ORGANIZATION_ID like", value, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdNotLike(String value) {
            addCriterion("EBS_ORGANIZATION_ID not like", value, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdIn(List<String> values) {
            addCriterion("EBS_ORGANIZATION_ID in", values, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdNotIn(List<String> values) {
            addCriterion("EBS_ORGANIZATION_ID not in", values, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdBetween(String value1, String value2) {
            addCriterion("EBS_ORGANIZATION_ID between", value1, value2, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdNotBetween(String value1, String value2) {
            addCriterion("EBS_ORGANIZATION_ID not between", value1, value2, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngIsNull() {
            addCriterion("EBS_ORG_NAME_ENG is null");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngIsNotNull() {
            addCriterion("EBS_ORG_NAME_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngEqualTo(String value) {
            addCriterion("EBS_ORG_NAME_ENG =", value, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngNotEqualTo(String value) {
            addCriterion("EBS_ORG_NAME_ENG <>", value, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngGreaterThan(String value) {
            addCriterion("EBS_ORG_NAME_ENG >", value, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_ORG_NAME_ENG >=", value, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngLessThan(String value) {
            addCriterion("EBS_ORG_NAME_ENG <", value, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngLessThanOrEqualTo(String value) {
            addCriterion("EBS_ORG_NAME_ENG <=", value, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngLike(String value) {
            addCriterion("EBS_ORG_NAME_ENG like", value, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngNotLike(String value) {
            addCriterion("EBS_ORG_NAME_ENG not like", value, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngIn(List<String> values) {
            addCriterion("EBS_ORG_NAME_ENG in", values, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngNotIn(List<String> values) {
            addCriterion("EBS_ORG_NAME_ENG not in", values, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngBetween(String value1, String value2) {
            addCriterion("EBS_ORG_NAME_ENG between", value1, value2, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngNotBetween(String value1, String value2) {
            addCriterion("EBS_ORG_NAME_ENG not between", value1, value2, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorIsNull() {
            addCriterion("EBS_ORG_NAME_KOR is null");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorIsNotNull() {
            addCriterion("EBS_ORG_NAME_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorEqualTo(String value) {
            addCriterion("EBS_ORG_NAME_KOR =", value, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorNotEqualTo(String value) {
            addCriterion("EBS_ORG_NAME_KOR <>", value, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorGreaterThan(String value) {
            addCriterion("EBS_ORG_NAME_KOR >", value, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_ORG_NAME_KOR >=", value, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorLessThan(String value) {
            addCriterion("EBS_ORG_NAME_KOR <", value, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorLessThanOrEqualTo(String value) {
            addCriterion("EBS_ORG_NAME_KOR <=", value, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorLike(String value) {
            addCriterion("EBS_ORG_NAME_KOR like", value, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorNotLike(String value) {
            addCriterion("EBS_ORG_NAME_KOR not like", value, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorIn(List<String> values) {
            addCriterion("EBS_ORG_NAME_KOR in", values, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorNotIn(List<String> values) {
            addCriterion("EBS_ORG_NAME_KOR not in", values, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorBetween(String value1, String value2) {
            addCriterion("EBS_ORG_NAME_KOR between", value1, value2, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorNotBetween(String value1, String value2) {
            addCriterion("EBS_ORG_NAME_KOR not between", value1, value2, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngIsNull() {
            addCriterion("EBS_GRADE_NAME_ENG is null");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngIsNotNull() {
            addCriterion("EBS_GRADE_NAME_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngEqualTo(String value) {
            addCriterion("EBS_GRADE_NAME_ENG =", value, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngNotEqualTo(String value) {
            addCriterion("EBS_GRADE_NAME_ENG <>", value, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngGreaterThan(String value) {
            addCriterion("EBS_GRADE_NAME_ENG >", value, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_GRADE_NAME_ENG >=", value, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngLessThan(String value) {
            addCriterion("EBS_GRADE_NAME_ENG <", value, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngLessThanOrEqualTo(String value) {
            addCriterion("EBS_GRADE_NAME_ENG <=", value, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngLike(String value) {
            addCriterion("EBS_GRADE_NAME_ENG like", value, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngNotLike(String value) {
            addCriterion("EBS_GRADE_NAME_ENG not like", value, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngIn(List<String> values) {
            addCriterion("EBS_GRADE_NAME_ENG in", values, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngNotIn(List<String> values) {
            addCriterion("EBS_GRADE_NAME_ENG not in", values, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngBetween(String value1, String value2) {
            addCriterion("EBS_GRADE_NAME_ENG between", value1, value2, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngNotBetween(String value1, String value2) {
            addCriterion("EBS_GRADE_NAME_ENG not between", value1, value2, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorIsNull() {
            addCriterion("EBS_GRADE_NAME_KOR is null");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorIsNotNull() {
            addCriterion("EBS_GRADE_NAME_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorEqualTo(String value) {
            addCriterion("EBS_GRADE_NAME_KOR =", value, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorNotEqualTo(String value) {
            addCriterion("EBS_GRADE_NAME_KOR <>", value, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorGreaterThan(String value) {
            addCriterion("EBS_GRADE_NAME_KOR >", value, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_GRADE_NAME_KOR >=", value, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorLessThan(String value) {
            addCriterion("EBS_GRADE_NAME_KOR <", value, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorLessThanOrEqualTo(String value) {
            addCriterion("EBS_GRADE_NAME_KOR <=", value, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorLike(String value) {
            addCriterion("EBS_GRADE_NAME_KOR like", value, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorNotLike(String value) {
            addCriterion("EBS_GRADE_NAME_KOR not like", value, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorIn(List<String> values) {
            addCriterion("EBS_GRADE_NAME_KOR in", values, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorNotIn(List<String> values) {
            addCriterion("EBS_GRADE_NAME_KOR not in", values, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorBetween(String value1, String value2) {
            addCriterion("EBS_GRADE_NAME_KOR between", value1, value2, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorNotBetween(String value1, String value2) {
            addCriterion("EBS_GRADE_NAME_KOR not between", value1, value2, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngIsNull() {
            addCriterion("EBS_GRADE_LEVEL_ENG is null");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngIsNotNull() {
            addCriterion("EBS_GRADE_LEVEL_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngEqualTo(String value) {
            addCriterion("EBS_GRADE_LEVEL_ENG =", value, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngNotEqualTo(String value) {
            addCriterion("EBS_GRADE_LEVEL_ENG <>", value, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngGreaterThan(String value) {
            addCriterion("EBS_GRADE_LEVEL_ENG >", value, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_GRADE_LEVEL_ENG >=", value, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngLessThan(String value) {
            addCriterion("EBS_GRADE_LEVEL_ENG <", value, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngLessThanOrEqualTo(String value) {
            addCriterion("EBS_GRADE_LEVEL_ENG <=", value, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngLike(String value) {
            addCriterion("EBS_GRADE_LEVEL_ENG like", value, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngNotLike(String value) {
            addCriterion("EBS_GRADE_LEVEL_ENG not like", value, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngIn(List<String> values) {
            addCriterion("EBS_GRADE_LEVEL_ENG in", values, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngNotIn(List<String> values) {
            addCriterion("EBS_GRADE_LEVEL_ENG not in", values, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngBetween(String value1, String value2) {
            addCriterion("EBS_GRADE_LEVEL_ENG between", value1, value2, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngNotBetween(String value1, String value2) {
            addCriterion("EBS_GRADE_LEVEL_ENG not between", value1, value2, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorIsNull() {
            addCriterion("EBS_GRADE_LEVEL_KOR is null");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorIsNotNull() {
            addCriterion("EBS_GRADE_LEVEL_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorEqualTo(String value) {
            addCriterion("EBS_GRADE_LEVEL_KOR =", value, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorNotEqualTo(String value) {
            addCriterion("EBS_GRADE_LEVEL_KOR <>", value, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorGreaterThan(String value) {
            addCriterion("EBS_GRADE_LEVEL_KOR >", value, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_GRADE_LEVEL_KOR >=", value, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorLessThan(String value) {
            addCriterion("EBS_GRADE_LEVEL_KOR <", value, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorLessThanOrEqualTo(String value) {
            addCriterion("EBS_GRADE_LEVEL_KOR <=", value, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorLike(String value) {
            addCriterion("EBS_GRADE_LEVEL_KOR like", value, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorNotLike(String value) {
            addCriterion("EBS_GRADE_LEVEL_KOR not like", value, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorIn(List<String> values) {
            addCriterion("EBS_GRADE_LEVEL_KOR in", values, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorNotIn(List<String> values) {
            addCriterion("EBS_GRADE_LEVEL_KOR not in", values, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorBetween(String value1, String value2) {
            addCriterion("EBS_GRADE_LEVEL_KOR between", value1, value2, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorNotBetween(String value1, String value2) {
            addCriterion("EBS_GRADE_LEVEL_KOR not between", value1, value2, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngIsNull() {
            addCriterion("EBS_PERSON_TYPE_ENG is null");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngIsNotNull() {
            addCriterion("EBS_PERSON_TYPE_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngEqualTo(String value) {
            addCriterion("EBS_PERSON_TYPE_ENG =", value, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngNotEqualTo(String value) {
            addCriterion("EBS_PERSON_TYPE_ENG <>", value, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngGreaterThan(String value) {
            addCriterion("EBS_PERSON_TYPE_ENG >", value, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_PERSON_TYPE_ENG >=", value, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngLessThan(String value) {
            addCriterion("EBS_PERSON_TYPE_ENG <", value, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngLessThanOrEqualTo(String value) {
            addCriterion("EBS_PERSON_TYPE_ENG <=", value, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngLike(String value) {
            addCriterion("EBS_PERSON_TYPE_ENG like", value, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngNotLike(String value) {
            addCriterion("EBS_PERSON_TYPE_ENG not like", value, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngIn(List<String> values) {
            addCriterion("EBS_PERSON_TYPE_ENG in", values, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngNotIn(List<String> values) {
            addCriterion("EBS_PERSON_TYPE_ENG not in", values, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngBetween(String value1, String value2) {
            addCriterion("EBS_PERSON_TYPE_ENG between", value1, value2, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngNotBetween(String value1, String value2) {
            addCriterion("EBS_PERSON_TYPE_ENG not between", value1, value2, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorIsNull() {
            addCriterion("EBS_PERSON_TYPE_KOR is null");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorIsNotNull() {
            addCriterion("EBS_PERSON_TYPE_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorEqualTo(String value) {
            addCriterion("EBS_PERSON_TYPE_KOR =", value, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorNotEqualTo(String value) {
            addCriterion("EBS_PERSON_TYPE_KOR <>", value, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorGreaterThan(String value) {
            addCriterion("EBS_PERSON_TYPE_KOR >", value, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_PERSON_TYPE_KOR >=", value, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorLessThan(String value) {
            addCriterion("EBS_PERSON_TYPE_KOR <", value, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorLessThanOrEqualTo(String value) {
            addCriterion("EBS_PERSON_TYPE_KOR <=", value, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorLike(String value) {
            addCriterion("EBS_PERSON_TYPE_KOR like", value, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorNotLike(String value) {
            addCriterion("EBS_PERSON_TYPE_KOR not like", value, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorIn(List<String> values) {
            addCriterion("EBS_PERSON_TYPE_KOR in", values, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorNotIn(List<String> values) {
            addCriterion("EBS_PERSON_TYPE_KOR not in", values, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorBetween(String value1, String value2) {
            addCriterion("EBS_PERSON_TYPE_KOR between", value1, value2, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorNotBetween(String value1, String value2) {
            addCriterion("EBS_PERSON_TYPE_KOR not between", value1, value2, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngIsNull() {
            addCriterion("EBS_USER_STATUS_ENG is null");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngIsNotNull() {
            addCriterion("EBS_USER_STATUS_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngEqualTo(String value) {
            addCriterion("EBS_USER_STATUS_ENG =", value, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngNotEqualTo(String value) {
            addCriterion("EBS_USER_STATUS_ENG <>", value, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngGreaterThan(String value) {
            addCriterion("EBS_USER_STATUS_ENG >", value, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_USER_STATUS_ENG >=", value, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngLessThan(String value) {
            addCriterion("EBS_USER_STATUS_ENG <", value, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngLessThanOrEqualTo(String value) {
            addCriterion("EBS_USER_STATUS_ENG <=", value, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngLike(String value) {
            addCriterion("EBS_USER_STATUS_ENG like", value, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngNotLike(String value) {
            addCriterion("EBS_USER_STATUS_ENG not like", value, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngIn(List<String> values) {
            addCriterion("EBS_USER_STATUS_ENG in", values, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngNotIn(List<String> values) {
            addCriterion("EBS_USER_STATUS_ENG not in", values, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngBetween(String value1, String value2) {
            addCriterion("EBS_USER_STATUS_ENG between", value1, value2, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngNotBetween(String value1, String value2) {
            addCriterion("EBS_USER_STATUS_ENG not between", value1, value2, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorIsNull() {
            addCriterion("EBS_USER_STATUS_KOR is null");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorIsNotNull() {
            addCriterion("EBS_USER_STATUS_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorEqualTo(String value) {
            addCriterion("EBS_USER_STATUS_KOR =", value, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorNotEqualTo(String value) {
            addCriterion("EBS_USER_STATUS_KOR <>", value, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorGreaterThan(String value) {
            addCriterion("EBS_USER_STATUS_KOR >", value, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_USER_STATUS_KOR >=", value, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorLessThan(String value) {
            addCriterion("EBS_USER_STATUS_KOR <", value, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorLessThanOrEqualTo(String value) {
            addCriterion("EBS_USER_STATUS_KOR <=", value, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorLike(String value) {
            addCriterion("EBS_USER_STATUS_KOR like", value, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorNotLike(String value) {
            addCriterion("EBS_USER_STATUS_KOR not like", value, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorIn(List<String> values) {
            addCriterion("EBS_USER_STATUS_KOR in", values, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorNotIn(List<String> values) {
            addCriterion("EBS_USER_STATUS_KOR not in", values, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorBetween(String value1, String value2) {
            addCriterion("EBS_USER_STATUS_KOR between", value1, value2, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorNotBetween(String value1, String value2) {
            addCriterion("EBS_USER_STATUS_KOR not between", value1, value2, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andPositionEngIsNull() {
            addCriterion("POSITION_ENG is null");
            return (Criteria) this;
        }

        public Criteria andPositionEngIsNotNull() {
            addCriterion("POSITION_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andPositionEngEqualTo(String value) {
            addCriterion("POSITION_ENG =", value, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngNotEqualTo(String value) {
            addCriterion("POSITION_ENG <>", value, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngGreaterThan(String value) {
            addCriterion("POSITION_ENG >", value, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngGreaterThanOrEqualTo(String value) {
            addCriterion("POSITION_ENG >=", value, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngLessThan(String value) {
            addCriterion("POSITION_ENG <", value, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngLessThanOrEqualTo(String value) {
            addCriterion("POSITION_ENG <=", value, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngLike(String value) {
            addCriterion("POSITION_ENG like", value, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngNotLike(String value) {
            addCriterion("POSITION_ENG not like", value, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngIn(List<String> values) {
            addCriterion("POSITION_ENG in", values, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngNotIn(List<String> values) {
            addCriterion("POSITION_ENG not in", values, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngBetween(String value1, String value2) {
            addCriterion("POSITION_ENG between", value1, value2, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngNotBetween(String value1, String value2) {
            addCriterion("POSITION_ENG not between", value1, value2, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionKorIsNull() {
            addCriterion("POSITION_KOR is null");
            return (Criteria) this;
        }

        public Criteria andPositionKorIsNotNull() {
            addCriterion("POSITION_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andPositionKorEqualTo(String value) {
            addCriterion("POSITION_KOR =", value, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorNotEqualTo(String value) {
            addCriterion("POSITION_KOR <>", value, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorGreaterThan(String value) {
            addCriterion("POSITION_KOR >", value, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorGreaterThanOrEqualTo(String value) {
            addCriterion("POSITION_KOR >=", value, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorLessThan(String value) {
            addCriterion("POSITION_KOR <", value, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorLessThanOrEqualTo(String value) {
            addCriterion("POSITION_KOR <=", value, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorLike(String value) {
            addCriterion("POSITION_KOR like", value, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorNotLike(String value) {
            addCriterion("POSITION_KOR not like", value, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorIn(List<String> values) {
            addCriterion("POSITION_KOR in", values, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorNotIn(List<String> values) {
            addCriterion("POSITION_KOR not in", values, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorBetween(String value1, String value2) {
            addCriterion("POSITION_KOR between", value1, value2, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorNotBetween(String value1, String value2) {
            addCriterion("POSITION_KOR not between", value1, value2, "positionKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusCodeIsNull() {
            addCriterion("STU_STATUS_CODE is null");
            return (Criteria) this;
        }

        public Criteria andStuStatusCodeIsNotNull() {
            addCriterion("STU_STATUS_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andStuStatusCodeEqualTo(String value) {
            addCriterion("STU_STATUS_CODE =", value, "stuStatusCode");
            return (Criteria) this;
        }

        public Criteria andStuStatusCodeNotEqualTo(String value) {
            addCriterion("STU_STATUS_CODE <>", value, "stuStatusCode");
            return (Criteria) this;
        }

        public Criteria andStuStatusCodeGreaterThan(String value) {
            addCriterion("STU_STATUS_CODE >", value, "stuStatusCode");
            return (Criteria) this;
        }

        public Criteria andStuStatusCodeGreaterThanOrEqualTo(String value) {
            addCriterion("STU_STATUS_CODE >=", value, "stuStatusCode");
            return (Criteria) this;
        }

        public Criteria andStuStatusCodeLessThan(String value) {
            addCriterion("STU_STATUS_CODE <", value, "stuStatusCode");
            return (Criteria) this;
        }

        public Criteria andStuStatusCodeLessThanOrEqualTo(String value) {
            addCriterion("STU_STATUS_CODE <=", value, "stuStatusCode");
            return (Criteria) this;
        }

        public Criteria andStuStatusCodeLike(String value) {
            addCriterion("STU_STATUS_CODE like", value, "stuStatusCode");
            return (Criteria) this;
        }

        public Criteria andStuStatusCodeNotLike(String value) {
            addCriterion("STU_STATUS_CODE not like", value, "stuStatusCode");
            return (Criteria) this;
        }

        public Criteria andStuStatusCodeIn(List<String> values) {
            addCriterion("STU_STATUS_CODE in", values, "stuStatusCode");
            return (Criteria) this;
        }

        public Criteria andStuStatusCodeNotIn(List<String> values) {
            addCriterion("STU_STATUS_CODE not in", values, "stuStatusCode");
            return (Criteria) this;
        }

        public Criteria andStuStatusCodeBetween(String value1, String value2) {
            addCriterion("STU_STATUS_CODE between", value1, value2, "stuStatusCode");
            return (Criteria) this;
        }

        public Criteria andStuStatusCodeNotBetween(String value1, String value2) {
            addCriterion("STU_STATUS_CODE not between", value1, value2, "stuStatusCode");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngIsNull() {
            addCriterion("STU_STATUS_ENG is null");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngIsNotNull() {
            addCriterion("STU_STATUS_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngEqualTo(String value) {
            addCriterion("STU_STATUS_ENG =", value, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngNotEqualTo(String value) {
            addCriterion("STU_STATUS_ENG <>", value, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngGreaterThan(String value) {
            addCriterion("STU_STATUS_ENG >", value, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngGreaterThanOrEqualTo(String value) {
            addCriterion("STU_STATUS_ENG >=", value, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngLessThan(String value) {
            addCriterion("STU_STATUS_ENG <", value, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngLessThanOrEqualTo(String value) {
            addCriterion("STU_STATUS_ENG <=", value, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngLike(String value) {
            addCriterion("STU_STATUS_ENG like", value, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngNotLike(String value) {
            addCriterion("STU_STATUS_ENG not like", value, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngIn(List<String> values) {
            addCriterion("STU_STATUS_ENG in", values, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngNotIn(List<String> values) {
            addCriterion("STU_STATUS_ENG not in", values, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngBetween(String value1, String value2) {
            addCriterion("STU_STATUS_ENG between", value1, value2, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngNotBetween(String value1, String value2) {
            addCriterion("STU_STATUS_ENG not between", value1, value2, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorIsNull() {
            addCriterion("STU_STATUS_KOR is null");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorIsNotNull() {
            addCriterion("STU_STATUS_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorEqualTo(String value) {
            addCriterion("STU_STATUS_KOR =", value, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorNotEqualTo(String value) {
            addCriterion("STU_STATUS_KOR <>", value, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorGreaterThan(String value) {
            addCriterion("STU_STATUS_KOR >", value, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorGreaterThanOrEqualTo(String value) {
            addCriterion("STU_STATUS_KOR >=", value, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorLessThan(String value) {
            addCriterion("STU_STATUS_KOR <", value, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorLessThanOrEqualTo(String value) {
            addCriterion("STU_STATUS_KOR <=", value, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorLike(String value) {
            addCriterion("STU_STATUS_KOR like", value, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorNotLike(String value) {
            addCriterion("STU_STATUS_KOR not like", value, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorIn(List<String> values) {
            addCriterion("STU_STATUS_KOR in", values, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorNotIn(List<String> values) {
            addCriterion("STU_STATUS_KOR not in", values, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorBetween(String value1, String value2) {
            addCriterion("STU_STATUS_KOR between", value1, value2, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorNotBetween(String value1, String value2) {
            addCriterion("STU_STATUS_KOR not between", value1, value2, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeIsNull() {
            addCriterion("ACAD_PROG_CODE is null");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeIsNotNull() {
            addCriterion("ACAD_PROG_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeEqualTo(String value) {
            addCriterion("ACAD_PROG_CODE =", value, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeNotEqualTo(String value) {
            addCriterion("ACAD_PROG_CODE <>", value, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeGreaterThan(String value) {
            addCriterion("ACAD_PROG_CODE >", value, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_PROG_CODE >=", value, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeLessThan(String value) {
            addCriterion("ACAD_PROG_CODE <", value, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeLessThanOrEqualTo(String value) {
            addCriterion("ACAD_PROG_CODE <=", value, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeLike(String value) {
            addCriterion("ACAD_PROG_CODE like", value, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeNotLike(String value) {
            addCriterion("ACAD_PROG_CODE not like", value, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeIn(List<String> values) {
            addCriterion("ACAD_PROG_CODE in", values, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeNotIn(List<String> values) {
            addCriterion("ACAD_PROG_CODE not in", values, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeBetween(String value1, String value2) {
            addCriterion("ACAD_PROG_CODE between", value1, value2, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeNotBetween(String value1, String value2) {
            addCriterion("ACAD_PROG_CODE not between", value1, value2, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorIsNull() {
            addCriterion("ACAD_PROG_KOR is null");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorIsNotNull() {
            addCriterion("ACAD_PROG_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorEqualTo(String value) {
            addCriterion("ACAD_PROG_KOR =", value, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorNotEqualTo(String value) {
            addCriterion("ACAD_PROG_KOR <>", value, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorGreaterThan(String value) {
            addCriterion("ACAD_PROG_KOR >", value, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_PROG_KOR >=", value, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorLessThan(String value) {
            addCriterion("ACAD_PROG_KOR <", value, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorLessThanOrEqualTo(String value) {
            addCriterion("ACAD_PROG_KOR <=", value, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorLike(String value) {
            addCriterion("ACAD_PROG_KOR like", value, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorNotLike(String value) {
            addCriterion("ACAD_PROG_KOR not like", value, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorIn(List<String> values) {
            addCriterion("ACAD_PROG_KOR in", values, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorNotIn(List<String> values) {
            addCriterion("ACAD_PROG_KOR not in", values, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorBetween(String value1, String value2) {
            addCriterion("ACAD_PROG_KOR between", value1, value2, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorNotBetween(String value1, String value2) {
            addCriterion("ACAD_PROG_KOR not between", value1, value2, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngIsNull() {
            addCriterion("ACAD_PROG_ENG is null");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngIsNotNull() {
            addCriterion("ACAD_PROG_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngEqualTo(String value) {
            addCriterion("ACAD_PROG_ENG =", value, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngNotEqualTo(String value) {
            addCriterion("ACAD_PROG_ENG <>", value, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngGreaterThan(String value) {
            addCriterion("ACAD_PROG_ENG >", value, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_PROG_ENG >=", value, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngLessThan(String value) {
            addCriterion("ACAD_PROG_ENG <", value, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngLessThanOrEqualTo(String value) {
            addCriterion("ACAD_PROG_ENG <=", value, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngLike(String value) {
            addCriterion("ACAD_PROG_ENG like", value, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngNotLike(String value) {
            addCriterion("ACAD_PROG_ENG not like", value, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngIn(List<String> values) {
            addCriterion("ACAD_PROG_ENG in", values, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngNotIn(List<String> values) {
            addCriterion("ACAD_PROG_ENG not in", values, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngBetween(String value1, String value2) {
            addCriterion("ACAD_PROG_ENG between", value1, value2, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngNotBetween(String value1, String value2) {
            addCriterion("ACAD_PROG_ENG not between", value1, value2, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andPersonGubunIsNull() {
            addCriterion("PERSON_GUBUN is null");
            return (Criteria) this;
        }

        public Criteria andPersonGubunIsNotNull() {
            addCriterion("PERSON_GUBUN is not null");
            return (Criteria) this;
        }

        public Criteria andPersonGubunEqualTo(String value) {
            addCriterion("PERSON_GUBUN =", value, "personGubun");
            return (Criteria) this;
        }

        public Criteria andPersonGubunNotEqualTo(String value) {
            addCriterion("PERSON_GUBUN <>", value, "personGubun");
            return (Criteria) this;
        }

        public Criteria andPersonGubunGreaterThan(String value) {
            addCriterion("PERSON_GUBUN >", value, "personGubun");
            return (Criteria) this;
        }

        public Criteria andPersonGubunGreaterThanOrEqualTo(String value) {
            addCriterion("PERSON_GUBUN >=", value, "personGubun");
            return (Criteria) this;
        }

        public Criteria andPersonGubunLessThan(String value) {
            addCriterion("PERSON_GUBUN <", value, "personGubun");
            return (Criteria) this;
        }

        public Criteria andPersonGubunLessThanOrEqualTo(String value) {
            addCriterion("PERSON_GUBUN <=", value, "personGubun");
            return (Criteria) this;
        }

        public Criteria andPersonGubunLike(String value) {
            addCriterion("PERSON_GUBUN like", value, "personGubun");
            return (Criteria) this;
        }

        public Criteria andPersonGubunNotLike(String value) {
            addCriterion("PERSON_GUBUN not like", value, "personGubun");
            return (Criteria) this;
        }

        public Criteria andPersonGubunIn(List<String> values) {
            addCriterion("PERSON_GUBUN in", values, "personGubun");
            return (Criteria) this;
        }

        public Criteria andPersonGubunNotIn(List<String> values) {
            addCriterion("PERSON_GUBUN not in", values, "personGubun");
            return (Criteria) this;
        }

        public Criteria andPersonGubunBetween(String value1, String value2) {
            addCriterion("PERSON_GUBUN between", value1, value2, "personGubun");
            return (Criteria) this;
        }

        public Criteria andPersonGubunNotBetween(String value1, String value2) {
            addCriterion("PERSON_GUBUN not between", value1, value2, "personGubun");
            return (Criteria) this;
        }

        public Criteria andProgEffdtIsNull() {
            addCriterion("PROG_EFFDT is null");
            return (Criteria) this;
        }

        public Criteria andProgEffdtIsNotNull() {
            addCriterion("PROG_EFFDT is not null");
            return (Criteria) this;
        }

        public Criteria andProgEffdtEqualTo(String value) {
            addCriterion("PROG_EFFDT =", value, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtNotEqualTo(String value) {
            addCriterion("PROG_EFFDT <>", value, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtGreaterThan(String value) {
            addCriterion("PROG_EFFDT >", value, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtGreaterThanOrEqualTo(String value) {
            addCriterion("PROG_EFFDT >=", value, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtLessThan(String value) {
            addCriterion("PROG_EFFDT <", value, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtLessThanOrEqualTo(String value) {
            addCriterion("PROG_EFFDT <=", value, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtLike(String value) {
            addCriterion("PROG_EFFDT like", value, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtNotLike(String value) {
            addCriterion("PROG_EFFDT not like", value, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtIn(List<String> values) {
            addCriterion("PROG_EFFDT in", values, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtNotIn(List<String> values) {
            addCriterion("PROG_EFFDT not in", values, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtBetween(String value1, String value2) {
            addCriterion("PROG_EFFDT between", value1, value2, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtNotBetween(String value1, String value2) {
            addCriterion("PROG_EFFDT not between", value1, value2, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdIsNull() {
            addCriterion("STDNT_TYPE_ID is null");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdIsNotNull() {
            addCriterion("STDNT_TYPE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdEqualTo(String value) {
            addCriterion("STDNT_TYPE_ID =", value, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdNotEqualTo(String value) {
            addCriterion("STDNT_TYPE_ID <>", value, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdGreaterThan(String value) {
            addCriterion("STDNT_TYPE_ID >", value, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdGreaterThanOrEqualTo(String value) {
            addCriterion("STDNT_TYPE_ID >=", value, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdLessThan(String value) {
            addCriterion("STDNT_TYPE_ID <", value, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdLessThanOrEqualTo(String value) {
            addCriterion("STDNT_TYPE_ID <=", value, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdLike(String value) {
            addCriterion("STDNT_TYPE_ID like", value, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdNotLike(String value) {
            addCriterion("STDNT_TYPE_ID not like", value, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdIn(List<String> values) {
            addCriterion("STDNT_TYPE_ID in", values, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdNotIn(List<String> values) {
            addCriterion("STDNT_TYPE_ID not in", values, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdBetween(String value1, String value2) {
            addCriterion("STDNT_TYPE_ID between", value1, value2, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdNotBetween(String value1, String value2) {
            addCriterion("STDNT_TYPE_ID not between", value1, value2, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassIsNull() {
            addCriterion("STDNT_TYPE_CLASS is null");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassIsNotNull() {
            addCriterion("STDNT_TYPE_CLASS is not null");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassEqualTo(String value) {
            addCriterion("STDNT_TYPE_CLASS =", value, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassNotEqualTo(String value) {
            addCriterion("STDNT_TYPE_CLASS <>", value, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassGreaterThan(String value) {
            addCriterion("STDNT_TYPE_CLASS >", value, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassGreaterThanOrEqualTo(String value) {
            addCriterion("STDNT_TYPE_CLASS >=", value, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassLessThan(String value) {
            addCriterion("STDNT_TYPE_CLASS <", value, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassLessThanOrEqualTo(String value) {
            addCriterion("STDNT_TYPE_CLASS <=", value, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassLike(String value) {
            addCriterion("STDNT_TYPE_CLASS like", value, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassNotLike(String value) {
            addCriterion("STDNT_TYPE_CLASS not like", value, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassIn(List<String> values) {
            addCriterion("STDNT_TYPE_CLASS in", values, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassNotIn(List<String> values) {
            addCriterion("STDNT_TYPE_CLASS not in", values, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassBetween(String value1, String value2) {
            addCriterion("STDNT_TYPE_CLASS between", value1, value2, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassNotBetween(String value1, String value2) {
            addCriterion("STDNT_TYPE_CLASS not between", value1, value2, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameEngIsNull() {
            addCriterion("STDNT_TYPE_NAME_ENG is null");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameEngIsNotNull() {
            addCriterion("STDNT_TYPE_NAME_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameEngEqualTo(String value) {
            addCriterion("STDNT_TYPE_NAME_ENG =", value, "stdntTypeNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameEngNotEqualTo(String value) {
            addCriterion("STDNT_TYPE_NAME_ENG <>", value, "stdntTypeNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameEngGreaterThan(String value) {
            addCriterion("STDNT_TYPE_NAME_ENG >", value, "stdntTypeNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameEngGreaterThanOrEqualTo(String value) {
            addCriterion("STDNT_TYPE_NAME_ENG >=", value, "stdntTypeNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameEngLessThan(String value) {
            addCriterion("STDNT_TYPE_NAME_ENG <", value, "stdntTypeNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameEngLessThanOrEqualTo(String value) {
            addCriterion("STDNT_TYPE_NAME_ENG <=", value, "stdntTypeNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameEngLike(String value) {
            addCriterion("STDNT_TYPE_NAME_ENG like", value, "stdntTypeNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameEngNotLike(String value) {
            addCriterion("STDNT_TYPE_NAME_ENG not like", value, "stdntTypeNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameEngIn(List<String> values) {
            addCriterion("STDNT_TYPE_NAME_ENG in", values, "stdntTypeNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameEngNotIn(List<String> values) {
            addCriterion("STDNT_TYPE_NAME_ENG not in", values, "stdntTypeNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameEngBetween(String value1, String value2) {
            addCriterion("STDNT_TYPE_NAME_ENG between", value1, value2, "stdntTypeNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameEngNotBetween(String value1, String value2) {
            addCriterion("STDNT_TYPE_NAME_ENG not between", value1, value2, "stdntTypeNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameKorIsNull() {
            addCriterion("STDNT_TYPE_NAME_KOR is null");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameKorIsNotNull() {
            addCriterion("STDNT_TYPE_NAME_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameKorEqualTo(String value) {
            addCriterion("STDNT_TYPE_NAME_KOR =", value, "stdntTypeNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameKorNotEqualTo(String value) {
            addCriterion("STDNT_TYPE_NAME_KOR <>", value, "stdntTypeNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameKorGreaterThan(String value) {
            addCriterion("STDNT_TYPE_NAME_KOR >", value, "stdntTypeNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameKorGreaterThanOrEqualTo(String value) {
            addCriterion("STDNT_TYPE_NAME_KOR >=", value, "stdntTypeNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameKorLessThan(String value) {
            addCriterion("STDNT_TYPE_NAME_KOR <", value, "stdntTypeNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameKorLessThanOrEqualTo(String value) {
            addCriterion("STDNT_TYPE_NAME_KOR <=", value, "stdntTypeNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameKorLike(String value) {
            addCriterion("STDNT_TYPE_NAME_KOR like", value, "stdntTypeNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameKorNotLike(String value) {
            addCriterion("STDNT_TYPE_NAME_KOR not like", value, "stdntTypeNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameKorIn(List<String> values) {
            addCriterion("STDNT_TYPE_NAME_KOR in", values, "stdntTypeNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameKorNotIn(List<String> values) {
            addCriterion("STDNT_TYPE_NAME_KOR not in", values, "stdntTypeNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameKorBetween(String value1, String value2) {
            addCriterion("STDNT_TYPE_NAME_KOR between", value1, value2, "stdntTypeNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameKorNotBetween(String value1, String value2) {
            addCriterion("STDNT_TYPE_NAME_KOR not between", value1, value2, "stdntTypeNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdIsNull() {
            addCriterion("STDNT_CATEGORY_ID is null");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdIsNotNull() {
            addCriterion("STDNT_CATEGORY_ID is not null");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdEqualTo(String value) {
            addCriterion("STDNT_CATEGORY_ID =", value, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdNotEqualTo(String value) {
            addCriterion("STDNT_CATEGORY_ID <>", value, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdGreaterThan(String value) {
            addCriterion("STDNT_CATEGORY_ID >", value, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdGreaterThanOrEqualTo(String value) {
            addCriterion("STDNT_CATEGORY_ID >=", value, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdLessThan(String value) {
            addCriterion("STDNT_CATEGORY_ID <", value, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdLessThanOrEqualTo(String value) {
            addCriterion("STDNT_CATEGORY_ID <=", value, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdLike(String value) {
            addCriterion("STDNT_CATEGORY_ID like", value, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdNotLike(String value) {
            addCriterion("STDNT_CATEGORY_ID not like", value, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdIn(List<String> values) {
            addCriterion("STDNT_CATEGORY_ID in", values, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdNotIn(List<String> values) {
            addCriterion("STDNT_CATEGORY_ID not in", values, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdBetween(String value1, String value2) {
            addCriterion("STDNT_CATEGORY_ID between", value1, value2, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdNotBetween(String value1, String value2) {
            addCriterion("STDNT_CATEGORY_ID not between", value1, value2, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameEngIsNull() {
            addCriterion("STDNT_CATEGORY_NAME_ENG is null");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameEngIsNotNull() {
            addCriterion("STDNT_CATEGORY_NAME_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameEngEqualTo(String value) {
            addCriterion("STDNT_CATEGORY_NAME_ENG =", value, "stdntCategoryNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameEngNotEqualTo(String value) {
            addCriterion("STDNT_CATEGORY_NAME_ENG <>", value, "stdntCategoryNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameEngGreaterThan(String value) {
            addCriterion("STDNT_CATEGORY_NAME_ENG >", value, "stdntCategoryNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameEngGreaterThanOrEqualTo(String value) {
            addCriterion("STDNT_CATEGORY_NAME_ENG >=", value, "stdntCategoryNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameEngLessThan(String value) {
            addCriterion("STDNT_CATEGORY_NAME_ENG <", value, "stdntCategoryNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameEngLessThanOrEqualTo(String value) {
            addCriterion("STDNT_CATEGORY_NAME_ENG <=", value, "stdntCategoryNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameEngLike(String value) {
            addCriterion("STDNT_CATEGORY_NAME_ENG like", value, "stdntCategoryNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameEngNotLike(String value) {
            addCriterion("STDNT_CATEGORY_NAME_ENG not like", value, "stdntCategoryNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameEngIn(List<String> values) {
            addCriterion("STDNT_CATEGORY_NAME_ENG in", values, "stdntCategoryNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameEngNotIn(List<String> values) {
            addCriterion("STDNT_CATEGORY_NAME_ENG not in", values, "stdntCategoryNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameEngBetween(String value1, String value2) {
            addCriterion("STDNT_CATEGORY_NAME_ENG between", value1, value2, "stdntCategoryNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameEngNotBetween(String value1, String value2) {
            addCriterion("STDNT_CATEGORY_NAME_ENG not between", value1, value2, "stdntCategoryNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameKorIsNull() {
            addCriterion("STDNT_CATEGORY_NAME_KOR is null");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameKorIsNotNull() {
            addCriterion("STDNT_CATEGORY_NAME_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameKorEqualTo(String value) {
            addCriterion("STDNT_CATEGORY_NAME_KOR =", value, "stdntCategoryNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameKorNotEqualTo(String value) {
            addCriterion("STDNT_CATEGORY_NAME_KOR <>", value, "stdntCategoryNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameKorGreaterThan(String value) {
            addCriterion("STDNT_CATEGORY_NAME_KOR >", value, "stdntCategoryNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameKorGreaterThanOrEqualTo(String value) {
            addCriterion("STDNT_CATEGORY_NAME_KOR >=", value, "stdntCategoryNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameKorLessThan(String value) {
            addCriterion("STDNT_CATEGORY_NAME_KOR <", value, "stdntCategoryNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameKorLessThanOrEqualTo(String value) {
            addCriterion("STDNT_CATEGORY_NAME_KOR <=", value, "stdntCategoryNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameKorLike(String value) {
            addCriterion("STDNT_CATEGORY_NAME_KOR like", value, "stdntCategoryNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameKorNotLike(String value) {
            addCriterion("STDNT_CATEGORY_NAME_KOR not like", value, "stdntCategoryNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameKorIn(List<String> values) {
            addCriterion("STDNT_CATEGORY_NAME_KOR in", values, "stdntCategoryNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameKorNotIn(List<String> values) {
            addCriterion("STDNT_CATEGORY_NAME_KOR not in", values, "stdntCategoryNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameKorBetween(String value1, String value2) {
            addCriterion("STDNT_CATEGORY_NAME_KOR between", value1, value2, "stdntCategoryNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameKorNotBetween(String value1, String value2) {
            addCriterion("STDNT_CATEGORY_NAME_KOR not between", value1, value2, "stdntCategoryNameKor");
            return (Criteria) this;
        }

        public Criteria andBankSectIsNull() {
            addCriterion("BANK_SECT is null");
            return (Criteria) this;
        }

        public Criteria andBankSectIsNotNull() {
            addCriterion("BANK_SECT is not null");
            return (Criteria) this;
        }

        public Criteria andBankSectEqualTo(String value) {
            addCriterion("BANK_SECT =", value, "bankSect");
            return (Criteria) this;
        }

        public Criteria andBankSectNotEqualTo(String value) {
            addCriterion("BANK_SECT <>", value, "bankSect");
            return (Criteria) this;
        }

        public Criteria andBankSectGreaterThan(String value) {
            addCriterion("BANK_SECT >", value, "bankSect");
            return (Criteria) this;
        }

        public Criteria andBankSectGreaterThanOrEqualTo(String value) {
            addCriterion("BANK_SECT >=", value, "bankSect");
            return (Criteria) this;
        }

        public Criteria andBankSectLessThan(String value) {
            addCriterion("BANK_SECT <", value, "bankSect");
            return (Criteria) this;
        }

        public Criteria andBankSectLessThanOrEqualTo(String value) {
            addCriterion("BANK_SECT <=", value, "bankSect");
            return (Criteria) this;
        }

        public Criteria andBankSectLike(String value) {
            addCriterion("BANK_SECT like", value, "bankSect");
            return (Criteria) this;
        }

        public Criteria andBankSectNotLike(String value) {
            addCriterion("BANK_SECT not like", value, "bankSect");
            return (Criteria) this;
        }

        public Criteria andBankSectIn(List<String> values) {
            addCriterion("BANK_SECT in", values, "bankSect");
            return (Criteria) this;
        }

        public Criteria andBankSectNotIn(List<String> values) {
            addCriterion("BANK_SECT not in", values, "bankSect");
            return (Criteria) this;
        }

        public Criteria andBankSectBetween(String value1, String value2) {
            addCriterion("BANK_SECT between", value1, value2, "bankSect");
            return (Criteria) this;
        }

        public Criteria andBankSectNotBetween(String value1, String value2) {
            addCriterion("BANK_SECT not between", value1, value2, "bankSect");
            return (Criteria) this;
        }

        public Criteria andAccountNoIsNull() {
            addCriterion("ACCOUNT_NO is null");
            return (Criteria) this;
        }

        public Criteria andAccountNoIsNotNull() {
            addCriterion("ACCOUNT_NO is not null");
            return (Criteria) this;
        }

        public Criteria andAccountNoEqualTo(String value) {
            addCriterion("ACCOUNT_NO =", value, "accountNo");
            return (Criteria) this;
        }

        public Criteria andAccountNoNotEqualTo(String value) {
            addCriterion("ACCOUNT_NO <>", value, "accountNo");
            return (Criteria) this;
        }

        public Criteria andAccountNoGreaterThan(String value) {
            addCriterion("ACCOUNT_NO >", value, "accountNo");
            return (Criteria) this;
        }

        public Criteria andAccountNoGreaterThanOrEqualTo(String value) {
            addCriterion("ACCOUNT_NO >=", value, "accountNo");
            return (Criteria) this;
        }

        public Criteria andAccountNoLessThan(String value) {
            addCriterion("ACCOUNT_NO <", value, "accountNo");
            return (Criteria) this;
        }

        public Criteria andAccountNoLessThanOrEqualTo(String value) {
            addCriterion("ACCOUNT_NO <=", value, "accountNo");
            return (Criteria) this;
        }

        public Criteria andAccountNoLike(String value) {
            addCriterion("ACCOUNT_NO like", value, "accountNo");
            return (Criteria) this;
        }

        public Criteria andAccountNoNotLike(String value) {
            addCriterion("ACCOUNT_NO not like", value, "accountNo");
            return (Criteria) this;
        }

        public Criteria andAccountNoIn(List<String> values) {
            addCriterion("ACCOUNT_NO in", values, "accountNo");
            return (Criteria) this;
        }

        public Criteria andAccountNoNotIn(List<String> values) {
            addCriterion("ACCOUNT_NO not in", values, "accountNo");
            return (Criteria) this;
        }

        public Criteria andAccountNoBetween(String value1, String value2) {
            addCriterion("ACCOUNT_NO between", value1, value2, "accountNo");
            return (Criteria) this;
        }

        public Criteria andAccountNoNotBetween(String value1, String value2) {
            addCriterion("ACCOUNT_NO not between", value1, value2, "accountNo");
            return (Criteria) this;
        }

        public Criteria andAdvrEmplidIsNull() {
            addCriterion("ADVR_EMPLID is null");
            return (Criteria) this;
        }

        public Criteria andAdvrEmplidIsNotNull() {
            addCriterion("ADVR_EMPLID is not null");
            return (Criteria) this;
        }

        public Criteria andAdvrEmplidEqualTo(String value) {
            addCriterion("ADVR_EMPLID =", value, "advrEmplid");
            return (Criteria) this;
        }

        public Criteria andAdvrEmplidNotEqualTo(String value) {
            addCriterion("ADVR_EMPLID <>", value, "advrEmplid");
            return (Criteria) this;
        }

        public Criteria andAdvrEmplidGreaterThan(String value) {
            addCriterion("ADVR_EMPLID >", value, "advrEmplid");
            return (Criteria) this;
        }

        public Criteria andAdvrEmplidGreaterThanOrEqualTo(String value) {
            addCriterion("ADVR_EMPLID >=", value, "advrEmplid");
            return (Criteria) this;
        }

        public Criteria andAdvrEmplidLessThan(String value) {
            addCriterion("ADVR_EMPLID <", value, "advrEmplid");
            return (Criteria) this;
        }

        public Criteria andAdvrEmplidLessThanOrEqualTo(String value) {
            addCriterion("ADVR_EMPLID <=", value, "advrEmplid");
            return (Criteria) this;
        }

        public Criteria andAdvrEmplidLike(String value) {
            addCriterion("ADVR_EMPLID like", value, "advrEmplid");
            return (Criteria) this;
        }

        public Criteria andAdvrEmplidNotLike(String value) {
            addCriterion("ADVR_EMPLID not like", value, "advrEmplid");
            return (Criteria) this;
        }

        public Criteria andAdvrEmplidIn(List<String> values) {
            addCriterion("ADVR_EMPLID in", values, "advrEmplid");
            return (Criteria) this;
        }

        public Criteria andAdvrEmplidNotIn(List<String> values) {
            addCriterion("ADVR_EMPLID not in", values, "advrEmplid");
            return (Criteria) this;
        }

        public Criteria andAdvrEmplidBetween(String value1, String value2) {
            addCriterion("ADVR_EMPLID between", value1, value2, "advrEmplid");
            return (Criteria) this;
        }

        public Criteria andAdvrEmplidNotBetween(String value1, String value2) {
            addCriterion("ADVR_EMPLID not between", value1, value2, "advrEmplid");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdIsNull() {
            addCriterion("ADVR_EBS_PERSON_ID is null");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdIsNotNull() {
            addCriterion("ADVR_EBS_PERSON_ID is not null");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdEqualTo(String value) {
            addCriterion("ADVR_EBS_PERSON_ID =", value, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdNotEqualTo(String value) {
            addCriterion("ADVR_EBS_PERSON_ID <>", value, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdGreaterThan(String value) {
            addCriterion("ADVR_EBS_PERSON_ID >", value, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdGreaterThanOrEqualTo(String value) {
            addCriterion("ADVR_EBS_PERSON_ID >=", value, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdLessThan(String value) {
            addCriterion("ADVR_EBS_PERSON_ID <", value, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdLessThanOrEqualTo(String value) {
            addCriterion("ADVR_EBS_PERSON_ID <=", value, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdLike(String value) {
            addCriterion("ADVR_EBS_PERSON_ID like", value, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdNotLike(String value) {
            addCriterion("ADVR_EBS_PERSON_ID not like", value, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdIn(List<String> values) {
            addCriterion("ADVR_EBS_PERSON_ID in", values, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdNotIn(List<String> values) {
            addCriterion("ADVR_EBS_PERSON_ID not in", values, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdBetween(String value1, String value2) {
            addCriterion("ADVR_EBS_PERSON_ID between", value1, value2, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdNotBetween(String value1, String value2) {
            addCriterion("ADVR_EBS_PERSON_ID not between", value1, value2, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrFirstNameIsNull() {
            addCriterion("ADVR_FIRST_NAME is null");
            return (Criteria) this;
        }

        public Criteria andAdvrFirstNameIsNotNull() {
            addCriterion("ADVR_FIRST_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andAdvrFirstNameEqualTo(String value) {
            addCriterion("ADVR_FIRST_NAME =", value, "advrFirstName");
            return (Criteria) this;
        }

        public Criteria andAdvrFirstNameNotEqualTo(String value) {
            addCriterion("ADVR_FIRST_NAME <>", value, "advrFirstName");
            return (Criteria) this;
        }

        public Criteria andAdvrFirstNameGreaterThan(String value) {
            addCriterion("ADVR_FIRST_NAME >", value, "advrFirstName");
            return (Criteria) this;
        }

        public Criteria andAdvrFirstNameGreaterThanOrEqualTo(String value) {
            addCriterion("ADVR_FIRST_NAME >=", value, "advrFirstName");
            return (Criteria) this;
        }

        public Criteria andAdvrFirstNameLessThan(String value) {
            addCriterion("ADVR_FIRST_NAME <", value, "advrFirstName");
            return (Criteria) this;
        }

        public Criteria andAdvrFirstNameLessThanOrEqualTo(String value) {
            addCriterion("ADVR_FIRST_NAME <=", value, "advrFirstName");
            return (Criteria) this;
        }

        public Criteria andAdvrFirstNameLike(String value) {
            addCriterion("ADVR_FIRST_NAME like", value, "advrFirstName");
            return (Criteria) this;
        }

        public Criteria andAdvrFirstNameNotLike(String value) {
            addCriterion("ADVR_FIRST_NAME not like", value, "advrFirstName");
            return (Criteria) this;
        }

        public Criteria andAdvrFirstNameIn(List<String> values) {
            addCriterion("ADVR_FIRST_NAME in", values, "advrFirstName");
            return (Criteria) this;
        }

        public Criteria andAdvrFirstNameNotIn(List<String> values) {
            addCriterion("ADVR_FIRST_NAME not in", values, "advrFirstName");
            return (Criteria) this;
        }

        public Criteria andAdvrFirstNameBetween(String value1, String value2) {
            addCriterion("ADVR_FIRST_NAME between", value1, value2, "advrFirstName");
            return (Criteria) this;
        }

        public Criteria andAdvrFirstNameNotBetween(String value1, String value2) {
            addCriterion("ADVR_FIRST_NAME not between", value1, value2, "advrFirstName");
            return (Criteria) this;
        }

        public Criteria andAdvrLastNameIsNull() {
            addCriterion("ADVR_LAST_NAME is null");
            return (Criteria) this;
        }

        public Criteria andAdvrLastNameIsNotNull() {
            addCriterion("ADVR_LAST_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andAdvrLastNameEqualTo(String value) {
            addCriterion("ADVR_LAST_NAME =", value, "advrLastName");
            return (Criteria) this;
        }

        public Criteria andAdvrLastNameNotEqualTo(String value) {
            addCriterion("ADVR_LAST_NAME <>", value, "advrLastName");
            return (Criteria) this;
        }

        public Criteria andAdvrLastNameGreaterThan(String value) {
            addCriterion("ADVR_LAST_NAME >", value, "advrLastName");
            return (Criteria) this;
        }

        public Criteria andAdvrLastNameGreaterThanOrEqualTo(String value) {
            addCriterion("ADVR_LAST_NAME >=", value, "advrLastName");
            return (Criteria) this;
        }

        public Criteria andAdvrLastNameLessThan(String value) {
            addCriterion("ADVR_LAST_NAME <", value, "advrLastName");
            return (Criteria) this;
        }

        public Criteria andAdvrLastNameLessThanOrEqualTo(String value) {
            addCriterion("ADVR_LAST_NAME <=", value, "advrLastName");
            return (Criteria) this;
        }

        public Criteria andAdvrLastNameLike(String value) {
            addCriterion("ADVR_LAST_NAME like", value, "advrLastName");
            return (Criteria) this;
        }

        public Criteria andAdvrLastNameNotLike(String value) {
            addCriterion("ADVR_LAST_NAME not like", value, "advrLastName");
            return (Criteria) this;
        }

        public Criteria andAdvrLastNameIn(List<String> values) {
            addCriterion("ADVR_LAST_NAME in", values, "advrLastName");
            return (Criteria) this;
        }

        public Criteria andAdvrLastNameNotIn(List<String> values) {
            addCriterion("ADVR_LAST_NAME not in", values, "advrLastName");
            return (Criteria) this;
        }

        public Criteria andAdvrLastNameBetween(String value1, String value2) {
            addCriterion("ADVR_LAST_NAME between", value1, value2, "advrLastName");
            return (Criteria) this;
        }

        public Criteria andAdvrLastNameNotBetween(String value1, String value2) {
            addCriterion("ADVR_LAST_NAME not between", value1, value2, "advrLastName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameIsNull() {
            addCriterion("ADVR_NAME is null");
            return (Criteria) this;
        }

        public Criteria andAdvrNameIsNotNull() {
            addCriterion("ADVR_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andAdvrNameEqualTo(String value) {
            addCriterion("ADVR_NAME =", value, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameNotEqualTo(String value) {
            addCriterion("ADVR_NAME <>", value, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameGreaterThan(String value) {
            addCriterion("ADVR_NAME >", value, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameGreaterThanOrEqualTo(String value) {
            addCriterion("ADVR_NAME >=", value, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameLessThan(String value) {
            addCriterion("ADVR_NAME <", value, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameLessThanOrEqualTo(String value) {
            addCriterion("ADVR_NAME <=", value, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameLike(String value) {
            addCriterion("ADVR_NAME like", value, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameNotLike(String value) {
            addCriterion("ADVR_NAME not like", value, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameIn(List<String> values) {
            addCriterion("ADVR_NAME in", values, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameNotIn(List<String> values) {
            addCriterion("ADVR_NAME not in", values, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameBetween(String value1, String value2) {
            addCriterion("ADVR_NAME between", value1, value2, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameNotBetween(String value1, String value2) {
            addCriterion("ADVR_NAME not between", value1, value2, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcIsNull() {
            addCriterion("ADVR_NAME_AC is null");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcIsNotNull() {
            addCriterion("ADVR_NAME_AC is not null");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcEqualTo(String value) {
            addCriterion("ADVR_NAME_AC =", value, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcNotEqualTo(String value) {
            addCriterion("ADVR_NAME_AC <>", value, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcGreaterThan(String value) {
            addCriterion("ADVR_NAME_AC >", value, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcGreaterThanOrEqualTo(String value) {
            addCriterion("ADVR_NAME_AC >=", value, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcLessThan(String value) {
            addCriterion("ADVR_NAME_AC <", value, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcLessThanOrEqualTo(String value) {
            addCriterion("ADVR_NAME_AC <=", value, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcLike(String value) {
            addCriterion("ADVR_NAME_AC like", value, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcNotLike(String value) {
            addCriterion("ADVR_NAME_AC not like", value, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcIn(List<String> values) {
            addCriterion("ADVR_NAME_AC in", values, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcNotIn(List<String> values) {
            addCriterion("ADVR_NAME_AC not in", values, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcBetween(String value1, String value2) {
            addCriterion("ADVR_NAME_AC between", value1, value2, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcNotBetween(String value1, String value2) {
            addCriterion("ADVR_NAME_AC not between", value1, value2, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andEbsStartDateIsNull() {
            addCriterion("EBS_START_DATE is null");
            return (Criteria) this;
        }

        public Criteria andEbsStartDateIsNotNull() {
            addCriterion("EBS_START_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andEbsStartDateEqualTo(String value) {
            addCriterion("EBS_START_DATE =", value, "ebsStartDate");
            return (Criteria) this;
        }

        public Criteria andEbsStartDateNotEqualTo(String value) {
            addCriterion("EBS_START_DATE <>", value, "ebsStartDate");
            return (Criteria) this;
        }

        public Criteria andEbsStartDateGreaterThan(String value) {
            addCriterion("EBS_START_DATE >", value, "ebsStartDate");
            return (Criteria) this;
        }

        public Criteria andEbsStartDateGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_START_DATE >=", value, "ebsStartDate");
            return (Criteria) this;
        }

        public Criteria andEbsStartDateLessThan(String value) {
            addCriterion("EBS_START_DATE <", value, "ebsStartDate");
            return (Criteria) this;
        }

        public Criteria andEbsStartDateLessThanOrEqualTo(String value) {
            addCriterion("EBS_START_DATE <=", value, "ebsStartDate");
            return (Criteria) this;
        }

        public Criteria andEbsStartDateLike(String value) {
            addCriterion("EBS_START_DATE like", value, "ebsStartDate");
            return (Criteria) this;
        }

        public Criteria andEbsStartDateNotLike(String value) {
            addCriterion("EBS_START_DATE not like", value, "ebsStartDate");
            return (Criteria) this;
        }

        public Criteria andEbsStartDateIn(List<String> values) {
            addCriterion("EBS_START_DATE in", values, "ebsStartDate");
            return (Criteria) this;
        }

        public Criteria andEbsStartDateNotIn(List<String> values) {
            addCriterion("EBS_START_DATE not in", values, "ebsStartDate");
            return (Criteria) this;
        }

        public Criteria andEbsStartDateBetween(String value1, String value2) {
            addCriterion("EBS_START_DATE between", value1, value2, "ebsStartDate");
            return (Criteria) this;
        }

        public Criteria andEbsStartDateNotBetween(String value1, String value2) {
            addCriterion("EBS_START_DATE not between", value1, value2, "ebsStartDate");
            return (Criteria) this;
        }

        public Criteria andEbsEndDateIsNull() {
            addCriterion("EBS_END_DATE is null");
            return (Criteria) this;
        }

        public Criteria andEbsEndDateIsNotNull() {
            addCriterion("EBS_END_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andEbsEndDateEqualTo(String value) {
            addCriterion("EBS_END_DATE =", value, "ebsEndDate");
            return (Criteria) this;
        }

        public Criteria andEbsEndDateNotEqualTo(String value) {
            addCriterion("EBS_END_DATE <>", value, "ebsEndDate");
            return (Criteria) this;
        }

        public Criteria andEbsEndDateGreaterThan(String value) {
            addCriterion("EBS_END_DATE >", value, "ebsEndDate");
            return (Criteria) this;
        }

        public Criteria andEbsEndDateGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_END_DATE >=", value, "ebsEndDate");
            return (Criteria) this;
        }

        public Criteria andEbsEndDateLessThan(String value) {
            addCriterion("EBS_END_DATE <", value, "ebsEndDate");
            return (Criteria) this;
        }

        public Criteria andEbsEndDateLessThanOrEqualTo(String value) {
            addCriterion("EBS_END_DATE <=", value, "ebsEndDate");
            return (Criteria) this;
        }

        public Criteria andEbsEndDateLike(String value) {
            addCriterion("EBS_END_DATE like", value, "ebsEndDate");
            return (Criteria) this;
        }

        public Criteria andEbsEndDateNotLike(String value) {
            addCriterion("EBS_END_DATE not like", value, "ebsEndDate");
            return (Criteria) this;
        }

        public Criteria andEbsEndDateIn(List<String> values) {
            addCriterion("EBS_END_DATE in", values, "ebsEndDate");
            return (Criteria) this;
        }

        public Criteria andEbsEndDateNotIn(List<String> values) {
            addCriterion("EBS_END_DATE not in", values, "ebsEndDate");
            return (Criteria) this;
        }

        public Criteria andEbsEndDateBetween(String value1, String value2) {
            addCriterion("EBS_END_DATE between", value1, value2, "ebsEndDate");
            return (Criteria) this;
        }

        public Criteria andEbsEndDateNotBetween(String value1, String value2) {
            addCriterion("EBS_END_DATE not between", value1, value2, "ebsEndDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateIsNull() {
            addCriterion("PROG_START_DATE is null");
            return (Criteria) this;
        }

        public Criteria andProgStartDateIsNotNull() {
            addCriterion("PROG_START_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andProgStartDateEqualTo(String value) {
            addCriterion("PROG_START_DATE =", value, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateNotEqualTo(String value) {
            addCriterion("PROG_START_DATE <>", value, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateGreaterThan(String value) {
            addCriterion("PROG_START_DATE >", value, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateGreaterThanOrEqualTo(String value) {
            addCriterion("PROG_START_DATE >=", value, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateLessThan(String value) {
            addCriterion("PROG_START_DATE <", value, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateLessThanOrEqualTo(String value) {
            addCriterion("PROG_START_DATE <=", value, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateLike(String value) {
            addCriterion("PROG_START_DATE like", value, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateNotLike(String value) {
            addCriterion("PROG_START_DATE not like", value, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateIn(List<String> values) {
            addCriterion("PROG_START_DATE in", values, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateNotIn(List<String> values) {
            addCriterion("PROG_START_DATE not in", values, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateBetween(String value1, String value2) {
            addCriterion("PROG_START_DATE between", value1, value2, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateNotBetween(String value1, String value2) {
            addCriterion("PROG_START_DATE not between", value1, value2, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateIsNull() {
            addCriterion("PROG_END_DATE is null");
            return (Criteria) this;
        }

        public Criteria andProgEndDateIsNotNull() {
            addCriterion("PROG_END_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andProgEndDateEqualTo(String value) {
            addCriterion("PROG_END_DATE =", value, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateNotEqualTo(String value) {
            addCriterion("PROG_END_DATE <>", value, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateGreaterThan(String value) {
            addCriterion("PROG_END_DATE >", value, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateGreaterThanOrEqualTo(String value) {
            addCriterion("PROG_END_DATE >=", value, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateLessThan(String value) {
            addCriterion("PROG_END_DATE <", value, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateLessThanOrEqualTo(String value) {
            addCriterion("PROG_END_DATE <=", value, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateLike(String value) {
            addCriterion("PROG_END_DATE like", value, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateNotLike(String value) {
            addCriterion("PROG_END_DATE not like", value, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateIn(List<String> values) {
            addCriterion("PROG_END_DATE in", values, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateNotIn(List<String> values) {
            addCriterion("PROG_END_DATE not in", values, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateBetween(String value1, String value2) {
            addCriterion("PROG_END_DATE between", value1, value2, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateNotBetween(String value1, String value2) {
            addCriterion("PROG_END_DATE not between", value1, value2, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andOuterStartDateIsNull() {
            addCriterion("OUTER_START_DATE is null");
            return (Criteria) this;
        }

        public Criteria andOuterStartDateIsNotNull() {
            addCriterion("OUTER_START_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andOuterStartDateEqualTo(String value) {
            addCriterion("OUTER_START_DATE =", value, "outerStartDate");
            return (Criteria) this;
        }

        public Criteria andOuterStartDateNotEqualTo(String value) {
            addCriterion("OUTER_START_DATE <>", value, "outerStartDate");
            return (Criteria) this;
        }

        public Criteria andOuterStartDateGreaterThan(String value) {
            addCriterion("OUTER_START_DATE >", value, "outerStartDate");
            return (Criteria) this;
        }

        public Criteria andOuterStartDateGreaterThanOrEqualTo(String value) {
            addCriterion("OUTER_START_DATE >=", value, "outerStartDate");
            return (Criteria) this;
        }

        public Criteria andOuterStartDateLessThan(String value) {
            addCriterion("OUTER_START_DATE <", value, "outerStartDate");
            return (Criteria) this;
        }

        public Criteria andOuterStartDateLessThanOrEqualTo(String value) {
            addCriterion("OUTER_START_DATE <=", value, "outerStartDate");
            return (Criteria) this;
        }

        public Criteria andOuterStartDateLike(String value) {
            addCriterion("OUTER_START_DATE like", value, "outerStartDate");
            return (Criteria) this;
        }

        public Criteria andOuterStartDateNotLike(String value) {
            addCriterion("OUTER_START_DATE not like", value, "outerStartDate");
            return (Criteria) this;
        }

        public Criteria andOuterStartDateIn(List<String> values) {
            addCriterion("OUTER_START_DATE in", values, "outerStartDate");
            return (Criteria) this;
        }

        public Criteria andOuterStartDateNotIn(List<String> values) {
            addCriterion("OUTER_START_DATE not in", values, "outerStartDate");
            return (Criteria) this;
        }

        public Criteria andOuterStartDateBetween(String value1, String value2) {
            addCriterion("OUTER_START_DATE between", value1, value2, "outerStartDate");
            return (Criteria) this;
        }

        public Criteria andOuterStartDateNotBetween(String value1, String value2) {
            addCriterion("OUTER_START_DATE not between", value1, value2, "outerStartDate");
            return (Criteria) this;
        }

        public Criteria andOuterEndDateIsNull() {
            addCriterion("OUTER_END_DATE is null");
            return (Criteria) this;
        }

        public Criteria andOuterEndDateIsNotNull() {
            addCriterion("OUTER_END_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andOuterEndDateEqualTo(String value) {
            addCriterion("OUTER_END_DATE =", value, "outerEndDate");
            return (Criteria) this;
        }

        public Criteria andOuterEndDateNotEqualTo(String value) {
            addCriterion("OUTER_END_DATE <>", value, "outerEndDate");
            return (Criteria) this;
        }

        public Criteria andOuterEndDateGreaterThan(String value) {
            addCriterion("OUTER_END_DATE >", value, "outerEndDate");
            return (Criteria) this;
        }

        public Criteria andOuterEndDateGreaterThanOrEqualTo(String value) {
            addCriterion("OUTER_END_DATE >=", value, "outerEndDate");
            return (Criteria) this;
        }

        public Criteria andOuterEndDateLessThan(String value) {
            addCriterion("OUTER_END_DATE <", value, "outerEndDate");
            return (Criteria) this;
        }

        public Criteria andOuterEndDateLessThanOrEqualTo(String value) {
            addCriterion("OUTER_END_DATE <=", value, "outerEndDate");
            return (Criteria) this;
        }

        public Criteria andOuterEndDateLike(String value) {
            addCriterion("OUTER_END_DATE like", value, "outerEndDate");
            return (Criteria) this;
        }

        public Criteria andOuterEndDateNotLike(String value) {
            addCriterion("OUTER_END_DATE not like", value, "outerEndDate");
            return (Criteria) this;
        }

        public Criteria andOuterEndDateIn(List<String> values) {
            addCriterion("OUTER_END_DATE in", values, "outerEndDate");
            return (Criteria) this;
        }

        public Criteria andOuterEndDateNotIn(List<String> values) {
            addCriterion("OUTER_END_DATE not in", values, "outerEndDate");
            return (Criteria) this;
        }

        public Criteria andOuterEndDateBetween(String value1, String value2) {
            addCriterion("OUTER_END_DATE between", value1, value2, "outerEndDate");
            return (Criteria) this;
        }

        public Criteria andOuterEndDateNotBetween(String value1, String value2) {
            addCriterion("OUTER_END_DATE not between", value1, value2, "outerEndDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateIsNull() {
            addCriterion("REGISTER_DATE is null");
            return (Criteria) this;
        }

        public Criteria andRegisterDateIsNotNull() {
            addCriterion("REGISTER_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterDateEqualTo(String value) {
            addCriterion("REGISTER_DATE =", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateNotEqualTo(String value) {
            addCriterion("REGISTER_DATE <>", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateGreaterThan(String value) {
            addCriterion("REGISTER_DATE >", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateGreaterThanOrEqualTo(String value) {
            addCriterion("REGISTER_DATE >=", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateLessThan(String value) {
            addCriterion("REGISTER_DATE <", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateLessThanOrEqualTo(String value) {
            addCriterion("REGISTER_DATE <=", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateLike(String value) {
            addCriterion("REGISTER_DATE like", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateNotLike(String value) {
            addCriterion("REGISTER_DATE not like", value, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateIn(List<String> values) {
            addCriterion("REGISTER_DATE in", values, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateNotIn(List<String> values) {
            addCriterion("REGISTER_DATE not in", values, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateBetween(String value1, String value2) {
            addCriterion("REGISTER_DATE between", value1, value2, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateNotBetween(String value1, String value2) {
            addCriterion("REGISTER_DATE not between", value1, value2, "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterIpIsNull() {
            addCriterion("REGISTER_IP is null");
            return (Criteria) this;
        }

        public Criteria andRegisterIpIsNotNull() {
            addCriterion("REGISTER_IP is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterIpEqualTo(String value) {
            addCriterion("REGISTER_IP =", value, "registerIp");
            return (Criteria) this;
        }

        public Criteria andRegisterIpNotEqualTo(String value) {
            addCriterion("REGISTER_IP <>", value, "registerIp");
            return (Criteria) this;
        }

        public Criteria andRegisterIpGreaterThan(String value) {
            addCriterion("REGISTER_IP >", value, "registerIp");
            return (Criteria) this;
        }

        public Criteria andRegisterIpGreaterThanOrEqualTo(String value) {
            addCriterion("REGISTER_IP >=", value, "registerIp");
            return (Criteria) this;
        }

        public Criteria andRegisterIpLessThan(String value) {
            addCriterion("REGISTER_IP <", value, "registerIp");
            return (Criteria) this;
        }

        public Criteria andRegisterIpLessThanOrEqualTo(String value) {
            addCriterion("REGISTER_IP <=", value, "registerIp");
            return (Criteria) this;
        }

        public Criteria andRegisterIpLike(String value) {
            addCriterion("REGISTER_IP like", value, "registerIp");
            return (Criteria) this;
        }

        public Criteria andRegisterIpNotLike(String value) {
            addCriterion("REGISTER_IP not like", value, "registerIp");
            return (Criteria) this;
        }

        public Criteria andRegisterIpIn(List<String> values) {
            addCriterion("REGISTER_IP in", values, "registerIp");
            return (Criteria) this;
        }

        public Criteria andRegisterIpNotIn(List<String> values) {
            addCriterion("REGISTER_IP not in", values, "registerIp");
            return (Criteria) this;
        }

        public Criteria andRegisterIpBetween(String value1, String value2) {
            addCriterion("REGISTER_IP between", value1, value2, "registerIp");
            return (Criteria) this;
        }

        public Criteria andRegisterIpNotBetween(String value1, String value2) {
            addCriterion("REGISTER_IP not between", value1, value2, "registerIp");
            return (Criteria) this;
        }

        public Criteria andExtOrgIdIsNull() {
            addCriterion("EXT_ORG_ID is null");
            return (Criteria) this;
        }

        public Criteria andExtOrgIdIsNotNull() {
            addCriterion("EXT_ORG_ID is not null");
            return (Criteria) this;
        }

        public Criteria andExtOrgIdEqualTo(String value) {
            addCriterion("EXT_ORG_ID =", value, "extOrgId");
            return (Criteria) this;
        }

        public Criteria andExtOrgIdNotEqualTo(String value) {
            addCriterion("EXT_ORG_ID <>", value, "extOrgId");
            return (Criteria) this;
        }

        public Criteria andExtOrgIdGreaterThan(String value) {
            addCriterion("EXT_ORG_ID >", value, "extOrgId");
            return (Criteria) this;
        }

        public Criteria andExtOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("EXT_ORG_ID >=", value, "extOrgId");
            return (Criteria) this;
        }

        public Criteria andExtOrgIdLessThan(String value) {
            addCriterion("EXT_ORG_ID <", value, "extOrgId");
            return (Criteria) this;
        }

        public Criteria andExtOrgIdLessThanOrEqualTo(String value) {
            addCriterion("EXT_ORG_ID <=", value, "extOrgId");
            return (Criteria) this;
        }

        public Criteria andExtOrgIdLike(String value) {
            addCriterion("EXT_ORG_ID like", value, "extOrgId");
            return (Criteria) this;
        }

        public Criteria andExtOrgIdNotLike(String value) {
            addCriterion("EXT_ORG_ID not like", value, "extOrgId");
            return (Criteria) this;
        }

        public Criteria andExtOrgIdIn(List<String> values) {
            addCriterion("EXT_ORG_ID in", values, "extOrgId");
            return (Criteria) this;
        }

        public Criteria andExtOrgIdNotIn(List<String> values) {
            addCriterion("EXT_ORG_ID not in", values, "extOrgId");
            return (Criteria) this;
        }

        public Criteria andExtOrgIdBetween(String value1, String value2) {
            addCriterion("EXT_ORG_ID between", value1, value2, "extOrgId");
            return (Criteria) this;
        }

        public Criteria andExtOrgIdNotBetween(String value1, String value2) {
            addCriterion("EXT_ORG_ID not between", value1, value2, "extOrgId");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameEngIsNull() {
            addCriterion("EXT_ETC_ORG_NAME_ENG is null");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameEngIsNotNull() {
            addCriterion("EXT_ETC_ORG_NAME_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameEngEqualTo(String value) {
            addCriterion("EXT_ETC_ORG_NAME_ENG =", value, "extEtcOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameEngNotEqualTo(String value) {
            addCriterion("EXT_ETC_ORG_NAME_ENG <>", value, "extEtcOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameEngGreaterThan(String value) {
            addCriterion("EXT_ETC_ORG_NAME_ENG >", value, "extEtcOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameEngGreaterThanOrEqualTo(String value) {
            addCriterion("EXT_ETC_ORG_NAME_ENG >=", value, "extEtcOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameEngLessThan(String value) {
            addCriterion("EXT_ETC_ORG_NAME_ENG <", value, "extEtcOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameEngLessThanOrEqualTo(String value) {
            addCriterion("EXT_ETC_ORG_NAME_ENG <=", value, "extEtcOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameEngLike(String value) {
            addCriterion("EXT_ETC_ORG_NAME_ENG like", value, "extEtcOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameEngNotLike(String value) {
            addCriterion("EXT_ETC_ORG_NAME_ENG not like", value, "extEtcOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameEngIn(List<String> values) {
            addCriterion("EXT_ETC_ORG_NAME_ENG in", values, "extEtcOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameEngNotIn(List<String> values) {
            addCriterion("EXT_ETC_ORG_NAME_ENG not in", values, "extEtcOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameEngBetween(String value1, String value2) {
            addCriterion("EXT_ETC_ORG_NAME_ENG between", value1, value2, "extEtcOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameEngNotBetween(String value1, String value2) {
            addCriterion("EXT_ETC_ORG_NAME_ENG not between", value1, value2, "extEtcOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameKorIsNull() {
            addCriterion("EXT_ETC_ORG_NAME_KOR is null");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameKorIsNotNull() {
            addCriterion("EXT_ETC_ORG_NAME_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameKorEqualTo(String value) {
            addCriterion("EXT_ETC_ORG_NAME_KOR =", value, "extEtcOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameKorNotEqualTo(String value) {
            addCriterion("EXT_ETC_ORG_NAME_KOR <>", value, "extEtcOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameKorGreaterThan(String value) {
            addCriterion("EXT_ETC_ORG_NAME_KOR >", value, "extEtcOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameKorGreaterThanOrEqualTo(String value) {
            addCriterion("EXT_ETC_ORG_NAME_KOR >=", value, "extEtcOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameKorLessThan(String value) {
            addCriterion("EXT_ETC_ORG_NAME_KOR <", value, "extEtcOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameKorLessThanOrEqualTo(String value) {
            addCriterion("EXT_ETC_ORG_NAME_KOR <=", value, "extEtcOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameKorLike(String value) {
            addCriterion("EXT_ETC_ORG_NAME_KOR like", value, "extEtcOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameKorNotLike(String value) {
            addCriterion("EXT_ETC_ORG_NAME_KOR not like", value, "extEtcOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameKorIn(List<String> values) {
            addCriterion("EXT_ETC_ORG_NAME_KOR in", values, "extEtcOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameKorNotIn(List<String> values) {
            addCriterion("EXT_ETC_ORG_NAME_KOR not in", values, "extEtcOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameKorBetween(String value1, String value2) {
            addCriterion("EXT_ETC_ORG_NAME_KOR between", value1, value2, "extEtcOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameKorNotBetween(String value1, String value2) {
            addCriterion("EXT_ETC_ORG_NAME_KOR not between", value1, value2, "extEtcOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameEngIsNull() {
            addCriterion("EXT_ORG_NAME_ENG is null");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameEngIsNotNull() {
            addCriterion("EXT_ORG_NAME_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameEngEqualTo(String value) {
            addCriterion("EXT_ORG_NAME_ENG =", value, "extOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameEngNotEqualTo(String value) {
            addCriterion("EXT_ORG_NAME_ENG <>", value, "extOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameEngGreaterThan(String value) {
            addCriterion("EXT_ORG_NAME_ENG >", value, "extOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameEngGreaterThanOrEqualTo(String value) {
            addCriterion("EXT_ORG_NAME_ENG >=", value, "extOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameEngLessThan(String value) {
            addCriterion("EXT_ORG_NAME_ENG <", value, "extOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameEngLessThanOrEqualTo(String value) {
            addCriterion("EXT_ORG_NAME_ENG <=", value, "extOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameEngLike(String value) {
            addCriterion("EXT_ORG_NAME_ENG like", value, "extOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameEngNotLike(String value) {
            addCriterion("EXT_ORG_NAME_ENG not like", value, "extOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameEngIn(List<String> values) {
            addCriterion("EXT_ORG_NAME_ENG in", values, "extOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameEngNotIn(List<String> values) {
            addCriterion("EXT_ORG_NAME_ENG not in", values, "extOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameEngBetween(String value1, String value2) {
            addCriterion("EXT_ORG_NAME_ENG between", value1, value2, "extOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameEngNotBetween(String value1, String value2) {
            addCriterion("EXT_ORG_NAME_ENG not between", value1, value2, "extOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameKorIsNull() {
            addCriterion("EXT_ORG_NAME_KOR is null");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameKorIsNotNull() {
            addCriterion("EXT_ORG_NAME_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameKorEqualTo(String value) {
            addCriterion("EXT_ORG_NAME_KOR =", value, "extOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameKorNotEqualTo(String value) {
            addCriterion("EXT_ORG_NAME_KOR <>", value, "extOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameKorGreaterThan(String value) {
            addCriterion("EXT_ORG_NAME_KOR >", value, "extOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameKorGreaterThanOrEqualTo(String value) {
            addCriterion("EXT_ORG_NAME_KOR >=", value, "extOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameKorLessThan(String value) {
            addCriterion("EXT_ORG_NAME_KOR <", value, "extOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameKorLessThanOrEqualTo(String value) {
            addCriterion("EXT_ORG_NAME_KOR <=", value, "extOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameKorLike(String value) {
            addCriterion("EXT_ORG_NAME_KOR like", value, "extOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameKorNotLike(String value) {
            addCriterion("EXT_ORG_NAME_KOR not like", value, "extOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameKorIn(List<String> values) {
            addCriterion("EXT_ORG_NAME_KOR in", values, "extOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameKorNotIn(List<String> values) {
            addCriterion("EXT_ORG_NAME_KOR not in", values, "extOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameKorBetween(String value1, String value2) {
            addCriterion("EXT_ORG_NAME_KOR between", value1, value2, "extOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameKorNotBetween(String value1, String value2) {
            addCriterion("EXT_ORG_NAME_KOR not between", value1, value2, "extOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgCodeIsNull() {
            addCriterion("FORE_ACAD_PROG_CODE is null");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgCodeIsNotNull() {
            addCriterion("FORE_ACAD_PROG_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgCodeEqualTo(String value) {
            addCriterion("FORE_ACAD_PROG_CODE =", value, "foreAcadProgCode");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgCodeNotEqualTo(String value) {
            addCriterion("FORE_ACAD_PROG_CODE <>", value, "foreAcadProgCode");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgCodeGreaterThan(String value) {
            addCriterion("FORE_ACAD_PROG_CODE >", value, "foreAcadProgCode");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgCodeGreaterThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_PROG_CODE >=", value, "foreAcadProgCode");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgCodeLessThan(String value) {
            addCriterion("FORE_ACAD_PROG_CODE <", value, "foreAcadProgCode");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgCodeLessThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_PROG_CODE <=", value, "foreAcadProgCode");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgCodeLike(String value) {
            addCriterion("FORE_ACAD_PROG_CODE like", value, "foreAcadProgCode");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgCodeNotLike(String value) {
            addCriterion("FORE_ACAD_PROG_CODE not like", value, "foreAcadProgCode");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgCodeIn(List<String> values) {
            addCriterion("FORE_ACAD_PROG_CODE in", values, "foreAcadProgCode");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgCodeNotIn(List<String> values) {
            addCriterion("FORE_ACAD_PROG_CODE not in", values, "foreAcadProgCode");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgCodeBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_PROG_CODE between", value1, value2, "foreAcadProgCode");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgCodeNotBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_PROG_CODE not between", value1, value2, "foreAcadProgCode");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgKorIsNull() {
            addCriterion("FORE_ACAD_PROG_KOR is null");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgKorIsNotNull() {
            addCriterion("FORE_ACAD_PROG_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgKorEqualTo(String value) {
            addCriterion("FORE_ACAD_PROG_KOR =", value, "foreAcadProgKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgKorNotEqualTo(String value) {
            addCriterion("FORE_ACAD_PROG_KOR <>", value, "foreAcadProgKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgKorGreaterThan(String value) {
            addCriterion("FORE_ACAD_PROG_KOR >", value, "foreAcadProgKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgKorGreaterThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_PROG_KOR >=", value, "foreAcadProgKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgKorLessThan(String value) {
            addCriterion("FORE_ACAD_PROG_KOR <", value, "foreAcadProgKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgKorLessThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_PROG_KOR <=", value, "foreAcadProgKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgKorLike(String value) {
            addCriterion("FORE_ACAD_PROG_KOR like", value, "foreAcadProgKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgKorNotLike(String value) {
            addCriterion("FORE_ACAD_PROG_KOR not like", value, "foreAcadProgKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgKorIn(List<String> values) {
            addCriterion("FORE_ACAD_PROG_KOR in", values, "foreAcadProgKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgKorNotIn(List<String> values) {
            addCriterion("FORE_ACAD_PROG_KOR not in", values, "foreAcadProgKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgKorBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_PROG_KOR between", value1, value2, "foreAcadProgKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgKorNotBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_PROG_KOR not between", value1, value2, "foreAcadProgKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgEngIsNull() {
            addCriterion("FORE_ACAD_PROG_ENG is null");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgEngIsNotNull() {
            addCriterion("FORE_ACAD_PROG_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgEngEqualTo(String value) {
            addCriterion("FORE_ACAD_PROG_ENG =", value, "foreAcadProgEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgEngNotEqualTo(String value) {
            addCriterion("FORE_ACAD_PROG_ENG <>", value, "foreAcadProgEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgEngGreaterThan(String value) {
            addCriterion("FORE_ACAD_PROG_ENG >", value, "foreAcadProgEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgEngGreaterThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_PROG_ENG >=", value, "foreAcadProgEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgEngLessThan(String value) {
            addCriterion("FORE_ACAD_PROG_ENG <", value, "foreAcadProgEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgEngLessThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_PROG_ENG <=", value, "foreAcadProgEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgEngLike(String value) {
            addCriterion("FORE_ACAD_PROG_ENG like", value, "foreAcadProgEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgEngNotLike(String value) {
            addCriterion("FORE_ACAD_PROG_ENG not like", value, "foreAcadProgEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgEngIn(List<String> values) {
            addCriterion("FORE_ACAD_PROG_ENG in", values, "foreAcadProgEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgEngNotIn(List<String> values) {
            addCriterion("FORE_ACAD_PROG_ENG not in", values, "foreAcadProgEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgEngBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_PROG_ENG between", value1, value2, "foreAcadProgEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgEngNotBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_PROG_ENG not between", value1, value2, "foreAcadProgEng");
            return (Criteria) this;
        }

        public Criteria andForeStdNoIsNull() {
            addCriterion("FORE_STD_NO is null");
            return (Criteria) this;
        }

        public Criteria andForeStdNoIsNotNull() {
            addCriterion("FORE_STD_NO is not null");
            return (Criteria) this;
        }

        public Criteria andForeStdNoEqualTo(String value) {
            addCriterion("FORE_STD_NO =", value, "foreStdNo");
            return (Criteria) this;
        }

        public Criteria andForeStdNoNotEqualTo(String value) {
            addCriterion("FORE_STD_NO <>", value, "foreStdNo");
            return (Criteria) this;
        }

        public Criteria andForeStdNoGreaterThan(String value) {
            addCriterion("FORE_STD_NO >", value, "foreStdNo");
            return (Criteria) this;
        }

        public Criteria andForeStdNoGreaterThanOrEqualTo(String value) {
            addCriterion("FORE_STD_NO >=", value, "foreStdNo");
            return (Criteria) this;
        }

        public Criteria andForeStdNoLessThan(String value) {
            addCriterion("FORE_STD_NO <", value, "foreStdNo");
            return (Criteria) this;
        }

        public Criteria andForeStdNoLessThanOrEqualTo(String value) {
            addCriterion("FORE_STD_NO <=", value, "foreStdNo");
            return (Criteria) this;
        }

        public Criteria andForeStdNoLike(String value) {
            addCriterion("FORE_STD_NO like", value, "foreStdNo");
            return (Criteria) this;
        }

        public Criteria andForeStdNoNotLike(String value) {
            addCriterion("FORE_STD_NO not like", value, "foreStdNo");
            return (Criteria) this;
        }

        public Criteria andForeStdNoIn(List<String> values) {
            addCriterion("FORE_STD_NO in", values, "foreStdNo");
            return (Criteria) this;
        }

        public Criteria andForeStdNoNotIn(List<String> values) {
            addCriterion("FORE_STD_NO not in", values, "foreStdNo");
            return (Criteria) this;
        }

        public Criteria andForeStdNoBetween(String value1, String value2) {
            addCriterion("FORE_STD_NO between", value1, value2, "foreStdNo");
            return (Criteria) this;
        }

        public Criteria andForeStdNoNotBetween(String value1, String value2) {
            addCriterion("FORE_STD_NO not between", value1, value2, "foreStdNo");
            return (Criteria) this;
        }

        public Criteria andForeAcadOrgIsNull() {
            addCriterion("FORE_ACAD_ORG is null");
            return (Criteria) this;
        }

        public Criteria andForeAcadOrgIsNotNull() {
            addCriterion("FORE_ACAD_ORG is not null");
            return (Criteria) this;
        }

        public Criteria andForeAcadOrgEqualTo(String value) {
            addCriterion("FORE_ACAD_ORG =", value, "foreAcadOrg");
            return (Criteria) this;
        }

        public Criteria andForeAcadOrgNotEqualTo(String value) {
            addCriterion("FORE_ACAD_ORG <>", value, "foreAcadOrg");
            return (Criteria) this;
        }

        public Criteria andForeAcadOrgGreaterThan(String value) {
            addCriterion("FORE_ACAD_ORG >", value, "foreAcadOrg");
            return (Criteria) this;
        }

        public Criteria andForeAcadOrgGreaterThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_ORG >=", value, "foreAcadOrg");
            return (Criteria) this;
        }

        public Criteria andForeAcadOrgLessThan(String value) {
            addCriterion("FORE_ACAD_ORG <", value, "foreAcadOrg");
            return (Criteria) this;
        }

        public Criteria andForeAcadOrgLessThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_ORG <=", value, "foreAcadOrg");
            return (Criteria) this;
        }

        public Criteria andForeAcadOrgLike(String value) {
            addCriterion("FORE_ACAD_ORG like", value, "foreAcadOrg");
            return (Criteria) this;
        }

        public Criteria andForeAcadOrgNotLike(String value) {
            addCriterion("FORE_ACAD_ORG not like", value, "foreAcadOrg");
            return (Criteria) this;
        }

        public Criteria andForeAcadOrgIn(List<String> values) {
            addCriterion("FORE_ACAD_ORG in", values, "foreAcadOrg");
            return (Criteria) this;
        }

        public Criteria andForeAcadOrgNotIn(List<String> values) {
            addCriterion("FORE_ACAD_ORG not in", values, "foreAcadOrg");
            return (Criteria) this;
        }

        public Criteria andForeAcadOrgBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_ORG between", value1, value2, "foreAcadOrg");
            return (Criteria) this;
        }

        public Criteria andForeAcadOrgNotBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_ORG not between", value1, value2, "foreAcadOrg");
            return (Criteria) this;
        }

        public Criteria andForeAcadNameIsNull() {
            addCriterion("FORE_ACAD_NAME is null");
            return (Criteria) this;
        }

        public Criteria andForeAcadNameIsNotNull() {
            addCriterion("FORE_ACAD_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andForeAcadNameEqualTo(String value) {
            addCriterion("FORE_ACAD_NAME =", value, "foreAcadName");
            return (Criteria) this;
        }

        public Criteria andForeAcadNameNotEqualTo(String value) {
            addCriterion("FORE_ACAD_NAME <>", value, "foreAcadName");
            return (Criteria) this;
        }

        public Criteria andForeAcadNameGreaterThan(String value) {
            addCriterion("FORE_ACAD_NAME >", value, "foreAcadName");
            return (Criteria) this;
        }

        public Criteria andForeAcadNameGreaterThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_NAME >=", value, "foreAcadName");
            return (Criteria) this;
        }

        public Criteria andForeAcadNameLessThan(String value) {
            addCriterion("FORE_ACAD_NAME <", value, "foreAcadName");
            return (Criteria) this;
        }

        public Criteria andForeAcadNameLessThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_NAME <=", value, "foreAcadName");
            return (Criteria) this;
        }

        public Criteria andForeAcadNameLike(String value) {
            addCriterion("FORE_ACAD_NAME like", value, "foreAcadName");
            return (Criteria) this;
        }

        public Criteria andForeAcadNameNotLike(String value) {
            addCriterion("FORE_ACAD_NAME not like", value, "foreAcadName");
            return (Criteria) this;
        }

        public Criteria andForeAcadNameIn(List<String> values) {
            addCriterion("FORE_ACAD_NAME in", values, "foreAcadName");
            return (Criteria) this;
        }

        public Criteria andForeAcadNameNotIn(List<String> values) {
            addCriterion("FORE_ACAD_NAME not in", values, "foreAcadName");
            return (Criteria) this;
        }

        public Criteria andForeAcadNameBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_NAME between", value1, value2, "foreAcadName");
            return (Criteria) this;
        }

        public Criteria andForeAcadNameNotBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_NAME not between", value1, value2, "foreAcadName");
            return (Criteria) this;
        }

        public Criteria andForeAcadKstOrgIdIsNull() {
            addCriterion("FORE_ACAD_KST_ORG_ID is null");
            return (Criteria) this;
        }

        public Criteria andForeAcadKstOrgIdIsNotNull() {
            addCriterion("FORE_ACAD_KST_ORG_ID is not null");
            return (Criteria) this;
        }

        public Criteria andForeAcadKstOrgIdEqualTo(String value) {
            addCriterion("FORE_ACAD_KST_ORG_ID =", value, "foreAcadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadKstOrgIdNotEqualTo(String value) {
            addCriterion("FORE_ACAD_KST_ORG_ID <>", value, "foreAcadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadKstOrgIdGreaterThan(String value) {
            addCriterion("FORE_ACAD_KST_ORG_ID >", value, "foreAcadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadKstOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_KST_ORG_ID >=", value, "foreAcadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadKstOrgIdLessThan(String value) {
            addCriterion("FORE_ACAD_KST_ORG_ID <", value, "foreAcadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadKstOrgIdLessThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_KST_ORG_ID <=", value, "foreAcadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadKstOrgIdLike(String value) {
            addCriterion("FORE_ACAD_KST_ORG_ID like", value, "foreAcadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadKstOrgIdNotLike(String value) {
            addCriterion("FORE_ACAD_KST_ORG_ID not like", value, "foreAcadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadKstOrgIdIn(List<String> values) {
            addCriterion("FORE_ACAD_KST_ORG_ID in", values, "foreAcadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadKstOrgIdNotIn(List<String> values) {
            addCriterion("FORE_ACAD_KST_ORG_ID not in", values, "foreAcadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadKstOrgIdBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_KST_ORG_ID between", value1, value2, "foreAcadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadKstOrgIdNotBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_KST_ORG_ID not between", value1, value2, "foreAcadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgIdIsNull() {
            addCriterion("FORE_ACAD_EBS_ORG_ID is null");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgIdIsNotNull() {
            addCriterion("FORE_ACAD_EBS_ORG_ID is not null");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgIdEqualTo(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_ID =", value, "foreAcadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgIdNotEqualTo(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_ID <>", value, "foreAcadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgIdGreaterThan(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_ID >", value, "foreAcadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_ID >=", value, "foreAcadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgIdLessThan(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_ID <", value, "foreAcadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgIdLessThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_ID <=", value, "foreAcadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgIdLike(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_ID like", value, "foreAcadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgIdNotLike(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_ID not like", value, "foreAcadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgIdIn(List<String> values) {
            addCriterion("FORE_ACAD_EBS_ORG_ID in", values, "foreAcadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgIdNotIn(List<String> values) {
            addCriterion("FORE_ACAD_EBS_ORG_ID not in", values, "foreAcadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgIdBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_EBS_ORG_ID between", value1, value2, "foreAcadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgIdNotBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_EBS_ORG_ID not between", value1, value2, "foreAcadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameEngIsNull() {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_ENG is null");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameEngIsNotNull() {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameEngEqualTo(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_ENG =", value, "foreAcadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameEngNotEqualTo(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_ENG <>", value, "foreAcadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameEngGreaterThan(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_ENG >", value, "foreAcadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameEngGreaterThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_ENG >=", value, "foreAcadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameEngLessThan(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_ENG <", value, "foreAcadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameEngLessThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_ENG <=", value, "foreAcadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameEngLike(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_ENG like", value, "foreAcadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameEngNotLike(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_ENG not like", value, "foreAcadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameEngIn(List<String> values) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_ENG in", values, "foreAcadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameEngNotIn(List<String> values) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_ENG not in", values, "foreAcadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameEngBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_ENG between", value1, value2, "foreAcadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameEngNotBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_ENG not between", value1, value2, "foreAcadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameKorIsNull() {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_KOR is null");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameKorIsNotNull() {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameKorEqualTo(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_KOR =", value, "foreAcadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameKorNotEqualTo(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_KOR <>", value, "foreAcadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameKorGreaterThan(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_KOR >", value, "foreAcadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameKorGreaterThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_KOR >=", value, "foreAcadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameKorLessThan(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_KOR <", value, "foreAcadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameKorLessThanOrEqualTo(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_KOR <=", value, "foreAcadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameKorLike(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_KOR like", value, "foreAcadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameKorNotLike(String value) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_KOR not like", value, "foreAcadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameKorIn(List<String> values) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_KOR in", values, "foreAcadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameKorNotIn(List<String> values) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_KOR not in", values, "foreAcadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameKorBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_KOR between", value1, value2, "foreAcadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameKorNotBetween(String value1, String value2) {
            addCriterion("FORE_ACAD_EBS_ORG_NAME_KOR not between", value1, value2, "foreAcadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andForeCampusIsNull() {
            addCriterion("FORE_CAMPUS is null");
            return (Criteria) this;
        }

        public Criteria andForeCampusIsNotNull() {
            addCriterion("FORE_CAMPUS is not null");
            return (Criteria) this;
        }

        public Criteria andForeCampusEqualTo(String value) {
            addCriterion("FORE_CAMPUS =", value, "foreCampus");
            return (Criteria) this;
        }

        public Criteria andForeCampusNotEqualTo(String value) {
            addCriterion("FORE_CAMPUS <>", value, "foreCampus");
            return (Criteria) this;
        }

        public Criteria andForeCampusGreaterThan(String value) {
            addCriterion("FORE_CAMPUS >", value, "foreCampus");
            return (Criteria) this;
        }

        public Criteria andForeCampusGreaterThanOrEqualTo(String value) {
            addCriterion("FORE_CAMPUS >=", value, "foreCampus");
            return (Criteria) this;
        }

        public Criteria andForeCampusLessThan(String value) {
            addCriterion("FORE_CAMPUS <", value, "foreCampus");
            return (Criteria) this;
        }

        public Criteria andForeCampusLessThanOrEqualTo(String value) {
            addCriterion("FORE_CAMPUS <=", value, "foreCampus");
            return (Criteria) this;
        }

        public Criteria andForeCampusLike(String value) {
            addCriterion("FORE_CAMPUS like", value, "foreCampus");
            return (Criteria) this;
        }

        public Criteria andForeCampusNotLike(String value) {
            addCriterion("FORE_CAMPUS not like", value, "foreCampus");
            return (Criteria) this;
        }

        public Criteria andForeCampusIn(List<String> values) {
            addCriterion("FORE_CAMPUS in", values, "foreCampus");
            return (Criteria) this;
        }

        public Criteria andForeCampusNotIn(List<String> values) {
            addCriterion("FORE_CAMPUS not in", values, "foreCampus");
            return (Criteria) this;
        }

        public Criteria andForeCampusBetween(String value1, String value2) {
            addCriterion("FORE_CAMPUS between", value1, value2, "foreCampus");
            return (Criteria) this;
        }

        public Criteria andForeCampusNotBetween(String value1, String value2) {
            addCriterion("FORE_CAMPUS not between", value1, value2, "foreCampus");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusCodeIsNull() {
            addCriterion("FORE_STU_STATUS_CODE is null");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusCodeIsNotNull() {
            addCriterion("FORE_STU_STATUS_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusCodeEqualTo(String value) {
            addCriterion("FORE_STU_STATUS_CODE =", value, "foreStuStatusCode");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusCodeNotEqualTo(String value) {
            addCriterion("FORE_STU_STATUS_CODE <>", value, "foreStuStatusCode");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusCodeGreaterThan(String value) {
            addCriterion("FORE_STU_STATUS_CODE >", value, "foreStuStatusCode");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusCodeGreaterThanOrEqualTo(String value) {
            addCriterion("FORE_STU_STATUS_CODE >=", value, "foreStuStatusCode");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusCodeLessThan(String value) {
            addCriterion("FORE_STU_STATUS_CODE <", value, "foreStuStatusCode");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusCodeLessThanOrEqualTo(String value) {
            addCriterion("FORE_STU_STATUS_CODE <=", value, "foreStuStatusCode");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusCodeLike(String value) {
            addCriterion("FORE_STU_STATUS_CODE like", value, "foreStuStatusCode");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusCodeNotLike(String value) {
            addCriterion("FORE_STU_STATUS_CODE not like", value, "foreStuStatusCode");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusCodeIn(List<String> values) {
            addCriterion("FORE_STU_STATUS_CODE in", values, "foreStuStatusCode");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusCodeNotIn(List<String> values) {
            addCriterion("FORE_STU_STATUS_CODE not in", values, "foreStuStatusCode");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusCodeBetween(String value1, String value2) {
            addCriterion("FORE_STU_STATUS_CODE between", value1, value2, "foreStuStatusCode");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusCodeNotBetween(String value1, String value2) {
            addCriterion("FORE_STU_STATUS_CODE not between", value1, value2, "foreStuStatusCode");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusEngIsNull() {
            addCriterion("FORE_STU_STATUS_ENG is null");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusEngIsNotNull() {
            addCriterion("FORE_STU_STATUS_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusEngEqualTo(String value) {
            addCriterion("FORE_STU_STATUS_ENG =", value, "foreStuStatusEng");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusEngNotEqualTo(String value) {
            addCriterion("FORE_STU_STATUS_ENG <>", value, "foreStuStatusEng");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusEngGreaterThan(String value) {
            addCriterion("FORE_STU_STATUS_ENG >", value, "foreStuStatusEng");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusEngGreaterThanOrEqualTo(String value) {
            addCriterion("FORE_STU_STATUS_ENG >=", value, "foreStuStatusEng");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusEngLessThan(String value) {
            addCriterion("FORE_STU_STATUS_ENG <", value, "foreStuStatusEng");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusEngLessThanOrEqualTo(String value) {
            addCriterion("FORE_STU_STATUS_ENG <=", value, "foreStuStatusEng");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusEngLike(String value) {
            addCriterion("FORE_STU_STATUS_ENG like", value, "foreStuStatusEng");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusEngNotLike(String value) {
            addCriterion("FORE_STU_STATUS_ENG not like", value, "foreStuStatusEng");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusEngIn(List<String> values) {
            addCriterion("FORE_STU_STATUS_ENG in", values, "foreStuStatusEng");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusEngNotIn(List<String> values) {
            addCriterion("FORE_STU_STATUS_ENG not in", values, "foreStuStatusEng");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusEngBetween(String value1, String value2) {
            addCriterion("FORE_STU_STATUS_ENG between", value1, value2, "foreStuStatusEng");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusEngNotBetween(String value1, String value2) {
            addCriterion("FORE_STU_STATUS_ENG not between", value1, value2, "foreStuStatusEng");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusKorIsNull() {
            addCriterion("FORE_STU_STATUS_KOR is null");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusKorIsNotNull() {
            addCriterion("FORE_STU_STATUS_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusKorEqualTo(String value) {
            addCriterion("FORE_STU_STATUS_KOR =", value, "foreStuStatusKor");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusKorNotEqualTo(String value) {
            addCriterion("FORE_STU_STATUS_KOR <>", value, "foreStuStatusKor");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusKorGreaterThan(String value) {
            addCriterion("FORE_STU_STATUS_KOR >", value, "foreStuStatusKor");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusKorGreaterThanOrEqualTo(String value) {
            addCriterion("FORE_STU_STATUS_KOR >=", value, "foreStuStatusKor");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusKorLessThan(String value) {
            addCriterion("FORE_STU_STATUS_KOR <", value, "foreStuStatusKor");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusKorLessThanOrEqualTo(String value) {
            addCriterion("FORE_STU_STATUS_KOR <=", value, "foreStuStatusKor");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusKorLike(String value) {
            addCriterion("FORE_STU_STATUS_KOR like", value, "foreStuStatusKor");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusKorNotLike(String value) {
            addCriterion("FORE_STU_STATUS_KOR not like", value, "foreStuStatusKor");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusKorIn(List<String> values) {
            addCriterion("FORE_STU_STATUS_KOR in", values, "foreStuStatusKor");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusKorNotIn(List<String> values) {
            addCriterion("FORE_STU_STATUS_KOR not in", values, "foreStuStatusKor");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusKorBetween(String value1, String value2) {
            addCriterion("FORE_STU_STATUS_KOR between", value1, value2, "foreStuStatusKor");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusKorNotBetween(String value1, String value2) {
            addCriterion("FORE_STU_STATUS_KOR not between", value1, value2, "foreStuStatusKor");
            return (Criteria) this;
        }

        public Criteria andForeProgStartDateIsNull() {
            addCriterion("FORE_PROG_START_DATE is null");
            return (Criteria) this;
        }

        public Criteria andForeProgStartDateIsNotNull() {
            addCriterion("FORE_PROG_START_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andForeProgStartDateEqualTo(String value) {
            addCriterion("FORE_PROG_START_DATE =", value, "foreProgStartDate");
            return (Criteria) this;
        }

        public Criteria andForeProgStartDateNotEqualTo(String value) {
            addCriterion("FORE_PROG_START_DATE <>", value, "foreProgStartDate");
            return (Criteria) this;
        }

        public Criteria andForeProgStartDateGreaterThan(String value) {
            addCriterion("FORE_PROG_START_DATE >", value, "foreProgStartDate");
            return (Criteria) this;
        }

        public Criteria andForeProgStartDateGreaterThanOrEqualTo(String value) {
            addCriterion("FORE_PROG_START_DATE >=", value, "foreProgStartDate");
            return (Criteria) this;
        }

        public Criteria andForeProgStartDateLessThan(String value) {
            addCriterion("FORE_PROG_START_DATE <", value, "foreProgStartDate");
            return (Criteria) this;
        }

        public Criteria andForeProgStartDateLessThanOrEqualTo(String value) {
            addCriterion("FORE_PROG_START_DATE <=", value, "foreProgStartDate");
            return (Criteria) this;
        }

        public Criteria andForeProgStartDateLike(String value) {
            addCriterion("FORE_PROG_START_DATE like", value, "foreProgStartDate");
            return (Criteria) this;
        }

        public Criteria andForeProgStartDateNotLike(String value) {
            addCriterion("FORE_PROG_START_DATE not like", value, "foreProgStartDate");
            return (Criteria) this;
        }

        public Criteria andForeProgStartDateIn(List<String> values) {
            addCriterion("FORE_PROG_START_DATE in", values, "foreProgStartDate");
            return (Criteria) this;
        }

        public Criteria andForeProgStartDateNotIn(List<String> values) {
            addCriterion("FORE_PROG_START_DATE not in", values, "foreProgStartDate");
            return (Criteria) this;
        }

        public Criteria andForeProgStartDateBetween(String value1, String value2) {
            addCriterion("FORE_PROG_START_DATE between", value1, value2, "foreProgStartDate");
            return (Criteria) this;
        }

        public Criteria andForeProgStartDateNotBetween(String value1, String value2) {
            addCriterion("FORE_PROG_START_DATE not between", value1, value2, "foreProgStartDate");
            return (Criteria) this;
        }

        public Criteria andForeProgEndDateIsNull() {
            addCriterion("FORE_PROG_END_DATE is null");
            return (Criteria) this;
        }

        public Criteria andForeProgEndDateIsNotNull() {
            addCriterion("FORE_PROG_END_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andForeProgEndDateEqualTo(String value) {
            addCriterion("FORE_PROG_END_DATE =", value, "foreProgEndDate");
            return (Criteria) this;
        }

        public Criteria andForeProgEndDateNotEqualTo(String value) {
            addCriterion("FORE_PROG_END_DATE <>", value, "foreProgEndDate");
            return (Criteria) this;
        }

        public Criteria andForeProgEndDateGreaterThan(String value) {
            addCriterion("FORE_PROG_END_DATE >", value, "foreProgEndDate");
            return (Criteria) this;
        }

        public Criteria andForeProgEndDateGreaterThanOrEqualTo(String value) {
            addCriterion("FORE_PROG_END_DATE >=", value, "foreProgEndDate");
            return (Criteria) this;
        }

        public Criteria andForeProgEndDateLessThan(String value) {
            addCriterion("FORE_PROG_END_DATE <", value, "foreProgEndDate");
            return (Criteria) this;
        }

        public Criteria andForeProgEndDateLessThanOrEqualTo(String value) {
            addCriterion("FORE_PROG_END_DATE <=", value, "foreProgEndDate");
            return (Criteria) this;
        }

        public Criteria andForeProgEndDateLike(String value) {
            addCriterion("FORE_PROG_END_DATE like", value, "foreProgEndDate");
            return (Criteria) this;
        }

        public Criteria andForeProgEndDateNotLike(String value) {
            addCriterion("FORE_PROG_END_DATE not like", value, "foreProgEndDate");
            return (Criteria) this;
        }

        public Criteria andForeProgEndDateIn(List<String> values) {
            addCriterion("FORE_PROG_END_DATE in", values, "foreProgEndDate");
            return (Criteria) this;
        }

        public Criteria andForeProgEndDateNotIn(List<String> values) {
            addCriterion("FORE_PROG_END_DATE not in", values, "foreProgEndDate");
            return (Criteria) this;
        }

        public Criteria andForeProgEndDateBetween(String value1, String value2) {
            addCriterion("FORE_PROG_END_DATE between", value1, value2, "foreProgEndDate");
            return (Criteria) this;
        }

        public Criteria andForeProgEndDateNotBetween(String value1, String value2) {
            addCriterion("FORE_PROG_END_DATE not between", value1, value2, "foreProgEndDate");
            return (Criteria) this;
        }

        public Criteria andLastupddttmIsNull() {
            addCriterion("LASTUPDDTTM is null");
            return (Criteria) this;
        }

        public Criteria andLastupddttmIsNotNull() {
            addCriterion("LASTUPDDTTM is not null");
            return (Criteria) this;
        }

        public Criteria andLastupddttmEqualTo(Date value) {
            addCriterion("LASTUPDDTTM =", value, "lastupddttm");
            return (Criteria) this;
        }

        public Criteria andLastupddttmNotEqualTo(Date value) {
            addCriterion("LASTUPDDTTM <>", value, "lastupddttm");
            return (Criteria) this;
        }

        public Criteria andLastupddttmGreaterThan(Date value) {
            addCriterion("LASTUPDDTTM >", value, "lastupddttm");
            return (Criteria) this;
        }

        public Criteria andLastupddttmGreaterThanOrEqualTo(Date value) {
            addCriterion("LASTUPDDTTM >=", value, "lastupddttm");
            return (Criteria) this;
        }

        public Criteria andLastupddttmLessThan(Date value) {
            addCriterion("LASTUPDDTTM <", value, "lastupddttm");
            return (Criteria) this;
        }

        public Criteria andLastupddttmLessThanOrEqualTo(Date value) {
            addCriterion("LASTUPDDTTM <=", value, "lastupddttm");
            return (Criteria) this;
        }

        public Criteria andLastupddttmIn(List<Date> values) {
            addCriterion("LASTUPDDTTM in", values, "lastupddttm");
            return (Criteria) this;
        }

        public Criteria andLastupddttmNotIn(List<Date> values) {
            addCriterion("LASTUPDDTTM not in", values, "lastupddttm");
            return (Criteria) this;
        }

        public Criteria andLastupddttmBetween(Date value1, Date value2) {
            addCriterion("LASTUPDDTTM between", value1, value2, "lastupddttm");
            return (Criteria) this;
        }

        public Criteria andLastupddttmNotBetween(Date value1, Date value2) {
            addCriterion("LASTUPDDTTM not between", value1, value2, "lastupddttm");
            return (Criteria) this;
        }

        public Criteria andSlastupddttmIsNull() {
            addCriterion("SLASTUPDDTTM is null");
            return (Criteria) this;
        }

        public Criteria andSlastupddttmIsNotNull() {
            addCriterion("SLASTUPDDTTM is not null");
            return (Criteria) this;
        }

        public Criteria andSlastupddttmEqualTo(String value) {
            addCriterion("SLASTUPDDTTM =", value, "slastupddttm");
            return (Criteria) this;
        }

        public Criteria andSlastupddttmNotEqualTo(String value) {
            addCriterion("SLASTUPDDTTM <>", value, "slastupddttm");
            return (Criteria) this;
        }

        public Criteria andSlastupddttmGreaterThan(String value) {
            addCriterion("SLASTUPDDTTM >", value, "slastupddttm");
            return (Criteria) this;
        }

        public Criteria andSlastupddttmGreaterThanOrEqualTo(String value) {
            addCriterion("SLASTUPDDTTM >=", value, "slastupddttm");
            return (Criteria) this;
        }

        public Criteria andSlastupddttmLessThan(String value) {
            addCriterion("SLASTUPDDTTM <", value, "slastupddttm");
            return (Criteria) this;
        }

        public Criteria andSlastupddttmLessThanOrEqualTo(String value) {
            addCriterion("SLASTUPDDTTM <=", value, "slastupddttm");
            return (Criteria) this;
        }

        public Criteria andSlastupddttmLike(String value) {
            addCriterion("SLASTUPDDTTM like", value, "slastupddttm");
            return (Criteria) this;
        }

        public Criteria andSlastupddttmNotLike(String value) {
            addCriterion("SLASTUPDDTTM not like", value, "slastupddttm");
            return (Criteria) this;
        }

        public Criteria andSlastupddttmIn(List<String> values) {
            addCriterion("SLASTUPDDTTM in", values, "slastupddttm");
            return (Criteria) this;
        }

        public Criteria andSlastupddttmNotIn(List<String> values) {
            addCriterion("SLASTUPDDTTM not in", values, "slastupddttm");
            return (Criteria) this;
        }

        public Criteria andSlastupddttmBetween(String value1, String value2) {
            addCriterion("SLASTUPDDTTM between", value1, value2, "slastupddttm");
            return (Criteria) this;
        }

        public Criteria andSlastupddttmNotBetween(String value1, String value2) {
            addCriterion("SLASTUPDDTTM not between", value1, value2, "slastupddttm");
            return (Criteria) this;
        }

        public Criteria andCombindedNameIsNull() {
            addCriterion("COMBINDED_NAME is null");
            return (Criteria) this;
        }

        public Criteria andCombindedNameIsNotNull() {
            addCriterion("COMBINDED_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andCombindedNameEqualTo(String value) {
            addCriterion("COMBINDED_NAME =", value, "combindedName");
            return (Criteria) this;
        }

        public Criteria andCombindedNameNotEqualTo(String value) {
            addCriterion("COMBINDED_NAME <>", value, "combindedName");
            return (Criteria) this;
        }

        public Criteria andCombindedNameGreaterThan(String value) {
            addCriterion("COMBINDED_NAME >", value, "combindedName");
            return (Criteria) this;
        }

        public Criteria andCombindedNameGreaterThanOrEqualTo(String value) {
            addCriterion("COMBINDED_NAME >=", value, "combindedName");
            return (Criteria) this;
        }

        public Criteria andCombindedNameLessThan(String value) {
            addCriterion("COMBINDED_NAME <", value, "combindedName");
            return (Criteria) this;
        }

        public Criteria andCombindedNameLessThanOrEqualTo(String value) {
            addCriterion("COMBINDED_NAME <=", value, "combindedName");
            return (Criteria) this;
        }

        public Criteria andCombindedNameLike(String value) {
            addCriterion("COMBINDED_NAME like", value, "combindedName");
            return (Criteria) this;
        }

        public Criteria andCombindedNameNotLike(String value) {
            addCriterion("COMBINDED_NAME not like", value, "combindedName");
            return (Criteria) this;
        }

        public Criteria andCombindedNameIn(List<String> values) {
            addCriterion("COMBINDED_NAME in", values, "combindedName");
            return (Criteria) this;
        }

        public Criteria andCombindedNameNotIn(List<String> values) {
            addCriterion("COMBINDED_NAME not in", values, "combindedName");
            return (Criteria) this;
        }

        public Criteria andCombindedNameBetween(String value1, String value2) {
            addCriterion("COMBINDED_NAME between", value1, value2, "combindedName");
            return (Criteria) this;
        }

        public Criteria andCombindedNameNotBetween(String value1, String value2) {
            addCriterion("COMBINDED_NAME not between", value1, value2, "combindedName");
            return (Criteria) this;
        }

        public Criteria andCombindedStatusIsNull() {
            addCriterion("COMBINDED_STATUS is null");
            return (Criteria) this;
        }

        public Criteria andCombindedStatusIsNotNull() {
            addCriterion("COMBINDED_STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andCombindedStatusEqualTo(String value) {
            addCriterion("COMBINDED_STATUS =", value, "combindedStatus");
            return (Criteria) this;
        }

        public Criteria andCombindedStatusNotEqualTo(String value) {
            addCriterion("COMBINDED_STATUS <>", value, "combindedStatus");
            return (Criteria) this;
        }

        public Criteria andCombindedStatusGreaterThan(String value) {
            addCriterion("COMBINDED_STATUS >", value, "combindedStatus");
            return (Criteria) this;
        }

        public Criteria andCombindedStatusGreaterThanOrEqualTo(String value) {
            addCriterion("COMBINDED_STATUS >=", value, "combindedStatus");
            return (Criteria) this;
        }

        public Criteria andCombindedStatusLessThan(String value) {
            addCriterion("COMBINDED_STATUS <", value, "combindedStatus");
            return (Criteria) this;
        }

        public Criteria andCombindedStatusLessThanOrEqualTo(String value) {
            addCriterion("COMBINDED_STATUS <=", value, "combindedStatus");
            return (Criteria) this;
        }

        public Criteria andCombindedStatusLike(String value) {
            addCriterion("COMBINDED_STATUS like", value, "combindedStatus");
            return (Criteria) this;
        }

        public Criteria andCombindedStatusNotLike(String value) {
            addCriterion("COMBINDED_STATUS not like", value, "combindedStatus");
            return (Criteria) this;
        }

        public Criteria andCombindedStatusIn(List<String> values) {
            addCriterion("COMBINDED_STATUS in", values, "combindedStatus");
            return (Criteria) this;
        }

        public Criteria andCombindedStatusNotIn(List<String> values) {
            addCriterion("COMBINDED_STATUS not in", values, "combindedStatus");
            return (Criteria) this;
        }

        public Criteria andCombindedStatusBetween(String value1, String value2) {
            addCriterion("COMBINDED_STATUS between", value1, value2, "combindedStatus");
            return (Criteria) this;
        }

        public Criteria andCombindedStatusNotBetween(String value1, String value2) {
            addCriterion("COMBINDED_STATUS not between", value1, value2, "combindedStatus");
            return (Criteria) this;
        }

        public Criteria andGrantedServicesIsNull() {
            addCriterion("GRANTED_SERVICES is null");
            return (Criteria) this;
        }

        public Criteria andGrantedServicesIsNotNull() {
            addCriterion("GRANTED_SERVICES is not null");
            return (Criteria) this;
        }

        public Criteria andGrantedServicesEqualTo(String value) {
            addCriterion("GRANTED_SERVICES =", value, "grantedServices");
            return (Criteria) this;
        }

        public Criteria andGrantedServicesNotEqualTo(String value) {
            addCriterion("GRANTED_SERVICES <>", value, "grantedServices");
            return (Criteria) this;
        }

        public Criteria andGrantedServicesGreaterThan(String value) {
            addCriterion("GRANTED_SERVICES >", value, "grantedServices");
            return (Criteria) this;
        }

        public Criteria andGrantedServicesGreaterThanOrEqualTo(String value) {
            addCriterion("GRANTED_SERVICES >=", value, "grantedServices");
            return (Criteria) this;
        }

        public Criteria andGrantedServicesLessThan(String value) {
            addCriterion("GRANTED_SERVICES <", value, "grantedServices");
            return (Criteria) this;
        }

        public Criteria andGrantedServicesLessThanOrEqualTo(String value) {
            addCriterion("GRANTED_SERVICES <=", value, "grantedServices");
            return (Criteria) this;
        }

        public Criteria andGrantedServicesLike(String value) {
            addCriterion("GRANTED_SERVICES like", value, "grantedServices");
            return (Criteria) this;
        }

        public Criteria andGrantedServicesNotLike(String value) {
            addCriterion("GRANTED_SERVICES not like", value, "grantedServices");
            return (Criteria) this;
        }

        public Criteria andGrantedServicesIn(List<String> values) {
            addCriterion("GRANTED_SERVICES in", values, "grantedServices");
            return (Criteria) this;
        }

        public Criteria andGrantedServicesNotIn(List<String> values) {
            addCriterion("GRANTED_SERVICES not in", values, "grantedServices");
            return (Criteria) this;
        }

        public Criteria andGrantedServicesBetween(String value1, String value2) {
            addCriterion("GRANTED_SERVICES between", value1, value2, "grantedServices");
            return (Criteria) this;
        }

        public Criteria andGrantedServicesNotBetween(String value1, String value2) {
            addCriterion("GRANTED_SERVICES not between", value1, value2, "grantedServices");
            return (Criteria) this;
        }

        public Criteria andIPinIsNull() {
            addCriterion("I_PIN is null");
            return (Criteria) this;
        }

        public Criteria andIPinIsNotNull() {
            addCriterion("I_PIN is not null");
            return (Criteria) this;
        }

        public Criteria andIPinEqualTo(String value) {
            addCriterion("I_PIN =", value, "iPin");
            return (Criteria) this;
        }

        public Criteria andIPinNotEqualTo(String value) {
            addCriterion("I_PIN <>", value, "iPin");
            return (Criteria) this;
        }

        public Criteria andIPinGreaterThan(String value) {
            addCriterion("I_PIN >", value, "iPin");
            return (Criteria) this;
        }

        public Criteria andIPinGreaterThanOrEqualTo(String value) {
            addCriterion("I_PIN >=", value, "iPin");
            return (Criteria) this;
        }

        public Criteria andIPinLessThan(String value) {
            addCriterion("I_PIN <", value, "iPin");
            return (Criteria) this;
        }

        public Criteria andIPinLessThanOrEqualTo(String value) {
            addCriterion("I_PIN <=", value, "iPin");
            return (Criteria) this;
        }

        public Criteria andIPinLike(String value) {
            addCriterion("I_PIN like", value, "iPin");
            return (Criteria) this;
        }

        public Criteria andIPinNotLike(String value) {
            addCriterion("I_PIN not like", value, "iPin");
            return (Criteria) this;
        }

        public Criteria andIPinIn(List<String> values) {
            addCriterion("I_PIN in", values, "iPin");
            return (Criteria) this;
        }

        public Criteria andIPinNotIn(List<String> values) {
            addCriterion("I_PIN not in", values, "iPin");
            return (Criteria) this;
        }

        public Criteria andIPinBetween(String value1, String value2) {
            addCriterion("I_PIN between", value1, value2, "iPin");
            return (Criteria) this;
        }

        public Criteria andIPinNotBetween(String value1, String value2) {
            addCriterion("I_PIN not between", value1, value2, "iPin");
            return (Criteria) this;
        }

        public Criteria andHighschoolCodeIsNull() {
            addCriterion("HIGHSCHOOL_CODE is null");
            return (Criteria) this;
        }

        public Criteria andHighschoolCodeIsNotNull() {
            addCriterion("HIGHSCHOOL_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andHighschoolCodeEqualTo(String value) {
            addCriterion("HIGHSCHOOL_CODE =", value, "highschoolCode");
            return (Criteria) this;
        }

        public Criteria andHighschoolCodeNotEqualTo(String value) {
            addCriterion("HIGHSCHOOL_CODE <>", value, "highschoolCode");
            return (Criteria) this;
        }

        public Criteria andHighschoolCodeGreaterThan(String value) {
            addCriterion("HIGHSCHOOL_CODE >", value, "highschoolCode");
            return (Criteria) this;
        }

        public Criteria andHighschoolCodeGreaterThanOrEqualTo(String value) {
            addCriterion("HIGHSCHOOL_CODE >=", value, "highschoolCode");
            return (Criteria) this;
        }

        public Criteria andHighschoolCodeLessThan(String value) {
            addCriterion("HIGHSCHOOL_CODE <", value, "highschoolCode");
            return (Criteria) this;
        }

        public Criteria andHighschoolCodeLessThanOrEqualTo(String value) {
            addCriterion("HIGHSCHOOL_CODE <=", value, "highschoolCode");
            return (Criteria) this;
        }

        public Criteria andHighschoolCodeLike(String value) {
            addCriterion("HIGHSCHOOL_CODE like", value, "highschoolCode");
            return (Criteria) this;
        }

        public Criteria andHighschoolCodeNotLike(String value) {
            addCriterion("HIGHSCHOOL_CODE not like", value, "highschoolCode");
            return (Criteria) this;
        }

        public Criteria andHighschoolCodeIn(List<String> values) {
            addCriterion("HIGHSCHOOL_CODE in", values, "highschoolCode");
            return (Criteria) this;
        }

        public Criteria andHighschoolCodeNotIn(List<String> values) {
            addCriterion("HIGHSCHOOL_CODE not in", values, "highschoolCode");
            return (Criteria) this;
        }

        public Criteria andHighschoolCodeBetween(String value1, String value2) {
            addCriterion("HIGHSCHOOL_CODE between", value1, value2, "highschoolCode");
            return (Criteria) this;
        }

        public Criteria andHighschoolCodeNotBetween(String value1, String value2) {
            addCriterion("HIGHSCHOOL_CODE not between", value1, value2, "highschoolCode");
            return (Criteria) this;
        }

        public Criteria andHighschoolNameIsNull() {
            addCriterion("HIGHSCHOOL_NAME is null");
            return (Criteria) this;
        }

        public Criteria andHighschoolNameIsNotNull() {
            addCriterion("HIGHSCHOOL_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andHighschoolNameEqualTo(String value) {
            addCriterion("HIGHSCHOOL_NAME =", value, "highschoolName");
            return (Criteria) this;
        }

        public Criteria andHighschoolNameNotEqualTo(String value) {
            addCriterion("HIGHSCHOOL_NAME <>", value, "highschoolName");
            return (Criteria) this;
        }

        public Criteria andHighschoolNameGreaterThan(String value) {
            addCriterion("HIGHSCHOOL_NAME >", value, "highschoolName");
            return (Criteria) this;
        }

        public Criteria andHighschoolNameGreaterThanOrEqualTo(String value) {
            addCriterion("HIGHSCHOOL_NAME >=", value, "highschoolName");
            return (Criteria) this;
        }

        public Criteria andHighschoolNameLessThan(String value) {
            addCriterion("HIGHSCHOOL_NAME <", value, "highschoolName");
            return (Criteria) this;
        }

        public Criteria andHighschoolNameLessThanOrEqualTo(String value) {
            addCriterion("HIGHSCHOOL_NAME <=", value, "highschoolName");
            return (Criteria) this;
        }

        public Criteria andHighschoolNameLike(String value) {
            addCriterion("HIGHSCHOOL_NAME like", value, "highschoolName");
            return (Criteria) this;
        }

        public Criteria andHighschoolNameNotLike(String value) {
            addCriterion("HIGHSCHOOL_NAME not like", value, "highschoolName");
            return (Criteria) this;
        }

        public Criteria andHighschoolNameIn(List<String> values) {
            addCriterion("HIGHSCHOOL_NAME in", values, "highschoolName");
            return (Criteria) this;
        }

        public Criteria andHighschoolNameNotIn(List<String> values) {
            addCriterion("HIGHSCHOOL_NAME not in", values, "highschoolName");
            return (Criteria) this;
        }

        public Criteria andHighschoolNameBetween(String value1, String value2) {
            addCriterion("HIGHSCHOOL_NAME between", value1, value2, "highschoolName");
            return (Criteria) this;
        }

        public Criteria andHighschoolNameNotBetween(String value1, String value2) {
            addCriterion("HIGHSCHOOL_NAME not between", value1, value2, "highschoolName");
            return (Criteria) this;
        }

        public Criteria andKaistUidIsNull() {
            addCriterion("KAIST_UID is null");
            return (Criteria) this;
        }

        public Criteria andKaistUidIsNotNull() {
            addCriterion("KAIST_UID is not null");
            return (Criteria) this;
        }

        public Criteria andKaistUidEqualTo(String value) {
            addCriterion("KAIST_UID =", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotEqualTo(String value) {
            addCriterion("KAIST_UID <>", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidGreaterThan(String value) {
            addCriterion("KAIST_UID >", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidGreaterThanOrEqualTo(String value) {
            addCriterion("KAIST_UID >=", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidLessThan(String value) {
            addCriterion("KAIST_UID <", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidLessThanOrEqualTo(String value) {
            addCriterion("KAIST_UID <=", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidLike(String value) {
            addCriterion("KAIST_UID like", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotLike(String value) {
            addCriterion("KAIST_UID not like", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidIn(List<String> values) {
            addCriterion("KAIST_UID in", values, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotIn(List<String> values) {
            addCriterion("KAIST_UID not in", values, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidBetween(String value1, String value2) {
            addCriterion("KAIST_UID between", value1, value2, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotBetween(String value1, String value2) {
            addCriterion("KAIST_UID not between", value1, value2, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidIsNull() {
            addCriterion("KAIST_SUID is null");
            return (Criteria) this;
        }

        public Criteria andKaistSuidIsNotNull() {
            addCriterion("KAIST_SUID is not null");
            return (Criteria) this;
        }

        public Criteria andKaistSuidEqualTo(String value) {
            addCriterion("KAIST_SUID =", value, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidNotEqualTo(String value) {
            addCriterion("KAIST_SUID <>", value, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidGreaterThan(String value) {
            addCriterion("KAIST_SUID >", value, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidGreaterThanOrEqualTo(String value) {
            addCriterion("KAIST_SUID >=", value, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidLessThan(String value) {
            addCriterion("KAIST_SUID <", value, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidLessThanOrEqualTo(String value) {
            addCriterion("KAIST_SUID <=", value, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidLike(String value) {
            addCriterion("KAIST_SUID like", value, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidNotLike(String value) {
            addCriterion("KAIST_SUID not like", value, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidIn(List<String> values) {
            addCriterion("KAIST_SUID in", values, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidNotIn(List<String> values) {
            addCriterion("KAIST_SUID not in", values, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidBetween(String value1, String value2) {
            addCriterion("KAIST_SUID between", value1, value2, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidNotBetween(String value1, String value2) {
            addCriterion("KAIST_SUID not between", value1, value2, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andClCampusIsNull() {
            addCriterion("CL_CAMPUS is null");
            return (Criteria) this;
        }

        public Criteria andClCampusIsNotNull() {
            addCriterion("CL_CAMPUS is not null");
            return (Criteria) this;
        }

        public Criteria andClCampusEqualTo(String value) {
            addCriterion("CL_CAMPUS =", value, "clCampus");
            return (Criteria) this;
        }

        public Criteria andClCampusNotEqualTo(String value) {
            addCriterion("CL_CAMPUS <>", value, "clCampus");
            return (Criteria) this;
        }

        public Criteria andClCampusGreaterThan(String value) {
            addCriterion("CL_CAMPUS >", value, "clCampus");
            return (Criteria) this;
        }

        public Criteria andClCampusGreaterThanOrEqualTo(String value) {
            addCriterion("CL_CAMPUS >=", value, "clCampus");
            return (Criteria) this;
        }

        public Criteria andClCampusLessThan(String value) {
            addCriterion("CL_CAMPUS <", value, "clCampus");
            return (Criteria) this;
        }

        public Criteria andClCampusLessThanOrEqualTo(String value) {
            addCriterion("CL_CAMPUS <=", value, "clCampus");
            return (Criteria) this;
        }

        public Criteria andClCampusLike(String value) {
            addCriterion("CL_CAMPUS like", value, "clCampus");
            return (Criteria) this;
        }

        public Criteria andClCampusNotLike(String value) {
            addCriterion("CL_CAMPUS not like", value, "clCampus");
            return (Criteria) this;
        }

        public Criteria andClCampusIn(List<String> values) {
            addCriterion("CL_CAMPUS in", values, "clCampus");
            return (Criteria) this;
        }

        public Criteria andClCampusNotIn(List<String> values) {
            addCriterion("CL_CAMPUS not in", values, "clCampus");
            return (Criteria) this;
        }

        public Criteria andClCampusBetween(String value1, String value2) {
            addCriterion("CL_CAMPUS between", value1, value2, "clCampus");
            return (Criteria) this;
        }

        public Criteria andClCampusNotBetween(String value1, String value2) {
            addCriterion("CL_CAMPUS not between", value1, value2, "clCampus");
            return (Criteria) this;
        }

        public Criteria andClBuildingIsNull() {
            addCriterion("CL_BUILDING is null");
            return (Criteria) this;
        }

        public Criteria andClBuildingIsNotNull() {
            addCriterion("CL_BUILDING is not null");
            return (Criteria) this;
        }

        public Criteria andClBuildingEqualTo(String value) {
            addCriterion("CL_BUILDING =", value, "clBuilding");
            return (Criteria) this;
        }

        public Criteria andClBuildingNotEqualTo(String value) {
            addCriterion("CL_BUILDING <>", value, "clBuilding");
            return (Criteria) this;
        }

        public Criteria andClBuildingGreaterThan(String value) {
            addCriterion("CL_BUILDING >", value, "clBuilding");
            return (Criteria) this;
        }

        public Criteria andClBuildingGreaterThanOrEqualTo(String value) {
            addCriterion("CL_BUILDING >=", value, "clBuilding");
            return (Criteria) this;
        }

        public Criteria andClBuildingLessThan(String value) {
            addCriterion("CL_BUILDING <", value, "clBuilding");
            return (Criteria) this;
        }

        public Criteria andClBuildingLessThanOrEqualTo(String value) {
            addCriterion("CL_BUILDING <=", value, "clBuilding");
            return (Criteria) this;
        }

        public Criteria andClBuildingLike(String value) {
            addCriterion("CL_BUILDING like", value, "clBuilding");
            return (Criteria) this;
        }

        public Criteria andClBuildingNotLike(String value) {
            addCriterion("CL_BUILDING not like", value, "clBuilding");
            return (Criteria) this;
        }

        public Criteria andClBuildingIn(List<String> values) {
            addCriterion("CL_BUILDING in", values, "clBuilding");
            return (Criteria) this;
        }

        public Criteria andClBuildingNotIn(List<String> values) {
            addCriterion("CL_BUILDING not in", values, "clBuilding");
            return (Criteria) this;
        }

        public Criteria andClBuildingBetween(String value1, String value2) {
            addCriterion("CL_BUILDING between", value1, value2, "clBuilding");
            return (Criteria) this;
        }

        public Criteria andClBuildingNotBetween(String value1, String value2) {
            addCriterion("CL_BUILDING not between", value1, value2, "clBuilding");
            return (Criteria) this;
        }

        public Criteria andClFloorIsNull() {
            addCriterion("CL_FLOOR is null");
            return (Criteria) this;
        }

        public Criteria andClFloorIsNotNull() {
            addCriterion("CL_FLOOR is not null");
            return (Criteria) this;
        }

        public Criteria andClFloorEqualTo(String value) {
            addCriterion("CL_FLOOR =", value, "clFloor");
            return (Criteria) this;
        }

        public Criteria andClFloorNotEqualTo(String value) {
            addCriterion("CL_FLOOR <>", value, "clFloor");
            return (Criteria) this;
        }

        public Criteria andClFloorGreaterThan(String value) {
            addCriterion("CL_FLOOR >", value, "clFloor");
            return (Criteria) this;
        }

        public Criteria andClFloorGreaterThanOrEqualTo(String value) {
            addCriterion("CL_FLOOR >=", value, "clFloor");
            return (Criteria) this;
        }

        public Criteria andClFloorLessThan(String value) {
            addCriterion("CL_FLOOR <", value, "clFloor");
            return (Criteria) this;
        }

        public Criteria andClFloorLessThanOrEqualTo(String value) {
            addCriterion("CL_FLOOR <=", value, "clFloor");
            return (Criteria) this;
        }

        public Criteria andClFloorLike(String value) {
            addCriterion("CL_FLOOR like", value, "clFloor");
            return (Criteria) this;
        }

        public Criteria andClFloorNotLike(String value) {
            addCriterion("CL_FLOOR not like", value, "clFloor");
            return (Criteria) this;
        }

        public Criteria andClFloorIn(List<String> values) {
            addCriterion("CL_FLOOR in", values, "clFloor");
            return (Criteria) this;
        }

        public Criteria andClFloorNotIn(List<String> values) {
            addCriterion("CL_FLOOR not in", values, "clFloor");
            return (Criteria) this;
        }

        public Criteria andClFloorBetween(String value1, String value2) {
            addCriterion("CL_FLOOR between", value1, value2, "clFloor");
            return (Criteria) this;
        }

        public Criteria andClFloorNotBetween(String value1, String value2) {
            addCriterion("CL_FLOOR not between", value1, value2, "clFloor");
            return (Criteria) this;
        }

        public Criteria andClRoomNoIsNull() {
            addCriterion("CL_ROOM_NO is null");
            return (Criteria) this;
        }

        public Criteria andClRoomNoIsNotNull() {
            addCriterion("CL_ROOM_NO is not null");
            return (Criteria) this;
        }

        public Criteria andClRoomNoEqualTo(String value) {
            addCriterion("CL_ROOM_NO =", value, "clRoomNo");
            return (Criteria) this;
        }

        public Criteria andClRoomNoNotEqualTo(String value) {
            addCriterion("CL_ROOM_NO <>", value, "clRoomNo");
            return (Criteria) this;
        }

        public Criteria andClRoomNoGreaterThan(String value) {
            addCriterion("CL_ROOM_NO >", value, "clRoomNo");
            return (Criteria) this;
        }

        public Criteria andClRoomNoGreaterThanOrEqualTo(String value) {
            addCriterion("CL_ROOM_NO >=", value, "clRoomNo");
            return (Criteria) this;
        }

        public Criteria andClRoomNoLessThan(String value) {
            addCriterion("CL_ROOM_NO <", value, "clRoomNo");
            return (Criteria) this;
        }

        public Criteria andClRoomNoLessThanOrEqualTo(String value) {
            addCriterion("CL_ROOM_NO <=", value, "clRoomNo");
            return (Criteria) this;
        }

        public Criteria andClRoomNoLike(String value) {
            addCriterion("CL_ROOM_NO like", value, "clRoomNo");
            return (Criteria) this;
        }

        public Criteria andClRoomNoNotLike(String value) {
            addCriterion("CL_ROOM_NO not like", value, "clRoomNo");
            return (Criteria) this;
        }

        public Criteria andClRoomNoIn(List<String> values) {
            addCriterion("CL_ROOM_NO in", values, "clRoomNo");
            return (Criteria) this;
        }

        public Criteria andClRoomNoNotIn(List<String> values) {
            addCriterion("CL_ROOM_NO not in", values, "clRoomNo");
            return (Criteria) this;
        }

        public Criteria andClRoomNoBetween(String value1, String value2) {
            addCriterion("CL_ROOM_NO between", value1, value2, "clRoomNo");
            return (Criteria) this;
        }

        public Criteria andClRoomNoNotBetween(String value1, String value2) {
            addCriterion("CL_ROOM_NO not between", value1, value2, "clRoomNo");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidIsNull() {
            addCriterion("ADVR_KAIST_UID is null");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidIsNotNull() {
            addCriterion("ADVR_KAIST_UID is not null");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidEqualTo(String value) {
            addCriterion("ADVR_KAIST_UID =", value, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidNotEqualTo(String value) {
            addCriterion("ADVR_KAIST_UID <>", value, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidGreaterThan(String value) {
            addCriterion("ADVR_KAIST_UID >", value, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidGreaterThanOrEqualTo(String value) {
            addCriterion("ADVR_KAIST_UID >=", value, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidLessThan(String value) {
            addCriterion("ADVR_KAIST_UID <", value, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidLessThanOrEqualTo(String value) {
            addCriterion("ADVR_KAIST_UID <=", value, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidLike(String value) {
            addCriterion("ADVR_KAIST_UID like", value, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidNotLike(String value) {
            addCriterion("ADVR_KAIST_UID not like", value, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidIn(List<String> values) {
            addCriterion("ADVR_KAIST_UID in", values, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidNotIn(List<String> values) {
            addCriterion("ADVR_KAIST_UID not in", values, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidBetween(String value1, String value2) {
            addCriterion("ADVR_KAIST_UID between", value1, value2, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidNotBetween(String value1, String value2) {
            addCriterion("ADVR_KAIST_UID not between", value1, value2, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andEmplidLikeInsensitive(String value) {
            addCriterion("upper(EMPLID) like", value.toUpperCase(), "emplid");
            return (Criteria) this;
        }

        public Criteria andSsoIdLikeInsensitive(String value) {
            addCriterion("upper(SSO_ID) like", value.toUpperCase(), "ssoId");
            return (Criteria) this;
        }

        public Criteria andSsoPwLikeInsensitive(String value) {
            addCriterion("upper(SSO_PW) like", value.toUpperCase(), "ssoPw");
            return (Criteria) this;
        }

        public Criteria andNameLikeInsensitive(String value) {
            addCriterion("upper(NAME) like", value.toUpperCase(), "name");
            return (Criteria) this;
        }

        public Criteria andNameAcLikeInsensitive(String value) {
            addCriterion("upper(NAME_AC) like", value.toUpperCase(), "nameAc");
            return (Criteria) this;
        }

        public Criteria andLastNameLikeInsensitive(String value) {
            addCriterion("upper(LAST_NAME) like", value.toUpperCase(), "lastName");
            return (Criteria) this;
        }

        public Criteria andFirstNameLikeInsensitive(String value) {
            addCriterion("upper(FIRST_NAME) like", value.toUpperCase(), "firstName");
            return (Criteria) this;
        }

        public Criteria andBirthdateLikeInsensitive(String value) {
            addCriterion("upper(BIRTHDATE) like", value.toUpperCase(), "birthdate");
            return (Criteria) this;
        }

        public Criteria andCountryLikeInsensitive(String value) {
            addCriterion("upper(COUNTRY) like", value.toUpperCase(), "country");
            return (Criteria) this;
        }

        public Criteria andNationalIdLikeInsensitive(String value) {
            addCriterion("upper(NATIONAL_ID) like", value.toUpperCase(), "nationalId");
            return (Criteria) this;
        }

        public Criteria andSexLikeInsensitive(String value) {
            addCriterion("upper(SEX) like", value.toUpperCase(), "sex");
            return (Criteria) this;
        }

        public Criteria andEmailAddrLikeInsensitive(String value) {
            addCriterion("upper(EMAIL_ADDR) like", value.toUpperCase(), "emailAddr");
            return (Criteria) this;
        }

        public Criteria andChMailLikeInsensitive(String value) {
            addCriterion("upper(CH_MAIL) like", value.toUpperCase(), "chMail");
            return (Criteria) this;
        }

        public Criteria andCellPhoneLikeInsensitive(String value) {
            addCriterion("upper(CELL_PHONE) like", value.toUpperCase(), "cellPhone");
            return (Criteria) this;
        }

        public Criteria andBusnPhoneLikeInsensitive(String value) {
            addCriterion("upper(BUSN_PHONE) like", value.toUpperCase(), "busnPhone");
            return (Criteria) this;
        }

        public Criteria andHomePhoneLikeInsensitive(String value) {
            addCriterion("upper(HOME_PHONE) like", value.toUpperCase(), "homePhone");
            return (Criteria) this;
        }

        public Criteria andFaxPhoneLikeInsensitive(String value) {
            addCriterion("upper(FAX_PHONE) like", value.toUpperCase(), "faxPhone");
            return (Criteria) this;
        }

        public Criteria andAddressTypeLikeInsensitive(String value) {
            addCriterion("upper(ADDRESS_TYPE) like", value.toUpperCase(), "addressType");
            return (Criteria) this;
        }

        public Criteria andPostalLikeInsensitive(String value) {
            addCriterion("upper(POSTAL) like", value.toUpperCase(), "postal");
            return (Criteria) this;
        }

        public Criteria andAddress1LikeInsensitive(String value) {
            addCriterion("upper(ADDRESS1) like", value.toUpperCase(), "address1");
            return (Criteria) this;
        }

        public Criteria andAddress2LikeInsensitive(String value) {
            addCriterion("upper(ADDRESS2) like", value.toUpperCase(), "address2");
            return (Criteria) this;
        }

        public Criteria andSemplidLikeInsensitive(String value) {
            addCriterion("upper(SEMPLID) like", value.toUpperCase(), "semplid");
            return (Criteria) this;
        }

        public Criteria andEbsPersonidLikeInsensitive(String value) {
            addCriterion("upper(EBS_PERSONID) like", value.toUpperCase(), "ebsPersonid");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLikeInsensitive(String value) {
            addCriterion("upper(EMPLOYEE_NUMBER) like", value.toUpperCase(), "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andStdNoLikeInsensitive(String value) {
            addCriterion("upper(STD_NO) like", value.toUpperCase(), "stdNo");
            return (Criteria) this;
        }

        public Criteria andAcadOrgLikeInsensitive(String value) {
            addCriterion("upper(ACAD_ORG) like", value.toUpperCase(), "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadNameLikeInsensitive(String value) {
            addCriterion("upper(ACAD_NAME) like", value.toUpperCase(), "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdLikeInsensitive(String value) {
            addCriterion("upper(ACAD_KST_ORG_ID) like", value.toUpperCase(), "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdLikeInsensitive(String value) {
            addCriterion("upper(ACAD_EBS_ORG_ID) like", value.toUpperCase(), "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngLikeInsensitive(String value) {
            addCriterion("upper(ACAD_EBS_ORG_NAME_ENG) like", value.toUpperCase(), "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorLikeInsensitive(String value) {
            addCriterion("upper(ACAD_EBS_ORG_NAME_KOR) like", value.toUpperCase(), "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andCampusLikeInsensitive(String value) {
            addCriterion("upper(CAMPUS) like", value.toUpperCase(), "campus");
            return (Criteria) this;
        }

        public Criteria andCollegeEngLikeInsensitive(String value) {
            addCriterion("upper(COLLEGE_ENG) like", value.toUpperCase(), "collegeEng");
            return (Criteria) this;
        }

        public Criteria andCollegeKorLikeInsensitive(String value) {
            addCriterion("upper(COLLEGE_KOR) like", value.toUpperCase(), "collegeKor");
            return (Criteria) this;
        }

        public Criteria andGradEngLikeInsensitive(String value) {
            addCriterion("upper(GRAD_ENG) like", value.toUpperCase(), "gradEng");
            return (Criteria) this;
        }

        public Criteria andGradKorLikeInsensitive(String value) {
            addCriterion("upper(GRAD_KOR) like", value.toUpperCase(), "gradKor");
            return (Criteria) this;
        }

        public Criteria andKaistOrgIdLikeInsensitive(String value) {
            addCriterion("upper(KAIST_ORG_ID) like", value.toUpperCase(), "kaistOrgId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdLikeInsensitive(String value) {
            addCriterion("upper(EBS_ORGANIZATION_ID) like", value.toUpperCase(), "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngLikeInsensitive(String value) {
            addCriterion("upper(EBS_ORG_NAME_ENG) like", value.toUpperCase(), "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorLikeInsensitive(String value) {
            addCriterion("upper(EBS_ORG_NAME_KOR) like", value.toUpperCase(), "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngLikeInsensitive(String value) {
            addCriterion("upper(EBS_GRADE_NAME_ENG) like", value.toUpperCase(), "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorLikeInsensitive(String value) {
            addCriterion("upper(EBS_GRADE_NAME_KOR) like", value.toUpperCase(), "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngLikeInsensitive(String value) {
            addCriterion("upper(EBS_GRADE_LEVEL_ENG) like", value.toUpperCase(), "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorLikeInsensitive(String value) {
            addCriterion("upper(EBS_GRADE_LEVEL_KOR) like", value.toUpperCase(), "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngLikeInsensitive(String value) {
            addCriterion("upper(EBS_PERSON_TYPE_ENG) like", value.toUpperCase(), "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorLikeInsensitive(String value) {
            addCriterion("upper(EBS_PERSON_TYPE_KOR) like", value.toUpperCase(), "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngLikeInsensitive(String value) {
            addCriterion("upper(EBS_USER_STATUS_ENG) like", value.toUpperCase(), "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorLikeInsensitive(String value) {
            addCriterion("upper(EBS_USER_STATUS_KOR) like", value.toUpperCase(), "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andPositionEngLikeInsensitive(String value) {
            addCriterion("upper(POSITION_ENG) like", value.toUpperCase(), "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionKorLikeInsensitive(String value) {
            addCriterion("upper(POSITION_KOR) like", value.toUpperCase(), "positionKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusCodeLikeInsensitive(String value) {
            addCriterion("upper(STU_STATUS_CODE) like", value.toUpperCase(), "stuStatusCode");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngLikeInsensitive(String value) {
            addCriterion("upper(STU_STATUS_ENG) like", value.toUpperCase(), "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorLikeInsensitive(String value) {
            addCriterion("upper(STU_STATUS_KOR) like", value.toUpperCase(), "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeLikeInsensitive(String value) {
            addCriterion("upper(ACAD_PROG_CODE) like", value.toUpperCase(), "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorLikeInsensitive(String value) {
            addCriterion("upper(ACAD_PROG_KOR) like", value.toUpperCase(), "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngLikeInsensitive(String value) {
            addCriterion("upper(ACAD_PROG_ENG) like", value.toUpperCase(), "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andPersonGubunLikeInsensitive(String value) {
            addCriterion("upper(PERSON_GUBUN) like", value.toUpperCase(), "personGubun");
            return (Criteria) this;
        }

        public Criteria andProgEffdtLikeInsensitive(String value) {
            addCriterion("upper(PROG_EFFDT) like", value.toUpperCase(), "progEffdt");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdLikeInsensitive(String value) {
            addCriterion("upper(STDNT_TYPE_ID) like", value.toUpperCase(), "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassLikeInsensitive(String value) {
            addCriterion("upper(STDNT_TYPE_CLASS) like", value.toUpperCase(), "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameEngLikeInsensitive(String value) {
            addCriterion("upper(STDNT_TYPE_NAME_ENG) like", value.toUpperCase(), "stdntTypeNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntTypeNameKorLikeInsensitive(String value) {
            addCriterion("upper(STDNT_TYPE_NAME_KOR) like", value.toUpperCase(), "stdntTypeNameKor");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdLikeInsensitive(String value) {
            addCriterion("upper(STDNT_CATEGORY_ID) like", value.toUpperCase(), "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameEngLikeInsensitive(String value) {
            addCriterion("upper(STDNT_CATEGORY_NAME_ENG) like", value.toUpperCase(), "stdntCategoryNameEng");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryNameKorLikeInsensitive(String value) {
            addCriterion("upper(STDNT_CATEGORY_NAME_KOR) like", value.toUpperCase(), "stdntCategoryNameKor");
            return (Criteria) this;
        }

        public Criteria andBankSectLikeInsensitive(String value) {
            addCriterion("upper(BANK_SECT) like", value.toUpperCase(), "bankSect");
            return (Criteria) this;
        }

        public Criteria andAccountNoLikeInsensitive(String value) {
            addCriterion("upper(ACCOUNT_NO) like", value.toUpperCase(), "accountNo");
            return (Criteria) this;
        }

        public Criteria andAdvrEmplidLikeInsensitive(String value) {
            addCriterion("upper(ADVR_EMPLID) like", value.toUpperCase(), "advrEmplid");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdLikeInsensitive(String value) {
            addCriterion("upper(ADVR_EBS_PERSON_ID) like", value.toUpperCase(), "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrFirstNameLikeInsensitive(String value) {
            addCriterion("upper(ADVR_FIRST_NAME) like", value.toUpperCase(), "advrFirstName");
            return (Criteria) this;
        }

        public Criteria andAdvrLastNameLikeInsensitive(String value) {
            addCriterion("upper(ADVR_LAST_NAME) like", value.toUpperCase(), "advrLastName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameLikeInsensitive(String value) {
            addCriterion("upper(ADVR_NAME) like", value.toUpperCase(), "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcLikeInsensitive(String value) {
            addCriterion("upper(ADVR_NAME_AC) like", value.toUpperCase(), "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andEbsStartDateLikeInsensitive(String value) {
            addCriterion("upper(EBS_START_DATE) like", value.toUpperCase(), "ebsStartDate");
            return (Criteria) this;
        }

        public Criteria andEbsEndDateLikeInsensitive(String value) {
            addCriterion("upper(EBS_END_DATE) like", value.toUpperCase(), "ebsEndDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateLikeInsensitive(String value) {
            addCriterion("upper(PROG_START_DATE) like", value.toUpperCase(), "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateLikeInsensitive(String value) {
            addCriterion("upper(PROG_END_DATE) like", value.toUpperCase(), "progEndDate");
            return (Criteria) this;
        }

        public Criteria andOuterStartDateLikeInsensitive(String value) {
            addCriterion("upper(OUTER_START_DATE) like", value.toUpperCase(), "outerStartDate");
            return (Criteria) this;
        }

        public Criteria andOuterEndDateLikeInsensitive(String value) {
            addCriterion("upper(OUTER_END_DATE) like", value.toUpperCase(), "outerEndDate");
            return (Criteria) this;
        }

        public Criteria andRegisterDateLikeInsensitive(String value) {
            addCriterion("upper(REGISTER_DATE) like", value.toUpperCase(), "registerDate");
            return (Criteria) this;
        }

        public Criteria andRegisterIpLikeInsensitive(String value) {
            addCriterion("upper(REGISTER_IP) like", value.toUpperCase(), "registerIp");
            return (Criteria) this;
        }

        public Criteria andExtOrgIdLikeInsensitive(String value) {
            addCriterion("upper(EXT_ORG_ID) like", value.toUpperCase(), "extOrgId");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameEngLikeInsensitive(String value) {
            addCriterion("upper(EXT_ETC_ORG_NAME_ENG) like", value.toUpperCase(), "extEtcOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtEtcOrgNameKorLikeInsensitive(String value) {
            addCriterion("upper(EXT_ETC_ORG_NAME_KOR) like", value.toUpperCase(), "extEtcOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameEngLikeInsensitive(String value) {
            addCriterion("upper(EXT_ORG_NAME_ENG) like", value.toUpperCase(), "extOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andExtOrgNameKorLikeInsensitive(String value) {
            addCriterion("upper(EXT_ORG_NAME_KOR) like", value.toUpperCase(), "extOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgCodeLikeInsensitive(String value) {
            addCriterion("upper(FORE_ACAD_PROG_CODE) like", value.toUpperCase(), "foreAcadProgCode");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgKorLikeInsensitive(String value) {
            addCriterion("upper(FORE_ACAD_PROG_KOR) like", value.toUpperCase(), "foreAcadProgKor");
            return (Criteria) this;
        }

        public Criteria andForeAcadProgEngLikeInsensitive(String value) {
            addCriterion("upper(FORE_ACAD_PROG_ENG) like", value.toUpperCase(), "foreAcadProgEng");
            return (Criteria) this;
        }

        public Criteria andForeStdNoLikeInsensitive(String value) {
            addCriterion("upper(FORE_STD_NO) like", value.toUpperCase(), "foreStdNo");
            return (Criteria) this;
        }

        public Criteria andForeAcadOrgLikeInsensitive(String value) {
            addCriterion("upper(FORE_ACAD_ORG) like", value.toUpperCase(), "foreAcadOrg");
            return (Criteria) this;
        }

        public Criteria andForeAcadNameLikeInsensitive(String value) {
            addCriterion("upper(FORE_ACAD_NAME) like", value.toUpperCase(), "foreAcadName");
            return (Criteria) this;
        }

        public Criteria andForeAcadKstOrgIdLikeInsensitive(String value) {
            addCriterion("upper(FORE_ACAD_KST_ORG_ID) like", value.toUpperCase(), "foreAcadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgIdLikeInsensitive(String value) {
            addCriterion("upper(FORE_ACAD_EBS_ORG_ID) like", value.toUpperCase(), "foreAcadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameEngLikeInsensitive(String value) {
            addCriterion("upper(FORE_ACAD_EBS_ORG_NAME_ENG) like", value.toUpperCase(), "foreAcadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andForeAcadEbsOrgNameKorLikeInsensitive(String value) {
            addCriterion("upper(FORE_ACAD_EBS_ORG_NAME_KOR) like", value.toUpperCase(), "foreAcadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andForeCampusLikeInsensitive(String value) {
            addCriterion("upper(FORE_CAMPUS) like", value.toUpperCase(), "foreCampus");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusCodeLikeInsensitive(String value) {
            addCriterion("upper(FORE_STU_STATUS_CODE) like", value.toUpperCase(), "foreStuStatusCode");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusEngLikeInsensitive(String value) {
            addCriterion("upper(FORE_STU_STATUS_ENG) like", value.toUpperCase(), "foreStuStatusEng");
            return (Criteria) this;
        }

        public Criteria andForeStuStatusKorLikeInsensitive(String value) {
            addCriterion("upper(FORE_STU_STATUS_KOR) like", value.toUpperCase(), "foreStuStatusKor");
            return (Criteria) this;
        }

        public Criteria andForeProgStartDateLikeInsensitive(String value) {
            addCriterion("upper(FORE_PROG_START_DATE) like", value.toUpperCase(), "foreProgStartDate");
            return (Criteria) this;
        }

        public Criteria andForeProgEndDateLikeInsensitive(String value) {
            addCriterion("upper(FORE_PROG_END_DATE) like", value.toUpperCase(), "foreProgEndDate");
            return (Criteria) this;
        }

        public Criteria andSlastupddttmLikeInsensitive(String value) {
            addCriterion("upper(SLASTUPDDTTM) like", value.toUpperCase(), "slastupddttm");
            return (Criteria) this;
        }

        public Criteria andCombindedNameLikeInsensitive(String value) {
            addCriterion("upper(COMBINDED_NAME) like", value.toUpperCase(), "combindedName");
            return (Criteria) this;
        }

        public Criteria andCombindedStatusLikeInsensitive(String value) {
            addCriterion("upper(COMBINDED_STATUS) like", value.toUpperCase(), "combindedStatus");
            return (Criteria) this;
        }

        public Criteria andGrantedServicesLikeInsensitive(String value) {
            addCriterion("upper(GRANTED_SERVICES) like", value.toUpperCase(), "grantedServices");
            return (Criteria) this;
        }

        public Criteria andIPinLikeInsensitive(String value) {
            addCriterion("upper(I_PIN) like", value.toUpperCase(), "iPin");
            return (Criteria) this;
        }

        public Criteria andHighschoolCodeLikeInsensitive(String value) {
            addCriterion("upper(HIGHSCHOOL_CODE) like", value.toUpperCase(), "highschoolCode");
            return (Criteria) this;
        }

        public Criteria andHighschoolNameLikeInsensitive(String value) {
            addCriterion("upper(HIGHSCHOOL_NAME) like", value.toUpperCase(), "highschoolName");
            return (Criteria) this;
        }

        public Criteria andKaistUidLikeInsensitive(String value) {
            addCriterion("upper(KAIST_UID) like", value.toUpperCase(), "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidLikeInsensitive(String value) {
            addCriterion("upper(KAIST_SUID) like", value.toUpperCase(), "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andClCampusLikeInsensitive(String value) {
            addCriterion("upper(CL_CAMPUS) like", value.toUpperCase(), "clCampus");
            return (Criteria) this;
        }

        public Criteria andClBuildingLikeInsensitive(String value) {
            addCriterion("upper(CL_BUILDING) like", value.toUpperCase(), "clBuilding");
            return (Criteria) this;
        }

        public Criteria andClFloorLikeInsensitive(String value) {
            addCriterion("upper(CL_FLOOR) like", value.toUpperCase(), "clFloor");
            return (Criteria) this;
        }

        public Criteria andClRoomNoLikeInsensitive(String value) {
            addCriterion("upper(CL_ROOM_NO) like", value.toUpperCase(), "clRoomNo");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidLikeInsensitive(String value) {
            addCriterion("upper(ADVR_KAIST_UID) like", value.toUpperCase(), "advrKaistUid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}