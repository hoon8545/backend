package com.bandi.service.orgSync.source;


import java.util.List;

public interface SourceConcurrentUserMapper {
    long countByCondition(SourceConcurrentUserCondition condition);

    List<SourceConcurrentUser> selectByCondition(SourceConcurrentUserCondition condition);

    List<SourceConcurrentUser> getAll();
}