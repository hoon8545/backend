package com.bandi.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import com.bandi.domain.base.BaseObject;

public class SyncGroup extends BaseObject implements Serializable {
    private String oid;

    private String id;

    private String name;

    private String description;

    private int sortOrder;

    private String parentId;

    private String actionType;

    private String actionStatus;

    private Timestamp createdAt;

    private Timestamp appliedAt;

    private String errorMessage;

    private String groupCode;

    private String creatorId;

    private String updatorId;

    private Timestamp updatedAt;

	private long seq;

    private static final long serialVersionUID = 1L;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid == null ? null : oid.trim();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId == null ? null : parentId.trim();
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType == null ? null : actionType.trim();
    }

    public String getActionStatus() {
        return actionStatus;
    }

    public void setActionStatus(String actionStatus) {
        this.actionStatus = actionStatus == null ? null : actionStatus.trim();
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getAppliedAt() {
        return appliedAt;
    }

    public void setAppliedAt(Timestamp appliedAt) {
        this.appliedAt = appliedAt;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage == null ? null : errorMessage.trim();
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode == null ? null : groupCode.trim();
    }

	public long getSeq() {
        return seq;
    }

    public void setSeq(long seq) {
        this.seq = seq;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", oid=").append(oid);
        sb.append(", id=").append(id);
        sb.append(", groupCode=").append(groupCode);
        sb.append(", name=").append(name);
        sb.append(", description=").append(description);
        sb.append(", sortOrder=").append(sortOrder);
        sb.append(", parentId=").append(parentId);
        sb.append(", actionType=").append(actionType);
        sb.append(", actionStatus=").append(actionStatus);
        sb.append(", creatorId=").append(creatorId);
        sb.append(", updatorId=").append(updatorId);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", appliedAt=").append(appliedAt);
        sb.append(", errorMessage=").append(errorMessage);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", seq=").append(seq);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    // ===================== End of Code Gen =====================
    private Timestamp[] createdAtBetween;
    private Timestamp[] appliedAtBetween;

    public Timestamp[] getAppliedAtBetween() {
        return appliedAtBetween;
    }

    public void setAppliedAtBetween(Timestamp[] appliedAtBetween) {
        this.appliedAtBetween = appliedAtBetween;
    }

    public Timestamp[] getCreatedAtBetween() {
        return createdAtBetween;
    }

    public void setCreatedAtBetween(Timestamp[] createdAtBetween) {
        this.createdAtBetween = createdAtBetween;
    }

    private List<String> multipleActionType;

    public List<String> getMultipleActionType() {
        return multipleActionType;
    }

    public void setMultipleActionType(List<String> multipleActionType) {
        this.multipleActionType = multipleActionType;
    }

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}