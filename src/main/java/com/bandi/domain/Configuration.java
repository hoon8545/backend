package com.bandi.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.bandi.common.BandiConstants;
import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.domain.base.BaseObject;

public class Configuration extends BaseObject implements Serializable {
    private String configKey;

    private String configValue;

    private String description;

    private String hashValue;

    private String flagValid;
    
    private String creatorId;

    private Timestamp createdAt;

    private String updatorId;

    private Timestamp updatedAt;

    private static final long serialVersionUID = 1L;

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey == null ? null : configKey.trim();
    }

    public String getConfigValue() {
        return configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue == null ? null : configValue.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getHashValue() {
        return hashValue;
    }

    public void setHashValue(String hashValue) {
        this.hashValue = hashValue == null ? null : hashValue.trim();
    }

    public String getFlagValid() {
        return flagValid;
    }

    public void setFlagValid(String flagValid) {
        this.flagValid = flagValid == null ? null : flagValid.trim();
    }

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", configKey=").append(configKey);
		sb.append(", configValue=").append(configValue);
		sb.append(", description=").append(description);
                sb.append(", flagValid=").append(flagValid);
		sb.append("]");
		return sb.toString();
	}
	//----------------------------- End of Code Gen --------------------------------

	public String getHashTarget(){
		StringBuilder sb = new StringBuilder();

		sb.append(configKey).append(", ");
		sb.append(configValue);

		return sb.toString();
	}

	public String toStringForHashValue() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("configKey=").append(configKey);
		sb.append(", configValue=").append(configValue);
		sb.append("]");
		return sb.toString();
	}

    public String get_rowVariant(){
        return BandiConstants.FLAG_N.equalsIgnoreCase( this.flagValid) ? "danger" : "";
    }

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	//--------KAIST
	
	public static List<String> getColumnList() {
		List<String> columnList = new ArrayList<String>();
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_ALLOW_MULTI_LOGIN); 
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_PASSWORD_MIN_LENGTH);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_PASSWORD_SPECIAL_CHARACTERS);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_PASSWORD_TYPE);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_POLICY_USER_AUTHORIZATION_PERIOD);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_PASSWORD_UPPER_CASE_NUM);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_PASSWORD_LOWER_CASE_NUM);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_PASSWORD_SPECIAL_CHARACTER_NUM);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_PASSWORD_NUMBER_NUM);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_POLICY_OTP_AUTHORIZATION_TIMES);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_POLICY_USER_PASSWORD_CHANGE_PERIOD_USE);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_POLICY_USER_PASSWORD_NOTICE_SETTING);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_PASSWORD_NON_REPEATABLE_NUMBER);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_PASSWORD_NON_INPUTABLE_SPECIAL_CHARACTERS);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_POLICY_USER_PASSWORD_CHANGE_PERIOD);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_RECENT_PASSWORD_NON_INPUTABLE_NUMBER);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_ID_SPECIAL_CHARACTERS);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_OTP_LONG_TERM_NON_USER_DAY);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_OTP_FAIL_COUNT);
		columnList.add(BandiConstants_KAIST.CONFIGURATION_COLUMN_ID_MIN_LENGTH_USER);
		
		return columnList;
	}
}