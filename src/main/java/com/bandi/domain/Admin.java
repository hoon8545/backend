package com.bandi.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import com.bandi.common.BandiConstants;
import com.bandi.domain.base.BaseObject;

public class Admin extends BaseObject implements Serializable {
    private String id;

    private String name;

    private String password;

    private String handphone;

    private String email;

    private String flagUse;

    private String creatorId;

    private Timestamp createdAt;

    private String updatorId;

    private Timestamp updatedAt;

    private int loginFailCount;

    private Timestamp loginFailedAt;

    private Timestamp passwordChangedAt;

    private String role;

    private String hashValue;

    private String passwordInitializeFlag;

    private String adminGrantFlag;

    private String flagValid;

    private int module;
    private String userId;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getHandphone() {
        return handphone;
    }

    public void setHandphone(String handphone) {
        this.handphone = handphone == null ? null : handphone.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getFlagUse() {
        return flagUse;
    }

    public void setFlagUse(String flagUse) {
        this.flagUse = flagUse == null ? null : flagUse.trim();
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId == null ? null : creatorId.trim();
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(String updatorId) {
        this.updatorId = updatorId == null ? null : updatorId.trim();
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getLoginFailCount() {
        return loginFailCount;
    }

    public void setLoginFailCount(int loginFailCount) {
        this.loginFailCount = loginFailCount;
    }

    public Timestamp getLoginFailedAt() {
        return loginFailedAt;
    }

    public void setLoginFailedAt(Timestamp loginFailedAt) {
        this.loginFailedAt = loginFailedAt;
    }

    public Timestamp getPasswordChangedAt() {
        return passwordChangedAt;
    }

    public void setPasswordChangedAt(Timestamp passwordChangedAt) {
        this.passwordChangedAt = passwordChangedAt;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role == null ? null : role.trim();
    }

    public String getHashValue() {
        return hashValue;
    }

    public void setHashValue(String hashValue) {
        this.hashValue = hashValue == null ? null : hashValue.trim();
    }

    public String getPasswordInitializeFlag() {
        return passwordInitializeFlag;
    }

    public void setPasswordInitializeFlag(String passwordInitializeFlag) {
        this.passwordInitializeFlag = passwordInitializeFlag == null ? null : passwordInitializeFlag.trim();
    }

    public String getAdminGrantFlag() {
        return adminGrantFlag;
    }

    public void setAdminGrantFlag(String adminGrantFlag) {
        this.adminGrantFlag = adminGrantFlag == null ? null : adminGrantFlag.trim();
    }

    public String getFlagValid() {
        return flagValid;
    }

    public void setFlagValid(String flagValid) {
        this.flagValid = flagValid == null ? null : flagValid.trim();
    }

    public int getModule() {
        return module;
    }

    public void setModule(int module) {
        this.module = module;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", password=").append("***");
        sb.append(", handphone=").append(handphone);
        sb.append(", email=").append(email);
        sb.append(", flagUse=").append(flagUse);
        sb.append(", role=").append(role);
        sb.append(", loginFailCount=").append(loginFailCount);
        sb.append(", loginFailedAt=").append(loginFailedAt);
        sb.append(", passwordChangedAt=").append(passwordChangedAt);
        sb.append(", flagValid=").append(flagValid);
        sb.append(", creatorId=").append(creatorId);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", updatorId=").append(updatorId);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", passwordInitializeFlag=").append(passwordInitializeFlag);
        sb.append(", adminGrantFlag=").append(adminGrantFlag);
        sb.append(", module=").append(module);
        sb.append(", userId=").append(userId);
        sb.append("]");
        return sb.toString();
    }

    //----------------------------- End of Code Gen --------------------------------

    private String passwordInitBody;

    private String originalPassword;

	public String getPasswordInitBody() {
		return passwordInitBody;
	}

	public void setPasswordInitBody(String passwordInitBody) {
		this.passwordInitBody = passwordInitBody == null ? null : passwordInitBody.trim();
	}

	public String getOriginalPassword() {
		return originalPassword;
	}

	public void setOriginalPassword(String originalPassword) {
		this.originalPassword = originalPassword == null ? null : originalPassword.trim();
	}

	public String getHashTarget(){
		StringBuilder sb = new StringBuilder();

		sb.append(id).append(", ");
		sb.append(name).append(", ");
		sb.append(password).append(", ");
		sb.append(handphone).append(", ");
		sb.append(email).append(", ");
		sb.append(flagUse).append(", ");
		sb.append(loginFailCount).append(", ");
		sb.append(loginFailedAt).append(", ");
		sb.append(passwordChangedAt).append(", ");
		sb.append(role).append(", ");

		return sb.toString();
	}

	public String toStringForHashValue() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("id=").append(id);
		sb.append(", name=").append(name);
		sb.append("]");
		return sb.toString();
	}

    public String get_rowVariant(){
        return BandiConstants.FLAG_N.equalsIgnoreCase( this.flagValid) ? "danger" : "";
    }

}