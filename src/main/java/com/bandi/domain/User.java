package com.bandi.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import com.bandi.common.BandiConstants;
import com.bandi.common.util.ContextUtil;
import com.bandi.domain.base.BaseObject;

public class User extends BaseObject implements Serializable {
    private String id;

    private String password;

    private String name;

    private String email;

    private String handphone;

    private Timestamp passwordChangedAt;

    private int loginFailCount;

    private Timestamp loginFailedAt;

    private String flagUse;

    private transient String creatorId;

    private transient Timestamp createdAt;

    private transient String updatorId;

    private transient Timestamp updatedAt;

    private String hashValue;

    private String flagValid;

    private transient static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getHandphone() {
        return handphone;
    }

    public void setHandphone(String handphone) {
        this.handphone = handphone == null ? null : handphone.trim();
    }

    public Timestamp getPasswordChangedAt() {
        return passwordChangedAt;
    }

    public void setPasswordChangedAt(Timestamp passwordChangedAt) {
        this.passwordChangedAt = passwordChangedAt;
    }

    public int getLoginFailCount() {
        return loginFailCount;
    }

    public void setLoginFailCount(int loginFailCount) {
        this.loginFailCount = loginFailCount;
    }

    public Timestamp getLoginFailedAt() {
        return loginFailedAt;
    }

    public void setLoginFailedAt(Timestamp loginFailedAt) {
        this.loginFailedAt = loginFailedAt;
    }

    public String getFlagUse() {
        return flagUse;
    }

    public void setFlagUse(String flagUse) {
        this.flagUse = flagUse == null ? null : flagUse.trim();
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId == null ? null : creatorId.trim();
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(String updatorId) {
        this.updatorId = updatorId == null ? null : updatorId.trim();
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getHashValue() {
        return hashValue;
    }

    public void setHashValue(String hashValue) {
        this.hashValue = hashValue == null ? null : hashValue.trim();
    }

    public String getFlagValid() {
        return flagValid;
    }

    public void setFlagValid(String flagValid) {
        this.flagValid = flagValid == null ? null : flagValid.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", groupId=").append(groupId);
        sb.append(", originalGroupId=").append(originalGroupId);
        sb.append(", password=").append(password);
        sb.append(", name=").append(name);
        sb.append(", email=").append(email);
        sb.append(", handphone=").append(handphone);
        sb.append(", passwordChangedAt=").append(passwordChangedAt);
        sb.append(", loginFailCount=").append(loginFailCount);
        sb.append(", loginFailedAt=").append(loginFailedAt);
        sb.append(", flagUse=").append(flagUse);
        sb.append(", creatorId=").append(creatorId);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", updatorId=").append(updatorId);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", hashValue=").append(hashValue);
        sb.append(", flagValid=").append(flagValid);
        sb.append(", genericId=").append(genericId);
        sb.append(", title=").append(title);
        sb.append(", position=").append(position);
        sb.append(", rank=").append(rank);
        sb.append(", userType=").append(userType);
        sb.append(", flagTemp=").append(flagTemp);
        sb.append(", tempStartDat=").append(tempStartDat);
        sb.append(", tempEndDat=").append(tempEndDat);
        sb.append(", flagSync=").append(flagSync);
		sb.append(", kaistUid=").append(kaistUid);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }


    // -----------------------------------------------------------------------------------------------------------------
    // IM --------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------

	private String groupId;

	private String originalGroupId;

    private String genericId;

    private int title;

    private int position;

    private int rank;

    private String userType;

    private String flagTemp;

    private Timestamp tempStartDat;

    private Timestamp tempEndDat;

    private String flagSync;

    public String getGroupId() {
		return groupId;
	}

	public void setGroupId( String groupId ) {
		this.groupId = groupId == null ? null : groupId.trim();
	}

    public String getOriginalGroupId() {
        return originalGroupId;
    }

    public void setOriginalGroupId(String originalGroupId) {
        this.originalGroupId = originalGroupId == null ? null : originalGroupId.trim();
    }

    public String getGenericId() {
        return genericId;
    }

    public void setGenericId(String genericId) {
        this.genericId = genericId == null ? null : genericId.trim();
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType == null ? null : userType.trim();
    }

    public String getFlagTemp() {
        return flagTemp;
    }

    public void setFlagTemp(String flagTemp) {
        this.flagTemp = flagTemp == null ? null : flagTemp.trim();
    }

    public Timestamp getTempStartDat() {
        return tempStartDat;
    }

    public void setTempStartDat(Timestamp tempStartDat) {
        this.tempStartDat = tempStartDat;
    }

    public Timestamp getTempEndDat() {
        return tempEndDat;
    }

    public void setTempEndDat(Timestamp tempEndDat) {
        this.tempEndDat = tempEndDat;
    }

    public String getFlagSync() {
        return flagSync;
    }

    public void setFlagSync(String flagSync) {
        this.flagSync = flagSync == null ? null : flagSync.trim();
    }

	//----------------------------- End of Code Gen --------------------------------

    private String passwordInitBody;

	public String getPasswordInitBody() {
		return passwordInitBody;
	}

	public void setPasswordInitBody(String passwordInitBody) {
		this.passwordInitBody = passwordInitBody == null ? null : passwordInitBody.trim();
	}

	public String getHashTarget(){
		StringBuilder sb = new StringBuilder();

		sb.append(id).append(", ");
		sb.append(name).append(", ");
		sb.append(password).append(", ");
		sb.append(handphone).append(", ");
		sb.append(email).append(", ");
		sb.append(flagUse).append(", ");
		sb.append(loginFailCount).append(", ");
		sb.append(loginFailedAt).append(", ");
		sb.append(passwordChangedAt);

		return sb.toString();
	}

	public String toStringForHashValue() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("id=").append(id);
		sb.append(", name=").append(name);
		sb.append("]");
		return sb.toString();
	}

	private Timestamp[] tempStartEndBetween;

    public Timestamp[] getLoginAtBetween() {
        return tempStartEndBetween;
    }

    private String groupName;

    private String concurrentGroupName;

    private String titleName;

    private String positionName;

    private String rankName;

    private String oldPassword;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getConcurrentGroupName() {
        return concurrentGroupName;
    }

    public void setConcurrentGroupName(String concurrentGroupName) {
        this.concurrentGroupName = concurrentGroupName;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getRankName() {
        return rankName;
    }

    public void setRankName(String rankName) {
        this.rankName = rankName;
    }

    public String get_rowVariant(){
        return BandiConstants.FLAG_N.equalsIgnoreCase( this.flagValid) ? "danger" : "";
    }

    private String clientOid;

    public String getClientOid(){
        return clientOid;
    }

    public void setClientOid( String clientOid ){
        this.clientOid = clientOid;
    }

    // -- KAIST
	private String passwordInitializeFlag;

	public String getPasswordInitializeFlag() {
        return passwordInitializeFlag;
    }

    public void setPasswordInitializeFlag(String passwordInitializeFlag) {
        this.passwordInitializeFlag = passwordInitializeFlag == null ? null : passwordInitializeFlag.trim();
    }

	private String kaistUid;

	    public String getKaistUid() {
        return kaistUid;
    }

    public void setKaistUid(String kaistUid) {
        this.kaistUid = kaistUid == null ? null : kaistUid.trim();
    }

    public String birthday;

    public String employeeNumber;

    public String koreanName;

    public String mobileTelephoneNumber;

    public String postNumber;

    public String address;

    public String addressDetail;

    public String personTypeCodeUid;

    public String organizationUid;

    public String emailAddress;

    public String emailAddressSub;

    public String englishName;

    public String nation;

    public String nationCodeUid;

    public String sex;

    public String sexCodeUid;

    public String ownHomeTelephoneNumber;

    public String residentNumber;

    public String employeeUid;

    public String officeTelephoneNumber;

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday == null ? null : birthday.trim();
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber == null ? null : employeeNumber.trim();
    }

    public String getMobileTelephoneNumber() {
        return mobileTelephoneNumber;
    }

    public void setMobileTelephoneNumber(String mobileTelephoneNumber) {
        this.mobileTelephoneNumber = mobileTelephoneNumber;
    }

    public String getKoreanName() {
        return koreanName;
    }

    public void setKoreanName(String koreanName) {
        this.koreanName = koreanName;
    }

    public String getPostNumber() {
        return postNumber;
    }

    public void setPostNumber(String postNumber) {
        this.postNumber = postNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailAddressSub() {
        return emailAddressSub;
    }

    public void setEmailAddressSub(String emailAddressSub) {
        this.emailAddressSub = emailAddressSub;
    }

    public String getPersonTypeCodeUid() {
        return personTypeCodeUid;
    }

    public void setPersonTypeCodeUid(String personTypeCodeUid) {
        this.personTypeCodeUid = personTypeCodeUid;
    }

    public String getOrganizationUid() {
        return organizationUid;
    }

    public void setOrganizationUid(String organizationUid) {
        this.organizationUid = organizationUid;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getNationCodeUid() {
        return nationCodeUid;
    }

    public void setNationCodeUid(String nationCodeUid) {
        this.nationCodeUid = nationCodeUid;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getOwnHomeTelephoneNumber() {
        return ownHomeTelephoneNumber;
    }

    public void setOwnHomeTelephoneNumber(String ownHomeTelephoneNumber) {
        this.ownHomeTelephoneNumber = ownHomeTelephoneNumber;
    }

    public String getResidentNumber() {
        return residentNumber;
    }

    public void setResidentNumber(String residentNumber) {
        this.residentNumber = residentNumber;
    }

    public String getEmployeeUid() {
        return employeeUid;
    }

    public void setEmployeeUid(String employeeUid) {
        this.employeeUid = employeeUid;
    }

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getSexCodeUid() {
		return sexCodeUid;
	}

	public void setSexCodeUid(String sexCodeUid) {
		this.sexCodeUid = sexCodeUid;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getOfficeTelephoneNumber() {
        return officeTelephoneNumber;
    }

    public void setOfficeTelephoneNumber(String officeTelephoneNumber) {
        this.officeTelephoneNumber = officeTelephoneNumber == null ? null : officeTelephoneNumber.trim();
    }

	/* SyncUser */
	private String register_ip;

	private String syncResult;

	private String ipin = ContextUtil.getCurrentRemoteIp();

	private String reqReason;

	private Date effdt = new Date();

	public String getReqReason() {
		return reqReason;
	}

	public void setReqReason(String reqReason) {
		this.reqReason = reqReason;
	}

	public String getIpin() {
		return ipin;
	}

	public Date getEffdt() {
		return effdt;
	}

	public String getRegister_ip() {
		return register_ip;
	}

	public void setRegister_ip(String register_ip) {
		this.register_ip = register_ip;
	}

	public String getSyncResult() {
		return syncResult;
	}

	public void setSyncResult(String syncResult) {
		this.syncResult = syncResult;
	}

	/* 겸직사용자 포함여부*/
	private boolean excludeConcurrentUser;

    public boolean isExcludeConcurrentUser() {
        return excludeConcurrentUser;
    }

    public void setExcludeConcurrentUser(boolean excludeConcurrentUser) {
        this.excludeConcurrentUser = excludeConcurrentUser;
    }

    /* USERTYPE NAME*/
    private String userTypeName;

    public String getUserTypeName() {
        return userTypeName;
    }

    public void setUserTypeName(String userTypeName) {
        this.userTypeName = userTypeName;
    }

    private String personId;

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

}