package com.bandi.domain;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import com.bandi.domain.base.BaseObject;

public class SyncUser extends BaseObject implements Serializable {
    private String oid;

    private String kaistUid;

    private String id;

    private String koreanName;

    private String englishName;

    private String lastName;

    private String firstName;

    private Date birthday;

    private String nationCodeUid;

    private String sexCodeUid;

    private String emailAddress;

    private String chMail;

    private String mobileTelephoneNumber;

    private String officeTelephoneNumber;

    private String oweHomeTelephoneNumber;

    private String faxTelephoneNumber;

    private String postNumber;

    private String address;

    private String addressDetail;

    private String personId;

    private String employeeNumber;

    private String stdNo;

    private String acadOrg;

    private String acadName;

    private String acadKstOrgId;

    private String acadEbsOrgId;

    private String acadEbsOrgNameEng;

    private String acadEbsOrgNameKor;

    private String campusUid;

    private String ebsOrganizationId;

    private String ebsOrgNameEng;

    private String ebsOrgNameKor;

    private String ebsGradeNameEng;

    private String ebsGradeNameKor;

    private String ebsGradeLevelEng;

    private String ebsGradeLevelKor;

    private String ebsPersonTypeEng;

    private String ebsPersonTypeKor;

    private String ebsUserStatusEng;

    private String ebsUserStatusKor;

    private String positionEng;

    private String positionKor;

    private String stuStatusEng;

    private String stuStatusKor;

    private String acadProgCode;

    private String acadProgKor;

    private String acadProgEng;

    private String personTypeCodeUid;

    private Date progEffdt;

    private String stdntTypeId;

    private String stdntTypeClass;

    private String stdntCategoryId;

    private String advrEbsPersonId;

    private String advrName;

    private String advrNameAc;

    private Date entranceDate;

    private Date resignDate;

    private Date progStartDate;

    private Date progEndDate;

    private String kaistSuid;

    private String advrKaistUid;

    private String actionType;

    private String actionStatus;

    private Timestamp createdAt;

    private String creatorId;

    private String updatorId;

    private Timestamp updatedAt;

    private Timestamp appliedAt;

    private String errorMessage;

    private long seq;

    private static final long serialVersionUID = 1L;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid == null ? null : oid.trim();
    }

    public String getKaistUid() {
        return kaistUid;
    }

    public void setKaistUid(String kaistUid) {
        this.kaistUid = kaistUid == null ? null : kaistUid.trim();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getKoreanName() {
        return koreanName;
    }

    public void setKoreanName(String koreanName) {
        this.koreanName = koreanName == null ? null : koreanName.trim();
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName == null ? null : englishName.trim();
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName == null ? null : lastName.trim();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName == null ? null : firstName.trim();
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getNationCodeUid() {
        return nationCodeUid;
    }

    public void setNationCodeUid(String nationCodeUid) {
        this.nationCodeUid = nationCodeUid == null ? null : nationCodeUid.trim();
    }

    public String getSexCodeUid() {
        return sexCodeUid;
    }

    public void setSexCodeUid(String sexCodeUid) {
        this.sexCodeUid = sexCodeUid == null ? null : sexCodeUid.trim();
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress == null ? null : emailAddress.trim();
    }

    public String getChMail() {
        return chMail;
    }

    public void setChMail(String chMail) {
        this.chMail = chMail == null ? null : chMail.trim();
    }

    public String getMobileTelephoneNumber() {
        return mobileTelephoneNumber;
    }

    public void setMobileTelephoneNumber(String mobileTelephoneNumber) {
        this.mobileTelephoneNumber = mobileTelephoneNumber == null ? null : mobileTelephoneNumber.trim();
    }

    public String getOfficeTelephoneNumber() {
        return officeTelephoneNumber;
    }

    public void setOfficeTelephoneNumber(String officeTelephoneNumber) {
        this.officeTelephoneNumber = officeTelephoneNumber == null ? null : officeTelephoneNumber.trim();
    }

    public String getOweHomeTelephoneNumber() {
        return oweHomeTelephoneNumber;
    }

    public void setOweHomeTelephoneNumber(String oweHomeTelephoneNumber) {
        this.oweHomeTelephoneNumber = oweHomeTelephoneNumber == null ? null : oweHomeTelephoneNumber.trim();
    }

    public String getFaxTelephoneNumber() {
        return faxTelephoneNumber;
    }

    public void setFaxTelephoneNumber(String faxTelephoneNumber) {
        this.faxTelephoneNumber = faxTelephoneNumber == null ? null : faxTelephoneNumber.trim();
    }

    public String getPostNumber() {
        return postNumber;
    }

    public void setPostNumber(String postNumber) {
        this.postNumber = postNumber == null ? null : postNumber.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail == null ? null : addressDetail.trim();
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId == null ? null : personId.trim();
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber == null ? null : employeeNumber.trim();
    }

    public String getStdNo() {
        return stdNo;
    }

    public void setStdNo(String stdNo) {
        this.stdNo = stdNo == null ? null : stdNo.trim();
    }

    public String getAcadOrg() {
        return acadOrg;
    }

    public void setAcadOrg(String acadOrg) {
        this.acadOrg = acadOrg == null ? null : acadOrg.trim();
    }

    public String getAcadName() {
        return acadName;
    }

    public void setAcadName(String acadName) {
        this.acadName = acadName == null ? null : acadName.trim();
    }

    public String getAcadKstOrgId() {
        return acadKstOrgId;
    }

    public void setAcadKstOrgId(String acadKstOrgId) {
        this.acadKstOrgId = acadKstOrgId == null ? null : acadKstOrgId.trim();
    }

    public String getAcadEbsOrgId() {
        return acadEbsOrgId;
    }

    public void setAcadEbsOrgId(String acadEbsOrgId) {
        this.acadEbsOrgId = acadEbsOrgId == null ? null : acadEbsOrgId.trim();
    }

    public String getAcadEbsOrgNameEng() {
        return acadEbsOrgNameEng;
    }

    public void setAcadEbsOrgNameEng(String acadEbsOrgNameEng) {
        this.acadEbsOrgNameEng = acadEbsOrgNameEng == null ? null : acadEbsOrgNameEng.trim();
    }

    public String getAcadEbsOrgNameKor() {
        return acadEbsOrgNameKor;
    }

    public void setAcadEbsOrgNameKor(String acadEbsOrgNameKor) {
        this.acadEbsOrgNameKor = acadEbsOrgNameKor == null ? null : acadEbsOrgNameKor.trim();
    }

    public String getCampusUid() {
        return campusUid;
    }

    public void setCampusUid(String campusUid) {
        this.campusUid = campusUid == null ? null : campusUid.trim();
    }

    public String getEbsOrganizationId() {
        return ebsOrganizationId;
    }

    public void setEbsOrganizationId(String ebsOrganizationId) {
        this.ebsOrganizationId = ebsOrganizationId == null ? null : ebsOrganizationId.trim();
    }

    public String getEbsOrgNameEng() {
        return ebsOrgNameEng;
    }

    public void setEbsOrgNameEng(String ebsOrgNameEng) {
        this.ebsOrgNameEng = ebsOrgNameEng == null ? null : ebsOrgNameEng.trim();
    }

    public String getEbsOrgNameKor() {
        return ebsOrgNameKor;
    }

    public void setEbsOrgNameKor(String ebsOrgNameKor) {
        this.ebsOrgNameKor = ebsOrgNameKor == null ? null : ebsOrgNameKor.trim();
    }

    public String getEbsGradeNameEng() {
        return ebsGradeNameEng;
    }

    public void setEbsGradeNameEng(String ebsGradeNameEng) {
        this.ebsGradeNameEng = ebsGradeNameEng == null ? null : ebsGradeNameEng.trim();
    }

    public String getEbsGradeNameKor() {
        return ebsGradeNameKor;
    }

    public void setEbsGradeNameKor(String ebsGradeNameKor) {
        this.ebsGradeNameKor = ebsGradeNameKor == null ? null : ebsGradeNameKor.trim();
    }

    public String getEbsGradeLevelEng() {
        return ebsGradeLevelEng;
    }

    public void setEbsGradeLevelEng(String ebsGradeLevelEng) {
        this.ebsGradeLevelEng = ebsGradeLevelEng == null ? null : ebsGradeLevelEng.trim();
    }

    public String getEbsGradeLevelKor() {
        return ebsGradeLevelKor;
    }

    public void setEbsGradeLevelKor(String ebsGradeLevelKor) {
        this.ebsGradeLevelKor = ebsGradeLevelKor == null ? null : ebsGradeLevelKor.trim();
    }

    public String getEbsPersonTypeEng() {
        return ebsPersonTypeEng;
    }

    public void setEbsPersonTypeEng(String ebsPersonTypeEng) {
        this.ebsPersonTypeEng = ebsPersonTypeEng == null ? null : ebsPersonTypeEng.trim();
    }

    public String getEbsPersonTypeKor() {
        return ebsPersonTypeKor;
    }

    public void setEbsPersonTypeKor(String ebsPersonTypeKor) {
        this.ebsPersonTypeKor = ebsPersonTypeKor == null ? null : ebsPersonTypeKor.trim();
    }

    public String getEbsUserStatusEng() {
        return ebsUserStatusEng;
    }

    public void setEbsUserStatusEng(String ebsUserStatusEng) {
        this.ebsUserStatusEng = ebsUserStatusEng == null ? null : ebsUserStatusEng.trim();
    }

    public String getEbsUserStatusKor() {
        return ebsUserStatusKor;
    }

    public void setEbsUserStatusKor(String ebsUserStatusKor) {
        this.ebsUserStatusKor = ebsUserStatusKor == null ? null : ebsUserStatusKor.trim();
    }

    public String getPositionEng() {
        return positionEng;
    }

    public void setPositionEng(String positionEng) {
        this.positionEng = positionEng == null ? null : positionEng.trim();
    }

    public String getPositionKor() {
        return positionKor;
    }

    public void setPositionKor(String positionKor) {
        this.positionKor = positionKor == null ? null : positionKor.trim();
    }

    public String getStuStatusEng() {
        return stuStatusEng;
    }

    public void setStuStatusEng(String stuStatusEng) {
        this.stuStatusEng = stuStatusEng == null ? null : stuStatusEng.trim();
    }

    public String getStuStatusKor() {
        return stuStatusKor;
    }

    public void setStuStatusKor(String stuStatusKor) {
        this.stuStatusKor = stuStatusKor == null ? null : stuStatusKor.trim();
    }

    public String getAcadProgCode() {
        return acadProgCode;
    }

    public void setAcadProgCode(String acadProgCode) {
        this.acadProgCode = acadProgCode == null ? null : acadProgCode.trim();
    }

    public String getAcadProgKor() {
        return acadProgKor;
    }

    public void setAcadProgKor(String acadProgKor) {
        this.acadProgKor = acadProgKor == null ? null : acadProgKor.trim();
    }

    public String getAcadProgEng() {
        return acadProgEng;
    }

    public void setAcadProgEng(String acadProgEng) {
        this.acadProgEng = acadProgEng == null ? null : acadProgEng.trim();
    }

    public String getPersonTypeCodeUid() {
        return personTypeCodeUid;
    }

    public void setPersonTypeCodeUid(String personTypeCodeUid) {
        this.personTypeCodeUid = personTypeCodeUid == null ? null : personTypeCodeUid.trim();
    }

    public Date getProgEffdt() {
        return progEffdt;
    }

    public void setProgEffdt(Date progEffdt) {
        this.progEffdt = progEffdt;
    }

    public String getStdntTypeId() {
        return stdntTypeId;
    }

    public void setStdntTypeId(String stdntTypeId) {
        this.stdntTypeId = stdntTypeId == null ? null : stdntTypeId.trim();
    }

    public String getStdntTypeClass() {
        return stdntTypeClass;
    }

    public void setStdntTypeClass(String stdntTypeClass) {
        this.stdntTypeClass = stdntTypeClass == null ? null : stdntTypeClass.trim();
    }

    public String getStdntCategoryId() {
        return stdntCategoryId;
    }

    public void setStdntCategoryId(String stdntCategoryId) {
        this.stdntCategoryId = stdntCategoryId == null ? null : stdntCategoryId.trim();
    }

    public String getAdvrEbsPersonId() {
        return advrEbsPersonId;
    }

    public void setAdvrEbsPersonId(String advrEbsPersonId) {
        this.advrEbsPersonId = advrEbsPersonId == null ? null : advrEbsPersonId.trim();
    }

    public String getAdvrName() {
        return advrName;
    }

    public void setAdvrName(String advrName) {
        this.advrName = advrName == null ? null : advrName.trim();
    }

    public String getAdvrNameAc() {
        return advrNameAc;
    }

    public void setAdvrNameAc(String advrNameAc) {
        this.advrNameAc = advrNameAc == null ? null : advrNameAc.trim();
    }

    public Date getEntranceDate() {
        return entranceDate;
    }

    public void setEntranceDate(Date entranceDate) {
        this.entranceDate = entranceDate;
    }

    public Date getResignDate() {
        return resignDate;
    }

    public void setResignDate(Date resignDate) {
        this.resignDate = resignDate;
    }

    public Date getProgStartDate() {
        return progStartDate;
    }

    public void setProgStartDate(Date progStartDate) {
        this.progStartDate = progStartDate;
    }

    public Date getProgEndDate() {
        return progEndDate;
    }

    public void setProgEndDate(Date progEndDate) {
        this.progEndDate = progEndDate;
    }

    public String getKaistSuid() {
        return kaistSuid;
    }

    public void setKaistSuid(String kaistSuid) {
        this.kaistSuid = kaistSuid == null ? null : kaistSuid.trim();
    }

    public String getAdvrKaistUid() {
        return advrKaistUid;
    }

    public void setAdvrKaistUid(String advrKaistUid) {
        this.advrKaistUid = advrKaistUid == null ? null : advrKaistUid.trim();
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType == null ? null : actionType.trim();
    }

    public String getActionStatus() {
        return actionStatus;
    }

    public void setActionStatus(String actionStatus) {
        this.actionStatus = actionStatus == null ? null : actionStatus.trim();
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId == null ? null : creatorId.trim();
    }

    public String getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(String updatorId) {
        this.updatorId = updatorId == null ? null : updatorId.trim();
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getAppliedAt() {
        return appliedAt;
    }

    public void setAppliedAt(Timestamp appliedAt) {
        this.appliedAt = appliedAt;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage == null ? null : errorMessage.trim();
    }

    public long getSeq() {
        return seq;
    }

    public void setSeq(long seq) {
        this.seq = seq;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", oid=").append(oid);
        sb.append(", kaistUid=").append(kaistUid);
        sb.append(", id=").append(id);
        sb.append(", koreanName=").append(koreanName);
        sb.append(", englishName=").append(englishName);
        sb.append(", lastName=").append(lastName);
        sb.append(", firstName=").append(firstName);
        sb.append(", birthday=").append(birthday);
        sb.append(", nationCodeUid=").append(nationCodeUid);
        sb.append(", sexCodeUid=").append(sexCodeUid);
        sb.append(", emailAddress=").append(emailAddress);
        sb.append(", chMail=").append(chMail);
        sb.append(", mobileTelephoneNumber=").append(mobileTelephoneNumber);
        sb.append(", officeTelephoneNumber=").append(officeTelephoneNumber);
        sb.append(", oweHomeTelephoneNumber=").append(oweHomeTelephoneNumber);
        sb.append(", faxTelephoneNumber=").append(faxTelephoneNumber);
        sb.append(", postNumber=").append(postNumber);
        sb.append(", address=").append(address);
        sb.append(", addressDetail=").append(addressDetail);
        sb.append(", personId=").append(personId);
        sb.append(", employeeNumber=").append(employeeNumber);
        sb.append(", stdNo=").append(stdNo);
        sb.append(", acadOrg=").append(acadOrg);
        sb.append(", acadName=").append(acadName);
        sb.append(", acadKstOrgId=").append(acadKstOrgId);
        sb.append(", acadEbsOrgId=").append(acadEbsOrgId);
        sb.append(", acadEbsOrgNameEng=").append(acadEbsOrgNameEng);
        sb.append(", acadEbsOrgNameKor=").append(acadEbsOrgNameKor);
        sb.append(", campusUid=").append(campusUid);
        sb.append(", ebsOrganizationId=").append(ebsOrganizationId);
        sb.append(", ebsOrgNameEng=").append(ebsOrgNameEng);
        sb.append(", ebsOrgNameKor=").append(ebsOrgNameKor);
        sb.append(", ebsGradeNameEng=").append(ebsGradeNameEng);
        sb.append(", ebsGradeNameKor=").append(ebsGradeNameKor);
        sb.append(", ebsGradeLevelEng=").append(ebsGradeLevelEng);
        sb.append(", ebsGradeLevelKor=").append(ebsGradeLevelKor);
        sb.append(", ebsPersonTypeEng=").append(ebsPersonTypeEng);
        sb.append(", ebsPersonTypeKor=").append(ebsPersonTypeKor);
        sb.append(", ebsUserStatusEng=").append(ebsUserStatusEng);
        sb.append(", ebsUserStatusKor=").append(ebsUserStatusKor);
        sb.append(", positionEng=").append(positionEng);
        sb.append(", positionKor=").append(positionKor);
        sb.append(", stuStatusEng=").append(stuStatusEng);
        sb.append(", stuStatusKor=").append(stuStatusKor);
        sb.append(", acadProgCode=").append(acadProgCode);
        sb.append(", acadProgKor=").append(acadProgKor);
        sb.append(", acadProgEng=").append(acadProgEng);
        sb.append(", personTypeCodeUid=").append(personTypeCodeUid);
        sb.append(", progEffdt=").append(progEffdt);
        sb.append(", stdntTypeId=").append(stdntTypeId);
        sb.append(", stdntTypeClass=").append(stdntTypeClass);
        sb.append(", stdntCategoryId=").append(stdntCategoryId);
        sb.append(", advrEbsPersonId=").append(advrEbsPersonId);
        sb.append(", advrName=").append(advrName);
        sb.append(", advrNameAc=").append(advrNameAc);
        sb.append(", entranceDate=").append(entranceDate);
        sb.append(", resignDate=").append(resignDate);
        sb.append(", progStartDate=").append(progStartDate);
        sb.append(", progEndDate=").append(progEndDate);
        sb.append(", kaistSuid=").append(kaistSuid);
        sb.append(", advrKaistUid=").append(advrKaistUid);
        sb.append(", actionType=").append(actionType);
        sb.append(", actionStatus=").append(actionStatus);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", creatorId=").append(creatorId);
        sb.append(", updatorId=").append(updatorId);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", appliedAt=").append(appliedAt);
        sb.append(", errorMessage=").append(errorMessage);
        sb.append(", seq=").append(seq);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

 // ===================== End of Code Gen =====================
    private Timestamp[] createdAtBetween;

    private Timestamp[] appliedAtBetween;

    private List<String> multipleActionType;

    public Timestamp[] getAppliedAtBetween() {
        return appliedAtBetween;
    }

    public void setAppliedAtBetween(Timestamp[] appliedAtBetween) {
        this.appliedAtBetween = appliedAtBetween;
    }

    public Timestamp[] getCreatedAtBetween() {
        return createdAtBetween;
    }

    public void setCreatedAtBetween(Timestamp[] createdAtBetween) {
        this.createdAtBetween = createdAtBetween;
    }

    public List<String> getMultipleActionType() {
        return multipleActionType;
    }

    public void setMultipleActionType(List<String> multipleActionType) {
        this.multipleActionType = multipleActionType;
    }

}