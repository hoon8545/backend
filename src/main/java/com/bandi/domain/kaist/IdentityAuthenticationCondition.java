package com.bandi.domain.kaist;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class IdentityAuthenticationCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public IdentityAuthenticationCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andServiceTypeIsNull() {
            addCriterion("SERVICETYPE is null");
            return (Criteria) this;
        }

        public Criteria andServiceTypeIsNotNull() {
            addCriterion("SERVICETYPE is not null");
            return (Criteria) this;
        }

        public Criteria andServiceTypeEqualTo(String value) {
            addCriterion("SERVICETYPE =", value, "serviceType");
            return (Criteria) this;
        }

        public Criteria andServiceTypeNotEqualTo(String value) {
            addCriterion("SERVICETYPE <>", value, "serviceType");
            return (Criteria) this;
        }

        public Criteria andServiceTypeGreaterThan(String value) {
            addCriterion("SERVICETYPE >", value, "serviceType");
            return (Criteria) this;
        }

        public Criteria andServiceTypeGreaterThanOrEqualTo(String value) {
            addCriterion("SERVICETYPE >=", value, "serviceType");
            return (Criteria) this;
        }

        public Criteria andServiceTypeLessThan(String value) {
            addCriterion("SERVICETYPE <", value, "serviceType");
            return (Criteria) this;
        }

        public Criteria andServiceTypeLessThanOrEqualTo(String value) {
            addCriterion("SERVICETYPE <=", value, "serviceType");
            return (Criteria) this;
        }

        public Criteria andServiceTypeLike(String value) {
            addCriterion("SERVICETYPE like", value, "serviceType");
            return (Criteria) this;
        }

        public Criteria andServiceTypeNotLike(String value) {
            addCriterion("SERVICETYPE not like", value, "serviceType");
            return (Criteria) this;
        }

        public Criteria andServiceTypeIn(List<String> values) {
            addCriterion("SERVICETYPE in", values, "serviceType");
            return (Criteria) this;
        }

        public Criteria andServiceTypeNotIn(List<String> values) {
            addCriterion("SERVICETYPE not in", values, "serviceType");
            return (Criteria) this;
        }

        public Criteria andServiceTypeBetween(String value1, String value2) {
            addCriterion("SERVICETYPE between", value1, value2, "serviceType");
            return (Criteria) this;
        }

        public Criteria andServiceTypeNotBetween(String value1, String value2) {
            addCriterion("SERVICETYPE not between", value1, value2, "serviceType");
            return (Criteria) this;
        }

        public Criteria andPiAuthenticationServiceIsNull() {
            addCriterion("PERSONAL_INFORMATION_AUTHENTICATION_SERVICE is null");
            return (Criteria) this;
        }

        public Criteria andPiAuthenticationServiceIsNotNull() {
            addCriterion("PERSONAL_INFORMATION_AUTHENTICATION_SERVICE is not null");
            return (Criteria) this;
        }

        public Criteria andPiAuthenticationServiceEqualTo(String value) {
            addCriterion("PERSONAL_INFORMATION_AUTHENTICATION_SERVICE =", value, "piAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andPiAuthenticationServiceNotEqualTo(String value) {
            addCriterion("PERSONAL_INFORMATION_AUTHENTICATION_SERVICE <>", value, "piAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andPiAuthenticationServiceGreaterThan(String value) {
            addCriterion("PERSONAL_INFORMATION_AUTHENTICATION_SERVICE >", value, "piAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andPiAuthenticationServiceGreaterThanOrEqualTo(String value) {
            addCriterion("PERSONAL_INFORMATION_AUTHENTICATION_SERVICE >=", value, "piAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andPiAuthenticationServiceLessThan(String value) {
            addCriterion("PERSONAL_INFORMATION_AUTHENTICATION_SERVICE <", value, "piAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andPiAuthenticationServiceLessThanOrEqualTo(String value) {
            addCriterion("PERSONAL_INFORMATION_AUTHENTICATION_SERVICE <=", value, "piAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andPiAuthenticationServiceLike(String value) {
            addCriterion("PERSONAL_INFORMATION_AUTHENTICATION_SERVICE like", value, "piAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andPiAuthenticationServiceNotLike(String value) {
            addCriterion("PERSONAL_INFORMATION_AUTHENTICATION_SERVICE not like", value, "piAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andPiAuthenticationServiceIn(List<String> values) {
            addCriterion("PERSONAL_INFORMATION_AUTHENTICATION_SERVICE in", values, "piAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andPiAuthenticationServiceNotIn(List<String> values) {
            addCriterion("PERSONAL_INFORMATION_AUTHENTICATION_SERVICE not in", values, "piAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andPiAuthenticationServiceBetween(String value1, String value2) {
            addCriterion("PERSONAL_INFORMATION_AUTHENTICATION_SERVICE between", value1, value2, "piAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andPiAuthenticationServiceNotBetween(String value1, String value2) {
            addCriterion("PERSONAL_INFORMATION_AUTHENTICATION_SERVICE not between", value1, value2, "piAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andMpAuthenticationServiceIsNull() {
            addCriterion("MOBILE_PHONE_AUTHENTICATION_SERVICE is null");
            return (Criteria) this;
        }

        public Criteria andMpAuthenticationServiceIsNotNull() {
            addCriterion("MOBILE_PHONE_AUTHENTICATION_SERVICE is not null");
            return (Criteria) this;
        }

        public Criteria andMpAuthenticationServiceEqualTo(String value) {
            addCriterion("MOBILE_PHONE_AUTHENTICATION_SERVICE =", value, "mpAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andMpAuthenticationServiceNotEqualTo(String value) {
            addCriterion("MOBILE_PHONE_AUTHENTICATION_SERVICE <>", value, "mpAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andMpAuthenticationServiceGreaterThan(String value) {
            addCriterion("MOBILE_PHONE_AUTHENTICATION_SERVICE >", value, "mpAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andMpAuthenticationServiceGreaterThanOrEqualTo(String value) {
            addCriterion("MOBILE_PHONE_AUTHENTICATION_SERVICE >=", value, "mpAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andMpAuthenticationServiceLessThan(String value) {
            addCriterion("MOBILE_PHONE_AUTHENTICATION_SERVICE <", value, "mpAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andMpAuthenticationServiceLessThanOrEqualTo(String value) {
            addCriterion("MOBILE_PHONE_AUTHENTICATION_SERVICE <=", value, "mpAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andMpAuthenticationServiceLike(String value) {
            addCriterion("MOBILE_PHONE_AUTHENTICATION_SERVICE like", value, "mpAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andMpAuthenticationServiceNotLike(String value) {
            addCriterion("MOBILE_PHONE_AUTHENTICATION_SERVICE not like", value, "mpAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andMpAuthenticationServiceIn(List<String> values) {
            addCriterion("MOBILE_PHONE_AUTHENTICATION_SERVICE in", values, "mpAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andMpAuthenticationServiceNotIn(List<String> values) {
            addCriterion("MOBILE_PHONE_AUTHENTICATION_SERVICE not in", values, "mpAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andMpAuthenticationServiceBetween(String value1, String value2) {
            addCriterion("MOBILE_PHONE_AUTHENTICATION_SERVICE between", value1, value2, "mpAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andMpAuthenticationServiceNotBetween(String value1, String value2) {
            addCriterion("MOBILE_PHONE_AUTHENTICATION_SERVICE not between", value1, value2, "mpAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andMobilePhoneIsNull() {
            addCriterion("MOBILE_PHONE is null");
            return (Criteria) this;
        }

        public Criteria andMobilePhoneIsNotNull() {
            addCriterion("MOBILE_PHONE is not null");
            return (Criteria) this;
        }

        public Criteria andMobilePhoneEqualTo(String value) {
            addCriterion("MOBILE_PHONE =", value, "mobilePhone");
            return (Criteria) this;
        }

        public Criteria andMobilePhoneNotEqualTo(String value) {
            addCriterion("MOBILE_PHONE <>", value, "mobilePhone");
            return (Criteria) this;
        }

        public Criteria andMobilePhoneGreaterThan(String value) {
            addCriterion("MOBILE_PHONE >", value, "mobilePhone");
            return (Criteria) this;
        }

        public Criteria andMobilePhoneGreaterThanOrEqualTo(String value) {
            addCriterion("MOBILE_PHONE >=", value, "mobilePhone");
            return (Criteria) this;
        }

        public Criteria andMobilePhoneLessThan(String value) {
            addCriterion("MOBILE_PHONE <", value, "mobilePhone");
            return (Criteria) this;
        }

        public Criteria andMobilePhoneLessThanOrEqualTo(String value) {
            addCriterion("MOBILE_PHONE <=", value, "mobilePhone");
            return (Criteria) this;
        }

        public Criteria andMobilePhoneLike(String value) {
            addCriterion("MOBILE_PHONE like", value, "mobilePhone");
            return (Criteria) this;
        }

        public Criteria andMobilePhoneNotLike(String value) {
            addCriterion("MOBILE_PHONE not like", value, "mobilePhone");
            return (Criteria) this;
        }

        public Criteria andMobilePhoneIn(List<String> values) {
            addCriterion("MOBILE_PHONE in", values, "mobilePhone");
            return (Criteria) this;
        }

        public Criteria andMobilePhoneNotIn(List<String> values) {
            addCriterion("MOBILE_PHONE not in", values, "mobilePhone");
            return (Criteria) this;
        }

        public Criteria andMobilePhoneBetween(String value1, String value2) {
            addCriterion("MOBILE_PHONE between", value1, value2, "mobilePhone");
            return (Criteria) this;
        }

        public Criteria andMobilePhoneNotBetween(String value1, String value2) {
            addCriterion("MOBILE_PHONE not between", value1, value2, "mobilePhone");
            return (Criteria) this;
        }

        public Criteria andMailIsNull() {
            addCriterion("MAIL is null");
            return (Criteria) this;
        }

        public Criteria andMailIsNotNull() {
            addCriterion("MAIL is not null");
            return (Criteria) this;
        }

        public Criteria andMailEqualTo(String value) {
            addCriterion("MAIL =", value, "mail");
            return (Criteria) this;
        }

        public Criteria andMailNotEqualTo(String value) {
            addCriterion("MAIL <>", value, "mail");
            return (Criteria) this;
        }

        public Criteria andMailGreaterThan(String value) {
            addCriterion("MAIL >", value, "mail");
            return (Criteria) this;
        }

        public Criteria andMailGreaterThanOrEqualTo(String value) {
            addCriterion("MAIL >=", value, "mail");
            return (Criteria) this;
        }

        public Criteria andMailLessThan(String value) {
            addCriterion("MAIL <", value, "mail");
            return (Criteria) this;
        }

        public Criteria andMailLessThanOrEqualTo(String value) {
            addCriterion("MAIL <=", value, "mail");
            return (Criteria) this;
        }

        public Criteria andMailLike(String value) {
            addCriterion("MAIL like", value, "mail");
            return (Criteria) this;
        }

        public Criteria andMailNotLike(String value) {
            addCriterion("MAIL not like", value, "mail");
            return (Criteria) this;
        }

        public Criteria andMailIn(List<String> values) {
            addCriterion("MAIL in", values, "mail");
            return (Criteria) this;
        }

        public Criteria andMailNotIn(List<String> values) {
            addCriterion("MAIL not in", values, "mail");
            return (Criteria) this;
        }

        public Criteria andMailBetween(String value1, String value2) {
            addCriterion("MAIL between", value1, value2, "mail");
            return (Criteria) this;
        }

        public Criteria andMailNotBetween(String value1, String value2) {
            addCriterion("MAIL not between", value1, value2, "mail");
            return (Criteria) this;
        }

        public Criteria andFindUidIsNull() {
            addCriterion("FINDUID is null");
            return (Criteria) this;
        }

        public Criteria andFindUidIsNotNull() {
            addCriterion("FINDUID is not null");
            return (Criteria) this;
        }

        public Criteria andFindUidEqualTo(String value) {
            addCriterion("FINDUID =", value, "findUid");
            return (Criteria) this;
        }

        public Criteria andFindUidNotEqualTo(String value) {
            addCriterion("FINDUID <>", value, "findUid");
            return (Criteria) this;
        }

        public Criteria andFindUidGreaterThan(String value) {
            addCriterion("FINDUID >", value, "findUid");
            return (Criteria) this;
        }

        public Criteria andFindUidGreaterThanOrEqualTo(String value) {
            addCriterion("FINDUID >=", value, "findUid");
            return (Criteria) this;
        }

        public Criteria andFindUidLessThan(String value) {
            addCriterion("FINDUID <", value, "findUid");
            return (Criteria) this;
        }

        public Criteria andFindUidLessThanOrEqualTo(String value) {
            addCriterion("FINDUID <=", value, "findUid");
            return (Criteria) this;
        }

        public Criteria andFindUidLike(String value) {
            addCriterion("FINDUID like", value, "findUid");
            return (Criteria) this;
        }

        public Criteria andFindUidNotLike(String value) {
            addCriterion("FINDUID not like", value, "findUid");
            return (Criteria) this;
        }

        public Criteria andFindUidIn(List<String> values) {
            addCriterion("FINDUID in", values, "findUid");
            return (Criteria) this;
        }

        public Criteria andFindUidNotIn(List<String> values) {
            addCriterion("FINDUID not in", values, "findUid");
            return (Criteria) this;
        }

        public Criteria andFindUidBetween(String value1, String value2) {
            addCriterion("FINDUID between", value1, value2, "findUid");
            return (Criteria) this;
        }

        public Criteria andFindUidNotBetween(String value1, String value2) {
            addCriterion("FINDUID not between", value1, value2, "findUid");
            return (Criteria) this;
        }

        public Criteria andResultScreenIsNull() {
            addCriterion("RESULT_SCREEN is null");
            return (Criteria) this;
        }

        public Criteria andResultScreenIsNotNull() {
            addCriterion("RESULT_SCREEN is not null");
            return (Criteria) this;
        }

        public Criteria andResultScreenEqualTo(String value) {
            addCriterion("RESULT_SCREEN =", value, "resultScreen");
            return (Criteria) this;
        }

        public Criteria andResultScreenNotEqualTo(String value) {
            addCriterion("RESULT_SCREEN <>", value, "resultScreen");
            return (Criteria) this;
        }

        public Criteria andResultScreenGreaterThan(String value) {
            addCriterion("RESULT_SCREEN >", value, "resultScreen");
            return (Criteria) this;
        }

        public Criteria andResultScreenGreaterThanOrEqualTo(String value) {
            addCriterion("RESULT_SCREEN >=", value, "resultScreen");
            return (Criteria) this;
        }

        public Criteria andResultScreenLessThan(String value) {
            addCriterion("RESULT_SCREEN <", value, "resultScreen");
            return (Criteria) this;
        }

        public Criteria andResultScreenLessThanOrEqualTo(String value) {
            addCriterion("RESULT_SCREEN <=", value, "resultScreen");
            return (Criteria) this;
        }

        public Criteria andResultScreenLike(String value) {
            addCriterion("RESULT_SCREEN like", value, "resultScreen");
            return (Criteria) this;
        }

        public Criteria andResultScreenNotLike(String value) {
            addCriterion("RESULT_SCREEN not like", value, "resultScreen");
            return (Criteria) this;
        }

        public Criteria andResultScreenIn(List<String> values) {
            addCriterion("RESULT_SCREEN in", values, "resultScreen");
            return (Criteria) this;
        }

        public Criteria andResultScreenNotIn(List<String> values) {
            addCriterion("RESULT_SCREEN not in", values, "resultScreen");
            return (Criteria) this;
        }

        public Criteria andResultScreenBetween(String value1, String value2) {
            addCriterion("RESULT_SCREEN between", value1, value2, "resultScreen");
            return (Criteria) this;
        }

        public Criteria andResultScreenNotBetween(String value1, String value2) {
            addCriterion("RESULT_SCREEN not between", value1, value2, "resultScreen");
            return (Criteria) this;
        }

        public Criteria andResultMailIsNull() {
            addCriterion("RESULT_MAIL is null");
            return (Criteria) this;
        }

        public Criteria andResultMailIsNotNull() {
            addCriterion("RESULT_MAIL is not null");
            return (Criteria) this;
        }

        public Criteria andResultMailEqualTo(String value) {
            addCriterion("RESULT_MAIL =", value, "resultMail");
            return (Criteria) this;
        }

        public Criteria andResultMailNotEqualTo(String value) {
            addCriterion("RESULT_MAIL <>", value, "resultMail");
            return (Criteria) this;
        }

        public Criteria andResultMailGreaterThan(String value) {
            addCriterion("RESULT_MAIL >", value, "resultMail");
            return (Criteria) this;
        }

        public Criteria andResultMailGreaterThanOrEqualTo(String value) {
            addCriterion("RESULT_MAIL >=", value, "resultMail");
            return (Criteria) this;
        }

        public Criteria andResultMailLessThan(String value) {
            addCriterion("RESULT_MAIL <", value, "resultMail");
            return (Criteria) this;
        }

        public Criteria andResultMailLessThanOrEqualTo(String value) {
            addCriterion("RESULT_MAIL <=", value, "resultMail");
            return (Criteria) this;
        }

        public Criteria andResultMailLike(String value) {
            addCriterion("RESULT_MAIL like", value, "resultMail");
            return (Criteria) this;
        }

        public Criteria andResultMailNotLike(String value) {
            addCriterion("RESULT_MAIL not like", value, "resultMail");
            return (Criteria) this;
        }

        public Criteria andResultMailIn(List<String> values) {
            addCriterion("RESULT_MAIL in", values, "resultMail");
            return (Criteria) this;
        }

        public Criteria andResultMailNotIn(List<String> values) {
            addCriterion("RESULT_MAIL not in", values, "resultMail");
            return (Criteria) this;
        }

        public Criteria andResultMailBetween(String value1, String value2) {
            addCriterion("RESULT_MAIL between", value1, value2, "resultMail");
            return (Criteria) this;
        }

        public Criteria andResultMailNotBetween(String value1, String value2) {
            addCriterion("RESULT_MAIL not between", value1, value2, "resultMail");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNull() {
            addCriterion("CREATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNotNull() {
            addCriterion("CREATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtEqualTo(Timestamp value) {
            addCriterion("CREATEDAT =", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <>", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThan(Timestamp value) {
            addCriterion("CREATEDAT >", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT >=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThan(Timestamp value) {
            addCriterion("CREATEDAT <", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIn(List<Timestamp> values) {
            addCriterion("CREATEDAT in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotIn(List<Timestamp> values) {
            addCriterion("CREATEDAT not in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT not between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNull() {
            addCriterion("CREATORID is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNotNull() {
            addCriterion("CREATORID is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdEqualTo(String value) {
            addCriterion("CREATORID =", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotEqualTo(String value) {
            addCriterion("CREATORID <>", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThan(String value) {
            addCriterion("CREATORID >", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATORID >=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThan(String value) {
            addCriterion("CREATORID <", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThanOrEqualTo(String value) {
            addCriterion("CREATORID <=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLike(String value) {
            addCriterion("CREATORID like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotLike(String value) {
            addCriterion("CREATORID not like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIn(List<String> values) {
            addCriterion("CREATORID in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotIn(List<String> values) {
            addCriterion("CREATORID not in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdBetween(String value1, String value2) {
            addCriterion("CREATORID between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotBetween(String value1, String value2) {
            addCriterion("CREATORID not between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNull() {
            addCriterion("UPDATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNotNull() {
            addCriterion("UPDATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT =", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <>", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThan(Timestamp value) {
            addCriterion("UPDATEDAT >", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT >=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThan(Timestamp value) {
            addCriterion("UPDATEDAT <", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT not in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT not between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNull() {
            addCriterion("UPDATORID is null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNotNull() {
            addCriterion("UPDATORID is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdEqualTo(String value) {
            addCriterion("UPDATORID =", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotEqualTo(String value) {
            addCriterion("UPDATORID <>", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThan(String value) {
            addCriterion("UPDATORID >", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATORID >=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThan(String value) {
            addCriterion("UPDATORID <", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThanOrEqualTo(String value) {
            addCriterion("UPDATORID <=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLike(String value) {
            addCriterion("UPDATORID like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotLike(String value) {
            addCriterion("UPDATORID not like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIn(List<String> values) {
            addCriterion("UPDATORID in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotIn(List<String> values) {
            addCriterion("UPDATORID not in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdBetween(String value1, String value2) {
            addCriterion("UPDATORID between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotBetween(String value1, String value2) {
            addCriterion("UPDATORID not between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andServiceTypeLikeInsensitive(String value) {
            addCriterion("upper(SERVICETYPE) like", value.toUpperCase(), "serviceType");
            return (Criteria) this;
        }

        public Criteria andPiAuthenticationServiceLikeInsensitive(String value) {
            addCriterion("upper(PERSONAL_INFORMATION_AUTHENTICATION_SERVICE) like", value.toUpperCase(), "piAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andMpAuthenticationServiceLikeInsensitive(String value) {
            addCriterion("upper(MOBILE_PHONE_AUTHENTICATION_SERVICE) like", value.toUpperCase(), "mpAuthenticationService");
            return (Criteria) this;
        }

        public Criteria andMobilePhoneLikeInsensitive(String value) {
            addCriterion("upper(MOBILE_PHONE) like", value.toUpperCase(), "mobilePhone");
            return (Criteria) this;
        }

        public Criteria andMailLikeInsensitive(String value) {
            addCriterion("upper(MAIL) like", value.toUpperCase(), "mail");
            return (Criteria) this;
        }

        public Criteria andFindUidLikeInsensitive(String value) {
            addCriterion("upper(FINDUID) like", value.toUpperCase(), "findUid");
            return (Criteria) this;
        }

        public Criteria andResultScreenLikeInsensitive(String value) {
            addCriterion("upper(RESULT_SCREEN) like", value.toUpperCase(), "resultScreen");
            return (Criteria) this;
        }

        public Criteria andResultMailLikeInsensitive(String value) {
            addCriterion("upper(RESULT_MAIL) like", value.toUpperCase(), "resultMail");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLikeInsensitive(String value) {
            addCriterion("upper(CREATORID) like", value.toUpperCase(), "creatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLikeInsensitive(String value) {
            addCriterion("upper(UPDATORID) like", value.toUpperCase(), "updatorId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}