package com.bandi.domain.kaist;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class PasswordChangeHistoryCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PasswordChangeHistoryCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOidIsNull() {
            addCriterion("OID is null");
            return (Criteria) this;
        }

        public Criteria andOidIsNotNull() {
            addCriterion("OID is not null");
            return (Criteria) this;
        }

        public Criteria andOidEqualTo(String value) {
            addCriterion("OID =", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotEqualTo(String value) {
            addCriterion("OID <>", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThan(String value) {
            addCriterion("OID >", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThanOrEqualTo(String value) {
            addCriterion("OID >=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThan(String value) {
            addCriterion("OID <", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThanOrEqualTo(String value) {
            addCriterion("OID <=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLike(String value) {
            addCriterion("OID like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotLike(String value) {
            addCriterion("OID not like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidIn(List<String> values) {
            addCriterion("OID in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotIn(List<String> values) {
            addCriterion("OID not in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidBetween(String value1, String value2) {
            addCriterion("OID between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotBetween(String value1, String value2) {
            addCriterion("OID not between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("USERID is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("USERID is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("USERID =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("USERID <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("USERID >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("USERID >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("USERID <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("USERID <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("USERID like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("USERID not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("USERID in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("USERID not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("USERID between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("USERID not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andChangedAtIsNull() {
            addCriterion("CHANGEDAT is null");
            return (Criteria) this;
        }

        public Criteria andChangedAtIsNotNull() {
            addCriterion("CHANGEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andChangedAtEqualTo(Timestamp value) {
            addCriterion("CHANGEDAT =", value, "changedAt");
            return (Criteria) this;
        }

        public Criteria andChangedAtNotEqualTo(Timestamp value) {
            addCriterion("CHANGEDAT <>", value, "changedAt");
            return (Criteria) this;
        }

        public Criteria andChangedAtGreaterThan(Timestamp value) {
            addCriterion("CHANGEDAT >", value, "changedAt");
            return (Criteria) this;
        }

        public Criteria andChangedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("CHANGEDAT >=", value, "changedAt");
            return (Criteria) this;
        }

        public Criteria andChangedAtLessThan(Timestamp value) {
            addCriterion("CHANGEDAT <", value, "changedAt");
            return (Criteria) this;
        }

        public Criteria andChangedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("CHANGEDAT <=", value, "changedAt");
            return (Criteria) this;
        }

        public Criteria andChangedAtIn(List<Timestamp> values) {
            addCriterion("CHANGEDAT in", values, "changedAt");
            return (Criteria) this;
        }

        public Criteria andChangedAtNotIn(List<Timestamp> values) {
            addCriterion("CHANGEDAT not in", values, "changedAt");
            return (Criteria) this;
        }

        public Criteria andChangedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CHANGEDAT between", value1, value2, "changedAt");
            return (Criteria) this;
        }

        public Criteria andChangedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CHANGEDAT not between", value1, value2, "changedAt");
            return (Criteria) this;
        }

        public Criteria andLoginIpIsNull() {
            addCriterion("LOGINIP is null");
            return (Criteria) this;
        }

        public Criteria andLoginIpIsNotNull() {
            addCriterion("LOGINIP is not null");
            return (Criteria) this;
        }

        public Criteria andLoginIpEqualTo(String value) {
            addCriterion("LOGINIP =", value, "loginIp");
            return (Criteria) this;
        }

        public Criteria andLoginIpNotEqualTo(String value) {
            addCriterion("LOGINIP <>", value, "loginIp");
            return (Criteria) this;
        }

        public Criteria andLoginIpGreaterThan(String value) {
            addCriterion("LOGINIP >", value, "loginIp");
            return (Criteria) this;
        }

        public Criteria andLoginIpGreaterThanOrEqualTo(String value) {
            addCriterion("LOGINIP >=", value, "loginIp");
            return (Criteria) this;
        }

        public Criteria andLoginIpLessThan(String value) {
            addCriterion("LOGINIP <", value, "loginIp");
            return (Criteria) this;
        }

        public Criteria andLoginIpLessThanOrEqualTo(String value) {
            addCriterion("LOGINIP <=", value, "loginIp");
            return (Criteria) this;
        }

        public Criteria andLoginIpLike(String value) {
            addCriterion("LOGINIP like", value, "loginIp");
            return (Criteria) this;
        }

        public Criteria andLoginIpNotLike(String value) {
            addCriterion("LOGINIP not like", value, "loginIp");
            return (Criteria) this;
        }

        public Criteria andLoginIpIn(List<String> values) {
            addCriterion("LOGINIP in", values, "loginIp");
            return (Criteria) this;
        }

        public Criteria andLoginIpNotIn(List<String> values) {
            addCriterion("LOGINIP not in", values, "loginIp");
            return (Criteria) this;
        }

        public Criteria andLoginIpBetween(String value1, String value2) {
            addCriterion("LOGINIP between", value1, value2, "loginIp");
            return (Criteria) this;
        }

        public Criteria andLoginIpNotBetween(String value1, String value2) {
            addCriterion("LOGINIP not between", value1, value2, "loginIp");
            return (Criteria) this;
        }

        public Criteria andIamServerIdIsNull() {
            addCriterion("IAMSERVERID is null");
            return (Criteria) this;
        }

        public Criteria andIamServerIdIsNotNull() {
            addCriterion("IAMSERVERID is not null");
            return (Criteria) this;
        }

        public Criteria andIamServerIdEqualTo(String value) {
            addCriterion("IAMSERVERID =", value, "iamServerId");
            return (Criteria) this;
        }

        public Criteria andIamServerIdNotEqualTo(String value) {
            addCriterion("IAMSERVERID <>", value, "iamServerId");
            return (Criteria) this;
        }

        public Criteria andIamServerIdGreaterThan(String value) {
            addCriterion("IAMSERVERID >", value, "iamServerId");
            return (Criteria) this;
        }

        public Criteria andIamServerIdGreaterThanOrEqualTo(String value) {
            addCriterion("IAMSERVERID >=", value, "iamServerId");
            return (Criteria) this;
        }

        public Criteria andIamServerIdLessThan(String value) {
            addCriterion("IAMSERVERID <", value, "iamServerId");
            return (Criteria) this;
        }

        public Criteria andIamServerIdLessThanOrEqualTo(String value) {
            addCriterion("IAMSERVERID <=", value, "iamServerId");
            return (Criteria) this;
        }

        public Criteria andIamServerIdLike(String value) {
            addCriterion("IAMSERVERID like", value, "iamServerId");
            return (Criteria) this;
        }

        public Criteria andIamServerIdNotLike(String value) {
            addCriterion("IAMSERVERID not like", value, "iamServerId");
            return (Criteria) this;
        }

        public Criteria andIamServerIdIn(List<String> values) {
            addCriterion("IAMSERVERID in", values, "iamServerId");
            return (Criteria) this;
        }

        public Criteria andIamServerIdNotIn(List<String> values) {
            addCriterion("IAMSERVERID not in", values, "iamServerId");
            return (Criteria) this;
        }

        public Criteria andIamServerIdBetween(String value1, String value2) {
            addCriterion("IAMSERVERID between", value1, value2, "iamServerId");
            return (Criteria) this;
        }

        public Criteria andIamServerIdNotBetween(String value1, String value2) {
            addCriterion("IAMSERVERID not between", value1, value2, "iamServerId");
            return (Criteria) this;
        }

        public Criteria andFlagSuccessIsNull() {
            addCriterion("FLAGSUCCESS is null");
            return (Criteria) this;
        }

        public Criteria andFlagSuccessIsNotNull() {
            addCriterion("FLAGSUCCESS is not null");
            return (Criteria) this;
        }

        public Criteria andFlagSuccessEqualTo(String value) {
            addCriterion("FLAGSUCCESS =", value, "flagSuccess");
            return (Criteria) this;
        }

        public Criteria andFlagSuccessNotEqualTo(String value) {
            addCriterion("FLAGSUCCESS <>", value, "flagSuccess");
            return (Criteria) this;
        }

        public Criteria andFlagSuccessGreaterThan(String value) {
            addCriterion("FLAGSUCCESS >", value, "flagSuccess");
            return (Criteria) this;
        }

        public Criteria andFlagSuccessGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGSUCCESS >=", value, "flagSuccess");
            return (Criteria) this;
        }

        public Criteria andFlagSuccessLessThan(String value) {
            addCriterion("FLAGSUCCESS <", value, "flagSuccess");
            return (Criteria) this;
        }

        public Criteria andFlagSuccessLessThanOrEqualTo(String value) {
            addCriterion("FLAGSUCCESS <=", value, "flagSuccess");
            return (Criteria) this;
        }

        public Criteria andFlagSuccessLike(String value) {
            addCriterion("FLAGSUCCESS like", value, "flagSuccess");
            return (Criteria) this;
        }

        public Criteria andFlagSuccessNotLike(String value) {
            addCriterion("FLAGSUCCESS not like", value, "flagSuccess");
            return (Criteria) this;
        }

        public Criteria andFlagSuccessIn(List<String> values) {
            addCriterion("FLAGSUCCESS in", values, "flagSuccess");
            return (Criteria) this;
        }

        public Criteria andFlagSuccessNotIn(List<String> values) {
            addCriterion("FLAGSUCCESS not in", values, "flagSuccess");
            return (Criteria) this;
        }

        public Criteria andFlagSuccessBetween(String value1, String value2) {
            addCriterion("FLAGSUCCESS between", value1, value2, "flagSuccess");
            return (Criteria) this;
        }

        public Criteria andFlagSuccessNotBetween(String value1, String value2) {
            addCriterion("FLAGSUCCESS not between", value1, value2, "flagSuccess");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNull() {
            addCriterion("CREATORID is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNotNull() {
            addCriterion("CREATORID is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdEqualTo(String value) {
            addCriterion("CREATORID =", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotEqualTo(String value) {
            addCriterion("CREATORID <>", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThan(String value) {
            addCriterion("CREATORID >", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATORID >=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThan(String value) {
            addCriterion("CREATORID <", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThanOrEqualTo(String value) {
            addCriterion("CREATORID <=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLike(String value) {
            addCriterion("CREATORID like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotLike(String value) {
            addCriterion("CREATORID not like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIn(List<String> values) {
            addCriterion("CREATORID in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotIn(List<String> values) {
            addCriterion("CREATORID not in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdBetween(String value1, String value2) {
            addCriterion("CREATORID between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotBetween(String value1, String value2) {
            addCriterion("CREATORID not between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNull() {
            addCriterion("CREATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNotNull() {
            addCriterion("CREATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtEqualTo(Timestamp value) {
            addCriterion("CREATEDAT =", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <>", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThan(Timestamp value) {
            addCriterion("CREATEDAT >", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT >=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThan(Timestamp value) {
            addCriterion("CREATEDAT <", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIn(List<Timestamp> values) {
            addCriterion("CREATEDAT in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotIn(List<Timestamp> values) {
            addCriterion("CREATEDAT not in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT not between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNull() {
            addCriterion("UPDATORID is null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNotNull() {
            addCriterion("UPDATORID is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdEqualTo(String value) {
            addCriterion("UPDATORID =", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotEqualTo(String value) {
            addCriterion("UPDATORID <>", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThan(String value) {
            addCriterion("UPDATORID >", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATORID >=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThan(String value) {
            addCriterion("UPDATORID <", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThanOrEqualTo(String value) {
            addCriterion("UPDATORID <=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLike(String value) {
            addCriterion("UPDATORID like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotLike(String value) {
            addCriterion("UPDATORID not like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIn(List<String> values) {
            addCriterion("UPDATORID in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotIn(List<String> values) {
            addCriterion("UPDATORID not in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdBetween(String value1, String value2) {
            addCriterion("UPDATORID between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotBetween(String value1, String value2) {
            addCriterion("UPDATORID not between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNull() {
            addCriterion("UPDATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNotNull() {
            addCriterion("UPDATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT =", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <>", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThan(Timestamp value) {
            addCriterion("UPDATEDAT >", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT >=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThan(Timestamp value) {
            addCriterion("UPDATEDAT <", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT not in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT not between", value1, value2, "updatedAt");
            return (Criteria) this;
        }
        
        public Criteria andChangedPasswordIsNull() {
            addCriterion("CHANGEDPASSWORD is null");
            return (Criteria) this;
        }

        public Criteria andChangedPasswordIsNotNull() {
            addCriterion("CHANGEDPASSWORD is not null");
            return (Criteria) this;
        }

        public Criteria andChangedPasswordEqualTo(String value) {
            addCriterion("CHANGEDPASSWORD =", value, "changedPassword");
            return (Criteria) this;
        }

        public Criteria andChangedPasswordNotEqualTo(String value) {
            addCriterion("CHANGEDPASSWORD <>", value, "changedPassword");
            return (Criteria) this;
        }

        public Criteria andChangedPasswordGreaterThan(String value) {
            addCriterion("CHANGEDPASSWORD >", value, "changedPassword");
            return (Criteria) this;
        }

        public Criteria andChangedPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("CHANGEDPASSWORD >=", value, "changedPassword");
            return (Criteria) this;
        }

        public Criteria andChangedPasswordLessThan(String value) {
            addCriterion("CHANGEDPASSWORD <", value, "changedPassword");
            return (Criteria) this;
        }

        public Criteria andChangedPasswordLessThanOrEqualTo(String value) {
            addCriterion("CHANGEDPASSWORD <=", value, "changedPassword");
            return (Criteria) this;
        }

        public Criteria andChangedPasswordLike(String value) {
            addCriterion("CHANGEDPASSWORD like", value, "changedPassword");
            return (Criteria) this;
        }

        public Criteria andChangedPasswordNotLike(String value) {
            addCriterion("CHANGEDPASSWORD not like", value, "changedPassword");
            return (Criteria) this;
        }

        public Criteria andChangedPasswordIn(List<String> values) {
            addCriterion("CHANGEDPASSWORD in", values, "changedPassword");
            return (Criteria) this;
        }

        public Criteria andChangedPasswordNotIn(List<String> values) {
            addCriterion("CHANGEDPASSWORD not in", values, "changedPassword");
            return (Criteria) this;
        }

        public Criteria andChangedPasswordBetween(String value1, String value2) {
            addCriterion("CHANGEDPASSWORD between", value1, value2, "changedPassword");
            return (Criteria) this;
        }

        public Criteria andChangedPasswordNotBetween(String value1, String value2) {
            addCriterion("CHANGEDPASSWORD not between", value1, value2, "changedPassword");
            return (Criteria) this;
        }

        public Criteria andOidLikeInsensitive(String value) {
            addCriterion("upper(OID) like", value.toUpperCase(), "oid");
            return (Criteria) this;
        }

        public Criteria andUserIdLikeInsensitive(String value) {
            addCriterion("upper(USERID) like", value.toUpperCase(), "userId");
            return (Criteria) this;
        }

        public Criteria andLoginIpLikeInsensitive(String value) {
            addCriterion("upper(LOGINIP) like", value.toUpperCase(), "loginIp");
            return (Criteria) this;
        }

        public Criteria andIamServerIdLikeInsensitive(String value) {
            addCriterion("upper(IAMSERVERID) like", value.toUpperCase(), "iamServerId");
            return (Criteria) this;
        }

        public Criteria andFlagSuccessLikeInsensitive(String value) {
            addCriterion("upper(FLAGSUCCESS) like", value.toUpperCase(), "flagSuccess");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLikeInsensitive(String value) {
            addCriterion("upper(CREATORID) like", value.toUpperCase(), "creatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLikeInsensitive(String value) {
            addCriterion("upper(UPDATORID) like", value.toUpperCase(), "updatorId");
            return (Criteria) this;
        }
        
        public Criteria andChangedPasswordLikeInsensitive(String value) {
            addCriterion("upper(CHANGEDPASSWORD) like", value.toUpperCase(), "changedPassword");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}