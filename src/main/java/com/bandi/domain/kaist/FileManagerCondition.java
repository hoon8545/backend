package com.bandi.domain.kaist;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;



public class FileManagerCondition {
	protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FileManagerCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOidIsNull() {
            addCriterion("OID is null");
            return (Criteria) this;
        }

        public Criteria andOidIsNotNull() {
            addCriterion("OID is not null");
            return (Criteria) this;
        }

        public Criteria andOidEqualTo(String value) {
            addCriterion("OID =", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotEqualTo(String value) {
            addCriterion("OID <>", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThan(String value) {
            addCriterion("OID >", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThanOrEqualTo(String value) {
            addCriterion("OID >=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThan(String value) {
            addCriterion("OID <", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThanOrEqualTo(String value) {
            addCriterion("OID <=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLike(String value) {
            addCriterion("OID like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotLike(String value) {
            addCriterion("OID not like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidIn(List<String> values) {
            addCriterion("OID in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotIn(List<String> values) {
            addCriterion("OID not in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidBetween(String value1, String value2) {
            addCriterion("OID between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotBetween(String value1, String value2) {
            addCriterion("OID not between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andOriginalNameIsNull() {
            addCriterion("ORIGINALNAME is null");
            return (Criteria) this;
        }

        public Criteria andOriginalNameIsNotNull() {
            addCriterion("ORIGINALNAME is not null");
            return (Criteria) this;
        }

        public Criteria andOriginalNameEqualTo(String value) {
            addCriterion("ORIGINALNAME =", value, "originalName");
            return (Criteria) this;
        }

        public Criteria andOriginalNameNotEqualTo(String value) {
            addCriterion("ORIGINALNAME <>", value, "originalName");
            return (Criteria) this;
        }

        public Criteria andOriginalNameGreaterThan(String value) {
            addCriterion("ORIGINALNAME >", value, "originalName");
            return (Criteria) this;
        }

        public Criteria andOriginalNameGreaterThanOrEqualTo(String value) {
            addCriterion("ORIGINALNAME >=", value, "originalName");
            return (Criteria) this;
        }

        public Criteria andOriginalNameLessThan(String value) {
            addCriterion("ORIGINALNAME <", value, "originalName");
            return (Criteria) this;
        }

        public Criteria andOriginalNameLessThanOrEqualTo(String value) {
            addCriterion("ORIGINALNAME <=", value, "originalName");
            return (Criteria) this;
        }

        public Criteria andOriginalNameLike(String value) {
            addCriterion("ORIGINALNAME like", value, "originalName");
            return (Criteria) this;
        }

        public Criteria andOriginalNameNotLike(String value) {
            addCriterion("ORIGINALNAME not like", value, "originalName");
            return (Criteria) this;
        }

        public Criteria andOriginalNameIn(List<String> values) {
            addCriterion("ORIGINALNAME in", values, "originalName");
            return (Criteria) this;
        }

        public Criteria andOriginalNameNotIn(List<String> values) {
            addCriterion("ORIGINALNAME not in", values, "originalName");
            return (Criteria) this;
        }

        public Criteria andOriginalNameBetween(String value1, String value2) {
            addCriterion("ORIGINALNAME between", value1, value2, "originalName");
            return (Criteria) this;
        }

        public Criteria andOriginalNameNotBetween(String value1, String value2) {
            addCriterion("ORIGINALNAME not between", value1, value2, "originalName");
            return (Criteria) this;
        }

        public Criteria andStoredNameIsNull() {
            addCriterion("STOREDNAME is null");
            return (Criteria) this;
        }

        public Criteria andStoredNameIsNotNull() {
            addCriterion("STOREDNAME is not null");
            return (Criteria) this;
        }

        public Criteria andStoredNameEqualTo(String value) {
            addCriterion("STOREDNAME =", value, "storedName");
            return (Criteria) this;
        }

        public Criteria andStoredNameNotEqualTo(String value) {
            addCriterion("STOREDNAME <>", value, "storedName");
            return (Criteria) this;
        }

        public Criteria andStoredNameGreaterThan(String value) {
            addCriterion("STOREDNAME >", value, "storedName");
            return (Criteria) this;
        }

        public Criteria andStoredNameGreaterThanOrEqualTo(String value) {
            addCriterion("STOREDNAME >=", value, "storedName");
            return (Criteria) this;
        }

        public Criteria andStoredNameLessThan(String value) {
            addCriterion("STOREDNAME <", value, "storedName");
            return (Criteria) this;
        }

        public Criteria andStoredNameLessThanOrEqualTo(String value) {
            addCriterion("STOREDNAME <=", value, "storedName");
            return (Criteria) this;
        }

        public Criteria andStoredNameLike(String value) {
            addCriterion("STOREDNAME like", value, "storedName");
            return (Criteria) this;
        }

        public Criteria andStoredNameNotLike(String value) {
            addCriterion("STOREDNAME not like", value, "storedName");
            return (Criteria) this;
        }

        public Criteria andStoredNameIn(List<String> values) {
            addCriterion("STOREDNAME in", values, "storedName");
            return (Criteria) this;
        }

        public Criteria andStoredNameNotIn(List<String> values) {
            addCriterion("STOREDNAME not in", values, "storedName");
            return (Criteria) this;
        }

        public Criteria andStoredNameBetween(String value1, String value2) {
            addCriterion("STOREDNAME between", value1, value2, "storedName");
            return (Criteria) this;
        }

        public Criteria andStoredNameNotBetween(String value1, String value2) {
            addCriterion("STOREDNAME not between", value1, value2, "storedName");
            return (Criteria) this;
        }

        public Criteria andPathIsNull() {
            addCriterion("PATH is null");
            return (Criteria) this;
        }

        public Criteria andPathIsNotNull() {
            addCriterion("PATH is not null");
            return (Criteria) this;
        }

        public Criteria andPathEqualTo(String value) {
            addCriterion("PATH =", value, "path");
            return (Criteria) this;
        }

        public Criteria andPathNotEqualTo(String value) {
            addCriterion("PATH <>", value, "path");
            return (Criteria) this;
        }

        public Criteria andPathGreaterThan(String value) {
            addCriterion("PATH >", value, "path");
            return (Criteria) this;
        }

        public Criteria andPathGreaterThanOrEqualTo(String value) {
            addCriterion("PATH >=", value, "path");
            return (Criteria) this;
        }

        public Criteria andPathLessThan(String value) {
            addCriterion("PATH <", value, "path");
            return (Criteria) this;
        }

        public Criteria andPathLessThanOrEqualTo(String value) {
            addCriterion("PATH <=", value, "path");
            return (Criteria) this;
        }

        public Criteria andPathLike(String value) {
            addCriterion("PATH like", value, "path");
            return (Criteria) this;
        }

        public Criteria andPathNotLike(String value) {
            addCriterion("PATH not like", value, "path");
            return (Criteria) this;
        }

        public Criteria andPathIn(List<String> values) {
            addCriterion("PATH in", values, "path");
            return (Criteria) this;
        }

        public Criteria andPathNotIn(List<String> values) {
            addCriterion("PATH not in", values, "path");
            return (Criteria) this;
        }

        public Criteria andPathBetween(String value1, String value2) {
            addCriterion("PATH between", value1, value2, "path");
            return (Criteria) this;
        }

        public Criteria andPathNotBetween(String value1, String value2) {
            addCriterion("PATH not between", value1, value2, "path");
            return (Criteria) this;
        }

        public Criteria andSizeIsNull() {
            addCriterion("FILESIZE is null");
            return (Criteria) this;
        }

        public Criteria andSizeIsNotNull() {
            addCriterion("FILESIZE is not null");
            return (Criteria) this;
        }

        public Criteria andSizeEqualTo(int value) {
            addCriterion("FILESIZE =", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeNotEqualTo(int value) {
            addCriterion("FILESIZE <>", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeGreaterThan(int value) {
            addCriterion("FILESIZE >", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeGreaterThanOrEqualTo(int value) {
            addCriterion("FILESIZE >=", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeLessThan(int value) {
            addCriterion("FILESIZE <", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeLessThanOrEqualTo(int value) {
            addCriterion("FILESIZE <=", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeIn(List<Integer> values) {
            addCriterion("FILESIZE in", values, "size");
            return (Criteria) this;
        }

        public Criteria andSizeNotIn(List<Integer> values) {
            addCriterion("FILESIZE not in", values, "size");
            return (Criteria) this;
        }

        public Criteria andSizeBetween(int value1, int value2) {
            addCriterion("FILESIZE between", value1, value2, "size");
            return (Criteria) this;
        }

        public Criteria andSizeNotBetween(int value1, int value2) {
            addCriterion("FILESIZE not between", value1, value2, "size");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNull() {
            addCriterion("CREATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNotNull() {
            addCriterion("CREATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtEqualTo(Timestamp value) {
            addCriterion("CREATEDAT =", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <>", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThan(Timestamp value) {
            addCriterion("CREATEDAT >", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT >=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThan(Timestamp value) {
            addCriterion("CREATEDAT <", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIn(List<Timestamp> values) {
            addCriterion("CREATEDAT in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotIn(List<Timestamp> values) {
            addCriterion("CREATEDAT not in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT not between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNull() {
            addCriterion("CREATORID is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNotNull() {
            addCriterion("CREATORID is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdEqualTo(String value) {
            addCriterion("CREATORID =", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotEqualTo(String value) {
            addCriterion("CREATORID <>", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThan(String value) {
            addCriterion("CREATORID >", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATORID >=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThan(String value) {
            addCriterion("CREATORID <", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThanOrEqualTo(String value) {
            addCriterion("CREATORID <=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLike(String value) {
            addCriterion("CREATORID like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotLike(String value) {
            addCriterion("CREATORID not like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIn(List<String> values) {
            addCriterion("CREATORID in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotIn(List<String> values) {
            addCriterion("CREATORID not in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdBetween(String value1, String value2) {
            addCriterion("CREATORID between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotBetween(String value1, String value2) {
            addCriterion("CREATORID not between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andTargetObjectTypeIsNull() {
            addCriterion("TARGETOBJECTTYPE is null");
            return (Criteria) this;
        }

        public Criteria andTargetObjectTypeIsNotNull() {
            addCriterion("TARGETOBJECTTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andTargetObjectTypeEqualTo(String value) {
            addCriterion("TARGETOBJECTTYPE =", value, "targetObjectType");
            return (Criteria) this;
        }

        public Criteria andTargetObjectTypeNotEqualTo(String value) {
            addCriterion("TARGETOBJECTTYPE <>", value, "targetObjectType");
            return (Criteria) this;
        }

        public Criteria andTargetObjectTypeGreaterThan(String value) {
            addCriterion("TARGETOBJECTTYPE >", value, "targetObjectType");
            return (Criteria) this;
        }

        public Criteria andTargetObjectTypeGreaterThanOrEqualTo(String value) {
            addCriterion("TARGETOBJECTTYPE >=", value, "targetObjectType");
            return (Criteria) this;
        }

        public Criteria andTargetObjectTypeLessThan(String value) {
            addCriterion("TARGETOBJECTTYPE <", value, "targetObjectType");
            return (Criteria) this;
        }

        public Criteria andTargetObjectTypeLessThanOrEqualTo(String value) {
            addCriterion("TARGETOBJECTTYPE <=", value, "targetObjectType");
            return (Criteria) this;
        }

        public Criteria andTargetObjectTypeLike(String value) {
            addCriterion("TARGETOBJECTTYPE like", value, "targetObjectType");
            return (Criteria) this;
        }

        public Criteria andTargetObjectTypeNotLike(String value) {
            addCriterion("TARGETOBJECTTYPE not like", value, "targetObjectType");
            return (Criteria) this;
        }

        public Criteria andTargetObjectTypeIn(List<String> values) {
            addCriterion("TARGETOBJECTTYPE in", values, "targetObjectType");
            return (Criteria) this;
        }

        public Criteria andTargetObjectTypeNotIn(List<String> values) {
            addCriterion("TARGETOBJECTTYPE not in", values, "targetObjectType");
            return (Criteria) this;
        }

        public Criteria andTargetObjectTypeBetween(String value1, String value2) {
            addCriterion("TARGETOBJECTTYPE between", value1, value2, "targetObjectType");
            return (Criteria) this;
        }

        public Criteria andTargetObjectTypeNotBetween(String value1, String value2) {
            addCriterion("TARGETOBJECTTYPE not between", value1, value2, "targetObjectType");
            return (Criteria) this;
        }

        public Criteria andTargetObjectOidIsNull() {
            addCriterion("TARGETOBJECTOID is null");
            return (Criteria) this;
        }

        public Criteria andTargetObjectOidIsNotNull() {
            addCriterion("TARGETOBJECTOID is not null");
            return (Criteria) this;
        }

        public Criteria andTargetObjectOidEqualTo(String value) {
            addCriterion("TARGETOBJECTOID =", value, "targetObjectOid");
            return (Criteria) this;
        }

        public Criteria andTargetObjectOidNotEqualTo(String value) {
            addCriterion("TARGETOBJECTOID <>", value, "targetObjectOid");
            return (Criteria) this;
        }

        public Criteria andTargetObjectOidGreaterThan(String value) {
            addCriterion("TARGETOBJECTOID >", value, "targetObjectOid");
            return (Criteria) this;
        }

        public Criteria andTargetObjectOidGreaterThanOrEqualTo(String value) {
            addCriterion("TARGETOBJECTOID >=", value, "targetObjectOid");
            return (Criteria) this;
        }

        public Criteria andTargetObjectOidLessThan(String value) {
            addCriterion("TARGETOBJECTOID <", value, "targetObjectOid");
            return (Criteria) this;
        }

        public Criteria andTargetObjectOidLessThanOrEqualTo(String value) {
            addCriterion("TARGETOBJECTOID <=", value, "targetObjectOid");
            return (Criteria) this;
        }

        public Criteria andTargetObjectOidLike(String value) {
            addCriterion("TARGETOBJECTOID like", value, "targetObjectOid");
            return (Criteria) this;
        }

        public Criteria andTargetObjectOidNotLike(String value) {
            addCriterion("TARGETOBJECTOID not like", value, "targetObjectOid");
            return (Criteria) this;
        }

        public Criteria andTargetObjectOidIn(List<String> values) {
            addCriterion("TARGETOBJECTOID in", values, "targetObjectOid");
            return (Criteria) this;
        }

        public Criteria andTargetObjectOidNotIn(List<String> values) {
            addCriterion("TARGETOBJECTOID not in", values, "targetObjectOid");
            return (Criteria) this;
        }

        public Criteria andTargetObjectOidBetween(String value1, String value2) {
            addCriterion("TARGETOBJECTOID between", value1, value2, "targetObjectOid");
            return (Criteria) this;
        }

        public Criteria andTargetObjectOidNotBetween(String value1, String value2) {
            addCriterion("TARGETOBJECTOID not between", value1, value2, "targetObjectOid");
            return (Criteria) this;
        }
        
        public Criteria andUpdatorIdIsNull() {
            addCriterion("UPDATORID is null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNotNull() {
            addCriterion("UPDATORID is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdEqualTo(String value) {
            addCriterion("UPDATORID =", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotEqualTo(String value) {
            addCriterion("UPDATORID <>", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThan(String value) {
            addCriterion("UPDATORID >", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATORID >=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThan(String value) {
            addCriterion("UPDATORID <", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThanOrEqualTo(String value) {
            addCriterion("UPDATORID <=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLike(String value) {
            addCriterion("UPDATORID like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotLike(String value) {
            addCriterion("UPDATORID not like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIn(List<String> values) {
            addCriterion("UPDATORID in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotIn(List<String> values) {
            addCriterion("UPDATORID not in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdBetween(String value1, String value2) {
            addCriterion("UPDATORID between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotBetween(String value1, String value2) {
            addCriterion("UPDATORID not between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNull() {
            addCriterion("UPDATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNotNull() {
            addCriterion("UPDATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT =", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <>", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThan(Timestamp value) {
            addCriterion("UPDATEDAT >", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT >=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThan(Timestamp value) {
            addCriterion("UPDATEDAT <", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT not in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT not between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andOidLikeInsensitive(String value) {
            addCriterion("upper(OID) like", value.toUpperCase(), "oid");
            return (Criteria) this;
        }

        public Criteria andOriginalNameLikeInsensitive(String value) {
            addCriterion("upper(ORIGINALNAME) like", value.toUpperCase(), "originalName");
            return (Criteria) this;
        }

        public Criteria andStoredNameLikeInsensitive(String value) {
            addCriterion("upper(STOREDNAME) like", value.toUpperCase(), "storedName");
            return (Criteria) this;
        }

        public Criteria andPathLikeInsensitive(String value) {
            addCriterion("upper(PATH) like", value.toUpperCase(), "path");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLikeInsensitive(String value) {
            addCriterion("upper(CREATORID) like", value.toUpperCase(), "creatorId");
            return (Criteria) this;
        }
        
        public Criteria andTargetObjectTypeLikeInsensitive(String value) {
            addCriterion("upper(TARGETOBJECTTYPE) like", value.toUpperCase(), "targetObjectType");
            return (Criteria) this;
        }

        public Criteria andTargetObjectOidLikeInsensitive(String value) {
            addCriterion("upper(TARGETOBJECTOID) like", value.toUpperCase(), "targetObjectOid");
            return (Criteria) this;
        }
        
        public Criteria andUpdatorIdLikeInsensitive(String value) {
            addCriterion("upper(UPDATORID) like", value.toUpperCase(), "updatorId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}