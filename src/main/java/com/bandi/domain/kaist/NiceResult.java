package com.bandi.domain.kaist;

import java.io.Serializable;

import com.bandi.domain.base.BaseObject;

public class NiceResult extends BaseObject implements Serializable {

    private String message;

    private String flag;

    private String kaistUid;

    private String sEncodeData;

    private String sMode;

    private boolean proceed;

    private int iReturn;

    private String sPlainData;

    private String sCipherTime;

    private String sRequestNumber;

    private String sResponseNumber;

    private String sAuthType;

    private String sName;

    private String sBirthDate;

    private String sGender;

    private String sNationalInfo;

    private String sDupInfo;

    private String sConnInfo;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getKaistUid() {
        return kaistUid;
    }

    public void setKaistUid(String kaistUid) {
        this.kaistUid = kaistUid;
    }

    public String getsEncodeData() {
        return sEncodeData;
    }

    public void setsEncodeData(String sEncodeData) {
        this.sEncodeData = sEncodeData;
    }

    public String getsMode() {
        return sMode;
    }

    public void setsMode(String sMode) {
        this.sMode = sMode;
    }

    public boolean isProceed() {
        return proceed;
    }

    public void setProceed(boolean proceed) {
        this.proceed = proceed;
    }

    public int getiReturn() {
        return iReturn;
    }

    public void setiReturn(int iReturn) {
        this.iReturn = iReturn;
    }

    public String getsPlainData() {
        return sPlainData;
    }

    public void setsPlainData(String sPlainData) {
        this.sPlainData = sPlainData;
    }

    public String getsCipherTime() {
        return sCipherTime;
    }

    public void setsCipherTime(String sCipherTime) {
        this.sCipherTime = sCipherTime;
    }

    public String getsRequestNumber() {
        return sRequestNumber;
    }

    public void setsRequestNumber(String sRequestNumber) {
        this.sRequestNumber = sRequestNumber;
    }

    public String getsResponseNumber() {
        return sResponseNumber;
    }

    public void setsResponseNumber(String sResponseNumber) {
        this.sResponseNumber = sResponseNumber;
    }

    public String getsAuthType() {
        return sAuthType;
    }

    public void setsAuthType(String sAuthType) {
        this.sAuthType = sAuthType;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public String getsBirthDate() {
        return sBirthDate;
    }

    public void setsBirthDate(String sBirthDate) {
        this.sBirthDate = sBirthDate;
    }

    public String getsGender() {
        return sGender;
    }

    public void setsGender(String sGender) {
        this.sGender = sGender;
    }

    public String getsNationalInfo() {
        return sNationalInfo;
    }

    public void setsNationalInfo(String sNationalInfo) {
        this.sNationalInfo = sNationalInfo;
    }

    public String getsDupInfo() {
        return sDupInfo;
    }

    public void setsDupInfo(String sDupInfo) {
        this.sDupInfo = sDupInfo;
    }

    public String getsConnInfo() {
        return sConnInfo;
    }

    public void setsConnInfo(String sConnInfo) {
        this.sConnInfo = sConnInfo;
    }

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", message=").append(message);
        sb.append(", flag=").append(flag);
        sb.append(", kaistUid=").append(kaistUid);
        sb.append(", sEncodeData=").append(sEncodeData);
        sb.append(", sMode=").append(sMode);
        sb.append(", iReturn=").append(iReturn);
        sb.append(", sPlainData=").append(sPlainData);
        sb.append(", sCipherTime=").append(sCipherTime);
        sb.append(", sRequestNumber=").append(sRequestNumber);
        sb.append(", sResponseNumber=").append(sResponseNumber);
        sb.append(", sAuthType=").append(sAuthType);
        sb.append(", sName=").append(sName);
        sb.append(", sBirthDate=").append(sBirthDate);
        sb.append(", sGender=").append(sGender);
        sb.append(", sNationalInfo=").append(sNationalInfo);
        sb.append(", sDupInfo=").append(sDupInfo);
        sb.append(", sConnInfo=").append(sConnInfo);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

}