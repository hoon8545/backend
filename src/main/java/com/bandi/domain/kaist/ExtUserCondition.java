package com.bandi.domain.kaist;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ExtUserCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ExtUserCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }
        
        public Criteria andOidIsNull() {
            addCriterion("OID is null");
            return (Criteria) this;
        }

        public Criteria andOidIsNotNull() {
            addCriterion("OID is not null");
            return (Criteria) this;
        }

        public Criteria andOidEqualTo(String value) {
            addCriterion("OID =", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotEqualTo(String value) {
            addCriterion("OID <>", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThan(String value) {
            addCriterion("OID >", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThanOrEqualTo(String value) {
            addCriterion("OID >=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThan(String value) {
            addCriterion("OID <", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThanOrEqualTo(String value) {
            addCriterion("OID <=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLike(String value) {
            addCriterion("OID like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotLike(String value) {
            addCriterion("OID not like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidIn(List<String> values) {
            addCriterion("OID in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotIn(List<String> values) {
            addCriterion("OID not in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidBetween(String value1, String value2) {
            addCriterion("OID between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotBetween(String value1, String value2) {
            addCriterion("OID not between", value1, value2, "oid");
            return (Criteria) this;
        }
        
        public Criteria andKaistUidIsNull() {
            addCriterion("KAISTUID is null");
            return (Criteria) this;
        }

        public Criteria andKaistUidIsNotNull() {
            addCriterion("KAISTUID is not null");
            return (Criteria) this;
        }

        public Criteria andKaistUidEqualTo(String value) {
            addCriterion("KAISTUID =", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotEqualTo(String value) {
            addCriterion("KAISTUID <>", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidGreaterThan(String value) {
            addCriterion("KAISTUID >", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidGreaterThanOrEqualTo(String value) {
            addCriterion("KAISTUID >=", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidLessThan(String value) {
            addCriterion("KAISTUID <", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidLessThanOrEqualTo(String value) {
            addCriterion("KAISTUID <=", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidLike(String value) {
            addCriterion("KAISTUID like", value, "kaistUid");
            return (Criteria) this;
        }
        
        public Criteria andMgmtKaistUidLike(String value) {
            addCriterion("BDSEXTUSER.KAISTUID like", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotLike(String value) {
            addCriterion("KAISTUID not like", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidIn(List<String> values) {
            addCriterion("KAISTUID in", values, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotIn(List<String> values) {
            addCriterion("KAISTUID not in", values, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidBetween(String value1, String value2) {
            addCriterion("KAISTUID between", value1, value2, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotBetween(String value1, String value2) {
            addCriterion("KAISTUID not between", value1, value2, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("NAME is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("NAME is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("NAME =", value, "name");
            return (Criteria) this;
        }
        
        public Criteria andMgmtNameEqualTo(String value) {
            addCriterion("main.NAME =", value, "name");
            return (Criteria) this;
        }
        
        public Criteria andNameNotEqualTo(String value) {
            addCriterion("NAME <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("NAME >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("NAME >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("NAME <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("NAME <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("NAME like", value, "name");
            return (Criteria) this;
        }
        
        public Criteria andNameLikeMgmt(String value) {
            addCriterion("BDSEXTUSER.NAME like", value, "name");
            return (Criteria) this;
        }
        
        public Criteria andMgmtNameLike(String value) {
            addCriterion("BDSEXTUSER.NAME like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("NAME not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("NAME in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("NAME not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("NAME between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("NAME not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andFirstNameIsNull() {
            addCriterion("FIRST_NAME is null");
            return (Criteria) this;
        }

        public Criteria andFirstNameIsNotNull() {
            addCriterion("FIRST_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andFirstNameEqualTo(String value) {
            addCriterion("FIRST_NAME =", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameNotEqualTo(String value) {
            addCriterion("FIRST_NAME <>", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameGreaterThan(String value) {
            addCriterion("FIRST_NAME >", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameGreaterThanOrEqualTo(String value) {
            addCriterion("FIRST_NAME >=", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameLessThan(String value) {
            addCriterion("FIRST_NAME <", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameLessThanOrEqualTo(String value) {
            addCriterion("FIRST_NAME <=", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameLike(String value) {
            addCriterion("FIRST_NAME like", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameNotLike(String value) {
            addCriterion("FIRST_NAME not like", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameIn(List<String> values) {
            addCriterion("FIRST_NAME in", values, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameNotIn(List<String> values) {
            addCriterion("FIRST_NAME not in", values, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameBetween(String value1, String value2) {
            addCriterion("FIRST_NAME between", value1, value2, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameNotBetween(String value1, String value2) {
            addCriterion("FIRST_NAME not between", value1, value2, "firstName");
            return (Criteria) this;
        }

        public Criteria andLastNameIsNull() {
            addCriterion("LAST_NAME is null");
            return (Criteria) this;
        }

        public Criteria andLastNameIsNotNull() {
            addCriterion("LAST_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andLastNameEqualTo(String value) {
            addCriterion("LAST_NAME =", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameNotEqualTo(String value) {
            addCriterion("LAST_NAME <>", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameGreaterThan(String value) {
            addCriterion("LAST_NAME >", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameGreaterThanOrEqualTo(String value) {
            addCriterion("LAST_NAME >=", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameLessThan(String value) {
            addCriterion("LAST_NAME <", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameLessThanOrEqualTo(String value) {
            addCriterion("LAST_NAME <=", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameLike(String value) {
            addCriterion("LAST_NAME like", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameNotLike(String value) {
            addCriterion("LAST_NAME not like", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameIn(List<String> values) {
            addCriterion("LAST_NAME in", values, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameNotIn(List<String> values) {
            addCriterion("LAST_NAME not in", values, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameBetween(String value1, String value2) {
            addCriterion("LAST_NAME between", value1, value2, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameNotBetween(String value1, String value2) {
            addCriterion("LAST_NAME not between", value1, value2, "lastName");
            return (Criteria) this;
        }

        public Criteria andSexIsNull() {
            addCriterion("SEX is null");
            return (Criteria) this;
        }

        public Criteria andSexIsNotNull() {
            addCriterion("SEX is not null");
            return (Criteria) this;
        }

        public Criteria andSexEqualTo(String value) {
            addCriterion("SEX =", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotEqualTo(String value) {
            addCriterion("SEX <>", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThan(String value) {
            addCriterion("SEX >", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexGreaterThanOrEqualTo(String value) {
            addCriterion("SEX >=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThan(String value) {
            addCriterion("SEX <", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLessThanOrEqualTo(String value) {
            addCriterion("SEX <=", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexLike(String value) {
            addCriterion("SEX like", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotLike(String value) {
            addCriterion("SEX not like", value, "sex");
            return (Criteria) this;
        }

        public Criteria andSexIn(List<String> values) {
            addCriterion("SEX in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotIn(List<String> values) {
            addCriterion("SEX not in", values, "sex");
            return (Criteria) this;
        }

        public Criteria andSexBetween(String value1, String value2) {
            addCriterion("SEX between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andSexNotBetween(String value1, String value2) {
            addCriterion("SEX not between", value1, value2, "sex");
            return (Criteria) this;
        }

        public Criteria andBirthdayIsNull() {
            addCriterion("BIRTHDAY is null");
            return (Criteria) this;
        }

        public Criteria andBirthdayIsNotNull() {
            addCriterion("BIRTHDAY is not null");
            return (Criteria) this;
        }

        public Criteria andBirthdayEqualTo(String value) {
            addCriterion("BIRTHDAY =", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayNotEqualTo(String value) {
            addCriterion("BIRTHDAY <>", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayGreaterThan(String value) {
            addCriterion("BIRTHDAY >", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayGreaterThanOrEqualTo(String value) {
            addCriterion("BIRTHDAY >=", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayLessThan(String value) {
            addCriterion("BIRTHDAY <", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayLessThanOrEqualTo(String value) {
            addCriterion("BIRTHDAY <=", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayLike(String value) {
            addCriterion("BIRTHDAY like", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayNotLike(String value) {
            addCriterion("BIRTHDAY not like", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayIn(List<String> values) {
            addCriterion("BIRTHDAY in", values, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayNotIn(List<String> values) {
            addCriterion("BIRTHDAY not in", values, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayBetween(String value1, String value2) {
            addCriterion("BIRTHDAY between", value1, value2, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayNotBetween(String value1, String value2) {
            addCriterion("BIRTHDAY not between", value1, value2, "birthday");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberIsNull() {
            addCriterion("MOBILETELEPHONENUMBER is null");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberIsNotNull() {
            addCriterion("MOBILETELEPHONENUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberEqualTo(String value) {
            addCriterion("MOBILETELEPHONENUMBER =", value, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberNotEqualTo(String value) {
            addCriterion("MOBILETELEPHONENUMBER <>", value, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberGreaterThan(String value) {
            addCriterion("MOBILETELEPHONENUMBER >", value, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberGreaterThanOrEqualTo(String value) {
            addCriterion("MOBILETELEPHONENUMBER >=", value, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberLessThan(String value) {
            addCriterion("MOBILETELEPHONENUMBER <", value, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberLessThanOrEqualTo(String value) {
            addCriterion("MOBILETELEPHONENUMBER <=", value, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberLike(String value) {
            addCriterion("MOBILETELEPHONENUMBER like", value, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberNotLike(String value) {
            addCriterion("MOBILETELEPHONENUMBER not like", value, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberIn(List<String> values) {
            addCriterion("MOBILETELEPHONENUMBER in", values, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberNotIn(List<String> values) {
            addCriterion("MOBILETELEPHONENUMBER not in", values, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberBetween(String value1, String value2) {
            addCriterion("MOBILETELEPHONENUMBER between", value1, value2, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberNotBetween(String value1, String value2) {
            addCriterion("MOBILETELEPHONENUMBER not between", value1, value2, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andExternEmailIsNull() {
            addCriterion("EXTERNEMAIL is null");
            return (Criteria) this;
        }

        public Criteria andExternEmailIsNotNull() {
            addCriterion("EXTERNEMAIL is not null");
            return (Criteria) this;
        }

        public Criteria andExternEmailEqualTo(String value) {
            addCriterion("EXTERNEMAIL =", value, "externEmail");
            return (Criteria) this;
        }

        public Criteria andExternEmailNotEqualTo(String value) {
            addCriterion("EXTERNEMAIL <>", value, "externEmail");
            return (Criteria) this;
        }

        public Criteria andExternEmailGreaterThan(String value) {
            addCriterion("EXTERNEMAIL >", value, "externEmail");
            return (Criteria) this;
        }

        public Criteria andExternEmailGreaterThanOrEqualTo(String value) {
            addCriterion("EXTERNEMAIL >=", value, "externEmail");
            return (Criteria) this;
        }

        public Criteria andExternEmailLessThan(String value) {
            addCriterion("EXTERNEMAIL <", value, "externEmail");
            return (Criteria) this;
        }

        public Criteria andExternEmailLessThanOrEqualTo(String value) {
            addCriterion("EXTERNEMAIL <=", value, "externEmail");
            return (Criteria) this;
        }

        public Criteria andExternEmailLike(String value) {
            addCriterion("EXTERNEMAIL like", value, "externEmail");
            return (Criteria) this;
        }

        public Criteria andExternEmailNotLike(String value) {
            addCriterion("EXTERNEMAIL not like", value, "externEmail");
            return (Criteria) this;
        }

        public Criteria andExternEmailIn(List<String> values) {
            addCriterion("EXTERNEMAIL in", values, "externEmail");
            return (Criteria) this;
        }

        public Criteria andExternEmailNotIn(List<String> values) {
            addCriterion("EXTERNEMAIL not in", values, "externEmail");
            return (Criteria) this;
        }

        public Criteria andExternEmailBetween(String value1, String value2) {
            addCriterion("EXTERNEMAIL between", value1, value2, "externEmail");
            return (Criteria) this;
        }

        public Criteria andExternEmailNotBetween(String value1, String value2) {
            addCriterion("EXTERNEMAIL not between", value1, value2, "externEmail");
            return (Criteria) this;
        }

        public Criteria andCountryIsNull() {
            addCriterion("COUNTRY is null");
            return (Criteria) this;
        }

        public Criteria andCountryIsNotNull() {
            addCriterion("COUNTRY is not null");
            return (Criteria) this;
        }

        public Criteria andCountryEqualTo(String value) {
            addCriterion("COUNTRY =", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryNotEqualTo(String value) {
            addCriterion("COUNTRY <>", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryGreaterThan(String value) {
            addCriterion("COUNTRY >", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryGreaterThanOrEqualTo(String value) {
            addCriterion("COUNTRY >=", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryLessThan(String value) {
            addCriterion("COUNTRY <", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryLessThanOrEqualTo(String value) {
            addCriterion("COUNTRY <=", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryLike(String value) {
            addCriterion("COUNTRY like", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryNotLike(String value) {
            addCriterion("COUNTRY not like", value, "country");
            return (Criteria) this;
        }

        public Criteria andCountryIn(List<String> values) {
            addCriterion("COUNTRY in", values, "country");
            return (Criteria) this;
        }

        public Criteria andCountryNotIn(List<String> values) {
            addCriterion("COUNTRY not in", values, "country");
            return (Criteria) this;
        }

        public Criteria andCountryBetween(String value1, String value2) {
            addCriterion("COUNTRY between", value1, value2, "country");
            return (Criteria) this;
        }

        public Criteria andCountryNotBetween(String value1, String value2) {
            addCriterion("COUNTRY not between", value1, value2, "country");
            return (Criteria) this;
        }

        public Criteria andExtReqTypeIsNull() {
            addCriterion("EXTREQTYPE is null");
            return (Criteria) this;
        }

        public Criteria andExtReqTypeIsNotNull() {
            addCriterion("EXTREQTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andExtReqTypeEqualTo(String value) {
            addCriterion("EXTREQTYPE =", value, "extReqType");
            return (Criteria) this;
        }
        
        public Criteria andMgmtExtReqTypeEqualTo(String value) {
            addCriterion("EXTREQTYPE =", value, "extReqType");
            return (Criteria) this;
        }
        
        public Criteria andExtReqTypeNotEqualTo(String value) {
            addCriterion("EXTREQTYPE <>", value, "extReqType");
            return (Criteria) this;
        }

        public Criteria andExtReqTypeGreaterThan(String value) {
            addCriterion("EXTREQTYPE >", value, "extReqType");
            return (Criteria) this;
        }

        public Criteria andExtReqTypeGreaterThanOrEqualTo(String value) {
            addCriterion("EXTREQTYPE >=", value, "extReqType");
            return (Criteria) this;
        }

        public Criteria andExtReqTypeLessThan(String value) {
            addCriterion("EXTREQTYPE <", value, "extReqType");
            return (Criteria) this;
        }

        public Criteria andExtReqTypeLessThanOrEqualTo(String value) {
            addCriterion("EXTREQTYPE <=", value, "extReqType");
            return (Criteria) this;
        }

        public Criteria andExtReqTypeLike(String value) {
            addCriterion("EXTREQTYPE like", value, "extReqType");
            return (Criteria) this;
        }
        
        public Criteria andMgmtExtReqTypeLike(String value) {
            addCriterion("BDSEXTUSER.EXTREQTYPE like", value, "extReqType");
            return (Criteria) this;
        }

        public Criteria andExtReqTypeNotLike(String value) {
            addCriterion("EXTREQTYPE not like", value, "extReqType");
            return (Criteria) this;
        }

        public Criteria andExtReqTypeIn(List<String> values) {
            addCriterion("EXTREQTYPE in", values, "extReqType");
            return (Criteria) this;
        }

        public Criteria andExtReqTypeNotIn(List<String> values) {
            addCriterion("EXTREQTYPE not in", values, "extReqType");
            return (Criteria) this;
        }

        public Criteria andExtReqTypeBetween(String value1, String value2) {
            addCriterion("EXTREQTYPE between", value1, value2, "extReqType");
            return (Criteria) this;
        }

        public Criteria andExtReqTypeNotBetween(String value1, String value2) {
            addCriterion("EXTREQTYPE not between", value1, value2, "extReqType");
            return (Criteria) this;
        }

        public Criteria andExtCompanyIsNull() {
            addCriterion("EXTCOMPANY is null");
            return (Criteria) this;
        }

        public Criteria andExtCompanyIsNotNull() {
            addCriterion("EXTCOMPANY is not null");
            return (Criteria) this;
        }

        public Criteria andExtCompanyEqualTo(String value) {
            addCriterion("EXTCOMPANY =", value, "extCompany");
            return (Criteria) this;
        }
        
        public Criteria andMgmtExtCompanyEqualTo(String value) {
            addCriterion("BDSEXTUSER.EXTCOMPANY =", value, "extCompany");
            return (Criteria) this;
        }
        
        public Criteria andExtCompanyNotEqualTo(String value) {
            addCriterion("EXTCOMPANY <>", value, "extCompany");
            return (Criteria) this;
        }

        public Criteria andExtCompanyGreaterThan(String value) {
            addCriterion("EXTCOMPANY >", value, "extCompany");
            return (Criteria) this;
        }

        public Criteria andExtCompanyGreaterThanOrEqualTo(String value) {
            addCriterion("EXTCOMPANY >=", value, "extCompany");
            return (Criteria) this;
        }

        public Criteria andExtCompanyLessThan(String value) {
            addCriterion("EXTCOMPANY <", value, "extCompany");
            return (Criteria) this;
        }

        public Criteria andExtCompanyLessThanOrEqualTo(String value) {
            addCriterion("EXTCOMPANY <=", value, "extCompany");
            return (Criteria) this;
        }

        public Criteria andExtCompanyLike(String value) {
            addCriterion("EXTCOMPANY like", value, "extCompany");
            return (Criteria) this;
        }
        
        public Criteria andMgmtExtCompanyLike(String value) {
            addCriterion("BDSEXTUSER.EXTCOMPANY like", value, "extCompany");
            return (Criteria) this;
        }

        public Criteria andExtCompanyNotLike(String value) {
            addCriterion("EXTCOMPANY not like", value, "extCompany");
            return (Criteria) this;
        }

        public Criteria andExtCompanyIn(List<String> values) {
            addCriterion("EXTCOMPANY in", values, "extCompany");
            return (Criteria) this;
        }

        public Criteria andExtCompanyNotIn(List<String> values) {
            addCriterion("EXTCOMPANY not in", values, "extCompany");
            return (Criteria) this;
        }

        public Criteria andExtCompanyBetween(String value1, String value2) {
            addCriterion("EXTCOMPANY between", value1, value2, "extCompany");
            return (Criteria) this;
        }

        public Criteria andExtCompanyNotBetween(String value1, String value2) {
            addCriterion("EXTCOMPANY not between", value1, value2, "extCompany");
            return (Criteria) this;
        }

        public Criteria andExtEndMonthIsNull() {
            addCriterion("EXTENDMONTH is null");
            return (Criteria) this;
        }

        public Criteria andExtEndMonthIsNotNull() {
            addCriterion("EXTENDMONTH is not null");
            return (Criteria) this;
        }

        public Criteria andExtEndMonthEqualTo(int value) {
            addCriterion("EXTENDMONTH =", value, "extEndMonth");
            return (Criteria) this;
        }
        
        public Criteria andMgmtExtEndMonthEqualTo(int value) {
            addCriterion("BDSEXTUSER.EXTENDMONTH =", value, "extEndMonth");
            return (Criteria) this;
        }

        public Criteria andExtEndMonthNotEqualTo(int value) {
            addCriterion("EXTENDMONTH <>", value, "extEndMonth");
            return (Criteria) this;
        }

        public Criteria andExtEndMonthGreaterThan(int value) {
            addCriterion("EXTENDMONTH >", value, "extEndMonth");
            return (Criteria) this;
        }

        public Criteria andExtEndMonthGreaterThanOrEqualTo(int value) {
            addCriterion("EXTENDMONTH >=", value, "extEndMonth");
            return (Criteria) this;
        }

        public Criteria andExtEndMonthLessThan(int value) {
            addCriterion("EXTENDMONTH <", value, "extEndMonth");
            return (Criteria) this;
        }

        public Criteria andExtEndMonthLessThanOrEqualTo(int value) {
            addCriterion("EXTENDMONTH <=", value, "extEndMonth");
            return (Criteria) this;
        }

        public Criteria andExtEndMonthIn(List<Integer> values) {
            addCriterion("EXTENDMONTH in", values, "extEndMonth");
            return (Criteria) this;
        }

        public Criteria andExtEndMonthNotIn(List<Integer> values) {
            addCriterion("EXTENDMONTH not in", values, "extEndMonth");
            return (Criteria) this;
        }

        public Criteria andExtEndMonthBetween(int value1, int value2) {
            addCriterion("EXTENDMONTH between", value1, value2, "extEndMonth");
            return (Criteria) this;
        }

        public Criteria andExtEndMonthNotBetween(int value1, int value2) {
            addCriterion("EXTENDMONTH not between", value1, value2, "extEndMonth");
            return (Criteria) this;
        }

        public Criteria andExtReqReasonIsNull() {
            addCriterion("EXTREQREASON is null");
            return (Criteria) this;
        }

        public Criteria andExtReqReasonIsNotNull() {
            addCriterion("EXTREQREASON is not null");
            return (Criteria) this;
        }

        public Criteria andExtReqReasonEqualTo(String value) {
            addCriterion("EXTREQREASON =", value, "extReqReason");
            return (Criteria) this;
        }

        public Criteria andExtReqReasonNotEqualTo(String value) {
            addCriterion("EXTREQREASON <>", value, "extReqReason");
            return (Criteria) this;
        }

        public Criteria andExtReqReasonGreaterThan(String value) {
            addCriterion("EXTREQREASON >", value, "extReqReason");
            return (Criteria) this;
        }

        public Criteria andExtReqReasonGreaterThanOrEqualTo(String value) {
            addCriterion("EXTREQREASON >=", value, "extReqReason");
            return (Criteria) this;
        }

        public Criteria andExtReqReasonLessThan(String value) {
            addCriterion("EXTREQREASON <", value, "extReqReason");
            return (Criteria) this;
        }

        public Criteria andExtReqReasonLessThanOrEqualTo(String value) {
            addCriterion("EXTREQREASON <=", value, "extReqReason");
            return (Criteria) this;
        }

        public Criteria andExtReqReasonLike(String value) {
            addCriterion("EXTREQREASON like", value, "extReqReason");
            return (Criteria) this;
        }

        public Criteria andExtReqReasonNotLike(String value) {
            addCriterion("EXTREQREASON not like", value, "extReqReason");
            return (Criteria) this;
        }

        public Criteria andExtReqReasonIn(List<String> values) {
            addCriterion("EXTREQREASON in", values, "extReqReason");
            return (Criteria) this;
        }

        public Criteria andExtReqReasonNotIn(List<String> values) {
            addCriterion("EXTREQREASON not in", values, "extReqReason");
            return (Criteria) this;
        }

        public Criteria andExtReqReasonBetween(String value1, String value2) {
            addCriterion("EXTREQREASON between", value1, value2, "extReqReason");
            return (Criteria) this;
        }

        public Criteria andExtReqReasonNotBetween(String value1, String value2) {
            addCriterion("EXTREQREASON not between", value1, value2, "extReqReason");
            return (Criteria) this;
        }

        public Criteria andAvailableFlagIsNull() {
            addCriterion("AVAILABLEFLAG is null");
            return (Criteria) this;
        }

        public Criteria andAvailableFlagIsNotNull() {
            addCriterion("AVAILABLEFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andAvailableFlagEqualTo(String value) {
            addCriterion("AVAILABLEFLAG =", value, "availableFlag");
            return (Criteria) this;
        }
        
        public Criteria andMgmtAvailableFlagEqualTo(String value) {
            addCriterion("AVAILABLEFLAG =", value, "availableFlag");
            return (Criteria) this;
        }

        public Criteria andAvailableFlagNotEqualTo(String value) {
            addCriterion("AVAILABLEFLAG <>", value, "availableFlag");
            return (Criteria) this;
        }

        public Criteria andAvailableFlagGreaterThan(String value) {
            addCriterion("AVAILABLEFLAG >", value, "availableFlag");
            return (Criteria) this;
        }

        public Criteria andAvailableFlagGreaterThanOrEqualTo(String value) {
            addCriterion("AVAILABLEFLAG >=", value, "availableFlag");
            return (Criteria) this;
        }

        public Criteria andAvailableFlagLessThan(String value) {
            addCriterion("AVAILABLEFLAG <", value, "availableFlag");
            return (Criteria) this;
        }

        public Criteria andAvailableFlagLessThanOrEqualTo(String value) {
            addCriterion("AVAILABLEFLAG <=", value, "availableFlag");
            return (Criteria) this;
        }

        public Criteria andAvailableFlagLike(String value) {
            addCriterion("AVAILABLEFLAG like", value, "availableFlag");
            return (Criteria) this;
        }

        public Criteria andMgmtAvailableFlagLike(String value) {
            addCriterion("BDSEXTUSER.AVAILABLEFLAG like", value, "availableFlag");
            return (Criteria) this;
        }
        
        public Criteria andAvailableFlagNotLike(String value) {
            addCriterion("AVAILABLEFLAG not like", value, "availableFlag");
            return (Criteria) this;
        }

        public Criteria andAvailableFlagIn(List<String> values) {
            addCriterion("AVAILABLEFLAG in", values, "availableFlag");
            return (Criteria) this;
        }

        public Criteria andAvailableFlagNotIn(List<String> values) {
            addCriterion("AVAILABLEFLAG not in", values, "availableFlag");
            return (Criteria) this;
        }

        public Criteria andAvailableFlagBetween(String value1, String value2) {
            addCriterion("AVAILABLEFLAG between", value1, value2, "availableFlag");
            return (Criteria) this;
        }

        public Criteria andAvailableFlagNotBetween(String value1, String value2) {
            addCriterion("AVAILABLEFLAG not between", value1, value2, "availableFlag");
            return (Criteria) this;
        }
        public Criteria andApproverCommentIsNull() {
            addCriterion("AVAILABLEFLAG is null");
            return (Criteria) this;
        }

        public Criteria andApproverCommentIsNotNull() {
            addCriterion("AVAILABLEFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andApproverCommentEqualTo(String value) {
            addCriterion("APPROVERCOMMENT =", value, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentNotEqualTo(String value) {
            addCriterion("APPROVERCOMMENT <>", value, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentGreaterThan(String value) {
            addCriterion("APPROVERCOMMENT >", value, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentGreaterThanOrEqualTo(String value) {
            addCriterion("APPROVERCOMMENT >=", value, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentLessThan(String value) {
            addCriterion("APPROVERCOMMENT <", value, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentLessThanOrEqualTo(String value) {
            addCriterion("APPROVERCOMMENT <=", value, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentLike(String value) {
            addCriterion("APPROVERCOMMENT like", value, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentNotLike(String value) {
            addCriterion("APPROVERCOMMENT not like", value, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentIn(List<String> values) {
            addCriterion("APPROVERCOMMENT in", values, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentNotIn(List<String> values) {
            addCriterion("APPROVERCOMMENT not in", values, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentBetween(String value1, String value2) {
            addCriterion("APPROVERCOMMENT between", value1, value2, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentNotBetween(String value1, String value2) {
            addCriterion("APPROVERCOMMENT not between", value1, value2, "approverComment");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNull() {
            addCriterion("CREATORID is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNotNull() {
            addCriterion("CREATORID is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdEqualTo(String value) {
            addCriterion("CREATORID =", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotEqualTo(String value) {
            addCriterion("CREATORID <>", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThan(String value) {
            addCriterion("CREATORID >", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATORID >=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThan(String value) {
            addCriterion("CREATORID <", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThanOrEqualTo(String value) {
            addCriterion("CREATORID <=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLike(String value) {
            addCriterion("CREATORID like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotLike(String value) {
            addCriterion("CREATORID not like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIn(List<String> values) {
            addCriterion("CREATORID in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotIn(List<String> values) {
            addCriterion("CREATORID not in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdBetween(String value1, String value2) {
            addCriterion("CREATORID between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotBetween(String value1, String value2) {
            addCriterion("CREATORID not between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNull() {
            addCriterion("CreatedAt is null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNotNull() {
            addCriterion("CreatedAt is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtEqualTo(Timestamp value) {
            addCriterion("CreatedAt =", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotEqualTo(Timestamp value) {
            addCriterion("CreatedAt <>", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThan(Timestamp value) {
            addCriterion("CreatedAt >", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("CreatedAt >=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThan(Timestamp value) {
            addCriterion("CreatedAt <", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("CreatedAt <=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIn(List<Timestamp> values) {
            addCriterion("CreatedAt in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotIn(List<Timestamp> values) {
            addCriterion("CreatedAt not in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CreatedAt between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CreatedAt not between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNull() {
            addCriterion("UPDATORID is null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNotNull() {
            addCriterion("UPDATORID is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdEqualTo(String value) {
            addCriterion("UPDATORID =", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotEqualTo(String value) {
            addCriterion("UPDATORID <>", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThan(String value) {
            addCriterion("UPDATORID >", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATORID >=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThan(String value) {
            addCriterion("UPDATORID <", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThanOrEqualTo(String value) {
            addCriterion("UPDATORID <=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLike(String value) {
            addCriterion("UPDATORID like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotLike(String value) {
            addCriterion("UPDATORID not like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIn(List<String> values) {
            addCriterion("UPDATORID in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotIn(List<String> values) {
            addCriterion("UPDATORID not in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdBetween(String value1, String value2) {
            addCriterion("UPDATORID between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotBetween(String value1, String value2) {
            addCriterion("UPDATORID not between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andTempEndDatIsNull() {
            addCriterion("TempEndDat is null");
            return (Criteria) this;
        }

        public Criteria andTempEndDatIsNotNull() {
            addCriterion("TempEndDat is not null");
            return (Criteria) this;
        }

        public Criteria andTempEndDatEqualTo(Timestamp value) {
            addCriterion("tempEndDat =", value, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatNotEqualTo(Timestamp value) {
            addCriterion("TempEndDat <>", value, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatGreaterThan(Timestamp value) {
            addCriterion("TempEndDat >", value, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("TempEndDat >=", value, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatLessThan(Timestamp value) {
            addCriterion("TempEndDat <", value, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatLessThanOrEqualTo(Timestamp value) {
            addCriterion("TempEndDat <=", value, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatIn(List<Timestamp> values) {
            addCriterion("TempEndDat in", values, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatNotIn(List<Timestamp> values) {
            addCriterion("TempEndDat not in", values, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatBetween(Timestamp value1, Timestamp value2) {
            addCriterion("BDSUSER.TempEndDat between", value1, value2, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("TempEndDat not between", value1, value2, "tempEndDat");
            return (Criteria) this;
        }
        
        public Criteria andUpdatedAtIsNull() {
            addCriterion("UpdatedAt is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNotNull() {
            addCriterion("UpdatedAt is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtEqualTo(Timestamp value) {
            addCriterion("UpdatedAt =", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotEqualTo(Timestamp value) {
            addCriterion("UpdatedAt <>", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThan(Timestamp value) {
            addCriterion("UpdatedAt >", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("UpdatedAt >=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThan(Timestamp value) {
            addCriterion("UpdatedAt <", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("UpdatedAt <=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIn(List<Timestamp> values) {
            addCriterion("UpdatedAt in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotIn(List<Timestamp> values) {
            addCriterion("UpdatedAt not in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UpdatedAt between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UpdatedAt not between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andOidLikeInsensitive(String value) {
            addCriterion("upper(OID) like", value.toUpperCase(), "oid");
            return (Criteria) this;
        }

        public Criteria andNameLikeInsensitive(String value) {
            addCriterion("upper(NAME) like", value.toUpperCase(), "name");
            return (Criteria) this;
        }

        public Criteria andFirstNameLikeInsensitive(String value) {
            addCriterion("upper(FIRST_NAME) like", value.toUpperCase(), "firstName");
            return (Criteria) this;
        }

        public Criteria andLastNameLikeInsensitive(String value) {
            addCriterion("upper(LAST_NAME) like", value.toUpperCase(), "lastName");
            return (Criteria) this;
        }

        public Criteria andSexLikeInsensitive(String value) {
            addCriterion("upper(SEX) like", value.toUpperCase(), "sex");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberLikeInsensitive(String value) {
            addCriterion("upper(MOBILETELEPHONENUMBER) like", value.toUpperCase(), "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andExternEmailLikeInsensitive(String value) {
            addCriterion("upper(EXTERNEMAIL) like", value.toUpperCase(), "externEmail");
            return (Criteria) this;
        }

        public Criteria andCountryLikeInsensitive(String value) {
            addCriterion("upper(COUNTRY) like", value.toUpperCase(), "country");
            return (Criteria) this;
        }

        public Criteria andExtReqTypeLikeInsensitive(String value) {
            addCriterion("upper(EXTREQTYPE) like", value.toUpperCase(), "extReqType");
            return (Criteria) this;
        }

        public Criteria andExtCompanyLikeInsensitive(String value) {
            addCriterion("upper(EXTCOMPANY) like", value.toUpperCase(), "extCompany");
            return (Criteria) this;
        }

        public Criteria andExtReqReasonLikeInsensitive(String value) {
            addCriterion("upper(EXTREQREASON) like", value.toUpperCase(), "extReqReason");
            return (Criteria) this;
        }

        public Criteria andAvailableFlagLikeInsensitive(String value) {
            addCriterion("upper(AVAILABLEFLAG) like", value.toUpperCase(), "availableFlag");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLikeInsensitive(String value) {
            addCriterion("upper(CREATORID) like", value.toUpperCase(), "creatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLikeInsensitive(String value) {
            addCriterion("upper(UPDATORID) like", value.toUpperCase(), "updatorId");
            return (Criteria) this;
        }
        
        public Criteria andEndAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("BDSUSER.TEMPENDDAT between", value1, value2, "BDSUSER.TEMPENDDAT");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}