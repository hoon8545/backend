package com.bandi.domain.kaist;

import java.io.Serializable;
import java.sql.Timestamp;

import com.bandi.domain.base.BaseObject;

public class FileManager extends BaseObject implements Serializable {
	private String oid;

	private String originalName;

	private String storedName;

	private String path;

	private int fileSize;

	private Timestamp createdAt;

	private String creatorId;

	private String targetObjectType;

	private String targetObjectOid;
	
    private String updatorId;

    private Timestamp updatedAt;

	private static final long serialVersionUID = 1L;

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid == null ? null : oid.trim();
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName == null ? null : originalName.trim();
	}

	public String getStoredName() {
		return storedName;
	}

	public void setStoredName(String storedName) {
		this.storedName = storedName == null ? null : storedName.trim();
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path == null ? null : path.trim();
	}

	public int getFileSize() {
		return fileSize;
	}

	public void setFileSize(int size) {
		this.fileSize = size;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId == null ? null : creatorId.trim();
	}

	public String getTargetObjectType() {
		return targetObjectType;
	}

	public void setTargetObjectType(String targetObjectType) {
		this.targetObjectType = targetObjectType == null ? null : targetObjectType.trim();
	}

	public String getTargetObjectOid() {
		return targetObjectOid;
	}

	public void setTargetObjectOid(String targetObjectOid) {
		this.targetObjectOid = targetObjectOid == null ? null : targetObjectOid.trim();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", oid=").append(oid);
		sb.append(", originalName=").append(originalName);
		sb.append(", storedName=").append(storedName);
		sb.append(", path=").append(path);
		sb.append(", fileSize=").append(fileSize);
		sb.append(", createdAt=").append(createdAt);
		sb.append(", creatorId=").append(creatorId);
		sb.append(", targetObjectType=").append(targetObjectType);
		sb.append(", targetObjectOid=").append(targetObjectOid);
		sb.append(", serialVersionUID=").append(serialVersionUID);
		sb.append("]");
		return sb.toString();
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}