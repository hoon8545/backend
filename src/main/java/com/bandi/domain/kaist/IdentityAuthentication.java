package com.bandi.domain.kaist;

import com.bandi.domain.base.BaseObject;
import java.io.Serializable;
import java.sql.Timestamp;

public class IdentityAuthentication extends BaseObject implements Serializable {
    private String serviceType;

    private String piAuthenticationService;

    private String mpAuthenticationService;

    private String mobilePhone;

    private String mail;

    private String findUid;

    private String resultScreen;

    private String resultMail;

    private Timestamp createdAt;

    private String creatorId;

    private Timestamp updatedAt;

    private String updatorId;

    private static final long serialVersionUID = 1L;

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType == null ? null : serviceType.trim();
    }

    public String getPiAuthenticationService() {
        return piAuthenticationService;
    }

    public void setPiAuthenticationService(String piAuthenticationService) {
        this.piAuthenticationService = piAuthenticationService == null ? null : piAuthenticationService.trim();
    }

    public String getMpAuthenticationService() {
        return mpAuthenticationService;
    }

    public void setMpAuthenticationService(String mpAuthenticationService) {
        this.mpAuthenticationService = mpAuthenticationService == null ? null : mpAuthenticationService.trim();
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone == null ? null : mobilePhone.trim();
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail == null ? null : mail.trim();
    }

    public String getFindUid() {
        return findUid;
    }

    public void setFindUid(String findUid) {
        this.findUid = findUid == null ? null : findUid.trim();
    }

    public String getResultScreen() {
        return resultScreen;
    }

    public void setResultScreen(String resultScreen) {
        this.resultScreen = resultScreen == null ? null : resultScreen.trim();
    }

    public String getResultMail() {
        return resultMail;
    }

    public void setResultMail(String resultMail) {
        this.resultMail = resultMail == null ? null : resultMail.trim();
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId == null ? null : creatorId.trim();
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(String updatorId) {
        this.updatorId = updatorId == null ? null : updatorId.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", serviceType=").append(serviceType);
        sb.append(", piAuthenticationService=").append(piAuthenticationService);
        sb.append(", mpAuthenticationService=").append(mpAuthenticationService);
        sb.append(", mobilePhone=").append(mobilePhone);
        sb.append(", mail=").append(mail);
        sb.append(", findUid=").append(findUid);
        sb.append(", resultScreen=").append(resultScreen);
        sb.append(", resultMail=").append(resultMail);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", creatorId=").append(creatorId);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", updatorId=").append(updatorId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}