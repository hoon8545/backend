package com.bandi.domain.kaist;

import java.io.Serializable;
import java.sql.Timestamp;

import com.bandi.domain.base.BaseObject;

public class ServiceRequest extends BaseObject implements Serializable {
    private String oid;

    private String docNo;

    private String kaistUid;

    private String approverUid;

    private String email;

    private String handphone;

    private String flagReg;

    private String reqType;

    private Timestamp createdAt;

    private Timestamp updatedAt;

    private String status;

    private String userType;

    private String requestComment;

    private String approverComment;

    private String creatorId;

    private String updatorId;

    private String userName;

    private String approver;

    private static final long serialVersionUID = 1L;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid == null ? null : oid.trim();
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo == null ? null : docNo.trim();
    }

    public String getKaistUid() {
        return kaistUid;
    }

    public void setKaistUid(String kaistUid) {
        this.kaistUid = kaistUid == null ? null : kaistUid.trim();
    }

    public String getApproverUid() {
        return approverUid;
    }

    public void setApproverUid(String approverUid) {
        this.approverUid = approverUid == null ? null : approverUid.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getHandphone() {
        return handphone;
    }

    public void setHandphone(String handphone) {
        this.handphone = handphone == null ? null : handphone.trim();
    }

    public String getFlagReg() {
        return flagReg;
    }

    public void setFlagReg(String flagReg) {
        this.flagReg = flagReg == null ? null : flagReg.trim();
    }

    public String getReqType() {
        return reqType;
    }

    public void setReqType(String reqType) {
        this.reqType = reqType == null ? null : reqType.trim();
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType == null ? null : userType.trim();
    }

    public String getRequestComment() {
        return requestComment;
    }

    public void setRequestComment(String requestComment) {
        this.requestComment = requestComment == null ? null : requestComment.trim();
    }

    public String getApproverComment() {
        return approverComment;
    }

    public void setApproverComment(String approverComment) {
        this.approverComment = approverComment == null ? null : approverComment.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver == null ? null : approver.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", oid=").append(oid);
        sb.append(", docNo=").append(docNo);
        sb.append(", kaistUid=").append(kaistUid);
        sb.append(", approverUid=").append(approverUid);
        sb.append(", email=").append(email);
        sb.append(", handphone=").append(handphone);
        sb.append(", flagReg=").append(flagReg);
        sb.append(", reqType=").append(reqType);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", status=").append(status);
        sb.append(", userType=").append(userType);
        sb.append(", requestComment=").append(requestComment);
        sb.append(", approverComment=").append(approverComment);
        sb.append(", creatorId=").append(creatorId);
        sb.append(", updatorId=").append(updatorId);
        sb.append(", userName=").append(userName);
        sb.append(", approver=").append(approver);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    // ===================== End of Code Gen =====================
    private Timestamp[] createdAtBetween;
    private Timestamp[] updatedAtBetween;

	public Timestamp[] getCreatedAtBetween() {
		return createdAtBetween;
	}

	public void setCreatedAtBetween( Timestamp[] createdAtBetween ) {
		this.createdAtBetween = createdAtBetween;
	}

	public Timestamp[] getUpdatedAtBetween() {
		return updatedAtBetween;
	}

	public void setUpdatedAtBetween( Timestamp[] updatedAtBetween ) {
		this.updatedAtBetween = updatedAtBetween;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

}