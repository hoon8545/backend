package com.bandi.domain.kaist;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ServiceRequestCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ServiceRequestCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOidIsNull() {
            addCriterion("OID is null");
            return (Criteria) this;
        }

        public Criteria andOidIsNotNull() {
            addCriterion("OID is not null");
            return (Criteria) this;
        }

        public Criteria andOidEqualTo(String value) {
            addCriterion("OID =", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotEqualTo(String value) {
            addCriterion("OID <>", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThan(String value) {
            addCriterion("OID >", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThanOrEqualTo(String value) {
            addCriterion("OID >=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThan(String value) {
            addCriterion("OID <", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThanOrEqualTo(String value) {
            addCriterion("OID <=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLike(String value) {
            addCriterion("OID like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotLike(String value) {
            addCriterion("OID not like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidIn(List<String> values) {
            addCriterion("OID in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotIn(List<String> values) {
            addCriterion("OID not in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidBetween(String value1, String value2) {
            addCriterion("OID between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotBetween(String value1, String value2) {
            addCriterion("OID not between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andDocNoIsNull() {
            addCriterion("DOCNO is null");
            return (Criteria) this;
        }

        public Criteria andDocNoIsNotNull() {
            addCriterion("DOCNO is not null");
            return (Criteria) this;
        }

        public Criteria andDocNoEqualTo(String value) {
            addCriterion("DOCNO =", value, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoNotEqualTo(String value) {
            addCriterion("DOCNO <>", value, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoGreaterThan(String value) {
            addCriterion("DOCNO >", value, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoGreaterThanOrEqualTo(String value) {
            addCriterion("DOCNO >=", value, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoLessThan(String value) {
            addCriterion("DOCNO <", value, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoLessThanOrEqualTo(String value) {
            addCriterion("DOCNO <=", value, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoLike(String value) {
            addCriterion("DOCNO like", value, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoNotLike(String value) {
            addCriterion("DOCNO not like", value, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoIn(List<String> values) {
            addCriterion("DOCNO in", values, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoNotIn(List<String> values) {
            addCriterion("DOCNO not in", values, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoBetween(String value1, String value2) {
            addCriterion("DOCNO between", value1, value2, "docNo");
            return (Criteria) this;
        }

        public Criteria andDocNoNotBetween(String value1, String value2) {
            addCriterion("DOCNO not between", value1, value2, "docNo");
            return (Criteria) this;
        }

        public Criteria andKaistUidIsNull() {
            addCriterion("KAISTUID is null");
            return (Criteria) this;
        }

        public Criteria andKaistUidIsNotNull() {
            addCriterion("KAISTUID is not null");
            return (Criteria) this;
        }

        public Criteria andKaistUidEqualTo(String value) {
            addCriterion("KAISTUID =", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotEqualTo(String value) {
            addCriterion("KAISTUID <>", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidGreaterThan(String value) {
            addCriterion("KAISTUID >", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidGreaterThanOrEqualTo(String value) {
            addCriterion("KAISTUID >=", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidLessThan(String value) {
            addCriterion("KAISTUID <", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidLessThanOrEqualTo(String value) {
            addCriterion("KAISTUID <=", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidLike(String value) {
            addCriterion("KAISTUID like", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotLike(String value) {
            addCriterion("KAISTUID not like", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidIn(List<String> values) {
            addCriterion("KAISTUID in", values, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotIn(List<String> values) {
            addCriterion("KAISTUID not in", values, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidBetween(String value1, String value2) {
            addCriterion("KAISTUID between", value1, value2, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotBetween(String value1, String value2) {
            addCriterion("KAISTUID not between", value1, value2, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andApproverUidIsNull() {
            addCriterion("APPROVERUID is null");
            return (Criteria) this;
        }

        public Criteria andApproverUidIsNotNull() {
            addCriterion("APPROVERUID is not null");
            return (Criteria) this;
        }

        public Criteria andApproverUidEqualTo(String value) {
            addCriterion("APPROVERUID =", value, "approverUid");
            return (Criteria) this;
        }

        public Criteria andApproverUidNotEqualTo(String value) {
            addCriterion("APPROVERUID <>", value, "approverUid");
            return (Criteria) this;
        }

        public Criteria andApproverUidGreaterThan(String value) {
            addCriterion("APPROVERUID >", value, "approverUid");
            return (Criteria) this;
        }

        public Criteria andApproverUidGreaterThanOrEqualTo(String value) {
            addCriterion("APPROVERUID >=", value, "approverUid");
            return (Criteria) this;
        }

        public Criteria andApproverUidLessThan(String value) {
            addCriterion("APPROVERUID <", value, "approverUid");
            return (Criteria) this;
        }

        public Criteria andApproverUidLessThanOrEqualTo(String value) {
            addCriterion("APPROVERUID <=", value, "approverUid");
            return (Criteria) this;
        }

        public Criteria andApproverUidLike(String value) {
            addCriterion("APPROVERUID like", value, "approverUid");
            return (Criteria) this;
        }

        public Criteria andApproverUidNotLike(String value) {
            addCriterion("APPROVERUID not like", value, "approverUid");
            return (Criteria) this;
        }

        public Criteria andApproverUidIn(List<String> values) {
            addCriterion("APPROVERUID in", values, "approverUid");
            return (Criteria) this;
        }

        public Criteria andApproverUidNotIn(List<String> values) {
            addCriterion("APPROVERUID not in", values, "approverUid");
            return (Criteria) this;
        }

        public Criteria andApproverUidBetween(String value1, String value2) {
            addCriterion("APPROVERUID between", value1, value2, "approverUid");
            return (Criteria) this;
        }

        public Criteria andApproverUidNotBetween(String value1, String value2) {
            addCriterion("APPROVERUID not between", value1, value2, "approverUid");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("EMAIL is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("EMAIL is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("EMAIL =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("EMAIL <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("EMAIL >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("EMAIL >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("EMAIL <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("EMAIL <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("EMAIL like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("EMAIL not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("EMAIL in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("EMAIL not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("EMAIL between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("EMAIL not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andHandphoneIsNull() {
            addCriterion("HANDPHONE is null");
            return (Criteria) this;
        }

        public Criteria andHandphoneIsNotNull() {
            addCriterion("HANDPHONE is not null");
            return (Criteria) this;
        }

        public Criteria andHandphoneEqualTo(String value) {
            addCriterion("HANDPHONE =", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneNotEqualTo(String value) {
            addCriterion("HANDPHONE <>", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneGreaterThan(String value) {
            addCriterion("HANDPHONE >", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneGreaterThanOrEqualTo(String value) {
            addCriterion("HANDPHONE >=", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneLessThan(String value) {
            addCriterion("HANDPHONE <", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneLessThanOrEqualTo(String value) {
            addCriterion("HANDPHONE <=", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneLike(String value) {
            addCriterion("HANDPHONE like", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneNotLike(String value) {
            addCriterion("HANDPHONE not like", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneIn(List<String> values) {
            addCriterion("HANDPHONE in", values, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneNotIn(List<String> values) {
            addCriterion("HANDPHONE not in", values, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneBetween(String value1, String value2) {
            addCriterion("HANDPHONE between", value1, value2, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneNotBetween(String value1, String value2) {
            addCriterion("HANDPHONE not between", value1, value2, "handphone");
            return (Criteria) this;
        }

        public Criteria andFlagRegIsNull() {
            addCriterion("FLAGREG is null");
            return (Criteria) this;
        }

        public Criteria andFlagRegIsNotNull() {
            addCriterion("FLAGREG is not null");
            return (Criteria) this;
        }

        public Criteria andFlagRegEqualTo(String value) {
            addCriterion("FLAGREG =", value, "flagReg");
            return (Criteria) this;
        }

        public Criteria andFlagRegNotEqualTo(String value) {
            addCriterion("FLAGREG <>", value, "flagReg");
            return (Criteria) this;
        }

        public Criteria andFlagRegGreaterThan(String value) {
            addCriterion("FLAGREG >", value, "flagReg");
            return (Criteria) this;
        }

        public Criteria andFlagRegGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGREG >=", value, "flagReg");
            return (Criteria) this;
        }

        public Criteria andFlagRegLessThan(String value) {
            addCriterion("FLAGREG <", value, "flagReg");
            return (Criteria) this;
        }

        public Criteria andFlagRegLessThanOrEqualTo(String value) {
            addCriterion("FLAGREG <=", value, "flagReg");
            return (Criteria) this;
        }

        public Criteria andFlagRegLike(String value) {
            addCriterion("FLAGREG like", value, "flagReg");
            return (Criteria) this;
        }

        public Criteria andFlagRegNotLike(String value) {
            addCriterion("FLAGREG not like", value, "flagReg");
            return (Criteria) this;
        }

        public Criteria andFlagRegIn(List<String> values) {
            addCriterion("FLAGREG in", values, "flagReg");
            return (Criteria) this;
        }

        public Criteria andFlagRegNotIn(List<String> values) {
            addCriterion("FLAGREG not in", values, "flagReg");
            return (Criteria) this;
        }

        public Criteria andFlagRegBetween(String value1, String value2) {
            addCriterion("FLAGREG between", value1, value2, "flagReg");
            return (Criteria) this;
        }

        public Criteria andFlagRegNotBetween(String value1, String value2) {
            addCriterion("FLAGREG not between", value1, value2, "flagReg");
            return (Criteria) this;
        }

        public Criteria andReqTypeIsNull() {
            addCriterion("REQTYPE is null");
            return (Criteria) this;
        }

        public Criteria andReqTypeIsNotNull() {
            addCriterion("REQTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andReqTypeEqualTo(String value) {
            addCriterion("REQTYPE =", value, "reqType");
            return (Criteria) this;
        }

        public Criteria andReqTypeNotEqualTo(String value) {
            addCriterion("REQTYPE <>", value, "reqType");
            return (Criteria) this;
        }

        public Criteria andReqTypeGreaterThan(String value) {
            addCriterion("REQTYPE >", value, "reqType");
            return (Criteria) this;
        }

        public Criteria andReqTypeGreaterThanOrEqualTo(String value) {
            addCriterion("REQTYPE >=", value, "reqType");
            return (Criteria) this;
        }

        public Criteria andReqTypeLessThan(String value) {
            addCriterion("REQTYPE <", value, "reqType");
            return (Criteria) this;
        }

        public Criteria andReqTypeLessThanOrEqualTo(String value) {
            addCriterion("REQTYPE <=", value, "reqType");
            return (Criteria) this;
        }

        public Criteria andReqTypeLike(String value) {
            addCriterion("REQTYPE like", value, "reqType");
            return (Criteria) this;
        }

        public Criteria andReqTypeNotLike(String value) {
            addCriterion("REQTYPE not like", value, "reqType");
            return (Criteria) this;
        }

        public Criteria andReqTypeIn(List<String> values) {
            addCriterion("REQTYPE in", values, "reqType");
            return (Criteria) this;
        }

        public Criteria andReqTypeNotIn(List<String> values) {
            addCriterion("REQTYPE not in", values, "reqType");
            return (Criteria) this;
        }

        public Criteria andReqTypeBetween(String value1, String value2) {
            addCriterion("REQTYPE between", value1, value2, "reqType");
            return (Criteria) this;
        }

        public Criteria andReqTypeNotBetween(String value1, String value2) {
            addCriterion("REQTYPE not between", value1, value2, "reqType");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNull() {
            addCriterion("CREATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNotNull() {
            addCriterion("CREATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtEqualTo(Timestamp value) {
            addCriterion("CREATEDAT =", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <>", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThan(Timestamp value) {
            addCriterion("CREATEDAT >", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT >=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThan(Timestamp value) {
            addCriterion("CREATEDAT <", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIn(List<Timestamp> values) {
            addCriterion("CREATEDAT in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotIn(List<Timestamp> values) {
            addCriterion("CREATEDAT not in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT not between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNull() {
            addCriterion("UPDATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNotNull() {
            addCriterion("UPDATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT =", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <>", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThan(Timestamp value) {
            addCriterion("UPDATEDAT >", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT >=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThan(Timestamp value) {
            addCriterion("UPDATEDAT <", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT not in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT not between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("STATUS is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("STATUS =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("STATUS <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("STATUS >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("STATUS >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("STATUS <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("STATUS <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("STATUS like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("STATUS not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("STATUS in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("STATUS not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("STATUS between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("STATUS not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andUserTypeIsNull() {
            addCriterion("USERTYPE is null");
            return (Criteria) this;
        }

        public Criteria andUserTypeIsNotNull() {
            addCriterion("USERTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andUserTypeEqualTo(String value) {
            addCriterion("USERTYPE =", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotEqualTo(String value) {
            addCriterion("USERTYPE <>", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeGreaterThan(String value) {
            addCriterion("USERTYPE >", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeGreaterThanOrEqualTo(String value) {
            addCriterion("USERTYPE >=", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLessThan(String value) {
            addCriterion("USERTYPE <", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLessThanOrEqualTo(String value) {
            addCriterion("USERTYPE <=", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLike(String value) {
            addCriterion("USERTYPE like", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotLike(String value) {
            addCriterion("USERTYPE not like", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeIn(List<String> values) {
            addCriterion("USERTYPE in", values, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotIn(List<String> values) {
            addCriterion("USERTYPE not in", values, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeBetween(String value1, String value2) {
            addCriterion("USERTYPE between", value1, value2, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotBetween(String value1, String value2) {
            addCriterion("USERTYPE not between", value1, value2, "userType");
            return (Criteria) this;
        }

        public Criteria andRequestCommentIsNull() {
            addCriterion("REQUESTCOMMENT is null");
            return (Criteria) this;
        }

        public Criteria andRequestCommentIsNotNull() {
            addCriterion("REQUESTCOMMENT is not null");
            return (Criteria) this;
        }

        public Criteria andRequestCommentEqualTo(String value) {
            addCriterion("REQUESTCOMMENT =", value, "requestComment");
            return (Criteria) this;
        }

        public Criteria andRequestCommentNotEqualTo(String value) {
            addCriterion("REQUESTCOMMENT <>", value, "requestComment");
            return (Criteria) this;
        }

        public Criteria andRequestCommentGreaterThan(String value) {
            addCriterion("REQUESTCOMMENT >", value, "requestComment");
            return (Criteria) this;
        }

        public Criteria andRequestCommentGreaterThanOrEqualTo(String value) {
            addCriterion("REQUESTCOMMENT >=", value, "requestComment");
            return (Criteria) this;
        }

        public Criteria andRequestCommentLessThan(String value) {
            addCriterion("REQUESTCOMMENT <", value, "requestComment");
            return (Criteria) this;
        }

        public Criteria andRequestCommentLessThanOrEqualTo(String value) {
            addCriterion("REQUESTCOMMENT <=", value, "requestComment");
            return (Criteria) this;
        }

        public Criteria andRequestCommentLike(String value) {
            addCriterion("REQUESTCOMMENT like", value, "requestComment");
            return (Criteria) this;
        }

        public Criteria andRequestCommentNotLike(String value) {
            addCriterion("REQUESTCOMMENT not like", value, "requestComment");
            return (Criteria) this;
        }

        public Criteria andRequestCommentIn(List<String> values) {
            addCriterion("REQUESTCOMMENT in", values, "requestComment");
            return (Criteria) this;
        }

        public Criteria andRequestCommentNotIn(List<String> values) {
            addCriterion("REQUESTCOMMENT not in", values, "requestComment");
            return (Criteria) this;
        }

        public Criteria andRequestCommentBetween(String value1, String value2) {
            addCriterion("REQUESTCOMMENT between", value1, value2, "requestComment");
            return (Criteria) this;
        }

        public Criteria andRequestCommentNotBetween(String value1, String value2) {
            addCriterion("REQUESTCOMMENT not between", value1, value2, "requestComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentIsNull() {
            addCriterion("APPROVERCOMMENT is null");
            return (Criteria) this;
        }

        public Criteria andApproverCommentIsNotNull() {
            addCriterion("APPROVERCOMMENT is not null");
            return (Criteria) this;
        }

        public Criteria andApproverCommentEqualTo(String value) {
            addCriterion("APPROVERCOMMENT =", value, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentNotEqualTo(String value) {
            addCriterion("APPROVERCOMMENT <>", value, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentGreaterThan(String value) {
            addCriterion("APPROVERCOMMENT >", value, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentGreaterThanOrEqualTo(String value) {
            addCriterion("APPROVERCOMMENT >=", value, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentLessThan(String value) {
            addCriterion("APPROVERCOMMENT <", value, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentLessThanOrEqualTo(String value) {
            addCriterion("APPROVERCOMMENT <=", value, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentLike(String value) {
            addCriterion("APPROVERCOMMENT like", value, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentNotLike(String value) {
            addCriterion("APPROVERCOMMENT not like", value, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentIn(List<String> values) {
            addCriterion("APPROVERCOMMENT in", values, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentNotIn(List<String> values) {
            addCriterion("APPROVERCOMMENT not in", values, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentBetween(String value1, String value2) {
            addCriterion("APPROVERCOMMENT between", value1, value2, "approverComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentNotBetween(String value1, String value2) {
            addCriterion("APPROVERCOMMENT not between", value1, value2, "approverComment");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNull() {
            addCriterion("CREATORID is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNotNull() {
            addCriterion("CREATORID is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdEqualTo(String value) {
            addCriterion("CREATORID =", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotEqualTo(String value) {
            addCriterion("CREATORID <>", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThan(String value) {
            addCriterion("CREATORID >", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATORID >=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThan(String value) {
            addCriterion("CREATORID <", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThanOrEqualTo(String value) {
            addCriterion("CREATORID <=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLike(String value) {
            addCriterion("CREATORID like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotLike(String value) {
            addCriterion("CREATORID not like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIn(List<String> values) {
            addCriterion("CREATORID in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotIn(List<String> values) {
            addCriterion("CREATORID not in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdBetween(String value1, String value2) {
            addCriterion("CREATORID between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotBetween(String value1, String value2) {
            addCriterion("CREATORID not between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNull() {
            addCriterion("UPDATORID is null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNotNull() {
            addCriterion("UPDATORID is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdEqualTo(String value) {
            addCriterion("UPDATORID =", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotEqualTo(String value) {
            addCriterion("UPDATORID <>", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThan(String value) {
            addCriterion("UPDATORID >", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATORID >=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThan(String value) {
            addCriterion("UPDATORID <", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThanOrEqualTo(String value) {
            addCriterion("UPDATORID <=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLike(String value) {
            addCriterion("UPDATORID like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotLike(String value) {
            addCriterion("UPDATORID not like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIn(List<String> values) {
            addCriterion("UPDATORID in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotIn(List<String> values) {
            addCriterion("UPDATORID not in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdBetween(String value1, String value2) {
            addCriterion("UPDATORID between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotBetween(String value1, String value2) {
            addCriterion("UPDATORID not between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNull() {
            addCriterion("USERNAME is null");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNotNull() {
            addCriterion("USERNAME is not null");
            return (Criteria) this;
        }

        public Criteria andUserNameEqualTo(String value) {
            addCriterion("USERNAME =", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("USERNAME <>", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("USERNAME >", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("USERNAME >=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThan(String value) {
            addCriterion("USERNAME <", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("USERNAME <=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLike(String value) {
            addCriterion("USERNAME like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotLike(String value) {
            addCriterion("USERNAME not like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameIn(List<String> values) {
            addCriterion("USERNAME in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("USERNAME not in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("USERNAME between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("USERNAME not between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andApproverIsNull() {
            addCriterion("APPROVER is null");
            return (Criteria) this;
        }

        public Criteria andApproverIsNotNull() {
            addCriterion("APPROVER is not null");
            return (Criteria) this;
        }

        public Criteria andApproverEqualTo(String value) {
            addCriterion("APPROVER =", value, "approver");
            return (Criteria) this;
        }

        public Criteria andApproverNotEqualTo(String value) {
            addCriterion("APPROVER <>", value, "approver");
            return (Criteria) this;
        }

        public Criteria andApproverGreaterThan(String value) {
            addCriterion("APPROVER >", value, "approver");
            return (Criteria) this;
        }

        public Criteria andApproverGreaterThanOrEqualTo(String value) {
            addCriterion("APPROVER >=", value, "approver");
            return (Criteria) this;
        }

        public Criteria andApproverLessThan(String value) {
            addCriterion("APPROVER <", value, "approver");
            return (Criteria) this;
        }

        public Criteria andApproverLessThanOrEqualTo(String value) {
            addCriterion("APPROVER <=", value, "approver");
            return (Criteria) this;
        }

        public Criteria andApproverLike(String value) {
            addCriterion("APPROVER like", value, "approver");
            return (Criteria) this;
        }

        public Criteria andApproverNotLike(String value) {
            addCriterion("APPROVER not like", value, "approver");
            return (Criteria) this;
        }

        public Criteria andApproverIn(List<String> values) {
            addCriterion("APPROVER in", values, "approver");
            return (Criteria) this;
        }

        public Criteria andApproverNotIn(List<String> values) {
            addCriterion("APPROVER not in", values, "approver");
            return (Criteria) this;
        }

        public Criteria andApproverBetween(String value1, String value2) {
            addCriterion("APPROVER between", value1, value2, "approver");
            return (Criteria) this;
        }

        public Criteria andApproverNotBetween(String value1, String value2) {
            addCriterion("APPROVER not between", value1, value2, "approver");
            return (Criteria) this;
        }

        public Criteria andOidLikeInsensitive(String value) {
            addCriterion("upper(OID) like", value.toUpperCase(), "oid");
            return (Criteria) this;
        }

        public Criteria andDocNoLikeInsensitive(String value) {
            addCriterion("upper(DOCNO) like", value.toUpperCase(), "docNo");
            return (Criteria) this;
        }

        public Criteria andKaistUidLikeInsensitive(String value) {
            addCriterion("upper(KAISTUID) like", value.toUpperCase(), "kaistUid");
            return (Criteria) this;
        }

        public Criteria andApproverUidLikeInsensitive(String value) {
            addCriterion("upper(APPROVERUID) like", value.toUpperCase(), "approverUid");
            return (Criteria) this;
        }

        public Criteria andEmailLikeInsensitive(String value) {
            addCriterion("upper(EMAIL) like", value.toUpperCase(), "email");
            return (Criteria) this;
        }

        public Criteria andHandphoneLikeInsensitive(String value) {
            addCriterion("upper(HANDPHONE) like", value.toUpperCase(), "handphone");
            return (Criteria) this;
        }

        public Criteria andFlagRegLikeInsensitive(String value) {
            addCriterion("upper(FLAGREG) like", value.toUpperCase(), "flagReg");
            return (Criteria) this;
        }

        public Criteria andReqTypeLikeInsensitive(String value) {
            addCriterion("upper(REQTYPE) like", value.toUpperCase(), "reqType");
            return (Criteria) this;
        }

        public Criteria andStatusLikeInsensitive(String value) {
            addCriterion("upper(STATUS) like", value.toUpperCase(), "status");
            return (Criteria) this;
        }

        public Criteria andUserTypeLikeInsensitive(String value) {
            addCriterion("upper(USERTYPE) like", value.toUpperCase(), "userType");
            return (Criteria) this;
        }

        public Criteria andRequestCommentLikeInsensitive(String value) {
            addCriterion("upper(REQUESTCOMMENT) like", value.toUpperCase(), "requestComment");
            return (Criteria) this;
        }

        public Criteria andApproverCommentLikeInsensitive(String value) {
            addCriterion("upper(APPROVERCOMMENT) like", value.toUpperCase(), "approverComment");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLikeInsensitive(String value) {
            addCriterion("upper(CREATORID) like", value.toUpperCase(), "creatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLikeInsensitive(String value) {
            addCriterion("upper(UPDATORID) like", value.toUpperCase(), "updatorId");
            return (Criteria) this;
        }

        public Criteria andUserNameLikeInsensitive(String value) {
            addCriterion("upper(USERNAME) like", value.toUpperCase(), "userName");
            return (Criteria) this;
        }

        public Criteria andApproverLikeInsensitive(String value) {
            addCriterion("upper(APPROVER) like", value.toUpperCase(), "approver");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}