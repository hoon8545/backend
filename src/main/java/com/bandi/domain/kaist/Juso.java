package com.bandi.domain.kaist;

import java.io.Serializable;

import com.bandi.domain.base.BaseObject;

public class Juso extends BaseObject implements Serializable {
    private String returnUrl;

    private String confmKey;

    private String resultType;


    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl == null ? null : returnUrl.trim();
    }

    public String getConfmKey() {
        return confmKey;
    }

    public void setConfmKey(String confmKey) {
        this.confmKey = confmKey == null ? null : confmKey.trim();
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType == null ? null : resultType.trim();
    }

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", returnUrl=").append(returnUrl);
        sb.append(", confmKey=").append(confmKey);
        sb.append(", resultType=").append(resultType);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}