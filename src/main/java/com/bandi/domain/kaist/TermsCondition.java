package com.bandi.domain.kaist;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class TermsCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TermsCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOidIsNull() {
            addCriterion("OID is null");
            return (Criteria) this;
        }

        public Criteria andOidIsNotNull() {
            addCriterion("OID is not null");
            return (Criteria) this;
        }

        public Criteria andOidEqualTo(String value) {
            addCriterion("OID =", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotEqualTo(String value) {
            addCriterion("OID <>", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThan(String value) {
            addCriterion("OID >", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThanOrEqualTo(String value) {
            addCriterion("OID >=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThan(String value) {
            addCriterion("OID <", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThanOrEqualTo(String value) {
            addCriterion("OID <=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLike(String value) {
            addCriterion("OID like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotLike(String value) {
            addCriterion("OID not like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidIn(List<String> values) {
            addCriterion("OID in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotIn(List<String> values) {
            addCriterion("OID not in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidBetween(String value1, String value2) {
            addCriterion("OID between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotBetween(String value1, String value2) {
            addCriterion("OID not between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andTermsTypeIsNull() {
            addCriterion("TERMSTYPE is null");
            return (Criteria) this;
        }

        public Criteria andTermsTypeIsNotNull() {
            addCriterion("TERMSTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andTermsTypeEqualTo(String value) {
            addCriterion("TERMSTYPE =", value, "termsType");
            return (Criteria) this;
        }

        public Criteria andTermsTypeNotEqualTo(String value) {
            addCriterion("TERMSTYPE <>", value, "termsType");
            return (Criteria) this;
        }

        public Criteria andTermsTypeGreaterThan(String value) {
            addCriterion("TERMSTYPE >", value, "termsType");
            return (Criteria) this;
        }

        public Criteria andTermsTypeGreaterThanOrEqualTo(String value) {
            addCriterion("TERMSTYPE >=", value, "termsType");
            return (Criteria) this;
        }

        public Criteria andTermsTypeLessThan(String value) {
            addCriterion("TERMSTYPE <", value, "termsType");
            return (Criteria) this;
        }

        public Criteria andTermsTypeLessThanOrEqualTo(String value) {
            addCriterion("TERMSTYPE <=", value, "termsType");
            return (Criteria) this;
        }

        public Criteria andTermsTypeLike(String value) {
            addCriterion("TERMSTYPE like", value, "termsType");
            return (Criteria) this;
        }

        public Criteria andTermsTypeNotLike(String value) {
            addCriterion("TERMSTYPE not like", value, "termsType");
            return (Criteria) this;
        }

        public Criteria andTermsTypeIn(List<String> values) {
            addCriterion("TERMSTYPE in", values, "termsType");
            return (Criteria) this;
        }

        public Criteria andTermsTypeNotIn(List<String> values) {
            addCriterion("TERMSTYPE not in", values, "termsType");
            return (Criteria) this;
        }

        public Criteria andTermsTypeBetween(String value1, String value2) {
            addCriterion("TERMSTYPE between", value1, value2, "termsType");
            return (Criteria) this;
        }

        public Criteria andTermsTypeNotBetween(String value1, String value2) {
            addCriterion("TERMSTYPE not between", value1, value2, "termsType");
            return (Criteria) this;
        }

        public Criteria andTermsIndexIsNull() {
            addCriterion("TERMSINDEX is null");
            return (Criteria) this;
        }

        public Criteria andTermsIndexIsNotNull() {
            addCriterion("TERMSINDEX is not null");
            return (Criteria) this;
        }

        public Criteria andTermsIndexEqualTo(String value) {
            addCriterion("TERMSINDEX =", value, "termsIndex");
            return (Criteria) this;
        }

        public Criteria andTermsIndexNotEqualTo(String value) {
            addCriterion("TERMSINDEX <>", value, "termsIndex");
            return (Criteria) this;
        }

        public Criteria andTermsIndexGreaterThan(String value) {
            addCriterion("TERMSINDEX >", value, "termsIndex");
            return (Criteria) this;
        }

        public Criteria andTermsIndexGreaterThanOrEqualTo(String value) {
            addCriterion("TERMSINDEX >=", value, "termsIndex");
            return (Criteria) this;
        }

        public Criteria andTermsIndexLessThan(String value) {
            addCriterion("TERMSINDEX <", value, "termsIndex");
            return (Criteria) this;
        }

        public Criteria andTermsIndexLessThanOrEqualTo(String value) {
            addCriterion("TERMSINDEX <=", value, "termsIndex");
            return (Criteria) this;
        }

        public Criteria andTermsIndexIn(List<String> values) {
            addCriterion("TERMSINDEX in", values, "termsIndex");
            return (Criteria) this;
        }

        public Criteria andTermsIndexNotIn(List<String> values) {
            addCriterion("TERMSINDEX not in", values, "termsIndex");
            return (Criteria) this;
        }

        public Criteria andTermsIndexBetween(String value1, String value2) {
            addCriterion("TERMSINDEX between", value1, value2, "termsIndex");
            return (Criteria) this;
        }

        public Criteria andTermsIndexNotBetween(String value1, String value2) {
            addCriterion("TERMSINDEX not between", value1, value2, "termsIndex");
            return (Criteria) this;
        }

        public Criteria andTermsSubjectIsNull() {
            addCriterion("TERMSSUBJECT is null");
            return (Criteria) this;
        }

        public Criteria andTermsSubjectIsNotNull() {
            addCriterion("TERMSSUBJECT is not null");
            return (Criteria) this;
        }

        public Criteria andTermsSubjectEqualTo(String value) {
            addCriterion("TERMSSUBJECT =", value, "termsSubject");
            return (Criteria) this;
        }

        public Criteria andTermsSubjectNotEqualTo(String value) {
            addCriterion("TERMSSUBJECT <>", value, "termsSubject");
            return (Criteria) this;
        }

        public Criteria andTermsSubjectGreaterThan(String value) {
            addCriterion("TERMSSUBJECT >", value, "termsSubject");
            return (Criteria) this;
        }

        public Criteria andTermsSubjectGreaterThanOrEqualTo(String value) {
            addCriterion("TERMSSUBJECT >=", value, "termsSubject");
            return (Criteria) this;
        }

        public Criteria andTermsSubjectLessThan(String value) {
            addCriterion("TERMSSUBJECT <", value, "termsSubject");
            return (Criteria) this;
        }

        public Criteria andTermsSubjectLessThanOrEqualTo(String value) {
            addCriterion("TERMSSUBJECT <=", value, "termsSubject");
            return (Criteria) this;
        }

        public Criteria andTermsSubjectLike(String value) {
            addCriterion("TERMSSUBJECT like", value, "termsSubject");
            return (Criteria) this;
        }

        public Criteria andTermsSubjectNotLike(String value) {
            addCriterion("TERMSSUBJECT not like", value, "termsSubject");
            return (Criteria) this;
        }

        public Criteria andTermsSubjectIn(List<String> values) {
            addCriterion("TERMSSUBJECT in", values, "termsSubject");
            return (Criteria) this;
        }

        public Criteria andTermsSubjectNotIn(List<String> values) {
            addCriterion("TERMSSUBJECT not in", values, "termsSubject");
            return (Criteria) this;
        }

        public Criteria andTermsSubjectBetween(String value1, String value2) {
            addCriterion("TERMSSUBJECT between", value1, value2, "termsSubject");
            return (Criteria) this;
        }

        public Criteria andTermsSubjectNotBetween(String value1, String value2) {
            addCriterion("TERMSSUBJECT not between", value1, value2, "termsSubject");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNull() {
            addCriterion("CREATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNotNull() {
            addCriterion("CREATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtEqualTo(Timestamp value) {
            addCriterion("CREATEDAT =", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <>", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThan(Timestamp value) {
            addCriterion("CREATEDAT >", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT >=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThan(Timestamp value) {
            addCriterion("CREATEDAT <", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIn(List<Timestamp> values) {
            addCriterion("CREATEDAT in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotIn(List<Timestamp> values) {
            addCriterion("CREATEDAT not in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT not between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNull() {
            addCriterion("UPDATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNotNull() {
            addCriterion("UPDATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT =", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <>", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThan(Timestamp value) {
            addCriterion("UPDATEDAT >", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT >=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThan(Timestamp value) {
            addCriterion("UPDATEDAT <", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT not in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT not between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andOidLikeInsensitive(String value) {
            addCriterion("upper(OID) like", value.toUpperCase(), "oid");
            return (Criteria) this;
        }

        public Criteria andTermsTypeLikeInsensitive(String value) {
            addCriterion("upper(TERMSTYPE) like", value.toUpperCase(), "termsType");
            return (Criteria) this;
        }

        public Criteria andTermsSubjectLikeInsensitive(String value) {
            addCriterion("upper(TERMSSUBJECT) like", value.toUpperCase(), "termsSubject");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}