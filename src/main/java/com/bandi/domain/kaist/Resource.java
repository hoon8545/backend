package com.bandi.domain.kaist;

import java.io.Serializable;
import java.sql.Timestamp;

import com.bandi.domain.base.BaseObject;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Resource extends BaseObject implements Serializable {
    private String oid;

    private String clientOid;

    private String name;

    private String parentOid;

    private String description;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private int sortOrder;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String creatorId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Timestamp createdAt;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String updatorId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Timestamp updatedAt;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private int subLastIndex;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String fullPathIndex;

    private String flagUse;

    private static final long serialVersionUID = 1L;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid == null ? null : oid.trim();
    }

    public String getClientOid() {
        return clientOid;
    }

    public void setClientOid(String clientOid) {
        this.clientOid = clientOid == null ? null : clientOid.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getParentOid() {
        return parentOid;
    }

    public void setParentOid(String parentOid) {
        this.parentOid = parentOid == null ? null : parentOid.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId == null ? null : creatorId.trim();
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(String updatorId) {
        this.updatorId = updatorId == null ? null : updatorId.trim();
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getSubLastIndex() {
        return subLastIndex;
    }

    public void setSubLastIndex(int subLastIndex) {
        this.subLastIndex = subLastIndex;
    }

    public String getFullPathIndex() {
        return fullPathIndex;
    }

    public void setFullPathIndex(String fullPathIndex) {
        this.fullPathIndex = fullPathIndex == null ? null : fullPathIndex.trim();
    }

    public String getFlagUse() {
        return flagUse;
    }

    public void setFlagUse(String flagUse) {
        this.flagUse = flagUse == null ? null : flagUse.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", oid=").append(oid);
        sb.append(", clientOid=").append(clientOid);
        sb.append(", name=").append(name);
        sb.append(", parentOid=").append(parentOid);
        sb.append(", description=").append(description);
        sb.append(", sortOrder=").append(sortOrder);
        sb.append(", creatorId=").append(creatorId);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", updatorId=").append(updatorId);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", subLastIndex=").append(subLastIndex);
        sb.append(", fullPathIndex=").append(fullPathIndex);
        sb.append(", flagUse=").append(flagUse);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    // tree 랜더링 할 때 사용됨.
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private boolean isDisabled;

    public boolean isDisabled(){
        return isDisabled;
    }

    public void setDisabled( boolean disabled ){
        isDisabled = disabled;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String flagApplyHierarchy;

    public String getFlagApplyHierarchy(){
        return flagApplyHierarchy;
    }

    public void setFlagApplyHierarchy( String flagApplyHierarchy ){
        this.flagApplyHierarchy = flagApplyHierarchy;
    }
}
