package com.bandi.domain.kaist;

import java.io.Serializable;
import java.util.Date;

import com.bandi.domain.base.BaseObject;

public class SourceResource extends BaseObject implements Serializable {
    private String clientOid;

    private String oid;

    private String name;

    private String description;

    private String parentOid;

    private int sortOrder;

    private Date createDateTime;

    private String createId;

    private Date deleteDateTime;

    private static final long serialVersionUID = 1L;

    public String getClientOid() {
        return clientOid;
    }

    public void setClientOid(String clientOid) {
        this.clientOid = clientOid == null ? null : clientOid.trim();
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid == null ? null : oid.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getParentOid() {
        return parentOid;
    }

    public void setParentOid(String parentOid) {
        this.parentOid = parentOid == null ? null : parentOid.trim();
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Date getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(Date createDateTime) {
        this.createDateTime = createDateTime;
    }

    public String getCreateId() {
        return createId;
    }

    public void setCreateId(String createId) {
        this.createId = createId == null ? null : createId.trim();
    }

    public Date getDeleteDateTime() {
        return deleteDateTime;
    }

    public void setDeleteDateTime(Date deleteDateTime) {
        this.deleteDateTime = deleteDateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", clientOid=").append(clientOid);
        sb.append(", oid=").append(oid);
        sb.append(", name=").append(name);
        sb.append(", description=").append(description);
        sb.append(", parentOid=").append(parentOid);
        sb.append(", sortOrder=").append(sortOrder);
        sb.append(", createDateTime=").append(createDateTime);
        sb.append(", createId=").append(createId);
        sb.append(", deleteDateTime=").append(deleteDateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    // -------------------End of CodeGen--------------------------

}