package com.bandi.domain.kaist;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class UserDetailCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserDetailCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andKaistUidIsNull() {
            addCriterion("KAISTUID is null");
            return (Criteria) this;
        }

        public Criteria andKaistUidIsNotNull() {
            addCriterion("KAISTUID is not null");
            return (Criteria) this;
        }

        public Criteria andKaistUidEqualTo(String value) {
            addCriterion("KAISTUID =", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotEqualTo(String value) {
            addCriterion("KAISTUID <>", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidGreaterThan(String value) {
            addCriterion("KAISTUID >", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidGreaterThanOrEqualTo(String value) {
            addCriterion("KAISTUID >=", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidLessThan(String value) {
            addCriterion("KAISTUID <", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidLessThanOrEqualTo(String value) {
            addCriterion("KAISTUID <=", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidLike(String value) {
            addCriterion("KAISTUID like", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotLike(String value) {
            addCriterion("KAISTUID not like", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidIn(List<String> values) {
            addCriterion("KAISTUID in", values, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotIn(List<String> values) {
            addCriterion("KAISTUID not in", values, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidBetween(String value1, String value2) {
            addCriterion("KAISTUID between", value1, value2, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotBetween(String value1, String value2) {
            addCriterion("KAISTUID not between", value1, value2, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("USERID is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("USERID is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("USERID =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("USERID <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("USERID >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("USERID >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("USERID <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("USERID <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("USERID like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("USERID not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("USERID in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("USERID not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("USERID between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("USERID not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andKoreanNameIsNull() {
            addCriterion("KOREAN_NAME is null");
            return (Criteria) this;
        }

        public Criteria andKoreanNameIsNotNull() {
            addCriterion("KOREAN_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andKoreanNameEqualTo(String value) {
            addCriterion("KOREAN_NAME =", value, "koreanName");
            return (Criteria) this;
        }

        public Criteria andKoreanNameNotEqualTo(String value) {
            addCriterion("KOREAN_NAME <>", value, "koreanName");
            return (Criteria) this;
        }

        public Criteria andKoreanNameGreaterThan(String value) {
            addCriterion("KOREAN_NAME >", value, "koreanName");
            return (Criteria) this;
        }

        public Criteria andKoreanNameGreaterThanOrEqualTo(String value) {
            addCriterion("KOREAN_NAME >=", value, "koreanName");
            return (Criteria) this;
        }

        public Criteria andKoreanNameLessThan(String value) {
            addCriterion("KOREAN_NAME <", value, "koreanName");
            return (Criteria) this;
        }

        public Criteria andKoreanNameLessThanOrEqualTo(String value) {
            addCriterion("KOREAN_NAME <=", value, "koreanName");
            return (Criteria) this;
        }

        public Criteria andKoreanNameLike(String value) {
            addCriterion("KOREAN_NAME like", value, "koreanName");
            return (Criteria) this;
        }

        public Criteria andKoreanNameNotLike(String value) {
            addCriterion("KOREAN_NAME not like", value, "koreanName");
            return (Criteria) this;
        }

        public Criteria andKoreanNameIn(List<String> values) {
            addCriterion("KOREAN_NAME in", values, "koreanName");
            return (Criteria) this;
        }

        public Criteria andKoreanNameNotIn(List<String> values) {
            addCriterion("KOREAN_NAME not in", values, "koreanName");
            return (Criteria) this;
        }

        public Criteria andKoreanNameBetween(String value1, String value2) {
            addCriterion("KOREAN_NAME between", value1, value2, "koreanName");
            return (Criteria) this;
        }

        public Criteria andKoreanNameNotBetween(String value1, String value2) {
            addCriterion("KOREAN_NAME not between", value1, value2, "koreanName");
            return (Criteria) this;
        }

        public Criteria andEnglishNameIsNull() {
            addCriterion("ENGLISH_NAME is null");
            return (Criteria) this;
        }

        public Criteria andEnglishNameIsNotNull() {
            addCriterion("ENGLISH_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andEnglishNameEqualTo(String value) {
            addCriterion("ENGLISH_NAME =", value, "englishName");
            return (Criteria) this;
        }

        public Criteria andEnglishNameNotEqualTo(String value) {
            addCriterion("ENGLISH_NAME <>", value, "englishName");
            return (Criteria) this;
        }

        public Criteria andEnglishNameGreaterThan(String value) {
            addCriterion("ENGLISH_NAME >", value, "englishName");
            return (Criteria) this;
        }

        public Criteria andEnglishNameGreaterThanOrEqualTo(String value) {
            addCriterion("ENGLISH_NAME >=", value, "englishName");
            return (Criteria) this;
        }

        public Criteria andEnglishNameLessThan(String value) {
            addCriterion("ENGLISH_NAME <", value, "englishName");
            return (Criteria) this;
        }

        public Criteria andEnglishNameLessThanOrEqualTo(String value) {
            addCriterion("ENGLISH_NAME <=", value, "englishName");
            return (Criteria) this;
        }

        public Criteria andEnglishNameLike(String value) {
            addCriterion("ENGLISH_NAME like", value, "englishName");
            return (Criteria) this;
        }

        public Criteria andEnglishNameNotLike(String value) {
            addCriterion("ENGLISH_NAME not like", value, "englishName");
            return (Criteria) this;
        }

        public Criteria andEnglishNameIn(List<String> values) {
            addCriterion("ENGLISH_NAME in", values, "englishName");
            return (Criteria) this;
        }

        public Criteria andEnglishNameNotIn(List<String> values) {
            addCriterion("ENGLISH_NAME not in", values, "englishName");
            return (Criteria) this;
        }

        public Criteria andEnglishNameBetween(String value1, String value2) {
            addCriterion("ENGLISH_NAME between", value1, value2, "englishName");
            return (Criteria) this;
        }

        public Criteria andEnglishNameNotBetween(String value1, String value2) {
            addCriterion("ENGLISH_NAME not between", value1, value2, "englishName");
            return (Criteria) this;
        }

        public Criteria andLastNameIsNull() {
            addCriterion("LAST_NAME is null");
            return (Criteria) this;
        }

        public Criteria andLastNameIsNotNull() {
            addCriterion("LAST_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andLastNameEqualTo(String value) {
            addCriterion("LAST_NAME =", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameNotEqualTo(String value) {
            addCriterion("LAST_NAME <>", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameGreaterThan(String value) {
            addCriterion("LAST_NAME >", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameGreaterThanOrEqualTo(String value) {
            addCriterion("LAST_NAME >=", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameLessThan(String value) {
            addCriterion("LAST_NAME <", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameLessThanOrEqualTo(String value) {
            addCriterion("LAST_NAME <=", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameLike(String value) {
            addCriterion("LAST_NAME like", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameNotLike(String value) {
            addCriterion("LAST_NAME not like", value, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameIn(List<String> values) {
            addCriterion("LAST_NAME in", values, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameNotIn(List<String> values) {
            addCriterion("LAST_NAME not in", values, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameBetween(String value1, String value2) {
            addCriterion("LAST_NAME between", value1, value2, "lastName");
            return (Criteria) this;
        }

        public Criteria andLastNameNotBetween(String value1, String value2) {
            addCriterion("LAST_NAME not between", value1, value2, "lastName");
            return (Criteria) this;
        }

        public Criteria andFirstNameIsNull() {
            addCriterion("FIRST_NAME is null");
            return (Criteria) this;
        }

        public Criteria andFirstNameIsNotNull() {
            addCriterion("FIRST_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andFirstNameEqualTo(String value) {
            addCriterion("FIRST_NAME =", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameNotEqualTo(String value) {
            addCriterion("FIRST_NAME <>", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameGreaterThan(String value) {
            addCriterion("FIRST_NAME >", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameGreaterThanOrEqualTo(String value) {
            addCriterion("FIRST_NAME >=", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameLessThan(String value) {
            addCriterion("FIRST_NAME <", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameLessThanOrEqualTo(String value) {
            addCriterion("FIRST_NAME <=", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameLike(String value) {
            addCriterion("FIRST_NAME like", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameNotLike(String value) {
            addCriterion("FIRST_NAME not like", value, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameIn(List<String> values) {
            addCriterion("FIRST_NAME in", values, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameNotIn(List<String> values) {
            addCriterion("FIRST_NAME not in", values, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameBetween(String value1, String value2) {
            addCriterion("FIRST_NAME between", value1, value2, "firstName");
            return (Criteria) this;
        }

        public Criteria andFirstNameNotBetween(String value1, String value2) {
            addCriterion("FIRST_NAME not between", value1, value2, "firstName");
            return (Criteria) this;
        }

        public Criteria andBirthdayIsNull() {
            addCriterion("BIRTHDAY is null");
            return (Criteria) this;
        }

        public Criteria andBirthdayIsNotNull() {
            addCriterion("BIRTHDAY is not null");
            return (Criteria) this;
        }

        public Criteria andBirthdayEqualTo(Date value) {
            addCriterion("BIRTHDAY =", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayNotEqualTo(Date value) {
            addCriterion("BIRTHDAY <>", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayGreaterThan(Date value) {
            addCriterion("BIRTHDAY >", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayGreaterThanOrEqualTo(Date value) {
            addCriterion("BIRTHDAY >=", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayLessThan(Date value) {
            addCriterion("BIRTHDAY <", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayLessThanOrEqualTo(Date value) {
            addCriterion("BIRTHDAY <=", value, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayIn(List<Date> values) {
            addCriterion("BIRTHDAY in", values, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayNotIn(List<Date> values) {
            addCriterion("BIRTHDAY not in", values, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayBetween(Date value1, Date value2) {
            addCriterion("BIRTHDAY between", value1, value2, "birthday");
            return (Criteria) this;
        }

        public Criteria andBirthdayNotBetween(Date value1, Date value2) {
            addCriterion("BIRTHDAY not between", value1, value2, "birthday");
            return (Criteria) this;
        }

        public Criteria andNationCodeUidIsNull() {
            addCriterion("NATION_CODE_UID is null");
            return (Criteria) this;
        }

        public Criteria andNationCodeUidIsNotNull() {
            addCriterion("NATION_CODE_UID is not null");
            return (Criteria) this;
        }

        public Criteria andNationCodeUidEqualTo(String value) {
            addCriterion("NATION_CODE_UID =", value, "nationCodeUid");
            return (Criteria) this;
        }

        public Criteria andNationCodeUidNotEqualTo(String value) {
            addCriterion("NATION_CODE_UID <>", value, "nationCodeUid");
            return (Criteria) this;
        }

        public Criteria andNationCodeUidGreaterThan(String value) {
            addCriterion("NATION_CODE_UID >", value, "nationCodeUid");
            return (Criteria) this;
        }

        public Criteria andNationCodeUidGreaterThanOrEqualTo(String value) {
            addCriterion("NATION_CODE_UID >=", value, "nationCodeUid");
            return (Criteria) this;
        }

        public Criteria andNationCodeUidLessThan(String value) {
            addCriterion("NATION_CODE_UID <", value, "nationCodeUid");
            return (Criteria) this;
        }

        public Criteria andNationCodeUidLessThanOrEqualTo(String value) {
            addCriterion("NATION_CODE_UID <=", value, "nationCodeUid");
            return (Criteria) this;
        }

        public Criteria andNationCodeUidLike(String value) {
            addCriterion("NATION_CODE_UID like", value, "nationCodeUid");
            return (Criteria) this;
        }

        public Criteria andNationCodeUidNotLike(String value) {
            addCriterion("NATION_CODE_UID not like", value, "nationCodeUid");
            return (Criteria) this;
        }

        public Criteria andNationCodeUidIn(List<String> values) {
            addCriterion("NATION_CODE_UID in", values, "nationCodeUid");
            return (Criteria) this;
        }

        public Criteria andNationCodeUidNotIn(List<String> values) {
            addCriterion("NATION_CODE_UID not in", values, "nationCodeUid");
            return (Criteria) this;
        }

        public Criteria andNationCodeUidBetween(String value1, String value2) {
            addCriterion("NATION_CODE_UID between", value1, value2, "nationCodeUid");
            return (Criteria) this;
        }

        public Criteria andNationCodeUidNotBetween(String value1, String value2) {
            addCriterion("NATION_CODE_UID not between", value1, value2, "nationCodeUid");
            return (Criteria) this;
        }

        public Criteria andSexCodeUidIsNull() {
            addCriterion("SEX_CODE_UID is null");
            return (Criteria) this;
        }

        public Criteria andSexCodeUidIsNotNull() {
            addCriterion("SEX_CODE_UID is not null");
            return (Criteria) this;
        }

        public Criteria andSexCodeUidEqualTo(String value) {
            addCriterion("SEX_CODE_UID =", value, "sexCodeUid");
            return (Criteria) this;
        }

        public Criteria andSexCodeUidNotEqualTo(String value) {
            addCriterion("SEX_CODE_UID <>", value, "sexCodeUid");
            return (Criteria) this;
        }

        public Criteria andSexCodeUidGreaterThan(String value) {
            addCriterion("SEX_CODE_UID >", value, "sexCodeUid");
            return (Criteria) this;
        }

        public Criteria andSexCodeUidGreaterThanOrEqualTo(String value) {
            addCriterion("SEX_CODE_UID >=", value, "sexCodeUid");
            return (Criteria) this;
        }

        public Criteria andSexCodeUidLessThan(String value) {
            addCriterion("SEX_CODE_UID <", value, "sexCodeUid");
            return (Criteria) this;
        }

        public Criteria andSexCodeUidLessThanOrEqualTo(String value) {
            addCriterion("SEX_CODE_UID <=", value, "sexCodeUid");
            return (Criteria) this;
        }

        public Criteria andSexCodeUidLike(String value) {
            addCriterion("SEX_CODE_UID like", value, "sexCodeUid");
            return (Criteria) this;
        }

        public Criteria andSexCodeUidNotLike(String value) {
            addCriterion("SEX_CODE_UID not like", value, "sexCodeUid");
            return (Criteria) this;
        }

        public Criteria andSexCodeUidIn(List<String> values) {
            addCriterion("SEX_CODE_UID in", values, "sexCodeUid");
            return (Criteria) this;
        }

        public Criteria andSexCodeUidNotIn(List<String> values) {
            addCriterion("SEX_CODE_UID not in", values, "sexCodeUid");
            return (Criteria) this;
        }

        public Criteria andSexCodeUidBetween(String value1, String value2) {
            addCriterion("SEX_CODE_UID between", value1, value2, "sexCodeUid");
            return (Criteria) this;
        }

        public Criteria andSexCodeUidNotBetween(String value1, String value2) {
            addCriterion("SEX_CODE_UID not between", value1, value2, "sexCodeUid");
            return (Criteria) this;
        }

        public Criteria andEmailAddressIsNull() {
            addCriterion("EMAIL_ADDRESS is null");
            return (Criteria) this;
        }

        public Criteria andEmailAddressIsNotNull() {
            addCriterion("EMAIL_ADDRESS is not null");
            return (Criteria) this;
        }

        public Criteria andEmailAddressEqualTo(String value) {
            addCriterion("EMAIL_ADDRESS =", value, "emailAddress");
            return (Criteria) this;
        }

        public Criteria andEmailAddressNotEqualTo(String value) {
            addCriterion("EMAIL_ADDRESS <>", value, "emailAddress");
            return (Criteria) this;
        }

        public Criteria andEmailAddressGreaterThan(String value) {
            addCriterion("EMAIL_ADDRESS >", value, "emailAddress");
            return (Criteria) this;
        }

        public Criteria andEmailAddressGreaterThanOrEqualTo(String value) {
            addCriterion("EMAIL_ADDRESS >=", value, "emailAddress");
            return (Criteria) this;
        }

        public Criteria andEmailAddressLessThan(String value) {
            addCriterion("EMAIL_ADDRESS <", value, "emailAddress");
            return (Criteria) this;
        }

        public Criteria andEmailAddressLessThanOrEqualTo(String value) {
            addCriterion("EMAIL_ADDRESS <=", value, "emailAddress");
            return (Criteria) this;
        }

        public Criteria andEmailAddressLike(String value) {
            addCriterion("EMAIL_ADDRESS like", value, "emailAddress");
            return (Criteria) this;
        }

        public Criteria andEmailAddressNotLike(String value) {
            addCriterion("EMAIL_ADDRESS not like", value, "emailAddress");
            return (Criteria) this;
        }

        public Criteria andEmailAddressIn(List<String> values) {
            addCriterion("EMAIL_ADDRESS in", values, "emailAddress");
            return (Criteria) this;
        }

        public Criteria andEmailAddressNotIn(List<String> values) {
            addCriterion("EMAIL_ADDRESS not in", values, "emailAddress");
            return (Criteria) this;
        }

        public Criteria andEmailAddressBetween(String value1, String value2) {
            addCriterion("EMAIL_ADDRESS between", value1, value2, "emailAddress");
            return (Criteria) this;
        }

        public Criteria andEmailAddressNotBetween(String value1, String value2) {
            addCriterion("EMAIL_ADDRESS not between", value1, value2, "emailAddress");
            return (Criteria) this;
        }

        public Criteria andChMailIsNull() {
            addCriterion("CH_MAIL is null");
            return (Criteria) this;
        }

        public Criteria andChMailIsNotNull() {
            addCriterion("CH_MAIL is not null");
            return (Criteria) this;
        }

        public Criteria andChMailEqualTo(String value) {
            addCriterion("CH_MAIL =", value, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailNotEqualTo(String value) {
            addCriterion("CH_MAIL <>", value, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailGreaterThan(String value) {
            addCriterion("CH_MAIL >", value, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailGreaterThanOrEqualTo(String value) {
            addCriterion("CH_MAIL >=", value, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailLessThan(String value) {
            addCriterion("CH_MAIL <", value, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailLessThanOrEqualTo(String value) {
            addCriterion("CH_MAIL <=", value, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailLike(String value) {
            addCriterion("CH_MAIL like", value, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailNotLike(String value) {
            addCriterion("CH_MAIL not like", value, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailIn(List<String> values) {
            addCriterion("CH_MAIL in", values, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailNotIn(List<String> values) {
            addCriterion("CH_MAIL not in", values, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailBetween(String value1, String value2) {
            addCriterion("CH_MAIL between", value1, value2, "chMail");
            return (Criteria) this;
        }

        public Criteria andChMailNotBetween(String value1, String value2) {
            addCriterion("CH_MAIL not between", value1, value2, "chMail");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberIsNull() {
            addCriterion("MOBILE_TELEPHONE_NUMBER is null");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberIsNotNull() {
            addCriterion("MOBILE_TELEPHONE_NUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberEqualTo(String value) {
            addCriterion("MOBILE_TELEPHONE_NUMBER =", value, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberNotEqualTo(String value) {
            addCriterion("MOBILE_TELEPHONE_NUMBER <>", value, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberGreaterThan(String value) {
            addCriterion("MOBILE_TELEPHONE_NUMBER >", value, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberGreaterThanOrEqualTo(String value) {
            addCriterion("MOBILE_TELEPHONE_NUMBER >=", value, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberLessThan(String value) {
            addCriterion("MOBILE_TELEPHONE_NUMBER <", value, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberLessThanOrEqualTo(String value) {
            addCriterion("MOBILE_TELEPHONE_NUMBER <=", value, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberLike(String value) {
            addCriterion("MOBILE_TELEPHONE_NUMBER like", value, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberNotLike(String value) {
            addCriterion("MOBILE_TELEPHONE_NUMBER not like", value, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberIn(List<String> values) {
            addCriterion("MOBILE_TELEPHONE_NUMBER in", values, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberNotIn(List<String> values) {
            addCriterion("MOBILE_TELEPHONE_NUMBER not in", values, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberBetween(String value1, String value2) {
            addCriterion("MOBILE_TELEPHONE_NUMBER between", value1, value2, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberNotBetween(String value1, String value2) {
            addCriterion("MOBILE_TELEPHONE_NUMBER not between", value1, value2, "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOfficeTelephoneNumberIsNull() {
            addCriterion("OFFICE_TELEPHONE_NUMBER is null");
            return (Criteria) this;
        }

        public Criteria andOfficeTelephoneNumberIsNotNull() {
            addCriterion("OFFICE_TELEPHONE_NUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andOfficeTelephoneNumberEqualTo(String value) {
            addCriterion("OFFICE_TELEPHONE_NUMBER =", value, "officeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOfficeTelephoneNumberNotEqualTo(String value) {
            addCriterion("OFFICE_TELEPHONE_NUMBER <>", value, "officeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOfficeTelephoneNumberGreaterThan(String value) {
            addCriterion("OFFICE_TELEPHONE_NUMBER >", value, "officeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOfficeTelephoneNumberGreaterThanOrEqualTo(String value) {
            addCriterion("OFFICE_TELEPHONE_NUMBER >=", value, "officeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOfficeTelephoneNumberLessThan(String value) {
            addCriterion("OFFICE_TELEPHONE_NUMBER <", value, "officeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOfficeTelephoneNumberLessThanOrEqualTo(String value) {
            addCriterion("OFFICE_TELEPHONE_NUMBER <=", value, "officeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOfficeTelephoneNumberLike(String value) {
            addCriterion("OFFICE_TELEPHONE_NUMBER like", value, "officeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOfficeTelephoneNumberNotLike(String value) {
            addCriterion("OFFICE_TELEPHONE_NUMBER not like", value, "officeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOfficeTelephoneNumberIn(List<String> values) {
            addCriterion("OFFICE_TELEPHONE_NUMBER in", values, "officeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOfficeTelephoneNumberNotIn(List<String> values) {
            addCriterion("OFFICE_TELEPHONE_NUMBER not in", values, "officeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOfficeTelephoneNumberBetween(String value1, String value2) {
            addCriterion("OFFICE_TELEPHONE_NUMBER between", value1, value2, "officeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOfficeTelephoneNumberNotBetween(String value1, String value2) {
            addCriterion("OFFICE_TELEPHONE_NUMBER not between", value1, value2, "officeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOweHomeTelephoneNumberIsNull() {
            addCriterion("OWE_HOME_TELEPHONE_NUMBER is null");
            return (Criteria) this;
        }

        public Criteria andOweHomeTelephoneNumberIsNotNull() {
            addCriterion("OWE_HOME_TELEPHONE_NUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andOweHomeTelephoneNumberEqualTo(String value) {
            addCriterion("OWE_HOME_TELEPHONE_NUMBER =", value, "oweHomeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOweHomeTelephoneNumberNotEqualTo(String value) {
            addCriterion("OWE_HOME_TELEPHONE_NUMBER <>", value, "oweHomeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOweHomeTelephoneNumberGreaterThan(String value) {
            addCriterion("OWE_HOME_TELEPHONE_NUMBER >", value, "oweHomeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOweHomeTelephoneNumberGreaterThanOrEqualTo(String value) {
            addCriterion("OWE_HOME_TELEPHONE_NUMBER >=", value, "oweHomeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOweHomeTelephoneNumberLessThan(String value) {
            addCriterion("OWE_HOME_TELEPHONE_NUMBER <", value, "oweHomeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOweHomeTelephoneNumberLessThanOrEqualTo(String value) {
            addCriterion("OWE_HOME_TELEPHONE_NUMBER <=", value, "oweHomeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOweHomeTelephoneNumberLike(String value) {
            addCriterion("OWE_HOME_TELEPHONE_NUMBER like", value, "oweHomeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOweHomeTelephoneNumberNotLike(String value) {
            addCriterion("OWE_HOME_TELEPHONE_NUMBER not like", value, "oweHomeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOweHomeTelephoneNumberIn(List<String> values) {
            addCriterion("OWE_HOME_TELEPHONE_NUMBER in", values, "oweHomeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOweHomeTelephoneNumberNotIn(List<String> values) {
            addCriterion("OWE_HOME_TELEPHONE_NUMBER not in", values, "oweHomeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOweHomeTelephoneNumberBetween(String value1, String value2) {
            addCriterion("OWE_HOME_TELEPHONE_NUMBER between", value1, value2, "oweHomeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOweHomeTelephoneNumberNotBetween(String value1, String value2) {
            addCriterion("OWE_HOME_TELEPHONE_NUMBER not between", value1, value2, "oweHomeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andFaxTelephoneNumberIsNull() {
            addCriterion("FAX_TELEPHONE_NUMBER is null");
            return (Criteria) this;
        }

        public Criteria andFaxTelephoneNumberIsNotNull() {
            addCriterion("FAX_TELEPHONE_NUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andFaxTelephoneNumberEqualTo(String value) {
            addCriterion("FAX_TELEPHONE_NUMBER =", value, "faxTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andFaxTelephoneNumberNotEqualTo(String value) {
            addCriterion("FAX_TELEPHONE_NUMBER <>", value, "faxTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andFaxTelephoneNumberGreaterThan(String value) {
            addCriterion("FAX_TELEPHONE_NUMBER >", value, "faxTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andFaxTelephoneNumberGreaterThanOrEqualTo(String value) {
            addCriterion("FAX_TELEPHONE_NUMBER >=", value, "faxTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andFaxTelephoneNumberLessThan(String value) {
            addCriterion("FAX_TELEPHONE_NUMBER <", value, "faxTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andFaxTelephoneNumberLessThanOrEqualTo(String value) {
            addCriterion("FAX_TELEPHONE_NUMBER <=", value, "faxTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andFaxTelephoneNumberLike(String value) {
            addCriterion("FAX_TELEPHONE_NUMBER like", value, "faxTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andFaxTelephoneNumberNotLike(String value) {
            addCriterion("FAX_TELEPHONE_NUMBER not like", value, "faxTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andFaxTelephoneNumberIn(List<String> values) {
            addCriterion("FAX_TELEPHONE_NUMBER in", values, "faxTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andFaxTelephoneNumberNotIn(List<String> values) {
            addCriterion("FAX_TELEPHONE_NUMBER not in", values, "faxTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andFaxTelephoneNumberBetween(String value1, String value2) {
            addCriterion("FAX_TELEPHONE_NUMBER between", value1, value2, "faxTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andFaxTelephoneNumberNotBetween(String value1, String value2) {
            addCriterion("FAX_TELEPHONE_NUMBER not between", value1, value2, "faxTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andPostNumberIsNull() {
            addCriterion("POST_NUMBER is null");
            return (Criteria) this;
        }

        public Criteria andPostNumberIsNotNull() {
            addCriterion("POST_NUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andPostNumberEqualTo(String value) {
            addCriterion("POST_NUMBER =", value, "postNumber");
            return (Criteria) this;
        }

        public Criteria andPostNumberNotEqualTo(String value) {
            addCriterion("POST_NUMBER <>", value, "postNumber");
            return (Criteria) this;
        }

        public Criteria andPostNumberGreaterThan(String value) {
            addCriterion("POST_NUMBER >", value, "postNumber");
            return (Criteria) this;
        }

        public Criteria andPostNumberGreaterThanOrEqualTo(String value) {
            addCriterion("POST_NUMBER >=", value, "postNumber");
            return (Criteria) this;
        }

        public Criteria andPostNumberLessThan(String value) {
            addCriterion("POST_NUMBER <", value, "postNumber");
            return (Criteria) this;
        }

        public Criteria andPostNumberLessThanOrEqualTo(String value) {
            addCriterion("POST_NUMBER <=", value, "postNumber");
            return (Criteria) this;
        }

        public Criteria andPostNumberLike(String value) {
            addCriterion("POST_NUMBER like", value, "postNumber");
            return (Criteria) this;
        }

        public Criteria andPostNumberNotLike(String value) {
            addCriterion("POST_NUMBER not like", value, "postNumber");
            return (Criteria) this;
        }

        public Criteria andPostNumberIn(List<String> values) {
            addCriterion("POST_NUMBER in", values, "postNumber");
            return (Criteria) this;
        }

        public Criteria andPostNumberNotIn(List<String> values) {
            addCriterion("POST_NUMBER not in", values, "postNumber");
            return (Criteria) this;
        }

        public Criteria andPostNumberBetween(String value1, String value2) {
            addCriterion("POST_NUMBER between", value1, value2, "postNumber");
            return (Criteria) this;
        }

        public Criteria andPostNumberNotBetween(String value1, String value2) {
            addCriterion("POST_NUMBER not between", value1, value2, "postNumber");
            return (Criteria) this;
        }

        public Criteria andAddressIsNull() {
            addCriterion("ADDRESS is null");
            return (Criteria) this;
        }

        public Criteria andAddressIsNotNull() {
            addCriterion("ADDRESS is not null");
            return (Criteria) this;
        }

        public Criteria andAddressEqualTo(String value) {
            addCriterion("ADDRESS =", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotEqualTo(String value) {
            addCriterion("ADDRESS <>", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThan(String value) {
            addCriterion("ADDRESS >", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThanOrEqualTo(String value) {
            addCriterion("ADDRESS >=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThan(String value) {
            addCriterion("ADDRESS <", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThanOrEqualTo(String value) {
            addCriterion("ADDRESS <=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLike(String value) {
            addCriterion("ADDRESS like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotLike(String value) {
            addCriterion("ADDRESS not like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressIn(List<String> values) {
            addCriterion("ADDRESS in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotIn(List<String> values) {
            addCriterion("ADDRESS not in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressBetween(String value1, String value2) {
            addCriterion("ADDRESS between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotBetween(String value1, String value2) {
            addCriterion("ADDRESS not between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andAddressDetailIsNull() {
            addCriterion("ADDRESS_DETAIL is null");
            return (Criteria) this;
        }

        public Criteria andAddressDetailIsNotNull() {
            addCriterion("ADDRESS_DETAIL is not null");
            return (Criteria) this;
        }

        public Criteria andAddressDetailEqualTo(String value) {
            addCriterion("ADDRESS_DETAIL =", value, "addressDetail");
            return (Criteria) this;
        }

        public Criteria andAddressDetailNotEqualTo(String value) {
            addCriterion("ADDRESS_DETAIL <>", value, "addressDetail");
            return (Criteria) this;
        }

        public Criteria andAddressDetailGreaterThan(String value) {
            addCriterion("ADDRESS_DETAIL >", value, "addressDetail");
            return (Criteria) this;
        }

        public Criteria andAddressDetailGreaterThanOrEqualTo(String value) {
            addCriterion("ADDRESS_DETAIL >=", value, "addressDetail");
            return (Criteria) this;
        }

        public Criteria andAddressDetailLessThan(String value) {
            addCriterion("ADDRESS_DETAIL <", value, "addressDetail");
            return (Criteria) this;
        }

        public Criteria andAddressDetailLessThanOrEqualTo(String value) {
            addCriterion("ADDRESS_DETAIL <=", value, "addressDetail");
            return (Criteria) this;
        }

        public Criteria andAddressDetailLike(String value) {
            addCriterion("ADDRESS_DETAIL like", value, "addressDetail");
            return (Criteria) this;
        }

        public Criteria andAddressDetailNotLike(String value) {
            addCriterion("ADDRESS_DETAIL not like", value, "addressDetail");
            return (Criteria) this;
        }

        public Criteria andAddressDetailIn(List<String> values) {
            addCriterion("ADDRESS_DETAIL in", values, "addressDetail");
            return (Criteria) this;
        }

        public Criteria andAddressDetailNotIn(List<String> values) {
            addCriterion("ADDRESS_DETAIL not in", values, "addressDetail");
            return (Criteria) this;
        }

        public Criteria andAddressDetailBetween(String value1, String value2) {
            addCriterion("ADDRESS_DETAIL between", value1, value2, "addressDetail");
            return (Criteria) this;
        }

        public Criteria andAddressDetailNotBetween(String value1, String value2) {
            addCriterion("ADDRESS_DETAIL not between", value1, value2, "addressDetail");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNull() {
            addCriterion("PERSON_ID is null");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNotNull() {
            addCriterion("PERSON_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPersonIdEqualTo(String value) {
            addCriterion("PERSON_ID =", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotEqualTo(String value) {
            addCriterion("PERSON_ID <>", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThan(String value) {
            addCriterion("PERSON_ID >", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThanOrEqualTo(String value) {
            addCriterion("PERSON_ID >=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThan(String value) {
            addCriterion("PERSON_ID <", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThanOrEqualTo(String value) {
            addCriterion("PERSON_ID <=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLike(String value) {
            addCriterion("PERSON_ID like", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotLike(String value) {
            addCriterion("PERSON_ID not like", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIn(List<String> values) {
            addCriterion("PERSON_ID in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotIn(List<String> values) {
            addCriterion("PERSON_ID not in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdBetween(String value1, String value2) {
            addCriterion("PERSON_ID between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotBetween(String value1, String value2) {
            addCriterion("PERSON_ID not between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIsNull() {
            addCriterion("EMPLOYEE_NUMBER is null");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIsNotNull() {
            addCriterion("EMPLOYEE_NUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER =", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER <>", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberGreaterThan(String value) {
            addCriterion("EMPLOYEE_NUMBER >", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberGreaterThanOrEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER >=", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLessThan(String value) {
            addCriterion("EMPLOYEE_NUMBER <", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLessThanOrEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER <=", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLike(String value) {
            addCriterion("EMPLOYEE_NUMBER like", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotLike(String value) {
            addCriterion("EMPLOYEE_NUMBER not like", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIn(List<String> values) {
            addCriterion("EMPLOYEE_NUMBER in", values, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotIn(List<String> values) {
            addCriterion("EMPLOYEE_NUMBER not in", values, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberBetween(String value1, String value2) {
            addCriterion("EMPLOYEE_NUMBER between", value1, value2, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotBetween(String value1, String value2) {
            addCriterion("EMPLOYEE_NUMBER not between", value1, value2, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andStdNoIsNull() {
            addCriterion("STD_NO is null");
            return (Criteria) this;
        }

        public Criteria andStdNoIsNotNull() {
            addCriterion("STD_NO is not null");
            return (Criteria) this;
        }

        public Criteria andStdNoEqualTo(String value) {
            addCriterion("STD_NO =", value, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoNotEqualTo(String value) {
            addCriterion("STD_NO <>", value, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoGreaterThan(String value) {
            addCriterion("STD_NO >", value, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoGreaterThanOrEqualTo(String value) {
            addCriterion("STD_NO >=", value, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoLessThan(String value) {
            addCriterion("STD_NO <", value, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoLessThanOrEqualTo(String value) {
            addCriterion("STD_NO <=", value, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoLike(String value) {
            addCriterion("STD_NO like", value, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoNotLike(String value) {
            addCriterion("STD_NO not like", value, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoIn(List<String> values) {
            addCriterion("STD_NO in", values, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoNotIn(List<String> values) {
            addCriterion("STD_NO not in", values, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoBetween(String value1, String value2) {
            addCriterion("STD_NO between", value1, value2, "stdNo");
            return (Criteria) this;
        }

        public Criteria andStdNoNotBetween(String value1, String value2) {
            addCriterion("STD_NO not between", value1, value2, "stdNo");
            return (Criteria) this;
        }

        public Criteria andAcadOrgIsNull() {
            addCriterion("ACAD_ORG is null");
            return (Criteria) this;
        }

        public Criteria andAcadOrgIsNotNull() {
            addCriterion("ACAD_ORG is not null");
            return (Criteria) this;
        }

        public Criteria andAcadOrgEqualTo(String value) {
            addCriterion("ACAD_ORG =", value, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgNotEqualTo(String value) {
            addCriterion("ACAD_ORG <>", value, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgGreaterThan(String value) {
            addCriterion("ACAD_ORG >", value, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_ORG >=", value, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgLessThan(String value) {
            addCriterion("ACAD_ORG <", value, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgLessThanOrEqualTo(String value) {
            addCriterion("ACAD_ORG <=", value, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgLike(String value) {
            addCriterion("ACAD_ORG like", value, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgNotLike(String value) {
            addCriterion("ACAD_ORG not like", value, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgIn(List<String> values) {
            addCriterion("ACAD_ORG in", values, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgNotIn(List<String> values) {
            addCriterion("ACAD_ORG not in", values, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgBetween(String value1, String value2) {
            addCriterion("ACAD_ORG between", value1, value2, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadOrgNotBetween(String value1, String value2) {
            addCriterion("ACAD_ORG not between", value1, value2, "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadNameIsNull() {
            addCriterion("ACAD_NAME is null");
            return (Criteria) this;
        }

        public Criteria andAcadNameIsNotNull() {
            addCriterion("ACAD_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andAcadNameEqualTo(String value) {
            addCriterion("ACAD_NAME =", value, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameNotEqualTo(String value) {
            addCriterion("ACAD_NAME <>", value, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameGreaterThan(String value) {
            addCriterion("ACAD_NAME >", value, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_NAME >=", value, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameLessThan(String value) {
            addCriterion("ACAD_NAME <", value, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameLessThanOrEqualTo(String value) {
            addCriterion("ACAD_NAME <=", value, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameLike(String value) {
            addCriterion("ACAD_NAME like", value, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameNotLike(String value) {
            addCriterion("ACAD_NAME not like", value, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameIn(List<String> values) {
            addCriterion("ACAD_NAME in", values, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameNotIn(List<String> values) {
            addCriterion("ACAD_NAME not in", values, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameBetween(String value1, String value2) {
            addCriterion("ACAD_NAME between", value1, value2, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadNameNotBetween(String value1, String value2) {
            addCriterion("ACAD_NAME not between", value1, value2, "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdIsNull() {
            addCriterion("ACAD_KST_ORG_ID is null");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdIsNotNull() {
            addCriterion("ACAD_KST_ORG_ID is not null");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdEqualTo(String value) {
            addCriterion("ACAD_KST_ORG_ID =", value, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdNotEqualTo(String value) {
            addCriterion("ACAD_KST_ORG_ID <>", value, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdGreaterThan(String value) {
            addCriterion("ACAD_KST_ORG_ID >", value, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_KST_ORG_ID >=", value, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdLessThan(String value) {
            addCriterion("ACAD_KST_ORG_ID <", value, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdLessThanOrEqualTo(String value) {
            addCriterion("ACAD_KST_ORG_ID <=", value, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdLike(String value) {
            addCriterion("ACAD_KST_ORG_ID like", value, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdNotLike(String value) {
            addCriterion("ACAD_KST_ORG_ID not like", value, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdIn(List<String> values) {
            addCriterion("ACAD_KST_ORG_ID in", values, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdNotIn(List<String> values) {
            addCriterion("ACAD_KST_ORG_ID not in", values, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdBetween(String value1, String value2) {
            addCriterion("ACAD_KST_ORG_ID between", value1, value2, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdNotBetween(String value1, String value2) {
            addCriterion("ACAD_KST_ORG_ID not between", value1, value2, "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdIsNull() {
            addCriterion("ACAD_EBS_ORG_ID is null");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdIsNotNull() {
            addCriterion("ACAD_EBS_ORG_ID is not null");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_ID =", value, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdNotEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_ID <>", value, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdGreaterThan(String value) {
            addCriterion("ACAD_EBS_ORG_ID >", value, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_ID >=", value, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdLessThan(String value) {
            addCriterion("ACAD_EBS_ORG_ID <", value, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdLessThanOrEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_ID <=", value, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdLike(String value) {
            addCriterion("ACAD_EBS_ORG_ID like", value, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdNotLike(String value) {
            addCriterion("ACAD_EBS_ORG_ID not like", value, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdIn(List<String> values) {
            addCriterion("ACAD_EBS_ORG_ID in", values, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdNotIn(List<String> values) {
            addCriterion("ACAD_EBS_ORG_ID not in", values, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdBetween(String value1, String value2) {
            addCriterion("ACAD_EBS_ORG_ID between", value1, value2, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdNotBetween(String value1, String value2) {
            addCriterion("ACAD_EBS_ORG_ID not between", value1, value2, "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngIsNull() {
            addCriterion("ACAD_EBS_ORG_NAME_ENG is null");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngIsNotNull() {
            addCriterion("ACAD_EBS_ORG_NAME_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG =", value, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngNotEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG <>", value, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngGreaterThan(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG >", value, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG >=", value, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngLessThan(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG <", value, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngLessThanOrEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG <=", value, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngLike(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG like", value, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngNotLike(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG not like", value, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngIn(List<String> values) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG in", values, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngNotIn(List<String> values) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG not in", values, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngBetween(String value1, String value2) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG between", value1, value2, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngNotBetween(String value1, String value2) {
            addCriterion("ACAD_EBS_ORG_NAME_ENG not between", value1, value2, "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorIsNull() {
            addCriterion("ACAD_EBS_ORG_NAME_KOR is null");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorIsNotNull() {
            addCriterion("ACAD_EBS_ORG_NAME_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR =", value, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorNotEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR <>", value, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorGreaterThan(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR >", value, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR >=", value, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorLessThan(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR <", value, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorLessThanOrEqualTo(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR <=", value, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorLike(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR like", value, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorNotLike(String value) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR not like", value, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorIn(List<String> values) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR in", values, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorNotIn(List<String> values) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR not in", values, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorBetween(String value1, String value2) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR between", value1, value2, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorNotBetween(String value1, String value2) {
            addCriterion("ACAD_EBS_ORG_NAME_KOR not between", value1, value2, "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andCampusUidIsNull() {
            addCriterion("CAMPUS_UID is null");
            return (Criteria) this;
        }

        public Criteria andCampusUidIsNotNull() {
            addCriterion("CAMPUS_UID is not null");
            return (Criteria) this;
        }

        public Criteria andCampusUidEqualTo(String value) {
            addCriterion("CAMPUS_UID =", value, "campusUid");
            return (Criteria) this;
        }

        public Criteria andCampusUidNotEqualTo(String value) {
            addCriterion("CAMPUS_UID <>", value, "campusUid");
            return (Criteria) this;
        }

        public Criteria andCampusUidGreaterThan(String value) {
            addCriterion("CAMPUS_UID >", value, "campusUid");
            return (Criteria) this;
        }

        public Criteria andCampusUidGreaterThanOrEqualTo(String value) {
            addCriterion("CAMPUS_UID >=", value, "campusUid");
            return (Criteria) this;
        }

        public Criteria andCampusUidLessThan(String value) {
            addCriterion("CAMPUS_UID <", value, "campusUid");
            return (Criteria) this;
        }

        public Criteria andCampusUidLessThanOrEqualTo(String value) {
            addCriterion("CAMPUS_UID <=", value, "campusUid");
            return (Criteria) this;
        }

        public Criteria andCampusUidLike(String value) {
            addCriterion("CAMPUS_UID like", value, "campusUid");
            return (Criteria) this;
        }

        public Criteria andCampusUidNotLike(String value) {
            addCriterion("CAMPUS_UID not like", value, "campusUid");
            return (Criteria) this;
        }

        public Criteria andCampusUidIn(List<String> values) {
            addCriterion("CAMPUS_UID in", values, "campusUid");
            return (Criteria) this;
        }

        public Criteria andCampusUidNotIn(List<String> values) {
            addCriterion("CAMPUS_UID not in", values, "campusUid");
            return (Criteria) this;
        }

        public Criteria andCampusUidBetween(String value1, String value2) {
            addCriterion("CAMPUS_UID between", value1, value2, "campusUid");
            return (Criteria) this;
        }

        public Criteria andCampusUidNotBetween(String value1, String value2) {
            addCriterion("CAMPUS_UID not between", value1, value2, "campusUid");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdIsNull() {
            addCriterion("EBS_ORGANIZATION_ID is null");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdIsNotNull() {
            addCriterion("EBS_ORGANIZATION_ID is not null");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdEqualTo(String value) {
            addCriterion("EBS_ORGANIZATION_ID =", value, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdNotEqualTo(String value) {
            addCriterion("EBS_ORGANIZATION_ID <>", value, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdGreaterThan(String value) {
            addCriterion("EBS_ORGANIZATION_ID >", value, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_ORGANIZATION_ID >=", value, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdLessThan(String value) {
            addCriterion("EBS_ORGANIZATION_ID <", value, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdLessThanOrEqualTo(String value) {
            addCriterion("EBS_ORGANIZATION_ID <=", value, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdLike(String value) {
            addCriterion("EBS_ORGANIZATION_ID like", value, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdNotLike(String value) {
            addCriterion("EBS_ORGANIZATION_ID not like", value, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdIn(List<String> values) {
            addCriterion("EBS_ORGANIZATION_ID in", values, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdNotIn(List<String> values) {
            addCriterion("EBS_ORGANIZATION_ID not in", values, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdBetween(String value1, String value2) {
            addCriterion("EBS_ORGANIZATION_ID between", value1, value2, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdNotBetween(String value1, String value2) {
            addCriterion("EBS_ORGANIZATION_ID not between", value1, value2, "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngIsNull() {
            addCriterion("EBS_ORG_NAME_ENG is null");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngIsNotNull() {
            addCriterion("EBS_ORG_NAME_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngEqualTo(String value) {
            addCriterion("EBS_ORG_NAME_ENG =", value, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngNotEqualTo(String value) {
            addCriterion("EBS_ORG_NAME_ENG <>", value, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngGreaterThan(String value) {
            addCriterion("EBS_ORG_NAME_ENG >", value, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_ORG_NAME_ENG >=", value, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngLessThan(String value) {
            addCriterion("EBS_ORG_NAME_ENG <", value, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngLessThanOrEqualTo(String value) {
            addCriterion("EBS_ORG_NAME_ENG <=", value, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngLike(String value) {
            addCriterion("EBS_ORG_NAME_ENG like", value, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngNotLike(String value) {
            addCriterion("EBS_ORG_NAME_ENG not like", value, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngIn(List<String> values) {
            addCriterion("EBS_ORG_NAME_ENG in", values, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngNotIn(List<String> values) {
            addCriterion("EBS_ORG_NAME_ENG not in", values, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngBetween(String value1, String value2) {
            addCriterion("EBS_ORG_NAME_ENG between", value1, value2, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngNotBetween(String value1, String value2) {
            addCriterion("EBS_ORG_NAME_ENG not between", value1, value2, "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorIsNull() {
            addCriterion("EBS_ORG_NAME_KOR is null");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorIsNotNull() {
            addCriterion("EBS_ORG_NAME_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorEqualTo(String value) {
            addCriterion("EBS_ORG_NAME_KOR =", value, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorNotEqualTo(String value) {
            addCriterion("EBS_ORG_NAME_KOR <>", value, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorGreaterThan(String value) {
            addCriterion("EBS_ORG_NAME_KOR >", value, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_ORG_NAME_KOR >=", value, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorLessThan(String value) {
            addCriterion("EBS_ORG_NAME_KOR <", value, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorLessThanOrEqualTo(String value) {
            addCriterion("EBS_ORG_NAME_KOR <=", value, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorLike(String value) {
            addCriterion("EBS_ORG_NAME_KOR like", value, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorNotLike(String value) {
            addCriterion("EBS_ORG_NAME_KOR not like", value, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorIn(List<String> values) {
            addCriterion("EBS_ORG_NAME_KOR in", values, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorNotIn(List<String> values) {
            addCriterion("EBS_ORG_NAME_KOR not in", values, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorBetween(String value1, String value2) {
            addCriterion("EBS_ORG_NAME_KOR between", value1, value2, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorNotBetween(String value1, String value2) {
            addCriterion("EBS_ORG_NAME_KOR not between", value1, value2, "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngIsNull() {
            addCriterion("EBS_GRADE_NAME_ENG is null");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngIsNotNull() {
            addCriterion("EBS_GRADE_NAME_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngEqualTo(String value) {
            addCriterion("EBS_GRADE_NAME_ENG =", value, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngNotEqualTo(String value) {
            addCriterion("EBS_GRADE_NAME_ENG <>", value, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngGreaterThan(String value) {
            addCriterion("EBS_GRADE_NAME_ENG >", value, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_GRADE_NAME_ENG >=", value, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngLessThan(String value) {
            addCriterion("EBS_GRADE_NAME_ENG <", value, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngLessThanOrEqualTo(String value) {
            addCriterion("EBS_GRADE_NAME_ENG <=", value, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngLike(String value) {
            addCriterion("EBS_GRADE_NAME_ENG like", value, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngNotLike(String value) {
            addCriterion("EBS_GRADE_NAME_ENG not like", value, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngIn(List<String> values) {
            addCriterion("EBS_GRADE_NAME_ENG in", values, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngNotIn(List<String> values) {
            addCriterion("EBS_GRADE_NAME_ENG not in", values, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngBetween(String value1, String value2) {
            addCriterion("EBS_GRADE_NAME_ENG between", value1, value2, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngNotBetween(String value1, String value2) {
            addCriterion("EBS_GRADE_NAME_ENG not between", value1, value2, "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorIsNull() {
            addCriterion("EBS_GRADE_NAME_KOR is null");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorIsNotNull() {
            addCriterion("EBS_GRADE_NAME_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorEqualTo(String value) {
            addCriterion("EBS_GRADE_NAME_KOR =", value, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorNotEqualTo(String value) {
            addCriterion("EBS_GRADE_NAME_KOR <>", value, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorGreaterThan(String value) {
            addCriterion("EBS_GRADE_NAME_KOR >", value, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_GRADE_NAME_KOR >=", value, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorLessThan(String value) {
            addCriterion("EBS_GRADE_NAME_KOR <", value, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorLessThanOrEqualTo(String value) {
            addCriterion("EBS_GRADE_NAME_KOR <=", value, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorLike(String value) {
            addCriterion("EBS_GRADE_NAME_KOR like", value, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorNotLike(String value) {
            addCriterion("EBS_GRADE_NAME_KOR not like", value, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorIn(List<String> values) {
            addCriterion("EBS_GRADE_NAME_KOR in", values, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorNotIn(List<String> values) {
            addCriterion("EBS_GRADE_NAME_KOR not in", values, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorBetween(String value1, String value2) {
            addCriterion("EBS_GRADE_NAME_KOR between", value1, value2, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorNotBetween(String value1, String value2) {
            addCriterion("EBS_GRADE_NAME_KOR not between", value1, value2, "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngIsNull() {
            addCriterion("EBS_GRADE_LEVEL_ENG is null");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngIsNotNull() {
            addCriterion("EBS_GRADE_LEVEL_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngEqualTo(String value) {
            addCriterion("EBS_GRADE_LEVEL_ENG =", value, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngNotEqualTo(String value) {
            addCriterion("EBS_GRADE_LEVEL_ENG <>", value, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngGreaterThan(String value) {
            addCriterion("EBS_GRADE_LEVEL_ENG >", value, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_GRADE_LEVEL_ENG >=", value, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngLessThan(String value) {
            addCriterion("EBS_GRADE_LEVEL_ENG <", value, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngLessThanOrEqualTo(String value) {
            addCriterion("EBS_GRADE_LEVEL_ENG <=", value, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngLike(String value) {
            addCriterion("EBS_GRADE_LEVEL_ENG like", value, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngNotLike(String value) {
            addCriterion("EBS_GRADE_LEVEL_ENG not like", value, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngIn(List<String> values) {
            addCriterion("EBS_GRADE_LEVEL_ENG in", values, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngNotIn(List<String> values) {
            addCriterion("EBS_GRADE_LEVEL_ENG not in", values, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngBetween(String value1, String value2) {
            addCriterion("EBS_GRADE_LEVEL_ENG between", value1, value2, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngNotBetween(String value1, String value2) {
            addCriterion("EBS_GRADE_LEVEL_ENG not between", value1, value2, "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorIsNull() {
            addCriterion("EBS_GRADE_LEVEL_KOR is null");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorIsNotNull() {
            addCriterion("EBS_GRADE_LEVEL_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorEqualTo(String value) {
            addCriterion("EBS_GRADE_LEVEL_KOR =", value, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorNotEqualTo(String value) {
            addCriterion("EBS_GRADE_LEVEL_KOR <>", value, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorGreaterThan(String value) {
            addCriterion("EBS_GRADE_LEVEL_KOR >", value, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_GRADE_LEVEL_KOR >=", value, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorLessThan(String value) {
            addCriterion("EBS_GRADE_LEVEL_KOR <", value, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorLessThanOrEqualTo(String value) {
            addCriterion("EBS_GRADE_LEVEL_KOR <=", value, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorLike(String value) {
            addCriterion("EBS_GRADE_LEVEL_KOR like", value, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorNotLike(String value) {
            addCriterion("EBS_GRADE_LEVEL_KOR not like", value, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorIn(List<String> values) {
            addCriterion("EBS_GRADE_LEVEL_KOR in", values, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorNotIn(List<String> values) {
            addCriterion("EBS_GRADE_LEVEL_KOR not in", values, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorBetween(String value1, String value2) {
            addCriterion("EBS_GRADE_LEVEL_KOR between", value1, value2, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorNotBetween(String value1, String value2) {
            addCriterion("EBS_GRADE_LEVEL_KOR not between", value1, value2, "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngIsNull() {
            addCriterion("EBS_PERSON_TYPE_ENG is null");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngIsNotNull() {
            addCriterion("EBS_PERSON_TYPE_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngEqualTo(String value) {
            addCriterion("EBS_PERSON_TYPE_ENG =", value, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngNotEqualTo(String value) {
            addCriterion("EBS_PERSON_TYPE_ENG <>", value, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngGreaterThan(String value) {
            addCriterion("EBS_PERSON_TYPE_ENG >", value, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_PERSON_TYPE_ENG >=", value, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngLessThan(String value) {
            addCriterion("EBS_PERSON_TYPE_ENG <", value, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngLessThanOrEqualTo(String value) {
            addCriterion("EBS_PERSON_TYPE_ENG <=", value, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngLike(String value) {
            addCriterion("EBS_PERSON_TYPE_ENG like", value, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngNotLike(String value) {
            addCriterion("EBS_PERSON_TYPE_ENG not like", value, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngIn(List<String> values) {
            addCriterion("EBS_PERSON_TYPE_ENG in", values, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngNotIn(List<String> values) {
            addCriterion("EBS_PERSON_TYPE_ENG not in", values, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngBetween(String value1, String value2) {
            addCriterion("EBS_PERSON_TYPE_ENG between", value1, value2, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngNotBetween(String value1, String value2) {
            addCriterion("EBS_PERSON_TYPE_ENG not between", value1, value2, "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorIsNull() {
            addCriterion("EBS_PERSON_TYPE_KOR is null");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorIsNotNull() {
            addCriterion("EBS_PERSON_TYPE_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorEqualTo(String value) {
            addCriterion("EBS_PERSON_TYPE_KOR =", value, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorNotEqualTo(String value) {
            addCriterion("EBS_PERSON_TYPE_KOR <>", value, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorGreaterThan(String value) {
            addCriterion("EBS_PERSON_TYPE_KOR >", value, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_PERSON_TYPE_KOR >=", value, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorLessThan(String value) {
            addCriterion("EBS_PERSON_TYPE_KOR <", value, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorLessThanOrEqualTo(String value) {
            addCriterion("EBS_PERSON_TYPE_KOR <=", value, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorLike(String value) {
            addCriterion("EBS_PERSON_TYPE_KOR like", value, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorNotLike(String value) {
            addCriterion("EBS_PERSON_TYPE_KOR not like", value, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorIn(List<String> values) {
            addCriterion("EBS_PERSON_TYPE_KOR in", values, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorNotIn(List<String> values) {
            addCriterion("EBS_PERSON_TYPE_KOR not in", values, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorBetween(String value1, String value2) {
            addCriterion("EBS_PERSON_TYPE_KOR between", value1, value2, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorNotBetween(String value1, String value2) {
            addCriterion("EBS_PERSON_TYPE_KOR not between", value1, value2, "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngIsNull() {
            addCriterion("EBS_USER_STATUS_ENG is null");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngIsNotNull() {
            addCriterion("EBS_USER_STATUS_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngEqualTo(String value) {
            addCriterion("EBS_USER_STATUS_ENG =", value, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngNotEqualTo(String value) {
            addCriterion("EBS_USER_STATUS_ENG <>", value, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngGreaterThan(String value) {
            addCriterion("EBS_USER_STATUS_ENG >", value, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_USER_STATUS_ENG >=", value, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngLessThan(String value) {
            addCriterion("EBS_USER_STATUS_ENG <", value, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngLessThanOrEqualTo(String value) {
            addCriterion("EBS_USER_STATUS_ENG <=", value, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngLike(String value) {
            addCriterion("EBS_USER_STATUS_ENG like", value, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngNotLike(String value) {
            addCriterion("EBS_USER_STATUS_ENG not like", value, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngIn(List<String> values) {
            addCriterion("EBS_USER_STATUS_ENG in", values, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngNotIn(List<String> values) {
            addCriterion("EBS_USER_STATUS_ENG not in", values, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngBetween(String value1, String value2) {
            addCriterion("EBS_USER_STATUS_ENG between", value1, value2, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngNotBetween(String value1, String value2) {
            addCriterion("EBS_USER_STATUS_ENG not between", value1, value2, "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorIsNull() {
            addCriterion("EBS_USER_STATUS_KOR is null");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorIsNotNull() {
            addCriterion("EBS_USER_STATUS_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorEqualTo(String value) {
            addCriterion("EBS_USER_STATUS_KOR =", value, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorNotEqualTo(String value) {
            addCriterion("EBS_USER_STATUS_KOR <>", value, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorGreaterThan(String value) {
            addCriterion("EBS_USER_STATUS_KOR >", value, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorGreaterThanOrEqualTo(String value) {
            addCriterion("EBS_USER_STATUS_KOR >=", value, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorLessThan(String value) {
            addCriterion("EBS_USER_STATUS_KOR <", value, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorLessThanOrEqualTo(String value) {
            addCriterion("EBS_USER_STATUS_KOR <=", value, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorLike(String value) {
            addCriterion("EBS_USER_STATUS_KOR like", value, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorNotLike(String value) {
            addCriterion("EBS_USER_STATUS_KOR not like", value, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorIn(List<String> values) {
            addCriterion("EBS_USER_STATUS_KOR in", values, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorNotIn(List<String> values) {
            addCriterion("EBS_USER_STATUS_KOR not in", values, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorBetween(String value1, String value2) {
            addCriterion("EBS_USER_STATUS_KOR between", value1, value2, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorNotBetween(String value1, String value2) {
            addCriterion("EBS_USER_STATUS_KOR not between", value1, value2, "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andPositionEngIsNull() {
            addCriterion("POSITION_ENG is null");
            return (Criteria) this;
        }

        public Criteria andPositionEngIsNotNull() {
            addCriterion("POSITION_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andPositionEngEqualTo(String value) {
            addCriterion("POSITION_ENG =", value, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngNotEqualTo(String value) {
            addCriterion("POSITION_ENG <>", value, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngGreaterThan(String value) {
            addCriterion("POSITION_ENG >", value, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngGreaterThanOrEqualTo(String value) {
            addCriterion("POSITION_ENG >=", value, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngLessThan(String value) {
            addCriterion("POSITION_ENG <", value, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngLessThanOrEqualTo(String value) {
            addCriterion("POSITION_ENG <=", value, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngLike(String value) {
            addCriterion("POSITION_ENG like", value, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngNotLike(String value) {
            addCriterion("POSITION_ENG not like", value, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngIn(List<String> values) {
            addCriterion("POSITION_ENG in", values, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngNotIn(List<String> values) {
            addCriterion("POSITION_ENG not in", values, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngBetween(String value1, String value2) {
            addCriterion("POSITION_ENG between", value1, value2, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionEngNotBetween(String value1, String value2) {
            addCriterion("POSITION_ENG not between", value1, value2, "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionKorIsNull() {
            addCriterion("POSITION_KOR is null");
            return (Criteria) this;
        }

        public Criteria andPositionKorIsNotNull() {
            addCriterion("POSITION_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andPositionKorEqualTo(String value) {
            addCriterion("POSITION_KOR =", value, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorNotEqualTo(String value) {
            addCriterion("POSITION_KOR <>", value, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorGreaterThan(String value) {
            addCriterion("POSITION_KOR >", value, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorGreaterThanOrEqualTo(String value) {
            addCriterion("POSITION_KOR >=", value, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorLessThan(String value) {
            addCriterion("POSITION_KOR <", value, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorLessThanOrEqualTo(String value) {
            addCriterion("POSITION_KOR <=", value, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorLike(String value) {
            addCriterion("POSITION_KOR like", value, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorNotLike(String value) {
            addCriterion("POSITION_KOR not like", value, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorIn(List<String> values) {
            addCriterion("POSITION_KOR in", values, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorNotIn(List<String> values) {
            addCriterion("POSITION_KOR not in", values, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorBetween(String value1, String value2) {
            addCriterion("POSITION_KOR between", value1, value2, "positionKor");
            return (Criteria) this;
        }

        public Criteria andPositionKorNotBetween(String value1, String value2) {
            addCriterion("POSITION_KOR not between", value1, value2, "positionKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngIsNull() {
            addCriterion("STU_STATUS_ENG is null");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngIsNotNull() {
            addCriterion("STU_STATUS_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngEqualTo(String value) {
            addCriterion("STU_STATUS_ENG =", value, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngNotEqualTo(String value) {
            addCriterion("STU_STATUS_ENG <>", value, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngGreaterThan(String value) {
            addCriterion("STU_STATUS_ENG >", value, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngGreaterThanOrEqualTo(String value) {
            addCriterion("STU_STATUS_ENG >=", value, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngLessThan(String value) {
            addCriterion("STU_STATUS_ENG <", value, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngLessThanOrEqualTo(String value) {
            addCriterion("STU_STATUS_ENG <=", value, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngLike(String value) {
            addCriterion("STU_STATUS_ENG like", value, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngNotLike(String value) {
            addCriterion("STU_STATUS_ENG not like", value, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngIn(List<String> values) {
            addCriterion("STU_STATUS_ENG in", values, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngNotIn(List<String> values) {
            addCriterion("STU_STATUS_ENG not in", values, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngBetween(String value1, String value2) {
            addCriterion("STU_STATUS_ENG between", value1, value2, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngNotBetween(String value1, String value2) {
            addCriterion("STU_STATUS_ENG not between", value1, value2, "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorIsNull() {
            addCriterion("STU_STATUS_KOR is null");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorIsNotNull() {
            addCriterion("STU_STATUS_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorEqualTo(String value) {
            addCriterion("STU_STATUS_KOR =", value, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorNotEqualTo(String value) {
            addCriterion("STU_STATUS_KOR <>", value, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorGreaterThan(String value) {
            addCriterion("STU_STATUS_KOR >", value, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorGreaterThanOrEqualTo(String value) {
            addCriterion("STU_STATUS_KOR >=", value, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorLessThan(String value) {
            addCriterion("STU_STATUS_KOR <", value, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorLessThanOrEqualTo(String value) {
            addCriterion("STU_STATUS_KOR <=", value, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorLike(String value) {
            addCriterion("STU_STATUS_KOR like", value, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorNotLike(String value) {
            addCriterion("STU_STATUS_KOR not like", value, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorIn(List<String> values) {
            addCriterion("STU_STATUS_KOR in", values, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorNotIn(List<String> values) {
            addCriterion("STU_STATUS_KOR not in", values, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorBetween(String value1, String value2) {
            addCriterion("STU_STATUS_KOR between", value1, value2, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorNotBetween(String value1, String value2) {
            addCriterion("STU_STATUS_KOR not between", value1, value2, "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeIsNull() {
            addCriterion("ACAD_PROG_CODE is null");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeIsNotNull() {
            addCriterion("ACAD_PROG_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeEqualTo(String value) {
            addCriterion("ACAD_PROG_CODE =", value, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeNotEqualTo(String value) {
            addCriterion("ACAD_PROG_CODE <>", value, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeGreaterThan(String value) {
            addCriterion("ACAD_PROG_CODE >", value, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_PROG_CODE >=", value, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeLessThan(String value) {
            addCriterion("ACAD_PROG_CODE <", value, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeLessThanOrEqualTo(String value) {
            addCriterion("ACAD_PROG_CODE <=", value, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeLike(String value) {
            addCriterion("ACAD_PROG_CODE like", value, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeNotLike(String value) {
            addCriterion("ACAD_PROG_CODE not like", value, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeIn(List<String> values) {
            addCriterion("ACAD_PROG_CODE in", values, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeNotIn(List<String> values) {
            addCriterion("ACAD_PROG_CODE not in", values, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeBetween(String value1, String value2) {
            addCriterion("ACAD_PROG_CODE between", value1, value2, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeNotBetween(String value1, String value2) {
            addCriterion("ACAD_PROG_CODE not between", value1, value2, "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorIsNull() {
            addCriterion("ACAD_PROG_KOR is null");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorIsNotNull() {
            addCriterion("ACAD_PROG_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorEqualTo(String value) {
            addCriterion("ACAD_PROG_KOR =", value, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorNotEqualTo(String value) {
            addCriterion("ACAD_PROG_KOR <>", value, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorGreaterThan(String value) {
            addCriterion("ACAD_PROG_KOR >", value, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_PROG_KOR >=", value, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorLessThan(String value) {
            addCriterion("ACAD_PROG_KOR <", value, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorLessThanOrEqualTo(String value) {
            addCriterion("ACAD_PROG_KOR <=", value, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorLike(String value) {
            addCriterion("ACAD_PROG_KOR like", value, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorNotLike(String value) {
            addCriterion("ACAD_PROG_KOR not like", value, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorIn(List<String> values) {
            addCriterion("ACAD_PROG_KOR in", values, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorNotIn(List<String> values) {
            addCriterion("ACAD_PROG_KOR not in", values, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorBetween(String value1, String value2) {
            addCriterion("ACAD_PROG_KOR between", value1, value2, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorNotBetween(String value1, String value2) {
            addCriterion("ACAD_PROG_KOR not between", value1, value2, "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngIsNull() {
            addCriterion("ACAD_PROG_ENG is null");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngIsNotNull() {
            addCriterion("ACAD_PROG_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngEqualTo(String value) {
            addCriterion("ACAD_PROG_ENG =", value, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngNotEqualTo(String value) {
            addCriterion("ACAD_PROG_ENG <>", value, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngGreaterThan(String value) {
            addCriterion("ACAD_PROG_ENG >", value, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngGreaterThanOrEqualTo(String value) {
            addCriterion("ACAD_PROG_ENG >=", value, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngLessThan(String value) {
            addCriterion("ACAD_PROG_ENG <", value, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngLessThanOrEqualTo(String value) {
            addCriterion("ACAD_PROG_ENG <=", value, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngLike(String value) {
            addCriterion("ACAD_PROG_ENG like", value, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngNotLike(String value) {
            addCriterion("ACAD_PROG_ENG not like", value, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngIn(List<String> values) {
            addCriterion("ACAD_PROG_ENG in", values, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngNotIn(List<String> values) {
            addCriterion("ACAD_PROG_ENG not in", values, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngBetween(String value1, String value2) {
            addCriterion("ACAD_PROG_ENG between", value1, value2, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngNotBetween(String value1, String value2) {
            addCriterion("ACAD_PROG_ENG not between", value1, value2, "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andPersonTypeCodeUidIsNull() {
            addCriterion("PERSON_TYPE_CODE_UID is null");
            return (Criteria) this;
        }

        public Criteria andPersonTypeCodeUidIsNotNull() {
            addCriterion("PERSON_TYPE_CODE_UID is not null");
            return (Criteria) this;
        }

        public Criteria andPersonTypeCodeUidEqualTo(String value) {
            addCriterion("PERSON_TYPE_CODE_UID =", value, "personTypeCodeUid");
            return (Criteria) this;
        }

        public Criteria andPersonTypeCodeUidNotEqualTo(String value) {
            addCriterion("PERSON_TYPE_CODE_UID <>", value, "personTypeCodeUid");
            return (Criteria) this;
        }

        public Criteria andPersonTypeCodeUidGreaterThan(String value) {
            addCriterion("PERSON_TYPE_CODE_UID >", value, "personTypeCodeUid");
            return (Criteria) this;
        }

        public Criteria andPersonTypeCodeUidGreaterThanOrEqualTo(String value) {
            addCriterion("PERSON_TYPE_CODE_UID >=", value, "personTypeCodeUid");
            return (Criteria) this;
        }

        public Criteria andPersonTypeCodeUidLessThan(String value) {
            addCriterion("PERSON_TYPE_CODE_UID <", value, "personTypeCodeUid");
            return (Criteria) this;
        }

        public Criteria andPersonTypeCodeUidLessThanOrEqualTo(String value) {
            addCriterion("PERSON_TYPE_CODE_UID <=", value, "personTypeCodeUid");
            return (Criteria) this;
        }

        public Criteria andPersonTypeCodeUidLike(String value) {
            addCriterion("PERSON_TYPE_CODE_UID like", value, "personTypeCodeUid");
            return (Criteria) this;
        }

        public Criteria andPersonTypeCodeUidNotLike(String value) {
            addCriterion("PERSON_TYPE_CODE_UID not like", value, "personTypeCodeUid");
            return (Criteria) this;
        }

        public Criteria andPersonTypeCodeUidIn(List<String> values) {
            addCriterion("PERSON_TYPE_CODE_UID in", values, "personTypeCodeUid");
            return (Criteria) this;
        }

        public Criteria andPersonTypeCodeUidNotIn(List<String> values) {
            addCriterion("PERSON_TYPE_CODE_UID not in", values, "personTypeCodeUid");
            return (Criteria) this;
        }

        public Criteria andPersonTypeCodeUidBetween(String value1, String value2) {
            addCriterion("PERSON_TYPE_CODE_UID between", value1, value2, "personTypeCodeUid");
            return (Criteria) this;
        }

        public Criteria andPersonTypeCodeUidNotBetween(String value1, String value2) {
            addCriterion("PERSON_TYPE_CODE_UID not between", value1, value2, "personTypeCodeUid");
            return (Criteria) this;
        }

        public Criteria andProgEffdtIsNull() {
            addCriterion("PROG_EFFDT is null");
            return (Criteria) this;
        }

        public Criteria andProgEffdtIsNotNull() {
            addCriterion("PROG_EFFDT is not null");
            return (Criteria) this;
        }

        public Criteria andProgEffdtEqualTo(Date value) {
            addCriterion("PROG_EFFDT =", value, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtNotEqualTo(Date value) {
            addCriterion("PROG_EFFDT <>", value, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtGreaterThan(Date value) {
            addCriterion("PROG_EFFDT >", value, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtGreaterThanOrEqualTo(Date value) {
            addCriterion("PROG_EFFDT >=", value, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtLessThan(Date value) {
            addCriterion("PROG_EFFDT <", value, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtLessThanOrEqualTo(Date value) {
            addCriterion("PROG_EFFDT <=", value, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtIn(List<Date> values) {
            addCriterion("PROG_EFFDT in", values, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtNotIn(List<Date> values) {
            addCriterion("PROG_EFFDT not in", values, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtBetween(Date value1, Date value2) {
            addCriterion("PROG_EFFDT between", value1, value2, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andProgEffdtNotBetween(Date value1, Date value2) {
            addCriterion("PROG_EFFDT not between", value1, value2, "progEffdt");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdIsNull() {
            addCriterion("STDNT_TYPE_ID is null");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdIsNotNull() {
            addCriterion("STDNT_TYPE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdEqualTo(String value) {
            addCriterion("STDNT_TYPE_ID =", value, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdNotEqualTo(String value) {
            addCriterion("STDNT_TYPE_ID <>", value, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdGreaterThan(String value) {
            addCriterion("STDNT_TYPE_ID >", value, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdGreaterThanOrEqualTo(String value) {
            addCriterion("STDNT_TYPE_ID >=", value, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdLessThan(String value) {
            addCriterion("STDNT_TYPE_ID <", value, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdLessThanOrEqualTo(String value) {
            addCriterion("STDNT_TYPE_ID <=", value, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdLike(String value) {
            addCriterion("STDNT_TYPE_ID like", value, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdNotLike(String value) {
            addCriterion("STDNT_TYPE_ID not like", value, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdIn(List<String> values) {
            addCriterion("STDNT_TYPE_ID in", values, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdNotIn(List<String> values) {
            addCriterion("STDNT_TYPE_ID not in", values, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdBetween(String value1, String value2) {
            addCriterion("STDNT_TYPE_ID between", value1, value2, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdNotBetween(String value1, String value2) {
            addCriterion("STDNT_TYPE_ID not between", value1, value2, "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassIsNull() {
            addCriterion("STDNT_TYPE_CLASS is null");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassIsNotNull() {
            addCriterion("STDNT_TYPE_CLASS is not null");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassEqualTo(String value) {
            addCriterion("STDNT_TYPE_CLASS =", value, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassNotEqualTo(String value) {
            addCriterion("STDNT_TYPE_CLASS <>", value, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassGreaterThan(String value) {
            addCriterion("STDNT_TYPE_CLASS >", value, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassGreaterThanOrEqualTo(String value) {
            addCriterion("STDNT_TYPE_CLASS >=", value, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassLessThan(String value) {
            addCriterion("STDNT_TYPE_CLASS <", value, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassLessThanOrEqualTo(String value) {
            addCriterion("STDNT_TYPE_CLASS <=", value, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassLike(String value) {
            addCriterion("STDNT_TYPE_CLASS like", value, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassNotLike(String value) {
            addCriterion("STDNT_TYPE_CLASS not like", value, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassIn(List<String> values) {
            addCriterion("STDNT_TYPE_CLASS in", values, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassNotIn(List<String> values) {
            addCriterion("STDNT_TYPE_CLASS not in", values, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassBetween(String value1, String value2) {
            addCriterion("STDNT_TYPE_CLASS between", value1, value2, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassNotBetween(String value1, String value2) {
            addCriterion("STDNT_TYPE_CLASS not between", value1, value2, "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdIsNull() {
            addCriterion("STDNT_CATEGORY_ID is null");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdIsNotNull() {
            addCriterion("STDNT_CATEGORY_ID is not null");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdEqualTo(String value) {
            addCriterion("STDNT_CATEGORY_ID =", value, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdNotEqualTo(String value) {
            addCriterion("STDNT_CATEGORY_ID <>", value, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdGreaterThan(String value) {
            addCriterion("STDNT_CATEGORY_ID >", value, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdGreaterThanOrEqualTo(String value) {
            addCriterion("STDNT_CATEGORY_ID >=", value, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdLessThan(String value) {
            addCriterion("STDNT_CATEGORY_ID <", value, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdLessThanOrEqualTo(String value) {
            addCriterion("STDNT_CATEGORY_ID <=", value, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdLike(String value) {
            addCriterion("STDNT_CATEGORY_ID like", value, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdNotLike(String value) {
            addCriterion("STDNT_CATEGORY_ID not like", value, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdIn(List<String> values) {
            addCriterion("STDNT_CATEGORY_ID in", values, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdNotIn(List<String> values) {
            addCriterion("STDNT_CATEGORY_ID not in", values, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdBetween(String value1, String value2) {
            addCriterion("STDNT_CATEGORY_ID between", value1, value2, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdNotBetween(String value1, String value2) {
            addCriterion("STDNT_CATEGORY_ID not between", value1, value2, "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdIsNull() {
            addCriterion("ADVR_EBS_PERSON_ID is null");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdIsNotNull() {
            addCriterion("ADVR_EBS_PERSON_ID is not null");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdEqualTo(String value) {
            addCriterion("ADVR_EBS_PERSON_ID =", value, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdNotEqualTo(String value) {
            addCriterion("ADVR_EBS_PERSON_ID <>", value, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdGreaterThan(String value) {
            addCriterion("ADVR_EBS_PERSON_ID >", value, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdGreaterThanOrEqualTo(String value) {
            addCriterion("ADVR_EBS_PERSON_ID >=", value, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdLessThan(String value) {
            addCriterion("ADVR_EBS_PERSON_ID <", value, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdLessThanOrEqualTo(String value) {
            addCriterion("ADVR_EBS_PERSON_ID <=", value, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdLike(String value) {
            addCriterion("ADVR_EBS_PERSON_ID like", value, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdNotLike(String value) {
            addCriterion("ADVR_EBS_PERSON_ID not like", value, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdIn(List<String> values) {
            addCriterion("ADVR_EBS_PERSON_ID in", values, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdNotIn(List<String> values) {
            addCriterion("ADVR_EBS_PERSON_ID not in", values, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdBetween(String value1, String value2) {
            addCriterion("ADVR_EBS_PERSON_ID between", value1, value2, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdNotBetween(String value1, String value2) {
            addCriterion("ADVR_EBS_PERSON_ID not between", value1, value2, "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrNameIsNull() {
            addCriterion("ADVR_NAME is null");
            return (Criteria) this;
        }

        public Criteria andAdvrNameIsNotNull() {
            addCriterion("ADVR_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andAdvrNameEqualTo(String value) {
            addCriterion("ADVR_NAME =", value, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameNotEqualTo(String value) {
            addCriterion("ADVR_NAME <>", value, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameGreaterThan(String value) {
            addCriterion("ADVR_NAME >", value, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameGreaterThanOrEqualTo(String value) {
            addCriterion("ADVR_NAME >=", value, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameLessThan(String value) {
            addCriterion("ADVR_NAME <", value, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameLessThanOrEqualTo(String value) {
            addCriterion("ADVR_NAME <=", value, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameLike(String value) {
            addCriterion("ADVR_NAME like", value, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameNotLike(String value) {
            addCriterion("ADVR_NAME not like", value, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameIn(List<String> values) {
            addCriterion("ADVR_NAME in", values, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameNotIn(List<String> values) {
            addCriterion("ADVR_NAME not in", values, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameBetween(String value1, String value2) {
            addCriterion("ADVR_NAME between", value1, value2, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameNotBetween(String value1, String value2) {
            addCriterion("ADVR_NAME not between", value1, value2, "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcIsNull() {
            addCriterion("ADVR_NAME_AC is null");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcIsNotNull() {
            addCriterion("ADVR_NAME_AC is not null");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcEqualTo(String value) {
            addCriterion("ADVR_NAME_AC =", value, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcNotEqualTo(String value) {
            addCriterion("ADVR_NAME_AC <>", value, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcGreaterThan(String value) {
            addCriterion("ADVR_NAME_AC >", value, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcGreaterThanOrEqualTo(String value) {
            addCriterion("ADVR_NAME_AC >=", value, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcLessThan(String value) {
            addCriterion("ADVR_NAME_AC <", value, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcLessThanOrEqualTo(String value) {
            addCriterion("ADVR_NAME_AC <=", value, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcLike(String value) {
            addCriterion("ADVR_NAME_AC like", value, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcNotLike(String value) {
            addCriterion("ADVR_NAME_AC not like", value, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcIn(List<String> values) {
            addCriterion("ADVR_NAME_AC in", values, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcNotIn(List<String> values) {
            addCriterion("ADVR_NAME_AC not in", values, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcBetween(String value1, String value2) {
            addCriterion("ADVR_NAME_AC between", value1, value2, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcNotBetween(String value1, String value2) {
            addCriterion("ADVR_NAME_AC not between", value1, value2, "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andEntranceDateIsNull() {
            addCriterion("ENTRANCE_DATE is null");
            return (Criteria) this;
        }

        public Criteria andEntranceDateIsNotNull() {
            addCriterion("ENTRANCE_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andEntranceDateEqualTo(Date value) {
            addCriterion("ENTRANCE_DATE =", value, "entranceDate");
            return (Criteria) this;
        }

        public Criteria andEntranceDateNotEqualTo(Date value) {
            addCriterion("ENTRANCE_DATE <>", value, "entranceDate");
            return (Criteria) this;
        }

        public Criteria andEntranceDateGreaterThan(Date value) {
            addCriterion("ENTRANCE_DATE >", value, "entranceDate");
            return (Criteria) this;
        }

        public Criteria andEntranceDateGreaterThanOrEqualTo(Date value) {
            addCriterion("ENTRANCE_DATE >=", value, "entranceDate");
            return (Criteria) this;
        }

        public Criteria andEntranceDateLessThan(Date value) {
            addCriterion("ENTRANCE_DATE <", value, "entranceDate");
            return (Criteria) this;
        }

        public Criteria andEntranceDateLessThanOrEqualTo(Date value) {
            addCriterion("ENTRANCE_DATE <=", value, "entranceDate");
            return (Criteria) this;
        }

        public Criteria andEntranceDateIn(List<Date> values) {
            addCriterion("ENTRANCE_DATE in", values, "entranceDate");
            return (Criteria) this;
        }

        public Criteria andEntranceDateNotIn(List<Date> values) {
            addCriterion("ENTRANCE_DATE not in", values, "entranceDate");
            return (Criteria) this;
        }

        public Criteria andEntranceDateBetween(Date value1, Date value2) {
            addCriterion("ENTRANCE_DATE between", value1, value2, "entranceDate");
            return (Criteria) this;
        }

        public Criteria andEntranceDateNotBetween(Date value1, Date value2) {
            addCriterion("ENTRANCE_DATE not between", value1, value2, "entranceDate");
            return (Criteria) this;
        }

        public Criteria andResignDateIsNull() {
            addCriterion("RESIGN_DATE is null");
            return (Criteria) this;
        }

        public Criteria andResignDateIsNotNull() {
            addCriterion("RESIGN_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andResignDateEqualTo(Date value) {
            addCriterion("RESIGN_DATE =", value, "resignDate");
            return (Criteria) this;
        }

        public Criteria andResignDateNotEqualTo(Date value) {
            addCriterion("RESIGN_DATE <>", value, "resignDate");
            return (Criteria) this;
        }

        public Criteria andResignDateGreaterThan(Date value) {
            addCriterion("RESIGN_DATE >", value, "resignDate");
            return (Criteria) this;
        }

        public Criteria andResignDateGreaterThanOrEqualTo(Date value) {
            addCriterion("RESIGN_DATE >=", value, "resignDate");
            return (Criteria) this;
        }

        public Criteria andResignDateLessThan(Date value) {
            addCriterion("RESIGN_DATE <", value, "resignDate");
            return (Criteria) this;
        }

        public Criteria andResignDateLessThanOrEqualTo(Date value) {
            addCriterion("RESIGN_DATE <=", value, "resignDate");
            return (Criteria) this;
        }

        public Criteria andResignDateIn(List<Date> values) {
            addCriterion("RESIGN_DATE in", values, "resignDate");
            return (Criteria) this;
        }

        public Criteria andResignDateNotIn(List<Date> values) {
            addCriterion("RESIGN_DATE not in", values, "resignDate");
            return (Criteria) this;
        }

        public Criteria andResignDateBetween(Date value1, Date value2) {
            addCriterion("RESIGN_DATE between", value1, value2, "resignDate");
            return (Criteria) this;
        }

        public Criteria andResignDateNotBetween(Date value1, Date value2) {
            addCriterion("RESIGN_DATE not between", value1, value2, "resignDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateIsNull() {
            addCriterion("PROG_START_DATE is null");
            return (Criteria) this;
        }

        public Criteria andProgStartDateIsNotNull() {
            addCriterion("PROG_START_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andProgStartDateEqualTo(Date value) {
            addCriterion("PROG_START_DATE =", value, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateNotEqualTo(Date value) {
            addCriterion("PROG_START_DATE <>", value, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateGreaterThan(Date value) {
            addCriterion("PROG_START_DATE >", value, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateGreaterThanOrEqualTo(Date value) {
            addCriterion("PROG_START_DATE >=", value, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateLessThan(Date value) {
            addCriterion("PROG_START_DATE <", value, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateLessThanOrEqualTo(Date value) {
            addCriterion("PROG_START_DATE <=", value, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateIn(List<Date> values) {
            addCriterion("PROG_START_DATE in", values, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateNotIn(List<Date> values) {
            addCriterion("PROG_START_DATE not in", values, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateBetween(Date value1, Date value2) {
            addCriterion("PROG_START_DATE between", value1, value2, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgStartDateNotBetween(Date value1, Date value2) {
            addCriterion("PROG_START_DATE not between", value1, value2, "progStartDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateIsNull() {
            addCriterion("PROG_END_DATE is null");
            return (Criteria) this;
        }

        public Criteria andProgEndDateIsNotNull() {
            addCriterion("PROG_END_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andProgEndDateEqualTo(Date value) {
            addCriterion("PROG_END_DATE =", value, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateNotEqualTo(Date value) {
            addCriterion("PROG_END_DATE <>", value, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateGreaterThan(Date value) {
            addCriterion("PROG_END_DATE >", value, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateGreaterThanOrEqualTo(Date value) {
            addCriterion("PROG_END_DATE >=", value, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateLessThan(Date value) {
            addCriterion("PROG_END_DATE <", value, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateLessThanOrEqualTo(Date value) {
            addCriterion("PROG_END_DATE <=", value, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateIn(List<Date> values) {
            addCriterion("PROG_END_DATE in", values, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateNotIn(List<Date> values) {
            addCriterion("PROG_END_DATE not in", values, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateBetween(Date value1, Date value2) {
            addCriterion("PROG_END_DATE between", value1, value2, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andProgEndDateNotBetween(Date value1, Date value2) {
            addCriterion("PROG_END_DATE not between", value1, value2, "progEndDate");
            return (Criteria) this;
        }

        public Criteria andKaistSuidIsNull() {
            addCriterion("KAIST_SUID is null");
            return (Criteria) this;
        }

        public Criteria andKaistSuidIsNotNull() {
            addCriterion("KAIST_SUID is not null");
            return (Criteria) this;
        }

        public Criteria andKaistSuidEqualTo(String value) {
            addCriterion("KAIST_SUID =", value, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidNotEqualTo(String value) {
            addCriterion("KAIST_SUID <>", value, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidGreaterThan(String value) {
            addCriterion("KAIST_SUID >", value, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidGreaterThanOrEqualTo(String value) {
            addCriterion("KAIST_SUID >=", value, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidLessThan(String value) {
            addCriterion("KAIST_SUID <", value, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidLessThanOrEqualTo(String value) {
            addCriterion("KAIST_SUID <=", value, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidLike(String value) {
            addCriterion("KAIST_SUID like", value, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidNotLike(String value) {
            addCriterion("KAIST_SUID not like", value, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidIn(List<String> values) {
            addCriterion("KAIST_SUID in", values, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidNotIn(List<String> values) {
            addCriterion("KAIST_SUID not in", values, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidBetween(String value1, String value2) {
            addCriterion("KAIST_SUID between", value1, value2, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andKaistSuidNotBetween(String value1, String value2) {
            addCriterion("KAIST_SUID not between", value1, value2, "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidIsNull() {
            addCriterion("ADVR_KAIST_UID is null");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidIsNotNull() {
            addCriterion("ADVR_KAIST_UID is not null");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidEqualTo(String value) {
            addCriterion("ADVR_KAIST_UID =", value, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidNotEqualTo(String value) {
            addCriterion("ADVR_KAIST_UID <>", value, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidGreaterThan(String value) {
            addCriterion("ADVR_KAIST_UID >", value, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidGreaterThanOrEqualTo(String value) {
            addCriterion("ADVR_KAIST_UID >=", value, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidLessThan(String value) {
            addCriterion("ADVR_KAIST_UID <", value, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidLessThanOrEqualTo(String value) {
            addCriterion("ADVR_KAIST_UID <=", value, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidLike(String value) {
            addCriterion("ADVR_KAIST_UID like", value, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidNotLike(String value) {
            addCriterion("ADVR_KAIST_UID not like", value, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidIn(List<String> values) {
            addCriterion("ADVR_KAIST_UID in", values, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidNotIn(List<String> values) {
            addCriterion("ADVR_KAIST_UID not in", values, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidBetween(String value1, String value2) {
            addCriterion("ADVR_KAIST_UID between", value1, value2, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidNotBetween(String value1, String value2) {
            addCriterion("ADVR_KAIST_UID not between", value1, value2, "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNull() {
            addCriterion("CREATORID is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNotNull() {
            addCriterion("CREATORID is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdEqualTo(String value) {
            addCriterion("CREATORID =", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotEqualTo(String value) {
            addCriterion("CREATORID <>", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThan(String value) {
            addCriterion("CREATORID >", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATORID >=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThan(String value) {
            addCriterion("CREATORID <", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThanOrEqualTo(String value) {
            addCriterion("CREATORID <=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLike(String value) {
            addCriterion("CREATORID like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotLike(String value) {
            addCriterion("CREATORID not like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIn(List<String> values) {
            addCriterion("CREATORID in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotIn(List<String> values) {
            addCriterion("CREATORID not in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdBetween(String value1, String value2) {
            addCriterion("CREATORID between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotBetween(String value1, String value2) {
            addCriterion("CREATORID not between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNull() {
            addCriterion("CREATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNotNull() {
            addCriterion("CREATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtEqualTo(Timestamp value) {
            addCriterion("CREATEDAT =", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <>", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThan(Timestamp value) {
            addCriterion("CREATEDAT >", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT >=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThan(Timestamp value) {
            addCriterion("CREATEDAT <", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIn(List<Timestamp> values) {
            addCriterion("CREATEDAT in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotIn(List<Timestamp> values) {
            addCriterion("CREATEDAT not in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT not between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNull() {
            addCriterion("UPDATORID is null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNotNull() {
            addCriterion("UPDATORID is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdEqualTo(String value) {
            addCriterion("UPDATORID =", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotEqualTo(String value) {
            addCriterion("UPDATORID <>", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThan(String value) {
            addCriterion("UPDATORID >", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATORID >=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThan(String value) {
            addCriterion("UPDATORID <", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThanOrEqualTo(String value) {
            addCriterion("UPDATORID <=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLike(String value) {
            addCriterion("UPDATORID like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotLike(String value) {
            addCriterion("UPDATORID not like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIn(List<String> values) {
            addCriterion("UPDATORID in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotIn(List<String> values) {
            addCriterion("UPDATORID not in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdBetween(String value1, String value2) {
            addCriterion("UPDATORID between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotBetween(String value1, String value2) {
            addCriterion("UPDATORID not between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNull() {
            addCriterion("UPDATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNotNull() {
            addCriterion("UPDATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT =", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <>", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThan(Timestamp value) {
            addCriterion("UPDATEDAT >", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT >=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThan(Timestamp value) {
            addCriterion("UPDATEDAT <", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT not in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT not between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andKaistUidLikeInsensitive(String value) {
            addCriterion("upper(KAISTUID) like", value.toUpperCase(), "kaistUid");
            return (Criteria) this;
        }

        public Criteria andUserIdLikeInsensitive(String value) {
            addCriterion("upper(USERID) like", value.toUpperCase(), "userId");
            return (Criteria) this;
        }

        public Criteria andKoreanNameLikeInsensitive(String value) {
            addCriterion("upper(KOREAN_NAME) like", value.toUpperCase(), "koreanName");
            return (Criteria) this;
        }

        public Criteria andEnglishNameLikeInsensitive(String value) {
            addCriterion("upper(ENGLISH_NAME) like", value.toUpperCase(), "englishName");
            return (Criteria) this;
        }

        public Criteria andLastNameLikeInsensitive(String value) {
            addCriterion("upper(LAST_NAME) like", value.toUpperCase(), "lastName");
            return (Criteria) this;
        }

        public Criteria andFirstNameLikeInsensitive(String value) {
            addCriterion("upper(FIRST_NAME) like", value.toUpperCase(), "firstName");
            return (Criteria) this;
        }

        public Criteria andNationCodeUidLikeInsensitive(String value) {
            addCriterion("upper(NATION_CODE_UID) like", value.toUpperCase(), "nationCodeUid");
            return (Criteria) this;
        }

        public Criteria andSexCodeUidLikeInsensitive(String value) {
            addCriterion("upper(SEX_CODE_UID) like", value.toUpperCase(), "sexCodeUid");
            return (Criteria) this;
        }

        public Criteria andEmailAddressLikeInsensitive(String value) {
            addCriterion("upper(EMAIL_ADDRESS) like", value.toUpperCase(), "emailAddress");
            return (Criteria) this;
        }

        public Criteria andChMailLikeInsensitive(String value) {
            addCriterion("upper(CH_MAIL) like", value.toUpperCase(), "chMail");
            return (Criteria) this;
        }

        public Criteria andMobileTelephoneNumberLikeInsensitive(String value) {
            addCriterion("upper(MOBILE_TELEPHONE_NUMBER) like", value.toUpperCase(), "mobileTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOfficeTelephoneNumberLikeInsensitive(String value) {
            addCriterion("upper(OFFICE_TELEPHONE_NUMBER) like", value.toUpperCase(), "officeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andOweHomeTelephoneNumberLikeInsensitive(String value) {
            addCriterion("upper(OWE_HOME_TELEPHONE_NUMBER) like", value.toUpperCase(), "oweHomeTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andFaxTelephoneNumberLikeInsensitive(String value) {
            addCriterion("upper(FAX_TELEPHONE_NUMBER) like", value.toUpperCase(), "faxTelephoneNumber");
            return (Criteria) this;
        }

        public Criteria andPostNumberLikeInsensitive(String value) {
            addCriterion("upper(POST_NUMBER) like", value.toUpperCase(), "postNumber");
            return (Criteria) this;
        }

        public Criteria andAddressLikeInsensitive(String value) {
            addCriterion("upper(ADDRESS) like", value.toUpperCase(), "address");
            return (Criteria) this;
        }

        public Criteria andAddressDetailLikeInsensitive(String value) {
            addCriterion("upper(ADDRESS_DETAIL) like", value.toUpperCase(), "addressDetail");
            return (Criteria) this;
        }

        public Criteria andPersonIdLikeInsensitive(String value) {
            addCriterion("upper(PERSON_ID) like", value.toUpperCase(), "personId");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLikeInsensitive(String value) {
            addCriterion("upper(EMPLOYEE_NUMBER) like", value.toUpperCase(), "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andStdNoLikeInsensitive(String value) {
            addCriterion("upper(STD_NO) like", value.toUpperCase(), "stdNo");
            return (Criteria) this;
        }

        public Criteria andAcadOrgLikeInsensitive(String value) {
            addCriterion("upper(ACAD_ORG) like", value.toUpperCase(), "acadOrg");
            return (Criteria) this;
        }

        public Criteria andAcadNameLikeInsensitive(String value) {
            addCriterion("upper(ACAD_NAME) like", value.toUpperCase(), "acadName");
            return (Criteria) this;
        }

        public Criteria andAcadKstOrgIdLikeInsensitive(String value) {
            addCriterion("upper(ACAD_KST_ORG_ID) like", value.toUpperCase(), "acadKstOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgIdLikeInsensitive(String value) {
            addCriterion("upper(ACAD_EBS_ORG_ID) like", value.toUpperCase(), "acadEbsOrgId");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameEngLikeInsensitive(String value) {
            addCriterion("upper(ACAD_EBS_ORG_NAME_ENG) like", value.toUpperCase(), "acadEbsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andAcadEbsOrgNameKorLikeInsensitive(String value) {
            addCriterion("upper(ACAD_EBS_ORG_NAME_KOR) like", value.toUpperCase(), "acadEbsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andCampusUidLikeInsensitive(String value) {
            addCriterion("upper(CAMPUS_UID) like", value.toUpperCase(), "campusUid");
            return (Criteria) this;
        }

        public Criteria andEbsOrganizationIdLikeInsensitive(String value) {
            addCriterion("upper(EBS_ORGANIZATION_ID) like", value.toUpperCase(), "ebsOrganizationId");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameEngLikeInsensitive(String value) {
            addCriterion("upper(EBS_ORG_NAME_ENG) like", value.toUpperCase(), "ebsOrgNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsOrgNameKorLikeInsensitive(String value) {
            addCriterion("upper(EBS_ORG_NAME_KOR) like", value.toUpperCase(), "ebsOrgNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameEngLikeInsensitive(String value) {
            addCriterion("upper(EBS_GRADE_NAME_ENG) like", value.toUpperCase(), "ebsGradeNameEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeNameKorLikeInsensitive(String value) {
            addCriterion("upper(EBS_GRADE_NAME_KOR) like", value.toUpperCase(), "ebsGradeNameKor");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelEngLikeInsensitive(String value) {
            addCriterion("upper(EBS_GRADE_LEVEL_ENG) like", value.toUpperCase(), "ebsGradeLevelEng");
            return (Criteria) this;
        }

        public Criteria andEbsGradeLevelKorLikeInsensitive(String value) {
            addCriterion("upper(EBS_GRADE_LEVEL_KOR) like", value.toUpperCase(), "ebsGradeLevelKor");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeEngLikeInsensitive(String value) {
            addCriterion("upper(EBS_PERSON_TYPE_ENG) like", value.toUpperCase(), "ebsPersonTypeEng");
            return (Criteria) this;
        }

        public Criteria andEbsPersonTypeKorLikeInsensitive(String value) {
            addCriterion("upper(EBS_PERSON_TYPE_KOR) like", value.toUpperCase(), "ebsPersonTypeKor");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusEngLikeInsensitive(String value) {
            addCriterion("upper(EBS_USER_STATUS_ENG) like", value.toUpperCase(), "ebsUserStatusEng");
            return (Criteria) this;
        }

        public Criteria andEbsUserStatusKorLikeInsensitive(String value) {
            addCriterion("upper(EBS_USER_STATUS_KOR) like", value.toUpperCase(), "ebsUserStatusKor");
            return (Criteria) this;
        }

        public Criteria andPositionEngLikeInsensitive(String value) {
            addCriterion("upper(POSITION_ENG) like", value.toUpperCase(), "positionEng");
            return (Criteria) this;
        }

        public Criteria andPositionKorLikeInsensitive(String value) {
            addCriterion("upper(POSITION_KOR) like", value.toUpperCase(), "positionKor");
            return (Criteria) this;
        }

        public Criteria andStuStatusEngLikeInsensitive(String value) {
            addCriterion("upper(STU_STATUS_ENG) like", value.toUpperCase(), "stuStatusEng");
            return (Criteria) this;
        }

        public Criteria andStuStatusKorLikeInsensitive(String value) {
            addCriterion("upper(STU_STATUS_KOR) like", value.toUpperCase(), "stuStatusKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgCodeLikeInsensitive(String value) {
            addCriterion("upper(ACAD_PROG_CODE) like", value.toUpperCase(), "acadProgCode");
            return (Criteria) this;
        }

        public Criteria andAcadProgKorLikeInsensitive(String value) {
            addCriterion("upper(ACAD_PROG_KOR) like", value.toUpperCase(), "acadProgKor");
            return (Criteria) this;
        }

        public Criteria andAcadProgEngLikeInsensitive(String value) {
            addCriterion("upper(ACAD_PROG_ENG) like", value.toUpperCase(), "acadProgEng");
            return (Criteria) this;
        }

        public Criteria andPersonTypeCodeUidLikeInsensitive(String value) {
            addCriterion("upper(PERSON_TYPE_CODE_UID) like", value.toUpperCase(), "personTypeCodeUid");
            return (Criteria) this;
        }

        public Criteria andStdntTypeIdLikeInsensitive(String value) {
            addCriterion("upper(STDNT_TYPE_ID) like", value.toUpperCase(), "stdntTypeId");
            return (Criteria) this;
        }

        public Criteria andStdntTypeClassLikeInsensitive(String value) {
            addCriterion("upper(STDNT_TYPE_CLASS) like", value.toUpperCase(), "stdntTypeClass");
            return (Criteria) this;
        }

        public Criteria andStdntCategoryIdLikeInsensitive(String value) {
            addCriterion("upper(STDNT_CATEGORY_ID) like", value.toUpperCase(), "stdntCategoryId");
            return (Criteria) this;
        }

        public Criteria andAdvrEbsPersonIdLikeInsensitive(String value) {
            addCriterion("upper(ADVR_EBS_PERSON_ID) like", value.toUpperCase(), "advrEbsPersonId");
            return (Criteria) this;
        }

        public Criteria andAdvrNameLikeInsensitive(String value) {
            addCriterion("upper(ADVR_NAME) like", value.toUpperCase(), "advrName");
            return (Criteria) this;
        }

        public Criteria andAdvrNameAcLikeInsensitive(String value) {
            addCriterion("upper(ADVR_NAME_AC) like", value.toUpperCase(), "advrNameAc");
            return (Criteria) this;
        }

        public Criteria andKaistSuidLikeInsensitive(String value) {
            addCriterion("upper(KAIST_SUID) like", value.toUpperCase(), "kaistSuid");
            return (Criteria) this;
        }

        public Criteria andAdvrKaistUidLikeInsensitive(String value) {
            addCriterion("upper(ADVR_KAIST_UID) like", value.toUpperCase(), "advrKaistUid");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLikeInsensitive(String value) {
            addCriterion("upper(CREATORID) like", value.toUpperCase(), "creatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLikeInsensitive(String value) {
            addCriterion("upper(UPDATORID) like", value.toUpperCase(), "updatorId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}