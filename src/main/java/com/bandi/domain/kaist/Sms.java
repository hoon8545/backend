package com.bandi.domain.kaist;

import java.io.Serializable;
import java.util.Date;

import com.bandi.domain.base.BaseObject;

public class Sms extends BaseObject implements Serializable {
    private Short trNum;

    private String tran_date;

    private Short trSerialnum;

    private String trId;

    private String tran_status;

    private String trRsltstat;

    private String tran_type;

    private String tran_phone;

    private String tran_callback;

    private Date trRsltdate;

    private Date trModified;

    private String tran_msg;

    private String trNet;

    private String trEtc1;

    private String trEtc2;

    private String trEtc3;

    private String trEtc4;

    private String trEtc5;

    private String trEtc6;

    private Date trRealsenddate;

    private String trRouteid;

    private static final long serialVersionUID = 1L;

    public Short getTrNum() {
        return trNum;
    }

    public void setTrNum(Short trNum) {
        this.trNum = trNum;
    }

    public String getTran_date() {
        return tran_date;
    }

    public void setTran_date(String tran_date) {
        this.tran_date = tran_date == null ? null : tran_date.trim();
    }

    public Short getTrSerialnum() {
        return trSerialnum;
    }

    public void setTrSerialnum(Short trSerialnum) {
        this.trSerialnum = trSerialnum;
    }

    public String getTrId() {
        return trId;
    }

    public void setTrId(String trId) {
        this.trId = trId == null ? null : trId.trim();
    }

    public String getTran_status() {
        return tran_status;
    }

    public void setTran_status(String tran_status) {
        this.tran_status = tran_status == null ? null : tran_status.trim();
    }

    public String getTrRsltstat() {
        return trRsltstat;
    }

    public void setTrRsltstat(String trRsltstat) {
        this.trRsltstat = trRsltstat == null ? null : trRsltstat.trim();
    }

    public String getTran_type() {
        return tran_type;
    }

    public void setTran_type(String tran_type) {
        this.tran_type = tran_type == null ? null : tran_type.trim();
    }

    public String getTran_phone() {
        return tran_phone;
    }

    public void setTran_phone(String tran_phone) {
        this.tran_phone = tran_phone == null ? null : tran_phone.trim();
    }

    public String getTran_callback() {
        return tran_callback;
    }

    public void setTran_callback(String tran_callback) {
        this.tran_callback = tran_callback == null ? null : tran_callback.trim();
    }

    public Date getTrRsltdate() {
        return trRsltdate;
    }

    public void setTrRsltdate(Date trRsltdate) {
        this.trRsltdate = trRsltdate;
    }

    public Date getTrModified() {
        return trModified;
    }

    public void setTrModified(Date trModified) {
        this.trModified = trModified;
    }

    public String getTran_msg() {
        return tran_msg;
    }

    public void setTran_msg(String tran_msg) {
        this.tran_msg = tran_msg == null ? null : tran_msg.trim();
    }

    public String getTrNet() {
        return trNet;
    }

    public void setTrNet(String trNet) {
        this.trNet = trNet == null ? null : trNet.trim();
    }

    public String getTrEtc1() {
        return trEtc1;
    }

    public void setTrEtc1(String trEtc1) {
        this.trEtc1 = trEtc1 == null ? null : trEtc1.trim();
    }

    public String getTrEtc2() {
        return trEtc2;
    }

    public void setTrEtc2(String trEtc2) {
        this.trEtc2 = trEtc2 == null ? null : trEtc2.trim();
    }

    public String getTrEtc3() {
        return trEtc3;
    }

    public void setTrEtc3(String trEtc3) {
        this.trEtc3 = trEtc3 == null ? null : trEtc3.trim();
    }

    public String getTrEtc4() {
        return trEtc4;
    }

    public void setTrEtc4(String trEtc4) {
        this.trEtc4 = trEtc4 == null ? null : trEtc4.trim();
    }

    public String getTrEtc5() {
        return trEtc5;
    }

    public void setTrEtc5(String trEtc5) {
        this.trEtc5 = trEtc5 == null ? null : trEtc5.trim();
    }

    public String getTrEtc6() {
        return trEtc6;
    }

    public void setTrEtc6(String trEtc6) {
        this.trEtc6 = trEtc6 == null ? null : trEtc6.trim();
    }

    public Date getTrRealsenddate() {
        return trRealsenddate;
    }

    public void setTrRealsenddate(Date trRealsenddate) {
        this.trRealsenddate = trRealsenddate;
    }

    public String getTrRouteid() {
        return trRouteid;
    }

    public void setTrRouteid(String trRouteid) {
        this.trRouteid = trRouteid == null ? null : trRouteid.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", trNum=").append(trNum);
        sb.append(", tran_date=").append(tran_date);
        sb.append(", trSerialnum=").append(trSerialnum);
        sb.append(", trId=").append(trId);
        sb.append(", tran_status=").append(tran_status);
        sb.append(", trRsltstat=").append(trRsltstat);
        sb.append(", tran_type=").append(tran_type);
        sb.append(", tran_phone=").append(tran_phone);
        sb.append(", tran_callback=").append(tran_callback);
        sb.append(", trRsltdate=").append(trRsltdate);
        sb.append(", trModified=").append(trModified);
        sb.append(", tran_msg=").append(tran_msg);
        sb.append(", trNet=").append(trNet);
        sb.append(", trEtc1=").append(trEtc1);
        sb.append(", trEtc2=").append(trEtc2);
        sb.append(", trEtc3=").append(trEtc3);
        sb.append(", trEtc4=").append(trEtc4);
        sb.append(", trEtc5=").append(trEtc5);
        sb.append(", trEtc6=").append(trEtc6);
        sb.append(", trRealsenddate=").append(trRealsenddate);
        sb.append(", trRouteid=").append(trRouteid);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}