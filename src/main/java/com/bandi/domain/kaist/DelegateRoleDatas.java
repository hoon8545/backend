package com.bandi.domain.kaist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DelegateRoleDatas{

    protected List<DelegateHistory> delegateRoles = new ArrayList<>();
    protected Map<String, List<DelegateAuthority>> delegateAuthoritys = new HashMap<>();
    protected List<String> deleteDelegateRoles = new ArrayList<>();

    public static final String START_CONDITION_FLAG = "S";
    public static final String END_CONDITION_FLAG = "E";

    public List<DelegateHistory> getDelegateRoles() {
        return delegateRoles;
    }
    public void setDelegateRoles(List<DelegateHistory> delegateRoles) {
        this.delegateRoles = delegateRoles;
    }
    public Map<String, List<DelegateAuthority>> getDelegateAuthoritys() {
        return delegateAuthoritys;
    }
    public void setDelegateAuthoritys(Map<String, List<DelegateAuthority>> delegateAuthoritys) {
        this.delegateAuthoritys = delegateAuthoritys;
    }
    public List<String> getDeleteDelegateRoles() {
        return deleteDelegateRoles;
    }
    public void setDeleteDelegateRoles(List<String> deleteDelegateRoles) {
        this.deleteDelegateRoles = deleteDelegateRoles;
    }

}
