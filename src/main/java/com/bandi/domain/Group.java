package com.bandi.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import com.bandi.domain.base.BaseObject;

public class Group extends BaseObject implements Serializable{

    private String id;

    private String name;

    private String description;

    private int sortOrder;

    private String parentId;

    private String originalParentId;

    private String fullPathIndex;

    private int subLastIndex;

    private String status;

    private String creatorId;

    private Timestamp createdAt;

    private String updatorId;

    private Timestamp updatedAt;

    private String flagSync;

	private String typeCode;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId == null ? null : parentId.trim();
    }

    public String getOriginalParentId() {
        return originalParentId;
    }

    public void setOriginalParentId(String originalParentId) {
        this.originalParentId = originalParentId == null ? null : originalParentId.trim();
    }

    public String getFullPathIndex() {
        return fullPathIndex;
    }

    public void setFullPathIndex(String fullPathIndex) {
        this.fullPathIndex = fullPathIndex == null ? null : fullPathIndex.trim();
    }

    public int getSubLastIndex() {
        return subLastIndex;
    }

    public void setSubLastIndex(int subLastIndex) {
        this.subLastIndex = subLastIndex;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId == null ? null : creatorId.trim();
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(String updatorId) {
        this.updatorId = updatorId == null ? null : updatorId.trim();
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getFlagSync() {
        return flagSync;
    }

    public void setFlagSync(String flagSync) {
        this.flagSync = flagSync == null ? null : flagSync.trim();
    }

	public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode == null ? null : typeCode.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", description=").append(description);
        sb.append(", sortOrder=").append(sortOrder);
        sb.append(", parentId=").append(parentId);
        sb.append(", originalParentId=").append(originalParentId);
        sb.append(", fullPathIndex=").append(fullPathIndex);
        sb.append(", subLastIndex=").append(subLastIndex);
        sb.append(", status=").append(status);
        sb.append(", creatorId=").append(creatorId);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", updatorId=").append(updatorId);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", originalParentGroupCode=").append(originalParentGroupCode);
        sb.append(", flagSync=").append(flagSync);
		sb.append(", typeCode=").append(typeCode);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    // ===================== End of Code Gen =====================

    public static final String GROUP_FULLPATH_NAME_SEPARATOR = ">";
    public static final String GROUP_ROOT_OID		= "G000";
    public static final String GROUP_RETIRE_OID 	= "R000";
    public static final String GROUP_UNDEFINED_OID  = "U000";

    public static final String CONST_STATUS_DELETE = "D";
    public static final String CONST_STATUS_ACTIVE = "A";

    private String originalParentGroupCode;

    public String getOriginalParentGroupCode() {
        return originalParentGroupCode;
    }

    public void setOriginalParentGroupCode(String originalParentGroupCode) {
        this.originalParentGroupCode = originalParentGroupCode;
    }

    // Tree 구현 시 사용자 포함 여부
    private boolean includeUser;

    public boolean isIncludeUser(){
        return includeUser;
    }

    public void setIncludeUser( boolean includeUser ){
        this.includeUser = includeUser;
    }

    private String clientOid;

    public String getClientOid(){
        return clientOid;
    }

    public void setClientOid( String clientOid ){
        this.clientOid = clientOid;
    }

}