package com.bandi.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import com.bandi.common.BandiConstants;
import com.bandi.domain.base.BaseObject;

public class Client extends BaseObject implements Serializable {
    private String oid;

    private String name;

    private String secret;

    private String url;

    private String redirectUri;

    private String ip;

    private String aliveFlag;

    private String flagLicense;

    private String creatorId;

    private Timestamp createdAt;

    private String updatorId;

    private Timestamp updatedAt;

    private String hashValue;

    private String flagValid;

    private String publicKey;

    private String flagUseSso;

    // -- KAIST
    private String flagUseEam;

    private String flagUseOtp;

    private String ssoType;

	private String description;

    private String infoMarkOptn;

    private int serviceOrderNo;

    private String flagUse;

    private String flagOpen;

    private String managerId;

    private String flagOtpRequired;

    private String userType;

    private String eamGroup;

    private static final long serialVersionUID = 1L;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid == null ? null : oid.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret == null ? null : secret.trim();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri == null ? null : redirectUri.trim();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    public String getAliveFlag() {
        return aliveFlag;
    }

    public void setAliveFlag(String aliveFlag) {
        this.aliveFlag = aliveFlag == null ? null : aliveFlag.trim();
    }

    public String getFlagLicense() {
        return flagLicense;
    }

    public void setFlagLicense(String flagLicense) {
        this.flagLicense = flagLicense == null ? null : flagLicense.trim();
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId == null ? null : creatorId.trim();
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(String updatorId) {
        this.updatorId = updatorId == null ? null : updatorId.trim();
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getHashValue() {
        return hashValue;
    }

    public void setHashValue(String hashValue) {
        this.hashValue = hashValue == null ? null : hashValue.trim();
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey == null ? null : publicKey.trim();
    }

    public String getFlagValid() {
        return flagValid;
    }

    public void setFlagValid(String flagValid) {
        this.flagValid = flagValid == null ? null : flagValid.trim();
    }

    public String getFlagUseSso() {
        return flagUseSso;
    }

    public void setFlagUseSso(String flagUseSso) {
        this.flagUseSso = flagUseSso == null ? null : flagUseSso.trim();
    }

    public String getFlagUseEam() {
        return flagUseEam;
    }

    public void setFlagUseEam(String flagUseEam) {
        this.flagUseEam = flagUseEam == null ? null : flagUseEam.trim();
    }

    public String getSsoType() {
        return ssoType;
    }

    public void setSsoType(String ssoType) {
        this.ssoType = ssoType == null ? null : ssoType.trim();
    }

    public String getFlagUseOtp() {
        return flagUseOtp;
    }

    public void setFlagUseOtp(String flagUseOtp) {
        this.flagUseOtp = flagUseOtp == null ? null : flagUseOtp.trim();
    }

	public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getInfoMarkOptn() {
        return infoMarkOptn;
    }

    public void setInfoMarkOptn(String infoMarkOptn) {
        this.infoMarkOptn = infoMarkOptn == null ? null : infoMarkOptn.trim();
    }

    public int getServiceOrderNo() {
        return serviceOrderNo;
    }

    public void setServiceOrderNo(int serviceOrderNo) {
        this.serviceOrderNo = serviceOrderNo;
    }

    public String getFlagUse() {
        return flagUse;
    }

    public void setFlagUse(String flagUse) {
        this.flagUse = flagUse == null ? null : flagUse.trim();
    }

    public String getFlagOpen() {
        return flagOpen;
    }

    public void setFlagOpen(String flagOpen) {
        this.flagOpen = flagOpen == null ? null : flagOpen.trim();
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId == null ? null : managerId.trim();
    }

    public String getFlagOtpRequired() {
        return flagOtpRequired;
    }

    public void setFlagOtpRequired(String flagOtpRequired) {
        this.flagOtpRequired = flagOtpRequired == null ? null : flagOtpRequired.trim();
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType == null ? null : userType.trim();
    }

    public String getEamGroup() {
        return eamGroup;
    }

    public void setEamGroup(String eamGroup) {
        this.eamGroup = eamGroup == null ? null : eamGroup.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", oid=").append(oid);
        sb.append(", name=").append(name);
        sb.append(", secret=").append(secret);
        sb.append(", url=").append(url);
        sb.append(", redirectUri=").append(redirectUri);
        sb.append(", ip=").append(ip);
        sb.append(", aliveFlag=").append(aliveFlag);
        sb.append(", flagLicense=").append(flagLicense);
        sb.append(", creatorId=").append(creatorId);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", updatorId=").append(updatorId);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", ip=").append(ip);
        sb.append(", aliveFlag=").append(aliveFlag);
        sb.append(", flagLicense=").append(flagLicense);
        sb.append(", publicKey=").append(publicKey);
        sb.append(", flagValid=").append(flagValid);
        sb.append(", flagUseSso=").append(flagUseSso);
        sb.append(", flagUseEam=").append(flagUseEam);
        sb.append(", flagUseOtp=").append(flagUseOtp);
        sb.append(", ssoType=").append(ssoType);
		sb.append(", description=").append(description);
        sb.append(", infoMarkOptn=").append(infoMarkOptn);
        sb.append(", serviceOrderNo=").append(serviceOrderNo);
        sb.append(", flagUse=").append(flagUse);
        sb.append(", flagOpen=").append(flagOpen);
        sb.append(", managerId=").append(managerId);
        sb.append(", flagOtpRequired=").append(flagOtpRequired);
        sb.append(", userType=").append(userType);
        sb.append(", eamGroup=").append(eamGroup);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

	// ===================== End of Code Gen =====================

	public String getHashTarget(){
		StringBuilder sb = new StringBuilder();

		sb.append(oid).append(", ");
		sb.append(name).append(", ");
		sb.append(secret).append(", ");
		sb.append(url).append(", ");
		sb.append(redirectUri).append(", ");
		sb.append(ip).append(", ");

		return sb.toString();
	}

	public String toStringForHashValue() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("oid=").append(oid);
		sb.append(", name=").append(name);
		sb.append("]");
		return sb.toString();
	}

    public String get_rowVariant(){
        return BandiConstants.FLAG_N.equalsIgnoreCase( this.flagValid) ? "danger" : "";
    }

    private String manager;
    private String flagUseSystem;

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager == null ? null : manager.trim();
    }

    public String getFlagUseSystem() {
        return flagUseSystem;
    }

    public void setFlagUseSystem(String flagUseSystem) {
        this.flagUseSystem = flagUseSystem == null ? null : flagUseSystem.trim();
    }
    
    // 서비스포트폴리오 담당자정보
    private String koreanName;
    private String emailAddress;
    private String officeTelephoneNumber;
    private String mobileTelephoneNumber;

	public String getKoreanName() {
		return koreanName;
	}

	public void setKoreanName(String koreanName) {
		this.koreanName = koreanName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getOfficeTelephoneNumber() {
		return officeTelephoneNumber;
	}

	public void setOfficeTelephoneNumber(String officeTelephoneNumber) {
		this.officeTelephoneNumber = officeTelephoneNumber;
	}

	public String getMobileTelephoneNumber() {
		return mobileTelephoneNumber;
	}

	public void setMobileTelephoneNumber(String mobileTelephoneNumber) {
		this.mobileTelephoneNumber = mobileTelephoneNumber;
	}
}