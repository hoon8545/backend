<%@page import="java.text.DecimalFormat" %>
<%@page import="java.text.Format" %>
<%@ page contentType="text/html;charset=utf-8" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="com.bandi.common.cache.BandiCacheManager" %>
<%@ page import="net.sf.ehcache.Ehcache" %>
<%@ page import="com.bandi.common.IdGenerator" %>
<!-- page import="com.bandi.domain.Session" % -->
<%
    List caches = BandiCacheManager.getAllCaches();

    /*String cmd = request.getParameter( "cmd" );
    String cacheName = request.getParameter( "name" );

    if( cmd != null && cmd.equals( "remove" ) ){
        DestinyCacheManager.getCache( cacheName ).removeAll();
    } else if( cmd != null && cmd.equals( "activate" ) ){
        DestinyCacheManager.getCache( cacheName ).removeAll();
        DestinyCacheManager.getCache( cacheName ).setActivate( true );
    } else if( cmd != null && cmd.equals( "deactivate" ) ){
        DestinyCacheManager.getCache( cacheName ).setActivate( false );
    }*/

    Class cacheClass = null;
    boolean isRedis = false;
    long totalSize = 0;
    long totalMemory = 0;
    long redisTotalSize = 0;
    long redisTotalMemory = 0;
    long ehTotalSize = 0;
    long ehTotalMemory = 0;
    int size = 0;
    long memory = 0;
    int redisServerCount = - 1;
    Format formatter = new DecimalFormat( ".##" );
%>
<%!

%>
<html>
<head>
</head>
<style type="text/css">
    a:link {
        color: black;
        text-decoration: none;
    }

    a:visited {
        color: black;
        text-decoration: none;
    }

    a:hover {
        color: blue;
        text-decoration: underline;
    }
</style>
<body>
<script type="text/javascript">
    function doCommand(cmd, name) {
        $("#cmd:first").val(cmd);
        $("#name:first").val(name);
        $("#_frm:first").submit();
    }
</script>
<form id="_frm" action="./moncache.jsp" method="post">
    <input type="hidden" id="cmd" name="cmd" value=""/>
    <input type="hidden" id="name" name="name" value=""/>
</form>
<button onclick="javascript:doCommand(null, null);">refresh</button>
<br/>

<hr/>
Cache Memory Size (<%= new Date() %> )<br/>
<table border="1" width="100%">
    <%
        long elapsedTime = 0;

        String sizeExtraInfo = null;
        String extraInfo = null;
        for( int i = 0; i < caches.size(); i++ ){

            Ehcache cache = ( Ehcache ) caches.get( i );

            cacheClass = cache.getClass();

            sizeExtraInfo = "";
            extraInfo = "";

            size = cache.getSize();
            memory = cache.calculateInMemorySize();

            ehTotalSize += size;
            ehTotalMemory += memory;
    %>
    <tr>
        <td>
            <%= cacheClass.getSimpleName() + "-" %> <a
                href="./moncacheDetail.jsp?cacheName=<%=cache.getName()%>"><%=cache.getName()%>
        </a> :
            <%= size %><%= sizeExtraInfo %> size,
            roughly <%= memory %><%= sizeExtraInfo %><%= extraInfo%>
        </td>
    </tr>
    <% } %>
    <tr>
        <td colspan="2">ehcache total : <%=ehTotalSize%> size, roughly <%= ehTotalMemory %>
        </td>
    </tr>

</table>
</body>

</html>
